<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       require base_path() . '/resources/views/macros/common.php';
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
