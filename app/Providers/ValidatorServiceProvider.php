<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	Validator::extend('edu', 'App\Http\CustomValidator@edu'); 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}

