<?php

namespace App\Console\Commands;

use App\Http\Models\Landlord;
use App\Http\Models\LandlordSubscription;
use App\Http\Models\LandlordSubscriptionSchedule;
use DB;
use Crypt;
use Mail;
use DateTime;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Traits\ForteRequestTrait;
use App\Traits\LandlordSubscriptionTrait;

class updateLandlordSubscriptionPaymentAmount extends Command
{
    use ForteRequestTrait, LandlordSubscriptionTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updateLandlordSubPaymentAmount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates management accounts, properties, and the monthly amount for Landlord subscription tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // // Get all Landlord Subscriptions
        // $landlordSubscriptions = DB::table('landlord_subscription')->get();

        // // Go through each subscription
        // foreach($landlordSubscriptions as $landlordSubscription)
        // {
        //     $landlordId = $landlordSubscription->landlord_id;
        //     // Search and Return application count by Landlord ID
        //     $landlordApplicationCount = DB::table('forte_landlord_applications')
        //                                     ->where('landlord_id', '=', $landlordId)
        //                                     ->count();
        //     // Check if Landlord's management account number matches the number of applications they put in. They should be the same (will change logic once we can make get requests to get application by status)
        //     $landlordManagementAccountCount = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('management_group_count');
        //     echo $landlordApplicationCount . "\n";
        //     echo $landlordManagementAccountCount . "\n";

        //     if($landlordApplicationCount != $landlordManagementAccountCount)
        //     {
        //         DB::table('landlord_subscription')
        //             ->where('landlord_id','=', $landlordId)
        //             ->update(array('management_group_count' => $landlordApplicationCount));
        //     }
        // }



        // First, look through the forte landlord application table, make a get request for all applications that are not approved
        $pendingApplications = DB::table('forte_landlord_applications')
                                    ->whereNotIn('status', ['approved'])
                                    ->get();

        // Count the applications that are either 'approved' or 'pending'
        $approvedApplications = DB::table('forte_landlord_applications')
                                        ->where('status', 'approved')
                                        ->count();
        $approvedLandlordApplications = DB::table('forte_landlord_applications')
                                            ->where('status', 'approved')
                                            ->get();
        $organization_id = env('FORTE_API_RESELLER_ID');
        $base_url = env('FORTE_BASE_URL');
        $api_access_id     = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
        //$this->calculateTotalSubscriptionPaymentAmount('test', 1, 12);
        // Go through each pending application, make a get request to see the status
        if($pendingApplications)
        {
            foreach($pendingApplications as $pendingApplication)
            {
                $applicationIdPre = $pendingApplication->application_id;
                $appLandlordId = $pendingApplication->landlord_id;
                $applicationId = Crypt::decrypt($applicationIdPre);
                $landlordSubscriptionId = $this->getLandlordSubscriptionId($appLandlordId);
                $landlordSubscriptionPropertyCount = $this->getLandlordSubscriptionPropertyCount($appLandlordId);
                //echo $applicationId;
                $applicationEndpoint = $base_url . '/organizations/' . $organization_id . '/applications/' . $applicationId;
                //exit($appLandlordId);
                $applicationCh = $this->forteGet($applicationEndpoint, $auth_token, $organization_id);
                $response = curl_exec($applicationCh);
                $info = curl_getinfo($applicationCh);
                curl_close($applicationCh);
                $data = json_decode($response);

                $applicationStatus = property_exists($data, 'status') ? $data->status : null;
               

                //If approved, update the database accordingly.
                if($applicationStatus == 'approved')
                {
                    DB::table('forte_landlord_applications')->where('application_id', '=', $applicationIdPre)
                        ->update(array('status' => 'approved'));
                }

                $landlordManagementAccountCount = DB::table('landlord_subscription')->where('landlord_id', '=', $appLandlordId)->value('management_group_count');
                $landlordApprovedApplicationCount = DB::table('forte_landlord_applications')->where('landlord_id', '=', $appLandlordId)->count();
                if($landlordManagementAccountCount != $landlordApprovedApplicationCount)
                {
                    DB::table('landlord_subscription')->where('landlord_id', '=', $appLandlordId)
                        ->update(array('management_group_count'=>$landlordApprovedApplicationCount));


                    // Now calculate new subscription amount
                    $newSubAmount = $this->calculateTotalSubscriptionPaymentAmount($landlordSubscriptionId, $landlordSubscriptionPropertyCount, $landlordApprovedApplicationCount);

                    // Change it in the table
                    $this->updateLandlordSubscriptionMonthlyAmount($landlordId, $newSubAmount);

                    // Update the Forte Schedule
                    $landlordScheduleUpdate = $this->updateLandlordSubscriptionAmountForte($newSubAmount, $appLandlordId);
                }
            }
        }
        else
        {
            foreach($approvedLandlordApplications as $approvedLandlordApplication)
            {
                $appLandlordId = $approvedLandlordApplication->landlord_id;
                // Update all Subscription Amounts to the right amount
                $landlordManagementAccountCount = DB::table('landlord_subscription')->where('landlord_id', '=', $appLandlordId)->value('management_group_count');
                $landlordApprovedApplicationCount = DB::table('forte_landlord_applications')->where('landlord_id', '=', $appLandlordId)->count();
                
                
                $landlordSubscriptionId = $this->getLandlordSubscriptionId($appLandlordId);
                $landlordSubscriptionPropertyCount = $this->getLandlordSubscriptionPropertyCount($appLandlordId);

                // Now calculate new subscription amount
                $newSubAmount = $this->calculateTotalSubscriptionPaymentAmount($landlordSubscriptionId, $landlordSubscriptionPropertyCount, $landlordApprovedApplicationCount);

                    // Change it in the table
                    $this->updateLandlordSubscriptionMonthlyAmount($landlordId, $newSubAmount);

                    // Update the Forte Schedule
                    $landlordScheduleUpdate = $this->updateLandlordSubscriptionAmountForte($newSubAmount, $appLandlordId);
            }
        }
    }

    public function getSubscriptionPlanAdditionalBasePrice($subscription_plan_id)
    {
        $subscription_additional_accounts_price = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('additional_accounts_base_price');
        return floatval($subscription_additional_accounts_price);
    }

    public function getSubscriptionPlanInitialBasePrice($subscription_plan_id)
    {
        $subscription_plan_id = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)
                                    ->value('initial_base_price');
        return floatval($subscription_plan_id);
    }
    public function getSubscriptionPlanPropertyPrice($subscription_plan_id)
    {
        $subscription_property_price = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('price_per_property');
        return floatval($subscription_property_price);
    }
    private function updateLandlordSubscriptionMonthlyAmount($landlordId, $newSubscriptionAmount)
    {
        DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)
            ->update(array('monthly_payment_amount'=>$newSubscriptionAmount));
    }

    public function getLandlordSubscriptionPropertyCount($landlordId)
    {
        $landlordSubscriptionPropertyCount = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('property_count');
        return intval($landlordSubscriptionPropertyCount);
    }

    public function getLandlordSubscriptionManagementAccountCount($landlordId)
    {
        $landlordSubscriptionManagementAccountCount = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('management_group_count');
        return intval($landlordSubscriptionManagementAccountCount);
    }

    public function getLandlordSubscriptionId($landlordId)
    {
        $landlordSubscriptionId = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('id');
        return $landlordSubscriptionId;
    }

    // Get Landlord's Subscription FORTE Schedule ID/Token, NOT THE ID OF THE SCHEDULE TABLE
    public function getLandlordSubscriptionScheduleId($landlordId)
    {
        $landlordSubscriptionScheduleId = DB::table('landlord_subscription_schedules')
                                            ->join('landlord_subscription', function ($join) use ($landlordId) {
                                                $join->on('landlord_subscription_schedules.landlord_subscription_id', '=', 'landlord_subscription.id')
                                                    ->where('landlord_subscription.landlord_id', '=', $landlordId);
                                                })
                                            ->value('landlord_subscription_schedule_id');
        if($landlordSubscriptionScheduleId != null)
        {
            $landlordSubscriptionScheduleId = Crypt::decrypt($landlordSubscriptionScheduleId);
        }
        else
        {
            $landlordSubscriptionScheduleId = null;
        }

        return $landlordSubscriptionScheduleId;
    }

    // Grab the customer ID of the Landlord that subscribed
    public function getLandlordCustomerId($landlordId)
    {
        $customer_id = DB::table('forte_landlord_subscription_pay_settings')
            ->where('landlord_id', $landlordId)
            ->value('customer_id');
        if($customer_id == '')
        {
            $current_customer_id = null;
        }
        else
        {
            try{
                $current_customer_id = Crypt::decrypt($customer_id);
            } catch (DecryptException $e){
                echo "Can't get the Id";
            }
        }
        return $current_customer_id;
    }




}    

