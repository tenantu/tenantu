<?php

namespace App\Console\Commands;

use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\Landlord;
use App\Http\Models\LandlordTenant;
use App\Http\Models\TenantPaymentSetting;
use App\Http\Models\LandlordPaymentReceiveSetting;
use App\Http\Models\ForteRentTransaction;
use DB;
use Crypt;
use Mail;
use Config;
use Carbon\Carbon;
use DateTime;
use Illuminate\Console\Command;


class CheckAutoPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkautopay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks Auto Pay and then executes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      // WHAT THIS AIMS TO DO:
      // 1. It makes a query to grab all the transactions from landlord_tenant_monthlyrents who's due date falls on today's date (date of running)
      // 2. Iterates through each record grabbed via foreach, and executes a Forte Transaction using parameters from the query
      // 3. Basically the same stuff as the manual pay button (posts to Forte, posts to our web DB, etc) We are also aiming to send e-mail notifications for any failed transactions.
        // Let's do cool stuff

        $base_url = env('FORTE_BASE_URL');
    	  $api_access_id = env('FORTE_API_ACCESS_ID');
        echo "bitch ass:" . $api_access_id . "\n";
    	  $api_secure_key = env('FORTE_API_SECURE_KEY');
        echo $api_secure_key . "\n";
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
        $today_date_time = Carbon::today();
        $today_date = substr($today_date_time, 0, 10);
        echo $today_date . "\n";

        echo Carbon::now() . "\n";


        $due_rent_payments = DB::table('landlord_tenants')
                        ->join('forte_tenant_pay_settings', function($join){
                          $join->on('landlord_tenants.tenant_id', '=', 'forte_tenant_pay_settings.tenant_id')
                                  ->where('forte_tenant_pay_settings.ready_pay', '=', 1)
                                  ->where('landlord_tenants.active' ,'=', 1);
                        })
                        ->join('landlord_tenant_monthlyrents', function($join) use($today_date){
                          $join->on('landlord_tenant_monthlyrents.landlord_tenant_id', '=', 'landlord_tenants.id')
                                  ->where('landlord_tenant_monthlyrents.bill_on', 'LIKE', $today_date);
                        })
                        ->get();

        // Go through each record retrieved and make them transactions
        foreach($due_rent_payments as $rent_payment)
        {
          $currentTenantId = $rent_payment->tenant_id;
          $organization_id = $this->getTenantCurrentLandlordOrgId($currentTenantId);
          $location_id = $this->getTenantCurrentPropertyLocationId($currentTenantId);
          // Service Fee
          $service_fee = 1.25;
          // Monthly Rent Amount
          $rent_amount = $rent_payment->amount;

          // Check whether this guy is on AutoPay
          $auto_pay = $rent_payment->auto_pay;
          echo $auto_pay . "\n";

          // Get Tenant's First Name for E-Mail
          $current_tenant_firstname = $this->getTenantFirstNamebyId($currentTenantId);
          // Get Tenant's E-Mail
          $current_tenant_email = $this->getTenantEmail($currentTenantId);
          $get_this_month = date('M');
          $tenant_address_id = Crypt::decrypt($rent_payment->addr_id);
          $tenant_customer_id = Crypt::decrypt($rent_payment->customer_id);
          $addr_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $tenant_customer_id . '/addresses/' . $tenant_address_id;
          $tenant_billing_address = $this->forteGet($addr_endpoint, $auth_token, $organization_id);
          $addr_response = curl_exec($tenant_billing_address);
      	  $addr_resp_info = curl_getinfo($tenant_billing_address);
      	  curl_close($tenant_billing_address);
      	  // This associative array is used for the billing_address param in the transaction param array
      	  $billing_address_data = json_decode($addr_response, true);

          $property_name = $this->getTenantCurrentProperty($currentTenantId);

          // This variable is just a means of describing the transaction (kind of) in Forte (Object attribute is reference_id)
          $reference_id = $get_this_month . '-AutoPay-' . $property_name;

          // Amount that includes service fee and rent_amount
          $final_rent_amount = $rent_amount + $service_fee;

          // Get the Due Date
          echo $rent_payment->bill_on . " :\n";

          // Rent Params to get put into the transaction to be posted
          $rent_transaction_params = array(
            'action' => 'sale',
            'customer_token'=>$tenant_customer_id,
            'authorization_amount' => $final_rent_amount,
            'billing_address' => $billing_address_data,
            //'service_fee_amount' => $service_fee,
            'reference_id'  => $reference_id
          );

           echo "Customer Token".$rent_transaction_params['customer_token'] . "\n";
           echo "Location ID: ".$location_id . "\n";
           echo "Authorized Amount: " . $final_rent_amount . "\n";
           echo "Reference_ID: " . $rent_transaction_params['reference_id'] . "\n";

          // Execute transaction
          $auto_rent_pay = $this->makeRentTransaction($rent_transaction_params, $currentTenantId, $location_id, $organization_id);

      	  // Post Responses
      	  $post_trans_response = json_decode(json_encode($auto_rent_pay['data']))->response;
      	  $post_trans_data = json_decode(json_encode($auto_rent_pay['data']));


          // Params to be used to store transaction info in database
          $post_transaction_db_params = array(
      	    'transaction_token'     => Crypt::encrypt($post_trans_data->transaction_id),
      	    'authorized_amount'    => $post_trans_data->authorization_amount,
      	    'response_code'         => $post_trans_response->response_code,
              'monthly_rent_unique_id' => $rent_payment->unique_id,
              'date_due'              => $rent_payment->bill_on
      	    );
            // POST THE PROPERTIES RETRIEVED FROM POSTED TRANSACTION
        	  $this->postRentTransactionDB($post_transaction_db_params, $currentTenantId);
            print_r($post_trans_response);
            exit('nigga');
            if ($post_trans_response->response_code != 'A01')
            {
              //continue;
              // Send Tenant an e-mail warning them that there was something wrong with the transaction
              echo "THIS FAILED HERE";
              $this->failedTransactionEmail($current_tenant_email, $reference_id, $current_tenant_firstname);
            }
            else
            {
              $update_monthly_rent_status = DB::table('landlord_tenant_monthlyrents')
                                              ->where('unique_id', $rent_payment->unique_id)
                                              ->update(['status'=>1]);
            }

            // Check if auto_pay is TRUE, it's not TRUE, set Ready_Pay back to 0, they need to press the button again to pay
            if($auto_pay != 1)
            {
              DB::table('forte_tenant_pay_settings')
                ->where('tenant_id', $currentTenantId)
                ->update(['ready_pay'=>0]);
              echo "UPDATED\n";
            }
            else
            {
              // This tenant is auto-paying like he has better stuff to do with his life than clicking a button every month
              // Leave Brittany alone
              continue;
            }
        }


        // echo "Customer Token".$rent_transaction_params['customer_token'] . "\n";
        // echo "Location ID: ".$location_id . "\n";
        // echo "Authorized Amount: " . $final_rent_amount . "\n";
        // echo "Reference_ID: " . $rent_transaction_params['reference_id'] . "\n";
        var_dump($due_rent_payments);

    }

    // This method takes the rent parameters from each Ready to Pay Tenant's Due Rent Payment and makes a Forte Transaction
    private function makeRentTransaction($rent_transaction_params, $tenantId, $landlord_loc_id, $organization_id)
    {
      $base_url           = env('FORTE_BASE_URL');
      $api_access_id      = env('FORTE_API_ACCESS_ID');
      $api_secure_key     = env('FORTE_API_SECURE_KEY');
      $auth_token         = base64_encode($api_access_id . ':' . $api_secure_key);
      $rent_transaction_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $landlord_loc_id . '/transactions';
      $transaction_rent_pay  = $this->fortePost($rent_transaction_endpoint, $rent_transaction_params, $auth_token, $organization_id);
      $transaction_pay_response = curl_exec($transaction_rent_pay);
      $transaction_pay_info     = curl_getinfo($transaction_rent_pay);
      curl_close($transaction_rent_pay);
      $transaction_data = json_decode($transaction_pay_response);
      $create_http_success = (($transaction_pay_info['http_code'] != 201) ? 0:1);

      $createOutput = array(
        'create_http_success'   => $create_http_success,
        'data'                  => $transaction_data
      );

      return $createOutput;
    }
    // Post Forte Rent Transaction Info into the Database (table: forte_rent_transactions)
    private function postRentTransactionDB($rent_transaction_params, $tenantId)
    {
        $currentTenantLeaseRelationshipId = $this->getTenantCurrentLeaseId($tenantId);

        $store_rent_transaction = new ForteRentTransaction;
        $store_rent_transaction->landlord_tenant_id = $currentTenantLeaseRelationshipId;
        $store_rent_transaction->landlord_tenant_monthlyrents_unique_id = $rent_transaction_params['monthly_rent_unique_id'];
        $store_rent_transaction->transaction_token = $rent_transaction_params['transaction_token'];
        $store_rent_transaction->authorized_amount = $rent_transaction_params['authorized_amount'];
        $store_rent_transaction->date_due           = $rent_transaction_params['date_due'];
        $store_rent_transaction->date_posted = Carbon::now();
        $store_rent_transaction->response_code = $rent_transaction_params['response_code'];
        $store_rent_transaction->save();
    }

      // Send Failed Transaction E-Mail
    private function failedTransactionEmail($tenant_email, $reference_id, $tenantfirstname)
    {
      $data = array(
        'email' => $tenant_email,
        'reference_id' => $reference_id,
        'first_name' => $tenantfirstname
      );

        Mail::send('emails.failedtransaction', ['reference_id' => $reference_id, 'tenant_firstname' => $tenantfirstname], function($message) use ($data)
        {
          $message->to($data['email'], $data['first_name'])->subject( 'TenantU - Transaction Failure');
        });
    }
    //This method takes two dateTime objects, either built in php dateTime objects or carbon objects,
    //and compares if the days are the same, regardless of times
    private function compareCarbonDatesIgnoreTime($dateTime1, $dateTime2)
    {
        $dateTimeArr1 = date_parse($dateTime1);
        $dateTimeArr2 = date_parse($dateTime2);

        if ($dateTimeArr1['year'] != $dateTimeArr2['year'])
        {
            return false;
        }
        else if ($dateTimeArr1['month'] != $dateTimeArr2['month'])
        {
            return false;
        }
        else if ($dateTimeArr1['day'] != $dateTimeArr2['day'])
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    private function forteGet($endpoint, $auth_token)
  	{
  	    $organization_id = Config::get('constants.FORTE_TENANTU_ORG_ID');
  	    $ch = curl_init($endpoint);
  	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
  	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  	    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
  	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  	    	'Authorization: Basic ' . $auth_token,
  	    	'X-Forte-Auth-Organization-id: ' . $organization_id,
  	    	'Accept:application/json',
  	    	'Content-type: application/json'
  		));

  	    return $ch;
  	}
    private function fortePost($endpoint, $params, $auth_token)
    {
        $organization_id = Config::get('constants.FORTE_TENANTU_ORG_ID');
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }
    private function getTenantFirstNamebyId($tenantId)
    {
      $tenant_firstname = DB::table('tenants')->where('id', $tenantId)->value('firstname');
      return $tenant_firstname;
    }

    private function getTenantCurrentLeaseId($tenantId)
    {
        //$todays_date = date("Y-m-d");

        $lease_relationship_id = DB::table('landlord_tenants')
                                ->where('tenant_id', $tenantId)
                                ->where('active', 1)
                                //->whereBetween($todays_date, ['agreement_start_date', 'agreement_end_date'])
                                ->value('id');
        return $lease_relationship_id;
    }
    private function getTenantCurrentLandlordLocationId($tenantId)
    {
        $currentlandlord_pay = DB::table('landlord_tenants')
            -> join('forte_landlord_pay_settings_rent', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_rent.landlord_id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
                //->first();
                    ->value('loc_id');
        if($currentlandlord_pay == '')
        {
          $currentlandlord_loc_id	= $currentlandlord_pay;
        }
        try{
            $currentlandlord_loc_id = Crypt::decrypt($currentlandlord_pay);
        } catch (DecryptException $e){
            echo "This is wrong";
        }
        return $currentlandlord_loc_id;
    }
    public function getTenantCurrentLandlordOrgId($tenantId)
    {
        $currentlandlord_org_id = null;
        $currentlandlord_pay = DB::table('landlord_tenants')
            -> join('forte_landlord_pay_settings_rent', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_rent.landlord_id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->value('org_id');
        // If the query is null, set org id to null, it's not there
        if($currentlandlord_pay == '')
        {
            $currentlandlord_org_id = null;
        }
        else // It's there, decrypt it and return it.
        {
            try{
                $currentlandlord_org_id = Crypt::decrypt($currentlandlord_pay);
            } catch (\Encryption $e){
                echo "This is wrong";
            }
            return $currentlandlord_org_id;
        }
    }
    ///////////////////////////////////////////////////////////////////
    // Get the current Property ID that the logged Tenant is living in //
    /////////////////////////////////////////////////////////////////
    public function getTenantCurrentPropertyId($tenantId)
    {
        $currentproperty = DB::table('landlord_tenants')
                        -> join('properties', function ($join) use ($tenantId) {
                            $join->on('landlord_tenants.property_id', '=', 'properties.id')
                                 ->where('landlord_tenants.tenant_id', '=', $tenantId)
                                 ->where('landlord_tenants.active', '=', 1);
                        })
                        ->first();
        if($currentproperty != false)
        {
          $currentpropertyid = $currentproperty->property_id;
                return $currentpropertyid;
        }
      else
        {
         return false;
      }
    }
    // Need LLC's to get this done
    public function getTenantCurrentPropertyLocationId($tenantId)
    {
        // Get Tenant Current Property ID
        $currentPropertyId = $this->getTenantCurrentPropertyId($tenantId);
        $currentPropertyLocationId = DB::table('landlord_property_management_accounts')
                                ->join('properties', function ($join) use ($currentPropertyId){
                                    $join->on('landlord_property_management_accounts.id', '=', 'properties.landlord_prop_mgmt_id')
                                        ->where('properties.id', '=', $currentPropertyId);
                                })
                                ->value('loc_id');
        if($currentPropertyLocationId == '')
        {
            $loc_id = null;
        }
        else
        {
            try{
                $loc_id = Crypt::decrypt($currentPropertyLocationId);
            } catch (Encryption $e){
                echo "This is wrong";
            }  
        }
        return $loc_id;
    }
    private function getTenantCurrentProperty($tenantId)
    {
        $currentproperty = DB::table('landlord_tenants')
                        -> join('properties', function ($join) use ($tenantId) {
                            $join->on('landlord_tenants.property_id', '=', 'properties.id')
                                 ->where('landlord_tenants.tenant_id', '=', $tenantId)
                                 ->where('landlord_tenants.active', '=', 1);
                        })
                        ->first();
        $currentpropertyname = $currentproperty->property_name;
        return $currentpropertyname;
    }
    // Tenant E-Mail address
    private function getTenantEmail($tenantId)
    {
      $tenant_email = DB::table('tenants')->where('id',$tenantId)->value('email');
      return $tenant_email;
    }
}
