<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Crypt;
use Carbon\Carbon;
use App\Http\Models\LandlordPaymentReceiveSetting;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\LandlordTenant;
use Mail;


class makeLeaseActive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'makeLeaseActive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Everyday check which leases to make active, and which leases to make inactive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Grab dates to be compared
        $today_date_time = Carbon::today();
        $today_date = substr($today_date_time, 0, 10);
        $dateStarts = DB::table('landlord_tenants')
                        ->where('agreement_start_date', 'LIKE', $today_date)
                        ->where('active', '=', 2)
                        ->get();
        $dateEnds = DB::table('landlord_tenants')
                        ->where('agreement_end_date', 'LIKE', $today_date)
                        ->where('active', '=', 1)
                        ->get();

        if ($dateEnds != null)
        {
            // Deactivate completed leases first.
            foreach($dateEnds as $dateEnd)
            {
                // 
                $landlordTenantId = $dateEnd->id;
                // Update the Ending Lease status to 0 (inactive)
                DB::table('landlord_tenants')
                    ->where('id', '=', $landlordTenantId)
                    ->update(['active'=>0]);
                // Send E-Mail notifiyng Tenant that your lease ends
            }
        }
        if($dateStarts != null)
        {
            
            // Activate the pending leases next
            foreach($dateStarts as $dateStart)
            {
                
                $landlordTenantPropertyId = $dateStart->property_id;
                $landlordTenantLandlordId = $dateStart->landlord_id;

                $due_on = $this->getLandlordRentDueDay($landlordTenantLandlordId);
                list($monthStart, $dayStart, $yearStart) =  explode('-', $dateStart->agreement_start_date);
                $factor = $this->getFactorByLandlordRentCollectionFreq($landlordTenantLandlordId);
                $landlordTenantId = $dateStart->id;


                // Update the Starting Lease status to 1 (active)
                DB::table('landlord_tenants')
                ->where('id', '=', $landlordTenantId)
                ->update(['active'=>1]);
                
                $loopCount = "";
                if(($dateStart->cycle)!=""){
                    switch($dateStart->cycle){
                           case "3months":
                             $loopCount = 3;
                           break;
                           case "6months":
                             $loopCount = 6;
                           break;
                           case "1year":
                             $loopCount = 12;
                           break;
                           case "2year":
                             $loopCount = 24;
                           break;
                           case "3year":
                              $loopCount =36;
                           break;
                    }
                }

                $this->updateOtherTenantsRent($landlordTenantPropertyId, $loopCount, $factor, $due_on, $monthStart, $yearStart);

                // Send E-Mail notifying Tenant that your lease ends

            }
        }
    }

    //This method takes two dateTime objects, either built in php dateTime objects or carbon objects,
    //and compares if the days are the same, regardless of times
    private function compareCarbonDatesIgnoreTime($dateTime1, $dateTime2)
    {
        $dateTimeArr1 = date_parse($dateTime1);
        $dateTimeArr2 = date_parse($dateTime2);

        if ($dateTimeArr1['year'] != $dateTimeArr2['year'])
        {
            return false;
        }
        else if ($dateTimeArr1['month'] != $dateTimeArr2['month'])
        {
            return false;
        }
        else if ($dateTimeArr1['day'] != $dateTimeArr2['day'])
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // public function updateOtherTenantsRent($propertyId, $loopCount, $factor, $due_on, $monthStart, $yearStart)
    // {
    //     // get current rent rate and set initial bill date
    //     $currentRent = ($this->getPropertyPrice($propertyId)/$this->getPropertyCurrentTenantCount($propertyId));
    //     // get landlordTenants tenants who are active on property
    //     $activeLandlordTenants = LandlordTenant::where('property_id', $propertyId)
    //         ->where('active', 1)
    //         ->select('tenant_id')
    //         ->get()
    //         ->toArray();
    //     // update all of their month_rent_initial fields accordingly
    //     foreach ($activeLandlordTenants as $tenant){
    //     $updateActiveTenantsMonthRentInitial = LandlordTenant::where('property_id', $propertyId)
    //         ->where('tenant_id',$tenant)
    //         ->where('active', 1)
    //         ->update(['month_rent_initial' => $currentRent]);
    //     }
    //     // get all landlord_tenant_id's and lease start dates for a given property which are active
    //     $activeLandlordTenantIds = LandlordTenant::where('property_id',$propertyId)
    //         ->where('active',1)
    //         ->select('id','agreement_start_date')
    //         ->get()
    //         ->toArray();
    //     // for each of these active landlord_tenant_id's update their monthly_rent amount accordingly
    //     foreach($activeLandlordTenantIds as $landlordTenantId){
    // $nextpaymonth = 0;
    // $billDate = $yearStart.'-'.$monthStart.'-'.$due_on;
    //     for($i=1;$i<=$loopCount/$factor;$i++){
    //     $nextpaymonth = $i * $factor;
    //     $nextBillDate = date("Y-m-d", strtotime("+".$nextpaymonth." month", strtotime($billDate)));
    //     $updatingMonth =  landlordTenantMonthlyRent::where('landlord_tenant_id', $landlordTenantId["id"])
    //                                               ->where('bill_on', $nextBillDate)
    //                                               ->update(['amount'=>($currentRent * $factor)]);
    //     }
    //     }
    //     // we did it
    //     return 1;
        
    // }
    public function updateOtherTenantsRent($propertyId, $loopCount, $factor, $due_on, $monthStart, $yearStart)
    {
        // get current rent rate and set initial bill date
    if($this->getPropertyCurrentTenantCount($propertyId) == 0){
    return 0;
    }
    else{
        $currentRent = ($this->getPropertyPrice($propertyId)/$this->getPropertyCurrentTenantCount($propertyId));
        // get landlordTenants tenants who are active on property
        $activeLandlordTenants = LandlordTenant::where('property_id', $propertyId)
            ->where('active', 1)
            ->select('tenant_id')
            ->get()
            ->toArray();
        // update all of their month_rent_initial fields accordingly
        foreach ($activeLandlordTenants as $tenant){
        $updateActiveTenantsMonthRentInitial = LandlordTenant::where('property_id', $propertyId)
            ->where('tenant_id',$tenant)
            ->where('active', 1)
            ->update(['month_rent_initial' => $currentRent]);
        }
        // get all landlord_tenant_id's and lease start dates for a given property which are active
        $activeLandlordTenantIds = LandlordTenant::where('property_id',$propertyId)
            ->where('active',1)
            ->select('id','agreement_start_date')
            ->get()
            ->toArray();

        // for each of these active landlord_tenant_id's update their monthly_rent amount accordingly
        foreach($activeLandlordTenantIds as $landlordTenantId){
    $nextpaymonth = 0;
     list($monthStart, $dayStart, $yearStart) =  explode('-', $landlordTenantId["agreement_start_date"]);
    $billDate = $monthStart.'-'.$dayStart.'-'.$due_on;

        for($i=1;$i<=$loopCount/$factor;$i++){
        $nextpaymonth = $i * $factor;

        $nextBillDate = date("Y-m-d", strtotime("+".$nextpaymonth." month", strtotime($billDate)));

        $updatingMonth =  landlordTenantMonthlyRent::where('landlord_tenant_id', $landlordTenantId["id"])
                                                  ->where('bill_on', $nextBillDate)
                                                  ->update(['amount'=>($currentRent * $factor)]);
        }
        }
        // we did it
        return 1;
    }
        
    }
    public function getFactorByLandlordRentCollectionFreq($landlordId)
    {
        // 0=monthly, 1=bi-monthly, 2=quarterly, 3=semi-annually
        // Assume lease length is 1 year (12 billable months).
        // Assume Monthly Rent Value/Amount is $700
        $collection_frequency = $this->getCollectionFrequencyLandlord($landlordId);

        switch($collection_frequency){
            case 0:
                // Monthly collect, so the factor is 1. 12 / 1 = 12 payments of (700 * 1) = $700 per payment
                $factor = 1;
                break;
            case 1:
                // Bi-monthly collect, so the factor is 2. 12 / 2 = 6 payments of (700 * 2) = $1400 per payment
                $factor = 2;
                break;
            case 2:
                // Quarterly (Every 3 Months), so the factor is 3. 12 / 3 = 4 payments of ($700 * 3) = $2100 per payment
                $factor = 3;
                break;
            case 3:
                // Semi-Annual (Every 6 months), so the factor is 6. 12 / 6 = 2 payments of ($700 * 6) = $4200 per payment
                $factor = 6;
                break;
        }

        return $factor;
    }
    public function getLandlordRentDueDay($landlordId)
    {
        $landlord_pay_settings = DB::table('forte_landlord_pay_settings_rent')->where('landlord_id', '=', $landlordId)->first();
        // If Landlord Has Payment Settings
        if($landlord_pay_settings !='')
        {
            $due_day = $landlord_pay_settings->rent_due_day_of_month;
        }
        else //He doesn't have payment Settings set
        {
            $due_day = '3';
        }

        return $due_day;
    }
    // Get Collection Frequency of Landlord
    public function getCollectionFrequencyLandlord($landlordId)
    {
        // 0=monthly, 1=bi-monthly, 2=quarterly, 3=semi-annually
        $rent_pay_settings = LandlordPaymentReceiveSetting::where('landlord_id', $landlordId)
            ->first();
        if ($rent_pay_settings != '')
        {
            $rent_collection_frequency = $rent_pay_settings->schedule_frequency;
        }
        else
        {
            // Default to Monthly Collections
            $rent_collection_frequency = 0;
        }
        return $rent_collection_frequency;
    }
    public function getPropertyPrice($propertyId)
    {
    $propertyPrice = Property::where('id', $propertyId)
        ->value('rent_price');

    return $propertyPrice;
    }
    public function getPropertyCurrentTenantCount($propertyId)
    {
     $activeLandlordTenantCount = LandlordTenant::where('property_id', $propertyId)
            ->where('active', 1)
            ->count();
    
    return $activeLandlordTenantCount;
    }
}
