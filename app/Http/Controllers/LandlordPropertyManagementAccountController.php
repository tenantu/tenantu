<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use URL;
use Mail;
use DB;
use Config;
use Validator;
use Auth;
use View;
use Redirect;
use Session;
use Crypt;
use App\Http\Models\LandlordPropertyManagementAccount;
use App\Http\Models\Property;
use App\Http\Models\Landlord;
use App\Http\Models\School;
use App\Http\Models\PropertyPhysical;
use App\Http\Models\PropertyMeta;
use App\Http\Models\ApartmentBuilding;
use App\Traits\ForteRequestTrait;
use App\Traits\ForteVerifyTrait;
use App\Traits\LandlordSubscriptionTrait;


class LandlordPropertyManagementAccountController extends Controller
{
    use ForteRequestTrait, ForteVerifyTrait, LandlordSubscriptionTrait;
    // GET request - Return JSON attributes
    public function getLandlordPropertyManagementAccount($property_management_account_id)
    {
        //$landlord_property_management_account = DB::table('landlord_property_management_');
    }

    // POST request - Return JSON attributes
    public function postLandlordPropertyManagementAccount(Request $request)
    {
        $forteLocationId = $request->locationId;
        $accountName = $request->accountName;

        //removes punctuation from the account name. needed since the view uses the account names as a class name.
        $sanitizedAccountName = preg_replace('#[^a-zA-Z0-9 ]#', '', $accountName);
        $landlordId = Auth::user('landlord')->id;



        if ($this->locIdAlreadyExists($forteLocationId, $landlordId))
        {
            Session::flash('message-error', "Location ID is already in use!");
                  return redirect()->back();
        }

        else if ($this->verifyLandlordLocationId($landlordId, $forteLocationId))
        {

            $encryptedLocId = Crypt::encrypt($forteLocationId);

            DB::table('landlord_property_management_accounts')->insert(
                ['account_name' => $sanitizedAccountName, 'loc_id' => $encryptedLocId, 'landlord_id' => $landlordId]
            );

            Session::flash('message-success', "Property management group successfully added.");
                  return redirect()->back();
        }
        else{
            Session::flash('message-error', "Invalid Forte Location Id.");
                  return redirect()->back();
        }
    }

    private function locIdAlreadyExists($locIdInput, $landlordId)
    {

        $locIds = DB::table('landlord_property_management_accounts')->where('landlord_id', $landlordId)->lists('loc_id');

        foreach ($locIds as $locId)
        {
            $unencryptedId = Crypt::decrypt($locId);
            if ($unencryptedId == $locIdInput)
            {
                return true;
            }
        }
        return false;
    }

    // UPDATE request - Return JSON attributes
    public function updateLandlordPropertyManagementAccount(Request $request)
    {

    }

    // DELETE request - Return Name and/or ID of 'deleted' Record
    public function deleteLandlordPropertyManagementAccount($property_management_account_id)
    {

    }

    // Get the view for Create
    public function createLandlordPropertyManagementAccount(Request $request)
    {
        //return view();
    }

    // From Sidebar, returns view that shows Management Accounts as Tabs, then clicking them will yield properties that belong to those accountss
    public function getAllLandlordPropertyManagementAccountsView(Request $request)
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $profileUrl       = asset('public/uploads/landlords/');
        $noProfileUrl     = asset('public/uploads/noimage/');
        $propertiesInMgmtAccounts = $this->getPropertiesThatBelongToMgmtAccount($loggedLandlordId);
        $apartmentsInMgmtAccounts = $this->getApartmentsThatBelongToMgmtAccount($loggedLandlordId);
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        $propertyManagementAccounts = DB::table('landlord_property_management_accounts')
            ->where('landlord_id', $loggedLandlordId)->get();

        $propertiesWithNoMgmtAccount = DB::table('property_physical')->where('landlord_id', $loggedLandlordId)
	    ->leftJoin('property_meta', 'property_physical.id', '=' , 'property_meta.property_physical_id')
            ->whereNull('landlord_prop_mgmt_id')->whereNull('apartment_building_id')->get();//->where('apartment_building_id', 0)->get();

        $apartmentUnitsWithNoMgmtAccount = DB::table('property_physical')->where('landlord_id', $loggedLandlordId)
          ->leftJoin('property_meta', 'property_physical.id', '=' , 'property_meta.property_physical_id')
          ->where('landlord_prop_mgmt_id', null)->whereNotNull('apartment_building_id')->select('apartment_building_id')->distinct()->get();


        $apartmentBuildingIdArray = collect($apartmentUnitsWithNoMgmtAccount)->pluck('apartment_building_id')->all();

        $apartmentBuildingsWithNoMgmtAccount = ApartmentBuilding::findMany($apartmentBuildingIdArray);

        $newPropertyDetails = $this->getDetailsForNewPropertyForm();

        return view('landlords.mypropertymanagementaccounts', [
            'pageTitle' => 'Active Subscription',
            'pageHeading' => 'Active Subscription',
            'pageMenuName' => 'mysubscription',
            'profile' => $loggedLandlord,
            'profileImage' => $profileImage,
            'propertiesWithNoMgmtAccount' => $propertiesWithNoMgmtAccount,
            'apartmentBuildingsWithNoMgmtAccount' => $apartmentBuildingsWithNoMgmtAccount,
            'propertiesWithMgmtAccount' => $propertiesInMgmtAccounts,
            'apartmentsWithMgmtAccount' => $apartmentsInMgmtAccounts,
            'propertyManagementAccounts'=> $propertyManagementAccounts
            //'amenities' => $newPropertyDetails['amenities']
        ])->with($newPropertyDetails);

    }
    // Gets Update View - Can Update and Delete
    public function editLandlordPropertyManagementAccount($property_management_account_id)
    {
        //return view();
    }

    public function postApartmentToMgmtAccount(Request $request) {
      $loggedLandlordId = Auth::user('landlord')->id;
      $propertyMgmtAccountId = $request->mgmtAccountId;
      $apartmentBuildingIds = $request->apartmentBuildings;
      $apartmentUnitIds = PropertyPhysical::whereIn('apartment_building_id', $apartmentBuildingIds)->with('property_meta')->get()->pluck('id');
      foreach($apartmentUnitIds as $unitId) {
        DB::table('property_meta')->where('property_physical_id', $unitId)->update(['landlord_prop_mgmt_id' => $propertyMgmtAccountId]);
      }
      $numProperties = count(PropertyMeta::where('landlord_prop_mgmt_id', $propertyMgmtAccountId)->get());
      DB::table('landlord_property_management_accounts')->where('id', $propertyMgmtAccountId)
              ->update(['property_count' => $numProperties]);
      $this->updateLandlordSubscriptionPropertyCount($loggedLandlordId);
      Session::flash('message-success', "Properties have been added to the account.");
                return redirect()->back();

    //  dd($apartmentUnits->get());
    }

    public function postPropertyToMgmtAccount(Request $request)
    {
        //return var_dump($request->all());

        $loggedLandlordId = Auth::user('landlord')->id;
        $propertyMgmtAccountId = $request->mgmtAccountId;
        $selectedPropertyIds = $request->property;
        foreach ($selectedPropertyIds as $propertyId)
        {
            DB::table('property_meta')->where('property_physical_id', $propertyId)->update(['landlord_prop_mgmt_id' => $propertyMgmtAccountId]);
        }
        $numProperties = count(PropertyMeta::where('landlord_prop_mgmt_id', $propertyMgmtAccountId)->get());
        DB::table('landlord_property_management_accounts')->where('id', $propertyMgmtAccountId)
                ->update(['property_count' => $numProperties]);
        $this->updateLandlordSubscriptionPropertyCount($loggedLandlordId);

        Session::flash('message-success', "Properties have been added to the account.");
                  return redirect()->back();

        //return var_dump($request->all());

    }

    public function removePropertyFromMgmtAccount(Request $request)
    {

        $loggedLandlordId = Auth::user('landlord')->id;

        $selectedPropertyIds = $request->property;
        $selectedApartmentIds = $request->apartment;

        $propertyId = $selectedPropertyIds[0];
        $propertyMgmtAccountId = DB::table('property_meta')->where('property_physical_id', $propertyId)->value('landlord_prop_mgmt_id');
        //if the edit property button has been clicked, call the correct route to bring up the corresponding edit page
        if (!empty($request->editProp))
        {
            return redirect()->route('property.edit', ['id'=>$request->editProp]);
        }
        if(!empty($selectedPropertyIds)) {
          foreach ($selectedPropertyIds as $propertyId)
          {
              DB::table('property_meta')->where('property_physical_id', $propertyId)->update(['landlord_prop_mgmt_id' => null]);
              DB::table('landlord_property_management_accounts')->where('id', $propertyMgmtAccountId)->decrement('property_count');
          }
        }

        if(!empty($selectedApartmentIds)) {
          foreach($selectedApartmentIds as $apartmentId) {

            $unitIds = DB::table('property_physical')->where('apartment_building_id', $apartmentId)->select('id')->get();//->join('property_meta', 'property_physical.id', '=', 'property_meta.property_physical_id')->update(['property_meta.landlord_prop_mgmt_id' => null]);
            //dd($unitIds);
            foreach($unitIds as $unitId) {
              DB::table('property_meta')->where('property_physical_id', $unitId->id)->update(['landlord_prop_mgmt_id' => null]);
              DB::table('landlord_property_management_accounts')->where('id', $propertyMgmtAccountId)->decrement('property_count');

            }

          }
        }

        $this->updateLandlordSubscriptionPropertyCount($loggedLandlordId);

        Session::flash('message-success', "Properties have been removed from the account.");
                  return redirect()->back();
    }

    //Validates a given forte location id through forte's service, returns true if the location id is valid, returns
    //false otherwise
    //ONCE DONE, THIS METHOD NEEDS THE FOLLOWING PARAMS: $landlordOrgId, $locationId
    //ORG ID HAS BEEN REMOVED FOR TESTING
    private function isValidForteLocationId($locationId)
    {
        //TODO add the actual functionality to this method

        return true;
    }

    ########################HELPER FUNCTIONS START########################################

    //This takes a landlordId as a parameter, and returns a nested array
    //The returned array contains an array for each management account id, and each of those arrays holds the
    //properties that belong to that management account
    private function getPropertiesThatBelongToMgmtAccount($landlordId)
    {
        $mgmtAccountIds = DB::table('landlord_property_management_accounts')->where('landlord_id', $landlordId)
                ->select('id')->get();
        //return $mgmtAccountIds;

        $allPropertiesAndMgmtAccounts = array();
        foreach ($mgmtAccountIds as $mgmtAccountId)
        {
            // return $mgmtAccountId;
            $id = $mgmtAccountId->id;

            //non-apartment properties
            $properties = DB::table('property_meta')->where('landlord_prop_mgmt_id', $id)//->whereNull('apartment_building_id')
		->leftJoin('property_physical', 'property_meta.property_physical_id', '=', 'property_physical.id')
                ->select('property_physical.id', 'property_physical.property_name', 'property_physical.thoroughfare', 'property_physical.locality', 'property_physical.administrative_area', 'property_meta.landlord_prop_mgmt_id')
                ->whereNull('property_physical.apartment_building_id')
                ->get();
            $allPropertiesAndMgmtAccounts[$id] = $properties;
        }
        //dd($allPropertiesAndMgmtAccounts);
        return $allPropertiesAndMgmtAccounts;


    }

    private function getApartmentsThatBelongToMgmtAccount($landlordId) {
      $mgmtAccountIds = DB::table('landlord_property_management_accounts')->where('landlord_id', $landlordId)
              ->select('id')->get();
      $apartmentBuildings = ApartmentBuilding::where('landlord_id', $landlordId)->with('property_physical')->get();
      $apartmentInfoUnit = PropertyPhysical::where('landlord_id', $landlordId)
          ->whereNotNull('apartment_building_id')->distinct('apartment_building_id')->groupBy('apartment_building_id')->get();

      $allApartmentsAndMgmtAccounts = array();


      foreach($apartmentInfoUnit as $unit) {
        $apartmentInfo = ApartmentBuilding::where('id', $unit->apartment_building_id)->with('property_physical')->first();
        if(!empty($apartmentInfo->property_physical)) {
          if (($unit != null) and (!empty($unit->property_meta->landlord_prop_mgmt_id))  ) {
            if(!empty($allApartmentsAndMgmtAccounts[$unit->property_meta->landlord_prop_mgmt_id])) {
              array_push($allApartmentsAndMgmtAccounts[$unit->property_meta->landlord_prop_mgmt_id], $apartmentInfo);
            }
            else {
              $allApartmentsAndMgmtAccounts[$unit->property_meta->landlord_prop_mgmt_id] = array($apartmentInfo);
            }
          }
        }
      }

      return $allApartmentsAndMgmtAccounts;

    }

    //THESE METHODS PROVIDE DATA FOR THE FORM THAT LETS TENANTS ADD NEW PROPERTIES DIRECTLY TO A MGMT ACCOUNT
    public function  getSchoolDetails()
     {
         $school = DB::table('schools')->where('is_active',1)->lists('schoolname', 'id');
         if(!empty($school))
         {
             return $school;
         }else
         {
             return false;
         }
     }

     private function getDetailsForNewPropertyForm(){
        $loggedLandlordId = Auth::user('landlord')->id;
        $school           = $this->getSchoolDetails();
        $properties       = $this->getProperties( $loggedLandlordId );
        $amenities        = $this->getAmenityDetails();

        $compactedDetails = compact('school', 'properties', 'amenities');

        return $compactedDetails;
    }

    private function getAmenityDetails()
     {
         $amenity = array();
         $amenity  = DB::table('amenities')->where('status',1)->orderBy('amenity_name', 'asc')->lists('amenity_name', 'id');
         return $amenity;
     }





}
