<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\PropertyPhysical;
use App\Http\Models\PropertyMeta;
use App\Http\Models\School;
use App\Http\Models\ApplicationQuestion;
use App\Http\Models\ApplicationQuestionTenantResponse;
use App\Http\Models\ApplicationSubmission;
use App\Http\Models\ApplicationGroup;
use App\Http\Models\ApplicationGroupTenant;
use App\Http\Models\Message;
use App\Http\Models\MessageThread;
use App\Http\Models\Thread;
use Mail;
use View;
use Crypt;
use Config;
use Redirect;
use Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use HTML;
use Auth;
use Response;
use Illuminate\Auth\Guard;
use Input;

class ApplicationController extends Controller {

  //This function is used to display the tenant's current application
  public function tenantIndex() {
    $tenantId = Auth::User('tenant')->id;
    $tenantProfile 	= $this->getTenantProfile($tenantId);
		$imageUrl       = asset('public/uploads/tenants/');

    $questionIds = ApplicationQuestion::select('id')->lists('id')->toArray();
    $tenantResponses = array();
    foreach($questionIds as $questionId) {
      $tenantResponses[$questionId] = null;
    }
    $tenantResponseModels = ApplicationQuestionTenantResponse::where('tenant_id', $tenantId)->get();
    foreach($tenantResponseModels as $responseModel) {
      $tenantResponses[$responseModel->question_id] = $responseModel->response;
    }

    $applicationQuestionCategories = ApplicationQuestion::distinct('category')->lists('category')->toArray();
    $applicationQuestionsCategorized = array();
    foreach($applicationQuestionCategories as $category) {
      $questions = ApplicationQuestion::where('category', $category)->get()->toArray();
      $applicationQuestionsCategorized[$category] = $questions;
    }
    //dd($applicationQuestionsCategorized);

    return view('tenantapplications.myApplication', [
      'pageMenuName' => 'myApplication',
      'pageTitle'   	=> 'myApplication',
      'pageHeading' => 'myApplication',
      'applicationQuestionCategories' => $applicationQuestionCategories,
      'categorizedQuestions' => $applicationQuestionsCategorized,
      'tenantResponses' => $tenantResponses,
      'profile' 		=> $tenantProfile,
      'imageUrl' 		=> $imageUrl
    ]);
  }
 
  public function viewTenantApplication($encryptedTenantId){
	$landlordId = Auth::User('landlord')->id;
        $landlordProfile      = $this->getLandlordProfile($landlordId);
        $imageUrl           = asset('public/uploads/landlords/');
        $noImageUrl         = asset('public/uploads/noimage/');

        if($landlordProfile->image!='')
        {
            $profileImageUrl=$imageUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImageUrl=$noImageUrl."/landlord-blank.png";
        }

        $landlord_subscription = $this->getLandlordSubscriptionPlan($landlordId);
        $subscription_plan_id = json_decode($landlord_subscription)->subscription_plan_id;
        $subscriptionPlan = $this->getSubscriptionPlan($subscription_plan_id);
        $profileImage =  HTML::getLandlordProfileImage();

    $tenantId = Crypt::decrypt($encryptedTenantId);
    $tenantName = $this->getTenantNameFromId($tenantId);
    $tenantProfile = $this->getTenantProfile($tenantId);
    $questionIds = ApplicationQuestion::select('id')->lists('id')->toArray();
    $tenantResponses = array();
    foreach($questionIds as $questionId) {
      $tenantResponses[$questionId] = null;
    }
    $tenantResponseModels = ApplicationQuestionTenantResponse::where('tenant_id', $tenantId)->get();
    foreach($tenantResponseModels as $responseModel) {
      $tenantResponses[$responseModel->question_id] = $responseModel->response;
    }

    $applicationQuestionCategories = ApplicationQuestion::distinct('category')->lists('category')->toArray();
    $applicationQuestionsCategorized = array();
    foreach($applicationQuestionCategories as $category) {
      $questions = ApplicationQuestion::where('category', $category)->get()->toArray();
      $applicationQuestionsCategorized[$category] = $questions;
    }
    //dd($applicationQuestionsCategorized);

    return view('landlordapplications.viewApplication', [
      'pageMenuName' => 'viewApplication',
      'pageTitle'       => 'viewApplication',
      'pageHeading' => 'viewApplication',
      'applicationQuestionCategories' => $applicationQuestionCategories,
      'categorizedQuestions' => $applicationQuestionsCategorized,
      'tenantResponses' => $tenantResponses,
      'profile'                 => $landlordProfile,
      'tenantProfile'		=> $tenantProfile,
      'subscriptionPlan' => json_decode($subscriptionPlan),
      'profileImage'	 => $profileImage,
      'imageUrl'                => $profileImageUrl,
      'tenantName'		=> $tenantName
    ]);
  }

  //this method is used to save tenant responses to application questions
  public function updateTenantApplication(Request $request) {
    $tenantId = Auth::User('tenant')->id;
    $tenantResponses = $request->except('_token');

    foreach($tenantResponses as $questionId => $response) {
      if(!empty($response)) {
        $existingResponseModel = ApplicationQuestionTenantResponse::where('tenant_id', $tenantId)->where('question_id', $questionId)->first();
        if (!empty($existingResponseModel)) {
          $existingResponseModel->response = $response;
          $existingResponseModel->save();
        }
        else {
          $responseModel = new ApplicationQuestionTenantResponse;
          $responseModel->tenant_id = $tenantId;
          $responseModel->question_id = $questionId;
          $responseModel->response = $response;
          $responseModel->save();
        }
      }
    }
    return Redirect()->back()->with('message-success', 'Your application has been successfully updated!');
  }

  //this function is used to display the tenant's submitted applications
  public function tenantSubmittedApplications() {
    $tenant = Auth::User('tenant');
    $tenantId = $tenant->id;
    $tenantProfile 	= $this->getTenantProfile($tenantId);
    $imageUrl       = asset('public/uploads/tenants/');
    $applicationGroups = $tenant->applicationGroups();
    $applicationGroups = $applicationGroups->with('members')->with('applicationSubmission')->get();
    foreach($applicationGroups as $group){
      if(!$group->applicationSubmission->isEmpty()) {
        $submissions = $group->applicationSubmission;
        foreach($submissions as $submission){
          $propertyId = $submission->property_id;
          $property = PropertyPhysical::where('id', $propertyId)->first();
          //$submission = $submission->put('propertyPhysical', $property);
          $submission->propertyPhysical = $property;
        }
      }
    }
    $tenantSoloApplications = $tenant->applicationSubmissions()->get();
    foreach($tenantSoloApplications as $application) {
      $propertyId = $application->property_id;
      $property = PropertyPhysical::where('id', $propertyId)->first();
      $application->propertyPhysical = $property;
    }
    foreach($applicationGroups as $key => $group) {
      if($group->pivot->status != 2){
        $applicationGroups->forget($key);
      }
    }

    $applicationGroups = $applicationGroups->sortBy(function($group) {
      return $group->created_at;
    });

    return view('tenantapplications.submittedApplications', [
      'pageMenuName' => 'myApplication',
      'pageTitle'   	=> 'My Application',
      'pageHeading' => 'My Application',
      'profile' 		=> $tenantProfile,
      'imageUrl' 		=> $imageUrl,
      'submittedApplications' => $applicationGroups,
      'tenantSoloApplications' => $tenantSoloApplications
    ]);

  }
  
  //this function is used to display the landlord's submitted applications
  public function landlordSubmittedApplications(Request $request) {
	$landlordId = Auth::User('landlord')->id;
    	$landlordProfile      = $this->getLandlordProfile($landlordId);
        $imageUrl           = asset('public/uploads/landlords/');
        $noImageUrl         = asset('public/uploads/noimage/');

        if($landlordProfile->image!='')
        {
            $profileImageUrl=$imageUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImageUrl=$noImageUrl."/landlord-blank.png";
        }
	
	$landlord_subscription = $this->getLandlordSubscriptionPlan($landlordId);
        $subscription_plan_id = json_decode($landlord_subscription)->subscription_plan_id;
        $subscriptionPlan = $this->getSubscriptionPlan($subscription_plan_id);
	$profileImage =  HTML::getLandlordProfileImage();

	$landlordsProperties = PropertyPhysical::where('landlord_id', $landlordId)->with('applicationSubmissions')->get();
	foreach($landlordsProperties as $key => $property){
	$groupId = collect();
	if($property->applicationSubmissions()->get()->isEmpty()){
	$landlordsProperties->forget($key);
	}
	else{
	foreach($property->applicationSubmissions as $submission){
	$groupId = $submission['application_group_id'];
	if($this->getApplicationGroupFromId($groupId)->count() < 1){
	$submission->group_name = null;
	$submission->tenant_name = $this->getTenantNameFromId($submission->tenant_id);
	}
	else{
	$submission->group_name = $this->getApplicationGroupFromId($groupId)[0]['group_name'];
	$onSiteMembers = $this->getApplicationOnSiteGroupMembersFromId($groupId);
	if(count($onSiteMembers) > 1){
	foreach($onSiteMembers as $key => $member){
	$onSiteMembers[$key]['tenant_name'] = $this->getTenantNameFromId($member['tenant_id']);
	}
	}
	$offSiteMembers = $this->getApplicationOffSiteGroupMembersFromId($groupId);
	$submission->offSiteMembers = $offSiteMembers;
        $submission->onSiteMembers = $onSiteMembers;
	}
	}
	}
	}	
	return view('landlordapplications.submittedApplications', [
	  'pageMenuName' => 'manageSubmittedApplications',
	  'pageTitle'	 => 'Manage Received Applications',
	  'pageHeading'  => 'Manage Received Applications',
	  'profile'	 => $landlordProfile,
	  'subscriptionPlan' => json_decode($subscriptionPlan),
	  'profileImage' => $profileImage,
	  'imageUrl'	 => $profileImageUrl,
	  'applications' => $landlordsProperties,
	]);
  }

  //expects the application group id in the request
  public function postTenantApplication(Request $request) {
    $tenantId = Auth::User('tenant')->id;
    $propertyId = $request->propertyId;
    //dd(empty($applicationSubmission = ApplicationSubmission::where('tenant_id', $tenantId)->where('property_id', $propertyId)->where('status', '>', 0)->first()));
    $applicationGroupId = $request->applicationGroup;
    $propertyId = $request->propertyId;

    if($applicationSubmission = ApplicationSubmission::where('tenant_id', $tenantId)->where('property_id', $propertyId)->where('status', '>', 0)->first()) {
      if($applicationGroupId == "false") {
        return redirect()->back()->with('message-error', 'You already have an application in progress for this property!');
      }
    }
    if($applicationSubmission = ApplicationSubmission::where('application_group_id', $applicationGroupId)->where('property_id', $propertyId)->where('status', '>', 0)->first()) {
      return redirect()->back()->with('message-error', 'This application group already has a pending application!');
    }
    else {
      $applicationSubmission = new ApplicationSubmission;
      $applicationSubmission->property_id = $propertyId;
      $applicationSubmission->status = 1;
      if($applicationGroupId == "false"){
        $applicationSubmission->tenant_id = $tenantId;
      }
      else{
        $applicationSubmission->application_group_id = $applicationGroupId;
        $applicationSubmission->tenant_id = null;
      }
      $applicationSubmission->save();
      return redirect()->back()->with('message-success', 'Application successfully submitted!');
    }
  }  

  public function applicationGroups() {
    $tenant = Auth::User('tenant');
    $tenantId = $tenant->id;
    $tenantProfile 	= $this->getTenantProfile($tenantId);
    $imageUrl       = asset('public/uploads/tenants/');

    $isMemberOfApplicationGroups = $tenant->applicationGroups()->exists();
    $isOwnerOfApplicationGroups = $tenant->ownedApplicationGroups()->exists();
    $applicationGroupsMember = collect();
    $applicationGroupsOwner = collect();
    if($isMemberOfApplicationGroups) {
      $applicationGroupsMember = $tenant->applicationGroups();
      $applicationGroupsMember = $applicationGroupsMember->with('members')->get();
      foreach($applicationGroupsMember as $key => $group){
        if($group->pivot->status != 2) {
          $applicationGroupsMember->forget($key);
        }
      }
    }
    if($isOwnerOfApplicationGroups) {
      $applicationGroupsOwner = $tenant->ownedApplicationGroups()->get();
    }
    $pendingApplications = $tenant->applicationGroups();
    $pendingApplications = $pendingApplications->with('members')->with('creator')->where('tenant_id', $tenantId)->where('status', 1)->get();
    $pendingApplications = $pendingApplications->reject(function($application) use ($tenantId){
      return $application->creator->id == $tenantId;
    });

    return view('tenantapplications.applicationGroups', [
      'pageMenuName' => 'applicationGroups',
      'pageTitle'   	=> 'My Application',
      'pageHeading' => 'applicationGroups',
      'applicationGroupsMember' => $applicationGroupsMember,
      'pendingApplications' => $pendingApplications,
      'profile' 		=> $tenantProfile,
      'tenantId'    => $tenantId,
      'imageUrl' 		=> $imageUrl,
    ]);
  }

  public function postNewApplicationGroup(Request $request) {
    $tenantId = Auth::User('tenant')->id;
    $tenantEmailArray = $request->tenant_email;
    $groupName = $request->group_name;
    $tenantsNotOnTenantU = array();

    $applicationGroup = new ApplicationGroup;
    $applicationGroup->group_name = $groupName;
    $applicationGroup->creator_id = $tenantId;
    $applicationGroup->save();

    $applicationGroupTenant = new ApplicationGroupTenant;
    $applicationGroupTenant->application_group_id = $applicationGroup->id;
    $applicationGroupTenant->tenant_id = $tenantId;
    $applicationGroupTenant->status = 2;
    $applicationGroupTenant->save();
    foreach($tenantEmailArray as $tenantEmail) {
      $tenantModel = Tenant::where('email', $tenantEmail)->first();
      $applicationGroupTenant = new ApplicationGroupTenant;
      $applicationGroupTenant->application_group_id = $applicationGroup->id;
      if(!$tenantModel) {
        array_push($tenantsNotOnTenantU, $tenantEmail);
        $applicationGroupTenant->tenant_email = $tenantEmail;
      }
      else{
        $applicationGroupTenant->tenant_id = $tenantModel->id;
      }
      $applicationGroupTenant->save();
    }
    if(!empty($tenantsNotOnTenantU)) {
      $warningMessageText = "Your application group has been saved! However, the following email addresses you added do not belong to an account on TenantU: ";
      $numOfElements = count($tenantsNotOnTenantU);
      $i = 0;
      foreach($tenantsNotOnTenantU as $key => $emailAddress) {
        if(++$i == $numOfElements) {
          $warningMessageText .= ' ' . $emailAddress . '.';
        }
        else{
          $warningMessageText .= ' ' . $emailAddress . ", ";
        }
      }
      $warningMessageText .= ' When applying to properties, landlords will still be able to see these email addresses as applicants.';
      $warningMessageText .= ' However, groups consiting of only TenantU users have a higher change of being accepted.';

      return redirect()->back()->with('warningMessage', $warningMessageText);
    }
    else{
      return redirect()->back()->with('message-success', 'Application group successfully saved! You may now submit property applications as a group.');
    }

  }

  public function getApplicationGroupFromId($groupId){
	$groupName = ApplicationGroup::where('id', $groupId)
			->select('group_name')
			->get();
	return $groupName;
  }
  
  public function getApplicationOnSiteGroupMembersFromId($groupId){
	$onSiteGroup = ApplicationGroupTenant::where('application_group_id',$groupId)
					->whereNotNull('tenant_id')
					->select('tenant_id')
					->get()
					->toArray();
	return $onSiteGroup;
  }

  public function getApplicationOffSiteGroupMembersFromId($groupId){
	$offSiteGroup = ApplicationGroupTenant::where('application_group_id', $groupId)
					->whereNull('tenant_id')
					->select('tenant_email')
					->get()
					->toArray();
	return $offSiteGroup;
  }
  
  public function changeApplicationStatusToApproved($encryptedPropertyId, $encryptedGroupId,  $encryptedApplicationId)
  {      
      $propertyId = Crypt::decrypt($encryptedPropertyId);
      $groupId = Crypt::decrypt($encryptedGroupId);
      $applicationId = Crypt::decrypt($encryptedApplicationId);

      ApplicationSubmission::where('id', $applicationId)
          ->update(['status' => 2]);
      $tenantIds = $this->getApplicationOnSiteGroupMembersFromId($groupId);
      $landlordId = Auth::User('landlord')->id;
      foreach($tenantIds as $tenantId){
      $this->postApprovalMessage($tenantId['tenant_id'], $landlordId, $propertyId);
      }
      
      return redirect()->route('landlords.getContact');
  }
  public function changeApplicationStatusToDenied($encryptedGroupId,  $encryptedApplicationId)
  {
      $groupId = Crypt::decrypt($encryptedGroupId);
      $applicationId = Crypt::decrypt($encryptedApplicationId);
      ApplicationSubmission::where('id', $applicationId)
          ->update(['status' => 0]);
      $tenantIds = $this->getApplicationOnSiteGroupMembersFromId($groupId);
      $landLordId = Auth::User('landlord')->id;
      foreach($tenantIds as $tenantId){
	    $tenants   = Tenant::find( $tenantId );
            $landlords = Landlord::find( $landLordId );
            $landLordName = $landlords['firstname'].' '.$landlords['lastname'];
            $tenantName   = $tenants[0]->firstname.' '.$tenants[0]->lastname;
            $tenantEmail = $tenants[0]->email;
	    $subject = "TenantU - Your application has been denied!";
            Mail::send('emails.denyFromLandlord', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail,$subject) {
               $message->to($tenantEmail, $tenantName)
                            ->subject($subject);
                });
      }
      return redirect()->back();
  }

  
    
    
    //Automate thread starting when approve application
    //this function is used for the  saving the message to message and message thread tables
    public function postApprovalMessage($tenantId, $landlordId, $propertyId)
    {
            $input['message']           = "You're application has been approved. Chat here to decide a time to sign your lease!";;
            $input['tenant_id']         = $tenantId;
            $input['sender']            = "L";
            $input['landlord_id']       = $landlordId;

            $inputArr['thread_id']      = 4;
            $inputArr['property_id']    = $propertyId;
            $inputArr['landlord_id']    = $landlordId;
            $inputArr['tenant_id']      = $tenantId;
            //dd($inputArr);exit;
            $tenants   = Tenant::find( $tenantId );
            $landlords = Landlord::find( $landlordId );
            $landLordName = $landlords->firstname.' '.$landlords->lastname;
            $tenantName   = $tenants->firstname.' '.$tenants->lastname;
            $tenantEmail = $tenants->email;
            $messageThread = MessageThread::create($inputArr);
                                                //dd($MessageThread);
            if(isset($messageThread))
            {
                $message = new Message;
                $message->message_thread_id = $messageThread->id;
                $message->message           = $input['message'];
                $message->landlord_id       = $input['landlord_id'];
                $message->tenant_id         = $input['tenant_id'];
                $message->sender            = $input['sender'];

                $message->save();
                $messageFlag = '1';
                $subject = "TenantU - Your application has been approved!";
                Mail::send('emails.applicationthreadFromLandlord', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail,$subject) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject($subject);
                });
            }
            $encryptedMessageThreadId = Crypt::encrypt($messageThread->id);
    }



  public function deleteApplicationGroup(Request $request) {
    $applicationGroupId = $request->applicationGroupId;
    ApplicationGroup::destroy($applicationGroupId);
    ApplicationGroupTenant::where('application_group_id', $applicationGroupId)->delete();
    return redirect()->back()->with('message-success', 'Application group deleted!');
  }

  public function acceptApplicationGroupInvitation($tenantId, $groupId) {
    if($tenantId == Auth::User('tenant')->id){
      $groupTenant = ApplicationGroupTenant::where('tenant_id', $tenantId)->where('application_group_id', $groupId)->first();
      $groupTenant->status = 2;
      $groupTenant->save();
      return redirect()->back()->with('message-success', 'Invitation accepted!');
    }
    else{
      return redirect()->back();
    }
  }

  public function rejectApplicationGroupInvitation($tenantId, $groupId) {
    if($tenantId == Auth::User('tenant')->id){
      $groupTenant = ApplicationGroupTenant::where('tenant_id', $tenantId)->where('application_group_id', $groupId)->first();
      $groupTenant->status = 0;
      $groupTenant->save();
      return redirect()->back()->with('warningMessage', 'Invitation rejected!');
    }
    else{
      return redirect()->back();
    }
  }

}
