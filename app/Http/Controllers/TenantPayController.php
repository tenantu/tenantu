<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\PaymentSetting;
use App\Http\Models\Setting;
use App\Http\Models\TenantPaymentSetting;
use App\Http\Models\LandlordPaymentReceiveSetting;
use App\Http\Models\ForteRentTransaction;
use DB;
use DateTime;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use Crypt;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class TenantPayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // This will run WHEN PAY RENT BUTTON IS PRESSED, can only be pressed when ready_pay = 0 (Not ready to pay) (Can make a check to see if an Active Schedule exists for the Tenant Under the Current Landlord Location ID)
    public function postRent($uniqueid)
    {
      // Ready pay and auto in current Tenant Pay Settings is definitely 0 (Not ready) at this point...keep going
	   $loggedTenant     = Auth::user('tenant');
	   $loggedTenantId   = $loggedTenant->id;
    // We need this to check whether to post the transaction or to make ready for this transaction to be posted on the Due Date
    $check_due_date = DB::table('landlord_tenant_monthlyrents')->where('unique_id', $uniqueid)->value('bill_on');
    $todays_date = Carbon::now();
    // Now let's check, if todays date is before today's date, run setReadyPay, and then redirect back to the payment view
    if(strtotime($todays_date) < strtotime($check_due_date))
    {
      $set_ready = $this->setReadyPay($loggedTenantId);
      $setMonthlyPaymentPending = $this->setMonthlyRentPaymentPending($uniqueid);
      return back()->with('message-success', 'Rent Transaction Scheduled!');
    }
    else
    {
	      //$base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
	      // Landlord Required Fields
    	  $base_url   = env('FORTE_BASE_URL');
        $api_access_id     = env('FORTE_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_API_SECURE_KEY');
        $organization_id   = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
        $location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
    	  $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
    	  $tenant_customer_id = $this->getTenantCustomerId($loggedTenantId);

    	  $tenant_address_id = $this->getTenantAddressId($loggedTenantId);

    	  $tenant_auto_pay = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('auto_pay');

    	  $addr_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $tenant_customer_id . '/addresses/' . $tenant_address_id;

    	  $tenant_billing_address = $this->forteGet($addr_endpoint, $auth_token, $organization_id);
    	  $addr_response = curl_exec($tenant_billing_address);
    	  $addr_resp_info = curl_getinfo($tenant_billing_address);
    	  curl_close($tenant_billing_address);

    	  // This associative array is used for the billing_address param in the transaction param array
    	  $billing_address_data = json_decode($addr_response, true);

    	  // Today's Date Info
    	  $get_this_month = date('M');

    	  // Misc Relationship info relevant to Transaction OR Schedule
    	  $current_relationship = $this->getCurrentTenantLandlordInfo($loggedTenantId);

    	  //The below line is the original monthly_rent_amount
    	  //$monthly_rent_amount  = $current_relationship->month_rent_initial;

        $monthly_rent_amount = $this->calculate_rent_amount($loggedTenantId, $uniqueid);
    	  $day_of_month_due     = $current_relationship->due_on;
    	  $property_name        = $this->getTenantCurrentProperty($loggedTenantId);

    	  $reference_id = $get_this_month . '-Rent-' . $property_name;
    	  // Single Transaction only params
    	  $service_fee_amount = 1.25;
    	  $final_rent_amount = $monthly_rent_amount + $service_fee_amount;
    	  // Quarterly_amount = monthly rent amount * 3, Semi-Annual Amount = monthly rent amount * 6, etc

    	  // Rent Pay Params for Manual Transactions
    	  //paymethod_token not needed if customer already has a paymethod token defined in their object
    	  $rent_transaction_params = array(
    	    'action' => 'sale',
    	    'customer_token' => $tenant_customer_id,
    	    'authorization_amount' => $final_rent_amount,
    	    'billing_address'     => $billing_address_data,
    	    //'service_fee_amount' => 1.25,
    	    'reference_id'        => $reference_id

    	  );

    	  // Doing the transaction!!!!
    	  $manual_rent_pay = $this->manual_rent_pay_transaction($rent_transaction_params);

    	  // Post Responses
    	  $post_trans_response = json_decode(json_encode($manual_rent_pay['data']))->response;
    	  $post_trans_data = json_decode(json_encode($manual_rent_pay['data']));

    	  $trans_id = $post_trans_data->transaction_id;

    	  // Will Move this to it's own method
    	  $get_trans_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions/' . $trans_id;


        $current_transaction_due_date = DB::table('landlord_tenant_monthlyrents')
                                            ->where('unique_id',$uniqueid)
                                            ->value('bill_on');

		if ($uniqueid == "undefined")
		{
			$current_transaction_due_date = '';
		}

    	  $post_transaction_db_params = array(
    	     'transaction_token'     => Crypt::encrypt($post_trans_data->transaction_id),
    	     'authorized_amount'    => $post_trans_data->authorization_amount,
    	     'response_code'         => $post_trans_response->response_code,
            'monthly_rent_unique_id' => $uniqueid,
            'date_due'              => $current_transaction_due_date
    	    );

    	  // POST THE PROPERTIES RETRIEVED FROM POSTED TRANSACTION
    	  $this->postRentTransactionDB($post_transaction_db_params);

          if ($post_trans_response->response_code != 'A01')
          {
            return back()->with('message-error', $post_trans_response->response_desc);
          }
          // Check today's date, is it before the due_date?
          /*elseif(Carbon::today() < $current_transaction_due_date)
          {
            return back()->with('message-success', "You're early!!");
          }
          elseif(Carbon::today() > $current_transaction_due_date)
          {
            return back()->with('message-error', "You're late!!");
          }*/
          else
          {
            // Update the status of the specific monthly rent payment so that user can't click it until a new response from Forte updates the transaction status
            $update_monthly_rent_status = DB::table('landlord_tenant_monthlyrents')
                                            ->where('unique_id', $uniqueid)
                                            ->update(['status'=>1]);
            return back()->with('message-success', 'Rent Transaction Made!');
          }
        }
      // If Ready Pay in forte_tenant_pay_settings = 1,
    }
    // Tenants without automated rent pay, who press the pay button on the due date

    //This method will take a logged tenant's Id and return the rent amount to be paid,
    //after taking into consideration the current date, when the rent was due, and the due date
    // We need to calculate based on the monthly rent payment, not the relationship.
    private function calculate_rent_amount($loggedTenantId,$uniqueId)
    {
        //current_relationship is an entry from the landlord_tenants table
        $current_relationship = $this->getCurrentTenantLandlordInfo($loggedTenantId);
        $currentMonthlyRentPay = $this->getMonthlyRentAmountByUniqueId($uniqueId);
        $currentMonthlyRentPayDueDate = $this->getMonthlyRentPayDueDate($uniqueId);
        // exit($currentMonthlyRentPayDueDate);
        if($current_relationship != null)
        {

        $landlordId = $current_relationship->landlord_id;
        $initialRentAmount = $currentMonthlyRentPay;
        $landlordRentSettings = DB::table('forte_landlord_pay_settings_rent')
                                    ->where('landlord_id', '=', $landlordId)
                                    ->first();

        $nowDate = Carbon::now();
        $currentDate = $currentMonthlyRentPayDueDate;
        // $firstOfTheMonth = new Carbon('first day of this month');
        //$firstOfTheMonth = Carbon::now()->startOfMonth;
        //$rentDueDayOfTheMonth = $landlordRentSettings->rent_due_day_of_month;
        //$rentDueDate = $firstOfTheMonth->addDays($rentDueDayOfTheMonth);
        //return var_dump($landlordRentSettings);

        $numDaysInGracePeriod = (int) $landlordRentSettings->late_grace_period_day;
        // echo $numDaysInGracePeriod;
        // exit('fuck me');
        $endOfGracePeriod = date('Y-m-d', strtotime($currentDate. ' + ' . $numDaysInGracePeriod . ' days'));
        //$endOfGracePeriod = $currentDate->addDays($numDaysInGracePeriod);
        //print_r($firstOfTheMonth . "<br>");
        if ($nowDate > $endOfGracePeriod)
        {
          
            $late_fee_percentage = ($landlordRentSettings->late_fee_percent) / 100;
            $late_fee = $initialRentAmount * $late_fee_percentage;
            $total_rent_amount = $initialRentAmount + $late_fee;
            return $total_rent_amount;
        }
        return $initialRentAmount;
      }
      else
      {
        return 0;
      }
    }


    public function manual_rent_pay_transaction($rent_pay_params)
    {
      $loggedTenant       = Auth::user('tenant');
      $loggedTenantId     = $loggedTenant->id;
      $current_property_location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
      $base_url           = env('FORTE_BASE_URL');
      // $organization_id    = Config::get('constants.FORTE_TENANTU_ORG_ID');
      $organization_id    = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
      $location_id        = $current_property_location_id;
      // $api_access_id      = Config::get('constants.FORTE_API_ACCESS_ID');
      // $api_secure_key     = Config::get('constants.FORTE_API_SECURE_KEY');
      $api_access_id      = env('FORTE_API_ACCESS_ID');
      $api_secure_key     = env('FORTE_API_SECURE_KEY');
      $auth_token         = base64_encode($api_access_id . ':' . $api_secure_key);


      /*$tenant_customer_id = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('customer_id');

      $tenant_payment_id = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('payment_id');

      $tenant_address_id = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('addr_id');
      */
      $tenant_auto_pay = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('auto_pay');
      $rent_transaction_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions';
      // This posts the rent payment schedule to Forte
      $transaction_rent_pay  = $this->fortePost($rent_transaction_endpoint, $rent_pay_params, $auth_token, $organization_id);

      $transaction_pay_response = curl_exec($transaction_rent_pay);
      $transaction_pay_info     = curl_getinfo($transaction_rent_pay);
      curl_close($transaction_rent_pay);
      $transaction_data = json_decode($transaction_pay_response);
      $create_http_success = (($transaction_pay_info['http_code'] != 201) ? 0:1);

      $createOutput = array(
        'create_http_success'   => $create_http_success,
        'data'                  => $transaction_data
      );

      return $createOutput;
    }

    // If a Tenant presses the rent button before the due date, let's set them as a Ready-To-Pay Tenant
    public function setReadyPay($tenantId)
    {
        //$today_date = Carbon::now();
        // Set Ready Pay to true in the database for whichever Tenant pressed Pay Rent before Due Date
        $setReadyPay = DB::table('forte_tenant_pay_settings')->where('tenant_id', $tenantId)
                          ->update(['ready_pay'=>1]);
    }
    // Set specific monthly rent payment to status = 1, which is pending
    public function setMonthlyRentPaymentPending($uniqueid)
    {
      DB::table('landlord_tenant_monthlyrents')->where('unique_id', $uniqueid)
                ->update(['status'=>1]);
    }

    public function getRentTransactionsHistory()
    {
        $loggedTenantId = Auth::user('tenant')->id;
        $tenantProfile = $this->getTenantProfile($loggedTenantId);
        $current_landlord_org_id = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
        $current_property_location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
        
        $organization_id = $current_landlord_org_id;
        // $location_id = $current_landlord_property_id;
        $tenant_customer_id = $this->getTenantCustomerId($loggedTenantId);
        //$current_landlord_id = $this->getTenantCurrentLandlordId($loggedTenantId);
        $current_landlord_relationship_id = $this->getTenantCurrentLeaseId($loggedTenantId);
        $imageUrl   = asset('public/uploads/tenants/');
        $tenantAssociatedWithLandlord = $this->isTenantAssociatedWithLandlord($loggedTenantId);
        // FORTE STUFF WE NEED
        $base_url          = env('FORTE_BASE_URL');     //production: http://api.forte.net/v3
        // Landlord Required Fields
        //$organization_id   = Config::get('constants.FORTE_TENANTU_ORG_ID');
        $location_id       = $current_property_location_id;
        $api_access_id     = env('FORTE_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_API_SECURE_KEY');
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);




        if($current_landlord_relationship_id == '')
        {
            Session::flash('warningMessage', "You are not currently with any landlord, no payments to be seen");
            return redirect()->route('tenants.dashboard');
        }
        else
        {
            // Lets get all the transaction_id's first
            /*$transaction_ids = ForteRentTransaction::where('landlord_tenant_id', $current_landlord_relationship_id)
                                                    ->select('transaction_token')
                                                    ->get();*/
            $transaction_ids = DB::table('forte_rent_transactions')->where('landlord_tenant_id', $current_landlord_relationship_id)
                                                                    ->select('transaction_token')
                                                                    ->get();

            foreach($transaction_ids as $key => $trans_id)
            {
                $trans_token = $trans_id->transaction_token;
                $transaction_id = Crypt::decrypt($trans_token);
                // Lets update the transactions accordingly
                $get_trans_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $tenant_customer_id .'/transactions/' . $transaction_id;

                // Now we wanna get the transaction and look for some properties
                /*$getSameTransaction = $this->forteGet($get_trans_endpoint, $auth_token, $organization_id);
                $trans_response = curl_exec($getSameTransaction);
                $info = curl_getinfo($getSameTransaction);
                curl_close($getSameTransaction);
                $trans_get_data = json_decode($trans_response);

                $new_status = $trans_get_data->status;
                $date_received = $trans_get_data->received_date;
                $date_funded_exists = property_exists($trans_get_data, 'origination_date');
                $date_funded = ($date_funded_exists != TRUE ? '':$trans_get_data->origination_date);
                */
                $check_receive_date_exist = DB::table('forte_rent_transactions')
                                            ->where('transaction_token', $trans_token)
                                            ->where('response_code', 'A01')
                                            ->value('date_received');
                $check_funded_date_exist = DB::table('forte_rent_transactions')
                                            ->where('transaction_token', $trans_token)
                                            ->where('response_code', 'A01')
                                            ->value('date_funded');

                //
                //
                if($check_funded_date_exist != '')
                {
                    continue;
                }
                else
                {
                    $getSameTransaction = $this->forteGet($get_trans_endpoint, $auth_token, $organization_id);
                    $trans_response = curl_exec($getSameTransaction);
                    $info = curl_getinfo($getSameTransaction);
                    curl_close($getSameTransaction);
                    $trans_get_data = json_decode($trans_response);

                    $new_status = property_exists($trans_get_data, 'status') ? $trans_get_data->status : null;
                    $date_received = property_exists($trans_get_data, 'received_date') ? $trans_get_data->received_date : null;
                    $date_funded_exists = property_exists($trans_get_data, 'origination_date');
                    $date_funded = ($date_funded_exists != TRUE ? '':$trans_get_data->origination_date);

                    if($check_receive_date_exist == '')
                    {
                        $update_transaction_now = DB::table('forte_rent_transactions')
                                                ->where('transaction_token', $trans_token)
                                                ->update(['status' => $new_status, 'date_received' => $date_received]);
                    }
                    if($check_funded_date_exist != '' || $date_funded_exists != TRUE)
                    {
                        continue;

                    }
                    else // No need to do anything for date received, it's already there
                    {

                        $update_transaction_now = DB::table('forte_rent_transactions')
                                                ->where('transaction_token', $trans_token)
                                                ->update(['date_funded' => $date_funded]);
                    }
                // NOW WE HAVE THE UPDATED FORTE STATUS STUFF
                }

            }


            // Get the transactions via Eloquent to paginate
            $my_rent_transactions = ForteRentTransaction::where('landlord_tenant_id', $current_landlord_relationship_id)->paginate(10);

                return view('tenants.myRentPaymentHistory',[
                'pageTitle'     => 'My Payment History for this Lease',
                'pageHeading'   => 'My Rent Pay History',
                'pageMenuName'  => 'myRentPaymentHistory',
                'profile'       => $tenantProfile,
                'imageUrl'      => $imageUrl,
                'rent_payments' => $my_rent_transactions,
                'isTenantAssociatedWithLandlord' => $tenantAssociatedWithLandlord
            ]);
        }
    }
    // Post Forte Rent Transaction Info into the Database (table: forte_rent_transactions)
    public function postRentTransactionDB($rent_transaction_params)
    {
        $loggedTenantId     = Auth::user('tenant')->id;
        $currentTenantLeaseRelationshipId = $this->getTenantCurrentLeaseId($loggedTenantId);
        $initialRentAmount = DB::table('landlord_tenants')->where('tenant_id', $loggedTenantId)
                ->where('active', 1)
                ->value('month_rent_initial');


        $store_rent_transaction = new ForteRentTransaction;
        $store_rent_transaction->landlord_tenant_id = $currentTenantLeaseRelationshipId;
        $store_rent_transaction->landlord_tenant_monthlyrents_unique_id = $rent_transaction_params['monthly_rent_unique_id'];
        $store_rent_transaction->transaction_token = $rent_transaction_params['transaction_token'];
        $store_rent_transaction->authorized_amount = $rent_transaction_params['authorized_amount'];
        $store_rent_transaction->date_due           = $rent_transaction_params['date_due'];
        $store_rent_transaction->date_posted = Carbon::now();
        $store_rent_transaction->response_code = $rent_transaction_params['response_code'];

        $store_rent_transaction->payment_is_late = $this->rentIsLate($store_rent_transaction->authorized_amount, $initialRentAmount);
        // print_r($store_rent_transaction);
        // exit('dfafwae');
        $store_rent_transaction->save();

        // These values are gotten from a Get, may get moved toa seaparate get method, and then update it to the db when seen
      //  $store_rent_transaction->status = $rent_transaction_params['status'];
      //  $store_rent_transaction->date_received = $rent_transaction_params['received_date'];


    }
    //Take in array of Transaction ID's to check for new status updates
    public function selfUpdateTransactions($transaction_id_array)
    {

        $foo = '';
        $date_funded = $foo;

        foreach($transaction_id_array as $trans_id)
        {
            $date_funded = ForteRentTransaction::where('transaction_token', $trans_id)
                                                ->select('date_funded')
                                                ->get();
            // Need to check for origination date and new statuses until status field in Database is 'Funded'
            if($date_funded != '')
            {
                //Carry on, transaction's been funded!
                continue;
            }
            else
            {
                // Make the Get Request to check for Date_Funded
                $get_trans_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions/' . $trans_id;

                // Now we wanna get the transaction and look for some properties
                
                //$get_stored_transaction = $this->forteGet($get_trans_endpoint, $auth_token);
            }
        }

    }


    public function schedule_rent_pay($rent_pay_params)
    {
      $loggedTenant       = Auth::user('tenant');
      $loggedTenantId     = $loggedTenant->id;
      $current_property_location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
      $base_url           = Config::get('constants.FORTE_BASE_URL');
      $organization_id    = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
      $location_id        = $current_property_location_id;
      $api_access_id      = Config::get('constants.FORTE_API_ACCESS_ID');
      $api_secure_key     = Config::get('constants.FORTE_API_SECURE_KEY');
      $auth_token         = base64_encode($api_access_id . ':' . $api_secure_key);

      $pay_schedule_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules';

      // This posts the rent payment schedule to Forte
      $schedule_rent_pay  = $this->fortePost($pay_schedule_endpoint, $rent_pay_params, $auth_token, $organization_id);

      $schedule_pay_response = curl_exec($schedule_rent_pay);
      $schedule_pay_info     = curl_getinfo($schedule_rent_pay);
      curl_close($schedule_rent_pay);
      $schedule_data = json_decode($pay_response);
      $create_http_success = (($pay_info['http_code'] != 201) ? 0:1);

      $createOutput = array(
        'create_http_success'   => $create_http_success,
        'data'                  => $schedule_data
      );

      return $createOutput;
    }

  public function getMyPaymentDetails($dateString=null)
  {
      $loggedTenantId = Auth::user('tenant')->id;
      $currentLeaseId = $this->getTenantCurrentLeaseId($loggedTenantId);
      $tenantCustomerId = $this->getTenantCustomerId($loggedTenantId);
      if($currentLeaseId && $tenantCustomerId)
      {
        $currentDateView = date("Y-m-d");

        $loggedTenantId =  Auth::user('tenant')->id;
        $currentDate ='';
        if($dateString!='')
        {
        $requestDate = substr($dateString,0,4).'-'.substr($dateString,-2);
        $currentDate = $requestDate;
        }
        else
        {
        $currentDate  =  date("Y-m");
        }

        $yearMonthTime  =  strtotime($currentDate);

        $nextMonth    =  str_replace('-','',date("Y-m", strtotime("+1 month", $yearMonthTime)));
        $prevMonth    =  str_replace('-','',date("Y-m", strtotime("-1 month", $yearMonthTime)));

        $currentMonth     = date("F", $yearMonthTime);
          $currentYear      = date("Y", $yearMonthTime);

        $myPaymentForMonthlyRents = DB::select('SELECT   "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LTM.payment_date,LT.id,LT.property_id,P.property_name, LTM.unique_id, LT.landlord_id, LT.tenant_id
                        FROM landlord_tenant_monthlyrents AS LTM
                        INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
                        INNER JOIN property_physical AS P ON LT.property_id = P.id
                        WHERE LT.tenant_id ='.$loggedTenantId.'
                        AND LTM.bill_on LIKE "'.$currentDate.'%'.'"  ORDER BY LTM.bill_on ASC');

        // print_r($myPaymentForMonthlyRents);
        // return var_dump($myPaymentForMonthlyRents);

        if($myPaymentForMonthlyRents)
        {
          // var_dump($myPaymentForMonthlyRents[0]->unique_id);
          // exit('');
          $initialRentAmount = $myPaymentForMonthlyRents[0]->amount;
          $uniqueId = $myPaymentForMonthlyRents[0]->unique_id;
          $myPaymentForMonthlyRents[0]->amount = '$'.$this->calculate_rent_amount($loggedTenantId, $uniqueId);
          $calculatedRentAmount = $myPaymentForMonthlyRents[0]->amount;

          $lateRent = $this->rentIsLate($initialRentAmount, $calculatedRentAmount);
        }
        else
        {
          $lateRent = null;
        }
        // print_r($myPaymentForMonthlyRents);
        // exit();
        return view('payment.mypayment_details', [
              'pageTitle'         => 'MyPayment',
              'pageHeading'       => 'MyPayment',
              'pageMenuName'      => 'mypayment',
              'monthlyrentDetails'  => $myPaymentForMonthlyRents,
              //'otherPayments'     => $myOtherPayments,
              'nextMonth'       => $nextMonth,
              'prevMonth'       => $prevMonth,
              'currentMonthDisplay'   => $currentMonth.' '.$currentYear,
              'currentDateView'   => $currentDateView,
              'lateRent' => $lateRent
          ]);
      }
      else
      {
        if(!$tenantCustomerId)
        {
          Session::flash('warningMessage', "Your payment settings are not set!");
          return redirect()->route('payments.getTenantPaymentSettings');
        }
        else
        {
          Session::flash('warningMessage', "You are not part of any active lease at the moment!");
          return redirect()->route('tenants.getMyLandlords');
        }
      }


    }

    public function rentIsLate($initialRentAmount, $calculatedRentAmount)
    {
      if (($initialRentAmount + 1.25) <= $calculatedRentAmount)
      {
        return false;
      }
      else{
        return true;
      }
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
        // This function posts the transaction via Forte Payment Processing (TEST FUNCTION)
    public function postTransaction(Request $request) {

      $base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
      // Landlord Required Fields
      
      // $api_access_id     = 'f82af85197d7f582789271f6ad15a958';
      // $api_secure_key    = 'f96cdf737b22ffe2397713ffd03d5d6d';
      $api_access_id     = env('FORTE_API_ACCESS_ID');
      $api_secure_key    = env('FORTE_API_SECURE_KEY');

      // END Landlord Required fields

      // Below is the value from the input field we put in the payment view form.
      $rent_amount       = $request->payment_amount;

      //Grab Logged Tenant Information from the Tenant table
      $loggedTenant = Auth::user('tenant');
      $loggedTenantID = $loggedTenant->id;
      $loggedTenantEmail = $loggedTenant->email;
      $organization_id   = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
      $location_id       = 'loc_193969';
      // $location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);

      // These Get Name methods are in Controller.php, not TenantsController
      $tenantFirstName = $this->getTenantFirstName($loggedTenant);
      $tenantLastName = $this->getTenantLastName($loggedTenant);

      // Property that the Tenant is part of.
      $tenantCurrentProperty = $this->getTenantCurrentProperty($loggedTenantID);

      ////////////////////////////
      // Tenant's Landlord Info //
      ////////////////////////////
      $landlord_loc_id = $this->getTenantCurrentLandlordLocationId($loggedTenantID);
      $landlordFirstName = $this->getTenantCurrentLandlordFirstName($loggedTenantID);
      $landlordLastName = $this->getTenantCurrentLandlordLastName($loggedTenantID);

      ///////////////////////
      // END LANDLORD INFO //
      ///////////////////////
      $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
      $endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions';

      //Address Info
      $address = array(
          'first_name' => $tenantFirstName,
          'last_name' => $tenantLastName
      );

      //eCheck Info
      $echeck = array(
          'account_holder' => 'Bill G Customer',
          'routing_number' => '091000019',
          'account_number' => '12345678901234',
          'account_type' => 'checking'
      );

      //Credit Card Info
      $card = array(
          'card_type' => 'VISA',
          'name_on_card' => 'Bill G Customer',
          'account_number' => '4111111111111111',
          'expire_month' => '12',
          'expire_year' => '2020',
          'card_verification_value' => '123'
      );

      $params = array(
          'action' => 'sale',        //sale, authorize, credit, void, capture, inquiry, verify, force, reverse
          'echeck' => $echeck,
          'billing_address' => $address,
          'authorization_amount' => $rent_amount,
          'customer_token' => '',
          'paymethod_token' => '',
          'transaction_id' => '',
          'authorization_code' => ''
      );

      $ch = curl_init($endpoint);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_VERBOSE, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Authorization: Basic ' . $auth_token,
          'X-Forte-Auth-Organization-id: ' . $organization_id,
          'Accept:application/json',
          'Content-type: application/json'
      ));

      $response = curl_exec($ch);
      $info = curl_getinfo($ch);
      curl_close($ch);
      $data = json_decode($response);

      echo '<pre>';
      echo 'Tenant ID:';
      echo $loggedTenant->id;
      echo '<br>';
      echo 'Tenant Email: ';
      echo $loggedTenantEmail;
      echo '<br>';
      echo $request->payment_amount;
      echo '<br>';
      echo 'Property Dump:<br>';
      echo $tenantCurrentProperty;
      echo '<br>';
      echo 'Landlord Name:';
      echo $landlordFirstName . ' ' . $landlordLastName;
      echo '<br>';
      echo 'Landlord Location ID: ';
      echo $landlord_loc_id . '<br>';
      //var_dump($loggedTenant);
      echo '<br>';
      print_r('HttpStatusCode: ' . $info['http_code'] . '<br><br>');
      print_r($data);
      echo '\n';

    }
}
