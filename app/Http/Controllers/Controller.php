<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\PropertyPhysical;
use App\Http\Models\PropertyMeta;
use App\Http\Models\MessageThread;
use App\Http\Models\Thread;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\Rating;
use App\Http\Models\RatingUser;
use App\Http\Models\Mysearch;
use App\Http\Models\LandlordTenant;
use App\Http\Models\TenantBillingAddress;
use App\Http\Models\LandlordPaymentReceiveSetting;
use App\Http\Models\ForteRentTransaction;
use App\Http\Models\LandlordPropertyManagementAccount;
use App\Http\Models\PropertyGroup;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\preEvApplications;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use Crypt;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function getTenantProfile($id)
    {
        $tenant 	= Tenant::find($id);

        /*$tenant 	= Tenant::with('school')
                            ->where('id',$id)->get();*/
        return $tenant;
    }

    public function getLandlordProfile($id)
    {

        $landlord 	= Landlord::findOrFail($id);
        $landlordSubscriptionPlan = DB::table('landlord_subscription')
            ->where('landlord_id', $landlord->id)->value('subscription_plan_id');
        //dd($landlord);
        $landlord['plan_type'] = $landlordSubscriptionPlan;

        return $landlord;
    }
    public function getLandlordProfileFromSlug($slug)
    {

        $landlord 	= Landlord::where('slug',$slug)->firstorfail();
        return $landlord;
    }

    public function getTenantProfileFromSlug($slug)
    {

        $tenant 	= Tenant::where('slug',$slug)->firstorfail();
        return $tenant;
    }

    public function getGender($gender)
    {
        if($gender=='')
        {
            $genderDetail = 'Not specified';
        }
        if($gender=='M')
        {
            $genderDetail = 'Male';
        }
        if($gender=='F')
        {
            $genderDetail = 'Female';
        }
        return $genderDetail;
    }
    public function getDob($dob)
    {
        if($dob=='0000-00-00')
        {
            $birthDate = 'Not specified';
        }
        else
        {
            $birthDate = $dob;
        }
        return $birthDate;
    }
    public function getState($state)
    {
        if($state=='')
        {
            $stateDetail = 'Not specified';
        }
        else
        {
            $stateDetail= $state;
        }
        return $stateDetail;
    }
    public function getCity($city)
    {

        if($city=='')
        {
            $cityDetail = 'Not specified';
        }
        else
        {
            $cityDetail= $city;
        }
        return $cityDetail;
    }
    public function getLanguage($language)
    {
        if($language=='')
        {
            $languageDetail = 'Not specified';
        }
        else
        {
            $languageDetail= $language;
        }
        return $languageDetail;
    }

    public function getPropertyPerBed($propertyId){

	$perBed = PropertyMeta::where('property_physical_id', '=', $propertyId)
		->select('charge_per_bed')
		->get();
	return $perBed;
    }

    public function getPropertyCount($landlordId)
    {

        $propertyCount = DB::table('property_physical')
            ->select(DB::raw('count(*) as propert_count'))
            ->where('landlord_id', '=',$landlordId)
            ->get();
        return $propertyCount;
    }

    public function getPropertyList($landlordId)
    {
        $propertyList = PropertyPhysical::where('landlord_id', '=', $landlordId)
	        ->orderBy('property_name', "ASC")
            ->select('id','property_name')
            ->with('property_meta')
            ->get();

        $activePropertyList = $propertyList->filter(function($item) {
           if ($item->property_meta->status == 1) {
               return $item;
           }
        });

        $activePropertyList->toArray();

        return $activePropertyList;
    }

    public function getPropertyIdList($landlordId)
    {
        $propertyList = Property::where('landlord_id', '=', $landlordId)
            ->where('status',1)
            ->select('id')
            ->get()
            ->toArray();

        return $propertyList;
    }

    public function getPropertyCurrentTenantCount($propertyId)
    {
	 $activeLandlordTenantCount = LandlordTenant::where('property_id', $propertyId)
            ->where('active', 1)
            ->count();

	return $activeLandlordTenantCount;
    }

    public function getPropertyPrice($propertyId)
    {
	$propertyPrice = PropertyMeta::where('property_physical_id', $propertyId)
		->value('rent_price');

	return $propertyPrice;
    }
    public function getPropertyNameFromId($propertyId)
    {
        $propertyName = PropertyPhysical::where('id', '=', $propertyId)
            ->value('property_name');
        return $propertyName;
    }

    // Get Tenant First Name
    public function getTenantFirstName($profile)
    {
        $firstname = ucfirst($profile->firstname);
        return $firstname;
    }

    // Get Tenant Last Name
    public function getTenantLastName($profile)
    {
        $lastname = ucfirst($profile->lastname);
        return $lastname;
    }

    public function getTenantFirstNamebyId($tenantId)
    {
        $tenant_firstname = DB::table('tenants')->where('id', $tenantId)->value('firstname');
        return $tenant_firstname;
    }
    // Full Name
    public function getTenantName($profile)
    {
        $name = ucfirst($profile->firstname)." ".ucfirst($profile->lastname);
        return $name;
    }

    // Tenant Billing address
    public function getTenantBillingAddress($tenantId)
    {
        $tenant_billing_address = TenantBillingAddress::where('tenant_id' ,'=', $tenantId)
            ->get();
        return $tenant_billing_address;
    }

    // Tenant E-Mail address
    public function getTenantEmail($tenantId)
    {
        $tenant_email = DB::table('tenants')->where('id',$tenantId)->value('email');
        return $tenant_email;
    }

    // Tenant ID by E-Mail address
    public function getTenantIdByEmail($email)
    {
	$tenantId = DB::table('tenants')->where('email',$email)->value('id');
	return $tenantId;
    }
    ///////////////////////////////////////////////////////////////////
    // Get the current Property that the logged Tenant is living in //
    /////////////////////////////////////////////////////////////////
    public function getTenantCurrentProperty($tenantId)
    {
      //dd($tenantId);
        $currentproperty = DB::table('landlord_tenants')
                        -> join('property_physical', function ($join) use ($tenantId) {
                            $join->on('landlord_tenants.property_id', '=', 'property_physical.id')
                                 ->where('landlord_tenants.tenant_id', '=', $tenantId)
                                 ->where('landlord_tenants.active', '=', 1);
                        })
                        ->first();
	if($currentproperty != false){
        $currentpropertyname = $currentproperty->property_name;
        return $currentpropertyname;
	}
	else{
	return false;
	}
    }
    ///////////////////////////////////////////////////////////////////
    // Get the current Property ID that the logged Tenant is living in //
    /////////////////////////////////////////////////////////////////
    public function getTenantCurrentPropertyId($tenantId)
    {
        $currentproperty = DB::table('landlord_tenants')
                        -> join('property_physical', function ($join) use ($tenantId) {
                            $join->on('landlord_tenants.property_id', '=', 'property_physical.id')
                                 ->where('landlord_tenants.tenant_id', '=', $tenantId)
                                 ->where('landlord_tenants.active', '=', 1);
                        })
                        ->first();
        if($currentproperty != false)
        {
        	$currentpropertyid = $currentproperty->property_id;
                return $currentpropertyid;
        }
    	else
        {
    	   return false;
    	}
    }

    ////////////////////////////////////////////////////////
    // Get Tenant's currently stored Landlord Location ID //
    ///////////////////////////////////////////////////////
    public function getTenantStoredLandlordOrgId($tenantId)
    {
        $current_tenant_pay_settings = DB::table('forte_tenant_pay_settings')
            ->select('current_landlord_org_id')
            ->where('tenant_id', '=', $tenantId)
            ->value('current_landlord_org_id');
        if($current_tenant_pay_settings == '')
        {
            $stored_landlord_org_id = $current_tenant_pay_settings;
        }
        else
        {
            try{
                $stored_landlord_org_id = Crypt::decrypt($current_tenant_pay_settings);
            } catch (DecryptException $e){
                echo "This is wrong";
            }
        }

        return $stored_landlord_org_id;
    }
    public function getTenantStoredPropertyLocId($tenantId)
    {
        $current_tenant_pay_settings = DB::table('forte_tenant_pay_settings')
            ->select('current_property_loc_id')
            ->where('tenant_id', '=', $tenantId)
            ->value('current_property_loc_id');
        if($current_tenant_pay_settings == '')
        {
            $stored_property_loc_id = $current_tenant_pay_settings;
        }
        else
        {
            try{
                $stored_property_loc_id = Crypt::decrypt($current_tenant_pay_settings);
            } catch (DecryptException $e){
                echo "This is wrong";
            }
        }

        return $stored_property_loc_id;
    }
    ////////////////////////////////////////////////////////////////
    // Get the current Landlord that the logged Tenant is living //
    //////////////////////////////////////////////////////////////
    public function getTenantCurrentLandlordId($tenantId)
    {
        $currentlandlord_id = DB::table('landlord_tenants')
            -> join('landlords', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'landlords.id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->value('landlords.id');
        //$currentlandlord_id = $currentlandlord->id;
        return $currentlandlord_id;
    }

    public function getTenantCurrentLandlordFirstName($tenantId)
    {
        $currentlandlord = DB::table('landlord_tenants')
            -> join('landlords', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'landlords.id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->first();
        $currentlandlord_firstname = $currentlandlord->firstname;
        return $currentlandlord_firstname;
    }

    public function getTenantCurrentLandlordLastName($tenantId)
    {
        $currentlandlord = DB::table('landlord_tenants')
            -> join('landlords', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'landlords.id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->first();
        $currentlandlord_lastname = $currentlandlord->lastname;
        return $currentlandlord_lastname;
    }

    public function getTenantCurrentLandlordEmail($tenantId)
    {
        $currentlandlord = DB::table('landlord_tenants')
            -> join('landlords', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'landlords.id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->first();
        $currentlandlord_email = $currentlandlord->email;
        return $currentlandlord_email;
    }

    // Get the important information - Schedule Frequency, Rent Due Day of Month (Found in forte_landlord_pay_settings_rent)
    // Also get the lease agreement start date, lease agreement end date, and the rent amount by month (the value will be multiplied according to the frequency)
    public function getTenantCurrentLandlordRentPaySettings($tenantId)
    {
        $currentlandlord_receive_pay_settings = DB::table('landlord_tenants')
            -> join('forte_landlord_pay_settings_rent', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_rent.landlord_id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->first();
        return $currentlandlord_receive_pay_settings;
    }
    // Returns the array that should have the required params to make a recurring schedule
    /* public function getTenantCurrentLandlordRentScheduleSettings($tenantId)
     {
         $currentlandlord_receive_schedule_settings = DB::table('landlord_tenants')
                         -> join('forte_landlord_pay_settings_rent', function ($join) use ($tenantId) {
                             $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_rent.landlord_id')
                                 ->where('landlord_tenants.tenant_id', '=', $tenantId)
                                 ->where('landlord_tenants.active', '=', 1);
                         })
                             ->first();
         $landlord_freq_num = $currentlandlord_receive_schedule_settings->schedule_frequency;
         $schedule_freq_list = array("monthly", "bi_monthly", "quarterly", "semi_annually");
         $schedule_params = array(
             'schedule_quantity' => ,
             'schedule_frequency' => $schedule_freq_list[$landlord_freq_num],
             'schedule_amount'	=> $currentlandlord_receive_schedule_settings->
         );
     }
     */
    // Get Landlord_Tenant information
    public function getCurrentTenantLandlordInfo($tenantId)
    {
        $currentrelationship = DB::table('landlord_tenants')
            ->where('landlord_tenants.tenant_id', '=', $tenantId)
            ->where('landlord_tenants.active', '=', 1)
            ->first();
        return $currentrelationship;
    }

    public function monthsLeftLeaseAgreement($tenantId)
    {

    }

    public function getTenantCurrentLeaseAgreementDuration($tenantId)
    {
        $currentLandlordId = $this->getTenantCurrentLandlordId($tenantId);
        $current_relationship = LandlordTenant::where('tenantId', $tenantId)
            ->where('landlord_id', $currentLandlordId)
            ->where('active', 1)
            ->first();
        $start_date = $current_relationship->agreement_start_date;
        $end_date = $current_relationship->agreement_end_date;

        $start_end = array(
            'start' => $start_date,
            'end'   => $end_date
        );
        return $start_end;
    }

    public function tenantCurrentLandlordRentPaySettingsCollectFrequency($tenantId)
    {
        $currentlandlord_pay_settings = $this->getTenantCurrentLandlordRentPaySettings($tenantId);
        $schedule_freq = $currentlandlord_pay_settings->schedule_frequency;

        switch ($schedule_freq)
        {
            case 0:
                $schedule_freq = 'monthly';
                break;
            case 1:
                $schedule_freq = 'bi_monthly';
                break;
            case 2:
                $schedule_freq = 'quarterly';
                break;
            case 3:
                $schedule_freq = 'semi_annually';
                break;
        }
        return $schedule_freq;
    }
    // ID of landlord_tenants
    public function getTenantCurrentLeaseId($tenantId)
    {
        //$todays_date = date("Y-m-d");

        $lease_relationship_id = DB::table('landlord_tenants')
            ->where('tenant_id', $tenantId)
            ->where('active', 1)
            //->whereBetween($todays_date, ['agreement_start_date', 'agreement_end_date'])
            ->value('id');
        return $lease_relationship_id;
    }

    public function landlordGetAllLease($landlordId)
    {
        $lease_history = LandlordTenant::where('landlord_id', $landlordId)
            ->all();
        return $lease_history;
    }

    public function getTenantCurrentRemainingPayments($tenantId)
    {
        $tenant_active_lease_relationship_id = $this->getTenantCurrentLeaseId($tenantId);
        $active_lease_relationship = DB::table('landlord_tenant_monthlyrents')
            ->where('landlord_tenant_monthlyrents.landlord_tenant_id', '=', $tenant_active_lease_relationship_id)
            ->where('landlord_tenant_monthlyrents.status', '=', 0)
            ->orWhere('landlord_tenant_monthlyrents.status', '=', 2)
            ->count();
        return $active_lease_relationship;
    }

    // LANDLORD GENERAL PAY SETTINGS FOR RENT - 12.24.16 - Model LandlordPaymentReceiveSetting, Table = forte_landlord_pay_settings_rent
    public function getLandlordRentPaySettings($landlordId)
    {
        $rent_pay_settings = LandlordPaymentReceiveSetting::where('landlord_id', $landlordId)
            ->first();
        return $rent_pay_settings;
    }

    // Get Collection Frequency of Landlord
    public function getCollectionFrequencyLandlord($landlordId)
    {
        // 0=monthly, 1=bi-monthly, 2=quarterly, 3=semi-annually
        $rent_pay_settings = LandlordPaymentReceiveSetting::where('landlord_id', $landlordId)
            ->first();
        if ($rent_pay_settings != '')
        {
            $rent_collection_frequency = $rent_pay_settings->schedule_frequency;
        }
        else
        {
            // Default to Monthly Collections
            $rent_collection_frequency = 0;
        }
        return $rent_collection_frequency;
    }
    // This function gets the number that separates the amount of payments depending on Landlord's rent collection rate/frequency
    // Example: Lease Length is 1 year, that is 12 months of rent to pay. Landlord wants quarterly payments (Every 3 months).
    //////////  Tenant now only has to make 4 payments, once every 3 months for the year.
    public function getFactorByLandlordRentCollectionFreq($landlordId)
    {
        // 0=monthly, 1=bi-monthly, 2=quarterly, 3=semi-annually
        // Assume lease length is 1 year (12 billable months).
        // Assume Monthly Rent Value/Amount is $700
        $collection_frequency = $this->getCollectionFrequencyLandlord($landlordId);

        switch($collection_frequency){
            case 0:
                // Monthly collect, so the factor is 1. 12 / 1 = 12 payments of (700 * 1) = $700 per payment
                $factor = 1;
                break;
            case 1:
                // Bi-monthly collect, so the factor is 2. 12 / 2 = 6 payments of (700 * 2) = $1400 per payment
                $factor = 2;
                break;
            case 2:
                // Quarterly (Every 3 Months), so the factor is 3. 12 / 3 = 4 payments of ($700 * 3) = $2100 per payment
                $factor = 3;
                break;
            case 3:
                // Semi-Annual (Every 6 months), so the factor is 6. 12 / 6 = 2 payments of ($700 * 6) = $4200 per payment
                $factor = 6;
                break;
        }

        return $factor;
    }

    // This gets the amount needed to pay on the landlord tenant monthly rent payment
    public function getMonthlyRentAmountByUniqueId($uniqueId)
    {
        $amount = DB::table('landlord_tenant_monthlyrents')->where('unique_id', $uniqueId)->value('amount');
        return $amount;
    }
    public function getMonthlyRentPayDueDate($uniqueId)
    {
        $due_date = DB::table('landlord_tenant_monthlyrents')->where('unique_id', $uniqueId)->value('bill_on');
        return $due_date;
    }
    // This should always return 0 or 1, if it returns more than 1, we have a problem, since Tenants can't have more than 1 active lease. If 1, then we should check if proposed new lease agreement conflicts with existing Lease Agreement
    public function activeLandlordTenantCount($tenantId, $lease_length)
    {
        $totalDurationStart = $lease_length['lease_start'];
        $totalDurationEnd = $lease_length['lease_end'];
        // lease_length is the array parameter that houses (lease start date, lease end date) - we need to check for any dates that fall between the two dates in this array.
        $activeLandlordTenant = LandlordTenant::where('tenant_id', $tenantId)
            ->where('active', 1)
            ->count();
        switch ($activeLandlordTenant){
            case 0:
                return $activeLandlordTenant;
                break;
            case 1:
                // This checks in the landlord_tenants table for ALL active relationships of the tenant id, making sure no date conflicts
                $activeLandlordTenantLeaseConflict = LandlordTenant::where('tenant_id', $tenantId)
                    ->where('active', 1)
                    ->where(function($q) use ($lease_length){
                        $q->whereBetween('agreement_start_date', $lease_length);
                        $q->orWhereBetween('agreement_end_date', $lease_length);
                    })
                    ->count();
                return $activeLandlordTenantLeaseConflict;
                break;
            default:
                // Something is wrong, tell Function in Controller to error out and contact Support.
                return $activeLandlordTenant;
        }
        // If count is 0, Landlord is good to add the Tenant, but if it is 1, that means Tenant has an active lease relationship with a Landlord. We must check if their existing lease conflicts with the new future lease at all.
    }


    public function getAdminServiceFee()
    {
        $service_fee = DB::table('admin_receive_pay_settings')->select('tenant_sub_fee')->get();
        return $service_fee;
    }
    //////////////////////////////////////////////////////////
    // General Landlord getPayment Settings unique to Forte //
    //////////////////////////////////////////////////////////
    // public function getLandlordLocationId($landlordId)
    // {
    //     $landlord_pay_receive = DB::table('forte_landlord_pay_settings_rent')
    //         ->where('landlord_id','=',$landlordId)->first();


    //     if($landlord_pay_receive != '')
    //     {
    //         $loc_id = $landlord_pay_receive->loc_id;
    //         $landlord_loc_id = Crypt::decrypt($loc_id);
    //     }
    //     else
    //     {
    //         // try {
    //         //     echo var_dump($landlord_pay_receive);
    //         //     $loc_id = $landlord_pay_receive->loc_id;
    //         //     $landlord_loc_id = Crypt::decrypt($loc_id);

    //         // } catch (Exception $e) {
    //         //     echo 'this is wrong';
    //         // }

    //         $landlord_loc_id = '';

    //         // Session::flash('warningMessage', "You must set your Rent Colection Payment Settings!");
    //         // return redirect()->route('landlords.dashboard');

    //     }
    //     return $landlord_loc_id;
    // }

    public function getLandlordOrgId($landlordId)
    {
        $landlord_organization_id = DB::table('forte_landlord_pay_settings_rent')
            ->where('landlord_id','=',$landlordId)->value('org_id');


        if($landlord_organization_id != '')
        {
            $org_id = $landlord_organization_id;
            $landlord_org_id = Crypt::decrypt($org_id);
        }
        else
        {
            // try {
            //     echo var_dump($landlord_pay_receive);
            //     $loc_id = $landlord_pay_receive->loc_id;
            //     $landlord_loc_id = Crypt::decrypt($loc_id);

            // } catch (Exception $e) {
            //     echo 'this is wrong';
            // }

            $landlord_org_id = '';

            // Session::flash('warningMessage', "You must set your Rent Colection Payment Settings!");
            // return redirect()->route('landlords.dashboard');

        }
        return $landlord_org_id;
    }

    public function getLandlordRentDueDay($landlordId)
    {
        $landlord_pay_settings = DB::table('forte_landlord_pay_settings_rent')->where('landlord_id', '=', $landlordId)->first();
        // If Landlord Has Payment Settings
        if($landlord_pay_settings !='')
        {
            $due_day = $landlord_pay_settings->rent_due_day_of_month;
        }
        else //He doesn't have payment Settings set
        {
            $due_day = '3';
        }

        return $due_day;
    }




    //////////////////////////////////////////////////////////////
    // END General Landlord getPayment Settings unique to Forte //
    //////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////////////////
    // Get Rent Payment Settings of Current Landlord of Logged In Tenant //
    ///////////////////////////////////////////////////////////////////////

    public function getTenantCurrentLandlordOrgId($tenantId)
    {
        $currentlandlord_org_id = null;
        $currentlandlord_pay = DB::table('landlord_tenants')
            -> join('forte_landlord_pay_settings_rent', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_rent.landlord_id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId)
                    ->where('landlord_tenants.active', '=', 1);
            })
            ->value('org_id');
        // If the query is null, set org id to null, it's not there
        if($currentlandlord_pay == '')
        {
            $currentlandlord_org_id = null;
        }
        else // It's there, decrypt it and return it.
        {
            try{
                $currentlandlord_org_id = Crypt::decrypt($currentlandlord_pay);
            } catch (\Encryption $e){
                echo "This is wrong";
            }
            return $currentlandlord_org_id;
        }
    }

    // Need LLC's to get this done
    public function getTenantCurrentPropertyLocationId($tenantId)
    {
        // Get Tenant Current Property ID
        $currentPropertyId = $this->getTenantCurrentPropertyId($tenantId);
        $currentPropertyLocationId = DB::table('landlord_property_management_accounts')
                                ->join('properties', function ($join) use ($currentPropertyId){
                                    $join->on('landlord_property_management_accounts.id', '=', 'properties.landlord_prop_mgmt_id')
                                        ->where('properties.id', '=', $currentPropertyId);
                                })
                                ->value('loc_id');
        if($currentPropertyLocationId == '')
        {
            $loc_id	= null;
        }
        else
        {
            try{
                $loc_id = Crypt::decrypt($currentPropertyLocationId);
            } catch (Encryption $e){
                echo "This is wrong";
            }
        }
        return $loc_id;
    }
    public function getTenantCustomerId($tenantId)
    {
        $customer_id = DB::table('forte_tenant_pay_settings')
            ->where('tenant_id', $tenantId)
            ->value('customer_id');
        if($customer_id == '')
        {
            $current_customer_id = $customer_id;
        }
        else
        {
            try{
                $current_customer_id = Crypt::decrypt($customer_id);
            } catch (DecryptException $e){
                echo "cAN'T GET the id";
            }
        }
        return $current_customer_id;
    }


    public function getTenantPaymentId($tenantId)
    {
        $payment_id = DB::table('forte_tenant_pay_settings')
            ->where('tenant_id', $tenantId)
            ->value('payment_id');
        if($payment_id == '')
        {
            $current_payment_id = $payment_id;
        }
        else
        {
            try{
                $current_payment_id = Crypt::decrypt($payment_id);
            } catch (DecryptException $e){
                //
            }
        }
        return $current_payment_id;
    }

    public function getTenantAddressId($tenantId)
    {
        $address_id = DB::table('forte_tenant_pay_settings')
            ->where('tenant_id', $tenantId)
            ->value('addr_id');
        if($address_id == '')
        {
            $current_address_id = $address_id;
        }
        else
        {
            try{
                $current_address_id = Crypt::decrypt($address_id);
            } catch (DecryptException $e){
                //
            }
        }
        return $current_address_id;
    }

    public function getLandlordAddressId($landlordId)
    {
        $address_id = DB::table('forte_landlord_subscription_pay_settings')
            ->where('landlord_id', $landlordId)
            ->value('addr_id');
        if($address_id == '')
        {
            $current_address_id = $address_id;
        }
        else
        {
            try{
                $current_address_id = Crypt::decrypt($address_id);
            } catch (DecryptException $e){
                //
            }
        }
        return $current_address_id;
    }
    /*
        public function getTenantCurrentLandlordOrgId($tenantId)
        {
            $currentlandlord_pay = DB::table('landlord_tenants')
                -> join('forte_landlord_pay_settings_rent', function ($join) use ($tenantId) {
                    $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_receive.landlord_id')
                        ->where('landlord_tenants.tenant_id', '=', $tenantId)
                        ->where('landlord_tenants.active', '=', 1);
                })
                    ->first();
            $currentlandlord_org_id = $currentlandlord_pay->org_id;
            return $currentlandlord_org_id;
        }
    */
    ///////////////////////////////////////////////////////////////////
    // END PAYMENT SETTINGS OF CURRENT LANDLORD OF LOGGED IN TENANT //
    /////////////////////////////////////////////////////////////////


    public function getPropertyMessageThread($tenantId)
    {

        $propertyCount = DB::table('message_threads')
            ->select(DB::raw('count(*) as threadcount'))
            ->where('tenant_id', '=',$tenantId)
            ->get();

        return $propertyCount;
    }

    public function getListMessageThreads($tenantId)
    {


        $listMessageThreads=  DB::table('message_threads')
            ->join('threads', 'message_threads.thread_id', '=', 'threads.id')
            ->select('message_threads.thread_id')
            ->addSelect('message_threads.created_at')
            ->addSelect('threads.thread_name')
            ->where('message_threads.tenant_id', '=', $tenantId)

            ->get();


        //$array = (array) $listMessageThreads;
        //print_r($array);exit;
        return $listMessageThreads;

    }

    public function getAllMessagesWithThread($threadId,$tenantId)
    {

        $listOfMessages = Message::where('message_thread_id', '=', $threadId)
            ->where('tenant_id', '=', $tenantId)
            ->select('message')
            ->addSelect('message_thread_id')
            ->addSelect('sender')
            ->addSelect('tenant_id')
            ->addSelect('landlord_id')
            ->addSelect('created_at')
            ->orderBy('created_at','ASC')
            ->get()
            ->toArray();

        return $listOfMessages;
    }
    public function getAllMessagesWithThreadLandlord($threadId,$landlordId)
    {
        $listOfMessages = Message::where('message_thread_id', '=', $threadId)
            ->where('landlord_id', '=', $landlordId)
            ->select('message')
            ->addSelect('message_thread_id')
            ->addSelect('sender')
            ->addSelect('tenant_id')
            ->addSelect('landlord_id')
            ->addSelect('created_at')
            ->orderBy('created_at','ASC')
            ->get()
            ->toArray();
            //dd($listOfMessages);
        return $listOfMessages;
    }

    public function getLandLordName($landlordId)
    {


        $landlordName = Landlord::where('id', '=', $landlordId)
            ->select('firstname')
            ->addSelect('lastname')
            ->get()->first();
        if(empty($landlordName))
        {
            return redirect('/404');
        }
        $name = $landlordName->getFullName();

        return $name;

    }
    public function getTenantNameFromId($tenantId)
    {
        //dd($tenantId);
        $tenantName = Tenant::where('id', '=', $tenantId)
            ->select('firstname')
            ->addSelect('lastname')
            ->first();

        return $tenantName->firstname." ".$tenantName->lastname;

    }
    public function getTenantsEmailsFromActiveProperty($property_id)
    {
        $tenants = LandlordTenant::where('property_id', $property_id)
                       ->where('active', '1')
                       ->select('tenant_id')
                       ->get()
                       ->toArray();
        $tenantsEmails = array();
        foreach($tenants as $tenant){
        $tenantsEmail = Tenant::where('id',$tenant["tenant_id"])
                        ->select('email')
                        ->get();
        $tenantsEmails[] = $tenantsEmail[0]->email;
        }
        return $tenantsEmails;

    }
    public function getTenantsForDropDown($landlordSchoolId)
    {
        $tenants = Tenant::where('school_id', $landlordSchoolId)
            ->orderBy('id')
            ->get();
        return $tenants;
    }
    public function getTenantsForEditDropDown($landLordId)
    {
        $tenants = LandlordTenant::where('landlord_id', $landLordId)
            ->with('tenant')
            ->groupBy('tenant_id')
            ->get();
        return $tenants;
    }
    public function getPropertiesForDropDown( $landLordId)
    {
        $properties = PropertyPhysical::where('landlord_id', $landLordId)
            ->orderBy('id')
            ->get();


        return $properties;
    }
    public function getPaidPropertyCount($landLordId)
    {
        $propertiesWithMgmtAccountCount = DB::table('property_physical')->where('landlord_id', $landLordId)
            ->leftJoin('property_meta', 'property_physical.id', '=' , 'property_meta.property_physical_id')
             ->whereNotNull('property_meta.landlord_prop_mgmt_id')
	      ->select(DB::raw('count(*) as propert_count'))->get();

	return $propertiesWithMgmtAccountCount;

    }
    public function getPaidPropertyIdList($landLordId){
	$propertyList = DB::table('property_physical')->where('landlord_id', $landLordId)
            ->leftJoin('property_meta', 'property_physical.id', '=', 'property_meta.property_physical_id')
	    ->whereNotNull('property_meta.landlord_prop_mgmt_id')
            ->select('property_physical.id')
	    ->get();

        return $propertyList;

    }
    public function getApartmentIdList($landLordId){
	$apartmentList = DB::table('apartment_building')->where('landlord_id', $landLordId)
	    ->select('id')
	    ->get();

	return $apartmentList;

    }

    public function getPaidPropertiesForDropDown($landLordId)
    {
        /*$properties = PropertyPhysical::where('landlord_id', $landLordId)
            ->orderBy('id')
            ->get();
	*/
	$propertiesWithMgmtAccount = DB::table('property_physical')->where('landlord_id', $landLordId)
	    ->leftJoin('property_meta', 'property_physical.id', '=' , 'property_meta.property_physical_id')
             ->whereNotNull('property_meta.landlord_prop_mgmt_id')->get();

	/*
        $relevantProperties = $properties->filter(function($item) {
            if (($item->property_meta->status == 1) && (!nullOrEmptyString($item->property_meta->landlord_prop_mgmt_id))) {
                return $item;
            }
        });
	*/
        return $propertiesWithMgmtAccount;
    }

    public function getMessageThreadName( $mgThreadId )
    {
        $threads = Thread::where('id',$mgThreadId)
            ->select('thread_name')
            ->addSelect('created_at')->first();
        return $threads;
    }
    public function getThreads($tenantId=null)
    {
        $threads = Thread::select('id')
            -> addSelect('thread_name')
            -> orderBy('order_status')
            -> get();


        return $threads;
    }

    public function getThreadsForFlag()
    {
        $threads = Thread::select('id')
            -> where('flag',1)
            -> orderBy('order_status')
            -> get()->toArray();
        return $threads;
    }

    public function getTenantThread($tenantId=null,$landlordId,$propertyId=null)
    {
        //echo $landlordId;exit;
        if($tenantId=="")
        {
            $tenantId = Auth::user('tenant')->id;
        }
        if($this->isTenantAssociatedWithThisProperty($tenantId,$landlordId,$propertyId))
        {
            $threads = Thread::select('id')
                -> addSelect('thread_name')
                -> orderBy('order_status')
                -> get();
            return $threads;

        }
        else
        {
            $threads = Thread::select('id')
                -> addSelect('thread_name')
                -> where('flag',0)
                -> orderBy('order_status')
                -> get();
            return $threads;
        }


    }

    public function isTenantAssociatedWithLandlord($tenantId)
    {
        $landlordTenantWithOutProperty = LandlordTenant::where('tenant_id',$tenantId)
            ->where('active',1)
            ->get();

        if(count($landlordTenantWithOutProperty) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    public function isTenantAssociatedWithThisProperty($tenantId,$landlordId,$propertyId=null)
    {
        //echo $landlordId;exit;
        if($propertyId!='')
        {
            $landlordTenantWithProperty = LandlordTenant::where('property_id',$propertyId)
                ->where('tenant_id',$tenantId)
                ->where('active',1)
                ->get();
            if(count($landlordTenantWithProperty) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            $landlordTenantWithOutProperty = LandlordTenant::where('landlord_id',$landlordId)
                ->where('tenant_id',$tenantId)
                ->get();
            if(count($landlordTenantWithOutProperty) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //TODO

        //LandlordTenant::
        //return true;

    }

    public function isTenantThread($messageThreadId,$tenantId)
    {
        $messages = Message::where('message_thread_id', '=', $messageThreadId)
            ->where('tenant_id', '=', $tenantId)
            ->select('message')
            ->get();
        if(count($messages) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getRating()
    {
        $ratings = Rating::select('id')
            ->addSelect('rating_name')
            ->get();

        return $ratings;
    }
    public function getProperties($landlordId)
    {
        $properties  = PropertyPhysical::where('landlord_id', $landlordId)
            -> orderBy('created_at','DESC')
            -> take(3)
            -> get();
        return  $properties;
    }
    public function findProperty( $propertyId )
    {
        $property  = PropertyPhysical::where('id', $propertyId)
            -> with('property_images')
            -> with('property_meta')
            -> with('property_amenities')
            -> with('property_docs')
            -> first();
        return  $property;
    }

    //this method is for fetch all the properties foa a landlord with status 1
    public function getAllProperties($landlordId)
    {
        $propertyDetails = Property::from('properties as P')
            ->select('P.property_name')
            ->addSelect('P.location')
            ->addSelect('P.rent_price')
            ->addSelect('P.rent_price')
            ->addSelect('PI.image_name')
            ->join('property_images as PI','P.id','=','PI.property_id')
            ->where('P.landlord_id',$landlordId)
            ->where('P.status',1)
            ->where('PI.featured',1)
            ->orderBy('P.created_at','DESC')
            ->get();

        return $propertyDetails;
    }

    /*public function findProperty( $propertyId )
    {
            $property  = Property::where('id', $propertyId)
                                       -> with('property_images')
                                       -> with('property_amenities')
                                       -> with('property_docs')
                                       -> first();
           return  $property;
    }*/
    public function getAllProperty( $landlordId )
    {

        $properties  = PropertyPhysical::where('landlord_id', $landlordId)
            -> with('property_meta')
            -> with('property_images')
            -> with('property_amenities')
            -> with('property_docs')
            -> get();

        $activeProperties = $properties->filter(function($item) {
            if ($item->property_meta->status == 1) {
                return $item;
            }
        });
        return  $activeProperties;

    }
    public function updateOtherTenantsRent($propertyId, $loopCount, $factor, $due_on, $monthStart, $yearStart)
    {
        // get current rent rate and set initial bill date
	if($this->getPropertyCurrentTenantCount($propertyId) == 0){
	return 0;
	}
	else{
        $currentRent = ($this->getPropertyPrice($propertyId)/$this->getPropertyCurrentTenantCount($propertyId));
        // get landlordTenants tenants who are active on property
        $activeLandlordTenants = LandlordTenant::where('property_id', $propertyId)
            ->where('active', 1)
            ->select('tenant_id')
            ->get()
            ->toArray();
        // update all of their month_rent_initial fields accordingly
        foreach ($activeLandlordTenants as $tenant){
        $updateActiveTenantsMonthRentInitial = LandlordTenant::where('property_id', $propertyId)
            ->where('tenant_id',$tenant)
            ->where('active', 1)
            ->update(['month_rent_initial' => $currentRent]);
        }
        // get all landlord_tenant_id's and lease start dates for a given property which are active
        $activeLandlordTenantIds = LandlordTenant::where('property_id',$propertyId)
            ->where('active',1)
            ->select('id','agreement_start_date')
            ->get()
            ->toArray();
        // for each of these active landlord_tenant_id's update their monthly_rent amount accordingly
        foreach($activeLandlordTenantIds as $landlordTenantId){
	$nextpaymonth = 0;
    list($monthStart, $dayStart, $yearStart) =  explode('-', $landlordTenantId["agreement_start_date"]);
    $billDate = $monthStart.'-'.$dayStart.'-'.$due_on;
        for($i=1;$i<=$loopCount/$factor;$i++){
        $nextpaymonth = $i * $factor;
        $nextBillDate = date("Y-m-d", strtotime("+".$nextpaymonth." month", strtotime($billDate)));
        $updatingMonth =  landlordTenantMonthlyRent::where('landlord_tenant_id', $landlordTenantId["id"])
                                                  ->where('bill_on', $nextBillDate)
                                                  ->update(['amount'=>($currentRent * $factor)]);
        }
        }
        // we did it
        return 1;
	}

    }

    //this method is for the listing of schools
    public function  getSchoolDetails()
    {
        $school  = array();
        $school  = DB::table('schools')->where('is_active',1)->orderBy('schoolname', 'asc')->lists('schoolname', 'id');
        return $school ;
    }
    //get a school's schoolId from id
    public function getSchoolSchoolId($id){
	$schoolId = DB::table('schools')->where('id', $id)
			->select('schoolid')
			->get();
	return $schoolId; 
    }

    //this method is for the fetch school with an id

    public function getSchoolWithId($schoolId)
    {
        $schoolName = School::where('id',$schoolId)
            ->select('schoolname')
            ->get()->first();
        return $schoolName;
    }

    public function getSchoolAddressById($schoolId) {
        $schoolAddress = School::where('id',$schoolId)
            ->select('address')
            ->get()->first();
        return $schoolAddress;
    }

    public function getSchoolCoordsById($schoolId) {
        $schoolCoords = School::where('id', $schoolId)
            ->select('latitude', 'longitude')->get()->first();
        return $schoolCoords;
    }

    //need method to get latitude and longitude of school based on name

    public function getSchoolInfoWithName($schoolName)
    {
        $school = School::where('schoolname',$schoolName)
		->select('latitude','longitude', 'id')
	        ->get()->first();
	return $school;
    }
    //this method is for fetch all the landlords

    public function getLandlords()
    {
        $landlords 	= Landlord::select('firstname')
            ->addSelect('lastname')
            ->addSelect('id')
            ->where('is_active',1)
            ->get();
        return $landlords;
    }

    public function getAllLandlords()
    {
        $landlords  = array();
        if(Session::has('schoolName'))
        {
            $sessionSchoolId = Session::get('schoolName');
            $landlords = DB::table('landlords')->select(DB::raw("CONCAT(firstname,' ',lastname) as fullname,id"))->where('is_active',1)->where('school_id',$sessionSchoolId)->lists('fullname', 'id');
        }else{
            $landlords = DB::table('landlords')->select(DB::raw("CONCAT(firstname,' ',lastname) as fullname,id"))->where('is_active',1)->lists('fullname', 'id');
        }
        return $landlords;
    }
    public function getReviewCount( $propertyId )
    {

        $reviewsCount = DB::select("SELECT COUNT(*) AS cnt FROM property_review WHERE property_id IN ($propertyId)");

        return $reviewsCount[0]->cnt;
    }
    public function getTenantsPayments( $landLordId )
    {
        $tenants  = LandlordTenant::where('landlord_id', $landLordId)
            -> with('tenant')
            -> get();
        return  $tenants;
    }
    public function getlandlordsPayments()
    {
        $landlords  = LandlordTenant::with('landlord')
            ->groupBy('landlord_id')
            -> get();
        return  $landlords;
    }
    public function getTenantsPaymentsForLandlord( $landLordId )
    {
        $tenants  = LandlordTenant::where('landlord_id', $landLordId)
            -> with('tenant')
            -> get();
        return  $tenants;
    }

    public function logMsg($msg)
    {
        $x='';
        $x.= "" . $msg . "\n \n";

        $myFile = file_put_contents('/home/jjeifa/public_html/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
        //$myFile = file_put_contents('/var/www/tenantudev/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
    }

    public function updateLandlordRating($landlordId)
    {
        $this->logMsg("posted landlord id =".$landlordId);
        $this->logMsg("query =SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id IN(SELECT id FROM properties WHERE landlord_id='.$landlordId.') AND rating_id=2");
        $avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id IN(SELECT id FROM properties WHERE landlord_id='.$landlordId.') AND rating_id=2 ');
        $avgRateValue = $avgReviewRatings[0]->rate_value;
        $this->logMsg("avg rate value before ceil =".$avgRateValue);
        $avgRateValueRounded = ceil($avgRateValue);
        $this->logMsg("avg rate value after ceil =".$avgRateValueRounded);
        $rating = 0;


        if($avgRateValueRounded >= 4)
        {
            $rating = 1;
        }
        else
        {
            $rating = 0;
        }

        $updatePropertiesRecomented = DB::table('properties')
            ->where('landlord_id',$landlordId)
            ->update(['recommented_flag'=>$rating]);
        $this->logMsg("updated data =".$updatePropertiesRecomented);
        return $updatePropertiesRecomented;
    }

    ///////////////////////////////////////////////////////////////////////////
    //                          FORTE TRANSACTION HELPERS                   //
    /////////////////////////////////////////////////////////////////////////
    public function getTransIdByLandlordTenantId($landlordTenantId)
    {
        $getAllTransId = ForteRentTransaction::where('landlord_tenant_id', $landlordTenantId)
            ->pluck('transaction_token');
        return $getAllTransId;
    }


    //////////////////////////////////////////////////////////////////////////
    // 							FORTE REST FUNCTIONS 						//
    //////////////////////////////////////////////////////////////////////////
    public function forteUpdate($endpoint, $params, $auth_token, $organization_id)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }

    public function forteGet($endpoint, $auth_token, $organization_id)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }


    public function fortePost($endpoint, $params, $auth_token, $organization_id)
    {
        $organization_id = Config::get('constants.FORTE_TENANTU_ORG_ID');
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }

    public function forteDelete($endpoint, $auth_token)
    {
        $organization_id = Config::get('constants.FORTE_TENANTU_ORG_ID');
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }
    //////////////////////////////////////////////////////////////////////////
    // 						FORTE REST FUNCTIONS END						//
    //////////////////////////////////////////////////////////////////////////

    // Function to verify validity of entered Location ID for Property Management Accounts
    public function verifyLocationId($organization_id, $location_id)
    {
        $base_url               = Config::get('constants.FORTE_BASE_URL');
        $organization_id      = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $location_id          = env('FORTE_TENANTU_MERCHANT_LOC_ID');
        $api_access_id        = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key       = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token           = base64_encode($api_access_id . ':' . $api_secure_key);

        // This Test Endpoint will simply try to get any transactions under his specific location ID under the Landlord's Organization ID provided by Forte
        $test_endpoint = '';
    }



    ///////////////////////////////////
    // Landlord Subscription Helpers //
    ///////////////////////////////////

    // Grab the customer ID of the Landlord that subscribed
    public function getLandlordCustomerId($landlordId)
    {
        $customer_id = DB::table('forte_landlord_subscription_pay_settings')
            ->where('landlord_id', $landlordId)
            ->value('customer_id');
        if($customer_id == '')
        {
            $current_customer_id = null;
        }
        else
        {
            try{
                $current_customer_id = Crypt::decrypt($customer_id);
            } catch (DecryptException $e){
                echo "Can't get the Id";
            }
        }
        return $current_customer_id;
    }

    public function getLandlordPaymentId($landlordId)
    {
        $payment_id = DB::table('forte_landlord_subscription_pay_settings')
            ->where('landlord_id', $landlordId)
            ->value('payment_id');
        if($payment_id == '')
        {
            $current_payment_id = $payment_id;
        }
        else
        {
            try{
                $current_payment_id = Crypt::decrypt($payment_id);
            } catch (DecryptException $e){
                //
            }
        }
        return $current_payment_id;
    }

    public function getSubscriptionPlan($subscription_plan_id)
    {
         $subscription_plan = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)
                                    ->first();
        return json_encode($subscription_plan);
    }

    public function getLandlordSubscriptionPlan($landlordId)
    {
        $landlord_subscription_plan = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->first();

        return json_encode($landlord_subscription_plan);
    }

    public function getSubscriptionPlanInitialBasePrice($subscription_plan_id)
    {
        $subscription_plan_id = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)
                                    ->value('initial_base_price');
        return floatval($subscription_plan_id);
    }

    public function getSubscriptionPlanPropertyPrice($subscription_plan_id)
    {
        $subscription_property_price = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('price_per_property');
        return floatval($subscription_property_price);
    }

    public function getSubscriptionPlanAdditionalBasePrice($subscription_plan_id)
    {
        $subscription_additional_accounts_price = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('additional_accounts_base_price');
        return floatval($subscription_additional_accounts_price);
    }

    public function getSubscriptionPlanAccountsCountPreAdditionalPrice($subscription_plan_id)
    {
        $subscription_num_accounts_pre_new_price = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('num_management_accounts_pre_new_base_price');
        return floatval($subscription_num_accounts_pre_new_price);
    }

    public function getSubscriptionPlanPropertyMax($subscription_plan_id)
    {
        $subscription_property_max = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('property_max');
        return $subscription_property_max;
    }

    public function getSubscriptionManagementAccountMax($subscription_plan_id)
    {
        $subscription_management_account_max = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('management_account_max');
        return $subscription_management_account_max;
    }

    public function getSubscriptionForteFeeId($subscription_plan_id)
    {
        $subscription_forte_fee_id = DB::table('subscription_plans')->where('id', '=', $subscription_plan_id)->value('forte_fee_id');
        return $subscription_forte_fee_id;
    }

    // Get Landlord's Subscription FORTE Schedule ID/Token, NOT THE ID OF THE SCHEDULE TABLE
    public function getLandlordSubscriptionScheduleId($landlordId)
    {
        $landlordSubscriptionScheduleId = DB::table('landlord_subscription_schedules')
                                            ->join('landlord_subscription', function ($join) use ($landlordId) {
                                                $join->on('landlord_subscription_schedules.landlord_subscription_id', '=', 'landlord_subscription.id')
                                                    ->where('landlord_subscription.landlord_id', '=', $landlordId);
                                                })
                                            ->value('landlord_subscription_schedule_id');
        if($landlordSubscriptionScheduleId != null)
        {
            $landlordSubscriptionScheduleId = Crypt::decrypt($landlordSubscriptionScheduleId);
        }
        else
        {
            $landlordSubscriptionScheduleId = null;
        }

        return $landlordSubscriptionScheduleId;
    }

    public function getLandlordSubscriptionPropertyCount($landlordId)
    {
        $landlordSubscriptionPropertyCount = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('property_count');
        return intval($landlordSubscriptionPropertyCount);
    }

    public function getLandlordSubscriptionManagementAccountCount($landlordId)
    {
        $landlordSubscriptionManagementAccountCount = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('management_group_count');
        return intval($landlordSubscriptionManagementAccountCount);
    }

    public function getLandlordSubscriptionId($landlordId)
    {
        $landlordSubscriptionId = DB::table('landlord_subscription')->where('landlord_id', '=', $landlordId)->value('id');
        return $landlordSubscriptionId;
    }

    public function updateLandlordSubscriptionPropertyCount($landlordId)
    {
        $property_count = $this->getTotalCurrentManagedPropertyCount($landlordId);
        DB::table('landlord_subscription')
                    ->where('landlord_id', $landlordId)
                    ->update(array('property_count'=>$property_count));
    }

    public function updateLandlordSubscriptionManagementAccountCount($landlordId)
    {
        $management_account_count = $this->getLandlordSubscriptionManagementAccountCount($landlordId);
        DB::table('landlord_subscription')
                    ->where('landlord_id', $landlordId)
                    ->update(array('management_group_count'=>$management_account_count));
    }

    /////////////////////////////////////
    // Management Account Helpers ///////
    /////////////////////////////////////
    public function getTotalCurrentManagedPropertyCount($landlordId)
    {
        $currentPropertyCount = DB::table('landlord_property_management_accounts')->where('landlord_id', $landlordId)
            ->sum('property_count');
        return intval($currentPropertyCount);
    }

    public function getTotalApprovedManagementAccountsCount($landlordId)
    {
        $totalApprovedAccounts = DB::table('forte_landlord_aplications')->where('landlord_id', $landlordId)
                                ->where('status', '=', 'approved')
                                ->count();
        return intval($totalApprovedAccounts);
    }

    public function getTotalLandlordApplications($landlordId)
    {
        $totalApplications = DB::table('forte_landlord_aplications')->where('landlord_id', $landlordId)
                                ->count();
        return intval($totalApplications);
    }

    
}
