<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\Mysearch;
use App\Http\Models\Amenity;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class MysearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //this method is for the fetching the data for the lodded tenants search field

    public function getMySearchDetails()
    {

		$loggedTenant 		= Auth::user('tenant');

		$mySavedSearchDetails 	= $this->getMySavedSearchData($loggedTenant->id);
		//print_r($mySavedSearchDetails);exit;
		$latestProperties 		= $this->getLatestProperties();

		//dd($mySavedSearchDetails);
		$searchDataArr=array();
		if(count($mySavedSearchDetails)<= 0 )
		{
			$searchDataArr['landlord_id']   = '';
			$searchDataArr['school_id'] 	= '';
			$searchDataArr['bedroomId'] 	= 'any';
			$searchDataArr['distance'] 		= 10;
			$searchDataArr['priceFrom'] 	= 0;
			$searchDataArr['priceTo'] 		= 10000;
			$searchDataArr['search'] 		= '';
			$searchDataArr['aminities'] 	= '';
			$searchDataArr['sortKey'] 		= 'all';
		}
		else
		{
			$searchDataArr['landlord_id']   = $mySavedSearchDetails->landlord_id;
			$searchDataArr['school_id'] 	= $mySavedSearchDetails->school_id;
			$searchDataArr['bedroomId'] 	= $mySavedSearchDetails->bedrooms;
			$searchDataArr['distance'] 		= $mySavedSearchDetails->distance;
			$searchDataArr['priceFrom'] 	= $mySavedSearchDetails->price_form;
			$searchDataArr['priceTo'] 		= $mySavedSearchDetails->price_to;
			$searchDataArr['search'] 		= $mySavedSearchDetails->search;
			$searchDataArr['aminities'] 	= $mySavedSearchDetails->aminities;
			$searchDataArr['sortKey'] 		= $mySavedSearchDetails->sortkey;
		}
		//print_r($searchDataArr);exit;
		$landLords 			= $this->getAllLandlords();
		//print_r($landLords);exit;
		$school  			= $this->getSchoolDetails();
		$aminity  			= $this->getAminityDetails();
		//dd($aminity);

		return view('mysearch.search', [
            'pageTitle'   			=> 'Search',
            'pageHeading' 			=> 'Search',
            'pageMenuName'			=> 'mysearch',
            'loggedTenant' 			=> $loggedTenant,
            'landlords' 			=> $landLords,
            'schools'				=> $school,
            'aminities'				=> $aminity,
            'savedSearchDetails' 	=> $searchDataArr,
            'latestProperties'		=> $latestProperties,


        ]);
	}

	 private function getMySavedSearchData($tenantId)
	 {
		 //echo $tenantId;exit;
		 $mysearch = Mysearch::where('tenant_id',$tenantId)
								->get()->first();
		 return $mysearch;
	 }

	 private function getAminityDetails()
	 {
		 $aminity = array();
		 $aminity  = DB::table('amenities')->where('status',1)->orderBy('amenity_name', 'asc')->lists('amenity_name', 'amenity_name');
		 return $aminity;
	 }

	 private function getLatestProperties()
	 {
		 $latestProperty = Property::where('status',1)
								-> select('property_name')
								-> addSelect('id')
								-> addSelect('slug')
								-> addSelect('created_at')
								-> orderBy('created_at', 'desc')
								-> take(4)
								-> get();
		 return $latestProperty;
	 }


}
