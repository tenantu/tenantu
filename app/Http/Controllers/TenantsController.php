<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\Thread;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\Mysearch;
use App\Http\Models\PropertyEnquiry;
use App\Http\Models\PropertyExpressInterest;
use App\Http\Models\PropertyRating;
use App\Http\Models\PropertyReview;
use App\Http\Models\PropertySearch;
use App\Http\Models\PropertyPhysical;
use App\Http\Models\PropertyMeta;
use App\Http\Models\RatingUser;
use App\Http\Models\TenantMaintenanceRequest;
use App\Http\Models\MaintenanceAvailableTimes;
use App\Http\Models\ApplicationQuestionTenantResponse;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Crypt;
use Response;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class TenantsController extends Controller
{

	public function getLogin(){
		return view('tenants.login');
	}

	public function postLogin( Request $request ){

		$rules = array(
			'email'            => 'required|email|',     // required and must be unique in the ducks table
			'password'         => 'required'
		);


		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			 //echo "121";exit;
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}else{

			if (Auth::attempt("tenant", ['email' => $request->input('email'), 'password' => $request->input('password'),'is_active'=>1])){
				$dataWithEmail = Tenant::where('email', '=', $request->input('email'))->first();
				return Redirect::route('tenants.dashboard');
			}
	    else{
				return redirect('tenants/login')->with('errors', 'please check the login credentials');
			}
		}
	}

	public function logout()
	{

		Auth::logout('tenant');
		return redirect('/login');
	}

	private function tenantHasProperty($tenantId)
	{
		$tenantPropertyId = LandlordTenant::where('tenant_id', '=', $tenantId)->first();
		if ($tenantPropertyId === null)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function createMaintenanceRequest(Request $request)
	{

		$loggedTenant = Auth::user('tenant');
		$loggedTenantId = $loggedTenant->id;
		$tenantProfile 	= $this->getTenantProfile($loggedTenantId);
		$imageUrl       = asset('public/uploads/tenants/');
		$tenantAssociatedWithLandlord = $this->isTenantAssociatedWithLandlord($loggedTenantId);


		if ($this->getTenantCurrentPropertyId($loggedTenantId))
		{
			return view('tenants.createMaintenanceRequest', [
				'pageTitle'   	=> 'Maintenance',
				'pageHeading' => 'Maintenance',
				'pageMenuName'	=> 'createMaintenanceRequest',
				'profile' 		=> $tenantProfile,
				'imageUrl' 		=> $imageUrl,
				'isTenantAssociatedWithLandlord'=> $tenantAssociatedWithLandlord
			]);
		}
		else
		{
			Session::flash('warningMessage', "You must be connected to a landlord and property to submit a maintenance request!");
			return redirect()->route('tenants.getMyLandlords');
		}
	}

	private function createCarbonFromDateTimePicker($timeString)
	{

		$dateArr = date_parse($timeString);
	//	$dump = var_dump($dateArr);
		/*echo '<pre>';
echo $dump;
echo '<br>';
echo '</pre>';
return null;
*/
		if ($dateArr['minute'] != false) {
            $carbonDateObject = Carbon::create(
                $dateArr['year'], $dateArr['month'], $dateArr['day'],
                $dateArr['hour'], $dateArr['minute']
            );
            return $carbonDateObject;
        }
	//$dateArr == bool(false)
        else
        {
            $carbonDateObject = Carbon::create(
                $dateArr['year'], $dateArr['month'], $dateArr['day']
            );
            return $carbonDateObject;
        }
	}

	public function postMaintenanceRequest(Request $request)
	{
		$rules = array(
			'maintType' => 'required',
			'issueDescription' => 'required',
			'dateTimeEnc' => 'required',
			'dateTimeAvailable1' => 'required',
			'dateTimeAvailable2' => 'required',
			'q1' => 'required',
			'q2' => 'required',
			'q3' => 'required'
		);

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		else
		{
			$loggedTenant           = Auth::user('tenant');
			$loggedTenantId         = $loggedTenant->id;

			$q1 = $request->q1;
			$q2 = $request->q2;
			$q3 = $request->q3;

			$dateEnc = $this->createCarbonFromDateTimePicker($request->dateTimeEnc);
			$dateAvailable1 = $this->createCarbonFromDateTimePicker($request->dateTimeAvailable1);
			$dateAvailable2 = $this->createCarbonFromDateTimePicker($request->dateTimeAvailable2);

			$input = array();
			$input['severity'] = $this->calculateSeverity($q1, $q2, $q3);
			//0 -> plumbing, 1 -> air conditioning, 2 -> electric, 3 -> other
			$input['issue_type_id'] = $request->maintType;
			$input['issue_desc'] = $request->issueDescription;
			$input['date_encountered'] = $request->dateTimeEnc;
			$input['property_id'] = $this->getTenantCurrentPropertyId($loggedTenantId);
			$input['status'] = 0;
			$input['date_encountered'] = $dateEnc;

			$maintenanceRequest = TenantMaintenanceRequest::create($input);
			echo 'ID of the maintenacne request just added: ';

			$timesAvailable1DbEntry = array();
			$timesAvailable1DbEntry['maintenance_request_id'] = $maintenanceRequest->id;
			$timesAvailable1DbEntry['time_available'] = $dateAvailable1;
			$timesAvailable1DbEntry['agree_status'] = 0;

			$timesAvailable2DbEntry = array();
			$timesAvailable2DbEntry['maintenance_request_id'] = $maintenanceRequest->id;
			$timesAvailable2DbEntry['time_available'] = $dateAvailable2;
			$timesAvailable2DbEntry['agree_status'] = 0;

			$timesAvailable1 = MaintenanceAvailableTimes::create($timesAvailable1DbEntry);
			$timesAvailable2 = MaintenanceAvailableTimes::create($timesAvailable2DbEntry);

			$tenantFullName = $loggedTenant->firstname." ".$loggedTenant->lastname;
			$tenantProperty = $this->getTenantCurrentProperty($loggedTenantId);
			$maintenanceIssueNum = $request->maintType;
			if($maintenanceIssueNum == 0){
			$maintenanceIssueType = "Plumbing";
			}
			else if($maintenanceIssueNum == 1){
			$maintenanceIssueType = "Air Conditioning";
			}
			else if($maintenanceIssueNum == 2){
			$maintenanceIssueType = "Electric";
			}
			else if($maintenanceIssueNum == 3){
			$maintenanceIssueType = "Other";
			}
			$landlordEmail = $this->getTenantCurrentLandlordEmail($loggedTenantId);
			Mail::send('emails.maintenance-request-created',['tenantFullName'=>$tenantFullName,'tenantProperty'=>$tenantProperty,'maintenanceIssueType'=>$maintenanceIssueType],
                                                                                function($message) use ($landlordEmail) {

                                        $message->to($landlordEmail)
                                                        ->subject("Your tenant has submitted a maintenance request on TenantU!");
                 });

			// $successMessage = "Good work guy";
			// Session::flash('success', $successMessage);
	    return redirect()->route('tenants.dashboard');
		}
	}


	private function calculateSeverity($q1, $q2, $q3)
	{
		//return 0 for low severity, 1 for medium severity, 2 for high severity
				return 1;
	}


	public function dashboard()
	{

		$loggedTenant = Auth::user('tenant');
		$loggedTenantId = $loggedTenant->id;
		$tenantProfile 	= $this->getTenantProfile($loggedTenantId);
		$imageUrl       = asset('public/uploads/tenants/');
		//To get the general queries  fom message table  and threds table having flag 0
		$generalMessages = $this->getGeneralQueries($loggedTenantId);
		$issueMessages = $this->getIssuedQueries($loggedTenantId);
		$tenantAssociatedWithLandlord = $this->isTenantAssociatedWithLandlord($loggedTenantId);

		//dd($request);
		 return view('tenants.dashboard', [
            'pageTitle'   	=> 'Dashboard',
            'pageHeading' 	=> 'Dashboard',
            'pageMenuName'	=> 'dashboard',
            'profile' 		=> $tenantProfile,
            'imageUrl' 		=> $imageUrl,
            'generalMessages'=> $generalMessages,
            'issueMessages'=> $issueMessages,
            'isTenantAssociatedWithLandlord'=> $tenantAssociatedWithLandlord,
						'tenantInManagementGroup' => $this->tenantInManagementGroup($loggedTenantId)
        ]);


	}

	public function tenantInManagementGroup($tenantId){
		$tenantApplicationGroupRecords = ApplicationQuestionTenantResponse::where('tenant_id', $tenantId)->first();
		if (!$tenantApplicationGroupRecords) {
			return false;
		}
		return true;
	}

	public function getChangePassword(Request $request)
	{
		return view('tenants.changepassword',[
			'pageTitle'   	=> 'ChangePassword',
			'pageMenuName'	=> 'changepassword',

		]);
	}

	public function postChangePassword(Request $request)
	{
		$rules = array(
			'current_password' => 'required',
			'password'         => 'required|confirmed|min:6',
		);

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}

        $userUpdate = $request->all();
        $value      = $userUpdate['current_password'];
        $tenant     = Auth::User('tenant');
       // print_r($tenant->password);exit;
        if (Hash::check($value, $tenant->password))
        {
			$userpassword = Hash::make($userUpdate['password']);
			$tenant       = DB::table('tenants')
                ->where('id', $tenant->id)
                ->update(['password' => $userpassword]);
            return redirect()->route('tenants.dashboard')->with('message-success', 'Your password has been updated.');

		}
	}



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $tenantsQuery = DB::table('tenants');

        if ($request->has('search')) {
            $search = $request->input('search');
            $tenantsQuery->where('firstname', 'like', '%' . $request->input('search') . '%');
        }
        $tenantsQuery->orderBy($sortby, $order);
        $tenants = $tenantsQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('tenants.index', ['pageTitle' => 'Tenants',
            'pageHeading'                                => 'Tenants',
            'pageDesc'                                   => 'Manage Tenants here',
            'tenants'                                    => $tenants,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

	   $school = $this->getSchoolDetails();
       return view('tenants.create', ['pageTitle' => 'Create Tenant',
            'pageHeading'                                 => 'Create Tenants',
            'pageDesc'                                    => 'Create Tenants here',
            'school'                                        => $school,
        ]);
    }

    //these func is used for getting the school
     public function  getSchoolDetails()
     {
		 $school = DB::table('schools')->where('is_active',1)->lists('schoolname', 'id');
		 if(!empty($school))
		 {
			 return $school;
		 }else
		 {
			 return false;
		 }
	 }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
         $rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'school'              	=> 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'email'                 => 'required|email|unique:tenants',

        ];
        $input     = $request->except('image');
        $imagePath = Config::get('constants.USER_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {

			$input['firstname'] = $request->firstname;
			$input['lastname'] = $request->lastname;
			$input['school_id'] = $request->school;
			$input['email'] = $request->email;
            $input['password'] = bcrypt($input['password']);

            $registeredEmail = $this->isRegistered($input['email']);
			if(count($registeredEmail) > 0)
			{
				return redirect()->route('tenants.create')->with('message-error', 'This email is already registered !');
			}
            $tenant = Tenant::create($input);

            if ($request->file('image')) {
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                $imagePath = $imagePath . $tenant['id'];
                if (!File::exists($imagePath)) {
                    // path does not exist
                    File::makeDirectory($imagePath, $mode = 0777, true, true);
                    File::copy(Config::get('constants.HTACCESS_FILE'), $imagePath . DS . '.htaccess');
                    File::makeDirectory($imagePath . '/60_x_60', $mode = 0777, true, true);

                }

                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(60, 60)->save($imagePath . '/60_x_60/' . $filename);
                // $userid = $user->id;
                $tenant = Tenant::find($tenant->id);
                $tenant->update(['image' => $filename]);

            }
            return redirect()->route('tenants.index')->with('message', 'User Created.');
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    private function isRegistered($email)
    {
		$users = DB::select('SELECT email
							FROM tenants
							WHERE email ="'.$email.'"
							UNION
							SELECT email
							FROM landlords
							WHERE email = "'.$email.'"');
		return $users;
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
        //$imageUrl           = Config::get('constants.USER_IMAGE_URL');
        $imageUrl       	= asset('public/uploads/tenants/');
        $noImageUrl			= asset('public/uploads/noimage/');
        $tenant             = Tenant::find($id);
        $school 			= $this->getSchoolDetails();

        list($tenant['year'], $tenant['month'], $tenant['day']) = explode('-', $tenant['dob']);
        $locations         = [];
        if ($tenant->location) {
            $locations = explode('//', $tenant->location);
        }

		if($tenant->image!='')
		{
			$profileImage=$imageUrl."/".$tenant->image;
		}
		else
		{
			$profileImage=$noImageUrl."/tenant-blank.png";
		}

        return view('tenants.edit', ['pageTitle' => 'Edit Tenant',
            'pageHeading'                               => 'Edit Tenant',
            'pageDesc'                                  => 'Edit Tenant here',
            'tenant'                                    => $tenant,
            'imageUrl'                                  => $imageUrl,
            'school'                                    => $school,
            'profileImage'								=> $profileImage,
            'locations'                                 => $locations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $rules = [
            'firstname'  => 'required',
            'lastname'   => 'required',
            'school'   	 => 'required',
            'email'      => 'required|email',
        ];

        $messages = [
            'firstname.required'  	=> 'First name is required.',
            'lastname.required'  	=> 'Last name is required.',
            'school.required' 		=> 'Please select the school',
            'email.required' 		=> 'A valid email is required.',
        ];



        $input     = $request->all();
        //dd($input);
        $imagePath = Config::get('constants.USER_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {

            $tenant         = Tenant::find($id);

            $tenant->update($input);

            $tenantId = Tenant::find($tenant->id);

            if ($tenantId['id']) {
                 return redirect()->route('tenants.index')->with('message-success', 'Profile is updated.');
            }

        }
        else
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
        $tenant = Tenant::find($id);
        $tenant->delete();
        /*Deleteing the associated tables
         * assign_tenant_docs
         * lanlord_tenants
         * messages
         * message_threads
         * mysearches
         * property_enquiry
         * property_express_interest
         * property_ratings
         * property_review
         * property_search
         * property_viewed
         * reting_user
         *
         * */
        $landlordTenant 		=  LandlordTenant::where('tenant_id',$id)
									->delete();
		$message 				= Message::where('tenant_id',$id)
									->delete();
		$messageThread 			= MessageThread::where('tenant_id',$id)
									->delete();
		$mySearch 				= Mysearch::where('tenant_id',$id)
										->delete();
		$propertyEnquiry		= PropertyEnquiry::where('tenant_id',$id)
										->delete();
		$propertyExpressIneterest = PropertyExpressInterest::where('tenant_id',$id)
										->delete();
		$propertyRatings 		= PropertyRating::where('tenant_id',$id)
										->delete();

		$propertyReview 		= PropertyReview::where('tenant_id',$id)
										->delete();
		$propertySearch			= PropertySearch::where('tenant_id',$id)
										->delete();
		$propertyViewedArray	= PropertyViewed::where('tenant_id',$id)
										->delete();
		$ratingUser				= RatingUser::where('tenant_id',$id)
										->delete();

        return redirect()->route('tenants.index')->with('message', 'Tenantu Deleted.');
    }

    public function changeStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
        $tenant = Tenant::find($id);
        if ($tenant->is_active) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }

        $tenant->update($data);
        return redirect()->route('tenants.index')->with('message', 'Tenants Status Updated');
    }

    public function getForgotPassword(){
		$pageTitle = 'Forgot Password';
        //return view('tenants.password')->with(compact('pageTitle'));
        return view('tenants.forgotpassword')->with(compact('pageTitle'));
	}
	public function postForgotPassword(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
        $postedEmail = $request->email;
        $dataWithEmail = Tenant::where('email', '=', $postedEmail)->first();
        //$dataWithEmail = DB::table('tenants')->where('email',$postedEmail)->first();
        if(!empty($dataWithEmail))
        {
			$currentDate = Carbon::now();
			$key = $currentDate->getTimestamp();
			$password_token = hash_hmac('sha256', str_random(40), $key);
			$originalToken = $key."_".$password_token;
			$dataWithEmail->password_token = $originalToken;
			$dataWithEmail->save();
			$emailFromAddress = Config::get('constants.FORGOT_PASSWORD_EMAIL_FROM');
			$emailFromName = Config::get('constants.FORGOT_PASSWORD_EMAIL_FROM_NAME');


			Mail::send('emails.passwordTenants',['token' => $originalToken], function($message) use ($dataWithEmail){

					$message->from('sooraj@gmail.com', 'sooraj');
					$message->to($dataWithEmail->email, $dataWithEmail->name)
							->subject('reset your password');
             });
			return redirect()->route('tenants.forgotpassword')->with('message', 'Mail sent  please check your email');
		}else{
			return redirect()->back()->withErrors('message', 'not a valid email');
		}


    }

    public function getResetPassword($token){
		if($token!=''){
		$pageTitle = 'Password reset';
        return view('tenants.resetpassword')->with(compact(array('pageTitle','token')));
		}else{
			return redirect()->back()->withErrors('message', 'Token must be needed');
		}
	}

	public function postResetPassword(Request $request)
	{
		$this->validate($request, [
            'token' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $credentials = $request->only(
            'password', 'password_confirmation', 'token'
        );

		$updatedPassword = DB::table('tenants')
						->where('password_token',$request->token)
						->update(['password' =>bcrypt($request->password)]);

		$updatedPassword = DB::table('tenants')
						->where('password_token',$request->token)
						->update(['password_token' =>'']);

		return redirect('tenants/login');
	}

	public function register(){
		return view('tenants.register');
	}

	public function signup(Request $request){

		$rules = [
            'name'                  => 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'email'                 => 'required|email|unique:tenants',
            'image'                 => 'mimes:jpeg,jpg,gif,png',

        ];

		$input     = $request->except('image');
        $imagePath = Config::get('constants.USER_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules);

        if (!$validator->fails()) {
			$input['name'] = $request->name;
			$input['password'] = bcrypt($request->password);
			$input['email'] = $request->email;
			$input['gender'] = $request->gender;
			$input['dob'] = $request->year. "-" . $request->month . "-" . $request->day;
			$input['location'] =$request->location;

			$tenant = Tenant::create($input);

			if ($request->file('image')) {
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                $imagePath = $imagePath . $tenant['id'];
                if (!File::exists($imagePath)) {
                    // path does not exist
                    File::makeDirectory($imagePath, $mode = 0777, true, true);
                    File::copy(Config::get('constants.HTACCESS_FILE'), $imagePath . DS . '.htaccess');
                    File::makeDirectory($imagePath . '/60_x_60', $mode = 0777, true, true);

                }

                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(60, 60)->save($imagePath . '/60_x_60/' . $filename);
                // $userid = $user->id;
                $tenant = Tenant::find($tenant->id);
                $tenant->update(['image' => $filename]);
            }
             return redirect()->route('tenants.register')->with('message', 'Tenant Created successfully.');
		}else{
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}

	}

	//this func is used to show the profile
	public function getProfile()
	{
		$loggedTenant = Auth::user('tenant');
		$imageUrl       = asset('public/uploads/tenants/');
		$noImageUrl       = asset('public/uploads/noimage/');

		$tenantProfile = $this->getTenantProfile($loggedTenant->id);
		$school = $this->getTenantSchool($tenantProfile->school_id);
		//print_r($school[0]['schoolname']);exit;
		$gender = $this->getGender($tenantProfile->gender);
		$dob = $this->getDob($tenantProfile->dob);
		$state = $this->getState($tenantProfile->state);
		$city = $this->getCity($tenantProfile->city);
		$language = $this->getLanguage($tenantProfile->language);

		if($tenantProfile->image!='')
		{
			$profileImage=$imageUrl."/".$tenantProfile->image;
		}
		else
		{
			$profileImage=$noImageUrl."/tenant-blank.png";
		}


		 return view('tenants.profile', [
            'pageTitle'   	=> 'Profile',
            'pageHeading' 	=> 'Profile',
            'imageUrl' 		=> $imageUrl,
            'profile'  		=> $tenantProfile,
            'schoolName'  	=> $school[0]->schoolname,
            'birthdate'		=> $dob,
            'gender'		=> $gender,
            'state'			=> $state,
            'city'			=> $city,
            'language'		=> $language,
            'pageMenuName'	=> 'profile',
            'profileImage'	=> $profileImage,
        ]);
	}

	//this fun is used for the school name

	public function getTenantSchool($schoolId)
	{
		$schoolName 	= School::where('id',$schoolId)->select('schoolname')->get();
		return $schoolName;
	}

	public function getEditProfile()
	{
		$imageUrl       = asset('public/uploads/tenants/');
		$noImageUrl     = asset('public/uploads/noimage/');
		$loggedTenant 	= Auth::user('tenant');
		$tenantProfile 	= $this->getTenantProfile($loggedTenant->id);
		//print_r($tenantProfile);exit;
		//$profileDob =  $tenantProfile->dob->format('m/d/Y');
		//print_r($profileDob);exit;
		$schools 		= $this->getSchoolDetails();

		if($tenantProfile->image!='')
		{
			$profileImage=$imageUrl."/".$tenantProfile->image;
		}
		else
		{
			$profileImage=$noImageUrl."/tenant-blank.png";
		}

		return view('tenants.edit_profile', [
            'pageTitle'   	=> 'EditProfile',
            'pageHeading' 	=> 'EditProfile',
            'imageUrl'      => $imageUrl,
            'profile' 		=> $tenantProfile,
            'schools'  		=> $schools,
            'pageMenuName'	=> 'dashboard',
            'profileImage'  => $profileImage  ,

        ]);
	}

	public function postEditProfile(Request $request)
	{

		$loggedTenant = Auth::user('tenant');
		$rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'phone' 				=> 'required',
			'school_id' 			=> 'required',
        ];

         $messages = [
            'firstname.required'  	=> 'Firstname is required.',
            'lastname.required'  	=> 'Lastname is required.',
            'phone.required'  		=> 'Phone is required.',
            'school_id.required' 	=> 'School is required'
        ];
        $validator = Validator::make($request->all(), $rules,$messages);
        if (!$validator->fails())
        {
			//dd($request->dob);
			$dateOfBirth = date('Y-m-d', strtotime(str_replace('-', '/', $request->dob)));
			$input['firstname'] = $request->firstname;
			$input['lastname'] 	= $request->lastname;
			$input['location'] 	= $request->location;
			$input['phone'] 	= $request->phone;
			$input['about'] 	= $request->about;
			$input['school_id'] = $request->school_id;
			$input['website'] 	= $request->website;
			$input['state'] 	= $request->state;
			$input['address'] 	= $request->address;
			$input['language'] 	= $request->language;
			$input['city'] 		= $request->city;
			$input['gender'] 	= $request->gender;
			$input['dob'] 		= $dateOfBirth;
			//this code is used to upload the image
			if($request->file('image'))
			{
				$imagePath = Config::get('constants.TENANT_IMAGE_PATH');

				$image     = $request->file('image');
				$extension = $image->getClientOriginalExtension();
				$filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
				$filename  = str_slug($filename, "-") . '.' . $extension;
				//$imagePath = $imagePath . $loggedTenant->id;


                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(144, 144)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
			}


			$updateTenantDetails = Tenant::where('id', '=', $loggedTenant->id)->update($input);

			//end of the image upload code
			return redirect()->route('tenants.editprofile')->with('message-success', 'Details are updated.');
		}
		else{
			return redirect()->route('tenants.editprofile')->withErrors($validator->errors())->withInput();
		}
	}

	public function postEditProfileImage(Request $request)
	{
		$loggedTenant = Auth::user('tenant');

		if($request->file('image'))
			{
				$imagePath = Config::get('constants.TENANT_IMAGE_PATH');

				$image     = $request->file('image');
				$extension = $image->getClientOriginalExtension();
				$filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
				$filename  = str_slug($filename, "-") . '.' . $extension;
				//$imagePath = $imagePath . $loggedTenant->id;


                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(144, 144)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
                $updateTenantDetails = Tenant::where('id', '=', $loggedTenant->id)->update($input);

				//end of the image upload code
				return redirect()->route('tenants.editprofile')->with('message-success', 'Profile image has been updated.');
			}

	}

	//this function is used  to get the contact details
	public function getMyContact()
	{

		$loggedTenant = Auth::user('tenant');
		//this method calling is for the landlords frm msg thread
		$landlords = $this->getContactedLandlord($loggedTenant->id);
		//print_r($landlords);exit;
		foreach($landlords as $landlord) {
			$landlord->latestMsgThreadId->message_thread_id = Crypt::encrypt($landlord->latestMsgThreadId->message_thread_id);
		}
		//dd($landlords);
		return view('tenants.contacts', [
            'pageTitle'   		=> 'Conversations',
            'pageHeading' 		=> 'Conversations',
            'pageMenuName'		=> 'mycontact',
            'landlordsObj' 		=> $landlords,

        ]);
	}

	//this methosd is for the fetch the landlords frm msg thread with corresponding tenant
	public function getContactedLandlord($tenantId)
	{
		 $landlords = DB::select('SELECT distinct(m.landlord_id),l.firstname,l.lastname,l.slug,l.about,l.city FROM message_threads m join landlords l on m.landlord_id=l.id where m.tenant_id='.$tenantId.' and l.is_active=1');
		 foreach($landlords as $landlord)
		 {
			$landlordId = $landlord->landlord_id;
			$landlord->latestMsgThreadId = $this->getLatestMessageThreadId($tenantId,$landlordId);
		 }
         return  $landlords;
	}

	//this method is for fetch the message frn table msg
	public function getLatestMessageThreadId($tenantId,$landlordId)
	{
		 $landlords = DB::select('SELECT message_thread_id FROM messages WHERE tenant_id ='.$tenantId.' AND landlord_id='.$landlordId.' ORDER BY created_at DESC LIMIT 1 ');
         return  $landlords[0];
	}

	//this function is for viwing the message composer

	public function getMessage($encryptedLandlordId,$propertyId=null)
	{
		$id = Crypt::decrypt($encryptedLandlordId);
		$loggedTenant 	= Auth::user('tenant');

		$name 			= $this->getTenantName($loggedTenant);

		$messageThreads = $this->getMyMessageThreads($id,$loggedTenant->id);
		//add threads for unit/all tenant messages
        $unitThreads = $this->getMyUnitThreads($loggedTenant->id);
		$landlordName = $this->getLandLordName($id);

			$propertyCount = $this->getPropertyCount($id);

			$landLordPropertyList = $this->getPropertyList($id);
			//print_r($landLordPropertyList);exit;
			if(empty($landLordPropertyList))
			{
				return redirect('/404');
			}

			$threads = $this->getTenantThread($loggedTenant->id,$id,$propertyId);
			//print_r($threads);exit;
			$propertyList= array();
			$propertyKey = array();
			foreach($landLordPropertyList as $key=>$val)
			{
				$propertyList[$val['id']]=$val['property_name'];
				$propertyKey=$val['id'];
			}
			//print_r($propertyKey);exit;
			foreach($threads as $thread)
			{
				$threadDropdown[$thread['id']]	=	$thread['thread_name'];
			}

			$threadsFlag = $this->getThreadsForFlag();

			foreach($threadsFlag as $thread)
			{
				$threadWithFlag[] = $thread['id'];
			}
			$jsonthreadWithFlag = json_encode(array("threads"=>$threadWithFlag));
			if($this->getTenantCurrentPropertyId($loggedTenant->id)){
			$propertyId = $this->getTenantCurrentPropertyId($loggedTenant->id);
			}
			//print_r($threadFlag);exit;
			foreach($messageThreads as $thread) {
				//dd($thread);
				$thread->id = Crypt::encrypt($thread->id);
			}
			$id = Crypt::encrypt($id);
			return view('tenants.message', [
					'pageTitle'   			=> 'Message',
					'profile'				=> $loggedTenant,
					'landlordId'			=> $id,
					'landlordName'			=> $landlordName,
					'tenantName' 			=> $name,
					'propertyList'			=> $propertyList,
					'propertyKey'			=> $propertyKey,
					'messageThreadsObj'		=> $messageThreads,
					'pageMenuName'			=> 'mycontact',
					'messageFlag'			=> 'compose',
					'threads'				=> $threadDropdown,
					'threadWithFlag'		=> $jsonthreadWithFlag,
					'messageThreadId'		=> '',//msg threadId is used to select threadName but it is not used in compose view
					'selectedPropertyId'	=> $propertyId


				]);

	}

	//this function is used for the  saving the message to message and message thread tables
	public function postMessage(Request $request)
	{

		$rules = array(
			'property'		   => 'required',
			'messageType'      => 'required',
			'message_content'  => 'required',

		);
		$landlordId = Crypt::decrypt($request->landlord_id);

		$threadsFlag = $this->getThreadsForFlag();
		foreach($threadsFlag as $thread)
		{
			$threadWithFlag[] = $thread['id'];
		}
		if (in_array($request->messageType, $threadWithFlag))
		{
			$rules['duration'] = 'required|numeric';
		}

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails())
		{
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		else
		{
			//$input['message_thread_id'] = $request->messageType;
			$input['message'] 			= $request->message_content;
			$input['tenant_id'] 		= $request->tenant_id;
			$input['sender'] 			= $request->sender;
			$input['landlord_id'] 		= $landlordId;

			$inputArr['thread_id'] 		= $request->messageType;
			$inputArr['property_id'] 	= $request->property;
			$inputArr['landlord_id'] 	= $landlordId;
			$inputArr['tenant_id'] 		= $request->tenant_id;
			$inputArr['duration'] 		= $request->duration;
            $tenants   = Tenant::find( $request->tenant_id );
            $landlords = Landlord::find( $landlordId );
            $landLordName = $landlords->firstname.' '.$landlords->lastname;
            $tenantName   = $tenants->firstname.' '.$tenants->lastname;
			$landLordEmail = $landlords->email;
			$messageThread = MessageThread::create($inputArr);
			if(isset($messageThread))
			{
				$message = new Message;
				$message->message_thread_id = $messageThread->id;
				$message->message 			= $input['message'];
				$message->landlord_id 		= $landlordId;
				$message->tenant_id 		= $input['tenant_id'];
				$message->sender 			= $input['sender'];

				$message->save();
				$messageFlag = '1';
                $threads = Thread::find( $request->messageType );
                $subject="";
                if($threads->flag==0){
                    $subject = "TenantU - You have received an enquiry message.";
                }
                else{
                    $subject = "TenantU - You have received an issue message.";
                }
                Mail::send('emails.enquirythread', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($landLordName,$landLordEmail,$subject) {
                    $message->to($landLordEmail, $landLordName)
                            ->subject($subject);
                });
			}
			//print_r(route('tenants.getMessageList',[$messageThread->id]));exit;
			$messageThread->id = Crypt::encrypt($messageThread->id);
			return redirect()->route('tenants.getMessageList',[$messageThread->id]);
		}


	}

	public function getMessageList(Request $request,$encryptedMessageThreadId)
	{
		$messageThreadId = Crypt::decrypt($encryptedMessageThreadId);

		$loggedTenant 	= Auth::user('tenant');

		if(!$this->isTenantThread($messageThreadId,$loggedTenant->id))
		{
			return redirect('/404');
		}

		$name 			= $this->getTenantName($loggedTenant);

		$allMessages 	= $this->getAllMessagesWithThread($messageThreadId,$loggedTenant->id);

		$landlordName 	= $this->getLandLordName($allMessages[0]['landlord_id']);

		$landlordCity 	= $this->getLandLordCity($allMessages[0]['landlord_id']);
		$messageThreads = MessageThread::where('id',$messageThreadId)
												->select('thread_id')
												->addSelect('property_id')
												->addSelect('created_at')
												->first();
		//print_r($messageThreads);exit;
		//dd($encryptedMessageThreadId);
		$msgThread 						= $this->getMessageThreadName($messageThreads->thread_id);

		$msgPropertyName 				= $this->getMessageThreadPropertyName($messageThreads->property_id);
		$formattedMsgThreadCreatedDate 	= $this->getFormattedMsgThreadCreatedDate($messageThreads->created_at);
		//print_r($formattedMsgThreadCreatedDate);exit;

        $unitThread = $this->getMyUnitThreads($loggedTenant->id);
		$threadName 	= $msgThread[0]->thread_name;
		//$threadCreated 	= $msgThread[0]->created_at;


		$messageThreads = $this->getMyMessageThreads($allMessages[0]['landlord_id'],$loggedTenant->id);
		//dd($messageThreads);


		$readFlag 		= $this->getChangeReadFlag($messageThreadId);
		foreach($messageThreads as $thread) {
			$thread->id = Crypt::encrypt($thread->id);
		}
		$messageThreadId = Crypt::encrypt($messageThreadId);
		//dd($messageThreadId);
		$landlordId = $allMessages[0]['landlord_id'];
		$landlordId = Crypt::encrypt($landlordId);
		{
			return view('tenants.message', [
					'pageTitle'   			=> 'Messages',
					'allMessages'			=> $allMessages,
					'tenantName' 			=> $name,
					'landlordId' 			=> $landlordId,
					'landlordName'			=> $landlordName,
					'landlordCity'			=> $landlordCity,
					'messageThreadId'		=> $messageThreadId,
					'messageThreadsObj'		=> $messageThreads,
                    'unitThread'            => $unitThread,
					'pageMenuName'			=> 'mycontact',
					'messageFlag'			=> 'list',
					'msgThreadName'			=> $threadName,
					'msgThreadCreated'		=> $formattedMsgThreadCreatedDate,
					'threadWithFlag'		=> '',
					'msgPropertyName'		=> $msgPropertyName->property_name,
				]);
		}

	}

	public function composeMessageList(Request $request,$messageThreadId)
	{
		$loggedTenant 	= Auth::user('tenant');
		$encryptedLandLordId 	= $request->landlordId;
		$landLordId = Crypt::decrypt($encryptedLandLordId);
		$message 		= $request->message;
		$messageThreadId = Crypt::decrypt($messageThreadId);

		DB::table('messages')->insert(
			['message_thread_id' => $messageThreadId, 'message' =>$message,'landlord_id'=>$landLordId,'tenant_id'=>$loggedTenant->id,'sender'=> 'T','created_at'=>Carbon::now()]
		);
		$messageThreadId = Crypt::encrypt($messageThreadId);
		return redirect()->route('tenants.getMessageList',[$messageThreadId]);
	}

	private function getMyMessageThreads($landlordId,$tenantId)
	{

		$messageThreads = DB::select('SELECT thread_name,image,m.id,m.property_id,m.created_at,p.property_name
					FROM message_threads m, threads t,property_physical p
					WHERE m.thread_id = t.id AND m.property_id = p.id
					AND m.tenant_id ='.$tenantId.' AND m.landlord_id='.$landlordId.' order by m.created_at');

		return $messageThreads;
	}

	//get threads for per-unit and per-landlord
	private function getMyUnitThreads($tenantId) {
        $propertyId = DB::table("landlord_tenants")->where("tenant_id", $tenantId)->where("active", 1)->pluck('property_id');
        $currentMgmtAccount = DB::table('property_meta')->where('property_physical_id', $propertyId)->pluck('landlord_prop_mgmt_id');

        $unitThread = DB::table('unit_message')->where('mgmt_acct_id', $currentMgmtAccount)->get();
        return $unitThread;
    }

	private function getMessageThreads()
	{
		$threads = DB::select('SELECT id,thread_name FROM threads ORDER BY thread_name');
		return $threads;
	}

	//this method is used for the landlord profile

	public function getLandlordProfileFromTenant($slug)
	{
		$slugCheck = $this->checkTheSlugForLandlord($slug);
		if(count($slugCheck) < 1)
		{
			return redirect('/404');
		}
			$landlordProfileFromTenant = $this->getLandlordProfileFromSlug($slug);
			$schoolName = $this->getSchoolWithId($landlordProfileFromTenant->school_id);
			$landlordProperties = $this->getAllProperties($landlordProfileFromTenant->id);

			return view('tenants.landlordprofile', [
				'pageTitle'   		=> 'Landlord Profile',
				'pageHeading' 		=> 'Landlord Profile',
				'pageMenuName'		=> 'mycontact',
				'landlordsObj' 		=> $landlordProfileFromTenant,
				'schoolname'        => $schoolName
			]);


	}

	//this method is for checking the landlord profile for the slug

	private function checkTheSlugForLandlord($slug)
	{
		$landlordCount = Landlord::where('slug', '=', $slug)->first();
		return $landlordCount;
	}

	public function getLandLordCity($landlordId)
	{
		$landlordCity = DB::select('SELECT city FROM landlords where id='.$landlordId);

		if($landlordCity[0]->city=='')
		{
			$cityDetail = '';
		}
		else
		{
			$cityDetail= $landlordCity[0]->city;
		}
		return $cityDetail;
	}

	//this method is for the msg thread name with created according to the thread id

    public function getMessageThreadName($mgThreadId)
    {
		$threadDetails = DB::select('SELECT thread_name,created_at FROM threads where id='.$mgThreadId);
		return $threadDetails;
	}

	public function getChangeReadFlag($messageThreadId)
	{

		$message = Message::where('message_thread_id', $messageThreadId)
							->where('sender','L')
							->update(['read_flag' => 1]);
	}


	//this method is for the general queries frm message tbl and threds with flag 0
	public function getGeneralQueries($tenantId)
	{

		$generalMessages = Message::from('messages AS M')
                                 -> select('M.message')
                                 -> addSelect('M.message_thread_id')
                                 -> addSelect('T.thread_name')
                                 -> join ('message_threads AS MT','MT.id','=','M.message_thread_id')
                                 -> join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 -> where('M.tenant_id', $tenantId)
                                 -> where('M.sender','L')
                                 -> where('T.flag',0)
                                 -> orderBy('M.created_at','DESC')
                                 -> take(4)
                                 -> get();

        return $generalMessages;
	}

	// this fun is used issue queries frm message tblw and threads with flag 1
	public function getIssuedQueries($tenantId)
	{


		$issueMessages = Message::from('messages AS M')
                                 -> select('M.message')
                                 -> addSelect('M.message_thread_id')
                                 -> addSelect('T.thread_name')
                                 -> join ('message_threads AS MT','MT.id','=','M.message_thread_id')
                                 -> join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 -> where('M.tenant_id', $tenantId)
                                 -> where('M.sender','L')
                                 -> where('T.flag',1)
                                 -> orderBy('M.created_at','DESC')
                                 -> take(4)
                                 -> get();


         return $issueMessages;
	}

	//this func is for the recent properties showing

	public function getRecentProperties()
	{

		$loggedTenantId 	= Auth::user('tenant')->id;
		$properties 		= $this->getTenantRecentProperties($loggedTenantId);
		//print_r($properties);exit;
		return view('tenants.recentProperties', [
					'pageTitle'   			=> 'RecentProperties',
					'properties' 			=> $properties,
					'pageMenuName'			=> 'recentproperties',
				]);
	}

	//this func is for the recnt properties for tenant

	 public function getTenantRecentProperties($tenantId)
	 {
		 $recentProperties = DB::select('SELECT M.*, P.*, V.* FROM property_meta M LEFT JOIN property_physical P ON M.property_physical_id = P.id, (SELECT property_id, MAX( created_at ) as viewed_date , tenant_id FROM  `property_viewed` WHERE tenant_id ='.$tenantId.' GROUP BY property_id )AS V WHERE M.property_physical_id = V.property_id AND M.status=1 ORDER BY V.viewed_date');
		 foreach($recentProperties as $recentProperty)
		 {
			 $schoolId = $recentProperty->school_id;
			 $recentProperty->schoolName = $this->getSchoolName($schoolId);
		 }

         return $recentProperties;
	 }

	 private function getSchoolName($schoolId)
	 {
		 $schoolName = School::where('id',$schoolId)
								->select('schoolname')
								->first();
		//print_r($schoolName);exit;
		return $schoolName['schoolname'];
	 }

	 //this method is for feth the landlords details from tenant-landlords  and landlords table

	 public function getMyLandlords()
	 {
		 $tenantId = Auth::user('tenant')->id;
		 //DB::connection()->enableQueryLog();
		 $tenantLandlords =  LandlordTenant::from('landlord_tenants AS LT')
								 -> select('LT.id')
                                 -> addSelect('LT.property_id')
                                 -> addSelect('LT.month_rent_initial')
                                 -> addSelect('LT.due_on')
                                 -> addSelect('LT.landlord_id')
                                 -> addSelect('LT.agreement_start_date')
                                 -> addSelect('LT.cycle')
                                 -> addSelect('L.firstname')
                                 -> addSelect('L.lastname')
                                 -> addSelect('L.school_id')
                                 -> addSelect('L.email')
                                 -> addSelect('L.image')
                                 -> addSelect('L.phone')
                                 -> addSelect('L.location')
                                 -> addSelect('L.address')
                                 -> addSelect('PP.property_name')
                                 -> addSelect('PP.slug')
                                 -> addSelect('PP.administrative_area as propertyState')
                                 -> addSelect('PP.locality as propertyCity')
                                 -> addSelect('PP.postal_code as propertyPostalCode')
                                 -> addSelect('PP.thoroughfare as propertyStreetAddress')
				 -> addSelect('L.slug as landlordSlug')
                                 //-> addSelect('P.location as propertyLocation') may need to change above to propLocation
                                 -> addSelect('S.schoolname')
                                 -> join ('landlords AS L','LT.landlord_id','=','L.id')
                                 -> join ('property_physical AS PP', 'LT.property_id', '=', 'PP.id')
                                 -> join ('property_meta AS PM', 'PP.id', '=', 'PM.property_physical_id')
                                 -> join ('schools AS S','L.school_id','=','S.id')
                                 -> where('LT.tenant_id', $tenantId)
                                 -> where('PM.status',1)
                                 -> orderBy('LT.agreement_start_date')
                                 -> get();
        //$queries = DB::getQueryLog();
        //dd($queries);
       //print_r($tenantLandlords);exit;

         //var_dump(json_decode(json_encode($tenantLandlords)));
       return view('tenants.mylandlord', [
					'pageTitle'   			=> 'My Landlord',
					'pageMenuName'			=> 'mylandlord',
					'tenantLandlords' 		=> $tenantLandlords,
				]);


	 }

	 //this method is for showing agreement details
	 public function getAgreementDetails($landlordTenantId)
	 {
		 $landlordTenantExist = $this->checkLandlordTenant($landlordTenantId);
		 if(count($landlordTenantExist) < 1)
		 {
			 return redirect('/404');
		 }
		 $currentDate 			=  date("Y-m");

		 $tenantId 				= Auth::user('tenant')->id;

		 $landlordTenantDetails = $this->getLandlordTenantDetails($tenantId,$landlordTenantId);
		 //print_r($landlordTenantDetails);exit;
		 $tenantOtherpayment 	= $this->PaymentExistForCurrentMonth($currentDate,$tenantId,$landlordTenantId);
         //print_r($tenantOtherpayment);exit;
          if($tenantOtherpayment[0]->billcount > 0)
          {
			  $currentDate 	=  date("Y-m");
		  }
		  else
		  {
			  $yearMonthTime  =  strtotime($currentDate);
			  $currentDate    = date("Y-m", strtotime("+1 month", $yearMonthTime));
		  }

		 $myPaymentForMonthlyRents 		= $this->getMonthlyRentForTheCorrespondingMonth($currentDate,$tenantId,$landlordTenantId);
		 //print_r($myPaymentForMonthlyRents);exit;
		 $myPaymentForMonthlyOtherRents = $this->getMonthlyOtherPayments($currentDate,$tenantId,$landlordTenantId);
		 //print_r($myPaymentForMonthlyOtherRents);exit;
         $rentalAgreementDocuments 		= $this->getAgreementDocuments($tenantId,$landlordTenantId);
		 //print_r($rentalAgreementDocuments);exit;
         return view('tenants.agreement_details', [
					'pageTitle'   			=> 'AgreementDetails',
					'pageMenuName'			=> 'mylandlord',
					'agreementDetails' 		=> $landlordTenantDetails,
					'monthlyRents'			=> $myPaymentForMonthlyRents,
					'otherPayments'			=> $myPaymentForMonthlyOtherRents,
					'documentDetails'		=> $rentalAgreementDocuments
				]);
	 }

	 //this method is for the payment details

	private function  getLandlordTenantDetails($tenantId,$landlordTenantId)
	{
		$landlordTenantDetails =  LandlordTenant::from('landlord_tenants AS LT')
								 -> select('LT.id')
                                 -> addSelect('LT.property_id')
                                 -> addSelect('LT.landlord_id')
                                 -> addSelect('LT.due_on')
                                 -> addSelect('LT.agreement_start_date')
                                 -> addSelect('LT.cycle')
                                 -> addSelect('L.firstname')
                                 -> addSelect('L.lastname')
                                 -> addSelect('L.image')
                                 -> addSelect('L.location')
                                 -> addSelect('P.property_name')
                                 -> addSelect('P.thoroughfare as propertyLocation')
                                 -> join ('landlords AS L','LT.landlord_id','=','L.id')
                                 -> join ('property_physical AS P','LT.property_id','=','P.id')
                                 -> where('LT.tenant_id', $tenantId)
                                 -> where('LT.id',$landlordTenantId)
                                 -> orderBy('LT.agreement_start_date')
                                 -> get();
         return $landlordTenantDetails;
	}

	//this method is for check payment exist for current month

	private function PaymentExistForCurrentMonth($currentDate,$tenantId,$landlordTenantId)
	{
		$tenantOtherpayment = DB::select('SELECT COUNT(LTM.bill_on) as billcount FROM landlord_tenant_monthlyrents AS LTM
          INNER JOIN landlord_tenants AS LT ON LT.id=LTM.landlord_tenant_id WHERE LT.tenant_id='.$tenantId.' AND LTM.bill_on LIKE  "'.$currentDate.'%'.'" AND LTM.landlord_tenant_id='.$landlordTenantId.' ');
          return $tenantOtherpayment;
	}
	//this method is for monthly rent for the corresponding month
	private function getMonthlyRentForTheCorrespondingMonth($currentDate,$tenantId,$landlordTenantId)
	{
		$myPaymentForMonthlyRents = DB::select('SELECT   "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LT.id,LT.property_id,P.property_name
											FROM landlord_tenant_monthlyrents AS LTM
											INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
											INNER JOIN properties AS P ON LT.property_id = P.id
											WHERE LT.tenant_id ='.$tenantId.'
											AND LTM.bill_on LIKE "'.$currentDate.'%'.'" AND LTM.landlord_tenant_id='.$landlordTenantId.'   ORDER BY LTM.bill_on ASC');
		return $myPaymentForMonthlyRents;
	}
	//thismethod is for the monthly otherpayments

	private function getMonthlyOtherPayments($currentDate,$tenantId,$landlordTenantId)
	{
		$myOtherPayments = DB::select('SELECT LTO.title, LTO.initial_amount, LTO.agreement_start_date, LTOD.bill_on, LTOD.amount AS otheramount,LTOD.status
										FROM landlord_tenant_otherpayments AS LTO
										INNER JOIN landlord_tenant_otherpayment_details AS LTOD ON LTO.id = LTOD.landlord_tenant_otherpayment_id
										INNER JOIN landlord_tenants AS LT ON LT.id=LTO.landlord_tenant_id
										WHERE LT.tenant_id ='.$tenantId.'
										AND LTO.landlord_tenant_id='.$landlordTenantId.'
										AND LTOD.bill_on LIKE "'.$currentDate.'%'.'"
										ORDER BY LTOD.bill_on ASC');

		return $myOtherPayments;

	}

	private function getAgreementDocuments($tenantId,$landlordTenantId)
	{
		$agreementDetails = DB::select('SELECT LTD.doc_title, LTD.file_name
				FROM landlord_tenant_docs AS LTD
				INNER JOIN landlord_tenants AS LT ON LT.id = LTD.landlord_tenant_id
				WHERE LT.tenant_id = '.$tenantId.' AND LTD.landlord_tenant_id='.$landlordTenantId.'');

		return $agreementDetails;
	}

	private function checkLandlordTenant($landlordTenantId)
	{
		$landlordTenant = LandlordTenant::where('id','=',$landlordTenantId)
											->select('landlord_id')
											->addSelect('property_id')
											->get()->first();
		return $landlordTenant;
	}

	private function getMessageThreadPropertyName($propertyId)
	{
		$msgThreadPropertyName = PropertyPhysical::where('id',$propertyId)
										->select('property_name')
										->get()->first();
		return $msgThreadPropertyName;
	}

	private function getFormattedMsgThreadCreatedDate($threadDate)
	{

		$formattedMsgThreadCreatedDate = date('d M Y h:i:s a',strtotime($threadDate));
		//print_r($formattedMsgThreadCreatedDate);exit;
		return $formattedMsgThreadCreatedDate;
	}


	public function messageLandlordOut( Request $request ){
         $loggedTenant   = Auth::user('tenant');
         $loggedTenantId = $loggedTenant->id;
         $tenantProfile =  $this->getTenantProfile($loggedTenantId);
         $tenantFirstName = $tenantProfile->firstname;
         $tenantLastName = $tenantProfile->lastname;
         $tenantFullName = $tenantProfile->getFullNameAttribute();
         $tenantSchoolId = $tenantProfile->school_id;
         $tenantSchoolName =  $this->getSchoolWithId($tenantSchoolId)->schoolname;
         $tenantEmail = $tenantProfile->email;
         $landlordName = $request->name;
         $messageToLandlord = $request->message;
	 $landlordEmail = $request->email;
	 if($request->message == ""){
	 $mailSubject = "Your tenant has invited you to join TenantU!";
	 }
	 else{
	 $mailSubject = "Your tenant has messaged you from TenantU!";
	 }
         $rules = array(
            'name'                  => 'required',
            'email'                 => 'required|email'
        );
         $messages = [
                        'name.required'                         => 'Landlord name is Required',
                        'email.required'                 => 'Landlord email is Required'
                    ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {

                 Mail::send('emails.message-landlords',['landlordName'=>$landlordName,'tenantEmail'=>$tenantEmail,'tenantSchoolName'=>$tenantSchoolName,'messageToLandlord'=>$messageToLandlord, 'tenantFullName'=>$tenantFullName],
                                                                                function($message) use ($landlordEmail, $mailSubject) {

                                        $message->to($landlordEmail)
                                                        ->subject($mailSubject);
                 });
                 return redirect()->route('tenants.getMyLandlords')->with('message-success','Your message has been successfully sent. Your landlord may respond via email or make an account on TenantU to communicate within!');
        }
	}

}
