<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\LandlordTenant;
use App\Http\Models\TenantMaintenanceRequest;
use App\Http\Models\MaintenanceAvailableTimes;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Config;
use Validator;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Response;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Mail;
use HTML;

class LandlordMaintenanceRequestController extends Controller{
  public function getMyMaintenanceRequests($sortBy, $sortOrder)
  {
    $loggedLandlord = Auth::user('landlord');
    $loggedLandlordId = $loggedLandlord->id;
    $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
    $profileUrl       = asset('public/uploads/landlords/');
    $noProfileUrl     = asset('public/uploads/noimage/');
    if($landlordProfile->image!='')
    {
        $profileImage=$profileUrl."/".$landlordProfile->image;
    }
    else
    {
        $profileImage=$noProfileUrl."/landlord-blank.png";
    }
    if (($this->getPropertyCount($loggedLandlordId))>0)
    {
      if($sortBy == "Default"){
      $maintenanceRequests = TenantMaintenanceRequest::join('property_physical as p', 'maintenance_request.property_id' ,'=', 'p.id'
    )->join('landlords', 'p.landlord_id', '=', 'landlords.id')
    ->select('p.property_name','maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id', 'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $loggedLandlordId)->paginate(10);
     }
      else if($sortBy == "Date"){
      $maintenanceRequests = TenantMaintenanceRequest::join('property_physical as p', 'maintenance_request.property_id' ,'=', 'p.id'
    )->join('landlords', 'p.landlord_id', '=', 'landlords.id')
    ->select('p.property_name','maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id', 'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $loggedLandlordId)->orderBy('date_encountered',$sortOrder)->paginate(10);
     }
      else if($sortBy == "Property"){
      $maintenanceRequests = TenantMaintenanceRequest::join('property_physical as p', 'maintenance_request.property_id' ,'=', 'p.id'
    )->join('landlords', 'p.landlord_id', '=', 'landlords.id')
    ->select('p.property_name','maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id', 'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $loggedLandlordId)->orderBy('property_id',$sortOrder)->paginate(10);
     }
      else if($sortBy == "Type"){
      $maintenanceRequests = TenantMaintenanceRequest::join('property_physical as p', 'maintenance_request.property_id' ,'=', 'p.id'
    )->join('landlords', 'p.landlord_id', '=', 'landlords.id')
    ->select('p.property_name','maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id', 'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $loggedLandlordId)->orderBy('issue_type_id',$sortOrder)->paginate(10);
     }
      else if($sortBy == "Description"){
      $maintenanceRequests = TenantMaintenanceRequest::join('property_physical as p', 'maintenance_request.property_id' ,'=', 'p.id'
    )->join('landlords', 'p.landlord_id', '=', 'landlords.id')
    ->select('p.property_name','maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id', 'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $loggedLandlordId)->orderBy('issue_desc',$sortOrder)->paginate(10);
     }    
      else if($sortBy == "Status"){
      $maintenanceRequests = TenantMaintenanceRequest::join('property_physical as p', 'maintenance_request.property_id' ,'=', 'p.id'
    )->join('landlords', 'p.landlord_id', '=', 'landlords.id')
    ->select('p.property_name','maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id', 'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $loggedLandlordId)->orderBy('status',$sortOrder)->paginate(10);
     }

     // $maintenanceRequests = var_dump($maintenanceRequest);
      //$maintenanceRequests = DB::table('maintenance_request')->where('property_id', $tenantsPropertyId);i
    return view('landlords.myMaintenanceRequests', [
      'pageTitle'       => 'Maintenance Requests',
      'pageHeading' => 'Maintenance Requests',
      'pageMenuName'    => 'maintenancerequests',
      'profile'                 => $landlordProfile,
      'profileImage'                => $profileImage,
      'maintenanceRequests' => $maintenanceRequests
    ]);

/*
 echo '<pre>';
 echo $maintenanceRequest;
 echo '<br>';
 echo is_array($maintenanceRequest);
 echo '</pre>';
*/
    }
  }

  public function changeMaintenanceRequestStatusToRead($maintenanceRequestId)
  {
      DB::table('maintenance_request')
          ->where('id', $maintenanceRequestId)
          ->update(['status' => 1]);
      $maintenanceIssueNum = DB::table('maintenance_request')
				->where('id', $maintenanceRequestId)
				->select('issue_type_id')
				->get();
      
      $maintenanceIssueNum = $maintenanceIssueNum[0]->issue_type_id;

      if($maintenanceIssueNum == 0){
      $maintenanceIssueType = "Plumbing";
      }
      else if($maintenanceIssueNum == 1){
      $maintenanceIssueType = "Air Conditioning";
      }
      else if($maintenanceIssueNum == 2){
      $maintenanceIssueType = "Electric";
      }
      else if($maintenanceIssueNum == 3){
      $maintenanceIssueType = "Other";
      }
      $maintenanceStatus = "Read";
      $maintenancePropertyId = DB::table('maintenance_request')
				    ->where('id', $maintenanceRequestId)
				    ->select('property_id')
				    ->get();

      $maintenancePropertyId = $maintenancePropertyId[0]->property_id;
      $tenantEmails = $this->getTenantsEmailsFromActiveProperty($maintenancePropertyId);
      foreach($tenantEmails as $tenantEmail){	
      Mail::send('emails.maintenance-request-updated',['maintenanceStatus'=>$maintenanceStatus,'maintenanceIssueType'=>$maintenanceIssueType],
                                                                                function($message) use ($tenantEmail) {

                                        $message->to($tenantEmail)
                                                        ->subject("Your landlord has updated a maintenance request on TenantU!");
                 });
      }

      return redirect()->route('landlords.myMaintenanceRequests',['Default','Default']);
  }

  public function changeMaintenanceRequestStatusToInProgress($maintenanceRequestId)
  {
      DB::table('maintenance_request')
          ->where('id', $maintenanceRequestId)
          ->update(['status' => 2]);
      $maintenanceIssueNum = DB::table('maintenance_request')
                                ->where('id', $maintenanceRequestId)
                                ->select('issue_type_id')
                                ->get();

      $maintenanceIssueNum = $maintenanceIssueNum[0]->issue_type_id;

      if($maintenanceIssueNum == 0){
      $maintenanceIssueType = "Plumbing";
      }
      else if($maintenanceIssueNum == 1){
      $maintenanceIssueType = "Air Conditioning";
      }
      else if($maintenanceIssueNum == 2){
      $maintenanceIssueType = "Electric";
      }
      else if($maintenanceIssueNum == 3){
      $maintenanceIssueType = "Other";
      }
      $maintenanceStatus = "In Progress";
      $maintenancePropertyId = DB::table('maintenance_request')
                                    ->where('id', $maintenanceRequestId)
                                    ->select('property_id')
                                    ->get();

      $maintenancePropertyId = $maintenancePropertyId[0]->property_id;
      $tenantEmails = $this->getTenantsEmailsFromActiveProperty($maintenancePropertyId);
      foreach($tenantEmails as $tenantEmail){
      Mail::send('emails.maintenance-request-updated',['maintenanceStatus'=>$maintenanceStatus,'maintenanceIssueType'=>$maintenanceIssueType],
                                                                                function($message) use ($tenantEmail) {

                                        $message->to($tenantEmail)
                                                        ->subject("Your landlord has updated a maintenance request on TenantU!");
                 });
      }
      return redirect()->back();
  }

  public function changeMaintenanceRequestStatusToCompleted($maintenanceRequestId)
  {
      DB::table('maintenance_request')
          ->where('id', $maintenanceRequestId)
          ->update(['status' => 3]);
      $maintenanceIssueNum = DB::table('maintenance_request')
                                ->where('id', $maintenanceRequestId)
                                ->select('issue_type_id')
                                ->get();


      $maintenanceIssueNum = $maintenanceIssueNum[0]->issue_type_id;
     
      if($maintenanceIssueNum == 0){
      $maintenanceIssueType = "Plumbing";
      }
      else if($maintenanceIssueNum == 1){
      $maintenanceIssueType = "Air Conditioning";
      }
      else if($maintenanceIssueNum == 2){
      $maintenanceIssueType = "Electric";
      }
      else if($maintenanceIssueNum == 3){
      $maintenanceIssueType = "Other";
      }
      $maintenanceStatus = "Completed";
      $maintenancePropertyId = DB::table('maintenance_request')
                                    ->where('id', $maintenanceRequestId)
                                    ->select('property_id')
                                    ->get();

      $maintenancePropertyId = $maintenancePropertyId[0]->property_id;
      $tenantEmails = $this->getTenantsEmailsFromActiveProperty($maintenancePropertyId);
      foreach($tenantEmails as $tenantEmail){
      Mail::send('emails.maintenance-request-updated',['maintenanceStatus'=>$maintenanceStatus,'maintenanceIssueType'=>$maintenanceIssueType],
                                                                                function($message) use ($tenantEmail) {

                                        $message->to($tenantEmail)
                                                        ->subject("Your landlord has updated a maintenance request on TenantU!");
                 });
      }


      return redirect()->route('landlords.myMaintenanceRequests', ['Default', 'Default']);
  }

  public function showSingleMaintenanceRequest($maintenanceRequestId)
  {
      $loggedLandlord = Auth::user('landlord');
      $loggedLandlordId = $loggedLandlord->id;
      $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
      $profileUrl       = asset('public/uploads/landlords/');
      $noProfileUrl     = asset('public/uploads/noimage/');
      if($landlordProfile->image!='')
      {
          $profileImage=$profileUrl."/".$landlordProfile->image;
      }
      else
      {
          $profileImage=$noProfileUrl."/landlord-blank.png";
      }

      $maintenanceRequest = TenantMaintenanceRequest::findOrFail($maintenanceRequestId);

      if ($maintenanceRequest != null) {
          return view('landlords.myMaintenanceRequests', [
              'pageTitle' => 'Maintenance Requests',
              'pageHeading' => 'Maintenance Requests',
              'pageMenuName' => 'myMaintenanceRequests',
              'profile' => $landlordProfile,
              'profileImage' => $profileImage,
              'maintenanceRequest' => $maintenanceRequest
          ]);
      }
      else
      {
          return redirect('/');
      }
  }

}
