<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\ForteRentTransaction;
use Config;
use Session;
use Crypt;
use DB;
use Carbon;
use Auth;

class LandlordRentPaymentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRentPaymentHistory()
    {
        //
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordProfile  = $this->getLandlordProfile($loggedLandlordId);
        // $location_id = $this->getLandlordLocationId($loggedLandlordId);
        $location_id = 'loc_193969';
        $profileUrl = asset('public/uploads/landlords/');
        $noProfileUrl = asset('public/uploads/noimage/');
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        // FORTE STUFF WE NEED
        $base_url          = env('FORTE_BASE_URL');     //production: http://api.forte.net/v3
        // Landlord Required Fields
        $organization_id   = $this->getLandlordOrgId($loggedLandlordId);
        $location_id       = $location_id;
        $api_access_id     = env('FORTE_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_API_SECURE_KEY');
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);

        // Get all the transactions of the currently Logged Landlord
        $transaction_ids = DB::table('forte_rent_transactions')
                              ->join('landlord_tenants', function($join) use ($loggedLandlordId){
                                  $join->on('forte_rent_transactions.landlord_tenant_id', '=', 'landlord_tenants.id')
                                        ->where('landlord_tenants.landlord_id', '=', $loggedLandlordId);
                              })
                              ->select('transaction_token')
                              ->get();
        foreach($transaction_ids as $key => $trans_id)
        {
          $trans_token = $trans_id->transaction_token;
          $transaction_id = Crypt::decrypt($trans_token);
          // Lets update the transactions accordingly
          $get_trans_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id .'/transactions/' . $transaction_id;
          $check_receive_date_exist = DB::table('forte_rent_transactions')
                                      ->where('transaction_token', $trans_token)
                                      ->where('response_code', 'A01')
                                      ->value('date_received');
          $check_funded_date_exist = DB::table('forte_rent_transactions')
                                      ->where('transaction_token', $trans_token)
                                      ->where('response_code', 'A01')
                                      ->value('date_funded');

          if($check_funded_date_exist != '')
          {
              continue;
          }
          else
          {
              $getSameTransaction = $this->forteGet($get_trans_endpoint, $auth_token, $organization_id);
              $trans_response = curl_exec($getSameTransaction);
              $info = curl_getinfo($getSameTransaction);
              curl_close($getSameTransaction);
              $trans_get_data = json_decode($trans_response);
        
               // echo '<pre>';
               // var_dump($trans_get_data);
               // echo '<br>';
               // echo '</pre>';
               // exit('done');
              $new_status = $trans_get_data->status;
              $date_received = $trans_get_data->received_date;
              $date_funded_exists = property_exists($trans_get_data, 'origination_date');
              $date_funded = ($date_funded_exists != TRUE ? '':$trans_get_data->origination_date);

              if($check_receive_date_exist == '')
              {
                  $update_transaction_now = DB::table('forte_rent_transactions')
                                          ->where('transaction_token', $trans_token)
                                          ->update(['status' => $new_status, 'date_received' => $date_received]);
              }
              if($check_funded_date_exist != '' || $date_funded_exists != TRUE)
              {
                  continue;

              }
              else // No need to do anything for date received, it's already there
              {

                  $update_transaction_now = DB::table('forte_rent_transactions')
                                          ->where('transaction_token', $trans_token)
                                          ->update(['date_funded' => $date_funded]);
              }
          // NOW WE HAVE THE UPDATED FORTE STATUS STUFF
          }
        }
        // Get the transactions via Eloquent to paginate
        // Get all the transactions that belong to the Landlord. (Every transaction that has a landlord tenant id that
        // has the same landlord id as the currently logged landlord)
        //$my_rent_transactions = ForteRentTransaction::where('landlord_tenant_id', $current_landlord_relationship_id)->paginate(10);
        $my_rent_transactions = ForteRentTransaction::join('landlord_tenants', function ($join) use ($loggedLandlordId){
                                                        $join->on('landlord_tenants.id', '=','forte_rent_transactions.landlord_tenant_id')
                                                              ->where('landlord_tenants.landlord_id', '=', $loggedLandlordId);
                                                      })
                                                      ->join('properties','landlord_tenants.property_id', '=', 'properties.id')
                                                      ->select('forte_rent_transactions.status', 'forte_rent_transactions.date_due', 'forte_rent_transactions.authorized_amount', 'forte_rent_transactions.date_posted', 'forte_rent_transactions.date_received', 'forte_rent_transactions.date_funded', 'forte_rent_transactions.transaction_token','properties.property_name')
                                                      ->where('landlord_tenants.landlord_id', '=', $loggedLandlordId)
                                                      ->paginate(10);

            if ($location_id != '')
            {
              return view('landlords.allRentPaymentHistory',[
              'pageTitle'     => 'Rent Collection History',
              'pageHeading'   => 'Rent Transactions History',
              'pageMenuName'  => 'renthistory',
              'profile'       => $landlordProfile,
              'profileImage'      => $profileImage,
              'rent_payments' => $my_rent_transactions
        ]);
            }
            else
            {
              Session::flash('warningMessage', "You must set your Rent Collection Payment Settings!");
              return redirect()->route('landlords.dashboard');
            }

    }

        public function getRentPaymentHistoryObject()
    	{
		//
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordProfile  = $this->getLandlordProfile($loggedLandlordId);
        // $location_id = $this->getLandlordLocationId($loggedLandlordId);
        $location_id = 'loc_193969';
        $profileUrl = asset('public/uploads/landlords/');
        $noProfileUrl = asset('public/uploads/noimage/');
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        // FORTE STUFF WE NEED
        $base_url          = env('FORTE_BASE_URL');     //production: http://api.forte.net/v3
        // Landlord Required Fields
        $organization_id   = $this->getLandlordOrgId($loggedLandlordId);
        $location_id       = $location_id;
        $api_access_id     = env('FORTE_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_API_SECURE_KEY');
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);

        // Get all the transactions of the currently Logged Landlord
        $transaction_ids = DB::table('forte_rent_transactions')
                              ->join('landlord_tenants', function($join) use ($loggedLandlordId){
                                  $join->on('forte_rent_transactions.landlord_tenant_id', '=', 'landlord_tenants.id')
                                        ->where('landlord_tenants.landlord_id', '=', $loggedLandlordId);
                              })
                              ->select('transaction_token')
                              ->get();
        foreach($transaction_ids as $key => $trans_id)
        {
          $trans_token = $trans_id->transaction_token;
          $transaction_id = Crypt::decrypt($trans_token);
          // Lets update the transactions accordingly
          $get_trans_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id .'/transactions/' . $transaction_id;
          $check_receive_date_exist = DB::table('forte_rent_transactions')
                                      ->where('transaction_token', $trans_token)
                                      ->where('response_code', 'A01')
                                      ->value('date_received');
          $check_funded_date_exist = DB::table('forte_rent_transactions')
                                      ->where('transaction_token', $trans_token)
                                      ->where('response_code', 'A01')
                                      ->value('date_funded');

          if($check_funded_date_exist != '')
          {
              continue;
          }
          else
          {
              $getSameTransaction = $this->forteGet($get_trans_endpoint, $auth_token, $organization_id);
              $trans_response = curl_exec($getSameTransaction);
              $info = curl_getinfo($getSameTransaction);
              curl_close($getSameTransaction);
              $trans_get_data = json_decode($trans_response);

               // echo '<pre>';
               // var_dump($trans_get_data);
               // echo '<br>';
               // echo '</pre>';
               // exit('done');
              $new_status = $trans_get_data->status;
              $date_received = $trans_get_data->received_date;
              $date_funded_exists = property_exists($trans_get_data, 'origination_date');
              $date_funded = ($date_funded_exists != TRUE ? '':$trans_get_data->origination_date);

              if($check_receive_date_exist == '')
              {
                  $update_transaction_now = DB::table('forte_rent_transactions')
                                          ->where('transaction_token', $trans_token)
                                          ->update(['status' => $new_status, 'date_received' => $date_received]);
              }
              if($check_funded_date_exist != '' || $date_funded_exists != TRUE)
              {
                  continue;

              }
              else // No need to do anything for date received, it's already there
              {

                  $update_transaction_now = DB::table('forte_rent_transactions')
                                          ->where('transaction_token', $trans_token)
                                          ->update(['date_funded' => $date_funded]);
              }
          // NOW WE HAVE THE UPDATED FORTE STATUS STUFF
          }
        }
        // Get the transactions via Eloquent to paginate
        // Get all the transactions that belong to the Landlord. (Every transaction that has a landlord tenant id that
        // has the same landlord id as the currently logged landlord)
        //$my_rent_transactions = ForteRentTransaction::where('landlord_tenant_id', $current_landlord_relationship_id)->paginate(10);
        $my_rent_transactions = ForteRentTransaction::join('landlord_tenants', function ($join) use ($loggedLandlordId){
                                                        $join->on('landlord_tenants.id', '=','forte_rent_transactions.landlord_tenant_id')
                                                              ->where('landlord_tenants.landlord_id', '=', $loggedLandlordId);
                                                      })
                                                      ->join('properties','landlord_tenants.property_id', '=', 'properties.id')
                                                      ->select('forte_rent_transactions.status', 'forte_rent_transactions.date_due', 'forte_rent_transactions.authorized_amount', 'forte_rent_transactions.date_posted', 'forte_rent_transactions.date_received', 'forte_rent_transactions.date_funded', 'forte_rent_transactions.transaction_token','properties.property_name')
                                                      ->where('landlord_tenants.landlord_id', '=', $loggedLandlordId)
						      ->get();
		return $my_rent_transactions;				  
	}

    public function getRentPaymentHistoryCsvDownload()
    { 
                $csvPath = Config::get('constants.LANDLORD_CSV_PATH');
				$loggedLandlord = Auth::user('landlord');
				$loggedLandlordId = $loggedLandlord->id;
				$uId = uniqid();
				$fileName  = "rentTransactionHistory-".$uId."-".$loggedLandlordId.".csv";
				$rentPayHistory = $this->getRentPaymentHistoryObject();
				$textToWrite = "Month Rent, Property, Data Submitted, Amount, Date Seen, Status, Date Rent Received \n ";
				foreach($rentPayHistory as $singleRow){
				$textToWrite = $textToWrite . date('M', strtotime($singleRow->date_due)).",".$singleRow->property_name.",".$singleRow->date_posted.",".$singleRow->authorized_amount.",".$singleRow->date_received.",".$singleRow->status.",".$singleRow->date_funded."\n "; 
				}
				file_put_contents($csvPath."/".$fileName,$textToWrite);
        			return response()->download($csvPath."/".$fileName, $fileName);        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
