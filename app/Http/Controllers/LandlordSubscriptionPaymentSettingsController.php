<?php

namespace App\Http\Controllers;
use App\Http\Models\ForteLandlordSubscriptionPaySettings;
use App\Http\Models\LandlordBillingAddress;
use DB;
use Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Response;

Class LandlordSubscriptionPaymentSettingsController extends Controller
{
    
    public function subscriptionSettingsCardPost()
    {
        return redirect('/');
    }

    public function createLandlordPaySettingsForte($landlord_pay_params)
    {
        $loggedLandlord           = Auth::user('landlord');
        $loggedLandlordId         = $loggedLandlord->id;
        $base_url   		= Config::get('constants.FORTE_BASE_URL');
        $organization_id 	= env('FORTE_TENANTU_MERCHANT_ORG_ID');
        // This location id will be TenantU's admin merchant account special location id, as will every id including api id, key, and org id
        $location_id 		= 'loc_193969';
        $api_access_id 		= env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key 	= env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token     	= base64_encode($api_access_id . ':' . $api_secure_key);
        $payment_setting_endpoint 			= $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/';

        $ch = $this->fortePost($payment_setting_endpoint, $landlord_pay_params, $auth_token, $organization_id);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $data = json_decode($response);
        $create_http_success = (($info['http_code'] != 201) ? 0:1);

        $create_output = json_encode(array(
            'create_http_success' => $create_http_success,
            'data' => $data
        ));
        echo '<pre>';
        var_dump($create_output);
        echo '<br>';
        echo '</pre>';
        //exit('stop');
        return $create_output;

    }

    public function postCustomer(Request $request) {

        //Grab Logged Tenant Information from the Tenant table
        $loggedLandlord 		= Auth::user('landlord');
        $loggedLandlordId 	= $loggedLandlord->id;

        //$base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
        // Landlord Required Fields
        $base_url   = Config::get('constants.FORTE_BASE_URL');
        $organization_id = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        //tenantu's location ID
        $location_id = 'loc_193969';
        $api_access_id = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        // END Landlord Required fields

        $usingBank = null;

        $rules = array();

        if ($request->card_type != null)
        {
            $usingBank = false;
            $rules = array(
                'card_type' => 'required',
                'name_on_card' => 'required',
                'card_number' => 'required',
                'expire_month' => 'required',
                'expire_year' => 'required',
                'card_verification_value' => 'required',
                'st_line1' => 'required',
                'city' => 'required',
                'state' => 'required',
                'zip_code' => 'required'
            );

            $messages = array(
              'card_type.required' => 'Credit Card Type is Required',
              'name_on_card.required' => 'Cardholder Name is Required',
              'card_number.required' => 'Card Number is Required',
              'card_verification_value.required' => 'Card Verification Number is Required',
              'st_line1.required' => 'Address Line 1 is Required',
              'city.required' => 'City is Required',
              'state.required' => 'State is Required',
              'zip_code.required' => 'Zip Code is Required'

            );
        }

        if ($request->bank_name != null || $request->account_no != null)
        {
            $usingBank = true;
            $rules = array(
                'bank_name' => 'required',
                'account_holder' => 'required',
                'account_no' => 'required',
                'routing_no' => 'required',
                'st_line1' => 'required',
                'city' => 'required',
                'state' => 'required',
                'zip_code' => 'required'
            );
            $messages = array(
                'bank_name.required' => 'Bank Name is Required',
                'account_holder.required' => 'Account Holder is Required',
                'account_no.required' => 'Account Number is Required',
                'routing_no.required' => 'Routing Number is Required',
                'st_line1.required' => 'Address Line 1 is Required',
                'city.required' => 'City is Required',
                'state.required' => 'State is Required',
                'zip_code.required' => 'Zip Code is Required'
            );
        }

        if($request->bank_name == '' && $request->card_type == '')
        {
          $rules = array(
            'bank_name' => 'required',
            'card_type' => 'required'
          );

          $messages = [
            'bank_name.required' => 'Bank Required - Pick A Payment Method!',
            'card_type.required' => 'Card Required - Pick A Payment Method!'
          ];
        }


        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->with('displayBank',1);
        }

        else // Commence with Create or Update
        {
            $loggedLandlordEmail = $loggedLandlord->email;

            $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
            $endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/';

            $physicaladdress = json_decode($this->getPhysicalAddress($request));
            $billingaddress = json_decode($this->getBillingAddress($request, $loggedLandlordId, $loggedLandlordEmail, $physicaladdress));

            $default_pay_method = null;

            if ($usingBank)
            {
                $echeck = json_decode($this->getBankInfo($request));


                //eCheck Info to store into Customer PayMethod - This is the account rent will be paid from

                $default_pay_method = json_decode($this->getDefaultPaymentMethod($echeck, $request, $usingBank));

            }
            else//if ($usingBank === false) We're gonna be using card instead
            {
                $card = json_decode($this->getCardInfo($request));
                $default_pay_method = json_decode($this->getDefaultPaymentMethod($card, $request, $usingBank));

            }
            //Address/Name Info
            $name = array(
                'first_name' => $loggedLandlord->firstname,
                'last_name' => $loggedLandlord->lastname
            );

            //$full_name = $name['first_name'] . ' ' . $name['last_name'];
            // PARAMETERS OF THE CUSTOMER TO BE MADE - used for createLandlordPaySettingsForte
            $customer_params = json_decode($this->getCustomerPaymentParameters($name['first_name'], $name['last_name'], $default_pay_method, $billingaddress));


            // CHECK IF LANDLORD HAS AN EXISTING FORTE CUSTOMER TOKEN
            $check_existing_cust_token = DB::table('forte_landlord_subscription_pay_settings')->where('landlord_id', $loggedLandlordId)->value('customer_id');

            // If Logged In Tenant HAS an existing Forte Customer ID inside forte_landlord_subscription_pay_settings table
            if($check_existing_cust_token != '')
            {
                $customer_token = Crypt::decrypt($check_existing_cust_token);
                // HTPP Code grabbed from checking the Two Update Requests in the Forte Update Function
                //$update_http_code = $this->updateLandlordPaySettingsForte($customer_params);
                // Screw the update, Forte wants you to create a whole new pay method instead
                if(strpos($request->account_no, '*') !== false || strpos($request->card_number, '*') !== false)
                {
                    // Just update the address
                     $addr_http_success = $this->updateCustomerAddressForte($physicaladdress);
                     // echo '<pre>';
                     // echo $addr_http_success;
                     // echo '</pre>';
                     // exit('whats up');
                    if ($addr_http_success != 1)
                    {
                    echo '<pre>';
                    echo 'FAIL';
                    echo '</pre>';

                    return redirect()->route('landlords.subscriptionpaymentsettings')->with('message-error', 'Invalid input detected, please ensure all information is valid.')->with('failureBank',1);
                    }
                    else
                    {
                        // echo '<pre>';
                        // echo 'SUCCESS';
                        // echo '</pre>';
                        $addr_http_success = $this->updateCustomerAddressForte($physicaladdress);
                        $this->updateLandlordBillingAddressDB($physicaladdress);
                        return redirect()->route('landlords.subscriptionpaymentsettings')->with('message-success', 'Address Saved!')->with('successBank',1);


                    }
                }
                else // Update Everything
                {
                    $createNewForte = json_decode($this->createNewPaymethodForte($customer_token, $customer_params));
                // print_r($createNewForte);
                // echo '<br>';
                // print_r($createNewForte->data->paymethod_token);
                // exit('exit');
                $addr_http_success = $this->updateCustomerAddressForte($physicaladdress);
                //  echo '<pre>';
                //  var_dump($createNewForte);
                //  echo '<br>';
                // var_dump($addr_http_success);
                // echo '</pre>';
                // exit('done');
                $new_payment_token = (property_exists($createNewForte, 'data') ? $createNewForte->data->paymethod_token : 'Error: Contact Support');

                $newDefaultSet = json_decode($this->setDefaultPaymentMethodForte($new_payment_token, $customer_token));
                //exit($new_payment_token);
                //$this->updateLandlordSubscriptionPayTokenDB($new_payment_token);



                //$this->updateLandlordPaySettingsDB($default_pay_method);
                if ($createNewForte->create_http_success != 1)
                {
                    echo '<pre>';
                    echo 'FAIL';
                    echo '</pre>';

                    return redirect()->route('landlords.subscriptionpaymentsettings')->with('message-error', 'Invalid input detected, please ensure all information is valid.')
                        ->with('failureBank',1);
                }
                else
                {
                    // echo '<pre>';
                    // echo 'SUCCESS';
                    // echo '</pre>';
                    $addr_http_success = $this->updateCustomerAddressForte($physicaladdress);
                    $this->updateLandlordBillingAddressDB($physicaladdress);
                    $this->updateLandlordSubscriptionPayTokenDB($new_payment_token);
                    return redirect()->route('landlords.subscriptionpaymentsettings')->with('message-success', 'New Payment Method Saved!')->with('successBank',1);
                }
                }
                

            }
            else // Create the Customer Object in Forte for the Landlord
            {

              $createForte = json_decode($this->createLandlordPaySettingsForte($customer_params));

              // Following Variables are used to update the DB with the fresh customer info
              //return var_dump($createForte);
              $landlord_payment_token = (property_exists($createForte, 'data') ? $createForte->data->default_paymethod_token : 'Error: Contact Support');
              //$landlord_payment_token = (isset($createForte->data->default_paymethod_token) ? $createForte->data->default_paymethod_token : 'Error: Contact Support');
              $landlord_customer_token = (property_exists($createForte,'data') ? $createForte->data->customer_token : 'Error: Contact Support');
              // $landlord_customer_token = (isset($createForte->data->customer_token) ? $createForte->data->customer_token : 'Error: Contact Support');
              $landlord_address_token = (property_exists($createForte,'data') ? $createForte->data->default_billing_address_token : 'Error: Contact Support');
              // $landlord_address_token = (isset($createForte->data->default_billing_address_token) ? $createForte->data->default_billing_address_token : 'Error: Contact Support');
              $create_http_success = $this->getHttpSuccess($createForte->create_http_success);
              if ($create_http_success != 1)
              {
                return redirect()->route('landlords.subscriptionpaymentsettings')->with('message-error', 'Invalid input detected, please ensure all information is valid.')->with('failureBank',1);
              }
              else
              {
                $payment_settings_db = json_decode($this->getCustomerPaymentParametersDB($landlord_customer_token, $landlord_payment_token, $landlord_address_token));

                $this->createLandlordSubscriptionPaySettingsDB($payment_settings_db, $physicaladdress);

                return redirect()->route('landlords.subscriptionpaymentsettings')->with('message-success', 'Payment Settings Saved!')->with('successBank',1);
              }


            }
        }
    }

    public function getCustomerPaymentParameters($firstname, $lastname, $paymethod, $billingaddress)
    {
        $customer_params = json_encode(array(
            'first_name' => $firstname,
            'last_name' => $lastname,
            'paymethod' => $paymethod,
            'addresses' => array($billingaddress)            
        ));

        return $customer_params;

    }

    public function getHttpSuccess($http_success)
    {
        return $http_success;
    }

    public function getCustomerPaymentParametersDB($customer_token, $payment_token, $address_token)
    {
        $payment_settings = json_encode(array(
            'customer_token' => $customer_token,
            'payment_token' => $payment_token,
            'address_token' => $address_token
        ));

        return $payment_settings;
    }

    public function getCardInfo(Request $request)
    {
      $card = json_encode(array(
        'card_type' => $request->card_type,
        'name_on_card' => $request->name_on_card,
        'account_number' =>  $request->card_number,
        'expire_month' => $request->expire_month,
        'expire_year' => $request->expire_year,
        'card_verification_value' => $request->card_verification_value
      ));

      return $card;
    }

    public function getBankInfo(Request $request)
    {
      $echeck = json_encode(array(
        'account_holder'    => $request->account_holder,
        'routing_number'    => $request->routing_no,
        'account_number'    => $request->account_no,
        'account_type'      => ($request->account_type == 'C' ? 'checking':'savings')
      ));

      return $echeck;
    }

    public function getDefaultPaymentMethod($pay_method, $request, $using_bank)
    {
      if ($using_bank === true)
      {
        $default_pay_method = json_encode(array(
          'label' => $request->bank_name,
          'notes' => $request->bank_name,
          'echeck' => $pay_method
        ));
      }
      else
      {
        $default_pay_method = json_encode(array(
          'label' => $pay_method->card_type,
          'notes' => $pay_method->card_type,
          'card' => $pay_method
        ));
      }
      return $default_pay_method;
    }

    public function setDefaultPaymentMethodForte($pay_method_token,$customer_token)
    {
        $base_url   = Config::get('constants.FORTE_BASE_URL');
        $organization_id = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        // MERCHANT ACCOUNT
        $location_id = 'loc_193969';
        $api_access_id = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token     = base64_encode($api_access_id . ':' . $api_secure_key);

        $customer_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token;

        $customer_params = json_encode(array(
            'default_paymethod_token' => $pay_method_token
        ));

        $ch = $this->forteUpdate($customer_endpoint, json_decode($customer_params), $auth_token, $organization_id);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $create_http_success = ($info['http_code'] != 200 ? 0:1);

        $updateOutput = json_encode(array(
            'create_http_success' => $create_http_success,
            'data'                => json_decode($response)
        ));

        // echo '<pre>';
        // print_r(json_decode($updateOutput));
        // echo 'START: ';
        // echo '<br>';
        // echo '<br>';
        // exit($updateOutput);

        return $updateOutput;

    }

    public function getPhysicalAddress(Request $request)
    {
        $physical_address = json_encode(array(
            'street_line1'  => $request->st_line1,
            'street_line2'  => $request->st_line2,
            'locality'      => $request->city,
            'region'        => $request->state,
            'postal_code'   => $request->zip_code
        ));
        return $physical_address;
    }

    // Returns Billing Address in JSON Format
    public function getBillingAddress(Request $request, $landlord_id, $landlord_email, $physical_address)
    {
      $landlord_full_name = explode(" ",$this->getLandLordName($landlord_id));
      $first_name = $landlord_full_name[0];
      $last_name = $landlord_full_name[1];
      $billing_address = json_encode(array(
        'first_name'            => $first_name,
        'last_name'             => $last_name,
        'label'                 => $first_name . ' ' . $last_name . "'s " . 'Billing Address',
        'email'                 => $landlord_email,
        'address_type'          => 'default_billing',
        'shipping_address_type' => $request->address_type,
        'physical_address'      => $physical_address
      ));
      return $billing_address;
    }
    // UPDATE REQUEST TO FORTE REGARDING PAY SETTINGS
    public function updateLandlordPaySettingsForte($landlord_payment_settings)
    {

        $loggedLandlord           = Auth::user('landlord');
        $loggedLandlordId         = $loggedLandlord->id;
        $base_url   = Config::get('constants.FORTE_BASE_URL');
        $organization_id = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $location_id = 'loc_193969';
        $api_access_id = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token     = base64_encode($api_access_id . ':' . $api_secure_key);

        $landlordCustomerIdPre = DB::table('forte_landlord_subscription_pay_settings')->where('landlord_id', $loggedLandlordId)->value('customer_id');
        $landlordCustomerId = Crypt::decrypt($landlordCustomerIdPre);
        $landlordPaymentIdPre = DB::table('forte_landlord_subscription_pay_settings')->where('landlord_id', $loggedLandlordId)->value('payment_id');
        $landlordPaymentId = Crypt::decrypt($landlordPaymentIdPre);
        $landlordAddressIdPre = DB::table('forte_landlord_subscription_pay_settings')->where('landlord_id', $loggedLandlordId)->value('addr_id');
        $landlordAddressId = Crypt::decrypt($landlordAddressIdPre);

        // ENDPOINT TO RETRIVE PAYMETHOD OBJECT FORTE
        $pay_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/paymethods/' . $landlordPaymentId;
        // ENDPOINT TO RETRIVE CUSTOMER'S BILLING ADDRESS
        $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $landlordCustomerId . '/addresses/' . $landlordAddressId;

        $payment_method = array();


        //NOTE: these two if statements depend on the landlord_payment_settings having a non-null value for fields that are specific to each payment option

        //landlord is using a credit card
        if (property_exists($landlord_payment_settings->paymethod, 'card'))//$landlord_payment_settings['card_type'] != null)
        {
            $payment_method = array(
                'card'      => $landlord_payment_settings->paymethod->card
            );

        }
        //landlord is using a bank
        else if (property_exists($landlord_payment_settings->paymethod,'echeck'))//$landlord_payment_settings['account_holder'] != null)
        {
            $payment_method = array(
                'label'             => $landlord_payment_settings->paymethod->label,
                'echeck'            => $landlord_payment_settings->paymethod->echeck
            );
        }


        $addr_params = array(
            'physical_address' => $landlord_payment_settings->addresses[0]->physical_address)
        ;
        //updates payment method
        //$ch = $this->forteUpdate($pay_endpoint, $payment_method, $auth_token);
        //updates address
        $ch1 = $this->forteUpdate($addr_endpoint, $addr_params, $auth_token, $organization_id);

        //$responsePayment = curl_exec($ch);
        $responsePhysicalAddress = curl_exec($ch1);
        //$curlInfoPayment = curl_getinfo($ch);
        $curlInfoPhysicalAddress = curl_getinfo($ch1);
        //curl_close($ch);
        curl_close($ch1);

        $dataPayment = json_decode($responsePayment);
        $dataPhysicalAddress = json_decode($responsePhysicalAddress);

        // If either request does not have a HTTP Code of 200 (Success), http_success = 0, which will prompt an error message in the main function
        $http_succeeded = (($curlInfoPayment['http_code'] != 200) || ($curlInfoPhysicalAddress['http_code'] != 200) ? 0:1);
        $update_data = json_encode(array(
                'http_succeeded' => $http_succeeded,
                //'data_payment' => $responsePayment,
                'data_address' => $responsePhysicalAddress
            ));
        //return $http_succeeded;
       

        echo '<pre';
        print_r(json_decode($update_data));
        echo '<br>';
        echo '<br><br><br>';
        echo '<br>';

        //print_r($landlord_payment_settings->paymethod);
        echo '</pre>';
        exit('doneso');

        return $update_data;
    }

    public function createNewPaymethodForte($customer_token, $customer_params)
    {
        $base_url   = Config::get('constants.FORTE_BASE_URL');
        $organization_id = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $location_id = 'loc_193969';
        $api_access_id = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token     = base64_encode($api_access_id . ':' . $api_secure_key);

        $payment_setting_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token . '/paymethods';
        $customer_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token;

        // echo '<pre>';
        // print_r($customer_params);
        // echo '</pre>';
        // exit('dlete this');

        // $paymethod_params = json_encode(array(
        //     'label' => $customer_params
        //     ));

        $ch = $this->fortePost($payment_setting_endpoint, $customer_params->paymethod, $auth_token, $organization_id);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $create_http_success = ($info['http_code'] != 201 ? 0:1);

        $createOutput = json_encode(array(
            'create_http_success' => $create_http_success,
            'data'                => json_decode($response)
        ));

        return $createOutput;
    }

    public function createLandlordSubscriptionPaySettingsDB($pay_settings, $billing_address)
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        // This location ID will be TenantU specific, once Merchant Account is approved. 
        $location_id = 'loc_193969';

        $landlord_pay_settings = ForteLandlordSubscriptionPaySettings::firstOrNew(array('landlord_id' => $loggedLandlordId));

        $landlord_pay_settings->customer_id = Crypt::encrypt($pay_settings->customer_token);
        $landlord_pay_settings->payment_id = Crypt::encrypt($pay_settings->payment_token);
        $landlord_pay_settings->addr_id = Crypt::encrypt($pay_settings->address_token);
        $landlord_pay_settings->tenantu_location_id = Crypt::encrypt($location_id);

        $landlord_pay_settings->save();

        $landlord_billing_addr_settings = LandlordBillingAddress::firstOrNew(array('landlord_id' => $loggedLandlordId));
        $landlord_billing_addr_settings->street_line_1 = $billing_address->street_line1;
        $landlord_billing_addr_settings->street_line_2 = $billing_address->street_line2;
        $landlord_billing_addr_settings->locality = $billing_address->locality;
        $landlord_billing_addr_settings->region = $billing_address->region;
        $landlord_billing_addr_settings->postal_code = $billing_address->postal_code;
        $landlord_billing_addr_settings->save();
    }

    public function updateLandlordBillingAddressDB($physical_address)
    {
        $loggedLandlordId = Auth::user('landlord')->id;

        $landlord_billing_addr_settings = LandlordBillingAddress::where('landlord_id', $loggedLandlordId)->first();
        $landlord_billing_addr_settings->street_line_1 = $physical_address->street_line1;
        $landlord_billing_addr_settings->street_line_2 = $physical_address->street_line2;
        $landlord_billing_addr_settings->locality = $physical_address->locality;
        $landlord_billing_addr_settings->region = $physical_address->region;
        $landlord_billing_addr_settings->postal_code = $physical_address->postal_code;
        $landlord_billing_addr_settings->save();
    }
    public function updateCustomerAddressForte($physical_address)
    {
        $loggedLandlordId = Auth::user('landlord')->id;

        $base_url   = Config::get('constants.FORTE_BASE_URL');
        $organization_id = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $location_id = 'loc_193969';
        $api_access_id = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token     = base64_encode($api_access_id . ':' . $api_secure_key);

        $landlord_customer_id_pre = DB::table('forte_landlord_subscription_pay_settings')->where('landlord_id', $loggedLandlordId)->value('customer_id');
        $landlord_customer_id = Crypt::decrypt($landlord_customer_id_pre);
        $landlord_address_id_pre = DB::table('forte_landlord_subscription_pay_settings')->where('landlord_id', $loggedLandlordId)->value('addr_id');
        $landlord_address_id = Crypt::decrypt($landlord_address_id_pre);

        $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $landlord_customer_id . '/addresses/' . $landlord_address_id;

        $addr_params = array(
            'physical_address' => $physical_address
        );

        $ch = $this->forteUpdate($addr_endpoint, $addr_params, $auth_token, $organization_id);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        $http_success = ($info['http_code'] != 200 ? 0:1);

        return $http_success;

    }
    public function updateLandlordSubscriptionPayTokenDB($pay_method_token)
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $location_id = 'loc_193969';
        $landlord_pay_settings = ForteLandlordSubscriptionPaySettings::where('landlord_id',$loggedLandlordId)->first();

        $landlord_pay_settings->payment_id = Crypt::encrypt($pay_method_token);

        $landlord_pay_settings->save();
    }
    public function displaySubscriptionSettingsView()
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $profileUrl       = asset('public/uploads/landlords/');
        $noProfileUrl     = asset('public/uploads/noimage/');
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        return view('landlords.landlordsubscriptionpaymentsettings', [
            'pageTitle' => 'Subscription Payment Settings',
            'pageHeading' => 'Subscription Payment Settings',
            'pageMenuName' => 'mysubscriptionpaymentsettings',
            'profile' => $loggedLandlord,
            'loggedLandlord' => $loggedLandlord,
            'profileImage' => $profileImage
        ]);
    }
    public function getPaymentSettings()
    {
//TODO TEST THE LANDLORD GETTERS YOU WROTE IN CONTROLLER.PHP
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $profileUrl       = asset('public/uploads/landlords/');
        $noProfileUrl     = asset('public/uploads/noimage/');
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }
        $base_url          = Config::get('constants.FORTE_BASE_URL');     //production: http://api.forte.net/v3
        // Landlord Required Fields
        $organization_id   = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $location_id = 'loc_193969';
        $api_access_id     = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);

        $landlord_customer_id = $this->getLandlordCustomerId($loggedLandlordId);
        $landlord_payment_id = $this->getLandlordPaymentId($loggedLandlordId);
        $landlord_address_id = $this->getLandlordAddressId($loggedLandlordId);

        // ENDPOINT TO RETRIVE PAYMETHOD OBJECT FORTE
        $pay_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/paymethods/' . $landlord_payment_id;
        // ENDPOINT TO RETRIVE CUSTOMER'S BILLING ADDRESS
        $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $landlord_customer_id . '/addresses/' . $landlord_address_id;

        // Payment Method Info
        $ch = $this->forteGet($pay_endpoint, $auth_token, $organization_id);
        // Address Info
        $ch1 = $this->forteGet($addr_endpoint, $auth_token, $organization_id);

        $pay_response = curl_exec($ch);
        $addr_response = curl_exec($ch1);
        $info = curl_getinfo($ch);
        curl_close($ch);
        curl_close($ch1);
        $payment_data = json_decode($pay_response);
        $addr_data = json_decode($addr_response);

        if ($landlord_customer_id == "")
        {
            $landlord_payment_settings = "";
        }
        else
        {
            $st_line2_exist = property_exists($addr_data->physical_address, 'street_line2');
            $street_line2 = ($st_line2_exist != TRUE ? '':$addr_data->physical_address->street_line2);

            if (property_exists($payment_data, 'echeck'))
            {

                $pay_settings_array = json_encode(array(
                    'account_holder'    => $payment_data->echeck->account_holder,
                    'bank_name'         => $payment_data->label,
                    'account_number'    => $payment_data->echeck->masked_account_number,
                    'routing_number'    => $payment_data->echeck->routing_number,
                    'account_type'      => $payment_data->echeck->account_type,
                    'street_line1'      => $addr_data->physical_address->street_line1,
                    'street_line2'      => $street_line2,
                    'city'              => $addr_data->physical_address->locality,
                    'state'             => $addr_data->physical_address->region,
                    'zip_code'          => $addr_data->physical_address->postal_code
                ));

                $landlord_payment_settings = json_decode($pay_settings_array);
            }
            else if(property_exists($payment_data, 'card'))
            {
                $pay_settings_array = json_encode(array(
                    'card_type' => $payment_data->card->card_type,
                    'name_on_card' => $payment_data->card->name_on_card,
                    'masked_account_number' =>  $payment_data->card->masked_account_number,
                    'expire_month' => $payment_data->card->expire_month,
                    'expire_year' => $payment_data->card->expire_year,
                    'card_verification_value' => '***',
                    'street_line1'      => $addr_data->physical_address->street_line1,
                    'street_line2'      => $street_line2,
                    'city'              => $addr_data->physical_address->locality,
                    'state'             => $addr_data->physical_address->region,
                    'zip_code'          => $addr_data->physical_address->postal_code,
                    'address_type'      => $addr_data->shipping_address_type
                ));

                $landlord_payment_settings = json_decode($pay_settings_array);
            }
        }
        return view('landlords.landlordsubscriptionpaymentsettings', [
            'pageTitle' => 'Subscription Payment Settings',
            'pageHeading' => 'Subscription Payment Settings',
            'pageMenuName' => 'mysubscriptionpaymentsettings',
            'profile' => $landlordProfile,
            'loggedLandlord' => $loggedLandlord,
            'profileImage' => $profileImage,
            'paymentSettings' => $landlord_payment_settings
        ]);

    }

}
