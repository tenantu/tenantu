<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\LandlordApplication;
use Auth;
use DB;
use Crypt;
use Validator;
//use Request;
use Config;
use Redirect;
use App\Traits\ForteRequestTrait;

class LandlordApplicationController extends Controller
{
    use ForteRequestTrait;

    public function createLandlordApplication()
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $profileUrl       = asset('public/uploads/landlords/');
        $noProfileUrl     = asset('public/uploads/noimage/');
	$applicationCount = DB::table('forte_landlord_applications')->where('landlord_id', '=', $loggedLandlordId)->count();

        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        return view('landlords.landlordApplication', [
            'pageTitle' => 'Account Application',
            'pageHeading' => 'Account Application',
            'pageMenuName' => 'createapplication',
            'profile' => $loggedLandlord,
            'loggedLandlord' => $loggedLandlord,
            'profileImage' => $profileImage,
	    'applicationCount' => $applicationCount
        ]);
    }

    public function postLandlordApplication(Request $request)
    {
 	$rules = array(
            'dba_name'                 => 'required',
	    'legal_name'	       => 'required',
	    'legal_structure'	       => 'required',
	    'tax_id_number'	       => 'required',
	    'streetaddress1'	       => 'required',
	    'postal_code'	       => 'required',
	    'locality'		       => 'required',
	    'region'		       => 'required',
	    'annual_volume'	       => 'required',
	    'average_transaction_amount'=>'required',
	    'bank_routing_number'	=>'required',
	    'bank_account_number'	=>'required',
	    'account_type'		=>'required',
	    'owner_first_name'		=>'required',
	    'owner_last_name'		=>'required',
	    'owner_email_address'	=>'required',
            'owner_mobile_phone'        =>'required|regex:/^\d{3}[\-]\d{3}[\-]\d{4}$/',
	    'owner_streetaddress1'	=>'required',
	    'owner_postal_code'		=>'required',
	    'owner_locality'	  	=>'required',
	    'owner_region'		=>'required',
	    'owner_date_of_birth'	=>'required',
	    'owner_last4_ssn'		=>'required',
	    'termsconditions'		=>'required'	
        );
        $messages = [
                    ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
	
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $owner = $this->getApplicationOwnerParams($request);

        $applicant_organization = $this->getApplicantOrganizationParams($request);

        $application_params = $this->getApplicationParams($request, $owner, $applicant_organization);

        $application_output = $this->sendApplicationForte($application_params);
        if($application_output["create_http_success"] == 0){
        return redirect()->back()->with('message-error',"There was something wrong with the information you inputted on this page. Please check for erros, if problem persists contact support.");
        }
        // Source IP just in case
        $application_source_ip = $application_output['application_info']['local_ip'];
        //var_export($application_output);

        $application_data = json_decode(json_encode($application_output['data']));
        $application_applicant_org_data = $application_data->applicant_organization;
        $application_id = $application_data->application_id;

        //exit('Application ID: ' . $application_id);

        // Get DB Params
        $application_db_params = json_encode(array(
            'dba_name' => $application_applicant_org_data->dba_name,
            'application_id' => $application_data->application_id,
            'status' => $application_data->status
        ));
        echo "Database Params: ";
        echo "<br>";
        var_export($application_db_params);

        // Post to DB
        $this->saveApplicationDB(json_decode($application_db_params));
	return redirect()->route('landlords.dashboard')->with('message-success', "You're application for underwriting has been successfully submitted. You will recieve an email containing your information to set up your Management Account.");
	}
    }

    public function postLandlordApplicationAndPay(Request $request)
    	{
        $rules = array(
            'dba_name'                 => 'required',
            'legal_name'               => 'required',
            'legal_structure'          => 'required',
            'tax_id_number'            => 'required',
            'streetaddress1'           => 'required',
            'postal_code'              => 'required',
            'locality'                 => 'required',
            'region'                   => 'required',
            'annual_volume'            => 'required',
            'average_transaction_amount'=>'required',
            'bank_routing_number'       =>'required',
            'bank_account_number'       =>'required',
            'account_type'              =>'required',
            'owner_first_name'          =>'required',
            'owner_last_name'           =>'required',
            'owner_email_address'       =>'required',
            'owner_mobile_phone'        =>'required|regex:/^\d{3}[\-]\d{3}[\-]\d{4}$/',
            'owner_streetaddress1'      =>'required',
            'owner_postal_code'         =>'required',
            'owner_locality'            =>'required',
            'owner_region'              =>'required',
            'owner_date_of_birth'       =>'required',
            'owner_last4_ssn'           =>'required',
            'termsconditions'           =>'required'
        );
        $messages = [
                    ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
	

        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
	$payFee = $this->payApplicationFee($loggedLandlordId);
	if($payFee != 1)
	{
	return redirect()->back()->with('message-error',"There was something wrong with the payment information you inputted. Please check for erros, if problem persists contact support.");
	}
	else {
        $owner = $this->getApplicationOwnerParams($request);

        $applicant_organization = $this->getApplicantOrganizationParams($request);

        $application_params = $this->getApplicationParams($request, $owner, $applicant_organization);

        $application_output = $this->sendApplicationForte($application_params);
	if($application_output["create_http_success"] == 0){
        return redirect()->back()->with('message-error',"There was something wrong with the information you inputted on this page. Please check for erros, if problem persists contact support.");
	}
        // Source IP just in case
        $application_source_ip = $application_output['application_info']['local_ip'];
        //var_export($application_output);

        $application_data = json_decode(json_encode($application_output['data']));
        $application_applicant_org_data = $application_data->applicant_organization;
        $application_id = $application_data->application_id;

        //exit('Application ID: ' . $application_id);

        // Get DB Params
        $application_db_params = json_encode(array(
            'dba_name' => $application_applicant_org_data->dba_name,
            'application_id' => $application_data->application_id,
            'status' => $application_data->status
        ));
        // echo "Database Params: ";
        // echo "<br>";
        // var_export($application_db_params);

        // Post to DB
        $this->saveApplicationDB(json_decode($application_db_params));
        return redirect()->route('landlords.dashboard')->with('message-success', "Your application for underwriting has been successfully submitted. You will recieve an email containing your information to set up your Management Account.");
        }
    	}
}
    public function landlordApplicationIndex()
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordProfile  = $this->getLandlordProfile($loggedLandlordId);
        $location_id = $this->getLandlordLocationId($loggedLandlordId);
        $profileUrl = asset('public/uploads/landlords/');
        $noProfileUrl = asset('public/uploads/noimage/');
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        $landlord_applications = LandlordApplication::where('landlord_id', '=', $loggedLandlordId)->paginate(10);

        // Check if subscribed...

        return view('landlords.allRentPaymentHistory',[
              'pageTitle'     => 'Management Account Underwriting',
              'pageHeading'   => 'Management Account Underwriting',
              'pageMenuName'  => 'myapplications',
              'profile'       => $landlordProfile,
              'profileImage'      => $profileImage,
              'rent_payments' => $landlord_applications
        ]);
    }

    public function sendApplicationForte($application_params)
    {
        $organization_id = env('FORTE_API_RESELLER_ID');
        $base_url           = env('FORTE_BASE_URL');
        $api_access_id      = env('FORTE_API_ACCESS_ID');
        $api_secure_key     = env('FORTE_API_SECURE_KEY');
        $auth_token         = base64_encode($api_access_id . ':' . $api_secure_key);

        $landlord_application_endpoint = $base_url . '/organizations/' . $organization_id . '/applications';

        $landlord_new_application = $this->fortePost($landlord_application_endpoint, $application_params, $auth_token, $organization_id);

        $application_response = curl_exec($landlord_new_application);
        $application_info = curl_getinfo($landlord_new_application);
        curl_close($landlord_new_application);
        $application_data = json_decode($application_response);

        // echo '<pre>';
        // print_r($application_info['http_code']);
        // echo '<br>';
        // print_r($application_data);
        // echo '<br>';
        // echo '</pre>';
        // exit('sdf');
        $create_http_success = (($application_info['http_code'] != 201) ? 0:1);
        $applicationOutput = array(
            'create_http_success'   => $create_http_success,
            'application_info'      => $application_info,
            'data'                  => $application_data
        );

        return $applicationOutput;

    }

    public function saveApplicationDB($application_db_params)
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $application_id_pre = $application_db_params->application_id;
        $application_id = Crypt::encrypt($application_id_pre);

        $store_landlord_application = new LandlordApplication;
        $store_landlord_application->name = $application_db_params->dba_name;
        $store_landlord_application->landlord_id = $loggedLandlordId;
        $store_landlord_application->application_id = $application_id;
        $store_landlord_application->status = $application_db_params->status;
        $store_landlord_application->save();

    }

    public function getApplicationOwnerParams(Request $request)
    {
        $owner = array(
            "first_name" => $request->owner_first_name,
            "last_name" => $request->owner_last_name,
            "street_address1" => $request->owner_streetaddress1,
            "locality" => $request->owner_locality,
            "region" => $request->owner_region,
            "postal_code" => $request->owner_postal_code,
            "email_address" => $request->owner_email_address,
            "mobile_phone" => $request->owner_mobile_phone,
            "last4_ssn" => $request->owner_last4_ssn,
            "date_of_birth" => $request->owner_date_of_birth
        );
        return $owner;
    }

    public function getApplicantOrganizationParams(Request $request)
    {
        $applicant_organization = array(
            "legal_name" => $request->legal_name,
            "tax_id_number" => $request->tax_id_number,
            "legal_structure" => $request->legal_structure,
            "dba_name" => $request->dba_name,
            "street_address1" => $request->streetaddress1,
            "locality" => $request->locality,
            "region" => $request->region,
            "postal_code" => $request->postal_code,
            "customer_service_phone" => "5555236987",
            "website" => "www.tenantu.com",
            "business_type" => "2741",
            "bank_routing_number" => $request->bank_routing_number,
            "bank_account_number" => $request->bank_account_number
        );
        return $applicant_organization;
    }

    public function getApplicationParams(Request $request, $owner, $applicant_organization)
    {
        $application_params = array(
            "fee_id" => env('FORTE_APP_FEE_ID'),
            "source_ip" => $request->ip(),
            "annual_volume" => $request->annual_volume,
            "average_transaction_amount" => $request->average_transaction_amount,
            "market_type" => "internet",
            "t_and_c_version" => "16.06.01",
            "t_and_c_time_stamp" => date("m-d-Y"),
            "applicant_organization" => $applicant_organization,
            "owner" => $owner
        );
        return $application_params;
    }

    public function chargeApplicationFeeView()
    {

    }

    public function payApplicationFee($landlordId)
    {
        $base_url          = env('FORTE_BASE_URL');     //production: http://api.forte.net/v3
        // Landlord Required Fields
        $organization_id   = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $location_id       = env('FORTE_TENANTU_MERCHANT_LOC_ID');
        $api_access_id     = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key    = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
        $endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions';

        $landlordCustomerId = $this->getLandlordCustomerId($landlordId);
        $landlordAddressId = $this->getLandlordAddressId($landlordId);
        $addrEndpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $landlordCustomerId . '/addresses/' . $landlordAddressId;
        $landlordBillingAddress = $this->forteGet($addrEndpoint, $auth_token, $organization_id);
        $addrResponse = curl_exec($landlordBillingAddress);
        $addrResponseInfo = curl_getinfo($landlordBillingAddress);
        curl_close($landlordBillingAddress);
        $billingAddressData = json_decode($addrResponse, true);
        $reference_id = 'MgmtApp';

        $transaction_params = array(
            'action' => 'sale',
            'customer_token'=>$landlordCustomerId,
            'authorization_amount' => 29.99,
            'billing_address' => $billingAddressData,
            'reference_id'  => $reference_id
          );

        $feeTransactionPay = $this->makeApplicationFeeTransaction($transaction_params, $landlordId, $location_id, $organization_id);

        // Post Responses
        $postTransResponse = json_decode(json_encode($feeTransactionPay['data']))->response;
        $postTransData = json_decode(json_encode($feeTransactionPay['data']));

        if($postTransResponse->response_code != 'A01')
        {
            echo 'FAIL';
            return 0;
        }
        else
        {
            // Go Ahead with submitting the application.
            return 1;
        }
    }

    public function makeApplicationFeeTransaction($feeTransactionParams, $landlordId, $location_id, $organization_id)
    {
        $base_url          = env('FORTE_BASE_URL');     //production: http://api.forte.net/v3
        $api_access_id      = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
        $api_secure_key     = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
        // Landlord Required Fields
        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
        $endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions';

        $feeTransactionPay = $this->fortePost($endpoint, $feeTransactionParams, $auth_token, $organization_id);
        $feePayResponse = curl_exec($feeTransactionPay);
        $feePayInfo = curl_getinfo($feeTransactionPay);
        curl_close($feeTransactionPay);
        $feePayData = json_decode($feePayResponse);
        $create_http_success = (($feePayInfo['http_code'] != 201) ? 0:1);

        $createOutput = array(
            'create_http_success' => $create_http_success,
            'data'                => $feePayData
        );

        return $createOutput;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
