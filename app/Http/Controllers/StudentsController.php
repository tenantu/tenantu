<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Auth;
use Validator;

class StudentsController extends Controller
{
    public function getLogin(){
		return view('students.login');
	}
		public function postLogin( Request $request ){
		/*$rules = array(
        'email'            => 'required|email|unique:ducks',     // required and must be unique in the ducks table
        'password'         => 'required'
    );*/
    $rules = array(
        'email'            => 'required|email|',     // required and must be unique in the ducks table
        'password'         => 'required'
    );

    // do the validation ----------------------------------
    // validate against the inputs from our form
    $validator = Validator::make($request->all(), $rules);
		//dd($request->all());
		//echo "123";
		 if ($validator->fails()) {
			 //echo "121";exit;
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		 }
		 else{
			 
		if (Auth::attempt("student", ['email' => $request->input('email'), 'password' => $request->input('password'),'is_active'=>1])){
		    echo "OK";
		    //dd(Auth::User('student'));
		    echo Auth::User('student')->id;
		    return redirect('student/dashboard');
	    }
	    else{
			echo "NOT OK";
		}
	}
	}
	public function dashboard(){
		echo "123";
	}
	public function logout(){
		Auth::logout();
		return redirect('student/login');
	}
}
