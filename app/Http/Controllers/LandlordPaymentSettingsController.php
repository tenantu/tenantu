<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\PaymentSetting;
use App\Http\Models\Setting;
use DB;
use DateTime;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_MerchantAccount;

class LandlordPaymentSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */







    //////////////////////////////////////////////////////////
    // General Landlord getPayment Settings unique to Forte //
    //////////////////////////////////////////////////////////
    public function getLandlordLocationId($landlordId)
    {
        $landlord_pay_receive = DB::table('forte_landlord_pay_settings_receive')
            ->where('landlord_id','=',$landlordId)->first();

        $landlord_loc_id = $landlord_pay_receive->loc_id;
        return $landlord_loc_id;
    }

    public function getLandlordOrgId($landlordId)
    {
        $landlord_pay_receive = DB::table('forte_landlord_pay_settings_receive')
            ->where('landlord_id','=',$landlordId)->first();

        $landlord_org_id = $landlord_pay_receive->org_id;
        return $landlord_org_id;
    }

    //////////////////////////////////////////////////////////////
    // END General Landlord getPayment Settings unique to Forte //
    //////////////////////////////////////////////////////////////
       

    /////////////////////////////////////////////////////////
    // Get Payment Settings of Current of Logged In Tenant //
    /////////////////////////////////////////////////////////
    

    public function getTenantCurrentLandlordLocationId($tenantId)
    {
        $currentlandlord_pay = DB::table('landlord_tenants')
            -> join('forte_landlord_pay_settings_receive', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_receive.landlord_id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId);
            })
                ->first();
        $currentlandlord_loc_id = $currentlandlord_pay->loc_id;
        return $currentlandlord_loc_id;
    }

    public function getTenantCurrentLandlordOrgId($tenantId)
    {
        $currentlandlord_pay = DB::table('landlord_tenants')
            -> join('forte_landlord_pay_settings_receive', function ($join) use ($tenantId) {
                $join->on('landlord_tenants.landlord_id', '=', 'forte_landlord_pay_settings_receive.landlord_id')
                    ->where('landlord_tenants.tenant_id', '=', $tenantId);
            })
                ->first();
        $currentlandlord_org_id = $currentlandlord_pay->org_id;
        return $currentlandlord_org_id;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
