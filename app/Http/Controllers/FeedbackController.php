<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Cmspage;
use App\Http\Models\Faq;
use App\Http\Models\Feedback;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $feedbackQuery = DB::table('feedbacks');
        if ($request->has('search')) {
            $search = $request->input('search');
            $feedbackQuery->where('company_name', 'like', '%' . $request->input('search') . '%');
        }
        $feedbackQuery->orderBy($sortby, $order);
        $feedbacks = $feedbackQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('feedbacks.index', ['pageTitle' => 'Testimonials',
            'pageHeading'                          => 'Testimonials for showing in the home page',
            'pageDesc'                             => 'Manage Testimonials here',
            'feedbacks'                            => $feedbacks,
            'search'                               => $search,
            'sortby'                               => $sortby,
            'order'                                => $order,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        return view('feedbacks.create', ['pageTitle' => 'Testimonials Page',
            'pageHeading'                     => 'Testimonials for showing in the home page',
            'pageDesc'                        => 'Create Testimonials here',
        ]);
    }
  

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = [
			'clientname'	   => 'required',
            'companyname'      => 'required',
            'content'          => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
         if (!$validator->fails())
         {
			//print_r($request->all());exit;
			$input['client_name'] 	= $request->clientname;
			$input['company_name']	= $request->companyname;
			$input['description']	= $request->content;
			$input['status']		= $request->status;
			
			$feedback = Feedback::create($input);
			return redirect()->route('feedback.index')->with('message', 'Testimonial Created.');
		 }
		 else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feedback   = Feedback::find($id);
        return view('feedbacks.edit', ['pageTitle' => 'Edit testimonials',
            'pageHeading'                               => 'Edit testimonials',
            'pageDesc'                                  => 'Edit testimonials here',
            'feedback'                                  => $feedback,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
			'clientname'	=> 'required',
            'companyname'  	=> 'required',
            'content'    	=> 'required',
        ];
        
         $messages = [
			'clientname.required' => 'Client name is required',
            'companyname.required'  => 'CompanyName is required.',
            'content.required' 	=> 'Content is required.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
         if (!$validator->fails()) 
         {
            $updatedFeedback = Feedback::where('id', $id)
									->update(['client_name'=>$request->clientname,'company_name'=>$request->companyname,'description'=>$request->content]);
			
            return redirect()->route('feedback.index')->with('message', 'Testimonial details are updated.');
            
         }
         else
         {
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		 }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feedback = Feedback::find($id);
        $feedback->delete();
        return redirect()->route('feedback.index')->with('message', 'Testimonial Deleted.');
    }
    
    public function changeStatus($id)
    {
        $feedback = Feedback::find($id);
       // dd($feedback);
        if ($feedback->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $feedback->update($data);
        return redirect()->route('feedback.index')->with('message', 'Testimonial status updated');
    }
}
