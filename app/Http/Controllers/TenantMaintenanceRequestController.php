<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\LandlordTenant;
use App\Http\Models\TenantMaintenanceRequest;
use App\Http\Models\MaintenanceAvailableTimes;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use Config;
use Validator;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Response;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class TenantMaintenanceRequestController extends Controller{
  private function tenantHasProperty($tenantId)
	{
		$tenantPropertyId = LandlordTenant::where('tenant_id', '=', $tenantId)->first();
		return (($tenantPropertyId === null) ? false : true);
	}

  public function getMyMaintenanceRequests($sortBy, $sortOrder)
  {
    $loggedTenant = Auth::user('tenant');
    $loggedTenantId = $loggedTenant->id;
    $tenantProfile 	= $this->getTenantProfile($loggedTenantId);
    $imageUrl       = asset('public/uploads/tenants/');
    $tenantAssociatedWithLandlord = $this->isTenantAssociatedWithLandlord($loggedTenantId);

    if ($this->tenantHasProperty($loggedTenantId))
    {
      $tenantsPropertyId = $this->getTenantCurrentPropertyId($loggedTenantId);
      if($tenantsPropertyId != false){
      $tenantsPropertyName = $this->getPropertyNameFromId($tenantsPropertyId);
      //$maintenanceRequests = DB::table('maintenance_request')->where('property_id', $tenantsPropertyId);
      if($sortBy == "Default"){
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', $tenantsPropertyId)->paginate(10);
      }
      else if($sortBy == "Date"){
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', $tenantsPropertyId)->orderBy('date_encountered',$sortOrder)->paginate(10);
      }
      else if($sortBy == "Property"){
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', $tenantsPropertyId)->orderBy('property_id',$sortOrder)->paginate(10);
      }
      else if($sortBy == "Type"){
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', $tenantsPropertyId)->orderBy('issue_type_id',$sortOrder)->paginate(10);
      }
      else if($sortBy == "Description"){
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', $tenantsPropertyId)->orderBy('issue_desc',$sortOrder)->paginate(10);
      }
      else if($sortBy == "Status"){
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', $tenantsPropertyId)->orderBy('status',$sortOrder)->paginate(10);
      }
      }
      else{
	$maintenanceRequests = TenantMaintenanceRequest::where('property_id', '=', '0')->paginate(10); //empty query because setting as false and using blade conditionals broke view
	$tenantsPropertyName = false;
      }
    return view('tenants.myMaintenanceRequests', [
      'pageTitle'   	=> 'My Maintenance Requests',
      'pageHeading' => 'My Maintenance Requests',
      'pageMenuName'	=> 'myMaintenanceRequests',
      'profile' 		=> $tenantProfile,
      'imageUrl' 		=> $imageUrl,
      'maintenanceRequests' => $maintenanceRequests,
      'isTenantAssociatedWithLandlord'=> $tenantAssociatedWithLandlord,
      'propertyName' => $tenantsPropertyName
    ]);
    }

    else
    {
      Session::flash('warningMessage', "You must be connected to a landlord and property to view maintenance requests!");
      return redirect()->route('tenants.getMyLandlords');
    }
  }

}
