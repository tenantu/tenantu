<?php
 /********************************************************/
//Company Name     : Offshorent Solutions Pvt Ltd
//Project Name 	   : Tenantu
//Page Description : Index Controller
//Created By       : sooraj
//Created On       : 16-10-2015
//Modified By      :
//Modified On      :
/*******************************************************/

namespace App\Http\Controllers;

use App\Http\Models\PropertyPhysical;
use Socialite;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Paginator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers;
use App\Http\Controllers\LandlordSubscriptionController;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\PropertyAmenity;
use App\Http\Models\PropertyReview;
use App\Http\Models\School;
use App\Http\Models\ContactUser;
use App\Http\Models\Cmspage;
use App\Http\Models\Faq;
use App\Http\Models\Mysearch;
use App\Http\Models\Feedback;
use App\Http\Models\Enquiry;
use App\Http\Models\SocialMedia;
use App\Http\Models\PropertySearch;
use App\Http\Models\BannerSetting;
use App\Http\Models\BannerPurchase;
use App\Http\Models\FeaturedSetting;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantEmailInvite;
use App\Http\Models\LandlordSubscription;
use App\Http\Models\preEvApplications;
use DB;
use Config;
use Crypt;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Contracts\Encryption\DecryptException;

class IndexController extends Controller
{

	public function __construct(\Illuminate\Auth\Guard $guard)
	{
		$this->auth = $guard;
	}

	public function login()
	{
		$school = $this->getSchoolDetails();
		$loginButton = $this->checkIfLoggedIn();
		return view('index.login', ['pageTitle' => 'Login',
            'school'      => $school,
	    'loginButton' => $loginButton
        ]);
	}
	public function getAreYouLandlord(){
		$school = $this->getSchoolDetails();

		return view('index.areYouLandlord', ['pageTitle' => 'Are you a Landlord?',
						     'school' => $school]);
	}

	//these func is used for getting the school
     public function  getSchoolDetails()
     {
		 $school  = array();
		 $school  = DB::table('schools')->where('is_active',1)->orderBy('schoolname', 'asc')->lists('schoolname', 'id');
		 return $school ;
	 }

	 public function pricing() {
         $loginButton = $this->checkIfLoggedIn();
         return view('index.pricing', ['pageTitle' => 'Pricing',
             'loginButton' => $loginButton]);
     }


    public function ourStory() {
        $loginButton = $this->checkIfLoggedIn();
        return view('index.ourStory', ['pageTitle' => 'Our Story',
            'loginButton' => $loginButton]);
    }

	public function postLogin(Request $request )
	{
		$rules = array(
			'email'           	=> 'required|email|',     // required and must be unique in the ducks table
			'password'         	=> 'required',

		);
		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			 return redirect()->back()->withErrors($validator->errors())->withInput()->with('login-failed',1);
		}
		else
		{
			if(Auth::attempt("tenant", ['email' => $request->input('email'), 'password' => $request->input('password'),'is_active'=>1]))
			{
				return redirect()->intended(route('tenants.dashboard'));
			}
			elseif(Auth::attempt("landlord", ['email' => $request->input('email'), 'password' => $request->input('password'),'is_active'=>1]))
			{
				return Redirect::intended(route('landlords.dashboard'));
			}

			else
			{
				return redirect()->back()->with('message-error','Wrong email or password');
				return Redirect::route('index.login');
			}


		}
	}


	/**
     * Redirect the user to the facebook authentication page.
     *
     * @return Response
     */

	public function redirectToProvider(Request $request)
    {
		//echo $request->id;exit;
		Session::put('userType', $request->id);
        return Socialite::driver('facebook')
            ->scopes(['email'])->redirect();


    }
     /**
     * Obtain the user information from facebook.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user 		= Socialite::driver('facebook')->user();
        $input					= array();
        $input['firstname'] 	= $user->user['first_name'];
        $input['lastname'] 		= $user->user['last_name'];
        $input['email'] 		= $user->user['email'];
        $input['fb_id']    		= $user->id;
        $input['is_active']		= 1;
        $input['is_fb_user']	= 1;

        /*$input				= array();
        $input['firstname'] 	= 'sooraj';
        $input['lastname'] 		= 'r';
        $input['email'] 		= 'soorajfouryou@gmail.com';
        $input['fb_id']    		= '234234';
        $input['is_active']		= 1;
        //print $input['firstname'];
        $input['is_fb_user']	= 1;*/


        $registrationStatus = $this->isRegisteredFacebook($input['email']);
        //print_r($registrationStatus['status']);
        // if not registered show options for sign up
        if ($registrationStatus['status']=='0')
        {
			$school = $this->getSchoolDetails();
			return view('index.facebook-auth', ['pageTitle' => 'facebookauth',
            'inputs'      => $input,
            'school'	  => $school,
        ]);
		}
		else
		{
            $dashBoard = $this->processFbUser($registrationStatus,$registrationStatus['userType'],$input);
            return Redirect::route($dashBoard['url']);
		}

    }

    public function facebookSignUp(Request $request)
    {
		$input	= array();
		$userTypeValue 		= $request->usertype;
		$registrationStatus = array('status'=>'0');
		$input['firstname'] = $request->firstname;
		$input['lastname'] 	= $request->lastname;
		$input['email'] 	= $request->email;
		$input['fb_id'] 	= $request->fb_id;
		$input['is_active'] = $request->is_active;
		$input['is_fb_user']= $request->is_fb_user;
		$input['school_id'] = $request->school_id;

		$dashBoard = $this->processFbUser($registrationStatus,$userTypeValue,$input);
		$dashBoardType = $dashBoard['type'];
		if($dashBoardType=='tenant')
		{
			$messageSuccessType = 'tenant';
		}
		else
		{
			$messageSuccessType = 'landlord';
		}
        return Redirect::route($dashBoard['url'])->with('message-success','You are successfully registered as '.$messageSuccessType.' using facebook');
	}

    public function processFbUser($registrationStatus,$userTypeValue,$input)
    {
		 switch($registrationStatus['status'])
        {
			case "0":
				//print ' case 0 ';
				if($userTypeValue == 'tenant')
				{
					$tenant = Tenant::create($input);
					$x = Auth::loginUsingId("tenant", $tenant->id);
					return array('url'=>'tenants.dashboard','type'=>'tenant');
				}
				if($userTypeValue == 'landlords')
				{
					$landlord = Landlord::create($input);

					Auth::loginUsingId("landlord", $landlord->id);
					return array('url'=>'landlords.dashboard','type'=>'landlord');
				}
			break;

			case "1":
			//print ' case 1 ';
				if($userTypeValue == 'tenant')
				{
					$objTenant = Tenant::where('email', '=', $input['email'])->firstOrFail();
					Auth::loginUsingId("tenant", $objTenant->id);
					return array('url'=>'tenants.dashboard');
					//return Redirect::route('tenants.dashboard');
				}
				if($userTypeValue == 'landlords')
				{
					$objLandlord = Landlord::where('email', '=', $input['email'])->firstOrFail();
					Auth::loginUsingId("landlord", $objLandlord->id);
					return array('url'=>'landlords.dashboard');
					//return Redirect::route('landlords.dashboard');
				}
			break;

			case "fb_user";
			//print ' fb_user ';
				if($userTypeValue == 'tenant')
				{
					$objTenant = Tenant::where('email', '=', $input['email'])->firstOrFail();
					Auth::loginUsingId("tenant", $objTenant->id);
					return array('url'=>'tenants.dashboard');
					//return Redirect::route('tenants.dashboard');
				}
				if($userTypeValue == 'landlords')
				{
					$objLandlord = Landlord::where('email', '=', $input['email'])->firstOrFail();
					Auth::loginUsingId("landlord", $objLandlord->id);
					return array('url'=>'landlords.dashboard');
					//return Redirect::route('landlords.dashboard');
				}

			break;

			case "normal_user":
			//print ' normal_user ';
				$inputArr					= array();
				$inputArr['fb_id']    		= $input['fb_id'];
				$inputArr['is_fb_user']		= 1;
				$inputArr['is_active']		= 1;
				if($userTypeValue == 'tenant')
				{
					$affectedRows = Tenant::where('email', '=', $input['email'])->update($inputArr);
					$objTenant = Tenant::where('email', '=', $input['email'])->firstOrFail();
					$type='tenants';
					$typeTable = "tenant";

					Auth::loginUsingId("tenant", $objTenant->id);
					return array('url'=>'tenants.dashboard');
					//return Redirect::route('tenants.dashboard');

				}
				if($userTypeValue == 'landlords')
				{
					$affectedRows = Landlord::where('email', '=', $input['email'])->update($inputArr);
					$objLandlord = Landlord::where('email', '=', $input['email'])->firstOrFail();
					$type='landlords';
					$typeTable = "landlord";

					Auth::loginUsingId("landlord", $objLandlord->id);
					return array('url'=>'landlords.dashboard');
					//return Redirect::route('landlords.dashboard');

				}


			break;
		}
	}

    //this function is used for the getting of password
    private function getUserPassword($userEmail,$type)
    {

		$users = DB::table($type)
					 ->select('password')
                     ->where('email', '=',$userEmail)
                     ->get();
        return $users;
	}

	//this function is used for the updation of password

	private function updateUserPassword($userEmail,$userPassword,$type)
	{
		DB::table($type)
            ->where('email', $userEmail)
            ->update(['password' => $userPassword]);
	}
	//this function is used for rendering the forgot password view

	public function getForgotPassword()
	{
		// echo "hi";exit;
		return view('index.forgotpassword', ['pageTitle' => 'Forgotpassword'
        ]);
	}

	public function postForgotPassword(Request $request)
	{
		$rules = array(
			'email'           	=> 'required|email',
		);
		$messages = array(
		'email.required' =>'Please enter your registered email'
		);
		$validator = Validator::make($request->all(), $rules,$messages);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}
		else
		{
			//$postedUserType = $request->usertype;
			$postedEmail = $request->email;
			$emailRegistered = $this->checkEmailRegistered($postedEmail);
			if(empty($emailRegistered))
			{
				return redirect()->back()->with('message-error','The email entered is not registered');
			}
			$activationMail = $this->forgotPasswordMail($postedEmail);
			if ($activationMail)
			{
				return redirect()->route('index.forgotPassword')->with('message-success', 'Mail sent  please check your email');
			}
			else
			{
				return redirect()->back()->withErrors('message-error', 'Not a valid email');
			}
		}
	}

	public function forgotPasswordMail($postedEmail)
	{
		$selectUserType = DB::select('SELECT email, "tenant" as userType FROM tenants WHERE email  = "'.$postedEmail.'"
									UNION
									SELECT email, "landlord" as userType FROM landlords WHERE email ="'.$postedEmail.'" ');
		$userTypeVal = $selectUserType[0]->userType;

		if($userTypeVal=='tenant')
		{
			$dataWithEmail = Tenant::where('email', '=', $postedEmail)->first();

		}
		else
		{
			$dataWithEmail = Landlord::where('email', '=', $postedEmail)->first();
		}
		//print_r($dataWithEmail);exit;
		if(!empty($dataWithEmail))
			{
				$currentDate 	= Carbon::now();
				$key 		 	= $currentDate->getTimestamp();
				$password_token = hash_hmac('sha256', str_random(40), $key);
				$originalToken 	= $key."_".$password_token;
				$dataWithEmail->password_token = $originalToken;
				$dataWithEmail->save();
				Mail::send('emails.passwordreset',['token' => $originalToken,'name'=>$dataWithEmail->firstname,'userType'=>$userTypeVal], function($message) use ($dataWithEmail){

					$message->to($dataWithEmail->email, $dataWithEmail->name)
							->subject('Tenantu - Password Reset');
				});

				return true;
			}
			else
			{
				return false;
			}
	}

	public function getResetPassword($token,$userType)
	{
		if($token!='' && $userType!='')
		{
			$pageTitle = 'Password reset';
			return view('index.resetpassword')->with(compact(array('pageTitle','token','userType')));
		}

	}

	//this function is used for actual resetting the user password
	public function postResetPassword(Request $request)
	{
		$rules = array(
			'token' 			=> 'required',     // required and must be unique in the ducks table
			'userType'			=>	'required',
			'password' 			=> 'required|min:6|confirmed',
		);

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}


        if($request->token!='' && $request->userType=='tenant')
        {
			$updatedPassword = DB::table('tenants')
						->where('password_token',$request->token)
						->update(['password' =>bcrypt($request->password)]);

			$updatedPassword = DB::table('tenants')
						->where('password_token',$request->token)
						->update(['password_token' =>'']);

		}
		if($request->token!='' && $request->userType=='landlord')
		{
			$updatedPassword = DB::table('landlords')
						->where('password_token',$request->token)
						->update(['password' =>bcrypt($request->password)]);

			$updatedPassword = DB::table('landlords')
						->where('password_token',$request->token)
						->update(['password_token' =>'']);
		}
		return redirect('/login')->with('message','Your password has been changed you can login here');
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

		//echo 'session = '.Session::get('schoolName');
		$sessionSchoolId = Session::get('schoolName');
		$pageTitle 				= 'HomePage';
		$school 				= $this->getSchoolDetails();
		$testimonials 			= $this->getClientTestimonials();
		$featuredProperties 	= $this->getFeaturedProperties($sessionSchoolId);
		$reviewCount= array();

		foreach($featuredProperties as $featuredProperty)
		{
			$reviewCount = $this->getPropertyReviewCount($featuredProperty->id);
			$featuredProperty->reviewCount=$reviewCount[0]->reviewCount;
		}
		$schoolDiv='';
		if (Session::has('schoolImage')) {
		 $schoolImage =  Session::get('schoolImage');
		 $schoolDiv = "home-school-bg";
		}
		else
		{
			$schoolImage= asset('public/img/delaware.jpg');
			$schoolDiv='home-school-bg';
		}
		$featuredPropertiesStatus = $this->updateFeaturedPropertiesSatatus();
		//print($schoolImage);	exit;
		$loginButton = $this->checkIfLoggedIn();
        return view('index.index', ['pageTitle' => 'live easy',
            'school'      		=> $school,
            'testimonials'   	=> $testimonials,
            'featuredProperties'=>$featuredProperties,
            'schoolImage' 		=> $schoolImage,
            'schoolDiv'			=> $schoolDiv,
	    'loginButton'		=> $loginButton

        ]);
    }

    public function features()
    {
     $loginButton = $this->checkIfLoggedIn();
     return view('index.features', ['pageTitle' => 'Features',
				    'loginButton' => $loginButton]);
    }

    public function signup()
    {
     $school = $this->getSchoolDetails();
     $loginButton = $this->checkIfLoggedIn();
     return view('index.signup', ['pageTitle' => 'Sign up',
				  'school' => $school,
				  'loginButton' => $loginButton]);
    }

    private function updateFeaturedPropertiesSatatus()
    {

		DB::connection()->enableQueryLog();
		$featuredStatus = DB::table('properties')
            ->where('featured_end','<=', date('Y-m-d H:i:s'))
            ->where('featured_end','<>','0000-00-00 00:00:00')
            ->update(array('featured' => 0));
        $queries = DB::getQueryLog();
        //dd($queries);
        return $featuredStatus;
	}

    public function postSignup(Request $request)
    {
		$userTypeValue = $request->usertype;
		//set edu rule based on if theres something after hash that looks like our inv link
		if($userTypeValue=='student'){
			$type='tenants';
		        $rules = [
           		 'firstname'             => 'required',
           		 'lastname'              => 'required',
          		 'school_id'             => 'required',
           		 'password'              => 'required|min:6|confirmed',
           		 'password_confirmation' => 'required|min:6',
           		 'email'                 => 'required|edu',
           		 'usertype'              => 'required',
			 'dob'			 => 'required'
        		];
		}
		else{
                        $rules = [
                         'firstname'             => 'required',
                         'lastname'              => 'required',
                         'school_id'             => 'required',
                         'password'              => 'required|min:6|confirmed',
                         'password_confirmation' => 'required|min:6',
                         'email'                 => 'required',
                         'usertype'              => 'required'
                        ];
			$type='landlords';
		}
        $messages = [
            'firstname.required'  	=> 'Firstname is required.',
            'lastname.required'  	=> 'Lastname is required.',
            'school_id.required'  	=> 'School is required.',
	    'dob.required'		=> 'Birthday is requred.',
            'password.required' 	=> 'Password is required.',
            'password_confirmation.required' 	=> 'Confirm password is required.',
	    'email.required'		=> 'Email is required.',
	    'email.edu'			=> 'An "edu" email is required.',
            'usertype.required' 	=> 'user type is required.'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
		if ($validator->fails())
		{
			 return redirect()->to($request->header('referer'). $request->hashContent)->withErrors($validator->errors())->withInput()->with('reg-failed',1);
		}
		else
		{
			if($type=='tenants'){
			$dobArray = explode("/",$request->dob);
			$dobFormatted = $dobArray[2] . $dobArray[0] . $dobArray[1];
			$dobTenantFormat = $dobArray[2] . "-" . $dobArray[0] . "-" . $dobArray[1];;
                        $input['dob'] = $dobTenantFormat;
			}
			$input				=  array();
			$input['firstname'] =  $request->firstname;
			$input['lastname'] 	=  $request->lastname;
			$input['school_id'] =  $request->school_id;
			$input['email'] 	=  $request->email;
			$input['password'] 	=  bcrypt($request->password);
			$input['is_normal_user'] 	=  1;

			$registrationStatus = $this->isRegistered($input['email'],$userTypeValue);
			//dd($registrationStatus);
			switch($registrationStatus['status'])
			{


				case "0":

					if($userTypeValue=='student')
					{
						$inputPreApp = array();
						$inputPreApp['Birthdate']	= $dobFormatted;
						$inputPreApp['SchoolId']	= $this->getSchoolSchoolId($request->school_id)[0]->schoolid;
						$inputPreApp['FirstName']	= $request->firstname;
						$inputPreApp['LastName']	= $request->lastname;
						$inputPreApp['SchoolName']	= $this->getSchoolWithId($request->school_id)['schoolname'];
						$addingPreApp = preEvApplications::create($inputPreApp);
						if($request->hashContent != ""){
							if(strlen($request->hashContent)>150){
							$hashString = Crypt::decrypt($request->hashContent);
							$hashString = substr($hashString,0);
							$hashStringArray = explode("+", $hashString);
							if(sizeof($hashStringArray) == 12){
								$landlord_id = $hashStringArray[0];
								$property_id = $hashStringArray[1];
								$month_rent_initial = $hashStringArray[2];
								$due_on = $hashStringArray[3];
								$cycle = $hashStringArray[4];
								$monthStart = $hashStringArray[5];
								$dayStart = $hashStringArray[6];
								$yearStart = $hashStringArray[7];
								$agreement_start_date = $hashStringArray[8];
								$agreement_end_date = $hashStringArray[9];
								$loopCount = $hashStringArray[10];
								$factor = $hashStringArray[11];
								$isEmailInvitedTenant = landlordTenantEmailInvite::where(["landlord_id" => $landlord_id, "tenant_invite_email" => $request->email])->first();
								if($isEmailInvitedTenant){

									$tenant = Tenant::create($input);
									$newTenantId = $tenant->id;
									$inputTwo = array();
									if(strtotime($agreement_start_date)>time()){
									$inputTwo['active'] = 2;
									}
									else{
									$inputTwo['active'] = 1;
									}
									$inputTwo['landlord_id'] = $landlord_id;
									$inputTwo['tenant_id'] = $newTenantId;
									$inputTwo['property_id'] = $property_id;
									$inputTwo['month_rent_initial'] = $month_rent_initial;
									$inputTwo['due_on'] = $due_on;
									$inputTwo['cycle'] = $cycle;
									$inputTwo['agreement_start_date'] = $agreement_start_date;
									$inputTwo['agreement_end_date'] = $agreement_end_date;
									$property = LandlordTenant::create($inputTwo);
							// If cycle was selected...
                					if($loopCount!=""){
                    						$billDate = $yearStart.'-'.$monthStart.'-'.$due_on;
                    						//echo $billDate;exit;
                    						for($i=1;$i<=$loopCount/$factor;$i++){
                        						$uId = uniqid();
                        						$nextpaymonth = $i * $factor;
                        						$nextBillDate = date("Y-m-d", strtotime("+".$nextpaymonth." month", strtotime($billDate)));

        						                $data[] = array('landlord_tenant_id'=>$property->id,
                                        						'bill_on'=>$nextBillDate,
                                        						'amount'=>(($month_rent_initial) * $factor),
                                        						'unique_id'=>$uId,
                                        						'created_at'=>date('Y-m-d H:i:s'),
                                        						'updated_at'=>date('Y-m-d H:i:s'));
                    						}
                    							$success = landlordTenantMonthlyrent::insert($data);
		                				}
							//if a property is per unit then when a tenant is added we need to make sure it is reflected to other tenants in that unit, per bed however doesnt need this
							if($this->getPropertyPerBed($request->poperty_id)[0]["charge_per_bed"] == 0){
							if($this->getPropertyCurrentTenantCount($property_id)>0){
                                                                        $updateTenants = $this->updateOtherTenantsRent($property_id, $loopCount, $factor, $due_on,$monthStart,$yearStart);
                                                                }
							}
                                                	$activationMail = $this->activationMail($userTypeValue,$input['firstname'],$input['email']);
                                                	return redirect()->back()->with('message-success','You are registered as a tenant and activation link is mailed to you');
							}
							else{
							return redirect()->to($request->header('referer'). $request->hashContent)->withInput()->with('message-error-signup', "The email you are trying to sign up with does not match the one which your landlord provided. Please contact your landlord about this matter.");
							}
							}
							}
							else{
							return redirect()->to($request->header('referer'). $request->hashContent)->withInput()->with('message-error-signup', "Something was wrong with you're sign up link, try clicking it again in your email and if it still does not work contact your landlord.");
							}
							}
						if($request->hashContent == ""){
						$tenant = Tenant::create($input);
						$activationMail = $this->activationMail($userTypeValue,$input['firstname'],$input['email']);
                                                return redirect()->back()->with('message-success','You are registered as a tenant and activation link is mailed to you');
						}
					}
					else
					{
						$landlord = Landlord::create($input);
						$this->makeLandlordFreeSubscription($landlord->id);
						$activationMail = $this->activationMail($userTypeValue,$input['firstname'],$input['email']);
						return redirect()->back()->with('message-success','You are registered as a landlord and activation link is mailed to you ');
					}
				break;

				case "1":

					return redirect()->route('index.login')->with('message-error', 'You are already registered!');
				break;

				case 'fb_user':

					if($userTypeValue=='student')
					{
						$updatePasswordTenant = DB::table('tenants')
												->where('email',$input['email'])
												->update(['password' => $input['password'],'is_normal_user'=>1]);

						return redirect()->route('index.login')->with('message-success', 'You are registered as a tenant you can login now!');
					}
					else
					{
						$updatePasswordLandlord = DB::table('landlords')
												->where('email',$input['email'])
												->update(['password' => $input['password'],'is_normal_user'=>1]);

						return redirect()->route('index.login')->with('message-success', 'You are registered as a landlord you can login now!');
					}
				break;

				case 'normal_user':
					if($type==$registrationStatus['userType'])
					{

						//return redirect()->route('index.login')->with('message-error-signup', 'This email is already registered!');
						return redirect()->back()->with('message-error-signup','This email id has already been used!');

					}


				break;

			}



		}
	}

	//this method is used to add an entry to the landlord_subscription db table for a landlord's
	//initial free subscription
	private function makeLandlordFreeSubscription($landlordId)
	{
		$subscriptionInputs = array();
		$subscriptionInputs['landlord_id'] = $landlordId;
		$subscriptionInputs['landlord_subscription_schedule_id'] = 0;
		//the subscription plan id of the free plan
		$subscriptionInputs['subscription_plan_id'] = 1;
		$subscriptionInputs['monthly_payment_amount'] = 0;
		$subscriptionInputs['payment_frequency'] = 0;
		$subscriptionInputs['management_group_count'] = 0;
		$subscriptionInputs['property_count'] = 0;
		$subscriptionInputs['active_status'] = 1;
		$currentDate = Carbon::now();
		$subscriptionInputs['join_date'] = $currentDate;

		$landlordSubscription = LandlordSubscription::create($subscriptionInputs);
		return $landlordSubscription;
	}

	//this function is used to check if the user registered via fb then update there password

	private function isRegistered($email,$userType)
	{


		if($userType=='student')
		{
			$typeVal='tenants';
		}
		else
		{
			$typeVal='landlords';
		}

		/*$users = DB::table($typeVal)
					 ->select('email','is_fb_user','is_normal_user')
                     ->where('email', '=',$email)
                     ->get();*/


		$users = DB::select('SELECT email, is_fb_user, is_normal_user
							FROM tenants
							WHERE email ="'.$email.'"
							UNION
							SELECT email, is_fb_user, is_normal_user
							FROM landlords
							WHERE email = "'.$email.'"');


        //not a registered user
        if(count($users)==0)
        {
			return array('status'=>0,'userType'=>$typeVal);
		}

		 if($users[0]->is_fb_user==1 && $users[0]->is_normal_user==1)
		 {
			return array('status'=>1,'userType'=>$typeVal);//Regstrerd as both type

		 }
		 elseif($users[0]->is_fb_user==1 && $users[0]->is_normal_user==0)
		 {
			 return array('status'=>'fb_user','userType'=>$typeVal);//registered as fb only
		 }

		 else
		 {
			 return array('status'=>'normal_user','userType'=>$typeVal);//registered as normal only
		 }


	}



	//this function is used for the email activation of registration

	public function activationMail($userTypeValue,$name,$email)
	{
		if($userTypeValue=='student')
		{
			$typeValue ='tenant';
			$dataWithEmail 	 = Tenant::where('email', '=', $email)->first();
		}
		if($userTypeValue=='landlord')
		{
			$typeValue ='landlord';
			$dataWithEmail 	 = Landlord::where('email', '=', $email)->first();
		}

		$curDate 			  = Carbon::now();
		$key                  = $curDate->getTimestamp();
        $token       		  = hash_hmac('sha256', str_random(40), $key);
        $originalToken 		  = $key."_".$token;

        $dataWithEmail->activation_token = $originalToken;
		$dataWithEmail->save();
		Mail::send('emails.activate', ['token' => $originalToken,'userType'=>$typeValue,'name'=>$name], function ($message) use ($name,$email) {
                    $message->to($email, $name)
							->subject('TenantU - Please confirm your email address.');
        });

	}


	//this function is used for activate the account

	public function accountIsActive($token,$userType)
    {
		if($userType=='tenant')
		{
			$tenant     		 	= Tenant::where('activation_token', '=', $token)->first();
			$userName             	= $tenant->firstname.' '.$tenant->lastname;
			$email               	= $tenant->email;
			$tenant->is_active   	= 1;
			$tenant->activation_token = '';
			if ($tenant->save())
			{
				Mail::send('emails.activationconfirm', ['name'=>$userName], function ($message) use ($userName,$email) {
                    $message->to($email, $userName)
							->subject('TenantU - Activatated successfully.');
        		});
				return redirect()->route('index.login')->with('message', 'Your account is activated you can login now!');
			}

		}
		else
		{
			$landlord       			= Landlord::where('activation_token', '=', $token)->first();
			$userName               	= $landlord->firstname.' '.$landlord->lastname;
			$email               		= $landlord->email;
			$landlord->is_active   		= 1;
			$landlord->activation_token 	= '';
			if ($landlord->save())
			{
        $LandlordSubscriptionController = new LandlordSubscriptionController;
        $LandlordSubscriptionController->setLandlordSubscriptionPlanOnCreate($landlord->id);

				Mail::send('emails.activationconfirm', ['name'=>$userName], function ($message) use ($userName,$email) {
                    $message->to($email, $userName)
							->subject('TenantU - Account activatated successfully.');
        		});
				return redirect()->route('index.login')->with('message', 'Your account is activated you can login now!');
			}

		}


    }


	public function postSearch(Request $request)
	{
		$selectedLandlordDropDown=array();
		$selectedSchool    = $this->getSchoolDetails();

		if(!empty(Auth::user('tenant'))){
		    $profile = $this->getTenantProfile(Auth::user('tenant')->id);
		      $selectedSchoolId = $profile['school_id'];
		}
		else if(!empty(Auth::user('landlord'))){
  		$profile = $this->getLandlordProfile(Auth::user('landlord')->id);
		    $selectedSchoolId = $profile['school_id'];
		}
		else{
		    $selectedSchoolId = "";
		}
		$landlords = $this->getLandlords();

		foreach ($landlords as $landlord)
        {
            $selectedLandlordDropDown[$landlord->id] = $landlord->firstname.' '.$landlord->lastname;
        }
      $aminity  			= $this->getAminityDetails();

			$searchKeyWord           = ($request->search!="")      ? $request->search      : "";

      $selectedAminity      	 = ($request->aminity!="")   ? implode(',',$request->aminity) : "";
			$selectedLandlordId      = ($request->landlord_id!="") ? $request->landlord_id : "";
			$selectedBedroomNo       = ($request->bedroomId!="")   ? $request->bedroomId   : "";
			$selectedBathroomNo	 = ($request->bathroomId!="") ? $request->bathroomId   : "";
			$selectedDistance        = ($request->distance!="")    ? $request->distance    : "";
			$selectedPriceFrom       = ($request->priceFrom!="")   ? $request->priceFrom   : "";
			$selectedPriceTo         = ($request->priceTo!="")     ? $request->priceTo     : "";
			$selectedSortKey         = ($request->sortKey!="")     ? $request->sortKey     : "";
			$selectedAvailabilityKey = ($request->availabilityKey!="")     ? $request->availabilityKey     : "";
			$selectedPropertyType    = ($request->propertyType!="") ? $request->propertyType : "";
			$page                    = ($request->page!="")        ? $request->page        : "1";

		//Gets all properties which are not belonging to an apartment
        	$propertyQuery = DB::table('property_physical as p')
                //->leftJoin('property_meta as m', 'p.id', '=', 'm.property_physical_id')
                //->leftJoin('property_meta as m', DB::raw('m.property_physical_id'), '=', DB::raw('p.id'))
                ->leftJoin('property_meta as m', 'm.property_physical_id', '=', 'p.id')
		            ->leftJoin('apartment_building as a', 'p.apartment_building_id', '=', 'a.id')
                //->leftJoin('property_amenities as prop_amen', DB::raw('p.id'), '=', DB::raw('prop_amen.property_id'))
                ->leftJoin('property_amenities as prop_amen', 'p.id', '=', 'prop_amen.property_id')
                ->join('landlords', 'p.landlord_id', '=', 'landlords.id')
                ->where('m.status', '=', 1)
		            ->whereNull('p.apartment_building_id')
                ->select('p.*')
                ->addSelect('m.*')
		            ->addSelect('a.building_name as apartmentName')
	 	            ->addSelect('a.id as apartmentId')
                ->addSelect('prop_amen.amenity as prop_amenities');
		//Gets all properties which belong to an apartment complex and limits it to one from each then joins that with the corresponding apartment table to get just one from each apartment complex for address, etc, and the bulding name

		 $apartmentQuery = DB::table('property_physical as p2')
                ->leftJoin('property_meta as m2', 'p2.id', '=', 'm2.property_physical_id')
		             ->leftJoin('apartment_building as a2', 'a2.id', '=', 'p2.apartment_building_id')
                 //->leftJoin('property_amenities as prop_amen2', DB::raw('p2.id'), '=', DB::raw('prop_amen2.property_id'))
                 ->leftJoin('property_amenities as prop_amen2', 'p2.id', '=', 'prop_amen2.property_id')
                ->join('landlords', 'p2.landlord_id', '=', 'landlords.id')
                ->where('m2.status', '=', 1)
                ->whereNotNull('p2.apartment_building_id')
                ->select('p2.*')
                ->addSelect('m2.*')
		            ->addSelect('a2.building_name as apartmentName')
		            ->addSelect('a2.id as apartmentId')
                ->addSelect('prop_amen2.amenity as prop_amenities2');
		            //->groupBy('p2.apartment_building_id');
    $selectedAmenityArray = $request->aminity;
    $searchKeyWordArray = "";
    if (!empty($request->search)) {
      $searchKeyWordArray = explode(" ", $request->search);
    }

		//The more filter type functions which for sake of time I felt were better to just keep as query form, could be improved

        	if($selectedPriceFrom!="" && $selectedPriceTo!=""){
        		$propertyQuery->whereBetween('rent_price',array( $selectedPriceFrom, $selectedPriceTo ));
        	}

        	if($selectedSchoolId!=""){
        		$propertyQuery->where('p.school_id',$selectedSchoolId);
			$apartmentQuery->where('p2.school_id',$selectedSchoolId);
        	}

        	if( $selectedLandlordId!="" ){
        		$propertyQuery->where('p.landlord_id',$selectedLandlordId);
			$apartmentQuery->where('p2.landlord_id',$selectedLandlordId);
        	}
        	if( $selectedBedroomNo!="" && $selectedBedroomNo!="any" ){
        		$propertyQuery->where('p.bedroom_no','=',$selectedBedroomNo);
        	}
        	if( $selectedDistance!="" && $selectedDistance!=100 ){
        		$propertyQuery->where('p.distance_from_school','<=',$selectedDistance);
        		$apartmentQuery->where('p2.distance_from_school','<=',$selectedDistance);
		}
        	if( $selectedAvailabilityKey!="" && $selectedAvailabilityKey!="all"){
        		$propertyQuery->where('m.availability_status',$selectedAvailabilityKey);
		}
        	if ($selectedPropertyType == 'house')
        	{
        		$propertyQuery->where('p.property_type','1');
			$apartmentQuery->where('p2.property_type', '1');
        	}
        	if ($selectedPropertyType == 'apartment')
        	{
        		$propertyQuery->where('p.property_type','0');
			$apartmentQuery->where('p2.property_type','0');
        	}

    if(!empty($selectedAmenityArray)){

      $propIdsWithMatchingAmenities = array();
      $propertyQueryCloneAmenities = clone $propertyQuery;
      $propertiesAndAmenities = $propertyQueryCloneAmenities->select('p.id')->addSelect('prop_amen.amenity')->get();

      foreach($propertiesAndAmenities as $property) {
        if (in_array($property->amenity, $selectedAmenityArray)) {
          array_push($propIdsWithMatchingAmenities, $property->id);
        }
      }
      $propIdsWithMatchingAmenities = array_unique($propIdsWithMatchingAmenities);
      $propertyQuery->whereIn('p.id', $propIdsWithMatchingAmenities);

      $apartmentIdsWithMatchingAmenities = array();
      $apartmentQueryCloneAmenities = clone $apartmentQuery;
      $apartmentsAndAmenities = $apartmentQueryCloneAmenities->select('p2.id')->addSelect('prop_amen2.amenity')->get();

      foreach($apartmentsAndAmenities as $apartment) {
        if (in_array($apartment->amenity, $selectedAmenityArray)) {
          array_push($apartmentIdsWithMatchingAmenities, $apartment->id);
        }
      }
      $apartmentQuery->whereIn('p2.id', $apartmentIdsWithMatchingAmenities);
  }
      $apartmentQuery->groupBy('p2.apartment_building_id');

        $foundProperties = collect();
        $matchedPropertyIds = array();
        $matchedApartmentUnitIds = array();


        if(!empty($searchKeyWordArray)) {
            $propertyQueryClone = clone $propertyQuery;
            $searchFields = $propertyQueryClone->select('m.property_physical_id')->addSelect('m.search_field')->get();
            $apartmentQueryClone = clone $apartmentQuery;
            $apartmentSearchFields = $apartmentQueryClone->select('p2.id')->addSelect('m2.search_field')->get();

            foreach($searchKeyWordArray as $userSearchWord) {
              foreach($searchFields as $propKey => $propertySearchField) {
                if(similar_text(strtolower($propertySearchField->search_field), strtolower($userSearchWord)) >= 3) {
                    array_push($matchedPropertyIds, $propertySearchField->property_physical_id);
                    unset($searchFields[$propKey]);
                }

              }

              foreach($apartmentSearchFields as $apartmentSearchField) {
                if(similar_text(strtolower($apartmentSearchField->search_field), strtolower($userSearchWord)) >= 3){
                  array_push($matchedApartmentUnitIds, $apartmentSearchField->id);
                }
              }
            }

            //dd($matchedApartmentUnitIds);
            $matchedPropertyIds = array_unique($matchedPropertyIds);

            $prop = $propertyQuery->whereIn('p.id', $matchedPropertyIds)->get();
            $apartments = $apartmentQuery->whereIn('p2.id', $matchedApartmentUnitIds)->get();
            $foundProperties->push($prop);
            $foundProperties->push($apartments);
            $foundProperties = $foundProperties->values()->all();
            $propertiesAll = array_flatten($foundProperties);
            $foundProperties = collect($propertiesAll);
            $propertiesAllCollection = $foundProperties;
            $propertiesAllCollection = $propertiesAllCollection->unique('id');
        }





        else{
          $propertyQuery = $propertyQuery->unionAll($apartmentQuery);
          $propertiesAll = $propertyQuery->get();
          $propertiesAllCollection = collect($propertiesAll);
          //dd($propertiesAllCollection);
          $propertiesAllCollection = $propertiesAllCollection->unique('id');
        }

                $paginate = 6;
        	if( $selectedSortKey!="" && $selectedSortKey=="MV"){
			         $propertiesAllCollection = $propertiesAllCollection->sortByDesc('most_viewed');
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="MR"){
			//unused most reviewed sort
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="LtoS"){
			$propertiesAllCollection = $propertiesAllCollection->sortByDesc('bedroom_no');
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="StoL"){
			$propertiesAllCollection = $propertiesAllCollection->sortBy('bedroom_no');
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="HRL"){
			//unused highest rated blah filter
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="HRLoc"){
			//unused highest rated blah filter
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="HRCl"){
			//unused highest rated blah filter
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="HRA"){
			//unused highest rated blah filter
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="AvgRat"){
			//unused averge rating filter
        	}
        	else if( $selectedSortKey!="" && $selectedSortKey=="WR"){
        		//dunno what they meant by this one, thinking emoji

			//$propertyQuery -> orderBy('p.recommented_flag','DESC')
			//					->orderBy('updated_at','DESC');
			//$apartmentQuery -> orderBy('p2.recommented_flag','DESC')
                        //                                        ->orderBy('updated_at','DESC');
        	}
        	else{
			         $propertiesAllCollection = $propertiesAllCollection->sortByDesc('created_at');
        	}

	             $propertyIds = array();
               $propertyImages = array();

            	foreach($propertiesAll as $property) {
               	array_push($propertyIds, $property->id);
               	$propertyImages[$property->id] = array();
               	$propertyImageDBResults = DB::table('property_images')->where('property_id', '=', $property->id)->select('*')->get();

                foreach ($propertyImageDBResults as $propertyImageEntry) {
                 	array_push($propertyImages[$property->id], $propertyImageEntry);
  		  		    }

            	}

			//where the paginator is created
			$properties = new \Illuminate\Pagination\LengthAwarePaginator($propertiesAllCollection->forPage($page,$paginate),$propertiesAllCollection->count(),$paginate,$page);

			foreach($properties as $property) {
				$propertyAmenities = PropertyAmenity::where('property_id',$property->id)
                                -> select('amenity')
                                -> get();
                                if(count($propertyAmenities) > 0){
                                $propertyAmenitiesArray[$property->id] = $propertyAmenities;
                                }
				if(count($propertyAmenities) == 0){
				$propertyAmenitiesArray[$property->id] = "";
				}

				if (!empty($propertyImages[$property->id])) {
					$property->images = $propertyImages[$property->id];
				}
				else {
					$property->images = array();
				}


			}
        	foreach($properties as $property)
        	{
				$schoolId = $property->school_id;
				$property->schoolName = $this->getSchoolName($schoolId);
			}

            if(!empty($propertiesAll)){
        		foreach( $propertiesAllCollection as $key=>$val ){

        			$googleMapArray[$key]['lat'] = $val->latitude;
        			$googleMapArray[$key]['lng'] = $val->longitude;
        			$googleMapArray[$key]['address'] = $val->thoroughfare.' '.$val->locality.' '.$val->administrative_area;
        			$googleMapArray[$key]['slug'] = $val->slug;
				      $googleMapArray[$key]['id'] = $val->id;

        		}
        	}
        	else{
        		$googleMapArray =array();
        	}

			$savedSearchData = $this->saveMySearchDetails($selectedSchoolId,$selectedLandlordId,$selectedBedroomNo,$selectedDistance,$selectedPriceFrom,$selectedPriceTo,$searchKeyWord,$selectedAminity,$selectedSortKey);
			$savedPropertySearchData = $this->savePropertySearch($selectedSchoolId,$selectedLandlordId,$selectedBedroomNo,$selectedDistance,$selectedPriceFrom,$selectedPriceTo,$searchKeyWord,$selectedAminity,$selectedSortKey);
			$pageTitle = 'SearchResult';
		if($page == 1){
	        return view('index.searchresults')->with(compact('pageTitle',
	        												 'properties',
	        												 'searchKeyWord',
	        												 'selectedSchool',
	        												 'selectedLandlordDropDown',
	        												 'selectedSchoolId',
	        												 'selectedLandlordId',
	        												 'selectedBedroomNo',
		    									 			   'selectedBathroomNo',
	        												 'selectedDistance',
	        												 'selectedPriceFrom',
	        												 'selectedPriceTo',
	        												 'selectedSortKey',
	        												 'selectedAvailabilityKey',
	        												 'selectedPropertyType',
	        												 'googleMapArray',
			                             'propertyAmenitiesArray',
	        												 'page','selectedAminity','aminity'));}
	         else{
		 return view('index._searchresults')->with(compact('pageTitle',
                                                                                                                 'properties',
                                                                                                                 'searchKeyWord',
                                                                                                                 'selectedSchool',
                                                                                                                 'selectedLandlordDropDown',
                                                                                                                 'selectedSchoolId',
                                                                                                                 'selectedLandlordId',
                                                                                                                 'selectedBedroomNo',
                                                                                                                   'selectedBathroomNo',
                                                                                                                 'selectedDistance',
                                                                                                                 'selectedPriceFrom',
                                                                                                                 'selectedPriceTo',
                                                                                                                 'selectedSortKey',
                                                                                                                 'selectedAvailabilityKey',
                                                                                                                 'selectedPropertyType',
                                                                                                                 'googleMapArray',
                                                     'propertyAmenitiesArray',
                                                                                                                 'page','selectedAminity','aminity'));}
	    //}
	}

	private function getAminityDetails()
	{
		 $aminity = array();
		 $aminity  = DB::table('amenities')->where('status',1)->orderBy('amenity_name', 'asc')->lists('amenity_name', 'amenity_name');
		 return $aminity;
	}

	 private function getSchoolName($schoolId)
	 {
		 $schoolName = School::where('id',$schoolId)
								->select('schoolname')
								->first();
		//print_r($schoolName);exit;
		return $schoolName['schoolname'];
	 }
	//this func is for saving the search keywords of logged tenant

	public function saveMySearchDetails($schoolId,$landlordId,$bedroomNo,$distance,$priceFrom,$priceTo,$search,$selectedAminity,$sortKey)
	{
		//echo $sortKey;exit;
		if(Auth::user('tenant'))
		{
			$loggedTenantId 		= Auth::user('tenant')->id;
			$mysearchDetails 		= $this->getMySavedSearchData($loggedTenantId);
			if(empty($mysearchDetails))
			{

				$searchDetails =  Mysearch::create(['tenant_id'=>$loggedTenantId,'landlord_id'=>$landlordId,'school_id'=>$schoolId,
							'bedrooms'=>$bedroomNo,'distance'=>$distance,'price_form'=>$priceFrom,'price_to'=>$priceTo,
							'search'=>$search,'aminities'=>$selectedAminity,'sortkey'=>$sortKey]);

			}
			else
			{
				//DB::connection()->enableQueryLog();

				$searchDetails =  Mysearch::where('tenant_id', $loggedTenantId)
						  ->update(['landlord_id' => $landlordId,'school_id'=>$schoolId,
						  'bedrooms'=>$bedroomNo,'distance'=>$distance,'price_form'=>$priceFrom,
						  'price_to'=>$priceTo,'search'=>$search,'aminities'=>$selectedAminity,'sortkey'=>$sortKey]);
				//$queries = DB::getQueryLog();
				//dd($queries);

			}
	    }
	}

	//this method is for saving the most ssearched data

	public function savePropertySearch($schoolId,$landlordId,$bedroomNo,$distance,$priceFrom,$priceTo,$search,$selectedAminity,$sortKey)
	{
		if(Auth::user('tenant'))
		{
			$loggedTenantId = Auth::user('tenant')->id;
		}
		else
		{
			$loggedTenantId= '';
		}
		$sessionId = Session::getId();

		$myPropertySearchDetails 		= $this->getMyPropertySearchData($loggedTenantId,$schoolId,$landlordId,$bedroomNo,$distance,$priceFrom,$priceTo,$search,$selectedAminity,$sortKey,$sessionId);
		//print_r($myPropertySearchDetails);exit;
			if(count($myPropertySearchDetails)< 1)
			{
				if($distance=='')
				{
					$distance='10';
				}
				if($priceFrom=='')
				{
					$priceFrom='0';
				}
				if($priceTo=='')
				{
					$priceTo='10000';
				}

				$searchDetails =  PropertySearch::create(['tenant_id'=>$loggedTenantId,'landlord_id'=>$landlordId,'school_id'=>$schoolId,
							'bedrooms'=>$bedroomNo,'distance'=>$distance,'price_form'=>$priceFrom,'price_to'=>$priceTo,
							'search'=>$search,'aminities'=>$selectedAminity,'sortkey'=>$sortKey,'session_id'=>$sessionId]);

			}

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function test()
    {
		$x = $this->isRegisteredFacebook('soorajfouryou@gmail.com');
		print_r($x);
		exit;
		$input					= array();
        $input['firstname'] 	= 'sooraj';
        $input['lastname'] 		= 'r';
        $input['email'] 		= 'soorajfouryou@gmail.com';
        $input['fb_id']    		= '234234';
        $input['is_active']		= 1;
        //print $input['firstname'];
        $input['is_fb_user']	= 1;
		return Redirect::route('facebook.auth',['id'=>stringy]);
		//return Redirect::to_action('index@facebookAuth', $input);

		/*$registrationStatus=array('status'=>'0');
		$input					= array();
        $input['firstname'] 	= 'sooraj';
        $input['lastname'] 		= 'r';
        $input['email'] 		= 'soorajfouryou@gmail.com';
        $input['fb_id']    		= '234234';
        $input['is_active']		= 1;
        //print $input['firstname'];
        $input['is_fb_user']	= 1;
		$x = $this->processFbUser($registrationStatus,'student',$input);
		//print $x['url'];
		return Redirect::route($x['url']);
		//exit;*/
	}

	public function getFormatDate($dt)
	{
		$actualTime				= '';
		$currentDateTime 		= Carbon::now();
		//echo $currentDateTime;exit;
		$currentDateTimeStamp 	= strtotime($currentDateTime);
		$messageDateTimeStamp 	= strtotime($dt);
		$timeDifference 		= $currentDateTimeStamp - $messageDateTimeStamp;

		if($timeDifference < 60)
		{
			 $actualTime = $timeDifference;
			 return $timeDifference." => ".$actualTime."sec";
		}
		elseif($timeDifference > 60 && $timeDifference < 3600)
		{
			$actualTime = round($timeDifference/60);
			return $timeDifference." => ".$actualTime."minute";
		}
		elseif($timeDifference >3600 && $timeDifference < 43200)
		{
			$actualTime = round($timeDifference/3600);
			return $timeDifference." => ".$actualTime."hour";
		}
		elseif($timeDifference > 43200 && $timeDifference < 86400)
		{
			$actualTime = round($timeDifference/3600);
			return $timeDifference." => "."Today";
		}
		elseif($timeDifference > 86400)
		{
			$dt = date('d M Y h:i:s a',strtotime($dt));
			return $timeDifference." => ".$dt;
		}


	}

	public function getPageNotFound()
	{

		return view('index.pageNotFound',[
			 'pageTitle'   => 'PageNotFound',
			 ]);
	}
	public function getSchool($schoolId)
	{
		$schoolName 	= School::where('id',$schoolId)->select('schoolname')->first();
		return $schoolName->schoolname;
	}
	public function getLandlords(){
		//print(Session::get('schoolName'));
		if(Session::has('schoolName'))
		{
			$sessionSchoolId = Session::get('schoolName');
			$landlords 	= Landlord::select('firstname')
								   ->addSelect('lastname')
								   ->addSelect('id')
								   ->where('school_id',$sessionSchoolId)
								   ->where('is_active',1)
								   ->get();

		}else{
		$landlords 	= Landlord::select('firstname')
							   ->addSelect('lastname')
							   ->addSelect('id')
							   ->where('is_active',1)
							   ->get();
		}
		return $landlords;
	}

	//this method is for clear the table
	public function clearTable($tablename)
	{
		$truncate = DB::table($tablename)->truncate();

	}

	//this method is for save contact us details in to contact table

	public function postContactUs(Request $request)
	{
		 $rules = [
            'name'             => 'required',
            'email'            => 'required|email',
            'message'          => 'required'
        ];
         $validator = Validator::make($request->all(), $rules);
         if ($validator->fails())
		 {
			 return redirect()->back()->with('message-error','Please fill all the fields');
		 }

		 $schoolName = $this->getSpecificSchool($request->school);
		 $contactusUser = ContactUser::create(['name'=> $request->name ,'email'=>$request->email ,'school_id'=>$request->school ,'reason'=>$request->reason,'message'=>$request->message]);

		 $contactAdminEmail = Config::get('constants.CONTACT_EMAIL');

		 Mail::send('emails.contact-users',['name'=>$request->name,'email'=>$request->email,'school'=>$schoolName,'reason'=>$request->reason,'messageContent'=>$request->message],
										function($message) use ($contactAdminEmail) {

					$message->to($contactAdminEmail)
							->subject("Contact User");
		 });
		 return redirect()->back()->with('message-success','Your message has been successfully sent. We will contact you very soon!');
	}

	private function getSpecificSchool($school_id)
	{
		$school = School::where('id',$school_id)
						->select('schoolname')
						->get()->first();
		return $school->schoolname;
	}

	//this method is for showing cms pages asper the slug

	public function showCmsPages($slug)
	{

		$pageDetail = Cmspage::where('pageslug',$slug)
					->select('title')
					->addSelect('content')
					->addSelect('pageslug')
					->get()->first();
		if(empty($pageDetail))
		{
			return redirect('/404');
		}

		return view('index.pages',[
			 'pageTitle'   => $pageDetail->title,
			 'pageContent' => $pageDetail->content,
			 'pageslug'	   => $pageDetail->slug,
			 ]);

	}

	public function showFaq()
	{
		$faqDetail = Faq::select('id')
					->addSelect('question')
					->addSelect('answer')
					->where('status',1)
					->get();
		//print_r($faqDetail);exit;
		if(empty($faqDetail))
		{
			return redirect('/404');
		}

		return view('index.faq',[
			 'pageTitle'   => 'Faq',
			 'faqDetail' => $faqDetail,

			 ]);
	}

	public function showBanners(Request $request)
	{
		$statusMessage = '';
		if(isset($request->paid))
		{
			$paidStatus = $request->paid;
			if($paidStatus== '1')
			{
				$statusMessage = 'Your transaction is completed';
			}
			else
			{
				$statusMessage = 'Your transaction is not completed';
			}
		}
		$bannerDetails = BannerSetting::where('status',1)
										->select('banner_name')
										->addSelect('banner_size')
										->addSelect('banner_price')
										->addSelect('sample_image')
										->addSelect('id')
										->get();

		foreach($bannerDetails as $bannerDetail)
		{
			$activeBannerPurchases = $this->getActiveBannerPurchases($bannerDetail->id);
			if(!empty($activeBannerPurchases))
			{
				$bannerDetail->availability='unavailable';
			}
		}
		//dd($bannerDetails);exit;
		if(empty($bannerDetails))
		{
			return redirect('/404');
		}

		return view('index.banners',[
			 'pageTitle'   => 'Advertisement page',
			 'bannerDetails' => $bannerDetails,
			 'statusMessage'  => $statusMessage
			 ]);
	}

	private function getActiveBannerPurchases($bannerSettingId)
	{

		$activeBannerPurchase = DB::select('SELECT * , DATE_ADD( activated_date, INTERVAL 30
					DAY )
					FROM banner_purchases
					WHERE NOW()
					BETWEEN activated_date
					AND DATE_ADD( activated_date, INTERVAL 30
					DAY ) AND bannerId='.$bannerSettingId.' AND status=1');
		return $activeBannerPurchase;
	}

	public function getMySavedSearchData($tenantId)
	 {
		 //echo $tenantId;exit;
		 $mysearch = Mysearch::where('tenant_id',$tenantId)
								->get()->first();
		 return $mysearch;
	 }

	 private function getMyPropertySearchData($loggedTenantId,$schoolId,$landlordId,$bedroomNo,$distance,$priceFrom,$priceTo,$search,$selectedAminity,$sortKey,$sessionId)
	 {

		 $propertySearch = PropertySearch::where('session_id',$sessionId)
											->where('tenant_id',$loggedTenantId)
											->where('landlord_id',$landlordId)
											->where('school_id',$schoolId)
											->where('bedrooms',$bedroomNo)
											->where('distance',$distance)
											->where('price_form',$priceFrom)
											->where('price_to',$priceTo)
											->where('search',$search)
											->where('aminities',$selectedAminity)
											->where('sortkey',$sortKey)
											->get();
		//print_r($propertySearch);exit;
		 return $propertySearch;
	 }

	 private function getClientTestimonials()
	 {
		 $clientFeedbacks = Feedback::where('status',1)
							->select('client_name')
							->addSelect('company_name')
							->addSelect('description')
							->orderBy('created_at','DESC')
							->get();
		return $clientFeedbacks;
	 }

	 private function getFeaturedProperties($schoolId=null)
	 {
		 if($schoolId!='')
		 {
			$featuredProperty = Property::where('featured',1)
							->where('school_id',$schoolId)
							->select('property_name')
							->addSelect('id')
							->addSelect('slug')
							->addSelect('landlord_id')
							->addSelect('school_id')
							->addSelect('rent_price')
							->orderBy('created_at','DESC')
							->get();
		 }
		else
		{
		$featuredProperty = Property::where('featured',1)
							->select('property_name')
							->addSelect('id')
							->addSelect('slug')
							->addSelect('landlord_id')
							->addSelect('school_id')
							->addSelect('rent_price')
							->orderBy('created_at','DESC')
							->get();
		}

		return $featuredProperty;
	}

	private function getSchoolImage($schoolId)
	{
		$schoolImageName = School::where('id',$schoolId)
							->select('image')
							->addSelect('schoolname')
							->first();
		return $schoolImageName;
	}

	private function getPropertyReviewCount($propertyId)
	{
		$reviewCount = DB::select('SELECT count(id) as reviewCount FROM property_review WHERE property_id='.$propertyId);
		return $reviewCount;
	}

	public function getContactUs()
	{
		$school 				= $this->getSchoolDetails();
		if(Auth::user('tenant')){
                $loggedName = Auth::user('tenant')->firstname ." ". Auth::user('tenant')->lastname;
                $loggedEmail = Auth::user('tenant')->email;
		$loggedSchoolId = Auth::user('tenant')->school_id;
                }
                else if(Auth::user('landlord')){
		$loggedName = Auth::user('landlord')->firstname ." ". Auth::user('landlord')->lastname;
		$loggedEmail = Auth::user('landlord')->email;
		$loggedSchoolId = Auth::user('landlord')->school_id;
		}
		else{
		$loggedName = "";
		$loggedEmail = "";
		$loggedSchoolId = "";
		}
		return view('index.contactus',[
			 'pageTitle'   => 'ContactUs',
			 'loggedName'  => $loggedName,
			 'loggedEmail' => $loggedEmail,
			 'loggedSchoolId' => $loggedSchoolId,
			 'school'	   => $school
			 ]);
	}

	public function postEnquiry(Request $request)
	{
		$rules = [
            'email'            => 'required|email',
        ];
         $validator = Validator::make($request->all(), $rules);
         if ($validator->fails())
		 {
			 return redirect()->back()->with('message-error','Please put your email');
		 }

		 $enquiryUser = Enquiry::create(['email'=> $request->email]);

		 $contactAdminEmail = Config::get('constants.CONTACT_EMAIL');

		 Mail::send('emails.enquiry-users',['email'=>$request->email],
										function($message) use ($contactAdminEmail) {

					$message->to($contactAdminEmail)
							->subject("Enquiry");
		 });
		 return redirect()->intended(route('index.index','#contact'))->with('message-success','Your enquiry is sent We will contact you very soon! ');

	}

	public function getAllActiveLandlords()
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$allLandlords = $this->getAllLandlords();
		//dd($allLandlords);
		return view('index.selectLandlords',[
			 'pageTitle'   => 'Landlords',
			 'landlords'   => $allLandlords
			 ]);
	}

	public function getAllLandlords()
	{

		$landlords  = array();
		$landlords = DB::table('landlords')->select(DB::raw("CONCAT(firstname,' ',lastname) as fullname,id"))->where('is_active',1)->lists('fullname', 'id');
		//$landlords = Landlord::select(DB::raw("CONCAT(firstname,' ', lastname) AS fullname,id"))->where('is_active',1)->lists('fullname', 'id');;
		return $landlords;
	}

	public function landlordLoginWithId(Request $request)
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$rules = [
            'landlord'            => 'required',
        ];
         $validator = Validator::make($request->all(), $rules);
         if ($validator->fails())
		 {
			 return redirect()->back()->with('message-error','Please select a landlord');
		 }

		$landlordId = $request->landlord;
		if(Auth::loginUsingId("landlord", $landlordId))
		{
			return Redirect::intended(route('landlords.dashboard'));
		}
	}

	public function getSocialMediaLinks()
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$socialLinks = SocialMedia::select('twitter')
					->addSelect('facebook')
					->addSelect('instagram')
					->get();
		//print_r($socialLinks);exit;
		return view('index.socialLinks',[
			 'pageTitle'   => 'Social media links',
			 'socialLinks'   => $socialLinks
			 ]);
	}

	public function postSocialMediaLinks(Request $request)
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		//dd($request->twitter);
		$updateSocialLink   	= DB::update('UPDATE social_links SET twitter="'.$request->twitter.'",facebook="'.$request->facebook.'",instagram="'.$request->instagram.'"');
		return redirect()->back()->with('message','Social media links are updated');
	}

	public function getEnquiries(Request $request)
	{

		$search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $enquiriesQuery = DB::table('enquiries');

        if ($request->has('search')) {
            $search = $request->input('search');
            $enquiriesQuery->where('email', 'like', '%' . $request->input('search') . '%');
        }
        $enquiriesQuery->orderBy($sortby, $order);
        $enquiries = $enquiriesQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';

		 return view('index.enquiries', ['pageTitle' => 'Enquiry emails',
            'pageHeading'                                => 'Be the first to find out..area',
            'enquiries'                                  => $enquiries,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);


	}

	private function checkEmailRegistered($email)
	{
		/*if($userType=='student')
		{
			$userTypeVal = 'tenant';
			$dataWithEmail = Tenant::where('email', '=', $email)->first();

		}
		else
		{
			$userTypeVal = 'landlord';
			$dataWithEmail = Landlord::where('email', '=', $email)->first();
		}*/


		$tenantData = DB::select('SELECT *
							FROM tenants
							WHERE email ="'.$email.'"');

		$landlordData = DB::select('SELECT *
							FROM landlords
							WHERE email ="'.$email.'"');

		if(empty($tenantData)) {
		    return $landlordData;
        }
        else {
		    return $tenantData;
        }

//		$dataWithEmail = DB::select('SELECT *
//							FROM tenants
//							WHERE email ="'.$email.'"
//							UNION
//							SELECT *
//							FROM landlords
//							WHERE email = "'.$email.'" LIMIT 1');
//
//		return $dataWithEmail;
	}

	private function isRegisteredFacebook($email)
	{
		$facebookUsers = DB::select('select email,is_fb_user,is_normal_user, "tenant" as usertype from tenants where email  = "'.$email.'"
							union
							select email,is_fb_user,is_normal_user, "landlord" as usertype from landlords where email ="'.$email.'"');
		//print_r($facebookUsers);
		if (empty($facebookUsers))
		{
			return array('status'=>0);
		}
		$userType = $facebookUsers[0]->usertype;

		if($userType=='tenant')
		{
			$typeVal='tenant';
		}
		else
		{
			$typeVal='landlords';
		}



		 if($facebookUsers[0]->is_fb_user==1 && $facebookUsers[0]->is_normal_user==1)
		 {
			return array('status'=>1,'userType'=>$typeVal);//Regstrerd as both type

		 }
		 elseif($facebookUsers[0]->is_fb_user==1 && $facebookUsers[0]->is_normal_user==0)
		 {
			 return array('status'=>'fb_user','userType'=>$typeVal);//registered as fb only
		 }

		 else
		 {
			 return array('status'=>'normal_user','userType'=>$typeVal);//registered as normal only
		 }
	}

	public function ajaxPostSchool(Request $request)
	{
		$page 					 = $request->page;

		if($page == 'featured'){
			$durationId = $request->duration;
			//$propertyId = $request->property;

			$featuredPrice = FeaturedSetting::where('id',$durationId)
											->select('price')
											->addSelect('id')
											->first();
			/*$property = Property::where('id',$propertyId)
								->select('property_name')
								->addSelect('id')
								->first();
			$featuredPrice->propertyId = $property->id;
			$featuredPrice->propertyName = $property->property_name;*/
			return $featuredPrice;
		}

		$schoolId 				 = $request->school_id;
		Session::put('schoolName', $schoolId);
		if($page == 'search-result'){
			$landlords = $this->getLandlordsWithSessionSchool($schoolId);
			return array('landlords'=>$landlords);
		}



		$featuredProperties 	 = $this->getFeaturedProperties(Session::get('schoolName'));

		$SchoolImage		 	 = $this->getSchoolImage(Session::get('schoolName'));
		$schoolName 			 = $SchoolImage->schoolname;

		$schoolImageUrl = asset('public/uploads/schools/');
		$schoolImage = $schoolImageUrl.'/'.$SchoolImage->image;
		Session::put('schoolImage', $schoolImage);


		$featuredPropertiesCount = count($featuredProperties);
		$reviewCount			 = array();
		foreach($featuredProperties as $featuredProperty)
		{
			$reviewCount 					= $this->getPropertyReviewCount($featuredProperty->id);
			$featuredProperty->reviewCount	= $reviewCount[0]->reviewCount;
		}


		  $view = View::make('index.featuredProperty',['featuredProperties'=>$featuredProperties]);
		  $contents = $view->render();
		  return array('featuredProperties'=>$contents,'featuredPropertiesCount'=>$featuredPropertiesCount,'schoolImage'=>$schoolImage);

      /*return view('index.featuredProperty', [
            'featuredProperties'=>$featuredProperties,
        ]); */



	}

	private function getLandlordsWithSessionSchool($schoolId)
	{
		$landlords = array();
		$landlords = DB::table('landlords')->select(DB::raw("CONCAT(firstname,' ',lastname) as fullname,id"))->where('is_active',1)->where('school_id',$schoolId)->lists('fullname', 'id');
		return $landlords;
	}

	public function bannerPurchase($bannerId)
	{
		$banner = BannerSetting::where('id',$bannerId)
									->select('id')
									->addSelect('banner_name')
									->addSelect('banner_size')
									->addSelect('banner_price')
									->addSelect('description')
									->first();
		if(empty($banner))
		{
			return redirect('/404');
		}

		return view('index.bannerPurchase', ['pageTitle' => 'Purchase ad banner slots',
            'pageHeading'                                => 'Purchase Advertisement',
            'banner'                                      => $banner,
        ]);
	}

	public function postBannerPurchase(Request $request)
	{
		//dd($request->all());
		$bannerId = $request->bannerId;
		$rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'phone'					=> 'required',
            'url'              		=> 'required',
            'image' 				=> 'required',

        ];

        $validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}
		else
		{
			$input					=  array();
			$input['name'] 			=  $request->firstname." ".$request->lastname;
			$input['phone'] 		=  $request->phone;
			$input['address'] 		=  $request->address;
			$input['banner_url'] 	=  $request->url;
			$input['bannerId'] 		=  $request->bannerId;
			$input['banner_name'] 	=  $request->bannerName;
			$input['banner_price'] 	=  $request->bannerPrice;

			$bannerSize = BannerSetting::where('id',$input['bannerId'])
									->select('width')
									->addSelect('height')
									->first();

			if($request->file('image'))
            {

				$imagePath = Config::get('constants.BANNER_IMAGE_PATH');
				$image     = $request->file('image');
				$extension = $image->getClientOriginalExtension();
				$filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
				$filename  = str_slug($filename, "-") . '.' . $extension;
				$image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
				Image::make($imagePath . '/' . $filename)->resize($bannerSize->width, $bannerSize->height)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
			}

			$bannerPuchase = BannerPurchase::create($input);
			//echo $bannerPuchase->id;exit;
			return view('index.bannerPurchasePaypal', ['pageTitle' => 'Purchase Banner ',
            'pageHeading'                 => 'Purchase Banner',
            'input'                       => $input,
            'bannerPurchaseId'			  => $bannerPuchase->id,
            'firstName'				  	  => $request->firstname,
            'lastName'				  	  => $request->lastname,
			]);

		}

	}

	public function logMsg($msg)
	{
		$x='';
		$x.= "" . $msg . "\n \n";
		$myFile = file_put_contents('/home/fodofnet/www/tenantu/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");

	}

	public function paypalNotify(Request $request)
	{
		//$x = print_r($_REQUEST,1);

		//$myFile = file_put_contents('/home/fodofnet/www/tenantu/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");


		$debug = 1;
		$useSandbox = 1;
		$logfile = Config::get('constants.LOG_FILE');
		//define("LOG_FILE", "/home/fodofnet/www/tenantu/public/log.txt");
		$raw_post_data = file_get_contents('php://input');
		$a = print_r($raw_post_data,1);
		$this->logMsg("row post data =".$a);
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		$this->logMsg("before 1st foreach");
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		$this->logMsg("after 1st foreach");
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		$this->logMsg("before 1st if get_magic_quotes_gpc ");
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		$this->logMsg("after 1st if get_magic_quotes_gpc ");
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}
		$this->logMsg("after 2nd foreach myPost ");
		// Post IPN data back to PayPal to validate the IPN data is genuine
		// Without this step anyone can fake IPN data
		$this->logMsg("before if useSandbox true");
		if($useSandbox == 1) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}
		$this->logMsg("paypal url =".$paypal_url);
		$this->logMsg("after if useSandbox true");
		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if($debug == 1) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		// CONFIG: Optional proxy configuration
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.
		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);
		$res = curl_exec($ch);
		if (curl_errno($ch) != 0) // cURL error
			{
			if($debug == 1) {
				error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, $logfile);
			}
			curl_close($ch);
			exit;
		} else {
				// Log the entire HTTP response if debug is switched on.
				if($debug == 1) {
					error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, $logfile);
					error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, $logfile);
				}
				curl_close($ch);
		}
		// Inspect IPN validation result and act accordingly
		// Split response headers and payload, a better way for strcmp
		$this->logMsg("before explode token");
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		$this->logMsg("before if VERIFIED==0 ");
		if (strcmp ($res, "VERIFIED") == 0) {
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment and mark item as paid.
			// assign posted variables to local variables
			//$item_name = $_POST['item_name'];
			$puchaseId = trim($_POST['custom']);
			$payment_status = trim($_POST['payment_status']);
			$payment_date = trim($_POST['payment_date']);
			$payment_amount = trim($_POST['mc_gross']);
			$txn_id = $_POST['txn_id'];
			$updateBannerPurchase = $this->updateBannerPurchase($puchaseId,$payment_status,$payment_date,$payment_amount,$txn_id);
			//$payment_currency = $_POST['mc_currency'];

			//$receiver_email = $_POST['receiver_email'];
			//$payer_email = $_POST['payer_email'];

			if($debug == 1) {
				error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, $logfile);
			}
		}
		 else if (strcmp ($res, "INVALID") == 0) {
			// log for manual investigation
			// Add business logic here which deals with invalid IPN messages
			if($debug == 1) {
				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, $logfile);
			}
		}
		$this->logMsg("after if INVALID==0 ");

	}

	private function updateBannerPurchase($bannerPurchaseId,$paymentStatus,$paymentDate,$amountPaid,$txn_id)
	{
		//$updateBannerPurchase = DB::update('UPDATE banner_purchases SET purchase_date_from_paypal='.$paymentDate.',payment_status='.$paymentStatus.',amount_paid='.$amountPaid.',txn_id='.$txn_id.' WHERE id='.$bannerPurchaseId.'');
		$updateBannerPurchase = DB::table('banner_purchases')
							 ->where('id',$bannerPurchaseId)
							 ->update(['purchase_date_from_paypal'=>$paymentDate,'payment_status'=>$paymentStatus,'amount_paid'=>$amountPaid,'txn_id'=>$txn_id]);
		return  $updateBannerPurchase;
	}

	// Beta Landing page functions
	public function validateBetaEmail (Request $request) {
		$email = $request->email;

		$betaEmails = DB::table('beta_code_list')->lists('beta_email');

		foreach($betaEmails as $betaEmail) {
			if ($betaEmail == $email)
				return array('validEmail'=>$betaEmail);
		}
		return false;
	}

	public function validateBetaCode (Request $request) {
		$email = $request->email;
		$userInput = $request->code;

		$code = DB::table('beta_code_list')->where('beta_email', $email)->value('beta_code');
		if ($code==$userInput)
			return array('validEmail'=>$email, 'validCode'=>$code);
		return false;
	}

	public function checkIfLoggedIn () {
                if(Auth::user('tenant')){
                $loggedTenantId = Auth::user('tenant')->id;
                $loggedTenantFirstName = DB::table('tenants')->where('id', $loggedTenantId)->value('firstname');
                return $loggedTenantFirstName;
                }
                if(Auth::user('landlord')){
                $loggedLandlordId = Auth::user('landlord')->id;
                $loggedLandlordFirstName = DB::table('landlords')->where('id', $loggedLandlordId)->value('firstname');
                return $loggedLandlordFirstName;
                }
                else{
                $noUserLoggedString = "Log in";
                return $noUserLoggedString;
                }
        }
}
