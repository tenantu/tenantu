<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\School;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class SchoolsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $schoolsQuery = DB::table('schools');
        
        if ($request->has('search')) {
            $search = $request->input('search');
            $schoolsQuery->where('schoolname', 'like', '%' . $request->input('search') . '%');
        }
        $schoolsQuery->orderBy($sortby, $order);
        $schools = $schoolsQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('schools.index', ['pageTitle' => 'Schools',
            'pageHeading'                                => 'Schools',
            'pageDesc'                                   => 'Manage schools here',
            'schools'                                    => $schools,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
        
    }
    
    //this fun is used for  change the status of schol
    public function changeStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $school = School::find($id);
        if ($school->is_active) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }

        $school->update($data);
        return redirect()->route('schools.index')->with('message', 'School status updated');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        return view('schools.create', ['pageTitle' => 'Create school',
            'pageHeading'                                 => 'Create Schools',
            'pageDesc'                                    => 'Create schools here',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $rules = [
            'schoolname'            => 'required',
            'location'              => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
         if (!$validator->fails())
         {
			//print_r($request->all());exit;
			$input['schoolname'] 	= $request->schoolname;
			$input['location']		= $request->location;
			$input['address'] 		= $request->address;
			$input['is_active'] 	= $request->is_active;
			list($latitude, $longitude) = explode('~', $request->hdPlace);;
			$input['latitude']          =  $latitude;
            $input['longitude']         =  $longitude;
            
            if($request->file('image'))
            {
				$imagePath = Config::get('constants.SCHOOL_IMAGE_PATH');
				$image     = $request->file('image');
				$extension = $image->getClientOriginalExtension();
				$filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
				$filename  = str_slug($filename, "-") . '.' . $extension;
				$image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(1366, 721)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
			}
            
			$school = School::create($input);
			return redirect()->route('schools.index')->with('message', 'School Created.');
		 }
		 else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $school   = School::find($id);
        return view('schools.edit', ['pageTitle' => 'Edit school',
            'pageHeading'                               => 'Edit school',
            'pageDesc'                                  => 'Edit school here',
            'school'                                    => $school,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $rules = [
            'schoolname'  => 'required',
            'location'    => 'required',
        ];
        
         $messages = [
            'schoolname.required'  	=> 'Schoolname is required.',
            'location.required' 	=> 'Location is required.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) 
        {
			$input		= array();
            $school   	= School::find($id);
            
            $input  	= $request->all();
            
            if($request->file('image'))
            {
				$imagePath = Config::get('constants.SCHOOL_IMAGE_PATH');
				$image     = $request->file('image');
				$extension = $image->getClientOriginalExtension();
				$filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
				$filename  = str_slug($filename, "-") . '.' . $extension;
				
				$image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(1366, 721)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
			}

            $school->update($input);

            $schoolId 	= School::find($school->id);
            
            if ($schoolId['id']) 
            {
                 return redirect()->route('schools.index')->with('message-success', 'School details are updated.');
            } 
        
        }
        else
        {
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $school = School::find($id);
        $school->delete();
        return redirect()->route('schools.index')->with('message', 'School Deleted.');
    }
    
    public function getSchools()
    {
		$allSchools = School::select('schoolname')
							->addSelect('id')
							->addSelect('address')
							->addSelect('latitude')
							->addSelect('longitude')
							->where('is_active',1)
							->get();
							
		$properties= array();					
		foreach($allSchools as $allSchool)
		{
			$properties = DB::select('SELECT COUNT(*) as propertyCount FROM properties WHERE school_id='.$allSchool->id.' AND status=1');
			$allSchool->propertyCount=$properties[0]->propertyCount;
		}
		//print_r($allSchools);exit;
		return view('schools.getSchools', ['pageTitle' => 'Schools',
            'pageHeading'                              => 'schools',
            'pageDesc'                                 => 'schools',
            'pageMenuName'							   => 'schools',
            'schools'                                  => $allSchools,
            'properties'							   => $properties,
        ]);
	}
	
	public function ajaxSchoolLatitudeLongitude(Request $request)
	{
		$schoolId = $request->schoolId;
		$schoolLocation = School::where('id',$schoolId)
								->select('latitude')
								->addSelect('longitude')
								->first();
		
		return $schoolLocation;
	}
}
