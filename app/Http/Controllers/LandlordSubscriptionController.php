<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Landlord;
use App\Http\Models\LandlordSubscription;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Crypt;


class LandlordSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //View Landlord's subscription plan current on
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $profileUrl       = asset('public/uploads/landlords/');
        $noProfileUrl     = asset('public/uploads/noimage/');
        $landlord_subscription = $this->getLandlordSubscriptionPlan($loggedLandlordId);
        $subscription_plan_id = json_decode($landlord_subscription)->subscription_plan_id;
        $subscriptionPlan = $this->getSubscriptionPlan($subscription_plan_id);

        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }


        return view('landlords.landlordsubscription', [
            'pageTitle' => 'Active Subscription',
            'pageHeading' => 'Active Subscription',
            'pageMenuName' => 'mysubscription',
            'profile' => $landlordProfile,
            'profileImage' => $profileImage,
            'landlordSubscription' => json_decode($landlord_subscription),
            'subscriptionPlan' => json_decode($subscriptionPlan)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $subscriptionPlanId = $request->plan_id;

        $schedule_id = Crypt::encrypt();

        //$schedule_id = $this->getLandlordSubscriptionScheduleId($loggedLandlordId); // Thie helper function decrypts
        $monthly_payment_amount = $this->getSubscriptionPlanInitialBasePrice($subscriptionPlanId);



        // Get Information of Subscription Plan by Id

        $landlord_subscription = LandlordSubscription::firstOrNew(array('landlord_id' => $loggedLandlordId));
        $landlord_subscription->landlord_subscription_schedule_id = $schedule_id;
        $landlord_subscription->subscription_plan_id = $subscriptionPlanId;
        $landlord_subscription->monthly_payment_amount = $monthly_payment_amount;
        $landlord_subscription->save();
    }


    /**
     * Assigns the landlord to the default subsription plan
     * The default subscription plan is the starter plus package, which includes all features for free.
     * This subsscription plan's ID is 4, on the live server and should be on local environments as well
     *
     * @param  int, the id of the landlord being created
     * @return \Illuminate\Http\Response
     */
    public function setLandlordSubscriptionPlanOnCreate($landlordId) {
      //the id of the starter plus subscription plan in the DB. Do not change unless the database and/or the default
      //subscription plan is changed
      $subscriptionPlanId = 4;

      //THIS MIGHT ONLY WORK WHILE WE ARE NOT USING PAID SUBCRIPTIONS.
      //THE CALL TO Crypt::encrypt() ORIGINALLY HAD NO PARAMETERS

      $schedule_id = Crypt::encrypt($landlordId);

      //$schedule_id = $this->getLandlordSubscriptionScheduleId($loggedLandlordId); // Thie helper function decrypts
      $monthly_payment_amount = $this->getSubscriptionPlanInitialBasePrice($subscriptionPlanId);



      // Get Information of Subscription Plan by Id

      $landlord_subscription = LandlordSubscription::firstOrNew(array('landlord_id' => $landlordId));
      $landlord_subscription->landlord_subscription_schedule_id = $schedule_id;
      $landlord_subscription->subscription_plan_id = $subscriptionPlanId;
      $landlord_subscription->monthly_payment_amount = $monthly_payment_amount;
      $landlord_subscription->save();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
