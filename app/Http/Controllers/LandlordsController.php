<?php

namespace App\Http\Controllers;

use App\Http\Models\PropertyPhysical;
use App\Http\Models\PropertyMeta;
use League\Csv\Reader;
use Illuminate\Http\Request;
use Crypt;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\ApartmentBuilding;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\MessageThread;
use App\Http\Models\Thread;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\PropertyAmenity;
use App\Http\Models\landlordTenantEmailInvite;
use App\Http\Models\RatingUser;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\PropertyReview;
use App\Http\Models\Mysearch;
use DB;
use Input;
use Config;
use Validator;
use File;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use View;
use Redirect;
use Session;
use HTML;
use Mail;
use URL;
use DateTime;
use Carbon\Carbon;
use App\Http\Helpers\UploadHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class LandlordsController extends Controller
{

	public function getLogin(){
		return view('landlords.login');
	}

	public function postLogin( Request $request ){

		$rules = array(
			'email'            => 'required|email|',     // required and must be unique in the ducks table
			'password'         => 'required'
		);


		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}else{

			if (Auth::attempt("landlord", ['email' => $request->input('email'), 'password' => $request->input('password'),'is_active'=>1])){

				return redirect('landlords/dashboard');
			}
	    else{
				return redirect('landlords/login')->with('errors', 'please check the login credentials');
			}
		}
	}

	public function logout(){
		Auth::logout('landlord');
		return redirect('/login');
	}

	public function dashboard()
	{
		//echo "1";exit;
        $loggedLandlord   = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile  = $this->getLandlordProfile($loggedLandlordId);
        $properties       = $this->getProperties( $loggedLandlordId );
        $propertiesReview = PropertyReview::from('property_review AS PR')
                                 -> select('P.id')
                                 -> addSelect('P.property_name')
                                 -> addSelect('P.slug')
                                 -> join ('properties AS P','PR.property_id','=','P.id')
                                 -> where('P.landlord_id', $loggedLandlordId)
                                 -> where('PR.status', 1)
                                 -> orderBy('PR.created_at','DESC')
                                 -> groupBy('PR.property_id')
                                 -> take(4)
                                 -> get();
        //dd($propertiesReview);
        $pageTitle        = "Dashboard";
        $originalProfile  = HTML::getLandlordProfileImage();
        $generalMessages  = Message::from('messages AS M')
                                 -> select('M.message')
                                 -> addSelect('M.message_thread_id')
                                 -> addSelect('T.thread_name')
                                 -> join ('message_threads AS MT','MT.id','=','M.message_thread_id')
                                 -> join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 -> where('M.landlord_id', $loggedLandlordId)
                                 -> where('T.flag',0)
                                 -> where('T.status',1)
                                 -> where('M.sender','T')
                                 -> orderBy('M.created_at','DESC')
                                 -> take(4)
                                 -> get();

        $issueMessages  = Message::from('messages AS M')
                                 -> select('M.message')
                                 -> addSelect('M.message_thread_id')
                                 -> addSelect('T.thread_name')
                                 -> join ('message_threads AS MT','MT.id','=','M.message_thread_id')
                                 -> join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 -> where('M.landlord_id', $loggedLandlordId)
                                 -> where('T.flag',1)
                                 -> where('T.status',1)
                                 -> where('M.sender','T')
                                 -> orderBy('M.created_at','DESC')
                                 -> take(4)
                                 -> get();
        $landlord_subscription = $this->getLandlordSubscriptionPlan($loggedLandlordId);

        $subscription_plan_id = json_decode($landlord_subscription)->subscription_plan_id;
        $subscriptionPlan = $this->getSubscriptionPlan($subscription_plan_id);

        //dd($issueMessages);
        $school                     = $this->getSchoolDetails();
		return view('landlords.dashboard',['loggedLandlordId'=> $loggedLandlordId,
                                           //'pageTitle'       => $pageTitle,
                                           'pageMenuName'	 => 'dashboard',
                                           'profileImage'    => $originalProfile,
                                           'profile'         => $landlordProfile,
                                           'properties'      => $properties,
                                           'generalMessages' => $generalMessages,
                                           'issueMessages'   => $issueMessages,
                                           'propertiesReview'=> $propertiesReview,
                                           'schools'         => $school,
                                           'subscriptionPlan' => json_decode($subscriptionPlan)
                                         ]);
	}
    public function profile()
    {
        $loggedLandlord     = Auth::user('landlord');
        $imageUrl           = asset('public/uploads/landlords/');
        $noImageUrl         = asset('public/uploads/noimage/');
        $landlordProfile    = $this->getLandlordProfile($loggedLandlord->id);
        $gender             = $this->getGender($landlordProfile->gender);
        $dob                = $this->getDob($landlordProfile->dob);
        $state              = $this->getState($landlordProfile->state);
        $city               = $this->getCity($landlordProfile->city);
        $language           = $this->getLanguage($landlordProfile->language);
        $properties         = $this->getAllProperty( $loggedLandlord->id );
        //dd( $properties );
        if(count( $properties )){
            foreach( $properties as $property ){
                $propertyIdArray[]  = $property->id;
            }
             $propertyIdString = implode(',',$propertyIdArray);
             $rating           = HTML::getAverageReviewRatingForLandlord( $propertyIdString );
             $review           = $this->getReviewCount($propertyIdString);
        }
        else{
            $rating  = asset('public/img/stars0.png');
            $review  = 0;
        }

        if($landlordProfile->image!='')
        {
            $profileImage=$imageUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noImageUrl."/landlord-blank.png";
        }
        $originalProfile=HTML::getLandlordProfileImage();


         return view('landlords.profile', [
            'pageTitle'    => 'Profile',
            'pageHeading'  => 'Profile',
            'pageMenuName' => 'Profile',
            'imageUrl'     => $imageUrl,
            'profile'      => $landlordProfile,
            'birthdate'    => $dob,
            'gender'       => $gender,
            'state'        => $state,
            'city'         => $city,
            'language'     => $language,
            'profileImage' => $originalProfile,
            'rating'       => $rating,
            'review'       => $review
        ]);
    }

	public function editprofile()
    {
        $imageUrl         = asset('public/uploads/landlords/');
        $noImageUrl       = asset('public/uploads/noimage/');
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $schools          = $this->getSchoolDetails();

        if($landlordProfile->image!='')
        {
            $profileImage=$imageUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noImageUrl."/landlord-blank.png";
        }
        $originalProfile=HTML::getLandlordProfileImage();
        return view('landlords.editprofile', [
            'pageTitle'     => 'EditProfile',
            'pageHeading'   => 'EditProfile',
            'pageMenuName'  => 'editprofile',
            'imageUrl'      => $imageUrl,
            'profile'       => $landlordProfile,
            'schools'       => $schools,
            'profileImage'  => $originalProfile
        ]);
    }
	public function getChangePassword(Request $request)
	{
		$id = $request->id;
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
		return view('landlords.changepassword',[
            'pageTitle'     => 'ChangePassword',
            'pageMenuName'  => 'changepassword',
            'profile'         => $landlordProfile,
            'profileImage'    => $originalProfile,
            ]);
	}

	public function postChangePassword(Request $request)
	{
		$this->validate($request, [
            'current_password' => 'required',
            'password'         => 'required|confirmed|min:6',
        ]);

        $userUpdate = $request->all();
        $value      = $userUpdate['current_password'];
        $landlord     = Auth::User('landlord');
       // print_r($tenant->password);exit;
        if (Hash::check($value, $landlord->password))
        {
			$userpassword = Hash::make($userUpdate['password']);
			$landlor      = DB::table('landlords')
                ->where('id', $landlord->id)
                ->update(['password' => $userpassword]);
            return redirect()->route('landlords.dashboard')->with('message-success', 'Your password has been updated.');

		}
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $landlordsQuery = DB::table('landlords');
        if ($request->has('search')) {
            $search = $request->input('search');
            $landlordsQuery->where('firstname', 'like', '%' . $request->input('search') . '%');
        }
        $landlordsQuery->orderBy($sortby, $order);
        $landlords = $landlordsQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('landlords.index', ['pageTitle' => 'Landlords',
            'pageHeading'                                => 'Landlords',
            'pageDesc'                                   => 'Manage Landlords here',
            'landlords'                                  => $landlords,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
    }
    public function posteditprofile(Request $request)
    {
        //dd($request);
        $loggedLandlord = Auth::user('landlord');
        $rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'phone'                 => 'required|regex:/^\d{3}[\-]\d{3}[\-]\d{4}$/',
            'school_id'             => 'required',
            'description'           => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails())
        {
            $input['firstname'] = $request->firstname;
            $input['lastname']  = $request->lastname;
            $input['location']  = $request->location;
            $input['phone']     = $request->phone;
            $input['about']     = $request->about;
            $input['school_id'] = $request->school_id;
            $input['website']   = $request->website;
            $input['state']     = $request->state;
            $input['address']   = $request->address;
            $input['language']  = $request->language;
            $input['description']  = $request->description;
            $input['city']      = $request->city;
            //this code is used to upload the image
            if($request->file('image'))
            {

                $imagePath = asset('/public/uploads/landlords/');

                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                //$imagePath = $imagePath . $loggedTenant->id;


                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(144, 144)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
            }


            $updateTenantDetails = Landlord::where('id', '=', $loggedLandlord->id)->update($input);

            //end of the image upload code
            return redirect()->route('landlords.profile')->with('message-success', 'Details are updated.');
        }
        else{
            return redirect()->route('landlords.editprofile')->withErrors($validator->errors())->withInput();
        }
    }
    public function posteditprofileimage(Request $request)
    {
        //dd($request->all());
        $loggedLandlord = Auth::user('landlord');

        if($request->file('image'))
            {

                $imagePath = Config::get('constants.LANDLORD_IMAGE_PATH');

                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                //$imagePath = $imagePath . $loggedTenant->id;


                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(144, 144)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
                $updateTenantDetails = Landlord::where('id', '=', $loggedLandlord->id)->update($input);

                //end of the image upload code
                return redirect()->route('landlords.editprofile')->with('message-success', 'Profile image has been updated.');
            }

    }

public function haversineGreatCircleDistance(
 	 $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 3959){
  	// convert from degrees to radians
  	$latFrom = deg2rad($latitudeFrom);
  	$lonFrom = deg2rad($longitudeFrom);
  	$latTo = deg2rad($latitudeTo);
  	$lonTo = deg2rad($longitudeTo);

  	$latDelta = $latTo - $latFrom;
  	$lonDelta = $lonTo - $lonFrom;

  	$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
    	cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  	return $angle * $earthRadius;
	}

 public function postPropertyCsvFile(Request $request)
    {
         $loggedLandlord = Auth::user('landlord');
         $loggedLandlordId = $loggedLandlord->id;
         if($request->file("csv"))
            {
                //grabbing and storing the csv on our site
                $csvPath = Config::get('constants.LANDLORD_CSV_PATH');
                $csv     = $request->file("csv");
                $extension = $csv->getClientOriginalExtension();
                $filename  = trim(pathinfo($csv->getClientOriginalName(), PATHINFO_FILENAME));
                $uId = uniqid();
                $filename  = str_slug($filename, "-")."-".$uId."-".$loggedLandlordId.'.' . $extension;
                $csv->move($csvPath, $filename);
                //opening the file, parsing the csv into an array, send out invites
                $readFile = Reader::createFromPath($csvPath."/".$filename);
                $fileContents = $readFile->fetch();
                //set some variables that will be reused when sending out emails, also set up file name and base text for error log
                $errorFileName  = str_slug($filename, "-")."-errors.doc";
                $textToWrite = "Warning: There were errors in your uploaded .csv file. Below, you will find detailed information about the error(s). For the affected line(s), fix the indicated errors and then reupload the .csv file with the changes.\n\n";
                $ignoreFirst = 0;
                $lineCount = 0;
                $errorCount = 0;
	        $successfulMatchedAmenities = [];
		$successfulPropertyCredentials = [];
                $landlordProfile =  $this->getLandlordProfile($loggedLandlordId);
                $landlordSchoolId = $landlordProfile->school_id;
                $landlordSchoolName =  $this->getSchoolWithId($landlordSchoolId)->schoolname;
           	$landlordName = $loggedLandlord->firstname.','.$loggedLandlord->lastname;
		$schoolArray = $this->getSchoolDetails();
		$amenitiesArray = $this->getAminityDetails();
                //begin parsing the csv
                foreach ($fileContents as $row){
			$matchedAmenities = [];
                        $isMatch = 0;
			$matchCount = 0;
			$correctAmenityCount = 0;
			$errorFlag = 0;
                        $lineCount = $lineCount + 1;
                        if($ignoreFirst==0){
                        $ignoreFirst = 1;
                        }
                        else{
			// use for apartment csv add
			$apartmentId = $row[0];
			$unitNum = $row[1];
			if($apartmentId != ""){
                		$landlordApartmentIdList = $this->getApartmentIdList($loggedLandlordId);
				foreach($landlordApartmentIdList as $singleId){
                        	if($apartmentId == $singleId->id){
                        	$isMatch = 1;
                        	}
                        	}
                        if($isMatch == 0){
                        $textToWrite = $textToWrite."There was an error with the input on line ".$lineCount." with the Apartment ID field as ". $apartmentId. " did not match one of your Apartment ID's provided on the add property page.\n\n";
                        $errorFlag = 1;
                        $errorCount = $errorCount + 1;
                        }
			}
			if($apartmentId != ""){
			$propertyType = "apartment";
			}
			else{
			$apartmentId = NULL;
			$propertyType = "house";
			}
                        $propertyTitle = $row[2];

			$streetAddress = $row[3];
			$state = $row[4];
			$city = $row[5];
			$zipCode = $row[6];
			// Append United States cause no international right now
			$location = $streetAddress . ", " . $city . ", " . $state . ", United States";

			if($propertyTitle==""){
			$propertyTitle = $location;
			}
			$address = $location; // inputted address
            		$prepAddr = str_replace(' ','+',$address); //preparing address
            		$geocode= $this->getCoordsFromAddress($prepAddr);
			if($geocode["lat"] != null){
            		$addressLatitude = $geocode["lat"];
            		$addressLongitude = $geocode["lng"];
			}
			else{
			$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Address fields as '.$location." is not a valid address. Please make sure you are typing it with the correct format. \n\n";
			$errorFlag = 1;
			$errorCount = $errorCount + 1;
			}

                        $bedrooms = $row[7];
			//if bedrooms input is not int then print error
			if(is_numeric($bedrooms) == false){
			$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Bedrooms field as '. $bedrooms. " was not a valid number input.\n\n";
                        $errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
			$bathrooms = $row[8];
			//if bathrooms input is not int print error
			if(is_numeric($bathrooms) == false){
			$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Bathrooms field as '. $bathrooms. " was not a valid number input.\n\n";
                        $errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
			$perBed = $row[9];
			if(strcasecmp($perBed, "bed") == 0){
			$perBed = 1;
			}
			else if(strcasecmp($perBed, "unit") == 0){
			$perBed = 0;
			}
			else{
			$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Per-bed / Per-unit field as '. $perBed. " was not one of the valid inputs accepted. Please refer to the description on the add\n\n";
                        $errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
			$maxOcc = $row[10];
			if(is_numeric($maxOcc) == false){
			$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Max Occupancy field as '. $maxOcc. " was not a valid number input.\n\n";
                        $errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
			$rentPrice = $row[11];
			//if rent price is not int then print error
			if(is_numeric($rentPrice) == false){
			$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Rent field as '. $rentPrice. " was not a valid number input. Do not append any dollar signs to this value, just the amount.\n\n";
                        $errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
			$propertyDescription = $row[12];
			$amenities = $row[13];
			$amenities = explode( ";", $amenities);
			//if there is one amenity
			if(count($amenities) == 1){
			//if there are no amenities
			if($amenities[0] == ""){
			//do nothing, that's fine
			}
			else{
                        $isMatch = 0;
                        if($amenities != ""){
			foreach($amenitiesArray as $singleAmenity){
                        if(strcasecmp($amenities[0], $singleAmenity) == 0){
                        $isMatch = 1;
			$matchCount += 1;
			$matchedAmenities[$matchCount] = $amenities[0];
                        }
                        if($singleAmenity === end($amenitiesArray)){
                                if($isMatch == 0){
                                $textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Amenities field as '.$amenities[0]." was not a valid amenity. It may have been misspelled, if not and we are missing an amenity you need let support know!\n\n";
                                $errorFlag = 1;
                                $errorCount = $errorCount + 1;
                                }
                        }
                        }
			}
			}
			}
			//if there is more than one amenity
			else{
			//check if all amenities match available ones in database
			foreach($amenities as $amenity){
			if((strlen(trim($amenity)) != 0)){
			$isMatch = 0;
			foreach($amenitiesArray as $singleAmenity){
			if(strcasecmp($amenity, $singleAmenity) == 0){
			$isMatch = 1;
			$matchCount += 1;
			$matchedAmenities[$matchCount] = $amenity;
			}
			if($singleAmenity === end($amenitiesArray)){
				if($isMatch == 0){
				$textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the Amenities field as '.$amenity." was not a valid amenity. It may have been misspelled, if not and we are missing an amenity you need let support know!\n\n";
				$errorFlag = 1;
				$errorCount = $errorCount + 1;
				}
			}
			}
			}
			}
			}
                        $inputSchool = $row[14];
			$isMatch = 0;
			if(strlen(trim($inputSchool)) == 0){
			$isMatch = 1;
			$schoolToUse = $landlordSchoolName;
			}
			else{
			foreach ($schoolArray as $school){
			if(strcasecmp($inputSchool, $school) == 0){
				$isMatch = 1;
				$schoolToUse = $school;
			}
			}
			}
                        if($isMatch == 0){
                                $textToWrite = $textToWrite.'There was an error with the input on line '.$lineCount.' with the School field '.$inputSchool." was not a valid school name. If the school you tried to enter is not on our site yet contact support and suggest it to us!\n\n";
                                $errorFlag = 1;
                                $errorCount = $errorCount + 1;
                        }
			else{
			$schoolData = $this->getSchoolInfoWithName($schoolToUse);
			}
			if($errorFlag == 0){
			$distanceFromSchool = $this->getDistanceFromSchool($prepAddr, $schoolData->id);
		    	$insertArray                         =  array();
            		$insertArrayMeta		     =  array();
			$insertArray['property_name']        =  $propertyTitle;
	    		if($propertyType == "house" || $propertyType == "House"){
	    			$propertyType = 1;
	    		}
	   		else{
				$propertyType = 0;
	    		}
			$insertArray['apartment_building_id']	     =	$apartmentId;
			$insertArray['premise']		     =  $unitNum;
            		$insertArray['property_type']        =  $propertyType;
            		$insertArray['landlord_id']          =  $loggedLandlordId;
            		$insertArray['school_id']            =  $schoolData->id;
			$insertArray['administrative_area']  =  $state;
			$insertArray['locality'] 	     =  $city;
			$insertArray['postal_code']	     =  $zipCode;
			$insertArray['thoroughfare']	     =  $streetAddress;
            		$insertArray['bedroom_no']           =  $bedrooms;
            		$insertArray['bathroom_no']          =  $bathrooms;
            		$insertArrayMeta['rent_price']           =  $rentPrice;
            		$insertArray['description']          =  $propertyDescription;
            		$insertArrayMeta['communication_medium'] =  "W";
            		$insertArray['distance_from_school'] =  $distanceFromSchool;
            		$insertArray['latitude']             =  $addressLatitude;
            		$insertArray['longitude']            =  $addressLongitude;
			$insertArrayMeta['charge_per_bed']   =  $perBed;
			$insertArray['max_occupancy']	     =  $maxOcc;
            		$searchField  = $request->property_name.','.$schoolToUse.','.$propertyDescription.','.$location.','.$landlordName;
            		$insertArrayMeta['search_field']       =  $searchField;
            		$insertArray['landlord_prop_mgmt_id'] = NULL;
			$successfulPropertyCredentials[$lineCount - 2] = $insertArray;
			$successfulMetaCredentials[$lineCount - 2] = $insertArrayMeta;
			$successfulMatchedAmenities[$lineCount - 2] = $matchedAmenities;
			}
			}
			}
                if($errorCount>0){
		$textToWrite = $textToWrite.'If you continue to experience issues please contact us at support@tenantu.com';
		file_put_contents($csvPath."/".$errorFileName,$textToWrite);
                return response()->download($csvPath."/".$errorFileName, $errorFileName);
                }
                else{
		for($i=0; $i<count($successfulMatchedAmenities); $i++){
		    $data = [];
		    $property = PropertyPhysical::create($successfulPropertyCredentials[$i]);
		    $property->save();
		    $propertyId = $property->id;
		    $successfulMetaCredentials[$i]['property_physical_id'] = $propertyId;
		    $propertyMeta = PropertyMeta::create($successfulMetaCredentials[$i]);
		    $propertyMeta->save();
                    $currentMatchedAmenities = $successfulMatchedAmenities[$i];
			if(count($currentMatchedAmenities) > 1){
                        foreach ($currentMatchedAmenities as $amenity){
                        $data[] = array('property_id'=>$propertyId,
                                          'amenity'=>$amenity,
                                          'created_at'=>date('Y-m-d H:i:s'),
                                          'updated_at'=>date('Y-m-d H:i:s'));
                                }
			$success = PropertyAmenity::insert($data);

                        if(!empty($currentMatchedAmenities)){
                        $amenitiesUpdate = implode(',', $currentMatchedAmenities);
                        $propertyUpdate  = PropertyPhysical::where('id',$property->id)
                                                    ->update(['amenities'=>$amenitiesUpdate]);
                        }
			}
			if(count($currentMatchedAmenities) == 1){
                        $data[] = array('property_id'=>$property->id,
                                          'amenity'=>$currentMatchedAmenities[1],
                                          'created_at'=>date('Y-m-d H:i:s'),
                                          'updated_at'=>date('Y-m-d H:i:s'));

                         $success = PropertyAmenity::insert($data);
                         $propertyUpdate  = PropertyPhysical::where('id',$property->id)
                                                    ->update(['amenities'=>$currentMatchedAmenities[1]]);
                        }
                    }
                return redirect()->route('landlords.dashboard')->with('message-success', 'Your properties have been added!');
                }


        }
    }

    public function postCsvFile(Request $request)
    {
	 $loggedLandlord = Auth::user('landlord');
         $loggedLandlordId = $loggedLandlord->id;
	 if($request->file("csv"))
            {
                //grabbing and storing the csv on our site
		$csvPath = Config::get('constants.LANDLORD_CSV_PATH');
                $csv     = $request->file("csv");
                $extension = $csv->getClientOriginalExtension();
                $filename  = trim(pathinfo($csv->getClientOriginalName(), PATHINFO_FILENAME));
		$uId = uniqid();
                $filename  = str_slug($filename, "-")."-".$uId."-".$loggedLandlordId.'.' . $extension;
                $csv->move($csvPath, $filename);
		//opening the file, parsing the csv into an array, send out invites
		$readFile = Reader::createFromPath($csvPath."/".$filename);
		$fileContents = $readFile->fetch();
		//set some variables that will be reused when sending out emails, also set up file name and base text for error log
	        $errorFileName  = str_slug($filename, "-")."-errors.doc";
		$textToWrite = "Warning: There were errors in your uploaded .csv file. Below, you will find detailed information about the error(s). For the affected line(s), fix the indicated errors and then reupload the .csv file with the changes.\n\n";
		$ignoreFirst = 0;
		$lineCount = 0;
		$errorCount = 0;
		$isMatch = 0;
		$successfulFullLinks = [];
		$successfulInviteEmails = [];
		$successfulReasons = [];
                $landlordProfile =  $this->getLandlordProfile($loggedLandlordId);
                $landlordFirstName = $landlordProfile->firstname;
                $landlordLastName = $landlordProfile->lastname;
                $landlordFullName = $landlordProfile->getFullNameAttribute();
                $landlordSchoolId = $landlordProfile->school_id;
                $landlordSchoolName =  $this->getSchoolWithId($landlordSchoolId)->schoolname;
                $landlordEmail = $landlordProfile->email;
		$tenantRentDue = "+".$this->getLandlordRentDueDay($loggedLandlordId);
		$landlordPropertyIdList = $this->getPaidPropertyIdList($loggedLandlordId);
		$validLeaseInputs = [0=>"3months",
				     1=>"6months",
				     2=>"1year",
				     3=>"2year",
				     4=>"3year"];
		//begin parsing the csv
		foreach ($fileContents as $row){
			$errorFlag = 0;
			$isMatch = 0;
			$lineCount = $lineCount + 1;
			if($ignoreFirst==0){
			$ignoreFirst = 1;
			}
			else{
			$propertyId = $row[0];
			//if property Id is not valid then print error
			foreach($landlordPropertyIdList as $singleId){
			if($propertyId == $singleId->id){
			$isMatch = 1;
			}
			}
			if($isMatch == 0){
			$textToWrite = $textToWrite."There was an error with the input on line ".$lineCount." with the Property ID field as ". $propertyId. " did not match one of your Property ID's provided on the add tenant page.\n\n";
			$errorFlag = 1;
			$errorCount = $errorCount + 1;
			}
			$tenantName = $row[1];
			$tenantEmail = $row[2];
			//if tenant email is not of type edu then print error
			$afterDot = substr($tenantEmail, strpos($tenantEmail, ".") + 1);
		        while(strpos($afterDot,'.') !== false){
			$afterDot = substr($tenantEmail, strpos($tenantEmail, ".") + 1);
			}
			if(!($afterDot == "edu")){
			$textToWrite = $textToWrite."There was an error with the input on line ".$lineCount." with the Email field as ". $tenantEmail. " does not end in .edu as emails for tenants are required to.\n\n";
			$errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
			$cycle = $row[3];
			//if lease length input does not match the form it needs to then print error
			if(!(in_array($cycle,$validLeaseInputs))){
                        $textToWrite = $textToWrite."There was an error with the input on line ".$lineCount." with the Lease Length field as ". $cycle. " does not match one of the valid inputs provided on the add tenant page.\n\n";
			$errorFlag = 1;
                        $errorCount = $errorCount + 1;
			}
		        $agreementStartDate = $row[4];
			if(!($this->validateDate($agreementStartDate, 'Y-m-d'))){
			$textToWrite = $textToWrite."There was an error with the input on line ".$lineCount." with the Agreement Start Date field as ".$agreementStartDate." is not in the correct form, which is YYYY-MM-DD.\n\n";
			$errorFlag = 1;
			$errorCount = $errorCount + 1;
			}
			if($errorFlag == 0){
			$loopCount = "";
            		if(($cycle)!=""){
                		switch($cycle){
                       			case "3months":
                         		$loopCount = 3;
                       			break;
                       			case "6months":
                         		$loopCount = 6;
                       			break;
                       			case "1year":
                         		$loopCount = 12;
                       			break;
                       			case "2year":
                         		$loopCount = 24;
                       			break;
                       			case "3year":
                          		$loopCount =36;
                       			break;
                		}
            		}
            //Factor is the amount of payments to be made after taking into account landlord collection frequency
            		switch($loopCount){
                		case 3:
                    		$factor = 1;
                    		break;
                		case 6:
                    		$factor = 1;
                    		break;
                		default:
                    		$factor = $this->getFactorByLandlordRentCollectionFreq($loggedLandlordId);
                    		break;
            		}

        	list($yearStart, $monthStart, $dayStart) =  explode('-', $agreementStartDate);
         	$startYear                               =  $yearStart.'-'.$monthStart.'-'.$dayStart;
         	$totalDurationStart  = date("Y-m-d", strtotime("+0 month", strtotime($startYear)));
         	$totalDurationEnd  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
         	$agreementEndDate = $totalDurationEnd;
         	$passLoopCount = $loopCount;
         	$passFactor = $factor;
	 	$propertyIdLink = "+".$propertyId;
		$propertyPrice = $this->getPropertyPrice($propertyId);
		$propertyCount = $this->getPropertyCurrentTenantCount($propertyId);
		$propertyCount = $propertyCount + 1;
		//if per-bed flag is no then do this
		if($this->getPropertyPerBed($request->poperty_id)[0]["charge_per_bed"] == 0){
		$actualPropertyPrice = $propertyPrice / $propertyCount;
		}
		//if per-bed flag is yes then just do $propertyPrice
         	else{
		$actualPropertyPrice = $propertyPrice;
		}
		$tenantMonthRent = "+".$actualPropertyPrice; //get current tenant count + 1 to count themselves being added to factor into the price
         	$tenantAgreementStart = "+".$startYear;
         	$tenantCycle = "+".$cycle;
         	$linkPart = $loggedLandlordId.$propertyIdLink.$tenantMonthRent.$tenantRentDue.$tenantCycle."+".$monthStart."+".$dayStart."+".$yearStart.$tenantAgreementStart."+".$totalDurationEnd."+".$passLoopCount."+".$passFactor;
         	$fullLink = "http://tenantu.com/signup#" . Crypt::encrypt($linkPart);
         	$successfulFullLinks[$lineCount - 2] = $fullLink;
		$successfulInviteEmails[$lineCount - 2] = $tenantEmail;
	 	$reason = $tenantName . "! " . "Your landlord, " . $landlordFullName . ", has invited you to join TenantU!";
	 	$successfulReasons[$lineCount - 2] = $reason;
			}
			}
		}
		if($errorCount>0){
		file_put_contents($csvPath."/".$errorFileName,$textToWrite);
                return response()->download($csvPath."/".$errorFileName, $errorFileName);
		}
		else{
		for($i=0;$i<count($successfulFullLinks);$i++){
			$inviteEmail["landlord_id"] = $loggedLandlordId;
               		$inviteEmail["tenant_invite_email"] = $successfulInviteEmails[$i];
			$tenantEmail = $successfulInviteEmails[$i];
			$addingEmailInviteEntry = landlordTenantEmailInvite::create($inviteEmail);
			Mail::send('emails.invite-tenants',['name'=>$landlordFullName,'email'=>$landlordEmail,'school'=>$landlordSchoolName,'reason'=>$successfulReasons[$i], 'link'=>$successfulFullLinks[$i]],
                         function($message) use ($tenantEmail) {
                                              $message->to($tenantEmail)
                                                        ->subject("Your landlord has invited you to join TenantU!");
                        });

		}
                return redirect()->route('landlords.addtenant')->with('message-success', 'Your .csv file has been uploaded and your tenants have been invited based on the form!');
        	}
	}
    }

    public function validateDate($date, $format = 'Y-m-d H:i:s')
    {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$school = $this->getSchoolDetails();
        return view('landlords.create', ['pageTitle' => 'Create Landlord',
            'pageHeading'                   => 'Create Landlord',
            'pageDesc'                      => 'Create Landlord here',
            'school'                        => $school,
        ]);
    }

    //these func is used for getting the school
    public function  getSchoolDetails()
     {
		 $school = DB::table('schools')->where('is_active',1)->lists('schoolname', 'id');
		 if(!empty($school))
		 {
			 return $school;
		 }else
		 {
			 return false;
		 }
	 }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

         $rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'school'                => 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'email'                 => 'required|email|unique:landlords',
        ];
        $input     = $request->except('image');
        $imagePath = Config::get('constants.LANDLORD_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
			$input['firstname'] = $request->firstname;
			$input['lastname'] = $request->lastname;
			$input['school_id'] = $request->school;
			$input['email'] = $request->email;
            $input['password'] = bcrypt($input['password']);

            $registeredEmail = $this->isRegistered($input['email']);
			if(count($registeredEmail) > 0)
			{
				return redirect()->route('landlords.create')->with('message-error', 'This email is already registered !');
			}
            $landlord = Landlord::create($input);

            if ($request->file('image')) {
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                $imagePath = $imagePath . $landlord['id'];
                if (!File::exists($imagePath)) {
                    // path does not exist
                    File::makeDirectory($imagePath, $mode = 0777, true, true);
                    File::copy(Config::get('constants.HTACCESS_FILE'), $imagePath . DS . '.htaccess');
                    File::makeDirectory($imagePath . '/60_x_60', $mode = 0777, true, true);

                }

                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(60, 60)->save($imagePath . '/60_x_60/' . $filename);
                // $userid = $user->id;
                $landlord = Landlord::find($landlord->id);
                $landlord->update(['image' => $filename]);

            }
            return redirect()->route('landlords.index')->with('message', 'Landlord Created.');
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }


	private function isRegistered($email)
    {
		$users = DB::select('SELECT email
							FROM tenants
							WHERE email ="'.$email.'"
							UNION
							SELECT email
							FROM landlords
							WHERE email = "'.$email.'"');
		return $users;
	}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $imageUrl       	= asset('public/uploads/landlords/');
        $noImageUrl			= asset('public/uploads/noimage/');
        $landlord           = Landlord::find($id);
        $school 			= $this->getSchoolDetails();

        list($landlord['year'], $landlord['month'], $landlord['day']) = explode('-', $landlord['dob']);
        $locations        = [];
        if ($landlord->location) {
            $locations = explode('//', $landlord->location);
        }


		if($landlord->image!='')
		{
			$profileImage=$imageUrl."/".$landlord->image;
		}
		else
		{
			$profileImage=$noImageUrl."/tenant-blank.png";
		}
        return view('landlords.edit', ['pageTitle' => 'Edit Landlord',
            'pageHeading'                               => 'Edit Landlord',
            'pageDesc'                                  => 'Edit Landlord here',
            'landlord'                                  => $landlord,
            'imageUrl'                                  => $imageUrl,
            'profileImage'								=> $profileImage,
            'school'                                    => $school,
            'locations'                                 => $locations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $rules = [
            'firstname'  => 'required',
            'lastname'	 => 'required',
            'school'	 => 'required',
            'email' 	 => 'required|email',
        ];

        $messages = [
            'firstname.required'  => 'First name is required.',
            'lastname.required'   => 'Last name is required.',
            'school.required'  	  => 'Please select the school.',
            'email.required' 	  => 'A valid email is required.',
        ];



        $input     = $request->except('image');
        $imagePath = Config::get('constants.LANDLORD_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {

            $landlord         = Landlord::find($id);

            $landlord->update($input);

            $landlordId = Landlord::find($landlord->id);

            if ($landlordId['id']) {
                 return redirect()->route('landlords.index')->with('message-success', 'Landlord Profile is updated.');
            }

        }else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $landlord = Landlord::find($id);
        $landlord->delete();
        return redirect()->route('landlords.index')->with('message', 'Landlord Deleted.');
    }

    public function changeStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $landlord = Landlord::find($id);
        if ($landlord->is_active) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }

        $landlord->update($data);
        return redirect()->route('landlords.index')->with('message', 'Landlord Status Updated');
    }

    public function getForgotPassword(){
		$pageTitle = 'Forgot Password';
        return view('landlords.forgotpassword')->with(compact('pageTitle'));
	}

	public function postForgotPassword(Request $request){
		$this->validate($request, ['email' => 'required|email']);
        $postedEmail = $request['email'];
        $dataWithEmail = Landlord::where('email', '=', $postedEmail)->first();
        //$dataWithEmail = DB::table('landlords')->where('email',$postedEmail)->first();
        if(!empty($dataWithEmail)){
			$currentDate = Carbon::now();
			$key = $currentDate->getTimestamp();
			$password_token = hash_hmac('sha256', str_random(40), $key);
			$originalToken = $key."_".$password_token;
			$dataWithEmail->password_token = $originalToken;
			$dataWithEmail->save();
			$emailFromAddress = Config::get('constants.FORGOT_PASSWORD_EMAIL_FROM');
			$emailFromName = Config::get('constants.FORGOT_PASSWORD_EMAIL_FROM_NAME');

			\Mail::send('emails.passwordLandlords', ['token' => $originalToken], function($message) use ($dataWithEmail,$emailFromAddress,$emailFromName){
				$message->from($emailFromAddress,$emailFromName);
                $message->to($dataWithEmail->email, $dataWithEmail->name)
                        ->subject('reset your password');
            });
            return redirect()->route('landlords.forgotpassword')->with('message', 'Mail sent  please check your email');

		}else{
			return redirect()->back()->withErrors('message', 'not a valid email');
		}
	}

	public function getResetPassword($token){
		if($token!=''){
		$pageTitle = 'Password reset';
        return view('landlords.resetpassword')->with(compact(array('pageTitle','token')));

		}else{
			return redirect()->back()->withErrors('message', 'Token must be needed');
		}
	}

	public function postResetPassword(Request $request)
	{

		 $this->validate($request, [
            'token' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $credentials = $request->only(
            'password', 'password_confirmation', 'token'
        );

		$updatedPassword = DB::table('landlords')
						->where('password_token',$request->token)
						->update(['password' =>bcrypt($request->password)]);

		$updatedPassword = DB::table('landlords')
						->where('password_token',$request->token)
						->update(['password_token' =>'']);

		return redirect('landlords/login');

	}

	public function register(){
		return view('landlords.register');
	}

	public function signup(Request $request){

		$this->validate($request, [
            'name' 					=> 'required',
            'password' 				=> 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            'email'                 => 'required|email|unique:tenants'
        ]);

            $input['name']     = $request->name;
            $input['password'] = bcrypt($request->password);
            $input['email']    = $request->email;
            $input['gender']   = $request->gender;
            $input['dob']      = $request->year. "-" . $request->month . "-" . $request->day;
            $input['location'] = $request->location;

			$tenant = Landlord::create($input);


	}
    public function addProperty(){

        $loggedLandlordId = Auth::user('landlord')->id;
        $pageTitle        = "Property Add";
		$pageMenuName	  = 'dashboard';
        $school           = $this->getSchoolDetails();
        $originalProfile  = HTML::getLandlordProfileImage();
        $profileImage     = $originalProfile;
        $profile          = $this->getLandlordProfile($loggedLandlordId);
        $properties       = $this->getProperties( $loggedLandlordId );
        $aminities  	  = $this->getAminityDetails();
		$apartments = $this->getApartmentBuildings($loggedLandlordId);
		return view('property.add')->with(compact('loggedLandlordId','pageTitle','pageMenuName','school','properties','profileImage','profile','aminities','apartments'));
    }

	private function getApartmentBuildings($landlordId) {
		$apartments = ApartmentBuilding::where('landlord_id', $landlordId)->get();
		$apartments = $apartments->toArray();
		return $apartments;
	}

	 private function getAminityDetails()
	 {
		 $aminity = array();
		 $aminity  = DB::table('amenities')->where('status',1)->orderBy('amenity_name', 'asc')->lists('amenity_name', 'amenity_name');
		 return $aminity;
	 }

     private function getMaxPropertiesForCurrentPlan($landlordId)
     {
        $subscriptionPlanId = DB::table('landlord_subscription')->where('landlord_id', $landlordId)->value('subscription_plan_id');

        $maxNumberOfProperties = DB::table('subscription_plans')->where('id', $subscriptionPlanId)->value('property_max');
        return intval($maxNumberOfProperties);
     }

     private function getCurrentPropertyCount($landlordId)
     {
        $currentPropertyCount = DB::table('landlord_property_management_accounts')->where('landlord_id', $landlordId)
            ->sum('property_count');
        //$currentProp
        return intval($currentPropertyCount);
     }

     private function processUnitDataArrays($unitNumberArray, $bedroomCountArray, $bathroomCountArray, $rentArray, $maxOccupancyArray) {
        $processedUnitArray = array();
        foreach ($unitNumberArray as $key => $unitNumber) {
            $unit = array();
            $unit['unitNumber'] = $unitNumber;
            $unit['bedroomCount'] = $bedroomCountArray[$key];
            $unit['bathroomCount'] = $bathroomCountArray[$key];
            $unit['rent'] = $rentArray[$key];
						$unit['maxOccupancy'] = $maxOccupancyArray[$key];
            $processedUnitArray[$key] = $unit;
        }
        return $processedUnitArray;
     }

	public function postApartmentComplexWithoutUnits(Request $request) { //apartment name, id
		$landlordId = Auth::user('landlord')->id;
		$apartmentBuildingName = $request->Apartment_Complex_Name;

		$apartmentModel = $this->createApartmentBuildingEntry($apartmentBuildingName, $landlordId);

		return redirect()->back()->with('message-success', 'Apartment complex posted successfully!');
	}



     private function processIndividualApartmentUnits($address, $landlordId, $schoolId, $amenityArr, $description, $unitNumberArray, $bedroomCountArray, $bathroomCountArray, $rentArray, $apartmentBuildingId, $apartmentBuildingName, $chargePerBed, $maxOccupancy, $utilitiesIncluded = 0) {
        $addressArray = $this->getAddressArrayFromAddress($address);
        $coordArray = $this->getCoordsFromAddress($address);
        $processedUnitArray = $this->processUnitDataArrays($unitNumberArray, $bedroomCountArray, $bathroomCountArray, $rentArray, $maxOccupancy);
        $processedUnitModels = array();
        foreach ($processedUnitArray as $key => $unit) {

            $unitModelArray = array();
            $unitModelArray['property_name'] = $apartmentBuildingName.' Unit #'.$unit['unitNumber'];
            $unitModelArray['property_type'] = 0;
            $unitModelArray['description'] = $description;
            $unitModelArray['landlord_id'] = $landlordId;
            $unitModelArray['school_id'] = $schoolId;
            $unitModelArray['active'] = 1;
            $unitModelArray['bedroom_no'] = $unit['bedroomCount'];
            $unitModelArray['bathroom_no'] = $unit['bathroomCount'];
            $unitModelArray['administrative_area'] = $addressArray['administrative_area'];
            $unitModelArray['locality'] = $addressArray['locality'];
            $unitModelArray['postal_code'] = $addressArray['postal_code'];
            $unitModelArray['thoroughfare'] = $addressArray['thoroughfare'];
            $unitModelArray['premise'] = $unit['unitNumber'];
            $unitModelArray['latitude'] = $coordArray['lat'];
            $unitModelArray['longitude'] = $coordArray['lng'];
            $unitModelArray['distance_from_school'] = $this->getDistanceFromSchool($address, $schoolId);
            $amenityList = implode(" ,", $amenityArr);
            $unitModelArray['amenities'] = $amenityList;
            $unitModelArray['apartment_building_id'] = $apartmentBuildingId;
						$unitModelArray['max_occupancy'] = $unit['maxOccupancy'];
            $unitModel = PropertyPhysical::create($unitModelArray);
            $unitModel->save();
						array_push($processedUnitModels, $unitModel);
						$schoolName = $this->getSchoolWithId($schoolId)->schoolname;
						$searchField  = $unitModelArray['property_name'].','.$schoolName.','.$description.','.$addressArray['locality'].','.$addressArray['administrative_area'].','.$addressArray['thoroughfare'].','.$this->getLandLordName($landlordId);
						if(!empty($amenityArr)) {
							foreach ($amenityArr as $amenity) {
									$amenityArray[] = $amenity;
									$data[] = array('property_id'=>$unitModel->id,
															'amenity'=>$amenity,
															'created_at'=>date('Y-m-d H:i:s'),
															'updated_at'=>date('Y-m-d H:i:s'));
							}
							$success = PropertyAmenity::insert($data);
						}
						$unitMetaModelArray = array();
						$unitMetaModelArray['property_physical_id'] = $unitModel->id;
						$unitMetaModelArray['rent_price'] = $unit['rent'];
						$unitMetaModelArray['charge_per_bed'] = $chargePerBed;
						$unitMetaModelArray['search_field'] = $searchField;
						$unitMetaModel = PropertyMeta::create($unitMetaModelArray);
        }
        return $processedUnitModels;

     }

     public function postPropertyApartments(Request $request) {
         $loggedLandlord                      =  Auth::user('landlord');
         $requestParameters = $request->all();
         $loggedLandlordId = $loggedLandlord->id;
         $apartmentBuildingName = $request->apartment_building_name;
         $apartmentBuilding = $this->createApartmentBuildingEntry($apartmentBuildingName, $loggedLandlordId);
         $apartmentUnitModels = $this->processIndividualApartmentUnits($request->full_address_autocomplete, $loggedLandlordId, $request->school_id, $request->aminity, $request->description, $request->unit_number,
        $request->bedroom_count, $request->bathroom_count, $request->rent, $apartmentBuilding->id, $apartmentBuilding->building_name, $request->charge_per_bed, $request->max_occupancy);

		return redirect()->back()->with('message-success', 'Apartment units successfully added!');
     }


    public function createApartmentBuildingEntry($apartmentBuildingName, $landlordId) {
        $apartmentBuildingParameterArray = [
            'building_name' => $apartmentBuildingName,
            'landlord_id' => $landlordId
        ];

        $apartmentBuildingModel = ApartmentBuilding::create($apartmentBuildingParameterArray);
        $apartmentBuildingModel->save();
        return $apartmentBuildingModel;
    }

    public function postProperty(Request $request){
        $rules = array(
            'school_id'                 => 'required',
            'bedroom_no'                => 'required|numeric',
            'rent_price'                => 'required|numeric',
            'description'               => 'required',
            'communication_medium'      => 'required',
        );
        $messages = [
                        'school_id.required'                 => 'School Name field is Required',
                        'location.required'                  => 'Location field is Required',
                        'bedroom_no.required'                => 'Bedroom field is Required',
                        'bedroom_no.numeric'                 => 'Please enter a valid number in Bedroom',
                        'rent_price.required'                => 'Rent field is Required',
                        'rent_price.numeric'                 => 'Please enter a valid amount as Rent',
                        'description.required'               => 'Description field is Required',
                        'communication_medium.required'      => 'Communication Medium field is Required',
                    ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            $loggedLandlord                      =  Auth::user('landlord');
            $landLordName                        =  $loggedLandlord->firstname.','.$loggedLandlord->lastname;
            $loggedLandlordId                    =  $loggedLandlord->id;

            $currentPropertyCount = $this->getCurrentPropertyCount($loggedLandlordId);
            $maxPropertyCount = $this->getMaxPropertiesForCurrentPlan($loggedLandlordId);
            $landlordSubscriptionPlanId = DB::table('landlord_subscription')->where('landlord_id', $loggedLandlordId)
                ->value('subscription_plan_id');
            if ($landlordSubscriptionPlanId != 1)
            {
                if ($currentPropertyCount >= $maxPropertyCount)
                {
                    $errorString = 'You currently have '.$currentPropertyCount.' properties listed on TenantU, while your current subscription plan
                    only allows for '.$maxPropertyCount.'. Please upgrade your subscription in order to list more properties.';

                    //return var_dump($errorString);
                    Session::flash('message-error', $errorString);
                    return redirect()->back();
                }
            }

            $insertArray                         =  array();
            $insertArrayMeta                     = array();
            $addressArray = $this->getAddressArrayFromAddress($request->full_address_autocomplete);
            $propertyCoords = $this->getCoordsFromAddress($request->full_address_autocomplete);
            $latitude = $propertyCoords['lat'];
            $longitude = $propertyCoords['lng'];
            $schoolCoordArray = explode(',', $request->schoolCalc);

            $distanceFromSchool = $this->getDistanceFromSchool($request->full_address_autocomplete, $request->school_id);
//            dd($distanceFromSchool, $request->all());
//            $autocompleteAddressString = $request->full_address_autocomplete;
//            $addressArray = explode(', ', $autocompleteAddressString);
////            dd($addressArray);
            $insertArray['property_name']        =  $request->full_address_autocomplete;
	       $insertArray['property_type']	 =  "1";
            $insertArray['landlord_id']          =  $loggedLandlordId;
            $insertArray['school_id']            =  $request->school_id;






//            $insertArray['administrative_area']             =  $request->state;
//            $insertArray['administrative_area']             =  $addressArray[2];
            //$insertArray['locality']             =  $request->city;
//            $insertArray['locality']             =  $addressArray[1];
//            $insertArray['postal_code']             =  $request->postal_code;
//            $insertArray['thoroughfare']             =  $request->street_address;
//            $insertArray['thoroughfare']             =  $addressArray[0];

            $insertArray['thoroughfare'] = $addressArray['thoroughfare'];
            $insertArray['locality'] = $addressArray['locality'];
            $insertArray['administrative_area'] = $addressArray['administrative_area'];
            $insertArray['postal_code'] = $addressArray['postal_code'];

            $insertArray['bedroom_no']           =  $request->bedroom_no;
            $insertArray['bathroom_no']          =  $request->bathroom_no;
            $insertArray['description']          =  $request->description;
            $insertArray['distance_from_school'] =  $distanceFromSchool;
						$insertArray['max_occupancy']        =  $request->max_occupancy;
           // list($month, $day, $year)            =  explode('/', $request->expireon);
            //dd($request);
//            list($latitude, $longitude)          =  explode('~', $request->hdPlace);
           // $insertArray['expireon']             =  $year.'-'.$month.'-'.$day;
            $insertArray['latitude']             =  $latitude;
            $insertArray['longitude']            =  $longitude;



            $school    = School::where('id',$request->school_id)
                                ->select('schoolname')->first();
            $searchField  = $request->full_address_autocomplete.','.$school->schoolname.','.$request->description.','.$addressArray['locality'].','.$addressArray['administrative_area'].','.$addressArray['thoroughfare'].','.$landLordName;
            $insertArrayMeta['search_field']       =  $searchField;
            $insertArray['landlord_prop_mgmt_id'] = NULL;
            $insertArrayMeta['communication_medium'] =  $request->communication_medium;
            $insertArrayMeta['rent_price']           =  $request->rent_price;
						$insertArrayMeta['charge_per_bed']       =  $request->charge_per_bed;


            if(!empty($request->mgmtAccountId))
            {
                $insertArray['landlord_prop_mgmt_id'] = $request->mgmtAccountId;
            }

            if (empty($insertArray['property_name'])) {
                $insertArray['property_name'] = $request->street_address;
            }

            $property = PropertyPhysical::create($insertArray);
            $property->save();

            $propertyId = $property->id;
            $insertArrayMeta['property_physical_id'] = $propertyId;
            $propertyMeta = PropertyMeta::create($insertArrayMeta);

            //mail sending to tenants if the property matches there saved search criteria and the school is mandatory
            // $matches  = $this->getMatchesTenants( $request->school_id, $loggedLandlordId, $request->bedroom_no, $request->rent_price, $request->distance_from_school);

            // if( count($matches) ){
            //    foreach( $matches as $match ){
            //         $landLordName  = $loggedLandlord->firstname.' '.$loggedLandlord->lastname;
            //         $tenantName    = $match->tenant->firstname.' '.$match->tenant->lastname;
            //         $tenantEmail   = $match->tenant->email;
            //         $propertyUrl 	= URL::to('/').'/property/'.$property->slug;
            //         Mail::send('emails.propertyMatchesForTenantSearch', ['landLordName'=>$landLordName,'tenantName'=>$tenantName,'propertyName'=>$property->property_name,'propertyUrl'=>$propertyUrl], function ($message) use ($tenantName,$tenantEmail,$propertyUrl) {
            //         $message->to($tenantEmail, $tenantName)
            //                 ->subject('TenantU - Matching property found.');
            //          });
            //    }
            // }
            if( $property && $request->img_name!="" ){
                $images   = explode(',',$request->img_name);
                //dd($images);
                foreach ($images as $image) {
                    $propertyImage = new propertyImage;
                    $propertyImage->property_id = $property->id;
                    $propertyImage->image_name  = $image;
                    $propertyImage->status = 1;
                    $propertyImage->save();
                }
                /* code for image resize */
                $propertyImages = PropertyImage::where('property_id',$property->id)
												->select('image_name')
												->get();

				$imagePath 			= Config::get('constants.PROPERTY_IMAGE_PATH');

				foreach($propertyImages as $propertyImage)
				{
					$img = Image::make($imagePath.$propertyImage->image_name)->resize(800,485);
					$img->save();
					$imgSquare = Image::make($imagePath.'/square/'.$propertyImage->image_name)->resize(300,300);
					$imgSquare->save();
					$imgThumb = Image::make($imagePath.'/thumb/'.$propertyImage->image_name)->resize(300,300);
					$imgThumb->save();
				}
				/* end of image resize */

            }
            //dd($request->amenities);exit;
            $amenityArray = [];
            if($property && $request->aminity!=""){
                foreach ($request->aminity as $amenity) {
                    $amenityArray[] = $amenity;
                    $data[] = array('property_id'=>$property->id,
                                'amenity'=>$amenity,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'));
                }

                $success = PropertyAmenity::insert($data);
            }
            if(!empty($amenityArray)){
               $amenities = implode(',', $amenityArray);
               //TODO update this to property_physical
               $propertyUpdate  = PropertyPhysical::where('id',$property->id)
                                            ->update(['amenities'=>$amenities]);
            }
            $maxNumberOfProperties = $this->getMaxPropertiesForCurrentPlan($loggedLandlordId);
            if ($request->isFromPropMgmtAccountPage == 'true')
            {
                $mgmtAccountId = $request->mgmtAccountId;
                DB::table('landlord_property_management_accounts')->where('id', $mgmtAccountId)->increment('property_count', 1);
                return redirect()->back()->with('message-success','Property posted successfully!');
            }

						$property->id = Crypt::encrypt($property->id);
            return redirect()->route('property.edit',[$property->id])->with('message-success','You can add more details about the property here!');
        }

    }

    public function getDistanceFromSchool($propertyAddress, $schoolId) {
        $schoolAddress = $this->getSchoolAddressById($schoolId);

        $url = "https://maps.googleapis.com/maps/api/directions/json?origin=".str_replace(' ', '+', $propertyAddress)."&destination=".str_replace(' ', '+', $schoolAddress)."&key=AIzaSyD4EGbQKOUHFDH_Xux4zjh4fVa-DEzw7k4&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_all = json_decode($response);
	if($response_all->status != "OK"){
	return 0;
	}
        // print_r($response);
        $distance = $response_all->routes[0]->legs[0]->distance->text;
        return $distance;

    }


    public function getAddressArrayFromAddress($fullAddress) {
        $json = $this->makeGoogleGeocodeRequest($fullAddress);
        $jsonArray = json_decode($json, true);
        $addressArray = array();

        $addressComponentArray = $jsonArray['results'][0]['address_components'];

        $addressArray['thoroughfare'] = $addressComponentArray[0]['long_name'] . ' ' . $addressComponentArray[1]['long_name'];
        $addressArray['locality'] = $addressComponentArray[2]['long_name'];
        $addressArray['administrative_area'] = $addressComponentArray[4]['long_name'];
        $addressArray['postal_code'] = $addressComponentArray[6]['long_name'];

//        dd($addressArray['thoroughfare']);

        return $addressArray;
    }

    public function getCoordsFromAddress($fullAddress) {
        $json = $this->makeGoogleGeocodeRequest($fullAddress);
        $jsonArray = json_decode($json, true);
        $coords = $jsonArray['results'][0]['geometry']['location'];

        return $coords;
    }

    public function makeGoogleGeocodeRequest($fullAddress) {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, 'https://maps.googleapis.com/maps/api/geocode/json?address=' . rawurlencode($fullAddress)."&key=AIzaSyD4EGbQKOUHFDH_Xux4zjh4fVa-DEzw7k4");

        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);

        $json = curl_exec($curl);

        curl_close ($curl);

        return $json;
    }

    public function upload(){
        //require('UploadHandler.php');
        $upload_handler = new UploadHandler();
        //$reponseArray   = json_decode($upload_handler);
        //print_r($upload_handler->testVar);
        // print_r("sssssssssssssssssssssssss");
    }
//<<<<<<< Updated upstream
    public function getMyContact($sortBy, $sortOrder) {
     $loggedLandlord = Auth::user('landlord');
     $loggedLandlordId     = $loggedLandlord->id;
     $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
     $pageTitle            = "Contacts";
     $originalProfile=HTML::getLandlordProfileImage();
	//this method calling is for the landlords frm msg thread
	$tenants = $this->getLandTenantCorrespondingLandlord($loggedLandlord->id,$sortOrder);
	foreach($tenants as $tenant) {
		$tenant->latestMsgThreadId->message_thread_id = Crypt::encrypt($tenant->latestMsgThreadId->message_thread_id);
	}


        return view('landlords.contacts', [
            'pageTitle'     => 'Conversations',
            'pageHeading'   => 'Conversations',
            'pageMenuName'	=> 'mytenantcontact',
            'tenants'       => $tenants,
            'profileImage'    => $originalProfile,
            'profile'         => $landlordProfile
        ]);

    }
//=======

    //used for landlord to unit messages
    public function getMyContactPortfolio() {

    }

////////////////////START MASS LANDLORD MESSAGES////////////////////////////

    public function newUnitMessage($mgmtAccountId) {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $originalProfile=HTML::getLandlordProfileImage();
        $rawMgmtAccountName = DB::table('landlord_property_management_accounts')->where('id', $mgmtAccountId)->select('account_name')->get()[0];
        $mgmtAccountName = $rawMgmtAccountName->account_name;
        return view('landlords.newunitmessage', [
                'pageTitle'     => 'Conversations',
                'pageHeading'   => 'Conversations',
                'pageMenuName'  => 'mytenantcontact',
                'profileImage'    => $originalProfile,
                'profile'         => $landlordProfile,
                'unitName' => $mgmtAccountName,
                'mgmtAccountId' => $mgmtAccountId
            ]);
    }

    public function sendUnitMessage(Request $request) {
        //dd($request);
        $message = $request->message;
        $mgmtAcctId = $request->mgmtAcctId;
        $landlordId = Auth::user('landlord')->id;
        $tenants = $this->getAllTenantsInMgmtAcct($mgmtAcctId);
        $this->pushUnitMessageToDb($message, $landlordId, $mgmtAcctId);
        return redirect('/');
    }

    //returns array of IDS of all active tenants in a given mgmt acct
    public function getAllTenantsInMgmtAcct($mgmtAccountId) {
        $propIds = DB::table('properties')->where('landlord_prop_mgmt_id', $mgmtAccountId)->select('id')->get();

        //praise edwin for array maps
        $propIdMap = array_map(function($item) {
            return $item->id;
        }, $propIds);
        $tenants = array();
        foreach ($propIdMap as $propId) {
            //$ids = DB::table('landlord_tenants')->where('property_id', $propId)->where('active', 1)->select('tenant_id')->get();
            $ids = DB::table('landlord_tenants')->where('property_id', $propId)->where('active', 1)->pluck('tenant_id');
            array_push($tenants, $ids);
        }

        return $tenants;
    }

    //used for landlord to unit messages. will message by LLC
    public function getMyContactUnit(){
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $originalProfile=HTML::getLandlordProfileImage();

        $mgmtAccounts = DB::table('landlord_property_management_accounts')->where('landlord_id', $loggedLandlordId)->select('id', 'account_name')->get();
        //dd($mgmtAccounts);

        return view('landlords.messageunit', [
            'pageTitle'     => 'Conversations',
            'pageHeading'   => 'Conversations',
            'pageMenuName'  => 'mytenantcontact',
            'profileImage'    => $originalProfile,
            'profile'         => $landlordProfile,
            'mgmtAccounts' => $mgmtAccounts
        ]);

    }

    public function pushUnitMessageToDb($message, $landlordId, $mgmtAcctId) {
        DB::table('unit_message')->insert(
            ['message' => $message, 'landlord_id' => $landlordId, 'mgmt_acct_id' => $mgmtAcctId]
        );
    }

////////////////////END MASS LANDLORD MESSAGES////////////////////////////




    //used for individual landlord to tenant messages
//    public function getMyContact() {
////>>>>>>> Stashed changes
//     $loggedLandlord = Auth::user('landlord');
//     $loggedLandlordId     = $loggedLandlord->id;
//     $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
//     $pageTitle            = "Contacts";
//     $originalProfile=HTML::getLandlordProfileImage();
//	//this method calling is for the landlords frm msg thread
//	$tenants = $this->getLandTenantCorrespondingLandlord($loggedLandlord->id,$sortOrder);
//
//        return view('landlords.contacts', [
//            'pageTitle'     => 'Conversations',
//            'pageHeading'   => 'Conversations',
//            'pageMenuName'	=> 'mytenantcontact',
//            'tenants'       => $tenants,
//            'profileImage'    => $originalProfile,
//            'profile'         => $landlordProfile
//        ]);
//
//    }


    public function getMatchesTenants( $schoolId, $landlordId, $bedRoom, $price, $distance ){
        //DB::connection()->enableQueryLog();
        $matches = Mysearch::where('school_id',$schoolId)
                        ->where('bedrooms','<=',$bedRoom)
                        ->whereRaw( "$distance <= distance" )
                        ->whereRaw( "$price between price_form and price_to" )
                        ->with('tenant')
                        ->select('tenant_id')
                        ->addSelect('landlord_id')
                        ->get();
        return $matches;
    }
    public function getLandTenantCorrespondingLandlord( $landlordId, $sortOrder)
    {
     if($sortOrder == "Default"){
     $tenats = MessageThread::from('message_threads AS MT')
                                 ->select('MT.tenant_id','T.firstname','T.lastname','T.about','T.slug','T.city')
                                 ->leftjoin('tenants AS T', 'MT.tenant_id', '=', 'T.id')
                                 ->where('MT.landlord_id', $landlordId)
                                 ->groupBy('MT.tenant_id')
                                 ->get();
     }
     else{
     $tenats = MessageThread::from('message_threads AS MT')
                                 ->select('MT.tenant_id','T.firstname','T.lastname','T.about','T.slug','T.city')
                                 ->leftjoin('tenants AS T', 'MT.tenant_id', '=', 'T.id')
                                 ->where('MT.landlord_id', $landlordId)
                                 ->groupBy('MT.tenant_id')
				 ->orderBy('T.lastname',$sortOrder)
                                 ->get();
      }
      //code by sooraj for getting the latest msg thread id
      foreach($tenats as $tenant)
      {
		  $tenantId = $tenant->tenant_id;
		  $tenant->latestMsgThreadId = $this->getLatestMessageThreadId($tenantId,$landlordId);
	  }
      //end of the code
      return $tenats;

    }

    //code by sooraj for fetch latest msg thread id
    public function getLatestMessageThreadId($tenantId,$landlordId)
	{
		 $landlords = DB::select('SELECT message_thread_id FROM messages WHERE tenant_id ='.$tenantId.' AND landlord_id='.$landlordId.' ORDER BY created_at DESC LIMIT 1 ');
         return  $landlords[0];
	}


    public function getMyTenants( $landlordId, $id=null )
    {

     $myTenant = LandlordTenant::where('landlord_id',$landlordId)
                               ->with('tenant')
                               ->with('property_physical');

     //dd( $tenants );
      if($id){
            $myTenant->where( 'property_id', $id );
      }
      $tenants =  $myTenant->get();

      return $tenants;

    }
    public function newmessage( $encryptedId,$propertyId=null ){
        $loggedLandlord   = Auth::user('landlord');
				$tenantId = Crypt::decrypt($encryptedId);
				//dd($tenantId)
				//dd($propertyId);

        $name = $this->getTenantNameFromId($tenantId);

        $messageThreads = $this->getMyMessageThreads($tenantId);
				foreach($messageThreads as $thread) {
					$thread->id = Crypt::encrypt($thread->id);
				}
        $properties     = $this->getPropertiesForDropDown( $loggedLandlord->id );
        $threads        = $this->getThreads();

        foreach ($properties as $property)
        {
            $items[$property->id] = $property->property_name;
        }
        foreach ($threads as $thread)
        {
            $threadDropdown[$thread->id] = $thread->thread_name;
        }
        $proId = "";
        if( count( $properties ) == 1 ){
            $proId  = $properties[0]->id;
        }

        if($propertyId=='')
        {
            $propertyCount = $this->getPropertyCount($loggedLandlord->id);
            $landLordPropertyList = $this->getPropertyList($loggedLandlord->id);


            $propertyList= array();
            $propertyKey = array();
            foreach($landLordPropertyList as $key=>$val)
            {

                $propertyList[$val['id']]=$val['property_name'];
                $propertyKey=$val['id'];
            }

            $originalProfile=HTML::getLandlordProfileImage();
						$this->getTenantCurrentPropertyId($tenantId);
            if($this->getTenantCurrentPropertyId($tenantId)){
                $proId = $this->getTenantCurrentPropertyId($tenantId);
            }
						//dd($proId);
            return view('landlords.newmessage', [
                    'pageTitle'             => 'New Message',
                    'pageMenuName'			=> 'newmessage',
                    'pageHeading'           => 'newmessage',
                    'landLordPropertyList'  => $landLordPropertyList,
                    'profile'               => $loggedLandlord,
                    'tenantId'              => $encryptedId,
                    'tenantName'            => $name,
                    'propertyList'          => $propertyList,
                    'propertyKey'           => $propertyKey,
                    'messageThreadsObj'     => $messageThreads,
                    'messageFlag'           => 'compose',
                    'profileImage'          => $originalProfile,
                    'properties'            => $items,
                    'threads'               => $threadDropdown,
                    'messageThreadId'       => "",
                    'propertyId'           => $proId
                ]);
        }
    }
        //this function is used for the  saving the message to message and message thread tables
    public function postMessage(Request $request)
    {

        //dd($request->all());


        $rules = array(
            'property'         => 'required',
            'messageType'      => 'required',     // required and must be unique in the ducks table
            'message_content'  => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        else
        {
						$decryptedTenantId = Crypt::decrypt($request->tenantId);
						//dd($decryptedTenantId);
            //$input['message_thread_id'] = $request->messageType;
            $input['message']           = $request->message_content;
            $input['tenant_id']         = $decryptedTenantId;
            $input['sender']            = $request->sender;
            $input['landlord_id']       = Auth::user('landlord')->id;

            $inputArr['thread_id']      = $request->messageType;
            $inputArr['property_id']    = $request->property;
            $inputArr['landlord_id']    = Auth::user('landlord')->id;
            $inputArr['tenant_id']      = $decryptedTenantId;
            //dd($inputArr);exit;
            $tenants   = Tenant::find( $decryptedTenantId );
            $landlords = Landlord::find( Auth::user('landlord')->id );
            $landLordName = $landlords->firstname.' '.$landlords->lastname;
            $tenantName   = $tenants->firstname.' '.$tenants->lastname;
            $tenantEmail = $tenants->email;
            $messageThread = MessageThread::create($inputArr);
						//dd($MessageThread);
            if(isset($messageThread))
            {
                $message = new Message;
                $message->message_thread_id = $messageThread->id;
                $message->message           = $input['message'];
                $message->landlord_id       = $input['landlord_id'];
                $message->tenant_id         = $input['tenant_id'];
                $message->sender            = $input['sender'];

                $message->save();
                $messageFlag = '1';
                $threads = Thread::find( $request->messageType );
                $subject="";
                if($threads->flag==0){
                    $subject = "TenantU - You have received an enquiry message.";
                }
                else{
                    $subject = "TenantU - You have received an issue message.";
                }
                Mail::send('emails.enquirythreadFromLandlord', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail,$subject) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject($subject);
                });
            }
            //print_r(route('tenants.getMessageList',[$messageThread->id]));exit;
						//dd($messageThread);
						$encryptedMessageThreadId = Crypt::encrypt($messageThread->id);
            return redirect()->route('landlords.getMessageList',[$encryptedMessageThreadId]);
        }


    }
    

    public function getMessageList(Request $request,$encryptedMessageThreadId)
    {
			//dd($request->all());
			$messageThreadId = Crypt::decrypt($encryptedMessageThreadId);
        $loggedLandlord   = Auth::user('landlord');
        $tenants  = MessageThread::where('id',$messageThreadId)
                                    ->select('*')
                                    ->first();
        //dd($request->all());
        $threadFlag            = Thread::where('id',$tenants->thread_id)
                                    ->select('flag')
                                    ->first();
				//dd($tenants);

        $name = $this->getTenantNameFromId($tenants->tenant_id);
				//dd($encryptedMessageThreadId);

        $threads  = $this->getMessageThreadName( $tenants->thread_id );
				//dd($tenants);
        $msgPropertyName = $this->getMessageThreadPropertyName($tenants->property_id);
        $formattedMsgThreadCreatedDate 	= $this->getFormattedMsgThreadCreatedDate($tenants->created_at);
        //dd($threads);

        $allMessages = $this->getAllMessagesWithThreadLandlord($messageThreadId,$loggedLandlord->id);
				//dd($allMessages);
        $originalProfile=HTML::getLandlordProfileImage();




        $landlordProfile      = $this->getLandlordProfile($loggedLandlord->id);

        // $landlordName = $this->getLandLordName($allMessages[0]['landlord_id']);

        // $landlordCity = $this->getLandLordCity($allMessages[0]['landlord_id']);

        $messageThreads = $this->getMyMessageThreads($tenants->tenant_id);
				foreach($messageThreads as $thread) {
					$thread->id = Crypt::encrypt($thread->id);
				}
        //dd( $messageThreads );
				//dd($msgPropertyName);
        $readFlag       = $this->getChangeReadFlag( $messageThreadId );
				//dd($msgPropertyName);
				//dd($allMessages);
				$unencryptedTenantId = $allMessages[0]['tenant_id'];
				//dd($unencryptedTenantId);

				$tenantId = Crypt::encrypt($unencryptedTenantId);
        if(!empty($allMessages))
        {
            return view('landlords.newmessage', [
                    'pageTitle'             => 'Messages',
                    'allMessages'           => $allMessages,
                    'tenantName'            => $name,
                    'landlordId'            => $loggedLandlord->id,
                    'pageHeading'           => 'messagelist',
                    'tenantId'              => $tenantId,
                    'messageThreadId'       => $encryptedMessageThreadId,
                    'messageThreadsObj'     => $messageThreads,
                    'pageMenuName'          => 'messagelist',
                    'messageFlag'           => 'list',
                    'profileImage'          => $originalProfile,
                    'profile'               => $landlordProfile,
                    'threads'               => $threads,
                    'threadStatus'          => $tenants->status,
                    'threadTypeFlag'        => $threadFlag->flag,
                    'msgThreadCreatedDate'  => $formattedMsgThreadCreatedDate,
                    'msgPropertyName'		=> $msgPropertyName->property_name
                ]);
        }
    }

    public function composeMessageList(Request $request,$encryptedMessageThreadId)
    {
        //dd($request->all());
				$messageThreadId = Crypt::decrypt($encryptedMessageThreadId);
        $loggedLandlord   = Auth::user('landlord');
        $tenantId         = Crypt::decrypt($request->tenantId);
        $tenants   = Tenant::find( $tenantId );
        $tenantEmail = $tenants->email;
        $tenantName   = $tenants->firstname.' '.$tenants->lastname;
        $landLordName = $loggedLandlord->firstname.' '.$loggedLandlord->lastname;
        if($request->buttonValue=='Processing'){
            $messageThreads = MessageThread::find($messageThreadId);
            $messageThreads->status = 1;
            $messageThreads->save();
            $message = "On Going";
            Mail::send('emails.enquirythreadCompleted', ['landLordName'=>$landLordName,'tenantName'=>$tenantName,'message'=>$message], function ($message) use ($tenantName,$tenantEmail) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject('TenantU - Enquiry response-processing.');
            });
        }
        else if($request->buttonValue=='Completed'){
            $messageThreads = MessageThread::find($messageThreadId);
            $Actialduration = $messageThreads->duration;
            $createdDate    = $messageThreads->created_at;
            $endDate        = time();
            $yourDate       = strtotime($createdDate);
            $datediff       = $endDate - $yourDate;
            $duration       = floor($datediff/(60*60*24));
            $data['rating_id']     =  2;
            $data['tenant_id']     = $messageThreads->tenant_id;
            $data['property_id']   = $messageThreads->property_id;
            if( $duration <= $Actialduration ){
                $data['rate_value']   = 5;
                //here comes the logic for recomended property
                //land lord id comes here from property id



            }
            else{

               $data['rate_value']    = 2;
            }
            //for showing 'would recommend' sort option in search
            $updateRecomended = $this->updateLandlordRating($loggedLandlord->id);

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $success = RatingUser::insert($data);
            $messageThreads->status = 2;
            $messageThreads->save();

            $message = "Completed";
            $messageRating = "Tenantu.com has given a rating(Landlord) of ".$data['rate_value']." out of 5 for solving this issue.";
            Mail::send('emails.enquirythreadCompleted', ['landLordName'=>$landLordName,'tenantName'=>$tenantName,'message'=>$message], function ($message) use ($tenantName,$tenantEmail) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject('TenantU - Enquiry response-Completed.');
            });
            $avgRateVal = $this->getAverageRating($messageThreads->property_id,2);
            $ratingproperties = DB::update('UPDATE properties SET rating_2='.$avgRateVal.' WHERE id='.$messageThreads->property_id);
            $totalAvgRateVal = $this->getAverageRating( $messageThreads->property_id );
            $ratingproperties = DB::update('UPDATE properties SET rating_avg='.$totalAvgRateVal.' WHERE id='.$messageThreads->property_id);

            //system generating rating value to landlord
			DB::table('messages')->insert(
					['message_thread_id' => $messageThreadId, 'message' =>$messageRating,'landlord_id'=>$loggedLandlord->id,'tenant_id'=>$tenantId,'sender'=> 'T','created_at'=>Carbon::now()]
			);
        }
        else{
            $message          = $request->message;
	     //TESTINGSHIT
	 Mail::send('emails.enquirythreadCompleted', ['landLordName'=>$landLordName,'tenantName'=>$tenantName,'message'=>$message], function ($message) use ($tenantName,$tenantEmail) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject('TenantU - Enquiry response-Completed.');
            });

        }


        DB::table('messages')->insert(
            ['message_thread_id' => $messageThreadId, 'message' =>$message,'landlord_id'=>$loggedLandlord->id,'tenant_id'=>$tenantId,'sender'=> 'L','created_at'=>Carbon::now()]
        );


        return redirect()->route('landlords.getMessageList',[$encryptedMessageThreadId]);
    }


    private function getAverageRating($propertyId,$rateId=null)
    {
        if($rateId!='')
        {
            $whereCondition = ' WHERE property_id='.$propertyId.' AND rating_id='.$rateId;
        }
        else
        {
            $whereCondition = ' WHERE property_id='.$propertyId;
        }

        $avgRate = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user'.$whereCondition );
        $avgRateValue = $avgRate[0]->rate_value;
        $avgRateValueRounded = ceil($avgRateValue);

        return $avgRateValueRounded;
    }
    public function getChangeReadFlag($messageThreadId)
    {

        $message = Message::where('message_thread_id', $messageThreadId)
                            ->where('sender','T')
                            ->update(['read_flag' => 1]);
    }
    private function getMyMessageThreads($tenantId)
    {
        $loggedLandlord   = Auth::user('landlord');
        $messageThreads = MessageThread::from('message_threads AS MT')
                                 ->select('MT.id','MT.property_id','MT.created_at','MT.tenant_id', 'T.thread_name','T.image','P.property_name')
                                 ->join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 ->join('property_physical AS P','MT.property_id','=','P.id')
                                 ->where('MT.landlord_id', $loggedLandlord->id)
                                 ->where('MT.tenant_id',$tenantId)
                                 ->orderBy('MT.created_at')
                                 ->get();

        return $messageThreads;
    }
    public function addTenant(){
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
       // THIS SNIPPET IS TRYING TO FILTER OUT TENANTS WHO HAVE MESSAGED WITH THE LANDLORD BEFORE
        /*$tenants          = MessageThread::where('landlord_id',$loggedLandlord->id)
                                          ->with(['tenant'=>function($q){
													$q->where( 'is_active',1 );
												 }])
                                          ->groupBy('tenant_id')
                                          ->get();


	SNIPPET ENDS
	*/


	//THIS SECTION CALLS THE FUNCTION FOUND IN Controller.php TO GET THE TENANTS THAT ARE AT THE SAME SCHOOL AS THE LANDLORD
	$tenants = $this->getTenantsForDropDown( $loggedLandlord->school_id );
        //END SECTION
        $tenantDropDown = array();
        $propertyDropdown =array();
        if(count($tenants)){
			foreach ($tenants as $tenant)
			{
				//if(!empty($tenant->tenant)){
					//$tenantDropDown[$tenant->tenant->id] = $tenant->tenant->firstname.' '.$tenant->tenant->lastname;
				$tenantDropDown[$tenant->id] = $tenant->firstname.' '.$tenant->lastname;
				//}
			}
		}
        $properties = $this->getPaidPropertiesForDropDown( $loggedLandlord->id );
        foreach ($properties as $property)
        {
            $propertyDropdown[$property->id] = $property->property_name;
        }
       $cycleDropdown = ['3months'=>"3 Month",
                         '6months'=>"6 Month",
                         '1year' =>"1 Year",
                         '2year' =>"2 Year",
                         '3year' =>"3 Year"];
       $landlordPropertyCount = $this->getPaidPropertyCount($loggedLandlord->id);
       $landlordPropertyList = $this->getPaidPropertiesForDropDown($loggedLandlord->id);
            return view('landlords.addtenant', [
                'pageTitle'       => 'Add Tenant',
                'pageMenuName'	  => 'mytenants',
                'pageHeading'     => 'Add Tenant',
                'profile'         => $landlordProfile,
                'profileImage'    => $originalProfile,
                'tenantDropDown'  => $tenantDropDown,
                'propertyDropdown'=> $propertyDropdown,
                'cycleDropdown'   => $cycleDropdown,
		'landlordPropertyCount' => $landlordPropertyCount,
		'landlordPropertyList' => $landlordPropertyList
            ]);
    }

    public function postTenant( Request $request ){

	$userTypeValue = $request->siteUser;

                if($userTypeValue=='addTenantOnSite'){

        $rules = array(
            'email'                 => 'required|edu',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'email.required'                 => 'Tenant Email field is Required',
			'email.edu'			 => 'Tenant Email must be a .edu address',
                        'poperty_id.required'                => 'Property field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Lease Length field is Required'
                    ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
	    $tenantId = $this->getTenantIdByEmail($request->email);

            if($tenantId == null){
	      return redirect()->back()->with('message-error','A tenant is not registered on TenantU with that email address')->withInput();

	    }
            //dd($request->all());
            //DB::connection()->enableQueryLog();
            $loopCount = "";
            if(($request->cycle)!=""){
                switch($request->cycle){
                       case "3months":
                         $loopCount = 3;
                       break;
                       case "6months":
                         $loopCount = 6;
                       break;
                       case "1year":
                         $loopCount = 12;
                       break;
                       case "2year":
                         $loopCount = 24;
                       break;
                       case "3year":
                          $loopCount =36;
                       break;
                }
            }

            // Wilson made. Factor is the amount of payments to be made after taking into account landlord collection frequencies
            switch($loopCount){
                case 3:
                    $factor = 1;
                    break;
                case 6:
                    $factor = 1;
                    break;
                default:
                    $factor = $this->getFactorByLandlordRentCollectionFreq(Auth::user('landlord')->id);
                    break;
            }

            $propertyPrice = $this->getPropertyPrice($request->poperty_id);
            $propertyCount = $this->getPropertyCurrentTenantCount($request->poperty_id);
            $propertyCount = $propertyCount + 1;
            //if(per_bed flag is no then this
	    if($this->getPropertyPerBed($request->poperty_id)[0]["charge_per_bed"] == 0){
	    $actualPropertyPrice = $propertyPrice / $propertyCount;
	    }
	    //if(per_bed flag is yes then just set $actual property price to the $property price
            else{
	    $actualPropertyPrice = $propertyPrice;
	    }
	    $insertArray                             =  [];
            $insertArray['landlord_id']              =  Auth::user('landlord')->id;
	   //I WAS HERE $request->email;
            $insertArray['tenant_id']                =  $tenantId;
            $insertArray['property_id']              =  $request->poperty_id;
            $insertArray['month_rent_initial']       =  $actualPropertyPrice;
            $insertArray['due_on']                   =  $this->getLandlordRentDueDay(Auth::user('landlord')->id);
            $insertArray['cycle']                    =  $request->cycle;
            list($monthStart, $dayStart, $yearStart) =  explode('/', $request->agriment_start_date);
            $startYear                               =  $yearStart.'-'.$monthStart.'-'.$dayStart;
            if(strtotime($startYear)>time()){
                  $insertArray['active'] = 2;
            }
            else{
                  $insertArray['active'] = 1;
            }
            $insertArray['agreement_start_date']     =  $startYear;

            $totalDurationStart  = date("Y-m-d", strtotime("+0 month", strtotime($startYear)));

            $totalDurationEnd  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));

            $insertArray['agreement_end_date']     =  $totalDurationEnd;
            $leaseLength = array('lease_start'=>$totalDurationStart, 'lease_end'=>$totalDurationEnd);

            // Wilson made
            $activeTenantExist                      = $this->activeLandlordTenantCount($tenantId, $leaseLength);

            /*$myTenantExist                           =  LandlordTenant::where('tenant_id',$request->tenant_id)
                                                        //->where('agreement_start_date','<=',$totalDurationEnd)
                                                        ->where(function($q) use ( $totalDurationStart, $totalDurationEnd){
                                                          $q->whereBetween('agreement_start_date', array($totalDurationStart, $totalDurationEnd));
                                                          $q->orWhereBetween('agreement_end_date', array($totalDurationStart, $totalDurationEnd));
                                                        })

                                                        ->where('landlord_id',Auth::user('landlord')->id)
                                                        ->count();
            */
            //$queries = DB::getQueryLog();
            //dd($queries);
            //dd($myTenantExist);

            if( $activeTenantExist == 0 ){
                // Create the new lease relationship
                $property = LandlordTenant::create($insertArray);
                // If cycle was selected...
                if($loopCount!=""){
                    $billDate = $yearStart.'-'.$monthStart.'-'.$insertArray['due_on'];
                    //echo $billDate;exit;
                    for($i=1;$i<=$loopCount/$factor;$i++){
                        $uId = uniqid();
                        $nextpaymonth = $i * $factor;
                        $nextBillDate = date("Y-m-d", strtotime("+".$nextpaymonth." month", strtotime($billDate)));

                        $data[] = array('landlord_tenant_id'=>$property->id,
                                        'bill_on'=>$nextBillDate,
                                        'amount'=>(($actualPropertyPrice) * $factor),
                                        'unique_id'=>$uId,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'));
                    }
                    $success = landlordTenantMonthlyrent::insert($data);
                }
		//if charge per bed is no dont update other tenants
		if($this->getPropertyPerBed($request->poperty_id)[0]["charge_per_bed"] == 0){
		$updateTenants = $this->updateOtherTenantsRent($request->poperty_id, $loopCount, $factor, $insertArray['due_on'],$monthStart,$yearStart);
                }
		return redirect()->route('landlords.viewtenant',$property->id)->with('message-success','Tenant added successfully!');
            }
        else{
           return redirect()->route('landlords.addtenant')->with('message-error','You cannot add this tenant for this period!');

        }
        }


		}
         if($userTypeValue = "inviteTenantByEmail"){

	$rules1 = array(
            'name'                  => 'required',
            'email'                 => 'required|edu',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
	 $messages1 = [
                        'name.required'                         => 'Tenant name is Required',
                        'email.required'                 => 'Tenant email is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make( $request->all(), $rules1, $messages1 );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
	$loopCount = "";
            if(($request->cycle)!=""){
                switch($request->cycle){
                       case "3months":
                         $loopCount = 3;
                       break;
                       case "6months":
                         $loopCount = 6;
                       break;
                       case "1year":
                         $loopCount = 12;
                       break;
                       case "2year":
                         $loopCount = 24;
                       break;
                       case "3year":
                          $loopCount =36;
                       break;
                }
            }

            // Wilson made. Factor is the amount of payments to be made after taking into account landlord collection frequencies
            switch($loopCount){
                case 3:
                    $factor = 1;
                    break;
                case 6:
                    $factor = 1;
                    break;
                default:
                    $factor = $this->getFactorByLandlordRentCollectionFreq(Auth::user('landlord')->id);
                    break;
            }

	 $loggedLandlord   = Auth::user('landlord');
         $loggedLandlordId = $loggedLandlord->id;
         $landlordProfile =  $this->getLandlordProfile($loggedLandlordId);
         $landlordFirstName = $landlordProfile->firstname;
         $landlordLastName = $landlordProfile->lastname;
         $landlordFullName = $landlordProfile->getFullNameAttribute();
         $landlordSchoolId = $landlordProfile->school_id;
         $landlordSchoolName =  $this->getSchoolWithId($landlordSchoolId)->schoolname;
         $landlordEmail = $landlordProfile->email;
         list($monthStart, $dayStart, $yearStart) =  explode('/', $request->agriment_start_date);
         $startYear                               =  $yearStart.'-'.$monthStart.'-'.$dayStart;
         $totalDurationStart  = date("Y-m-d", strtotime("+0 month", strtotime($startYear)));
         $totalDurationEnd  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
         $agreementEndDate = $totalDurationEnd;
         $propertyPrice = $this->getPropertyPrice($request->poperty_id);
         $propertyCount = $this->getPropertyCurrentTenantCount($request->poperty_id);
         $propertyCount = $propertyCount + 1;
	 //if per bed flag is no then this
	 if($this->getPropertyPerBed($request->poperty_id)[0]["charge_per_bed"] == 0){
         $actualPropertyPrice = $propertyPrice / $propertyCount;
	 }
	 //if per bed flag is yes then set it to $propertyPrice
         else{
	 $actualPropertyPrice = $propertyPrice;
	 }
	 $tenantMonthRent = "+".$actualPropertyPrice;
	 $passLoopCount = $loopCount;
         $passFactor = $factor;
         $tenantName = $request->name;
         $propertyId = "+".$request->poperty_id;
         $tenantRentDue = "+".$this->getLandlordRentDueDay($loggedLandlordId);
         $tenantAgreementStart = "+".$startYear;
         $tenantCycle = "+".$request->cycle;
        //linkPart left for potential automatic add tenant as landlord functionality in future, currently not implementing this functionality
         $linkPart = $loggedLandlordId.$propertyId.$tenantMonthRent.$tenantRentDue.$tenantCycle."+".$monthStart."+".$dayStart."+".$yearStart.$tenantAgreementStart."+".$totalDurationEnd."+".$passLoopCount."+".$passFactor;
         $fullLink = "http://tenantu.com/signup#" . Crypt::encrypt($linkPart);
	 $contactAdminEmail = $request->email;
	 $inviteEmail = [];
	 $inviteEmail["landlord_id"] = $loggedLandlordId;
	 $inviteEmail["tenant_invite_email"] = $contactAdminEmail;
	 $addingEmailInviteEntry = landlordTenantEmailInvite::create($inviteEmail);



                  $reason = $tenantName . "! " . "Your landlord, " . $landlordFullName . ", has invited you to join TenantU!";


                 Mail::send('emails.invite-tenants',['name'=>$landlordFullName,'email'=>$landlordEmail,'school'=>$landlordSchoolName,'reason'=>$reason, 'link'=>$fullLink],
                                                                                function($message) use ($contactAdminEmail) {

                                        $message->to($contactAdminEmail)
                                                        ->subject("Your landlord has invited you to join TenantU!");
                 });
                 return redirect()->route('landlords.addtenant')->with('message-success','Your message has been successfully sent. Once your tenant has signed up they will automatically be added as a tenant of your property!');
        }




		}

    }

    public function viewTenant( $id ){

        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();

        //dd( $tenantDropDown );
        $landlordTenant     =  LandlordTenant::where('landlord_id',$loggedLandlord->id)
                                            ->where('id',$id)
                                            ->with('landlord_tenant_monthlyrents')
                                            ->with('landlord_tenant_docs')
                                            ->with('landlord_tenant_otherpayments')
                                            ->with('landlord_tenant_otherpayment_details')
                                            ->with('tenant')
                                            ->with('property_physical')
                                            ->first();
        //dd( $landlordTenant );
        if(count($landlordTenant) > 0){

            return view('landlords.viewtenant', [
                'pageTitle'       => 'Add Tenant',
                'pageHeading'     => 'Add Tenant',
                'pageMenuName'    => 'viewtenant',
                'profile'         => $landlordProfile,
                'profileImage'    => $originalProfile,
                'landlordTenant'  => $landlordTenant
            ]);
        }
        else{
            return redirect('/404');
        }
    }

    public function editTenant($id){
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
        $tenants          = MessageThread::where('landlord_id',$loggedLandlord->id)
                                          ->with('tenant')
                                          ->groupBy('tenant_id')
                                          ->get();
        //dd( $tenants );
        for($i=1;$i<31;$i++){
            $dueOn[$i] = $i.' Every month';
        }
        $tenantDropDown = array();
        foreach ($tenants as $tenant)
        {
            $tenantDropDown[$tenant->tenant->id] = $tenant->tenant->firstname.' '.$tenant->tenant->lastname;
        }
        $properties = $this->getPaidPropertiesForDropDown( $loggedLandlord->id );
        foreach ($properties as $property)
        {
            $propertyDropdown[$property->id] = $property->property_name;
        }
       $cycleDropdown = ['3months'=>"3 Month",
                         '6months'=>"6 Month",
                         '1year' =>"1 Year",
                         '2year' =>"2 Year",
                         '3year' =>"3 Year"];
        //dd( $tenantDropDown );
        $landlordTenant     =  LandlordTenant::where('landlord_id',$loggedLandlord->id)
                                            ->where('id',$id)
                                            ->with('landlord_tenant_monthlyrents')
                                            ->with('landlord_tenant_docs')
                                            ->with('landlord_tenant_otherpayments')
                                            ->with('tenant')
                                            ->with('property_physical')
                                            ->with('landlord_tenant_otherpayment_details')
                                            ->first();
        //dd( $landlordTenant );
            return view('landlords.edittenant', [
                'pageTitle'       => 'Edit Tenant',
                'pageHeading'     => 'Edit Tenant',
                'pageMenuName'    => 'edittenant',
                'profile'         => $landlordProfile,
                'profileImage'    => $originalProfile,
                'tenantDropDown'  => $tenantDropDown,
                'dueOn'           => $dueOn,
                'propertyDropdown'=> $propertyDropdown,
                'cycleDropdown'   => $cycleDropdown,
                'landlordTenant'  => $landlordTenant
            ]);
    }

    public function changeRent( Request $request ){
        //print_r($request->all());
        $id                                   = $request->pkId;
        $amount                               = $request->amount;
        $landlordTenantRent                   = landlordTenantMonthlyrent::find( $id );
        if($landlordTenantRent->update(['amount' => $amount])){
           return "1";
        }
        else{
           return "0";
        }
    }
    public function updateDocData( Request $request ){
        //print_r($request->all());
        $id                                   = $request->pkId;
        $amount                               = $request->amount;
        $title                                = $request->title;
        $flag                                 = $request->flag;
        $description                          = $request->description;
        $landlordTenantDoc                    = landlordTenantDoc::find( $id );
        if($flag=='update'){
            if($landlordTenantDoc->update(['doc_title' => $title,'doc_description'=>$description])){
               return "1";
            }
            else{
               return "0";
            }
        }
        else{
            $docName  = $landlordTenantDoc->file_name;
            $destinationPath = Config::get('constants.TENANT_DOC_PATH');
            unlink($destinationPath.$docName);
            if($landlordTenantDoc->delete()){
                return "1";
            }
            else{
                return "0";
            }
        }
    }
    public function updateItineraryData( Request $request ){
        //print_r($request->all());
        $id                                   = $request->pkId;
        $amount                               = $request->amount;
        $title                                = $request->title;
        $landlordTenantOtherpaymentDetail     = landlordTenantOtherpaymentDetail::find( $id );
        if($landlordTenantOtherpaymentDetail->update(['amount' => $amount,'title'=>$title])){
           return "1";
        }
        else{
           return "0";
        }
    }
    public function postEditTenant( Request $request, $id ){
        //dd($request->all());
           $rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            //dd($request->all());
            $loopCount = "";
            if(($request->cycle)!=""){
                switch($request->cycle){
                       case "3months":
                         $loopCount = 3;
                       break;
                       case "6months":
                         $loopCount = 6;
                       break;
                       case "1year":
                         $loopCount = 12;
                       break;
                       case "2year":
                         $loopCount = 24;
                       break;
                       case "3year":
                          $loopCount =36;
                       break;
                }
            }
            $landlordTenantRent                       = LandlordTenant::find( $id );
            $landlordTenantRent->landlord_id          =  Auth::user('landlord')->id;
            $landlordTenantRent->tenant_id            =  $request->tenant_id;
            $landlordTenantRent->property_id          =  $request->poperty_id;
            $landlordTenantRent->month_rent_initial   =  $request->month_rent;
            $landlordTenantRent->due_on               =  $request->due_on;
            $landlordTenantRent->cycle                =  $request->cycle;
            list($monthStart, $dayStart, $yearStart)  =  explode('/', $request->agriment_start_date);
            $startYear                                =  $yearStart.'-'.$monthStart.'-'.$dayStart;
            $landlordTenantRent->agreement_start_date =  $startYear;

            //$totalDuration  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
            $totalDurationStart  = date("Y-m-d", strtotime("+0 month", strtotime($startYear)));

            $totalDurationEnd  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
            $landlordTenantRent->agreement_end_date     =  $totalDurationEnd;
            //DB::connection()->enableQueryLog();
            $myTenantExist                           =  LandlordTenant::where('tenant_id',$request->tenant_id)
                                                        //->where('agreement_start_date','<=',$totalDuration)
                                                        ->where(function($q) use ( $totalDurationStart, $totalDurationEnd){
                                                          $q->whereBetween('agreement_start_date', array($totalDurationStart, $totalDurationEnd));
                                                          $q->orWhereBetween('agreement_end_date', array($totalDurationStart, $totalDurationEnd));
                                                        })
                                                        ->where('landlord_id',Auth::user('landlord')->id)
                                                        ->where('id','!=',$id)
                                                        ->count();
            //$queries = DB::getQueryLog();
            //dd($queries);
            //dd($myTenantExist);
            if($myTenantExist==0){
            $property = $landlordTenantRent->update();
            if($loopCount!=""){
                $billDate = $yearStart.'-'.$monthStart.'-'.$request->due_on;
                $landlordTenantMonthlyRent = landlordTenantMonthlyrent::where('landlord_tenant_id', $id );
                //landlordTenantOtherpaymentDetail code starts here
                $this->updateLandlordTenantOtherPaymentdetails( $id, $startYear, $request->cycle, $request->due_on );
                $landlordTenantMonthlyRent->delete();
                for($i=1;$i<=$loopCount;$i++){
                    $uId  = uniqid();
                    $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));

                    $data[] = array('landlord_tenant_id'=>$id,
                                    'bill_on'=>$nextBillDate,
                                    'amount'=>$request->month_rent,
                                    'unique_id'=>$uId,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s'));
                }
                $success = landlordTenantMonthlyrent::insert($data);
            }

            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Tenant edited successfully!');
        }
        else{
            return redirect()->route('landlords.edittenant',$id)->with('message-error','You cannot add this tenant for this period!');
        }
        }
    }
    public function updateLandlordTenantOtherPaymentdetails( $id, $startYear, $cycle, $due ){
        $landlordTenantRent                  = landlordTenantOtherpayment::where( 'landlord_tenant_id',$id )
                                                                          ->get();
        if(count($landlordTenantRent)){

            landlordTenantOtherpayment::where('landlord_tenant_id', $id)
                                        ->update(['agreement_start_date' => $startYear,
                                                  'due_on'               => $due ,
                                                  'cycle'                => $cycle
                                            ]);
            $landlordTenantOtherpayments                  = landlordTenantOtherpayment::where( 'landlord_tenant_id',$id )
                                                                              ->get();
             //dd($landlordTenantOtherpayments);
             foreach( $landlordTenantOtherpayments as $landlordTenantOtherpayment){
                    $deleteSuccess = landlordTenantOtherpaymentDetail::where('landlord_tenant_otherpayment_id', $landlordTenantOtherpayment->id)->delete();

             //}


                /*$landlordTenantOtherpayments = landlordTenantOtherpayment::where('landlord_tenant_id', $id )->get();*/
                //foreach($landlordTenantOtherpayment as $landlordTenantOtherpayment){

                /*$data[] = array('landlord_tenant_id'=>$id,
                                'agreement_start_date'=>$landlordTenantRent->agreement_start_date,
                                'cycle'=>$landlordTenantRent->cycle,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'));*/
                /*$insertArray['landlord_tenant_id']   = $id;
                $insertArray['agreement_start_date'] = $landlordTenantOtherpayment->agreement_start_date;
                $insertArray['cycle']                = $landlordTenantOtherpayment->cycle;
                $insertArray['title']                = $landlordTenantOtherpayment->title ;
                $insertArray['due_on']               = $landlordTenantOtherpayment->due_on;
                $insertArray['initial_amount']       = $landlordTenantOtherpayment->initial_amount;
                $success                             = landlordTenantOtherpayment::create($insertArray);*/

                //$success = landlordTenantOtherpayment::insert( $data );
                //if( $deleteSuccess ){
                    //echo "1";exit;
                    $loopCount = "";
                    if(($landlordTenantOtherpayment->cycle)!=""){
                        switch($landlordTenantOtherpayment->cycle){
                               case "3months":
                                 $loopCount = 3;
                               break;
                               case "6months":
                                 $loopCount = 6;
                               break;
                               case "1year":
                                 $loopCount = 12;
                               break;
                               case "2year":
                                 $loopCount = 24;
                               break;
                               case "3year":
                                  $loopCount =36;
                               break;
                        }
                    }
                    if($loopCount!=""){
                        list( $yearStart, $monthStart, $dayStart ) =  explode('-', $landlordTenantOtherpayment->agreement_start_date);
                        $billDate = $yearStart.'-'.$monthStart.'-'.$landlordTenantOtherpayment->due_on;
                        //echo $billDate;exit;
                        for($i=1;$i<=$loopCount;$i++){

                            $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));

                            $data[] = array('landlord_tenant_otherpayment_id'=>$landlordTenantOtherpayment->id,
                                            'bill_on'=>$nextBillDate,
                                            'amount'=>$landlordTenantOtherpayment->initial_amount,
                                            'title'=>$landlordTenantOtherpayment->title,
                                            'created_at'=>date('Y-m-d H:i:s'),
                                            'updated_at'=>date('Y-m-d H:i:s'));
                        }

                    }
                //}
            }
            $success = landlordTenantOtherpaymentDetail::insert($data);
            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Data successfully posted!');
        }
    }
    public function postEditTenantDoc( Request $request, $id ){
        //dd($request->all());
           /*$rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {*/
            $files = Input::file('file_name');

            // Making counting of uploaded images
            $fileCount = count($files);
            // start count how many uploaded
            foreach( $files as $key=>$file ) {
              $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
              $validator = Validator::make(array('file'=> $file), $rules);
              if($validator->passes()){
                $destinationPath = Config::get('constants.TENANT_DOC_PATH');
                $extension = $file->getClientOriginalExtension();
                $filename  = trim(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename.uniqid(), "-") . '.' . $extension;
                $upload_success = $file->move($destinationPath, $filename);
                $data[] = array('landlord_tenant_id'=>$id,
                                        'doc_title'=>$request->doc_title[$key],
                                        'file_name'=>$filename,
                                        'doc_description'=>$request->doc_description[$key],
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'));
                $success = landlordTenantDoc::insert($data);
              }
            }

            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Documents successfully posted!');
        //}
    }
     public function postEditTenantItinerary( Request $request, $id ){
        //dd($request->all());
           /*$rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails())
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {*/
            $titles  = $request->title;
            $amounts = $request->amount;
            $landlordTenantRent                  = LandlordTenant::find( $id );
            foreach($titles as $key => $title){

            /*$data[] = array('landlord_tenant_id'=>$id,
                            'agreement_start_date'=>$landlordTenantRent->agreement_start_date,
                            'cycle'=>$landlordTenantRent->cycle,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'));*/
            $insertArray['landlord_tenant_id']   = $id;
            $insertArray['agreement_start_date'] = $landlordTenantRent->agreement_start_date;
            $insertArray['cycle']                = $landlordTenantRent->cycle;
            $insertArray['title']                = $titles[$key] ;
            $insertArray['due_on']               = $landlordTenantRent->due_on;
            $insertArray['initial_amount']       = $amounts[$key];
            $success                             = landlordTenantOtherpayment::create($insertArray);

            //$success = landlordTenantOtherpayment::insert( $data );
            if( $success ){
                $loopCount = "";
                if(($landlordTenantRent->cycle)!=""){
                    switch($landlordTenantRent->cycle){
                           case "3months":
                             $loopCount = 3;
                           break;
                           case "6months":
                             $loopCount = 6;
                           break;
                           case "1year":
                             $loopCount = 12;
                           break;
                           case "2year":
                             $loopCount = 24;
                           break;
                           case "3year":
                              $loopCount =36;
                           break;
                    }
                }
                if($loopCount!=""){
                    list( $yearStart, $monthStart, $dayStart ) =  explode('-', $landlordTenantRent->agreement_start_date);
                    $billDate = $yearStart.'-'.$monthStart.'-'.$landlordTenantRent->due_on;
                    //echo $billDate;exit;
                    for($i=1;$i<=$loopCount;$i++){
                        $uId  = uniqid();
                        $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));

                        $data[] = array('landlord_tenant_otherpayment_id'=>$success->id,
                                        'bill_on'=>$nextBillDate,
                                        'amount'=>$amounts[$key],
                                        'unique_id'=>$uId,
                                        'title'=>$titles[$key],
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'));
                    }

                }
            }
        }
        $success = landlordTenantOtherpaymentDetail::insert($data);
            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Data successfully posted!');
        //}
    }


    public function getTenant( $id=null ){
         $loggedLandlord = Auth::user('landlord');
         $loggedLandlordId     = $loggedLandlord->id;
         $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
         $pageTitle            = "Contacts";
         $originalProfile      = HTML::getLandlordProfileImage();
            //this method calling is for the landlords frm msg thread
         if($id!=""){
            $tenants = $this->getMyTenants( $loggedLandlord->id, $id );
         }
         else{
            $tenants = $this->getMyTenants($loggedLandlord->id);
         }
         $yearMonthTime =  strtotime(date("Y-m"));
         $currMonthYear =  str_replace('-','',date("Y-m", strtotime("+0 month", $yearMonthTime)));

            return view('landlords.mytenants', [
                'pageTitle'     => 'Conversations',
                'pageHeading'   => 'Conversations',
                'pageMenuName'	=> 'mytenants',
                'tenants'       => $tenants,
                'profileImage'  => $originalProfile,
                'profile'       => $landlordProfile,
                'id'            => $id,
                'currMonthYear' => $currMonthYear
            ]);
    }

    public function getTenantProfileFromLandlord($slug)
    {
		$slugCheck = $this->checkTheSlugForTenant($slug);
		if(count($slugCheck) < 1)
		{
			return redirect('/404');
		}
			$loggedLandlordId 		= Auth::user('landlord')->id;
			$landlordProfile      	= $this->getLandlordProfile($loggedLandlordId);
			$tenantProfileFromLandlord = $this->getTenantProfileFromSlug($slug);
			//dd($tenantProfileFromLandlord);
			$schoolName = $this->getSchoolWithId($tenantProfileFromLandlord->school_id);
			$originalProfile=HTML::getLandlordProfileImage();
			//$landlordProperties = $this->getAllProperties($landlordProfileFromTenant->id);

			return view('landlords.tenantprofile', [
				'pageTitle'   		=> 'Tenant Profile',
				'pageHeading' 		=> 'Tenant Profile',
				'pageMenuName'		=> 'mytenantcontact',
				'tenantObj' 		=> $tenantProfileFromLandlord,
				'schoolname'        => $schoolName,
				'profileImage'    	=> $originalProfile,
				'profile'         	=> $landlordProfile
			]);

	}
	private function checkTheSlugForTenant($slug)
	{
		$tenantCount = Tenant::where('slug', '=', $slug)->first();
		return $tenantCount;
	}

	private function getMessageThreadPropertyName($propertyId)
	{
		$msgThreadPropertyName = PropertyPhysical::where('id',$propertyId)
										->select('property_name')
										->get()->first();
		return $msgThreadPropertyName;
	}

	private function getFormattedMsgThreadCreatedDate($threadDate)
	{

		$formattedMsgThreadCreatedDate = date('d M Y h:i:s a',strtotime($threadDate));
		//print_r($formattedMsgThreadCreatedDate);exit;
		return $formattedMsgThreadCreatedDate;
	}

	public function getCsvDownload()
	{
                $file = public_path("csv/MassAddTenantForm.csv");
                return response()->download($file, 'MassAddTenantForm.csv');
	}


        public function getPropertyCsvDownload()
        {
                $file = public_path("csv/MassAddPropertyForm.csv");
                return response()->download($file, 'MassAddPropertyForm.csv');
        }
}
