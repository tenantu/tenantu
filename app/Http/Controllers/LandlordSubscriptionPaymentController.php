<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Landlord;
use App\Http\Models\LandlordSubscription;
use App\Http\Models\LandlordSubscriptionSchedule;
use Carbon;
use DB;
use Crypt;
use Config;
use Validator;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Response;
use App\Traits\LandlordSubscriptionTrait;
use App\Traits\ForteRequestTrait;

Class LandlordSubscriptionPaymentController extends Controller
{
    use LandlordSubscriptionTrait, ForteRequestTrait;

    public function getLandlordSubscriptionPayment()
    {

    }
    // This function make a request to Forte, and then store inside the DB using helper functions defined in this controller
    public function postLandlordSubscriptionPayment(Request $request)
    {
        $loggedLandlordId = Auth::user('landlord')->id;
	
        $customer_token = $this->getLandlordCustomerId($loggedLandlordId);
        if($customer_token == null)
        {
            Session::flash('warningMessage', "You must set your payment settings in order to upgrade!");
            return redirect()->route('landlords.subscriptionpaymentsettings');
        }
	
        if($request->subscriptionId != 4)
        {
		
                $subscription_schedule_params = $this->getSubscriptionPaymentParams($request, $loggedLandlordId);
                
                // Make the initial schedule - it will be disabled and at base values until the applications get approved in forte_landlord_applications table, and until landlord starts adding properties into their management accounts

                $forte_post_schedule = $this->schedule_subscription_pay(json_decode($subscription_schedule_params), $customer_token); 
               $schedule_http_success = json_decode($forte_post_schedule)->create_http_success;
               $post_schedule_data = json_decode($forte_post_schedule)->data;
         
               //exit('subscription success: ' . $request->subscriptionId);
                


               if($schedule_http_success != 1)
               {
                    //Fail out
                    exit('Error. Contact support!');

               }
               else
               {
                    // Update the existing landlord subscription ()
                    $landlord_subscription_db_params = array(
                        'landlord_id' => $loggedLandlordId,
                        'landlord_subscription_schedule_id' => $post_schedule_data->schedule_id,
                        'subscription_plan_id' => $request->subscriptionId
                    );  
                    // Post this info to Landlord Subscription DB
                    $this->postLandlordSubscriptionInfoDB($landlord_subscription_db_params);

                    // Now get the landlord_subscription id
                    $landlordSubscriptionId = $this->getLandlordSubscriptionId($loggedLandlordId);

                    $landlord_subscription_schedule_db_params = array(
                        'schedule_id' => $post_schedule_data->schedule_id,
                        'landlord_subscription_id'  => $landlordSubscriptionId,
                        'schedule_frequency'        => $post_schedule_data->schedule_frequency,
                        'reference_id'              => $post_schedule_data->reference_id
                    );

                    // Post This info to the landlord subscription schedule DB
                    $this->postLandlordScheduleDB($landlord_subscription_schedule_db_params);

                    // Route to My Subscription Page for Landlord
                    Session::flash('warningMessage', "Subscribed!");
                    return redirect()->route('landlords.dashboard');
               }
	}
            // Update the existing landlord subscription (This is free, so we're assigning schedule id to 0 until they actually pay) 
            $landlord_subscription_db_params = array(
                'landlord_id' => $loggedLandlordId,
                'landlord_subscription_schedule_id' => 0,
                'subscription_plan_id' => $request->subscriptionId
            );  
            // Post this info to Landlord Subscription DB
            $this->postLandlordSubscriptionInfoDB($landlord_subscription_db_params);

            // Route to My Subscription Page for Landlord
            Session::flash('warningMessage', "Subscribed!");
            return redirect()->route('landlords.dashboard');
        
        
    }
    // Update the Schedule with whatever needs updating such as New Pay Method, Schedule Amount, etc
    public function updateLandlordSubscriptionPaymentStatusActive()
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordSubscription = LandlordSubscription::where('landlord_id', $loggedLandlordId)->first();
        $landlordSubscription->active_status = 1;
        $landlordSubscription->save();
    }

    public function updateLandlordSubscriptionPaymentAmountDB($new_amount)
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordSubscription = LandlordSubscription::where('landlord_id', $loggedLandlordId)->first();
        $landlordSubscription->monthly_payment_amount = $new_amount;

        $landlordSubscription->save();
    }

    public function updateLandlordSubscriptionScheduleId($forteScheduleId)
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordSubscription = LandlordSubscription::where('landlord_id', $loggedLandlordId)->first();
        $landlordSubscription->landlord_subscription_schedule_id = $forteScheduleId;
        $landlordSubscription->save();
    }
    // Set Schedule as inactive
    public function updateLandlordSubscriptionPaymentStatusInactive()
    {
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordSubscription = LandlordSubscription::where('landlord_id', $loggedLandlordId)->first();
        $landlordSubscription->active_status = 0;
        $landlordSubscription->save();
    }
    public function displaySubscriptionView()
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile = $this->getLandlordProfile($loggedLandlordId);
        $profileUrl       = asset('public/uploads/landlords/');
        $noProfileUrl     = asset('public/uploads/noimage/');
        if($landlordProfile->image!='')
        {
            $profileImage=$profileUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noProfileUrl."/landlord-blank.png";
        }

        $landlordSubscription = $this->getLandlordSubscriptionPlan($loggedLandlordId);

        return view('landlords.subscription', [
            'pageTitle' => 'Upgrade Subscription',
            'pageHeading' => 'Upgrade Subscription',
            'pageMenuName' => 'upgradesubscription',
            'profile' => $landlordProfile,
            'profileImage' => $profileImage,
            'landlordSubscription' => json_decode($landlordSubscription)
        ]);
    }

    public function getSubscriptionPayConfirmView()
    {

    }

    private function getSubscriptionPaymentParams(Request $request, $landlordId)
    {
        $customer_token = $this->getLandlordCustomerId($landlordId);
        $paymethod_token = $this->getLandlordPaymentId($landlordId);
        $subscriptionPlanId = $request->subscriptionId;
        $monthly_payment_amount = $this->getSubscriptionPlanInitialBasePrice($subscriptionPlanId);
        $landlordName = $this->getLandLordName($landlordId);

        $schedule_params = json_encode(array(
            'customer_token' => $customer_token,
            'paymethod_token' => $paymethod_token,
            'action'            => 'sale',
            'schedule_quantity' => 0,
            'schedule_frequency' => 'monthly',
            'schedule_amount' => $monthly_payment_amount,
            'schedule_created_date' => date("m-d-Y"),
            'reference_id' => $landlordName,
            'schedule_status' => 'active'
        ));

        return $schedule_params;
    }
    private function postLandlordSubscriptionInfoDB($db_params)
    {
        $subscriptionPlanId = $db_params['subscription_plan_id'];
        $schedule_id_pre = $db_params['landlord_subscription_schedule_id'];
        $landlord_id = $db_params['landlord_id'];
        $schedule_id = Crypt::encrypt($schedule_id_pre);

        //$schedule_id = $this->getLandlordSubscriptionScheduleId($loggedLandlordId); // Thie helper function decrypts 
        $monthly_payment_amount = $this->getSubscriptionPlanInitialBasePrice($subscriptionPlanId);

        // Get Information of Subscription Plan by Id

        $landlord_subscription = LandlordSubscription::firstOrNew(array('landlord_id' => $landlord_id));
        $landlord_subscription->landlord_subscription_schedule_id = $schedule_id;
        $landlord_subscription->subscription_plan_id = $subscriptionPlanId;
        $landlord_subscription->monthly_payment_amount = $monthly_payment_amount;
        $landlord_subscription->save();
    }
    private function postLandlordScheduleDB($schedule_db_params)
    {
        $landlord_subscription_id = $schedule_db_params['landlord_subscription_id'];
        $schedule_id_pre = $schedule_db_params['schedule_id'];
        $schedule_id = Crypt::encrypt($schedule_id_pre);


        $landlord_subscription_schedule = LandlordSubscriptionSchedule::firstOrNew(array('landlord_subscription_id' => $landlord_subscription_id));
        $landlord_subscription_schedule->schedule_id = $schedule_id;
        $landlord_subscription_schedule->schedule_frequency = $schedule_db_params['schedule_frequency'];
        $landlord_subscription_schedule->schedule_created_date = date("m-d-Y");
        $landlord_subscription_schedule->reference_id = $schedule_db_params['reference_id'];
        $landlord_subscription_schedule->active_status = 0;
        $landlord_subscription_schedule->save();

    }

    public function makeSubscriptionParams(Request $request, $landlord_id)
    {
        $customer_token = $this->getLandlordCustomerId($landlord_id);
        $paymethod_token = $this->getLandlordPaymentId($landlord_id);
        $schedule_created_date = date("m-d-Y");
        $
        $schedule_params = array(
            'customer_token'        => $customer_token,
            'paymethod_token'       => $paymethod_token,
            'action'                => 'sale',
            'schedule_quantity'     => 0,
            'schedule_frequency'    => 'monthly',
            'schedule_amount'       => $schedule_amount,
            'schedule_created_date' => $schedule_created_date
        );
    }

    // Forte Schedule Maker
    // private function schedule_subscription_pay($rent_pay_params)
    // {
    //   $loggedLandlordId       = Auth::user('landlord')->id;
    //   $customer_token = 
    //   $base_url           = Config::get('constants.FORTE_BASE_URL');
    //   $organization_id    = env('FORTE_TENANTU_MERCHANT_ORG_ID');
    //   $location_id        = env('FORTE_TENANTU_MERCHANT_LOC_ID');
    //   $api_access_id      = env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
    //   $api_secure_key     = env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
    //   $auth_token         = base64_encode($api_access_id . ':' . $api_secure_key);

    //   $pay_schedule_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules';

    //   // This posts the rent payment schedule to Forte
    //   $schedule_rent_pay  = $this->fortePost($pay_schedule_endpoint, $rent_pay_params, $auth_token, $organization_id);

    //   $schedule_pay_response = curl_exec($schedule_rent_pay);
    //   $schedule_pay_info     = curl_getinfo($schedule_rent_pay);
    //   curl_close($schedule_rent_pay);
    //   $schedule_data = json_decode($pay_response);
    //   $create_http_success = (($pay_info['http_code'] != 201) ? 0:1);

    //   $createOutput = array(
    //     'create_http_success'   => $create_http_success,
    //     'data'                  => $schedule_data
    //   );

    //   return $createOutput;
    // }
}
