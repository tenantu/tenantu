<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Landlord;
use App\Http\Models\Tenant;
use App\Http\Models\Property;
use App\Http\Models\PropertyPhysical;
use App\Http\Models\PropertyMeta;
use App\Http\Models\School;
use App\Http\Models\PropertySearch;
use App\Http\Models\ContactUser;
use App\Http\Models\Amenity;
use App\Http\Models\BannerSetting;
use App\Http\Models\BannerPurchase;
use App\Http\Models\FeaturedSetting;
use App\Http\Models\FeaturedPurchase;
use App\Http\Models\MessageThread;
use App\Http\Models\Message;
use App\Http\Models\LandlordTenant;
use App\Http\Models\PropertyExpressInterest;
use App\Http\Models\Setting;
use App\Http\Models\Admin;
use App\Http\Models\SubscriptionPlan;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Response;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class AdminsController extends Controller
{
    public function getLogin(){
		return view('admins.login');
	}
		public function postLogin( Request $request ){

    $rules = array(
        'email'            => 'required|email|',     // required and must be unique in the ducks table
        'password'         => 'required'
    );

    // do the validation ----------------------------------
    // validate against the inputs from our form
    $validator = Validator::make($request->all(), $rules);
		 if ($validator->fails()) {
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		 }
		 else{

		if (Auth::attempt("admin", ['email' => $request->input('email'), 'password' => $request->input('password')])){
		    return redirect('admin/dashboard');
	    }
	    else{
			return redirect()->route('admins.getLogin')->with('message-error', 'The username or password doesnot match.');
		}
	}
	}
	public function dashboard(Request $request)
    {
        $pageTitle = 'Dashboard';
        $imageUrl     = asset('public/uploads/noimage/').'/landlord-blank.png';

        return view('admins.dashboard')->with(compact(['pageTitle','imageUrl']));
    }

    public function profile()
    {
        $imageUrl                = asset('public/uploads/noimage/');
        $adminUser               = Auth::User('admin');

        return view('admins.adminProfile', [
            'pageTitle'   => 'Admin profile',
            'pageHeading' => 'Admin profile',
            'admin'       => $adminUser,
            'imageUrl'    => $imageUrl,

        ]);
    }

    public function editprofile(Request $request)
    {
		//dd($request->all());
		$rules = array(
				'email'            			=> 'required|email',
				'password' 					=> 'required|confirmed|min:6',

         );


         $validator = Validator::make($request->all(), $rules);
         if ($validator->fails()) {
			 return redirect()->back()->withErrors($validator->errors());
		 }

		 $input = array();
		 $input['email'] 	= $request->email;
		 $input['password'] = $request->password;

		 $adminUser     = Auth::user('admin');

		$adminPassword = Hash::make($input['password']);
		$admin       = DB::table('admins')
			->where('id', $adminUser->id)
			->update(['password' => $adminPassword,'email'=>$input['email']]);

		return redirect()->route('admins.dashboard')->with('message-success', 'Admin password has been updated.');



	}

	public function logout(){
		Auth::logout();
		return redirect('admin/login');
	}

	public function registerAsLandlord()
	{
		$schools = $this->getSchoolDetails();
		return view('admins.landlordRegistration', [
            'pageTitle'   => 'Register as landlord',
            'pageHeading' => 'Register as landlord',
            'pageDesc'    => 'Register as landlord',
            'school'      =>  $schools,
        ]);
	}

	public function postRegisterAsLandlord(Request $request)
	{
		$rules = [
			'firstname'           	=> 'required',
			'lastname'            	=> 'required',
            'email'            		=> 'required|email|unique:landlords',
            'school'            	=> 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
        $validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			 return redirect()->back()->withErrors($validator->errors())->withInput()->with('reg-failed',1);
		}
			$input				=  array();
			$input['firstname'] =  $request->firstname;
			$input['lastname'] 	=  $request->lastname;
			$input['school_id'] =  $request->school;
			$input['email'] 	=  $request->email;
			$input['password'] 	=  bcrypt($request->password);
			$input['is_active'] =  1;

			$emailExist = $this->isRegistered($input['email']);
			if(count($emailExist)>0)
			{
				return redirect()->route('admin.landlordRegister')->with('message-error', 'This email is already registered !');
			}
			else
			{
				$landlord = Landlord::create($input);
				return redirect()->route('admins.signinlandlord')->with('message', 'You are successfully registered as landlord you can login now');
			}
	}

	private function isRegistered($email)
    {
		$users = DB::select('SELECT email
							FROM tenants
							WHERE email ="'.$email.'"
							UNION
							SELECT email
							FROM landlords
							WHERE email = "'.$email.'"');
		return $users;
	}

	private function isRegisteredEmail($email)
	{
		$userExist = DB::table('landlords')
					 ->select('email')
                     ->where('email', '=',$email)
                     ->get();
        return $userExist;
	}

	public function getTrend()
	{
		$tenantCountDetails 			= $this->getTenantCount();
		$landlordCountDetails 			= $this->getLandlordCount();
		$propertyCountDetails 			= $this->getPropertyCountValue();
		$schoolCountDetails 			= $this->getSchoolCount();
		$searchCountDetails 			= $this->getSearchCount();
		$contactCountDetails 			= $this->getContactUsCount();

		$mostViewedPropertyDetails 		= $this->getMostViewedProperty();
//		$mostInquiredDetails 			= $this->getMostInquired();
		$mostSearchedDetails 			= $this->getMostSearched();
		$mostSearchedPriceDetails 		= $this->getMostSearchedPrice();
		$mostSchoolsSearchDetails 		= $this->getMostSchoolsSearched();
		$mostSearchedMonthYear			= $this->getMostSearchedMonthYear();


		return view('admins.trend', [
            'pageTitle'   					 => 'Trend',
            'pageHeading' 					 => 'User activity/Trends',
            'pageDesc'    					 => '',
            'mostViewedPropertyDetails'      =>  $mostViewedPropertyDetails,
//            'mostInquiredDetails'      		 =>  $mostInquiredDetails,
            'mostSearchedDetails'      		 =>  $mostSearchedDetails,
            'mostSearchedPriceDetails'       =>  $mostSearchedPriceDetails,
            'mostSchoolsSearchDetails'       =>  $mostSchoolsSearchDetails,
            'mostSearchedMonthYears'       	 =>  $mostSearchedMonthYear,
            'tenantCountDetails'       	 	 =>  $tenantCountDetails,
            'landlordCountDetails'       	 =>  $landlordCountDetails,
            'propertyCountDetails'       	 =>  $propertyCountDetails,
            'schoolCountDetails'       	 	 =>  $schoolCountDetails,
            'searchCountDetails'       	 	 =>  $searchCountDetails,
            'contactCountDetails'       	 =>  $contactCountDetails,
        ]);
	}

	private function getMostViewedProperty()
	{
		$propertyViewedArray = DB::select('SELECT count(PV.property_id) as searchCount,P.property_name FROM property_viewed as PV join properties as P on PV.property_id=P.id group by(PV.property_id) order by (PV.property_id) asc LIMIT 0 , 50');
		return $propertyViewedArray;
	}
//      The table property_enquiry_view is being retired.
//
//	private function getMostInquired()
//	{
//		$mostInquiredArray = DB::select('SELECT property_id, property_name, count( property_id ) AS enquirycount
//										FROM property_enquiry_view
//										JOIN properties
//										WHERE properties.id = property_enquiry_view.property_id
//										GROUP BY property_id
//										ORDER BY enquirycount DESC LIMIT 0 , 50');
//		return $mostInquiredArray;
//	}

	private function getMostSearched()
	{
		$mostSearchedArray = DB::select("SELECT count( search ) AS searchCount, search
											FROM property_search
											WHERE search <> ''
											GROUP BY search
											ORDER BY searchCount DESC LIMIT 0 , 50 ");
		return $mostSearchedArray;
	}

	private function getMostSearchedPrice()
	{
		$mostSearchedPriceArray = DB::select("SELECT CONCAT( price_form, ' ', price_to ) AS pricerange, count( * ) AS searchCount
											FROM property_search
											GROUP BY pricerange
											ORDER BY searchCount DESC LIMIT 0 , 50 ");
		return $mostSearchedPriceArray;
	}

	private function getMostSchoolsSearched()
	{
		$mostSchoolsSearchedArray = DB::select('SELECT count(PS.school_id) as searchCount,PS.school_id,S.schoolname FROM property_search as PS join schools as S on PS.school_id=S.id group by(PS.school_id) order by searchCount DESC LIMIT 0 , 50');
		return $mostSchoolsSearchedArray;
	}

	private function getMostSearchedMonthYear()
	{
		$mostSearchedMonthYearArray = DB::select('SELECT count( * ) AS searchCount, YEAR( created_at ) AS year, MONTH( created_at ) AS
													month FROM property_search
													GROUP BY YEAR( created_at ) , MONTH( created_at )
													ORDER BY YEAR( created_at ) DESC , MONTH( created_at ) DESC
													LIMIT 0 , 50');
		return $mostSearchedMonthYearArray;
	}

	private function getTenantCount()
	{
		$tenantCountArr = Tenant::count();
		return $tenantCountArr;
	}

	private function getLandlordCount()
	{
		$landlordCountArr = Landlord::count();
		return $landlordCountArr;
	}

	private function getPropertyCountValue()
	{
		$propertyCountArr = Property::count();
		return $propertyCountArr;
	}

	private function getSchoolCount()
	{
		$schoolCountArr = School::count();
		return $schoolCountArr;
	}

	private function getSearchCount()
	{
		$searchCountArr = PropertySearch::count();
		return $searchCountArr;
	}

	private function getContactUsCount()
	{
		$contactCountArr =ContactUser::count();
		return $contactCountArr;
	}
	public function getAmenities(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $amenitiesQuery = DB::table('amenities');
        if ($request->has('search')) {
            $search = $request->input('search');
            $amenitiesQuery->where('amenity_name', 'like', '%' . $request->input('search') . '%');
        }
        $amenitiesQuery->orderBy($sortby, $order);
        $amenities = $amenitiesQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('admins.getAmenities', ['pageTitle' => 'Amenities',
            'pageHeading'                               => 'Amenities',
            'pageDesc'                                  => 'Manage Amenities here',
            'amenities'                                 => $amenities,
            'search'                                    => $search,
            'sortby'                                    => $sortby,
            'order'                                     => $order,
        ]);

    }
    public function createAmenity()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

	   $school = $this->getSchoolDetails();
       return view('admins.createAmenity', ['pageTitle' => 'Create Tenant',
            'pageHeading'                                 => 'Create Tenants',
            'pageDesc'                                    => 'Create Tenants here',
            'school'                                        => $school,
        ]);
    }
    public function storeAmenity(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
         $rules = [
            'amenity_name'             => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {

			$input['amenity_name'] = $request->amenity_name;

            $tenant = Amenity::create($input);


            return redirect()->route('admins.amenities')->with('message', 'Amenity Created.');
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }
    public function editAmenity($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $amenity             = Amenity::find($id);



        return view('admins.editAmenities', ['pageTitle' => 'Edit Amenity',
            'pageHeading'                               => 'Edit Amenity',
            'pageDesc'                                  => 'Edit Amenity here',
            'amenity'                                    => $amenity,
        ]);
    }
    public function updateAmenity(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $rules = [
            'amenity_name'  => 'required',
        ];

        $messages = [
            'amenity_name.required'  => 'Amenity Name is required.',
        ];



        $input     = $request->all();
        //dd($input);

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {

            $amenity         = Amenity::find($id);

            $amenity->update($input);

            $amenityId = Amenity::find($amenity->id);

            if ($amenityId['id']) {
                 return redirect()->route('admins.amenities')->with('message-success', 'Amenity is updated.');
            }

        }else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }
    public function changeStatusAmenity($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
        $amenity = Amenity::find($id);
        if ($amenity->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $amenity->update($data);
        return redirect()->route('admins.amenities')->with('message', 'Amenity Status Updated');
    }
    public function destroyAmenity($id)
    {
        //
        $adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
        $amenity = Amenity::find($id);
        $amenity->delete();
        return redirect()->route('admins.amenities')->with('message', 'Amenity deleted successfuly.');
    }

    public function getBannerSettings()
    {
		$bannerDetails = $this->getBannerDetails();

		 return view('admins.banner_settings', ['pageTitle' => 'Banner details',
            'pageHeading'                               => 'Banner details',
            'pageDesc'                                  => 'Edit banner details',
            'bannerDetails'                             => $bannerDetails,
        ]);
	}

	public function editBannerSettings($bannerId)
	{
			$banner             = BannerSetting::find($bannerId);
			return view('admins.editBannerSetting', ['pageTitle' => 'Edit Banner Settings',
            'pageHeading'                               => 'Edit Banner Settings',
            'pageDesc'                                  => 'Edit Banner Settings here',
            'banner'                                    => $banner,
        ]);
	}




	private function getBannerDetails()
	{
		$bannerDetails = BannerSetting::select('banner_name')
						->addSelect('banner_size')
						->addSelect('banner_price')
						->addSelect('id')
						->addSelect('status')
						->addSelect('sample_image')
						->addSelect('description')
						->get();
		return $bannerDetails;
	}

	public function updateBannerSettings(Request $request,$bannerId)
	{
		$rules = [
            'banner_name'  => 'required',
            'banner_price'    	=> 'required',

        ];


        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails())
        {
			$updatedBanner = BannerSetting::where('id', $bannerId)
									->update(['banner_name'=>$request->banner_name,'banner_price'=>$request->banner_price,'description'=>$request->banner_description]);

            return redirect()->route('admins.bannerSettings')->with('message', 'Banner details are updated.');
		}
		else
        {
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
	}




	public function getBannerPurchases(Request $request)
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		$search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';

        $bannerPurchasesQuery = DB::table('banner_purchases');
        if ($request->has('search')) {
            $search = $request->input('search');
            $bannerPurchasesQuery->where('name', 'like', '%' . $request->input('search') . '%');
        }
        $bannerPurchasesQuery->orderBy($sortby, $order);
        $bannerPurchases = $bannerPurchasesQuery->paginate(10);
		$order = ($order == 'asc') ? 'desc' : 'asc';

		/*$bannerPurchases = BannerPurchase::select('name')
							->addSelect('banner_name')
							->addSelect('purchase_date_from_paypal')
							->addSelect('status')
							->addSelect('payment_status')
							->addSelect('created_at')
							->addSelect('updated_at')
							->addSelect('id')
							->addSelect('amount_paid')
							->addSelect('image')
							->get();*/

		return view('admins.bannerPurchases', ['pageTitle' => 'Banner Purchases',
            'pageHeading'                               => 'Banner Purchases',
            'pageDesc'                                  => 'Banner Purchases here',
            'bannerPurchases'                           => $bannerPurchases,
            'search'                                    => $search,
            'sortby'                                    => $sortby,
            'order'                                     => $order,
        ]);

	}

	public function changeBannerStatus($bannerId)
	{
		$banner = BannerPurchase::find($bannerId);
		if ($banner->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $banner->update($data);
        return redirect()->route('admins.bannerPurchases')->with('message', 'Banner Status Updated');

	}

	public function changeBannerSettingStatus($bannerId)
	{

		$bannerSetting = BannerSetting::find($bannerId);
		if ($bannerSetting->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $bannerSetting->update($data);
        return redirect()->route('admins.bannerSettings')->with('message', 'Banner Setting Status Updated');
	}



	public function getBannerPurchaseDetails($bannerId)
	{
		$bannerPurchaseDetails = BannerPurchase::where('id',$bannerId)
											->select('name')
											->addSelect('banner_name')
											->addSelect('purchase_date_from_paypal')
											->addSelect('created_at')
											->addSelect('updated_at')
											->addSelect('status')
											->addSelect('payment_status')
											->addSelect('image')
											->addSelect('amount_paid')
											->addSelect('activated_date')
											->addSelect('id')
											->first();
		$bannerImage = asset('public/uploads/banners/').'/'.$bannerPurchaseDetails->image;
		return view('admins.bannerPurchaseDetails', ['pageTitle' => 'Banner Purchase Details',
            'pageHeading'                               => 'Banner Purchase Details',
            'pageDesc'                                  => 'Banner Purchases Details',
            'bannerPurchaseDetails'                     => $bannerPurchaseDetails,
            'bannerImage'								=> $bannerImage
        ]);

	}

	public function activateBannerPurchase(Request $request,$bannerId)
	{
		$currentDate = date('Y-m-d');
		$updatedBanner = BannerPurchase::where('id', $bannerId)
									->update(['status'=>1,'activated_date'=>Carbon::now()]);

		return redirect()->route('admins.bannerPurchases')->with('message', 'Banner Purchase Status Updated');

	}

	public function deActivateBannerPurchase(Request $request,$bannerId)
	{
		$updatedBanner = BannerPurchase::where('id', $bannerId)
									->update(['status'=>0,'deactivated_date'=>Carbon::now()]);

		return redirect()->route('admins.bannerPurchases')->with('message', 'Banner Purchase Status Updated');
	}

	public function bannerPurchaseDestroy($bannerId)
	{
		$bannerPurchase = BannerPurchase::find($bannerId);
        $bannerPurchase->delete();
        return redirect()->route('admins.bannerPurchases')->with('message', 'Banner Purchase Details Deleted.');
	}

	public function getFeaturedPropertySettings()
	{

		$featuredSettings = $this->getFeaturedSettings();

		 return view('admins.featured_settings', ['pageTitle' => 'Featured Property Details',
            'pageHeading'                               => 'Featured Property Details',
            'pageDesc'                                  => 'Featured Property Details',
            'featuredSettings'                          => $featuredSettings,
        ]);
	}

	public function editFeaturedPropertySettings($featuredId)
	{
		$featured             = FeaturedSetting::find($featuredId);

		return view('admins.editFeaturedSetting', ['pageTitle' => 'Edit Featured Settings',
            'pageHeading'                               => 'Edit Featured Settings',
            'pageDesc'                                  => 'Edit Featured Settings here',
            'featured'                                    => $featured,
        ]);
	}

	public function updateFeaturedPropertySettings(Request $request,$featuredId)
	{
		$rules = [
            'price'  => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails())
        {
			$updatedBanner = FeaturedSetting::where('id', $featuredId)
									->update(['price'=>$request->price]);

            return redirect()->route('admins.featuredPropertySettings')->with('message', 'Featured details are updated.');
		}
		else
        {
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}

	}

	public function changeFeaturedSettingStatus($featuredId)
	{
		$data = array();
		$featuredSetting = FeaturedSetting::find($featuredId);
		//dd($featuredSetting);
		if ($featuredSetting->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $featuredSetting->update($data);
        return redirect()->route('admins.featuredPropertySettings')->with('message', 'Featured Setting Status Updated');
	}

	private function getNonFeaturedProperties()
	{
		$nonFeatured = array();
		$nonFeatured  = DB::table('properties')->where('status',1)->orderBy('property_name', 'asc')->lists('id', 'property_name');
		return $nonFeatured;
	}

	private function getFeaturedSettings()
	{
		$featuredSettings = FeaturedSetting::select('duration')
											->addSelect('price')
											->addSelect('id')
											->addSelect('status')
											->get();
		return $featuredSettings;
	}

	public function getFeaturedPurchases(Request $request)
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}



		$featuredPurchasesQuery = DB::table('featured_purchase AS FP')
							 ->select('FP.landlord_id','FP.id','FP.property_id','FP.duration_name','FP.amount_paid','FP.payment_status','P.property_name','L.firstname','L.lastname')

							 -> join ('properties AS P','FP.property_id','=','P.id')
							 -> join('landlords AS L', 'FP.landlord_id', '=', 'L.id')
							 -> where('FP.payment_status','Completed')
							 -> orderBy('FP.created_at','DESC');

        $featuredPurchases = $featuredPurchasesQuery->paginate(10);

         return view('admins.featuredPurchases', ['pageTitle' => 'Featured Purchases',
            'pageHeading'                               => 'Featured Purchases',
            'pageDesc'                                  => 'Featured Purchases here',
            'featuredPurchases'                         => $featuredPurchases,
            'search'                                    => '',
            'sortby'                                    => '',
            'order'                                     => '',
        ]);


	}

	public function getFeaturedPurchasesDetail($id)
	{
		$featuredPurchaseDetail = FeaturedPurchase::from('featured_purchase AS FP')
                                 -> select('FP.landlord_id')
                                 -> addSelect('FP.id')
                                 -> addSelect('FP.property_id')
                                 -> addSelect('FP.duration_name')
                                 -> addSelect('FP.amount_paid')
                                 -> addSelect('FP.payment_status')
                                 -> addSelect('FP.featured_start')
                                 -> addSelect('FP.featured_end')
                                 -> addSelect('P.property_name')
                                 -> addSelect('P.featured')
                                 -> addSelect('L.firstname')
                                 -> addSelect('L.lastname')
                                 -> join ('properties AS P','FP.property_id','=','P.id')
                                 -> join('landlords AS L', 'FP.landlord_id', '=', 'L.id')
                                 -> where('FP.id',$id)
                                 -> get();
        //print($featuredPurchaseDetail);

         return view('admins.featuredPurchasesDetail', ['pageTitle' => 'Featured Purchase Detail',
            'pageHeading'                               => 'Featured Purchase Detail ',
            'pageDesc'                                  => 'Featured Purchase Detail',
            'featuredPurchasesdetail'                   => $featuredPurchaseDetail,

        ]);


	}

	public function getDetailsForReassign()
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$schools = $this->getSchoolDetails();

		return view('admins.getReassignLandlord', ['pageTitle' => 'Reassign Landlord',
            'pageHeading'               => 'Reassign Landlord ',
            'pageDesc'                  => 'Reassign Landlord',
            'schools'                   =>  $schools,

        ]);
	}

	public function getPropertyLandlord(Request $request)
	{
		$page 		= $request->page;
		if($page == 'schoolchange')
		{
			$schoolArray = array();
			$schoolId = $request->schoolId;
			$property = $this->getPropertyDetails($schoolId);

			$schholLandlords = $this->getSchoolLandlords($schoolId);

			$schoolArray['property'] = $property;
			$schoolArray['landlords'] = $schholLandlords;

			return $schoolArray;
		}
		if($page == 'propertychange')
		{
			$propertyId 		= $request->propertyId;
			$propertyLandlord 	= $this->getPropertyForLandlord($propertyId);
			return $propertyLandlord;
		}
	}

	private function getSchoolLandlords($schoolId)
	{
		$landlords = DB::select('select CONCAT(firstname," ",lastname)as fullname,id FROM landlords where school_id='.$schoolId.'');

		return $landlords;
	}

	private function getPropertyForLandlord($propertyId)
	{
		$property = PropertyPhysical::where('id',$propertyId)
						->select('landlord_id')
						->first();
		$landlord = Landlord::where('id',$property->landlord_id)
						->select('firstname')
						->addSelect('lastname')
						->first();
		return $landlord;
	}

	private function getPropertyDetails($schoolId)
	{
		$property  = array();
		$property  = DB::table('property_meta as m')
					   ->where('m.status',1)
					   ->leftJoin('property_physical as p', 'm.property_physical_id', '=', 'p.id')
					   ->where('p.school_id',$schoolId)
					   ->orderBy('p.property_name', 'asc')
					   ->lists('p.property_name', 'id');
		return $property ;

	}

	public function reassignSave(Request $request)
	{
		$rules = array(
			'school' 			=> 'required',     // required and must be unique in the ducks table
			'property'			=>	'required',
			'landlord' 			=> 'required',
		);

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}

		$school 	= $request->school;
		$property 	= $request->property;
		$landlord 	= $request->landlord;

		$this->updateProperties($school,$property,$landlord);
		$this->updateFeaturedPurchases($school,$property,$landlord);
		$this->updateMessageThread($school,$property,$landlord);
		$this->updateLandlordTenants($school,$property,$landlord);
		$this->updatePropertyExpressInterest($school,$property,$landlord);


		return redirect()->route('admins.reassignLandlord')->with('message-success', 'Landlord has been updated for the selected property');
	}

	private function updateProperties($school,$property,$landlord)
	{


		PropertyPhysical::where('id',$property)
						->update(['landlord_id'=>$landlord]);

	}

	private function updateFeaturedPurchases($school,$property,$landlord)
	{
		FeaturedPurchase::where('property_id',$property)
							->update(['landlord_id'=>$landlord]);
	}

	private function updateMessageThread($school,$property,$landlord)
	{
		$messageThreads = MessageThread::where('property_id',$property)
						//->where('landlord_id',$landlord)
						->select('id')
						->get();


			MessageThread::where('property_id',$property)
						->update(['landlord_id'=>$landlord]);
			foreach($messageThreads as $messageThread)
			{
					Message::where('message_thread_id',$messageThread->id)
						->update(['landlord_id'=>$landlord]);
			}

	}

	private function updateLandlordTenants($school,$property,$landlord)
	{
		LandlordTenant::where('property_id',$property)
						->update(['landlord_id'=>$landlord]);
	}

	private function updatePropertyExpressInterest($school,$property,$landlord)
	{
		PropertyExpressInterest::where('property_id',$property)
						->update(['landlord_id'=>$landlord]);
	}


	public function getSettings()
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$setting = $this->getSettingsDetail();

		return view('admins.getSettings', ['pageTitle' => 'Set commision',
            'pageHeading'               => 'Set commision fee for rent payment',
            'pageDesc'                  => 'Set commision fee for rent payment',
            'setting'                   =>  $setting,

        ]);
	}

	private function getSettingsDetail()
	{
		$commisionSetting = Setting::select('commision')
								->get();
		return $commisionSetting;
	}

	public function postSetting(Request $request)
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}


		$rules = array(
			'commission' 			=> 'required'     // required and must be unique in the ducks table

		);

		$validator = Validator::make($request->all(), $rules);
		if ($validator->fails())
		{
			return redirect()->back()->withErrors($validator->errors());
		}

		else
		{
			$commision = $request->commission;
			$commissionSetting = $this->getCommission();

			if(count($commissionSetting)> 0)
			{
				Setting::where('id', 1)
							->update(['commision' => $commision]);
			}
			else
			{
				Setting::create(['commision'=>$commision]);
			}
			return redirect()->route('admins.settings')->with('message-success', 'Commission  charge has been updated');

		}

	}

	private function getCommission()
	{
		$commision = Setting::select('commision')
					->get();
		return $commision;
	}

	public function getAdmins()
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$admins = Admin::select('name')
					->addSelect('email')
					->addSelect('id')
					->get();
		 return view('admins.getAdmins', ['pageTitle' => 'Admins',
            'pageHeading'               => 'Admins',
            'pageDesc'                  => 'Admins',
            'admins'                   =>  $admins,

        ]);

	}


	public function  AddAdminUsers()
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		return view('admins.addAdmins', ['pageTitle' => 'Add Admin',
            'pageHeading'               => 'Add Admin',
            'pageDesc'                  => 'Add Admin'

        ]);

	}

	public function store(Request $request)
	{
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

		$rules = [
            'name'             	=> 'required',
            'email'            	=> 'required|email|unique:admins',
            'password'              	=> 'required|min:6|confirmed',
            'password_confirmation' 	=> 'required|min:6'

        ];

        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
			$input['name'] = $request->name;
			$input['email'] = $request->email;
            $input['password'] = bcrypt($request->password);

            $admin = Admin::create($input);
            return redirect()->route('admin.getAdmins')->with('message', 'Admin Created.');
		}
		else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

	}

	public function destroyAdmin($id)
    {
        //
        $adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
        $admin = Admin::find($id);
        $admin->delete();
        return redirect()->route('admin.getAdmins')->with('message', 'Admin deleted successfuly.');
    }

    public function editAdmin($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $admin             = Admin::find($id);



        return view('admins.editAdmin', ['pageTitle' => 'Edit Admin',
            'pageHeading'                               => 'Edit Admin',
            'pageDesc'                                  => 'Edit Admin here',
            'admin'                                    => $admin,
        ]);
    }

    public function updateAdmin(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}

        $rules = [
            'name'  => 'required',
            'email'            	=> 'required|email',
            'password'              	=> 'required|min:6|confirmed',
            'password_confirmation' 	=> 'required|min:6'
        ];






        //dd($input);

        $validator = Validator::make($request->all(), $rules);

        if (!$validator->fails()) {
			$input['name'] = $request->name;
			$input['email'] = $request->email;
			$input['password'] = bcrypt($request->password);

            $adminUpdate = Admin::where('id',$id)
            ->update(['name'=>$input['name'],'email'=>$input['email'],'password'=>$input['password']]);

            if ($adminUpdate) {
                 return redirect()->route('admin.getAdmins')->with('message', 'Admin is updated.');
            }

        }else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

}
