<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\PaymentSetting;
use App\Http\Models\Setting;
use App\Http\Models\TenantPaymentSetting;
use App\Http\Models\TenantBillingAddress;
use DB;
use DateTime;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use Crypt;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Response;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class TenantCreatePayCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // POST to Forte
    public function postCustomer(Request $request) {

    	//Grab Logged Tenant Information from the Tenant table
	    $loggedTenant 		= Auth::user('tenant');
	    $loggedTenantId 	= $loggedTenant->id;

	    //$base_url          = 'https://sandbox.forte.net/api/v3';     //production: http://api.forte.net/v3
	    // Landlord Required Fields
	    $base_url   = env('FORTE_BASE_URL');
        $organization_id = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
        //$location_id = 'loc_193969';
        $location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
        $api_access_id = env('FORTE_API_ACCESS_ID');
        $api_secure_key = env('FORTE_API_SECURE_KEY');
        // END Landlord Required fields



	    $rules = array(
	            'bank_name'       => 'required',
	            'account_holder'  => 'required',
	            'account_no'      => 'required',
	            'routing_no'     => 'required',
	            'st_line1'        => 'required',
	            'city'            => 'required',
	            'state'           => 'required',
	            'zip_code'        => 'required',
	        );
	    $messages = [
	                        'bank_name.required'      => 'Bank name field is required',
	                        'account_holder.required' => 'Account Holder is required',
	                        'account_no.required'     => 'Account Number is required',
	                        'routing_no.required'    => 'Routing Number is required',
	                        'st_line1.required'       => 'Address Line 1 is required',
	                        'city.required'           => 'City is required',
	                        'state.required'          => 'State is required',
	                        'zip_code.required'       => 'Zip Code is required'
	                ];

	    $validator = Validator::make($request->all(), $rules, $messages);
	    if ($validator->fails())
	    {
	      return redirect()->back()->withErrors($validator->errors())->with('displayBank',1);
	    }
	    //if(preg_match('/\*/', $request->account_no, $matches) != 0)
	    /*{

	    	return redirect()->route('payments.getTenantPaymentSettings')->with('message-error', 'Invalid Characters in Account Number')->with('failureBank',1);
	    }*/

	    else // Commence with Create or Update
	    {
		    //$loggedTenantID = $loggedTenant->id;
		    $loggedTenantEmail = $loggedTenant->email;

		    // These Get Name methods are in Controller.php, not TenantsController
		    $tenantFirstName = $this->getTenantFirstName($loggedTenant);
		    $tenantLastName = $this->getTenantLastName($loggedTenant);

		    // ECheck/Bank Variables
		    $bank_name      = $request->bank_name;
		    /**** Account Type according to selection in form ****/
		    $account_type   = ($request->account_type == 'C' ? 'checking':'savings');
		    $account_holder = $request->account_holder;
		    $account_number = $request->account_no;
		    $routing_number = $request->routing_no;


		    // Physical Address Variables
		    $street_line1   = $request->st_line1;
		    $street_line2   = $request->st_line2;
		    $locality       = $request->city;
        $region         = $request->state;
		    $postal_code    = $request->zip_code;

		    // Auto Pay Variable - LAZY PROGRAMMING...SETTING IT TO 0 IF $request->auto_payrent is not 1.
            $auto_payrent   = ($request->auto_payrent != 1 ? 0:1);
            $ready_pay      = $auto_payrent;

		    // Property that the Tenant is part of
		    $tenantCurrentProperty = $this->getTenantCurrentProperty($loggedTenantId);

		    $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
		    $endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/';

		    //Address/Name Info
		    $name = array(
		        'first_name' => $tenantFirstName,
		        'last_name' => $tenantLastName
		    );

		    $full_name = $name['first_name'] . ' ' . $name['last_name'];

		    // Physical address
		    $physical_address = array(
		        'street_line1'  => $street_line1,
		        'street_line2'  => $street_line2,
		        'locality'      => $locality,
            	'region'        => $region,
		        'postal_code'   => $postal_code
		    );


		    $billingaddress = array(
		        'first_name'            => $name['first_name'],
		        'last_name'             => $name['last_name'],
		        'label'                 => $full_name . "'s " . 'Billing Address',
		        'email'                 => $loggedTenantEmail,
		        'address_type'          => 'default_billing',
		        'shipping_address_type' => 'residential',
		        'physical_address'      => $physical_address
		    );
		    //eCheck Info to store into Customer PayMethod - This is the account rent will be paid from
		    $echeck = array(
		        'account_holder'    => $account_holder,
		        'routing_number'    => $routing_number,
		        'account_number'    => $account_number,
		        'account_type'      => $account_type
		    );
		    // ARRAY TO STORE PAYMETHOD INTO CUSTOMER PARAMETERS
		    $default_pay_method = array(
		        'label'     => $bank_name,
		        'notes'     => $full_name . "'s ". 'Rent Payment Bank Account' . ' - ' . $tenantCurrentProperty,
		        'echeck'    => $echeck
		    );

		    // PARAMETERS OF THE CUSTOMER TO BE MADE - used for createTenantPaySettingsForte
		    $customer_params = array(
		        'first_name'    => $name['first_name'],
		        'last_name'     => $name['last_name'],
		        'paymethod'     => $default_pay_method,
		        'addresses'     => array($billingaddress)
		    );

		    // PARAMETERS OF USER INPUT FROM VIEW, used for updating Forte and DB
		    $tenant_input_params = array(
		    	'label'				=> $bank_name,
		    	'account_type'		=> $account_type,
		    	'account_holder' 	=> $account_holder,
		    	'account_number'	=> $account_number,
		    	'routing_number'	=> $routing_number,
		    	'street_line1'		=> $street_line1,
		    	'street_line2'		=> $street_line2,
		    	'locality'			=> $locality,
		    	'region'			=> $region,
		    	'postal_code'		=> $postal_code,
		    	'auto_payrent'		=> $auto_payrent,
		    	'ready_pay'			=> $ready_pay
		    );
		    // CHECK IF TENANT HAS AN EXISTING FORTE CUSTOMER TOKEN
	    	$check_existing_cust_token = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('customer_id');

	    	// If Logged In Tenant HAS an existing Forte Customer ID inside forte_tenant_pay_settings table
	    	if($check_existing_cust_token != '')
	    	{

	    		$stored_landlord_org_id = $this->getTenantStoredLandlordOrgId($loggedTenantId);
	    		$current_associated_landlord_org_id = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
	    		$storedPropertyLocId = $this->getTenantStoredPropertyLocId($loggedTenantId);
	    		$currentAssociatedPropertyLocId = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
	    		// If tenant's stored landlord location id is NOT EQUAL to the loc id of their current Landlord, we need to create a new Customer for Tenant in Forte under the Current Landlord's Loc ID, and update
	    		if(($stored_landlord_org_id != $current_associated_landlord_org_id) || ($storedPropertyLocId != $currentAssociatedPropertyLocId))
	    		{
	    			if(strpos($request->account_no,'*') !== false)
	    			{

	    				return redirect()->route('payments.getTenantPaymentSettings')->with('message-error', 'Invalid Characters in Account Number')->with('failureBank',1);
	    			}
	    			else
	    			{
			    		$pay_data = $this->createTenantPaySettingsForte($customer_params);

			    		// Tenant address token refers to the billing address
			    		$tenant_payment_token = $pay_data['data']->default_paymethod_token;
			    		$tenant_customer_token = $pay_data['data']->customer_token;
			    		$tenant_address_token = $pay_data['data']->default_billing_address_token;
			    		$create_http_success = $pay_data['create_http_success'];
		    			if ($create_http_success != 1)
		    			{
		    				return redirect()->route('payments.getTenantPaymentSettings')->with('message-error', 'Invalid input detected, please ensure all information is valid.')->with('failureBank',1);
		    			}
		    			else
		    			{
						    // Array of parameters to be stored in the Tenant Table - used in another function in this controller
						    $tenant_payment_settings = array(
						        'customer_token' => $tenant_customer_token,
						        'payment_token' => $tenant_payment_token,
				                'address_token' => $tenant_address_token,
				                'auto_pay'      => $auto_payrent,
				                'ready_pay'     => $ready_pay
					    	);
					    	// Update the Existing Forte Tenant Payment Setting table with a new customer token, paymethod token, address token, current landlord loc id, etc
			    			$this->updateNewTenantPaySettingsDB($tenant_payment_settings);
			    			return redirect()->route('payments.getTenantPaymentSettings')->with('message-success', 'Payment Settings Saved!')->with('successBank',1);
			    		}
			    	}
	    		}
	    		else
	    		{
	    			// HTPP Code grabbed from checking the Two Update Requests in the Forte Update Function
		    		$update_http_code = $this->updateTenantPaySettingsForte($tenant_input_params);
		    		$tenant_payment_settings = array(
		    			'auto_pay'	=> $auto_payrent,
		    			'ready_pay' => $ready_pay
		    		);
		    		$this->updateTenantPaySettingsDB($tenant_payment_settings);

		    		if ($update_http_code != 1)
		    		{
		    			return redirect()->route('payments.getTenantPaymentSettings')->with('message-error', 'Invalid input detected, please ensure all information is valid.')->with('failureBank',1);
		    		}
		    		else
		    		{
		    			return redirect()->route('payments.getTenantPaymentSettings')->with('message-success', 'Payment Settings Saved!')->with('successBank',1);
		    		}
	    		}
			}
			// If Logged In Tenant HAS an existing Forte Customer ID, BUT Tenant's stored landlord_location_id does not match the location_id of currently associated Landlord, this means Tenant must have moved housing. So we will Create a new Forte customer under Landlord Loc ID, and update Tenant's information in the Database Accordingly
			else
			// Create the Customer Object in Forte for the Tenant
			{

				$pay_data = $this->createTenantPaySettingsForte($customer_params);

		    // Tenant address token refers to the billing address
				//return var_dump($pay_data);
				if ($pay_data['data'] != null)
				{
		    		$tenant_payment_token = $pay_data['data']->default_paymethod_token;
		    		$tenant_customer_token = $pay_data['data']->customer_token;
		    		$tenant_address_token = $pay_data['data']->default_billing_address_token;
		    	//$response_desc 		  = $pay_data['data']->response->response_desc;
		    	}
		    	$create_http_success = $pay_data['create_http_success'];
	    		
	    		if ($create_http_success != 1)
	    		{
	    			return redirect()->route('payments.getTenantPaymentSettings')->with('message-error', 'Invalid input detected, please ensure all information is valid.')->with('failureBank',1);
	    		}
	    		else
	    		{
				    // Array of parameters to be stored in the Tenant Table - used in another function in this controller
				    $tenant_payment_settings = array(
				        'customer_token' => $tenant_customer_token,
				        'payment_token' => $tenant_payment_token,
		                'address_token' => $tenant_address_token,
		                'auto_pay'      => $auto_payrent,
		                'ready_pay'     => $ready_pay
				    );
				    $this->createTenantPaySettingsDB($tenant_payment_settings, $physical_address);

				    return redirect()->route('payments.getTenantPaymentSettings')->with('message-success', 'Payment Settings Saved!')->with('successBank',1);
	    		}
		    }
        }
    }

    // Showing the form for creating / editing Tenant Pay Settings
    public function getPaymentSettings()
    {
    	$loggedTenant = Auth::user('tenant');
	    $loggedTenantId = $loggedTenant->id;
	    // $encrypted_id = Crypt::encrypt('loc_193969');
	    // $do_encrypt_loc_id = DB::table('forte_landlord_pay_settings_rent')->where('id', 1)->update(['loc_id' => $encrypted_id]);
	    $tenantAssociatedWithLandlord = $this->isTenantAssociatedWithLandlord($loggedTenantId);
	    $tenantProfile  = $this->getTenantProfile($loggedTenantId);
	    $imageUrl       = asset('public/uploads/tenants/');
		if ($tenantAssociatedWithLandlord !== true)
        {
        	Session::flash('warningMessage', "You must have a Landlord on TenantU to set your Rent Payment Settings!");
        	return redirect()->route('tenants.getMyLandlords');
        }
        else
        {

	        // $current_landlord_location_id = $this->getTenantCurrentLandlordLocationId($loggedTenantId);
	        $base_url          = env('FORTE_BASE_URL');     //production: http://api.forte.net/v3
	        // Landlord Required Fields
	        $organization_id   = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
	        $location_id       = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
	        $api_access_id     = env('FORTE_API_ACCESS_ID');
	        $api_secure_key    = env('FORTE_API_SECURE_KEY');
	        $auth_token        = base64_encode($api_access_id . ':' . $api_secure_key);
	        //$endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/';

	        //$tenant_customer_id = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('customer_id');
	        $tenant_customer_id = $this->getTenantCustomerId($loggedTenantId);
	        //$tenant_payment_id = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('payment_id');
	        $tenant_payment_id = $this->getTenantPaymentId($loggedTenantId);
	        //$tenant_address_id = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('addr_id');
	        $tenant_address_id = $this->getTenantAddressId($loggedTenantId);
	        $tenant_auto_pay = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('auto_pay');

	        // ENDPOINT TO RETRIVE PAYMETHOD OBJECT FORTE
	        $pay_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/paymethods/' . $tenant_payment_id;
	        // ENDPOINT TO RETRIVE CUSTOMER'S BILLING ADDRESS
	        $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $tenant_customer_id . '/addresses/' . $tenant_address_id;

	        // Payment Method Info
	        $ch = $this->forteGet($pay_endpoint, $auth_token, $organization_id);
	        // Address Info
	        $ch1 = $this->forteGet($addr_endpoint, $auth_token, $organization_id);

	        $pay_response = curl_exec($ch);
	        $addr_response = curl_exec($ch1);
	        $info = curl_getinfo($ch);
	        curl_close($ch);
	        curl_close($ch1);
	        $payment_data = json_decode($pay_response);
	        $addr_data = json_decode($addr_response);

	        if ($tenant_customer_id == ""){

	            $tenant_payment_settings = "";
	        }
	        else {
	        	// Checks if Street Line 2 exists in the current Tenant's Address Object from Forte
	        	$st_line2_exist = property_exists($addr_data->physical_address, 'street_line2');
	        	$street_line2 = ($st_line2_exist != TRUE ? '':$addr_data->physical_address->street_line2);

	            $pay_settings_array = json_encode(array(
	                'account_holder'    => $payment_data->echeck->account_holder,
	                'bank_name'         => $payment_data->label,
	                'account_number'    => $payment_data->echeck->masked_account_number,
	                'routing_number'    => $payment_data->echeck->routing_number,
	                'account_type'      => $payment_data->echeck->account_type,
	                'street_line1'      => $addr_data->physical_address->street_line1,
	                'street_line2'      => $street_line2,
	                'city'              => $addr_data->physical_address->locality,
	                'state'             => $addr_data->physical_address->region,
	                'zip_code'          => $addr_data->physical_address->postal_code,
	                'auto_pay'          => $tenant_auto_pay
	            ));

	            // THE ACTUAL TENANT PAYMENT SETTINGS TO BE PASSED INTO THE VIEW
	            $tenant_payment_settings = json_decode($pay_settings_array);
	        }

	        return view('payment.tenantPaySettings', [
	            'pageTitle'         => 'Payment Settings',
	            'pageHeading'       => 'Payment Settings',
	            'pageMenuName'      => 'tenantPaymentSettings',
	            'profile'           => $tenantProfile,
	            'imageUrl'          => $imageUrl,
	            'paymentSettings'   => $tenant_payment_settings
	            //'isTenantAssociatedWithLandlord' => $tenantAssociatedWithLandlord

	        ]);
    	}
    }
    // Post to Web DB
    public function createTenantPaySettingsDB($pay_settings, $billing_address)
    {
    	// WE GOTTA ENCRYPT THE SENSITIVE SHIT
        $loggedTenant           = Auth::user('tenant');
        $loggedTenantId         = $loggedTenant->id;
        $payment_count 			= $this->getTenantCurrentRemainingPayments($loggedTenantId);

        $currentLandlordOrgId = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
        $currentPropertyLocId = $this->getTenantCurrentPropertyLocationId($loggedTenantId);

        // Defining new Tenant Payment Setting Model / Object
        $tenant_pay_settings                = TenantPaymentSetting::firstOrNew(array('tenant_id' => $loggedTenantId));
        /***** Defining the attributes of the new Tenant Payment Setting Model ******/
        $tenant_pay_settings->customer_id   						= Crypt::encrypt($pay_settings['customer_token']);
        $tenant_pay_settings->payment_id    						= Crypt::encrypt($pay_settings['payment_token']);
        $tenant_pay_settings->addr_id       						= Crypt::encrypt($pay_settings['address_token']);
        $tenant_pay_settings->current_landlord_org_id 				= Crypt::encrypt($currentLandlordOrgId);
        $tenant_pay_settings->current_property_loc_id				= Crypt::encrypt($currentPropertyLocId);
        $tenant_pay_settings->auto_pay      						= $pay_settings['auto_pay'];
        $tenant_pay_settings->ready_pay     						= $pay_settings['ready_pay'];
        $tenant_pay_settings->payments_remaining 					= $payment_count;

        // Save to the table of Tenant Payment Setting Model, save() will put in the tenant_id automatically due to the Belongs To Relationship TenantPaymentSetting Model has to the Tenant Model
        $tenant_pay_settings->save();

        // Create a new billing address for Tenant (Table tenant_billing_address)
        $tenant_billing_addr_settings 		= TenantBillingAddress::firstOrNew(array('tenant_id' => $loggedTenantId));
        $tenant_billing_addr_settings->st_line_1 		= $billing_address['street_line1'];
        $tenant_billing_addr_settings->st_line_2 		= $billing_address['street_line2'];
        $tenant_billing_addr_settings->locality 		= $billing_address['locality'];
        $tenant_billing_addr_settings->region 			= $billing_address['region'];
        $tenant_billing_addr_settings->postal_code 		= $billing_address['postal_code'];
        $tenant_billing_addr_settings->save();
    }

    // Update tenant pay settings table in Web DB
    public function updateTenantPaySettingsDB($pay_settings)
    {
    	$loggedTenant 	= Auth::user('tenant');
    	$loggedTenantId = $loggedTenant->id;
    	$auto_pay = $pay_settings['auto_pay'];
    	$ready_pay = ($auto_pay == 1 ? 1 : 0); // Might change the logic behind ready pay
    	DB::table('forte_tenant_pay_settings')
    				->where('tenant_id', $loggedTenantId)
    				->update(['auto_pay' => $auto_pay, 'ready_pay' => $ready_pay]);

    	// Also Update Tenant Billing Address if it is changed

    }
    // This function is used when Tenant switches Landlords and need to update their payment settings record in the tenant forte payment table
    public function updateNewTenantPaySettingsDB($pay_settings)
    {
    	$loggedTenant = Auth::user('tenant');
    	$loggedTenantId = $loggedTenant->id;
    	$payment_count 			= $this->getTenantCurrentRemainingPayments($loggedTenantId);
    	$currentLandlordOrgId = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
    	$currentPropertyLocId = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
    	$auto_pay = $pay_settings['auto_pay'];
    	$ready_pay = ($auto_pay == 1 ? 1 : 0);
    	$new_forte_tenant_pay_settings = TenantPaymentSetting::where('tenant_id',$loggedTenantId)->first();
    	$new_forte_tenant_pay_settings->customer_id 						= Crypt::encrypt($pay_settings['customer_token']);
    	$new_forte_tenant_pay_settings->payment_id    						= Crypt::encrypt($pay_settings['payment_token']);
        $new_forte_tenant_pay_settings->addr_id       						= Crypt::encrypt($pay_settings['address_token']);
        $new_forte_tenant_pay_settings->current_landlord_org_id 			= Crypt::encrypt($currentLandlordOrgId);
        $new_forte_tenant_pay_settings->current_property_loc_id 			= Crypt::encrypt($currentPropertyLocId);
        $new_forte_tenant_pay_settings->auto_pay      						= $pay_settings['auto_pay'];
        $new_forte_tenant_pay_settings->ready_pay     						= $pay_settings['ready_pay'];
        $new_forte_tenant_pay_settings->payments_remaining 					= $payment_count;
        $new_forte_tenant_pay_settings->save();
    }
    // CREATE REQUEST TO FORTE REGARDING TENANT CUSTOMER/PAY SETTINGS
    public function createTenantPaySettingsForte($tenant_pay_params)
    {
        $loggedTenant           = Auth::user('tenant');
        $loggedTenantId         = $loggedTenant->id;
        // $current_landlord_location_id = $this->getTenantCurrentLandlordLocationId($loggedTenantId);
        $base_url   		= env('FORTE_BASE_URL');
        $organization_id 	= $this->getTenantCurrentLandlordOrgId($loggedTenantId);
        $location_id 		= $this->getTenantCurrentPropertyLocationId($loggedTenantId);
        $api_access_id 		= env('FORTE_API_ACCESS_ID');
        $api_secure_key 	= env('FORTE_API_SECURE_KEY');
        $auth_token     	= base64_encode($api_access_id . ':' . $api_secure_key);
        // Endpoint to create the Tenant as a payinng Customer with payment method set-up in Forte
        $payment_setting_endpoint 			= $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/';

        $ch = $this->fortePost($payment_setting_endpoint, $tenant_pay_params, $auth_token, $organization_id);

        $response = curl_exec($ch);
    		$info = curl_getinfo($ch);
    		curl_close($ch);
    		$data = json_decode($response);
    		$create_http_success = (($info['http_code'] != 201) ? 0:1);

    		$createOutput = array(
    			'create_http_success'	=> $create_http_success,
    			'data'					=> $data
		    );

    		// Array Containing the Created Customer Object info and the HTTP Code needed to see if successful or not
    		return $createOutput;
    }

    // UPDATE REQUEST TO FORTE REGARDING PAY SETTINGS
    public function updateTenantPaySettingsForte($tenant_payment_settings)
    {
    	$loggedTenant           = Auth::user('tenant');
        $loggedTenantId         = $loggedTenant->id;
        // $current_landlord_location_id = $this->getTenantCurrentLandlordLocationId($loggedTenantId);
        $base_url   = env('FORTE_BASE_URL');
        $organization_id = $this->getTenantCurrentLandlordOrgId($loggedTenantId);
        $location_id = $this->getTenantCurrentPropertyLocationId($loggedTenantId);
        $api_access_id = env('FORTE_API_ACCESS_ID');
        $api_secure_key = env('FORTE_API_SECURE_KEY');
        $auth_token     = base64_encode($api_access_id . ':' . $api_secure_key);

        $tenant_customer_id_pre = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('customer_id');
        $tenant_customer_id = Crypt::decrypt($tenant_customer_id_pre);
        $tenant_payment_id_pre = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('payment_id');
        $tenant_payment_id = Crypt::decrypt($tenant_payment_id_pre);
        $tenant_address_id_pre = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('addr_id');
        $tenant_address_id = Crypt::decrypt($tenant_address_id_pre);
        $tenant_auto_pay = DB::table('forte_tenant_pay_settings')->where('tenant_id', $loggedTenantId)->value('auto_pay');

        // ENDPOINT TO RETRIVE PAYMETHOD OBJECT FORTE
        $pay_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/paymethods/' . $tenant_payment_id;
        // ENDPOINT TO RETRIVE CUSTOMER'S BILLING ADDRESS
        $addr_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $tenant_customer_id . '/addresses/' . $tenant_address_id;

        // Check if the account number has '*' in the string, because that would signify that the user most likely didn't update their account number
        if(strpos($tenant_payment_settings['account_number'],'*') != false)
        {
            // If '*' is not found in account number from view, use this array
            $echeck = array(
                'account_holder' => $tenant_payment_settings['account_holder'],
                'routing_number' => $tenant_payment_settings['routing_number'],
                'account_number' => $tenant_payment_settings['account_number'],
                'account_type'   => $tenant_payment_settings['account_type']
            );
        }
        else
        {
            $echeck = array(
                'account_holder'    => $tenant_payment_settings['account_holder'],
                'routing_number'    => $tenant_payment_settings['routing_number'],
                'account_type'      => $tenant_payment_settings['account_type']
            );
        }

        $pay_params = array(
        		'label'		=> $tenant_payment_settings['label'],
        		'echeck'	=> $echeck
        	);

        $addr_params = array(
        		'physical_address' => array(
        			'street_line1' 	=> $tenant_payment_settings['street_line1'],
        			'street_line2' 	=> $tenant_payment_settings['street_line2'],
        			'locality'		=> $tenant_payment_settings['locality'],
        			'region'		=> $tenant_payment_settings['region'],
        			'postal_code'	=> $tenant_payment_settings['postal_code']
        		)
        );

        $ch = $this->forteUpdate($pay_endpoint, $pay_params, $auth_token, $organization_id);
        $ch1 = $this->forteUpdate($addr_endpoint, $addr_params, $auth_token, $organization_id);

        $response = curl_exec($ch);
        $response1 = curl_exec($ch1);
        $info = curl_getinfo($ch);
        $info1 = curl_getinfo($ch1);
        curl_close($ch);
        curl_close($ch1);
        $data = json_decode($response);
        $data1 = json_decode($response1);

        // If either request does not have a HTTP Code of 200 (Success), http_success = 0, which will prompt an error message in the main function
        $http_success = (($info['http_code'] != 200) || ($info1['http_code'] != 200) ? 0:1);
        return $http_success;
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
