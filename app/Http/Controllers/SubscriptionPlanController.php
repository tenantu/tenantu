<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\SubscriptionPlan;
use Auth;
use DB;
use Redirect;

class SubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function subscriptionPlanIndex()
    {
        $adminUser = Auth::User('admin');
        if(empty($adminUser)){
            return redirect()->route('admins.getLogin');
        }

        $subscriptionPlanQuery = DB::table('subscription_plans')->select('id', 'name', 'initial_base_price', 'additional_accounts_base_price', 'price_per_property', 'property_max', 'management_account_max', 'landlord_credit_multiplier', 'tenant_credit_multiplier', 'available_status');

        $subscription_plans = $subscriptionPlanQuery->paginate(10);

        return view('admins.subscriptionPlanIndex', [
            'pageTitle' => 'Subscription Plans',
            'pageDesc'  => 'TenantU Subscription Plans',
            'subscription_plans' => $subscription_plans
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSubscriptionPlan()
    {
        //
        $adminUser = Auth::User('admin');
        if(empty($adminUser)){
            return redirect()->route('admins.getLogin');
        }

        return view('admins.createSubscriptionPlan', [
            'pageTitle' => 'Create Subscription Plan',
            'pageDesc'  => 'Create Subscription Plan Here',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeSubscriptionPlan(Request $request)
    {
        //
        $subscription_plan = new SubscriptionPlan;
        $subscription_plan->name = $request->name;
        $subscription_plan->initial_base_price = $request->initial_base_price;
        $subscription_plan->price_per_property = $request->price_per_property;
        $subscription_plan->property_max = $request->property_max;
        $subscription_plan->management_account_max = $request->management_account_max;
        $subscription_plan->num_management_accounts_pre_new_base_price = $request->num_management_accounts_pre_new_base_price;
        $subscription_plan->additional_accounts_base_price = $request->additional_accounts_base_price;
        $subscription_plan->landlord_credit_multiplier = $request->landlord_credit_multiplier;
        $subscription_plan->tenant_credit_multiplier = $request->tenant_credit_multiplier;
        $subscription_plan->available_status = $request->available_status;
        $subscription_plan->save();

        return redirect()->route('admins.subscriptionPlanIndex')->with('message-success', 'New Subscription Plan Created!');

    }

    public function changeAvailability($id)
    {
        $adminUser = Auth::User('admin');
        if(empty($adminUser)){
            return redirect()->route('admins.getLogin');
        }

        $subscription_plan = SubscriptionPlan::find($id);
        if ($subscription_plan->available_status)
        {
            $data['available_status'] = 0;
        }
        else
        {
            $data['available_status'] = 1;
        }

        $subscription_plan->update($data);

        return redirect()->route('admins.subscriptionPlanIndex')->with('message', 'Subscription Plan Availability Updated');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
