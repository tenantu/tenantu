<?php

/**
 * global constants
 */
define('DS', DIRECTORY_SEPARATOR);

/**
 * this function helps in debugging data
 */
function pr($data = null) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

/**
 * this function helps in setting active menu links for a view
 */
function setActive($path = null) {
    return Request::is($path) ? 'class=active' : '';
}

/**
 * this function helps in setting active sub-menu links for a view
 */
function setActiveList($path = null) {
    return Request::is($path . "/*") ? 'class=active' : '';
}
// Find ip address
function IPv6() {
	$IPv6 = '';
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $IPv6 = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $IPv6 = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $IPv6 = $_SERVER['REMOTE_ADDR'];
    }
    return $IPv6;
}