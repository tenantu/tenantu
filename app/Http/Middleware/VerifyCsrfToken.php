<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'landlords/property/upload',
        'tenants/changerent',
        'tenants/updateDocData',
        'tenants/updateItineraryData',
        'tenants/updateItineraryData',
        'property/saveImage',
        'property/deleteImage',
        'property/deleteImageDB',
        'property/setImageFeatured',
        'property/saveDoc',
        'property/deleteDoc',
        'property/deleteDocDB',
        'payment/updatePayment',
        'admin/payment/getTenantFromLandlord',
        'property/fetchAmenities',
        '/advertisement',
        '/paypal_payment_notify',
        'landlords/featuredproperties',
        '/featured_paypal_payment_notify',
        'admin/getPropertyLandlord'
    ];

}
