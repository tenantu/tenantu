<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Redirect;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
        //dd(Auth::user('landlord'));
	if(Route::getFacadeRoot()->current()->uri() == "search"){
	    if((!empty(Auth::user('landlord')) && Auth::user('landlord')->is_active == 1)||(!empty(Auth::user('tenant')) && Auth::user('tenant')->is_active == 1)){
	    return $next($request);
	    }
	    else{
	    return Redirect::route('index.signup')->with('message-success', 'You must be logged in to view listings, sign-up below!');
;
	    }
	}
        else if( !empty(Auth::user('landlord')) && Auth::user('landlord')->is_active == 1 ){
            return redirect()->guest('landlords/dashboard');    
        }
        else if( !empty(Auth::user('tenant')) && Auth::user('tenant')->is_active == 1 ){
            return Redirect::route('tenants.dashboard'); 
        }
        else{
            return $next($request);
        }
        
    }
}
