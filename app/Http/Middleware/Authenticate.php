<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
        if (session_id() == '') {
            session_start();
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

		$currentAction = \Route::currentRouteAction();
		list($controller, $method) = explode('@', $currentAction);
		$controller = preg_replace('/.*\\\/', '', $controller);
		if($controller=='TenantsController'){
			$tenantUser = Auth::User('tenant');
			if(empty($tenantUser)){
				return redirect()->guest('/login');
			}
			else{
				return $next($request);
			}
		}
		if($controller=='LandlordsController'){
			$landlordUser = Auth::User('landlord');
			$adminUser = Auth::User('admin');
			if(empty($landlordUser)){
				return redirect()->guest('/login');
			}

			else{
				return $next($request);
			}

		}
    if($controller=='LandlordPropertyManagementGroupsController'){
      $landlordUser = Auth::User('landlord');
      $adminUser = Auth::User('admin');
      if(empty($landlordUser)){
        return redirect()->guest('/login');
      }
      else{
        return $next($request);
      }
    }
    if($controller=='LandlordApplicationController'){
      $landlordUser = Auth::User('landlord');
      $adminUser = Auth::User('admin');
      if(empty($landlordUser)){
        return redirect()->guest('/login');
      }
      else{
        return $next($request);
      }
    }
    if($controller=='LandlordRentPaymentHistoryController'){
			$landlordUser = Auth::User('landlord');
			$adminUser = Auth::User('admin');
			if(empty($landlordUser)){
				return redirect()->guest('/login');
			}

			else{
				return $next($request);
			}

		}
        if($controller=='TenantCreatePayCustomerController'){
            $tenantUser = Auth::User('tenant');
            if(empty($tenantUser)){
                return redirect()->guest('/login');
            }
            else{
                return $next($request);
            }
        }
        if($controller=='TenantPayController'){
            $tenantUser = Auth::User('tenant');
            if(empty($tenantUser)){
                return redirect()->guest('/login');
            }
            else{
                return $next($request);
            }
        }
		if($controller=='MysearchController'){

			$tenantUser = Auth::User('tenant');
			if(empty($tenantUser)){
				return redirect()->guest('/login');
			}else{
				return $next($request);
			}
		}
		if($controller=='PropertiesController'){
			$tenantUser   = Auth::User('tenant');
			$landlordUser = Auth::User('landlord');
			if(empty($tenantUser) && empty($landlordUser) && (!$request->ajax())){
				return redirect()->guest('/login');
			}else{
				return $next($request);
			}



		}

		if($controller=='PaymentsController'){

			$tenantUser   = Auth::User('tenant');
			$landlordUser = Auth::User('landlord');
			if(empty($tenantUser) && empty($landlordUser)){
				return redirect()->guest('/login');
			}else{
				return $next($request);
			}
		}

		if($controller=='AdminsController'){

			$adminUser   = Auth::User('admin');
			if(empty($adminUser)){
				return redirect()->guest('admin/login');
			}else{
				return $next($request);
			}
		}

		if($controller=='FeaturedPropertiesController'){

			$landlordUser   = Auth::User('landlord');
			if(empty($landlordUser)){
				return redirect()->guest('/login');
			}else{
				return $next($request);
			}
		}

    if($controller=='TenantMaintenanceRequestController')
    {
      $tenantUser = Auth::User('tenant');
      if(empty($tenantUser))
      {
        return redirect()->guest('/login');
      }
      else
      {
        return $next($request);
      }
    }

    if($controller=='ApplicationController') {
      $tenant = Auth::User('tenant');
      $landlord = Auth::User('landlord');
      if((empty($tenant)) and (empty($landlord)) ){
        return redirect()->guest('/login');
      }
      else{
        return $next($request);
      }
    }

    if($controller=='LandlordMaintenanceRequestController')
    {
      $landlordUser = Auth::User('landlord');
      if(empty($landlordUser))
      {
        return redirect()->guest('/login');
      }
      else
      {
        return $next($request);
      }
    }

    if($controller=='LandlordSubscriptionPaymentController')
    {
        $landlordUser = Auth::User('landlord');
        if(empty($landlordUser))
        {
            return redirect()->guest('/login');
        }
        else
        {
            return $next($request);
        }
    }
    if($controller=='LandlordSubscriptionController')
    {
        $landlordUser = Auth::User('landlord');
        if(empty($landlordUser))
        {
            return redirect()->guest('/login');
        }
        else
        {
            return $next($request);
        }
    }

        if($controller=='LandlordSubscriptionPaymentSettingsController')
        {
            $landlordUser = Auth::User('landlord');
            if(empty($landlordUser))
            {
                return redirect()->guest('/login');
            }
            else
            {
                return $next($request);
            }
        }





		if (Auth::guest()) {

			return redirect()->guest('login');
		}
			 return $next($request);

	}


}
