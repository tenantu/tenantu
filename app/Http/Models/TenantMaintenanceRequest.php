<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TenantMaintenanceRequest extends Model
{
    protected $table = 'maintenance_request';

    protected $fillable = ['severity', 'issue_type_id', 'property_id', 'issue_desc', 'date_encountered'];


    //DEFINE RELATIONSHIPS

    public function property()
    {
        return $this->belongsTo('App\Http\Models\Property');
    }

}
