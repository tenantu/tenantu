<?php

namespace App\Http\Models;
require('vendor/autoload.php');
use Illuminate\Database\Eloquent\Model;

class DwollaPay extends Model
{
    // Test API
    $apiClient = new DwollaSwagger\ApiClient("https://api-uat.dwolla.com/");
    // We need this to do 
    $customersApi = new DwollaSwagger\CustomersApi($apiClient);
    // Get amount of rent owed for each payment period (monthly, quarterly, etc)
    public function getRentAmt() {
    
    }

    public function getPayPeriod() {
        
    }

    public function getTenant() {
        return $this->belongsTo('App\Http\Models\Tenant');
    }

    public function getLandlord() {
        return $this->belongsTo('App\Http\Models\Landlord');
    }

    public function createAccount() {
        $tenantDwolla = new DwollaSwagger\CreateCustomer();
        $tenantDwolla->firstName = '';
        $tenantDwolla->lastName = '';
        $tenantDwolla->email = '';
        $tenantDwolla->phone = '';    
        
        $makeAccount = $customersApi.create($tenantDwolla);
    }

    public function executePay() {
        // 
    }
}
