<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class Feedback extends Model
{
    protected $table = 'feedbacks';
    
    protected $fillable = ['client_name','company_name','description','status'];
}
