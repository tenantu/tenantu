<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class ApplicationSubmission extends Model
{
  protected $table = 'application_submission';

  //STATUS FIELD:
  //0->rejected by landlord, 1->submitted, 2->approved by landlord, 3->tenant agrees to sign lease, 4->lease signed
  protected $fillable = ['tenant_id', 'property_id', 'application_group_id', 'status', 'active'];

  public function tenant() {
    return $this->belognsTo('App\Http\Models\Tenant');
  }
  public function propertyPhysical() {
    return $this->belongsTo('App\Http\Models\PropertyPhysical', 'id');
  }

  public function applicationGroup() {
    return $this->hasOne('App\Http\Models\ApplicationGroup');
  }

}
