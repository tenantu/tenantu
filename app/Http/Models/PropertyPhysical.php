<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class PropertyPhysical extends Model implements  SluggableInterface
{
    use sluggableTrait;
    protected $table = 'property_physical';

    protected $fillable = ['id', 'property_name', 'property_type', 'description', 'landlord_id', 'school_id', 'active',
        'bedroom_no', 'bathroom_no','max_occupancy','country','administrative_area','sub_administrative_area','locality', 'postal_code','thoroughfare','premise','latitude','longitude',
        'distance_from_school','amenities', 'apartment_building_id', 'slug'];

    protected $sluggable = [
        'build_from' => 'thoroughfare',
        'save_to' => 'slug',
    ];
//    public function sluggable() {
//
//    }

    public function apartment_building() {
        return $this->belongsTo('App\Http\Models\ApartmentBuilding');
    }

    public function property_meta()
    {
        return $this->hasOne('App\Http\Models\PropertyMeta', 'property_physical_id');
    }
    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord', 'landlord_id');
    }
    public function school()
    {
        return $this->belongsTo('App\Http\Models\School', 'school_id');
    }
    public function message_threads()
    {
        return $this->hasMany('App\Http\Models\MessageThread');
    }
    public function property_comments()
    {
        return $this->hasMany('App\Http\Models\PropertyComment');
    }
    public function property_docs()
    {
        return $this->hasMany('App\Http\Models\PropertyDoc', 'property_id');
    }
    public function property_images()
    {
        return $this->hasMany('App\Http\Models\PropertyImage', 'property_id');
    }
    public function property_amenities()
    {
        return $this->hasMany('App\Http\Models\PropertyAmenity', 'property_id');
    }
    public function property_ratings()
    {
        return $this->hasMany('App\Http\Models\PropertyRating');
    }
    public function applicationSubmissions() {
        return $this->hasMany('App\Http\Models\ApplicationSubmission', 'property_id');
    }

}
