<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class landlordTenantEmailInvite extends Model
{
    protected $table = 'landlord_tenant_email_invites';

    protected $fillable = ['id','landlord_id','tenant_invite_email'];


}

