<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BannerSetting extends Model
{
    protected $table = 'banner_settings';
    protected $fillable = ['banner_name','banner_size','banner_price','status','width','height','description'];
}
