<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyViewed extends Model
{
    //
    
    protected $table = 'property_viewed';
    
    protected $fillable = ['tenant_id','property_id','session_id'];
}
