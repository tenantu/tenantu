<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class PropertyMeta extends Model
{
    protected $primaryKey = 'property_physical_id';

    protected $table = 'property_meta';

    protected $fillable = ['property_physical_id', 'rent_price', 'charge_per_bed','expireon', 'status', 'communication_medium', 'slug', 'search_field',
    'most_viewed', 'featured','most_reviewed','next_year_availability_status','availability_status','rating_1','rating_2', 'rating_3','rating_4','rating_avg','recommended_flag','featured_start',
    'featured_end','suggested_property','landlord_prop_mgmt_id'];

    //protected $touches = ['landlordSubscription'];


    public function property_physical()
    {
        return $this->belongsTo('App\Http\Models\PropertyPhysical', 'property_physical_id');
    }

    public function property_comments()
    {
        return $this->hasMany('App\Http\Models\PropertyComment');
    }
    public function property_docs()
    {
        return $this->hasMany('App\Http\Models\PropertyDoc');
    }
    public function property_images()
    {
        return $this->hasMany('App\Http\Models\PropertyImage', 'property_id');
    }
    public function property_amenities()
    {
        return $this->hasMany('App\Http\Models\PropertyAmenity', 'property_id');
    }

}
