<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class ApplicationGroupTenant extends Model
{
  protected $table = 'application_group_tenant';

  //STATUS FIELD:
  //0-> rejected, 1->pending, 2->confirmed
  protected $fillable = ['tenant_id', 'tenant_email', 'application_group_id', 'status'];

}
