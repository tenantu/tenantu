<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordTenant extends Model
{
    //
    protected $table = 'landlord_tenants';
	
    protected $fillable = ['landlord_id','tenant_id','property_id','month_rent_initial', 'due_on','agreement_start_date','cycle', 'agreement_end_date', 'active'];
    
    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord');
    }
    public function tenant()
    {
        return $this->belongsTo('App\Http\Models\Tenant');
    }
    public function property_physical()
    {
        return $this->belongsTo('App\Http\Models\PropertyPhysical', 'property_id');
    }
    public function landlord_tenant_docs()
    {
        return $this->hasMany('App\Http\Models\landlordTenantDoc');
    }
    public function landlord_tenant_monthlyrents()
    {
        return $this->hasMany('App\Http\Models\landlordTenantMonthlyrent');
    }
    public function landlord_tenant_otherpayments()
    {
        return $this->hasMany('App\Http\Models\landlordTenantOtherpayment');
    }
    public function landlord_tenant_otherpayment_details(){
        return $this->hasManyThrough('App\Http\Models\landlordTenantOtherpaymentDetail', 'App\Http\Models\landlordTenantOtherpayment');
    }
    
}
