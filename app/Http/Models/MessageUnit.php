<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'unit_message';

    protected $fillable = ['message', 'landlord_id','mgmt_acct_id', 'created_at','updated_at'];

}
