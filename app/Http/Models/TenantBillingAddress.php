<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TenantBillingAddress extends Model
{
    //
    protected $table = 'tenant_billing_address';

    protected $fillable = ['tenant_id','st_line_1','st_line_2','locality', 'region', 'postal_code'];
    
    protected $touches = ['tenant'];

    public function tenant()
    {
        return $this->belongsTo('App\Http\Models\Tenant');
    }


}
