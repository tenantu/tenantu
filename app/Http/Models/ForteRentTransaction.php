<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ForteRentTransaction extends Model
{
    protected $table = 'forte_rent_transactions';

    protected $fillable = ['landlord_tenant_id', 'landlord_tenant_monthlyrents_unique_id', 'transaction_token','date_due', 'authorized_amount', 'status', 'date_received', 'payment_is_late' ];

    public function landlord_tenant()
    {
    	return $this->belongsTo('App\Http\Models\LandlordTenant');
    }
}
