<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    protected $table = 'amenities';
    
    protected $fillable = ['amenity_name','status'];

    
}
