<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;


class ForteLandlordSubscriptionPaySettings extends Model
{
    protected $table = 'forte_landlord_subscription_pay_settings';

    protected $fillable = ['landlord_id', 'customer_id', 'paymethod_id', 'addr_id', 'plan_type', 'tenantu_location_id'];

    protected $touches = ['landlord'];

    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord');
    }
}