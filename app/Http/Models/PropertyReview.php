<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyReview extends Model
{
    protected $table = 'property_review';
    
    protected $fillable = ['property_id','title','review','tenant_id','status','created_at','updated_at'];

    public function tenant()
    {
        return $this->belongsTo('App\Http\Models\Tenant');
    }
    public function property()
    {
        return $this->belongsTo('App\Http\Models\Property');
    }
   
}
