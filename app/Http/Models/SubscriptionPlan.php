<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    //
    protected $table = 'subscription_plans';

    protected $fillable = ['name', 'forte_fee_id', 'initial_base_price', 'price_per_property', 'property_max','management_account_max', 'additional_accounts_base_price', 'num_management_accounts_pre_new_base_price', 'landlord_credit_multiplier', 'tenant_credit_multiplier', 'num_property_for_discount', 'num_management_account_for_discount', 'min_property_price', 'min_management_account_price', 'percent_discount_property', 'percent_discount_management_account', 'available_status'];
    
}
