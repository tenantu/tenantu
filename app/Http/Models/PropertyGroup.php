<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyGroup extends Model
{
    //
    protected $table = 'property_groups';
    protected $fillable = ['landlord_id', 'landlord_property_management_acct_id', 'active_status', 'property_total'];

    public function landlord()
    {
    	return $this->belongsTo('App\Http\Models\Landlord');
    }
    public function property()
    {
    	return $this->hasMany('App\Http\Models\Property');
    }
    public function landlordPropertyManagementGroup()
    {
    	return $this->belongsTo('App\Http\Models\landlordPropertyManagementGroup');
    }
}
