<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyEnquiry extends Model
{
     protected $table = 'property_enquiry';
    
    protected $fillable = ['property_id','tenant_id','tenant_phone','message','created_at','updated_at'];
}
