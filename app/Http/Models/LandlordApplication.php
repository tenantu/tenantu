<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordApplication extends Model
{
    //
    protected $table = 'forte_landlord_applications';

    protected $fillable = ['name', 'landlord_id', 'application_id', 'status'];


}
