<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class landlordTenantOtherpaymentDetail extends Model
{
    protected $table = 'landlord_tenant_otherpayment_details';
	
    protected $fillable = ['landlord_tenant_otherpayment_id','bill_on','title', 'amount'];
    
    public function landlord_tenant_otherpayment()
    {
        return $this->belongsTo('App\Http\Models\landlordTenantOtherpayment');
    }
}
