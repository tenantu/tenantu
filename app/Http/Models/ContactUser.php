<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUser extends Model
{
    protected $table = 'contact_users';
    
    protected $fillable = ['name','email','school_id','reason','message'];
}
