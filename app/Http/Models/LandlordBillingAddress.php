<?php
/**
 * Created by PhpStorm.
 * User: alexcarr
 * Date: 1/9/17
 * Time: 2:22 PM
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordBillingAddress extends Model
{
    protected $table = 'landlord_billing_addresses';

    protected $fillable = ['landlord_id', 'street_line_1', 'street_line_2', 'locality', 'region', 'postal_code'];

}