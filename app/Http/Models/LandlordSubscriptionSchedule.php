<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordSubscriptionSchedule extends Model
{
    //
    protected $table = 'landlord_subscription_schedules';

    protected $fillable = ['schedule_id', 'landlord_subscription_id', 'schedule_frequency', 'schedule_created_date', 'reference_id', '', 'active_status'];

    protected $touches = ['landlordSubscription'];

    public function landlordSubscription()
    {
    	return $this->belongsTo('App\Http\Models\LandlordSubscription');
    }
}
