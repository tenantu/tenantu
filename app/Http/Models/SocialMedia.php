<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $table = 'social_links';
    
    protected $fillable = ['twitter','facebook','instagram'];
}
