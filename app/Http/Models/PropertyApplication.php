<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class PropertyApplication extends Model
{
  protected $table = 'property_application';

  protected $fillable = ['property_id', 'accepting_applications'];

  public function Property() {
    return $this->hasOne('App\Http\Models\Property');
  }

}
