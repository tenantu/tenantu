<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyDoc extends Model
{
    protected $table = 'property_docs';
	
    protected $fillable = ['property_id','doc_name','status','doc_type'];

    // DEFINE RELATIONSHIPS
    public function properties()
    {
        return $this->belongsTo('App\Http\Models\Property');
    }
}
