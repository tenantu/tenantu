<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordSubscription extends Model
{
    //
    protected $table = 'landlord_subscription';

    protected $fillable = ['landlord_id', 'landlord_subscription_schedule_id', 'subscription_plan_id', 'monthly_payment_amount','payment_frequency', 'management_group_count','property_count', 'active_status', 'join_date', 'cancel_date'];

    protected $touches = ['landlord'];

    public function landlord()
    {
    	return $this->belongsTo('App\Http\Models\Landlord');
    }
}
