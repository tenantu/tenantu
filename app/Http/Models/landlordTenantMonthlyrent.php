<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class landlordTenantMonthlyrent extends Model
{
    protected $table = 'landlord_tenant_monthlyrents';
	
    protected $fillable = ['landlord_tenant_id','bill_on','amount'];
    
    public function landlord_tenant()
    {
        return $this->belongsTo('App\Http\Models\LandlordTenant');
    }
}
