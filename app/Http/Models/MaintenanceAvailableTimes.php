<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MaintenanceAvailableTimes extends Model
{
    protected $table = 'maintenance_available_times';

    protected $fillable = ['maintenance_request_id', 'time_available', 'agree_status'];


    //DEFINE RELATIONSHIPS

    public function property()
    {
        return $this->belongsTo('App\Http\Models\Property');
    }

}
