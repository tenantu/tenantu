<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyAmenity extends Model
{
    protected $table = 'property_amenities';
	
    protected $fillable = ['property_id','amenity'];

    // DEFINE RELATIONSHIPS
    public function properties()
    {
        return $this->belongsTo('App\Http\Models\Property');
    }
}
