<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class ApplicationQuestionTenantResponse extends Model
{
  protected $table = 'application_question_tenant_responses';

  protected $fillable = ['tenant_id', 'question_id', 'response'];

  public function Question() {
    return $this->belongsTo('App\Http\Models\ApplicationQuestion');
  }

}
