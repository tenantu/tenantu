<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordPropertyManagementAccount extends Model
{
    protected $table = 'landlord_property_management_accounts';
    protected $fillable = ['landlord_id', 'label', 'loc_id', 'property_count'];

    public function landlord()
    {
    	return $this->belongsTo('App\Http\Models\Landlord');
    }
    public function propertyGroup()
    {
    	return $this->hasMany('App\Http\Models\PropertyGroup');
    }
    public function property()
    {
    	return $this->hasMany('App\Http\Models\Property');
    }


}
