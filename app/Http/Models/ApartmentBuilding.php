<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class ApartmentBuilding extends Model implements SluggableInterface
{
    use sluggableTrait;

    protected $table = 'apartment_building';

    protected $fillable = ['id', 'building_name', 'landlord_id', 'slug'];

    protected $sluggable = [
        'build_from' => 'building_name',
        'save_to' => 'slug',
    ];

    public function property_physical() {
        return $this->hasMany('App\Http\Models\PropertyPhysical', 'apartment_building_id');
    }
}
