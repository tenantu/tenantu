<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordPaymentReceiveSetting extends Model
{
    //
    protected $table = 'forte_landlord_pay_settings_rent';
	
    protected $fillable = ['landlord_id', 'org_id', 'late_fee_percent', 'schedule_frequency', 'rent_due_day_of_month', 'late_grace_period_day'];
    
    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord');
    }
    
    
}
