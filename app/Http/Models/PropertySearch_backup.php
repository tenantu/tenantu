<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertySearch extends Model
{
    protected $table = 'property_search';
    
    protected $fillable = ['tenant_id','landlord_id','school_id','bedrooms','distance','price_form','price_to','search','sortkey','session_id'];
}
