<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class landlordTenantOtherpayment extends Model
{
    protected $table = 'landlord_tenant_otherpayments';
	
    protected $fillable = ['landlord_tenant_id','title','agreement_start_date','cycle','due_on','initial_amount'];
    
    public function landlord_tenant()
    {
        return $this->belongsTo('App\Http\Models\LandlordTenant');
    }
    public function landlord_tenant_otherpayment_details()
    {
        return $this->hasMany('App\Http\Models\landlordTenantOtherpaymentDetail');
    }
}
