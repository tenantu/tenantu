<?php

namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, SluggableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['role_id', 'name', 'slug', 'email', 'password', 'image', 'gender', 'description', 'dob', 'location',
        'personal_link', 'is_active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $sluggable = [
        'build_from' => 'name',
        'save_to' => 'slug',
    ];

    // DEFINE RELATIONSHIPS
    public function role()
    {
        return $this->belongsTo('App\Http\Models\Role');
    }
    public function competition_winners()
    {
        return $this->hasMany('App\Http\Models\CompetitionWinner');
    }
    public function competitions()
    {
        return $this->hasMany('App\Http\Models\Competition');
    }
    public function forums()
    {
        return $this->hasMany('App\Http\Models\Forum');
    }
    public function likes()
    {
        return $this->hasMany('App\Http\Models\Like');
    }
    public function posts()
    {
        return $this->hasMany('App\Http\Models\Post');
    }
    public function user_followers()
    {
        return $this->hasMany('App\Http\Models\UserFollower');
    }
    public function user_points()
    {
        return $this->hasMany('App\Http\Models\UserPoint');
    }
    public function forum_comments()
    {
        return $this->hasMany('App\Http\Models\ForumComment', 'from_user_id');
    }
    public function post_views()
    {
        return $this->hasMany('App\Http\Models\PostView');
    }

}