<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class RatingUser extends Model
{
    protected $table = 'rating_user';
    
    protected $fillable = ['rating_id','property_id','tenant_id','review_id','rate_value'];
}
