<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class ApplicationGroup extends Model
{
  protected $table = 'application_group';

  //STATUS FIELD:
  //0->rejected by landlord, 1->submitted, 2->approved by landlord, 3->tenant agrees to sign lease, 4->lease signed
  protected $fillable = ['group_name', 'creator_id'];

  public function members() {
    return $this->belongsToMany('App\Http\Models\Tenant')->withPivot('status');
  }

  public function creator() {
    return $this->belongsTo('App\Http\Models\Tenant');
  }

  public function applicationSubmission() {
    return $this->hasMany('App\Http\Models\ApplicationSubmission');
  }

}
