<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class preEvApplications extends Model
{
    protected $table = 'pre_ev_applications';

    protected $fillable = ['Birthdate','LastName','FirstName','SchoolId', 'SchoolName'];


}

