<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentSetting extends Model
{
    //
    protected $table = 'payment_settings';
	
    protected $fillable = ['landlord_id','type','paypal_id','bank_name', 'account_no','unique_code','first_name', 'last_name', 'address'];
    
    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord');
    }

}
