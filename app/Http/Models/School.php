<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    
    protected $table = 'schools';
    
    protected $fillable = ['schoolname','schoolid','location','address','is_active','latitude','longitude','image'];
    
    
    public function tenants()
    {
        return $this->hasMany('App\Http\Models\Tenant','school_id');
    }
}
