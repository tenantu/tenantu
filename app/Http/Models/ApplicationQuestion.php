<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;



class ApplicationQuestion extends Model
{
  protected $table = 'application_questions';

  protected $fillable = ['question', 'category', 'required', 'data_entry_type'];

  public function QuestionResponses() {
    return $this->hasMany('App\Http\Models\ApplicationQuestionTenantResponse', 'question_id');
  }

}
