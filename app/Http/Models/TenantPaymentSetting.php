<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TenantPaymentSetting extends Model
{
    //
    protected $table = 'forte_tenant_pay_settings';

    protected $fillable = ['customer_id','tenant_id','payment_id','addr_id','current_landlord_org_id', 'current_property_loc_id', 'auto_pay', 'late_pay', 'ready_pay', 'payments_remaining'];
    
    protected $touches = ['tenant'];

    public function tenant()
    {
        return $this->belongsTo('App\Http\Models\Tenant');
    }


}

?>
