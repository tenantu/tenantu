<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LandlordSubscriptionScheduleItem extends Model
{
    //
    protected $table = 'landlord_subscription_scheduleitems';

    protected $fillable = ['landlord_subscription_schedule_id', 'scheduleitem_token', 'subscription_transaction_token', 'amount', 'status', 'schedule_item_date', 'schedule_item_processed_date', 'schedule_item_created_date', 'schedule_item_description'];

    protected $touches = ['landlordSubscriptionSchedule'];

    public function landlordSubscriptionSchedule()
    {
    	return $this->belongsTo('App\Http\Models\LandlordSubscriptionSchedule');
    }
}
