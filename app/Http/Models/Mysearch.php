<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Mysearch extends Model
{
    protected $table = 'mysearches';
    
    protected $fillable = ['tenant_id','landlord_id','school_id','bedrooms','distance','price_form','price_to','search','aminities','sortkey'];

    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord');
    }
    public function tenant()
    {
        return $this->belongsTo('App\Http\Models\Tenant');
    }
}
