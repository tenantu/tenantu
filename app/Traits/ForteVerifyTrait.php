<?php

namespace App\Traits;
use DB;
use Crypt;
use Config;


trait ForteVerifyTrait
{
    // Grab the customer ID of the Landlord that subscribed
    public function verifyLandlordLocationId($landlordId, $location_id)
    {
        $base_url             = Config::get('constants.FORTE_BASE_URL');
        $organization_id = $this->getLandlordOrgId($landlordId);
        $api_access_id        = env('FORTE_API_ACCESS_ID');
        $api_secure_key       = env('FORTE_API_SECURE_KEY');
        $auth_token           = base64_encode($api_access_id . ':' . $api_secure_key);
        $test_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/transactions';

        $test_verify = $this->forteGet($test_endpoint, $auth_token, $organization_id);
        $response = curl_exec($test_verify);
        $info = curl_getinfo($test_verify);
        curl_close($test_verify);
        $data = json_decode($response);

        if($info['http_code'] != 200)
        {
            $verify_success = 0;
        }
        else
        {
            $verify_success = 1;
        }

        return $verify_success;

    }


}