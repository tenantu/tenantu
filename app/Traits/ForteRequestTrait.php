<?php

namespace App\Traits;

trait ForteRequestTrait
{
	//////////////////////////////////////////////////////////////////////////
    // 							FORTE REST FUNCTIONS 						//
    //////////////////////////////////////////////////////////////////////////
    public function forteUpdate($endpoint, $params, $auth_token, $organization_id)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }

    public function forteGet($endpoint, $auth_token, $organization_id)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }


    public function fortePost($endpoint, $params, $auth_token, $organization_id)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));     //Disable this line for GET's and DELETE's
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }

    public function forteDelete($endpoint, $auth_token)
    {
        $ch = curl_init($endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');     //POST, GET, PUT or DELETE (Create, Find, Update or Delete)
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth_token,
            'X-Forte-Auth-Organization-id: ' . $organization_id,
            'Accept:application/json',
            'Content-type: application/json'
        ));

        return $ch;
    }
}