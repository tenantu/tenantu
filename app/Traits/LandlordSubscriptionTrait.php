<?php

namespace App\Traits;
use DB;
use Crypt;
use Config;


trait LandlordSubscriptionTrait
{
    // Grab the customer ID of the Landlord that subscribed
    public function getLandlordCustomerId($landlordId)
    {
        $customer_id = DB::table('forte_landlord_subscription_pay_settings')
            ->where('landlord_id', $landlordId)
            ->value('customer_id');
        if($customer_id == '')
        {
            $current_customer_id = null;
        }
        else
        {
            try{
                $current_customer_id = Crypt::decrypt($customer_id);
            } catch (DecryptException $e){
                echo "Can't get the Id";
            }
        }
        return $current_customer_id;
    }

    private function schedule_subscription_pay($subscription_pay_params, $customer_token)
    {
      $base_url           	= Config::get('constants.FORTE_BASE_URL');
      $organization_id    	= env('FORTE_TENANTU_MERCHANT_ORG_ID');
      $location_id        	= env('FORTE_TENANTU_MERCHANT_LOC_ID');
      $api_access_id      	= env('FORTE_TENANTU_MERCHANT_API_ACCESS_ID');
      $api_secure_key     	= env('FORTE_TENANTU_MERCHANT_API_SECURE_KEY');
      $auth_token         	= base64_encode($api_access_id . ':' . $api_secure_key);

      $pay_schedule_endpoint          = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules';

      // This posts the rent payment schedule to Forte
      $schedule_subscription_pay  = $this->fortePost($pay_schedule_endpoint, $subscription_pay_params, $auth_token, $organization_id);

      $schedule_pay_response = curl_exec($schedule_subscription_pay);
      $schedule_pay_info     = curl_getinfo($schedule_subscription_pay);
      curl_close($schedule_subscription_pay);
      $schedule_data = json_decode($schedule_pay_response);
      $create_http_success = (($schedule_pay_info['http_code'] != 201) ? 0:1);

      $createOutput = json_encode(array(
        'create_http_success'   => $create_http_success,
        'data'                  => $schedule_data
      ));

      return $createOutput;
    }

    public function updateLandlordSubscriptionAmountForte($new_amount, $landlordId)
    {
        $loggedLandlord = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $location_id = env('FORTE_TENANTU_MERCHANT_LOC_ID');
        $organization_id = env('FORTE_TENANTU_MERCHANT_ORG_ID');
        $customer_token = $this->getLandlordCustomerId($landlordId);
        $schedule_id = $this->getLandlordSubscriptionScheduleId($landlordId);

        $update_schedule_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules/' . $schedule_id;

        $schedule_params = array(
            'schedule_amount' => $new_amount
        );

        $amount_ch = $this->forteUpdate($update_schedule_endpoint, $schedule_params, $organization_id);

        $response = curl_exec($amount_ch);
        $info = curl_getinfo($amount_ch);
        curl_close($amount_ch);
        $data = json_decode($response);

        $http_success = (($info['http_code'] != 200) || ($info1['http_code'] != 200) ? 0:1);
        return $http_success;

    }

    public function updateLandlordSubscriptionStatusForte($status, $organization_id, $schedule_id)
    {
        $update_schedule_endpoint = $base_url . '/organizations/' . $organization_id . '/locations/' . $location_id . '/customers/' . $customer_token .'/schedules/' . $schedule_id;

        $schedule_params = array(
            'schedule_status' => $status
        );

        $status_ch = $this->forteUpdate($update_schedule_endpoint, $schedule_params, $organization_id);

        $response = curl_exec($status_ch);
        $info = curl_getinfo($status_ch);
        curl_close($status_ch);
        $data = json_decode($response);

        $http_success = (($info['http_code'] != 200) || ($info1['http_code'] != 200) ? 0:1);
        return $http_success;
    }
    
    public function calculateTotalSubscriptionPaymentAmount($subscription_plan_id, $property_count, $management_account_count)
    {
        
        $price_per_property = $this->getSubscriptionPlanPropertyPrice($subscription_plan_id);
        $base_price = $this->getSubscriptionPlanInitialBasePrice($subscription_plan_id);
        $additional_price_management_accounts = $this->getSubscriptionPlanAdditionalBasePrice($subscription_plan_id);
        $property_total_monthly = $price_per_property * $property_count;
        if($management_account_count < 2)
        {
            $management_account_total_monthly = $base_price + ($additional_price_management_accounts * ($management_account_count));
        }
        else
        {
            $management_account_total_monthly = $base_price + ($additional_price_management_accounts * ($management_account_count - 1));
        }
        $total_monthly_price = $property_total_monthly + $management_account_total_monthly;
        
        return floatval($total_monthly_price);
    }

}