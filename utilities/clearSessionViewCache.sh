
#remove sessions + views directories recursively
echo "Removing session and view cache...";
rm -rf ../storage/framework/sessions&&
rm -rf ../storage/framework/views&&

#make the directories again
mkdir ../storage/framework/sessions&&
mkdir ../storage/framework/views&&

#chmod the newly created directories
chmod 777 -R ../storage/framework/sessions&&
chmod 777 -R ../storage/framework/views&&
echo "Session and view cache successfully cleared."
exit 1;
