<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('role_id')->unsigned()->index('role_id');
			$table->string('name');
			$table->string('slug');
			$table->string('email');
			$table->string('password');
			$table->string('image');
			$table->char('gender', 1);
			$table->text('description', 65535);
			$table->date('dob');
			$table->string('location');
			$table->string('personal_link');
			$table->string('remember_token');
			$table->string('password_token');
			$table->boolean('is_active')->default(0)->comment('1= true, 0=false');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
	}

}
