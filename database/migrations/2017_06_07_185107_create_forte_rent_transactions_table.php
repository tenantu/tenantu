<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForteRentTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forte_rent_transactions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_tenant_id');
			$table->string('landlord_tenant_monthlyrents_unique_id');
			$table->text('transaction_token', 65535);
			$table->date('date_due');
			$table->decimal('authorized_amount', 4);
			$table->string('status', 30)->nullable();
			$table->dateTime('date_posted');
			$table->date('date_received')->nullable();
			$table->date('date_funded')->nullable();
			$table->string('response_code', 4);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forte_rent_transactions');
	}

}
