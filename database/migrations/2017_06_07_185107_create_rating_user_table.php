<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatingUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rating_user', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('rating_id');
			$table->integer('property_id');
			$table->integer('tenant_id');
			$table->integer('review_id');
			$table->integer('rate_value');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rating_user');
	}

}
