<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_groups', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('landlord_id');
			$table->integer('landlord_property_management_acct_id');
			$table->boolean('active_status');
			$table->integer('property_total');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_groups');
	}

}
