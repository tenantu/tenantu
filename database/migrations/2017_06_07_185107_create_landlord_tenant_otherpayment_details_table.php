<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordTenantOtherpaymentDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_tenant_otherpayment_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_tenant_otherpayment_id');
			$table->date('bill_on');
			$table->string('title', 500);
			$table->float('amount', 10);
			$table->string('unique_id', 200);
			$table->string('transaction_id', 500);
			$table->boolean('status');
			$table->dateTime('payment_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_tenant_otherpayment_details');
	}

}
