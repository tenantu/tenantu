<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlords', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('firstname');
			$table->string('lastname');
			$table->integer('school_id');
			$table->string('slug');
			$table->string('email');
			$table->string('password');
			$table->string('image');
			$table->char('gender', 1);
			$table->text('description', 65535);
			$table->date('dob');
			$table->string('location');
			$table->string('phone', 22);
			$table->text('about', 65535);
			$table->string('website');
			$table->string('state');
			$table->string('address');
			$table->string('language');
			$table->string('city');
			$table->string('remember_token');
			$table->string('password_token');
			$table->string('activation_token');
			$table->boolean('is_active')->default(0)->comment('1= true, 0=false');
			$table->integer('active_properties')->default(0);
			$table->integer('fb_id');
			$table->integer('is_fb_user')->default(0);
			$table->integer('is_normal_user')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlords');
	}

}
