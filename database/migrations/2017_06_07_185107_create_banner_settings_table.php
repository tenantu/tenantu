<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannerSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banner_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('banner_name');
			$table->string('banner_size');
			$table->decimal('banner_price', 10);
			$table->integer('status');
			$table->string('sample_image');
			$table->string('width', 50);
			$table->string('height', 50);
			$table->text('description', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banner_settings');
	}

}
