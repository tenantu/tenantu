<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaintenanceAvailableTimesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maintenance_available_times', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('maintenance_request_id');
			$table->dateTime('time_available');
			$table->boolean('agree_status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maintenance_available_times');
	}

}
