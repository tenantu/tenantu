<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyMetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_meta', function(Blueprint $table) {
            $table->integer('property_physical_id');
            $table->integer('rent_price');
            $table->boolean('charge_per_bed')->default(0);
            $table->date('expireon');
            $table->integer('status')->default(1)->comment('0=>deactive,1=>active');
            $table->string('communication_medium', 5)->comment('W=>Web, E=>Email');
            $table->string('slug', 3000);
            $table->text('search_field', 65535)->comment('This fileds is a collection of all the fields used for search');
            $table->integer('most_viewed')->default(0)->nullable();
            $table->boolean('featured')->default(0)->nullable()->comment('0=> not featured, 1=>featured properties');
            $table->integer('most_reviewed')->default(0)->nullable();
            $table->string('availability_status')->default('Available');
            $table->integer('rating_1');
            $table->integer('rating_2');
            $table->integer('rating_3');
            $table->integer('rating_4');
            $table->integer('rating_avg');
            $table->integer('recommended_flag')->default(0)->nullable()->comment('0 =>not recommented, 1=> recommented');
            $table->dateTime('featured_start')->nullable();
            $table->dateTime('featured_end')->nullable();
            $table->boolean('suggested_property')->default(0)->nullable()->comment('0 = Not Suggested, 1 = Suggested');
            $table->integer('landlord_prop_mgmt_id')->nullable();
            $table->timestamps();

            $table->foreign('property_physical_id')->references('id')->on('property_physical');
        });
        DB::statement('CREATE INDEX search_field ON property_meta (search_field(255));');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_meta');
    }
}
