<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnitMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unit_message', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('message', 65535);
			$table->integer('landlord_id');
			$table->integer('mgmt_acct_id');
			$table->boolean('read')->comment('0->unread, 1->read');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unit_message');
	}

}
