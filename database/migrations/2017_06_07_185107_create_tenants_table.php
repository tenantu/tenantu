<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTenantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tenants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('fb_id');
			$table->string('firstname');
			$table->string('lastname');
			$table->integer('school_id');
			$table->string('slug');
			$table->string('email');
			$table->string('password');
			$table->string('image');
			$table->char('gender', 1);
			$table->text('description', 65535);
			$table->date('dob');
			$table->string('location');
			$table->string('phone', 22);
			$table->text('about', 65535);
			$table->string('website');
			$table->string('state');
			$table->string('address');
			$table->string('language');
			$table->string('city');
			$table->string('remember_token');
			$table->string('password_token');
			$table->string('activation_token');
			$table->boolean('is_active')->default(0)->comment('1= true, 0=false');
			$table->integer('is_fb_user')->default(0);
			$table->integer('is_normal_user')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tenants');
	}

}
