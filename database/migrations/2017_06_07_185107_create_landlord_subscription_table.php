<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordSubscriptionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_subscription', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('landlord_id');
			$table->text('landlord_subscription_schedule_id', 65535);
			$table->integer('subscription_plan_id');
			$table->decimal('monthly_payment_amount', 6);
			$table->integer('payment_frequency')->default(0);
			$table->integer('management_group_count')->default(0);
			$table->integer('property_count')->default(0);
			$table->boolean('active_status')->default(0);
			$table->date('join_date');
			$table->date('cancel_date')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_subscription');
	}

}
