<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForteLandlordSubscriptionPaySettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forte_landlord_subscription_pay_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_id');
			$table->text('customer_id', 65535);
			$table->text('payment_id', 65535);
			$table->text('addr_id', 65535);
			$table->boolean('plan_type');
			$table->text('tenantu_location_id', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forte_landlord_subscription_pay_settings');
	}

}
