<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBetaCodeListTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('beta_code_list', function(Blueprint $table)
		{
			$table->string('beta_email', 89)->nullable();
			$table->string('beta_code', 10)->default('')->primary();
			$table->boolean('code_activated')->default(0);
			$table->integer('view_count')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('beta_code_list');
	}

}
