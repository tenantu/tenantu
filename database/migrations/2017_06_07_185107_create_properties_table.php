<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('properties', function(Blueprint $table)
		{
			$table->integer('id', true);//done
			$table->string('property_name');//done
			$table->boolean('property_type')->default(0)->comment('0 = Apartment, 1 = House');//done
			$table->text('description', 65535);//done
			$table->integer('landlord_id');//done
			$table->integer('school_id');//done
			$table->boolean('active');//done
			$table->integer('bedroom_no');//done
			$table->integer('bathroom_no');//done
			$table->string('location');//decomposed, done


			$table->integer('rent_price');//done
			$table->date('expireon');//done
			$table->decimal('distance_from_school', 10);//done
			$table->integer('status')->default(1)->comment('0=>deactive,1=>active');//done
			$table->string('communication_medium', 5)->comment('W=>Web, E=>Email');//done
			$table->string('slug', 3000);//done
			$table->text('search_field', 65535)->comment('This fileds is a collection of all the fields used for search');//done
			$table->string('amenities', 3000);//done
			$table->string('latitude', 100);//done
			$table->string('longitude', 100);//done

			$table->integer('most_viewed')->default(0);//done
			$table->boolean('featured')->default(0)->comment('0=> not featured, 1=>featured properties');//done
			$table->integer('most_reviewed')->default(0);//done
			$table->string('availability_status')->default('Available');//done
			$table->integer('rating_1');//done
			$table->integer('rating_2');//done
			$table->integer('rating_3');//done
			$table->integer('rating_4');//done
			$table->integer('rating_avg');//done
			$table->integer('recommented_flag')->default(0)->comment('0 =>not recommented, 1=> recommented');//done
			$table->dateTime('featured_start');//done
			$table->dateTime('featured_end');//done
			$table->boolean('suggested_property')->default(0)->comment('0 = Not Suggested, 1 = Suggested');//done
			$table->integer('landlord_prop_mgmt_id')->nullable();//done
			$table->timestamps();
		});
        DB::statement('CREATE INDEX search_field ON properties (search_field(255));');
    }


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('ALTER TABLE properties DROP INDEX search_field');
		Schema::drop('properties');
	}

}
