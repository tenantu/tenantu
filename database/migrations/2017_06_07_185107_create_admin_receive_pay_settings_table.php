<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminReceivePaySettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_receive_pay_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('loc_id', 65535);
			$table->decimal('tenant_sub_fee', 3)->default(1.25);
			$table->integer('landlord_cc_count');
			$table->integer('landlord_ach_count');
			$table->integer('tenant_sub_count');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_receive_pay_settings');
	}

}
