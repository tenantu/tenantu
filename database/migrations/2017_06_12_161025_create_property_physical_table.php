<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyPhysicalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('property_physical', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('property_name');
            $table->boolean('property_type')->default(0)->comment('0 = Apartment, 1 = House');
            $table->text('description', 65535);
            $table->integer('landlord_id');
            $table->integer('school_id');
            $table->boolean('active');
            $table->integer('bedroom_no');
            $table->integer('bathroom_no');
            $table->string('country');
            $table->string('administrative_area')->comment('state/province/region');
            $table->string('sub_administrative_area')->comment('county/district')->nullable();
            $table->string('locality')->comment('city/town');
            $table->string('postal_code')->comment('zip code');
            $table->string('thoroughfare')->comment('street address');
            $table->string('premise')->comment('apartment number, suite number, box number')->nullable();
            //do we need latitude/longitude?
            $table->string('latitude', 100)->nullable();
            $table->string('longitude', 100)->nullable();
            $table->decimal('distance_from_school', 10)->nullable();
            $table->string('amenities', 3000);
            $table->integer('number_of_parking_spots');
            $table->boolean('off_street_parking');
            $table->boolean('utilities_included');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_physical');
    }
}
