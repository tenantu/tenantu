<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageThreadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message_threads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('thread_id');
			$table->integer('property_id');
			$table->integer('landlord_id');
			$table->integer('tenant_id');
			$table->integer('duration')->comment('specific task duration in days');
			$table->integer('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message_threads');
	}

}
