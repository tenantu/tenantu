<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForteLandlordPaySettingsRentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forte_landlord_pay_settings_rent', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('landlord_id');
			$table->text('org_id', 65535);
			$table->integer('late_fee_percent');
			$table->integer('schedule_frequency');
			$table->integer('rent_due_day_of_month');
			$table->integer('late_grace_period_day');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forte_landlord_pay_settings_rent');
	}

}
