<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyExpressInterestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_express_interest', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_id');
			$table->integer('tenant_id');
			$table->integer('landlord_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_express_interest');
	}

}
