<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMaintenanceRequestTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('maintenance_request', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('severity');
			$table->integer('issue_type_id');
			$table->integer('property_id');
			$table->string('issue_desc', 1000)->nullable();
			$table->dateTime('date_encountered');
			$table->integer('status')->default(0);
			$table->dateTime('date_completed')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('maintenance_request');
	}

}
