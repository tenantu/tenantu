<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordPropertyManagementAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_property_management_accounts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('landlord_id');
			$table->text('account_name', 65535);
			$table->text('loc_id', 65535);
			$table->integer('property_count')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_property_management_accounts');
	}

}
