<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordTenantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_tenants', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_id');
			$table->integer('tenant_id');
			$table->integer('property_id');
			$table->float('month_rent_initial', 10);
			$table->integer('due_on');
			$table->date('agreement_start_date');
			$table->string('cycle', 50);
			$table->date('agreement_end_date');
			$table->boolean('active')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_tenants');
	}

}
