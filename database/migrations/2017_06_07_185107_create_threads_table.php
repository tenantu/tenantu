<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateThreadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('threads', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('thread_name', 3000);
			$table->integer('status')->default(1)->comment('1 =>active and 0=> deactive');
			$table->integer('flag')->comment('0 =>general and 1=>connected tenants and landlords');
			$table->string('image');
			$table->integer('order_status')->comment('order by is taking ');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('threads');
	}

}
