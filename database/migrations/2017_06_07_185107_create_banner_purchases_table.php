<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannerPurchasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banner_purchases', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bannerId');
			$table->string('name');
			$table->string('phone', 20);
			$table->string('address', 2000);
			$table->string('banner_name');
			$table->decimal('banner_price', 10);
			$table->string('banner_url', 500);
			$table->string('purchase_date_from_paypal', 200);
			$table->integer('status');
			$table->string('payment_status');
			$table->string('image');
			$table->decimal('amount_paid', 10);
			$table->string('txn_id', 200);
			$table->date('activated_date');
			$table->date('deactivated_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banner_purchases');
	}

}
