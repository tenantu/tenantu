<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordSubscriptionSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_subscription_schedules', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('schedule_id', 65535);
			$table->integer('landlord_subscription_id');
			$table->string('schedule_frequency');
			$table->date('schedule_created_date');
			$table->string('reference_id');
			$table->boolean('active_status')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_subscription_schedules');
	}

}
