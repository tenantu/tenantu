<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForteLandlordSubscriptionTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forte_landlord_subscription_transactions', function(Blueprint $table)
		{
			$table->integer('id');
			$table->integer('landlord_id');
			$table->text('customer_id', 65535);
			$table->text('transaction_token', 65535)->nullable();
			$table->decimal('authorized_amount', 5)->nullable();
			$table->string('status', 30);
			$table->dateTime('date_posted');
			$table->date('date_received');
			$table->date('date_funded');
			$table->string('response_code', 4);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forte_landlord_subscription_transactions');
	}

}
