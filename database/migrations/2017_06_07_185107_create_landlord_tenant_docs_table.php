<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordTenantDocsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_tenant_docs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_tenant_id');
			$table->string('doc_title', 500);
			$table->text('doc_description', 65535);
			$table->string('file_name', 500);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_tenant_docs');
	}

}
