<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordTenantMonthlyrentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_tenant_monthlyrents', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_tenant_id');
			$table->string('bill_on', 50);
			$table->float('amount', 10);
			$table->string('unique_id', 200);
			$table->string('transaction_id', 500)->nullable();
			$table->boolean('status')->comment('0 => Not Paid(Paynow buton),1 => Paid, 2=>due');
			$table->dateTime('payment_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_tenant_monthlyrents');
	}

}
