<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionPlansTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subscription_plans', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('forte_fee_id');
			$table->decimal('initial_base_price', 5);
			$table->decimal('price_per_property', 4);
			$table->integer('property_max');
			$table->integer('management_account_max');
			$table->decimal('additional_accounts_base_price', 5);
			$table->integer('num_management_accounts_pre_new_base_price');
			$table->decimal('landlord_credit_multiplier', 4)->default(0.00);
			$table->decimal('tenant_credit_multiplier', 4)->default(0.00);
			$table->integer('num_property_for_discount')->nullable();
			$table->integer('num_management_account_for_discount')->nullable();
			$table->decimal('min_property_price', 4)->nullable();
			$table->decimal('min_management_account_price', 4)->nullable();
			$table->integer('percent_discount_property')->nullable();
			$table->integer('percent_discount_management_account')->nullable();
			$table->boolean('available_status')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subscription_plans');
	}

}
