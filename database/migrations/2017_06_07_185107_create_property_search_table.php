<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertySearchTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_search', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tenant_id');
			$table->integer('landlord_id');
			$table->integer('school_id');
			$table->string('bedrooms', 3);
			$table->integer('distance');
			$table->integer('price_form');
			$table->integer('price_to');
			$table->string('search');
			$table->string('aminities', 3000);
			$table->string('sortkey', 50);
			$table->string('session_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_search');
	}

}
