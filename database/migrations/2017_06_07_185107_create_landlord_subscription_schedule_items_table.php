<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordSubscriptionScheduleItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_subscription_schedule_items', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('landlord_subscription_schedule_id');
			$table->text('scheduleitem_token', 65535);
			$table->text('subscription_transaction_token', 65535);
			$table->decimal('amount', 5);
			$table->string('status');
			$table->date('schedule_item_date');
			$table->date('schedule_item_processed_date');
			$table->date('schedule_item_created_date');
			$table->text('schedule_item_description', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_subscription_schedule_items');
	}

}
