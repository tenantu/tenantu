<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTenantBillingAddressTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tenant_billing_address', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tenant_id');
			$table->string('st_line_1', 50);
			$table->string('st_line_2', 30)->nullable();
			$table->string('locality', 100);
			$table->string('region', 3);
			$table->char('postal_code', 5);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tenant_billing_address');
	}

}
