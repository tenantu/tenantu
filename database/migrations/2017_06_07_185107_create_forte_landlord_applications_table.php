<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForteLandlordApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forte_landlord_applications', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('name', 65535);
			$table->integer('landlord_id');
			$table->text('application_id', 65535);
			$table->string('status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forte_landlord_applications');
	}

}
