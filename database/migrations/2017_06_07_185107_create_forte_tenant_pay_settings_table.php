<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForteTenantPaySettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forte_tenant_pay_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('customer_id', 65535);
			$table->integer('tenant_id');
			$table->text('payment_id', 65535);
			$table->text('addr_id', 65535);
			$table->text('current_landlord_location_id', 65535);
			$table->boolean('auto_pay')->default(0);
			$table->boolean('late_pay')->default(0);
			$table->integer('late_pay_count')->default(0);
			$table->boolean('ready_pay')->default(0);
			$table->integer('payments_remaining');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forte_tenant_pay_settings');
	}

}
