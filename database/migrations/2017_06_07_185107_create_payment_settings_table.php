<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_id');
			$table->integer('type')->comment('1=>Paypal,2=>Bank');
			$table->string('paypal_id', 100)->comment('Venmo email id');
			$table->string('bank_name', 100);
			$table->string('account_no', 100);
			$table->string('unique_code', 100);
			$table->string('first_name', 50);
			$table->string('last_name', 50);
			$table->string('address', 1000);
			$table->string('city', 500);
			$table->string('state', 500);
			$table->string('zip_code', 10);
			$table->date('dob');
			$table->string('braintree_SubMerchantAccountId', 500)->nullable();
			$table->integer('success');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_settings');
	}

}
