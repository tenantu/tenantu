<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMysearchesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mysearches', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('tenant_id');
			$table->integer('landlord_id');
			$table->integer('school_id');
			$table->integer('bedrooms');
			$table->float('distance', 10, 0);
			$table->float('price_form', 10, 0);
			$table->float('price_to', 10, 0);
			$table->string('search');
			$table->string('aminities', 3000);
			$table->string('sortkey', 50);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mysearches');
	}

}
