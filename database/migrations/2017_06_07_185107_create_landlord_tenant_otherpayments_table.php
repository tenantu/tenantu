<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandlordTenantOtherpaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('landlord_tenant_otherpayments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_tenant_id');
			$table->string('title', 3000);
			$table->float('initial_amount', 10);
			$table->integer('due_on');
			$table->date('agreement_start_date');
			$table->string('cycle', 50);
			$table->string('transaction_id', 500);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landlord_tenant_otherpayments');
	}

}
