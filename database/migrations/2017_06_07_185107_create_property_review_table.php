<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyReviewTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('property_review', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('property_id');
			$table->string('title');
			$table->text('review', 65535);
			$table->integer('tenant_id');
			$table->integer('status')->default(1)->comment('0=>not approved review, 1=>approved review');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('property_review');
	}

}
