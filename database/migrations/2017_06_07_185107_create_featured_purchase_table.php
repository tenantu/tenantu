<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeaturedPurchaseTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('featured_purchase', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('landlord_id');
			$table->integer('property_id');
			$table->integer('duration_id');
			$table->string('duration_name', 300);
			$table->decimal('actual_amount', 10);
			$table->string('txn_id', 200);
			$table->decimal('amount_paid', 10);
			$table->string('payment_status');
			$table->integer('status');
			$table->string('purchase_date_from_paypal', 200);
			$table->dateTime('featured_start');
			$table->dateTime('featured_end');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('featured_purchase');
	}

}
