<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('message_thread_id');
			$table->text('message', 65535);
			$table->integer('landlord_id');
			$table->integer('tenant_id');
			$table->char('sender', 1)->comment('T=>Tenant, L=>Landlord');
			$table->integer('read_flag')->default(0)->comment('0=>unread ,1=>read');
			$table->text('group')->comment('p => portfolio, u=>unit');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
