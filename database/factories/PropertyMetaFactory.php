<?php
/**
 * Created by PhpStorm.
 * User: alexcarr
 * Date: 6/13/17
 * Time: 12:59 PM
 */


$factory->define(App\Http\Models\PropertyMeta::class, function (Faker\Generator $faker) {
    return [
//        'property_physical_id' => function() {
//            return factory(App\Http\Models\PropertyPhysical::class)->create()->id;
//        },

        'property_physical_id' => factory(App\Http\Models\PropertyPhysical::class)->create()->id,
        'rent_price' => $faker->numberBetween($min = 0, $max = 4000),
        'charge_per_bed' =>$faker->randomElement([0, 1]),
        'expireon' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+ 1 year'),
        'status' => '1',
        'communication_medium' => $faker->randomElement(['E', 'W']),
        'slug' => $faker->slug,
        'search_field' => $faker->text,
        //some of these are probably not valid test values.
        'most_viewed' => NULL,
        'featured' => NULL,
        'most_reviewed' => NULL,
        'availability_status' => 'Available',
        'rating_1' => $faker->numberBetween($min = 0, $max = 4000),
        'rating_2' => $faker->numberBetween($min = 0, $max = 4000),
        'rating_3' => $faker->numberBetween($min = 0, $max = 4000),
        'rating_4' => $faker->numberBetween($min = 0, $max = 4000),
        'rating_avg' => $faker->numberBetween($min = 0, $max = 4000),
        'recommended_flag' => NULL,
        'featured_start' => NULL,
        'featured_end' => NULL,
        'suggested_property' => NULL,
        'landlord_prop_mgmt_id'=> NULL
    ];
});