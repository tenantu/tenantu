<?php
/**
 * Created by PhpStorm.
 * User: alexcarr
 * Date: 6/13/17
 * Time: 12:28 PM
 */

    $factory->define(App\Http\Models\PropertyPhysical::class, function (Faker\Generator $faker) {
        return [
            'property_name' => $faker->text,
            'property_type' => $faker->numberBetween(0, 1),
            'description' => $faker->paragraphs(3, true),
            'landlord_id' => 68,
            'school_id' => 1,
            'active' => 1,
            'bedroom_no' => $faker->numberBetween(0, 10),
            'bathroom_no' => $faker->numberBetween(0, 10),
            'country' => 'US',
            'administrative_area' => $faker->state,
            'sub_administrative_area' => NULL,
            'locality' => $faker->city,
            'postal_code' => $faker->postcode,
            'thoroughfare' => $faker->streetAddress,
            'premise' => NULL,
            'latitude' => NULL,
            'longitude' => NULL,
            'distance_from_school' => NULL,
            'amenities' => $faker->randomElement($array = array('parking', 'fireplace', 'basement', 'working sinks')),
            'distance_from_school' => $faker->numberBetween(0, 15)

        ];
    });

