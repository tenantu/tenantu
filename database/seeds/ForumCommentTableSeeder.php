<?php

use Illuminate\Database\Seeder;
use App\Http\Models\ForumComment;

class ForumCommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 100;
 
        $this->command->info('Deleting existing Forum Comment table ...');
        DB::table('forum_comments')->delete();
 
        $faker = Faker\Factory::create('en_GB');
 
        $faker->addProvider(new Faker\Provider\en_GB\Address($faker));
        $faker->addProvider(new Faker\Provider\en_GB\Internet($faker));
        $faker->addProvider(new Faker\Provider\Uuid($faker));
 
        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);
 
        for( $x=0 ; $x<$count; $x++ )
        {
        	$user = App\Http\Models\User::select('id')->where('is_active', '=', 1)->orderByRaw("RAND()")->first();
        	$forum = App\Http\Models\Forum::select('id')->where('is_active', '=', 1)->orderByRaw("RAND()")->first();
            $forumComment = ForumComment::create([
                'from_user_id' => $user->id,
                'forum_id' =>  $forum->id,
                'comment' => '<p>'.  implode('</p><p>', $faker->paragraphs(5)) .'</p>',
                'is_active' => $faker->randomElement($array = array ('0','1')),
                'is_published' => $faker->randomElement($array = array ('0','1')),
            ]);
            
        }
 
        $this->command->info('Forum Comment table seeded...');
    }
}
