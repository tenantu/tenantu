<?php
//namespace App;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;


class PropertySeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        $properties_physical = factory(App\Http\Models\PropertyPhysical::class, 50)->make();
        $properties_meta = factory(App\Http\Models\PropertyMeta::class, 50)->create();

//        $meta_properties = factory(App\Http\Models\PropertyMeta::class, 100)->create()->each(function($meta_property) {
//            $meta_property->attach()
//        });
        //factory(App\Http\Models\PropertyMeta::class, 100)->create();

        $this->command->info('Property tables seeded using Faker ...');
    }

}