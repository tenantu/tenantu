<?php
//use HTML;
use Carbon\Carbon;
use App\Http\Models\PaymentSetting;
use App\Http\Models\TenantPaymentSetting;
use App\Http\Models\LandlordPaymentReceiveSetting;
use App\Http\Models\TenantMaintenanceRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;


HTML::macro('getTenantProfileImage', function( $tenantId=null, $profileImage=null )
{
	try {
		$tenantId = Crypt::decrypt($tenantId);
	} catch (DecryptException $e) {
		//tenantId isnt encrypted
	}
	$tenantImage='';
	if( $tenantId=='' && $profileImage=='' )
	{

		if(Auth::user('tenant'))
		{
			$tenantImage = Auth::user('tenant')->image;
		}
		else
		{
			$tenantImage='';
		}
	}
	elseif($tenantId!='')
	{
		$objTenant = DB::table('tenants')
            ->select('image')
            ->where('id', '=', $tenantId)
            ->get();
		if (!empty($objTenant)) {
		//dd($tenantId);
          $tenantImage =  $objTenant[0]->image;
		}
		else {
			$tenantImage = '';
		}
	}
    elseif($profileImage!='')
	{
		$landlordImage= $profileImage;
	}
		$originalProfile='';
				if($tenantImage!='')
				{
					$imageUrl=asset('public/uploads/tenants/');
					$originalProfile=$imageUrl."/".$tenantImage;
				}
				else
				{
					$imageUrl=asset('public/uploads/noimage/');
					$originalProfile=$imageUrl."/tenant-blank.png";
				}
				return $originalProfile;
});

HTML::macro('getLandlordProfileImage', function($landlordId=null,$profileImage=null)
{
	try {
		$landlordId = Crypt::decrypt($landlordId);
	} catch (DecryptException $e) {
		//landlordId isn't encrypted, procceed
	}

	$landlordImage='';
	if($landlordId=='' && $profileImage=='')
	{
		if(Auth::user('landlord'))
		{
			$landlordImage = Auth::user('landlord')->image;
		}
		else
		{
			$landlordImage='';
		}
	}

	elseif($landlordId!='')
	{
		$objLandlord = DB::table('landlords')
            ->select('image')
            ->where('id', '=', $landlordId)
            ->get();
          $landlordImage =  $objLandlord[0]->image;
	}
	elseif($profileImage!='')
	{
		$landlordImage= $profileImage;
	}

		$originalProfile='';
				if($landlordImage!='')
				{
					$imageUrl=asset('public/uploads/landlords/');
					$originalProfile=$imageUrl."/".$landlordImage;
				}
				else
				{
					$imageUrl=asset('public/uploads/noimage/');
					$originalProfile=$imageUrl."/landlord-blank.png";
				}
				return $originalProfile;
});

HTML::macro('getTenantMenuClass',function($pageName,$menuName){
	if($pageName==$menuName)
	{
		return 'active';
	}
	else
	{
		return '';

	}
	});


HTML::macro('getThreadMenuClass',function($messageThreadId,$currentThreadId){
	if($messageThreadId==$currentThreadId)
	{
		return 'active';
	}
	else
	{
		return '';
	}
	});


HTML::macro('getTenantMessageNotification',function(){
	$tenantId = Auth::user('tenant')->id;

	$nonReadMessages = DB::select('SELECT message,message_thread_id,firstname,lastname,m.created_at,l.image FROM messages m,landlords l WHERE m.landlord_id=l.id AND  tenant_id='.$tenantId.' AND sender="L"  AND read_flag=0 AND message_thread_id IN(SELECT MT.id FROM message_threads as MT,threads as T where MT.thread_id=T.id AND T.flag=0) order by m.created_at DESC');
	$countNonReadMessages = count($nonReadMessages);

	$messageNotification="<li class='dropdown messages-menu'>

                <a title='Messages' href='#' class='dropdown-toggle' data-toggle='dropdown'>
                  <i class='fa fa-envelope-o'></i>
                  <span class='label label-success'>".$countNonReadMessages ."</span>
                </a>
                <ul class='dropdown-menu'>";
                if($countNonReadMessages==0)
                {
                  $messageNotification.="<li class='header'>You have no messages</li>";
				}
				else
				{
					$messageNotification.="<li class='header'>You have ".$countNonReadMessages." messages</li>";
				}
                  $messageNotification.="<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class='menu'>";
                    $latesMessageThreadId='';

                  foreach($nonReadMessages as $key=>$nonReadMessage){
					  $profileImage=HTML::getLandlordProfileImage('',$nonReadMessage->image);
					  $url=URL::to('tenants/messagelist/'.Crypt::encrypt($nonReadMessage->message_thread_id));
					  $messageNotificationTime=HTML::getMessageNotificationDateTime($nonReadMessage->created_at);
					  $latesMessageThreadId=$nonReadMessages[0]->message_thread_id;
						//~ if($key==0)
						//~ {
							//~ $latesMessageThreadId=$nonReadMessage->message_thread_id;
						//~ }else
						//~ {
							//~ $latesMessageThreadId=$key;
						//~ }

					   $messageNotification.= "
						  <li>
							<a href='".$url."'>
							  <div class='pull-left'>
								<img src='".$profileImage."' class='img-circle' alt='User Image'>
							  </div>
							  <h4>
								".$nonReadMessage->firstname." ".$nonReadMessage->lastname."
								<small><i class='fa fa-clock-o'></i> ".$messageNotificationTime." </small>
							  </h4>
							  <p>".substr($nonReadMessage->message,0,20)."</p>
							</a>
						  </li>
						  ";
				}
				$urlAll=URL::to('tenants/messagelist/'.$latesMessageThreadId);

               $messageNotification.= " </ul> </li>";
               if($latesMessageThreadId!='')
               {
					$messageNotification.= " <li class='footer'><a href='".$urlAll."'>See All Messages</a></li>";
			   }
                $messageNotification.= "</ul></li>";

	return $messageNotification;
	});
HTML::macro('getLandLordGeneralMessageNotification',function(){
	$landlordId = Auth::user('landlord')->id;
	$nonReadMessages = DB::select('SELECT message,message_thread_id,firstname,lastname,m.created_at,t.image FROM messages m,tenants t, message_threads mt, threads th WHERE m.tenant_id=t.id AND m.message_thread_id=mt.id AND mt.thread_id=th.id AND m.landlord_id='.$landlordId.' AND sender="T"  AND m.read_flag=0 AND th.flag=0 order by m.created_at DESC ');
	//print_r($nonReadMessages);exit;


	$countNonReadMessages = count($nonReadMessages);
	//print HTML::getLandlordProfileImage('','a.jpg');
	$messageNotification="<li class='dropdown messages-menu'>

                <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                  <i class='fa fa-envelope-o'></i>
                  <span class='label label-success'>".$countNonReadMessages ."</span>
                </a>
                <ul class='dropdown-menu'>";
                if($countNonReadMessages==0)
                {
                  $messageNotification.="<li class='header'>You have no new messages</li>";
				}
				else
				{
					$messageNotification.="<li class='header'>You have ".$countNonReadMessages." new message(s)</li>";
				}
                  $messageNotification.="<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class='menu'>";
                    $latesMessageThreadId='';

                  foreach($nonReadMessages as $key=>$nonReadMessage){
					  $profileImage=HTML::getTenantProfileImage('',$nonReadMessage->image);
					  $url=URL::to('landlords/messagelist/'.Crypt::encrypt($nonReadMessage->message_thread_id));
					  $messageNotificationTime=HTML::getMessageNotificationDateTime($nonReadMessage->created_at);
					  $latesMessageThreadId   = $nonReadMessages[0]->message_thread_id;
						/*if($key==0)
						{
							$latesMessageThreadId=$nonReadMessage->message_thread_id;
						}else
						{
							$latesMessageThreadId=$key;
						}*/

					   $messageNotification.= "
						  <li>
							<a href='".$url."'>
							  <div class='pull-left'>
								<img src='".$profileImage."' class='img-circle' alt='User Image'>
							  </div>
							  <h4>
								".$nonReadMessage->firstname." ".$nonReadMessage->lastname."
								<small><i class='fa fa-clock-o'></i> ".$messageNotificationTime." </small>
							  </h4>
							  <p>".substr($nonReadMessage->message,0,20)."</p>
							</a>
						  </li>
						  ";
				}
				$urlAll=URL::to('landlords/messagelist/'.$latesMessageThreadId);

               $messageNotification.= " </ul> </li>";
               if($latesMessageThreadId!='')
               {
					$messageNotification.= " <li class='footer'><a href='".$urlAll."'>See All Messages</a></li>";
			   }
                $messageNotification.= "</ul></li>";

	return $messageNotification;
	});

HTML::macro('getMessageNotificationDateTime',function($dt){
		$actualTime				= '';
		$currentDateTime 		= Carbon::now();
		//echo $currentDateTime;exit;
		$currentDateTimeStamp 	= strtotime($currentDateTime);
		$messageDateTimeStamp 	= strtotime($dt);
		$timeDifference 		= $currentDateTimeStamp - $messageDateTimeStamp;

		if($timeDifference < 60)
		{
			 $actualTime = $timeDifference;
			 return $actualTime."Sec ago";
		}
		elseif($timeDifference > 60 && $timeDifference < 3600)
		{
			$actualTime = round($timeDifference/60);
			return $actualTime."Minute ago";
		}
		elseif($timeDifference >3600 && $timeDifference < 43200)
		{
			$actualTime = round($timeDifference/3600);
			return $actualTime."Hour ago";
		}
		elseif($timeDifference > 43200 && $timeDifference < 86400)
		{
			$actualTime = round($timeDifference/3600);
			return "Today";
		}
		elseif($timeDifference > 86400)
		{
			$dt = date('d M Y h:i:s a',strtotime($dt));
			return $dt;
		}

	});

// HTML::macro('getPropertyImageArray', function($propertyId, $folderName=null, $imageSize=null)
// {
//
// 	$customFolderName ='';
//
// 	$image ='';
// 	if(($folderName=='thumb')||($folderName=='square'))
// 	{
// 		$customFolderName="/".$folderName;
// 	}
// 	if($propertyId!='')
// 	{
// 		$objProperty = DB::table('property_images')
// 						->select('image_name')
// 						->where('property_id', '=', $propertyId)
// 						->where('featured','=',1)
// 						->get();
//
// 	}
//
// 	$imageNames = array();
//
// 	foreach($objProperty as $imageEntry) {
// 		array_push($imageNames, $imageEntry->image_name);
// 	}
//
// 	var_dump($objProperty);
//
// 	$originalPropertyImage='';
// 	if($imageNames!='')
// 	{
// 		$imageUrl=asset('public/uploads/properties/').$customFolderName;
// 		$originalPropertyImage=$imageUrl."/".$image;
// 	}
// 	else
// 	{
// 		$imageUrl=asset('public/uploads/noimage/');
// 		$originalPropertyImage=$imageUrl."/propert-blank".$imageSize.".png";//this image is used in different sizes
// 	}
//
// 	return $originalPropertyImage;
// });

//this is for taking only one image of the property
HTML::macro('getPropertyImage', function($propertyId=null,$propertyImage=null,$imageSize=null,$folderName=null)
{

	$customFolderName ='';

	$image ='';
	if(($folderName=='thumb')||($folderName=='square'))
	{
		$customFolderName="/".$folderName;
	}
	if($propertyId!='')
	{
		$objProperty = DB::table('property_images')
						->select('image_name')
						->where('property_id', '=', $propertyId)
						->where('featured','=',1)
						->get();

		if($objProperty)
		{
			$image =  $objProperty[0]->image_name;
		}
	}
	if($propertyImage!='')
	{
		$image = $propertyImage;
	}

	$originalPropertyImage='';
	if($image!='')
	{
		$imageUrl=asset('public/uploads/properties/').$customFolderName;
		$originalPropertyImage=$imageUrl."/".$image;
	}
	else
	{
		$imageUrl=asset('public/uploads/noimage/');
		$originalPropertyImage=$imageUrl."/propert-blank".$imageSize.".png";//this image is used in different sizes
	}

	return $originalPropertyImage;
});

HTML::macro('getFeaturedPropertyImage', function($propertyId,$propertyImage=null,$imageSize=null,$folderName=null)
{

	$customFolderName ='';

	$image ='';
	if(($folderName=='thumb')||($folderName=='square'))
	{
		$customFolderName="/".$folderName;
	}
	if($propertyId!='')
	{
		$objProperty = DB::table('property_images')
						->select('image_name')
						->where('property_id', '=', $propertyId)
						->where('featured','=',1)
						->get();

		if($objProperty)
		{
			$image =  $objProperty[0]->image_name;
		}
	}
	if($propertyImage!='')
	{
		$image = $propertyImage;
	}

	$originalPropertyImage='';
	if($image!='')
	{
		$imageUrl=asset('public/uploads/properties/').$customFolderName;
		$originalPropertyImage=$imageUrl."/".$image;
	}
	else
	{
		$imageUrl=asset('public/uploads/noimage/');
		$originalPropertyImage=$imageUrl."/propert-blank".$imageSize.".png";//this image is used in different sizes
	}

	return $originalPropertyImage;
});

HTML::macro('getReviewRating',function($reviewId,$rateId){
	$reviewRatings = DB::select('SELECT rate_value, rating_id FROM rating_user WHERE  review_id ='.$reviewId.' AND rating_id='.$rateId);

	$rateValue = $reviewRatings[0]->rate_value;

	$rateImage = asset('public/img/stars'.$rateValue.'.png');

  	return $rateImage;
});

HTML::macro('getAdminReviewRating',function($rateValue){

	$rateImage = asset('public/img/stars'.$rateValue.'.png');

  	return $rateImage;
});


HTML::macro('getDocExtn',function( $docName ){
	 $temp = explode(".", $docName);
	 $file_ext = end($temp);
	 switch( $file_ext ){
	 	case "doc":
	 		return "doc";
	 	break;
	 	case "DOC":
	 		return "doc";
	 	break;
	 	case "pdf":
	 		return "pdf";
	 	break;
	 	case "PDF":
	 		return "pdf";
	 	break;
	 	case "docx":
	 		return "docx";
	 	break;
	 	default:
	 		return "pdf";
	 	break;
	 }
});

HTML::macro('getAverageReviewRating',function($propertyId,$rateId){
	$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id ='.$propertyId.' AND rating_id='.$rateId);
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');
  	return $avgRateImage;
});



HTML::macro('getAverageReviewRatingFromReviewId',function($propertyId, $rateId, $reviewId){
	$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id ='.$propertyId.' AND rating_id='.$rateId.' AND review_id='.$reviewId);
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');

  	return $avgRateImage;
});
HTML::macro('getAverageReviewRatingForLandlord',function( $propertyId ){
	$avgReviewRatings = DB::select("SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id IN($propertyId)");
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');

  	return $avgRateImage;
});

HTML::macro('getLandLordIssueMessageNotification',function(){
	$landLordId = Auth::user('landlord')->id;
	$nonReadMessages = DB::select('SELECT message,message_thread_id,firstname,lastname,m.created_at,t.image,thread_name,mt.status FROM messages m,tenants t, message_threads mt, threads th WHERE m.tenant_id=t.id AND m.message_thread_id=mt.id AND mt.thread_id=th.id AND m.landlord_id='.$landLordId.' AND sender="T"  AND m.read_flag=0 AND th.flag=1 order by m.created_at DESC ');
	//print_r($nonReadMessages);exit;

	$nonReadMaintenanceRequests = TenantMaintenanceRequest::join('properties', 'maintenance_request.property_id' ,'=', 'properties.id'
    )->join('landlords', 'properties.landlord_id', '=', 'landlords.id')
    ->select('properties.property_name', 'maintenance_request.id', 'maintenance_request.severity', 'maintenance_request.issue_type_id', 'maintenance_request.property_id',
    	'maintenance_request.issue_desc', 'maintenance_request.date_encountered', 'maintenance_request.status', 'maintenance_request.date_completed', 'maintenance_request.created_at', 'maintenance_request.updated_at')
    ->where("landlords.id", "=", $landLordId)->where("maintenance_request.status", "=", 0)->get();

    $nonReadMaintRequestCount = count($nonReadMaintenanceRequests);

	$countNonReadMessages = count($nonReadMessages) + $nonReadMaintRequestCount;

	$urlToMaintRequests=URL::to('landlords/myMaintenanceRequests/Date/DESC');

	//print HTML::getLandlordProfileImage('','a.jpg');
	$messageNotification='<li class="dropdown tasks-menu">
		                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                  <i class="fa fa-flag-o"></i>
		                  <span class="label label-danger">'.$countNonReadMessages.'</span>
		                </a>
						<ul class="dropdown-menu">';
					if($countNonReadMessages>0){
						$messageNotification.='<li class="header">You have '. $countNonReadMessages.' new maintenance request(s)</li>';
					}
					else{
						$messageNotification.='<li class="header">You have no new maintenance requests</li>';
					}

                  $messageNotification.='<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">';
                    //if($countNonReadMessages>0){
                    foreach($nonReadMessages as $key=>$nonReadMessage){
                      $messageNotification.='<li><!-- Task item -->
                        <a href="#">
                          <h3> '.$nonReadMessage->thread_name.' <small class="pull-right">50%</small>
                          </h3>
                          <div class="progress xs">';
                          	switch($nonReadMessage->thread_name){

                          	}
                            $messageNotification.='<div class="progress-bar progress-bar-yellow" style="width: 50%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">50% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->';
                      	}

                     foreach($nonReadMaintenanceRequests as $maintRequest){
                     	$messageNotification.="<li><!-- Task item -->
                        <a href='$urlToMaintRequests'>
                          <h3> ".$maintRequest->property_name.'
                          </h3>
                          <div>';
                            $messageNotification.='<div>
                              New Maintenance Request
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->';
                      	}

                    $messageNotification.='</ul>
                  </li>';
                  if($countNonReadMessages>0){
                  $messageNotification.='<li class="footer">
                    <a href="#">View all tasks</a>
                  </li>';
                  	}
                $messageNotification.='</ul></li>';

	return $messageNotification;
	});
HTML::macro('getTenantIssueMessageNotification',function(){
	$tenantId = Auth::user('tenant')->id;
	$nonReadMessages = DB::select('SELECT message,message_thread_id,mt.status,firstname,lastname,m.created_at,t.image,thread_name
	FROM messages m,tenants t, message_threads mt, threads th
	WHERE m.tenant_id=t.id
	AND m.message_thread_id=mt.id
	AND mt.thread_id=th.id
	AND m.tenant_id='.$tenantId.'
	AND sender="L"
	AND m.read_flag=0
	AND th.flag=1 order by m.created_at DESC ');
	//print_r($nonReadMessages);exit;
	$countNonReadMessages = count($nonReadMessages);
	//print HTML::getLandlordProfileImage('','a.jpg');
	$messageNotification='<li class="dropdown tasks-menu">
		                <a title="Responses" href="#" class="dropdown-toggle" data-toggle="dropdown">
		                  <i class="fa fa-flag-o"></i>
		                  <span class="label label-danger">'.$countNonReadMessages.'</span>
		                </a>
						<ul class="dropdown-menu">';
					if($countNonReadMessages>0){
						$messageNotification.='<li class="header">You have '. $countNonReadMessages.' message(s)</li>';
					}
					else{
						$messageNotification.='<li class="header">You have no messages</li>';
					}

                  $messageNotification.='<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">';
                    //if($countNonReadMessages>0){
                    $latesMessageThreadId='';
                    //$progressBarClass='';
                    //$progressBarPercentage='';
                    foreach($nonReadMessages as $key=>$nonReadMessage){
						switch($nonReadMessage->status)
						{
							case "0":
									$progressBarClass='red';
									$progressBarPercentage='20%';
							break;

							case "1":
									$progressBarClass='yellow';
									$progressBarPercentage='60%';
							break;

							case "2":
									$progressBarClass='green';
									$progressBarPercentage='100%';
							break;
						}
					$latesMessageThreadId=$nonReadMessages[0]->message_thread_id;
					$url=URL::to('tenants/messagelist/'.$nonReadMessage->message_thread_id);
                      $messageNotification.='<li><!-- Task item -->
                        <a href='.$url.'>
                          <h3> '.$nonReadMessage->thread_name.' <small class="pull-right">'.$progressBarPercentage.'</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-'.$progressBarClass.'" style="width: '.$progressBarPercentage.'" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">'.$progressBarPercentage.' Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->';

                      	}
                    $messageNotification.='</ul>
                  </li>';
                  $urlAll=URL::to('tenants/messagelist/'.$latesMessageThreadId);
                  if($countNonReadMessages>0){
                  $messageNotification.='<li class="footer">
                    <a href="'.$urlAll.'">View all tasks</a>
                  </li>';
                  	}
                $messageNotification.='</ul></li>';

	return $messageNotification;
	});

	HTML::macro('getAverageReviewRatingProperty',function($propertyId){

	$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id ='.$propertyId);
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');

  	return $avgRateImage;
});

//this method is for the notifications in tenant side
HTML::macro('getNotificationTenantSide',function(){

$loggedTenantId = Auth::user('tenant')->id;

$notificationMonthlyRentAlerts = DB::select('SELECT "Monthly Rent" AS title, LTM.bill_on, LTM.amount, LTM.status, LT.id
FROM landlord_tenant_monthlyrents AS LTM
INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTM.bill_on
BETWEEN CURDATE( )
AND DATE_ADD( CURDATE( ) , INTERVAL 10
DAY ) AND LTM.status=0
ORDER BY LTM.bill_on ASC ');

$notificationOtherPaymentAlerts = DB::select('SELECT LTOD.bill_on, LTOD.title, LTOD.status, LTOD.amount
FROM landlord_tenant_otherpayment_details AS LTOD
INNER JOIN landlord_tenant_otherpayments AS LTO ON LTOD.landlord_tenant_otherpayment_id = LTO.id
INNER JOIN landlord_tenants AS LT ON LT.id = LTO.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTOD.bill_on
BETWEEN CURDATE( )
AND DATE_ADD( CURDATE( ) , INTERVAL 10
DAY )
AND LTOD.status =0
ORDER BY LTOD.bill_on ASC');

$notificationMonthlyRentNotPaidAlerts=DB::select('SELECT "Monthly Rent" AS title, LTM.bill_on, LTM.amount, LTM.status, LT.id
FROM landlord_tenant_monthlyrents AS LTM
INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTM.bill_on < CURDATE( )
AND LTM.status =0
ORDER BY LTM.bill_on ASC ');

$notificationOtherPaymentNotPaidAlerts=DB::select('SELECT LTOD.bill_on, LTOD.title, LTOD.status, LTOD.amount
FROM landlord_tenant_otherpayment_details AS LTOD
INNER JOIN landlord_tenant_otherpayments AS LTO ON LTOD.landlord_tenant_otherpayment_id = LTO.id
INNER JOIN landlord_tenants AS LT ON LT.id = LTO.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTOD.bill_on < CURDATE( )
AND LTOD.status =0
ORDER BY LTOD.bill_on ASC');

$notificationMonthlyRentAlertsCount 		= count($notificationMonthlyRentAlerts);
$notificationOtherPaymentAlertsCount 		= count($notificationOtherPaymentAlerts);
$notificationMonthlyRentNotPaidAlertsCount 	= count($notificationMonthlyRentNotPaidAlerts);
$notificationOtherPaymentNotPaidAlertsCount = count($notificationOtherPaymentNotPaidAlerts);

$totalAlertsCount = $notificationMonthlyRentAlertsCount + $notificationOtherPaymentAlertsCount + $notificationMonthlyRentNotPaidAlertsCount + $notificationOtherPaymentNotPaidAlertsCount;

$notification= '<li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">'.$totalAlertsCount.'</span>
                </a>
                <ul class="dropdown-menu">';
   if($totalAlertsCount > 0)
   {
	   $notification.='<li class="header">You have '.$totalAlertsCount.' notification(s)</li>';
   }
   else
   {
	   $notification.='<li class="header">You have no notifications</li>';
   }

   $notification.='</li>
				<li>
				 <ul class="menu">';
				foreach($notificationMonthlyRentAlerts as $notificationMonthlyRentAlert)
				{
					$billDate 					= $notificationMonthlyRentAlert->bill_on;
					$billDateFormat 			= date('d M Y',strtotime($billDate));
					$billDateString 			= str_replace("-", "", $billDate);
					$billDateStringMonthlyRent  = substr($billDateString,0,6);

					$url=URL::to('tenants/mypayments/'.$billDateStringMonthlyRent);
					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-users text-aqua"></i> Your '.strtolower($notificationMonthlyRentAlert->title).' is due on '.$billDateFormat.'
										</a>
									</li>';
				}
				foreach($notificationOtherPaymentAlerts as $notificationOtherPaymentAlert)
				{
					$billDate 					 = $notificationOtherPaymentAlert->bill_on;
					$billDateFormat 			 = date('d M Y',strtotime($billDate));
					$billDateString 			 = str_replace("-", "", $billDate);
					$billDateStringOtherPayment  = substr($billDateString,0,6);

					$url=URL::to('tenants/mypayments/'.$billDateStringOtherPayment);
					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-warning text-yellow"></i> Your '.strtolower($notificationOtherPaymentAlert->title).' is due on '.$billDateFormat.'
										</a>
									</li>';
				}
				foreach($notificationMonthlyRentNotPaidAlerts as $notificationMonthlyRentNotPaidAlert)
				{
					$billDate 					 		= $notificationMonthlyRentNotPaidAlert->bill_on;
					$billDateFormat 			 		= date('d M Y',strtotime($billDate));
					$billDateString 			 		= str_replace("-", "", $billDate);
					$billDateStringMonthlyRentNotPaid  = substr($billDateString,0,6);
					$url=URL::to('tenants/mypayments/'.$billDateStringMonthlyRentNotPaid);

					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-exclamation-circle"></i> Your '.strtolower($notificationMonthlyRentNotPaidAlert->title).' ('.$billDateFormat.') payment is pending
										</a>
									</li>';
				}

				foreach($notificationOtherPaymentNotPaidAlerts as $notificationOtherPaymentNotPaidAlert)
				{
					$billDate 					 		= $notificationOtherPaymentNotPaidAlert->bill_on;
					$billDateFormat 			 		= date('d M Y',strtotime($billDate));
					$billDateString 			 		= str_replace("-", "", $billDate);
					$billDateStringOtherPaymentNotPaid  = substr($billDateString,0,6);
					$url=URL::to('tenants/mypayments/'.$billDateStringOtherPaymentNotPaid);

					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-exclamation"></i> Your '.strtolower($notificationOtherPaymentNotPaidAlert->title).' ('.$billDateFormat.') payment is pending
										</a>
									</li>';
				}
				$notification.='</ul>
                  </li>';
               $notification.='</ul></li>';
      return $notification;

});

HTML::macro('getCmsPages',function(){
	$cmsPagesArray = DB::select('SELECT * FROM pages WHERE status=1 AND footerpage=1 ');
	$cmsPagesHtml= '<ul class="left-menu">';
	for($i=0;$i<count($cmsPagesArray);$i++)
	{
		$url=URL::to('cms/'.$cmsPagesArray[$i]->pageslug);
		$cmsPagesHtml.= '<li><a href='.$url.'>'.$cmsPagesArray[$i]->title.'</a></li>';
		$max = count($cmsPagesArray)-1;
		if($i==$max)
		{
			$cmsPagesHtml.='</ul>';
		}
		elseif(($i%5==0)&&($i>0)){
			$cmsPagesHtml.='</ul><ul class="left-menu">';
		}
	}
	return $cmsPagesHtml;
});

HTML::macro('getTenantBreadcrumbs',function($menuName){

	//$url='';
	//dd($url);
	$homeUrl = URL::to('tenants/dashboard');
	if($menuName == 'dashboard')
	{
		$pageHeading='';
		$breadCrumb = 'Home';
	}
	elseif($menuName=='myApplication') {
		$pageHeading='';
		$breadCrumb = 'My Application';
	}
	elseif($menuName=='manageSubmittedApplications') {
		$pageHeading='';
		$breadCrumb='Manage Submitted Applications';
	}
	elseif($menuName=='applicationGroups') {
		$pageHeading='';
		$breadCrumb='Application Groups';
	}
	elseif($menuName == 'mysearch')
	{
		$pageHeading='';
		$breadCrumb = 'My search';
	}
	elseif($menuName == 'recentproperties')
	{
		$pageHeading='';
		$breadCrumb = 'Recent Properties';
	}
	elseif($menuName == 'mylandlord')
	{
		$pageHeading='';
		$breadCrumb = 'My landlord';
	}
	elseif($menuName == 'mypayment')
	{
		$pageHeading='';
		$breadCrumb = 'My Payments';
	}
	elseif($menuName == 'mycontact')
	{
		$pageHeading='';
		$breadCrumb = 'My contact';
	}

	elseif($menuName == 'changepassword')
	{
		$pageHeading='';
		$breadCrumb = 'Change Password';
	}
	elseif($menuName == 'profile')
	{
		$pageHeading='';
		$breadCrumb = 'Profile';
	}
	elseif($menuName == 'schools')
	{
		$pageHeading='';
		$breadCrumb = 'Schools';
	}
	elseif($menuName == 'newmessage')
	{
		$pageHeading='';
		$breadCrumb = 'Messages';
	}
	elseif($menuName == 'messagelist')
	{
		$pageHeading = '';
		$breadCrumb  = 'List message';
	}
	elseif($menuName == 'tenantPaymentSettings')
    {
        $pageHeading = '';
        $breadCrumb = 'My Payment Settings';
    }
    elseif($menuName == 'createMaintenanceRequest')
	{
		$pageHeading = '';
		$breadCrumb = 'Create Maintenance Request';
    }
	elseif($menuName == 'myMaintenanceRequests')
	{
		$pageHeading = '';
		$breadCrumb = 'My Maintenance Requests';
	}
	elseif($menuName == 'myRentPaymentHistory')
	{
		$pageHeading = '';
		$breadCrumb = 'My Rent Pay History';
	}

	$breadCrumbHtml='';
	//dd($menuName);
	if($menuName=='dashboard')
	{
		$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'

					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>

					  </ol>
					</section>';
	}
	else
	{
	$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'

					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active"><a href="">'.$breadCrumb.'</a></li>
					  </ol>
					</section>';
	}
	return $breadCrumbHtml;
	});


	HTML::macro('getLandlordBreadcrumbs',function($menuName){

	$homeUrl = URL::to('landlords/dashboard');
	if($menuName == 'dashboard')
	{
		$pageHeading='';
		$breadCrumb = 'Home';
	}
	elseif($menuName=='manageSubmittedApplications') {
                $pageHeading='';
                $breadCrumb='Manage Received Applications';
        }
	elseif($menuName == 'myproperty')
	{
		$pageHeading='';
		$breadCrumb = 'My property';
	}
	  elseif($menuName == 'propertymanagementgroups')
    {
    	$pageHeading='';
    	$breadCrumb = 'my property management groups';
    }
	elseif($menuName == 'mypayment')
	{
		$pageHeading='';
		$breadCrumb = 'My payment';
	}
	elseif($menuName == 'renthistory')
	{
		$pageHeading='';
		$breadCrumb = 'Rent History';
	}
	elseif($menuName == 'mytenantcontact')
	{
		$pageHeading='';
		$breadCrumb = 'Conversations';
	}
	elseif($menuName == 'messageunit')
	{
		$pageHeading='';
		$breadCrumb = 'Unit Conversations';
	}elseif($menuName == 'messageportfolio')
	{
		$pageHeading='';
		$breadCrumb = 'Portfolio Conversations';
	}
	elseif($menuName == 'maintenancerequests')
	{
		$pageHeading = '';
		$breadCrumb = 'Maintenance requests';
	}
	elseif($menuName == 'mytenants')
	{
		$pageHeading='';
		$breadCrumb = 'My tenant';
	}
	elseif($menuName == 'payment')
	{
		$pageHeading='';
		$breadCrumb = 'Payment Settings';
	}
	elseif($menuName == 'featured properties')
	{
		$pageHeading='';
		$breadCrumb = 'Featured Properties';
	}

	elseif($menuName == 'changepassword')
	{
		$pageHeading='';
		$breadCrumb = 'Change Password';
	}
	elseif($menuName == 'viewtenant')
	{
		$pageHeading='';
		$breadCrumb = 'View Tenant';
	}
	elseif($menuName == 'edittenant')
	{
		$pageHeading='';
		$breadCrumb = 'Edit Tenant';
	}
	elseif($menuName == 'Profile')
	{
		$pageHeading='';
		$breadCrumb = 'Profile';
	}
	elseif($menuName == 'editprofile')
	{
		$pageHeading='';
		$breadCrumb = 'Edit profile';
	}
	elseif($menuName == 'Review')
	{
		$pageHeading='';
		$breadCrumb = 'Property Review';
	}
	elseif($menuName == 'newmessage')
	{
		$pageHeading='';
		$breadCrumb = 'Messages';
	}
	elseif($menuName == 'messagelist')
	{
		$pageHeading='';
		$breadCrumb = 'List message';
	}
	elseif($menuName == 'mysubscription')
    {
        $pageHeading='';
        $breadCrumb = 'My subscription';
    }
    elseif($menuName == 'upgradesubscription')
    {
        $pageHeading='';
        $breadCrumb = 'Upgrade subscription';
    }
    elseif($menuName == 'mysubscriptionpaymentsettings')
    {
        $pageHeading='';
        $breadCrumb = 'My subscription payment settings';
    }
    elseif($menuName == 'createapplication')
    {
        $pageHeading='';
        $breadCrumb = 'Account Application';
    }
	if($menuName=='dashboard')
	{
		$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'

					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
					  </ol>
					</section>';
	}
	else
	{
	//dd($menuName);
	$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'

					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active"><a href="">'.$breadCrumb.'</a></li>
					  </ol>
					</section>';
	}
	return $breadCrumbHtml;
	});

// Parameters needed from monthly rent pay to be passed into here to be passed into Forte Post Rent:
	// Monthly Rent Amount, Bill Date (Due On), uniqueId for URL,
HTML::macro('getPaynowButton',function( $status, $billDate, $uniqueId, $flag, $landlordId, $tenantId){
	// $onClick = "onclick=confirmPay('583d00afe8f07')";
	//exit($uniqueId);
	$onClick = "onclick=confirmPay('".$uniqueId."')";
	if($flag=="month"){
		//$url=URL::to('payments/paynowmonthlyrent').'/'.$uniqueId;
		$url=route('payment.paynowmonthlyrent', $uniqueId);
	}
	else{
		$url=URL::to('payments/paynowitinerary').'/'.$uniqueId;
	}
	//$tenant_payment_settings  = PaymentSetting::where( 'landlord_id', $landlordId )
	//							->first();
	// We have to make this smarter so that the stored landlord id is the same as the current landlord
	try{
	$tenant_stored_landlord_loc = Crypt::decrypt(TenantPaymentSetting::where('tenant_id',$tenantId)->pluck('current_landlord_org_id'));
	}
	catch (DecryptException $e){
                //echo "This is wrong";
    }
	try
	{
	$landlord_loc = Crypt::decrypt(LandlordPaymentReceiveSetting::where('landlord_id',$landlordId)->pluck('org_id'));
	}
	catch (DecryptException $e){
                //echo "This is wrong";
    }

	$curDate 			= date('Y-m-d');
	$currentDateTime 	= strtotime($curDate);
	$billDateTime 		= strtotime($billDate);


    if(empty( $tenant_stored_landlord_loc ))
    {
    	$url="javascript:void(0)";
    	$onClick = 'onclick=alertError();';
    }
    elseif($tenant_stored_landlord_loc != $landlord_loc)
    {
    	$url="javascript:void(0)";
    	$onClick = 'onclick=differenceError();';
    }

    if(($status==0) && ($billDateTime <= $currentDateTime))
    {
    	$url="javascript:void(0)";
    	$onClick = 'onclick=alertLateRent();';
    }



	//$payNowHtml = $billDate;
	if(($status==0) && ($billDateTime <= $currentDateTime))
	{
		//$url ="javascript:void(0)";
		$url = "#";
		$onClick = "onclick=alertLateRent('".$uniqueId."')";
		$payNowHtml = '<td><a href="'.$url.'" class="paynow paynow-exp"'.$onClick.'> Pay Late Rent </a></td>';
	}
	elseif($status==0)
	{
		$payNowHtml = '<td><a href="#" class="paynow"'.$onClick.'>Pay Rent</a></td>';
	}
	elseif($status==1)
	{
		$payNowHtml = '<td><a href="'. route('tenants.myPaymentHistory') .'" class="paid">Pending</td>';
	}
	elseif($status==2)
	{
		// We want this to direct user to payment list
		$payNowHtml = '<td class="paid">Paid</td>';
	}

	return $payNowHtml;
});


HTML::macro('getReviewCount',function( $propertyId ){

		$reviewsCount = DB::select("SELECT COUNT(*) AS cnt FROM property_review WHERE property_id IN ($propertyId)");

        return $reviewsCount[0]->cnt;
});

HTML::macro('getSocialMediaLinks',function( $socialLink ){
	$link='';
	if($socialLink == 'twitter')
	{
		$social = DB::select("SELECT twitter FROM `social_links` ");

		$link 	= $social[0]->twitter;
		if($link=='')
		{
			$link='#';
		}
	}
	elseif($socialLink == 'facebook')
	{
		$social = DB::select("SELECT facebook FROM `social_links` ");

		$link 	= $social[0]->facebook;
		if($link=='')
		{
			$link='#';
		}
	}
	elseif($socialLink == 'instagram')
	{
		$social = DB::select("SELECT instagram FROM `social_links` ");

		$link 	= $social[0]->instagram;
		if($link=='')
		{
			$link='#';
		}
	}

	return $link;

});

HTML::macro('getPaymentDate',function( $status,$paymentDate){
	if(($status==1) &&($paymentDate!='0000-00-00 00:00:00'))
	{

		$date =  date("d-m-Y", strtotime($paymentDate));
	}
	else
	{
		$date =  '';
	}
	return $date;
});

HTML::macro('getSchoolImage',function($schoolId){

	$objSchool = DB::table('schools')
            ->select('image')
            ->where('id', '=', $schoolId)
            ->get();

    $schoolImage =  $objSchool[0]->image;
   // dd($schoolImage);
    $originalSchoolImage='';
	if($schoolImage!='')
	{
		$imageUrl=asset('public/uploads/schools/');
		$originalSchoolImage=$imageUrl."/".$schoolImage;
	}

	return $originalSchoolImage;

});

HTML::macro('getAdminImage',function(){
	$imageUrl     = asset('public/uploads/noimage/').'/landlord-blank.png';
	return $imageUrl;
});

HTML::macro('getBannerImage',function($imageName){
	$imageUrl     = asset('public/uploads/banners/');
	$fullImage    = $imageUrl.'/'.$imageName;
	return $fullImage;
});

HTML::macro('getSampleBannerImage',function($sampleImageName){
	$imageUrl     = asset('public/img/');
	$fullImage    = $imageUrl.'/'.$sampleImageName;
	return $fullImage;
});

HTML::macro('resolveMaintenanceRequestIssueType', function($issueType){
    if ($issueType == 0)
    {
        return 'Plumbing';
    }
    else if ($issueType == 1)
    {
        return 'Air Conditioning';
    }
    else if ($issueType == 2)
    {
        return 'Electric';
    }
    else if ($issueType == 3)
    {
        return 'Other';
    }
});

HTML::macro('resolveMaintenanceRequestStatusCode', function($statusCode){
    if ($statusCode == 0)
    {
        return 'Submitted';
    }
    else if ($statusCode == 1)
    {
        return 'Read';
    }
    else if ($statusCode == 2)
    {
        return 'Maintenance In Progress';
    }
    else if ($statusCode == 3)
    {
        return 'Issue Resolved';
    }
});

HTML::macro('getBanner',function($bannerId){
	$imageHtml='';
	$fullImage = '';
	$objBanner = DB::table('banner_purchases')
            ->select('image')
            ->addSelect('banner_url')
            ->where('bannerId', '=', $bannerId)
            ->where('status','=',1)
            ->get();

	$imageUrl     = asset('public/uploads/banners');
	if(count($objBanner) > 0)
	{
		$fullImage    = $imageUrl.'/'.$objBanner[0]->image;
	}
	if(($bannerId==2) && (count($objBanner)>0))
	{
	$imageHtml = '<div class="row ad-container">
  				<div class="col-sm-12 col-xs-12">
					<a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
				</div>
  			</div>';
	}
	elseif(($bannerId==1) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container">
          <div class="col-sm-12 col-xs-12">
			  <a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
			  </div>
        </div>';
	}
	elseif(($bannerId==4) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container map">
          <div class="col-sm-12 col-xs-12">
			 <a href="'.$objBanner[0]->banner_url.'" target="_blank"> <img src="'.$fullImage.'"></a>
			  </div>
        </div>';
	}
	elseif(($bannerId==3) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container">
  				<div class="col-sm-12 col-xs-12">
					<a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
				</div>
  			</div>';
	}

	elseif(($bannerId==5) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container">
		<div class="col-sm-12 col-xs-12">
			<a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
		</div>
  	</div>';
	}
	return $imageHtml;
});

HTML::macro('grabAvailableTimes',function($maintenanceRequestId) {
     $maintenanceAvailableDb = DB::table('maintenance_available_times')
	->select('time_available')
          ->where('maintenance_request_id',"=", $maintenanceRequestId)
           ->get();

    if($maintenanceAvailableDb != null)
    {

     $maintenanceAvailableString = "<strong>".$maintenanceAvailableDb[0]->time_available."</strong> or <strong>".$maintenanceAvailableDb[1]->time_available."</strong>";

    }
    else
    {
    	$maintenanceAvailableString = "<strong>Not Available</strong>";
    }
    return $maintenanceAvailableString;
  });

HTML::macro('isUserLoggedIn', function(){
   if(Auth::user('landlord')){
   return route("landlords.logout");
   }
   else if(Auth::user('tenant')){
   return route("tenants.logout");
   }
   else{
   return route("index.login");
   }
});
HTML::macro('isUserLoggedInBool', function(){
   if(Auth::user('landlord')){
   return true;
   }
   else if(Auth::user('tenant')){
   return true;
   }
   else{
   return false;
   }
});
