<!DOCTYPE html>
<html lang="en">
<head>
	@include('elements.frontend-head')
	<!-- page specific css -->
        @yield('page-css')

 <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

  </script>
</head>
<body class="inner-subpage">
	<script>
		var email='';
		<?php 
		$email = '';
		if(Auth::user('tenant')){ 
			$email = Auth::user('tenant')->email;
		}else if(Auth::user('landlord')){
			$email = Auth::user('landlord')->email;
		}else{
			$email = 'Anonymous';
		}
		?>
	  
	  
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78780279-1', 'auto');
  ga('send', 'pageview');
ga('set', 'userId', '{{ $email }}');
	  
	</script>
	@include('elements.non-logged-search-header')

	@yield('content')
	
	<!-- footer -->
	<!-- //////// starts//////// -->
  	@include('elements.frontend-footer')
  	<!-- footer -->
	<!-- //////// ends//////// -->
	</div>

	<!-- Bootstrap core JavaScript
    ================================================== --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	@include('elements.frontend-search-scripts')
	@yield('page-scripts')
	@yield('page-css')

</body>
</html>
