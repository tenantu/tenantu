<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- metas & css -->
        @include('elements.head')
        <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

        </script>
    </head>
    <body class="login-page">
		<script>
			var email='';
			<?php 
			$email = '';
			if(Auth::user('tenant')){ 
				$email = Auth::user('tenant')->email;
			}else if(Auth::user('landlord')){
				$email = Auth::user('landlord')->email;
			}else{
				$email = 'Anonymous';
			}
			?>
	
		  
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78780279-1', 'auto');
  ga('send', 'pageview');
ga('set', 'userId', '{{ $email }}');

		</script>

        <!-- page content -->
        @yield('content')

        <!-- scripts -->
        @include('elements.scripts')

        <!-- page specific scripts -->
        @yield('page-scripts')

    </body>
</html>
