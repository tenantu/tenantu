<!DOCTYPE html>
<html>
  <head>
    @include('elements.frontend-tenant-inner-head')
	<!-- page specific css -->
        @yield('page-css')

    <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

    </script>
  </head>
  <body class="skin-green sidebar-mini fixed">
	  <script>
		 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78780279-1', 'auto');
  ga('send', 'pageview');
  ga('set', 'userId', '{{ Auth::user("tenant")->email }}');
		</script>
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="{{route('index.index')}}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg pull-left" style="font-size: 40px; padding: 4px 7px 7px 6px;">
			  TenantU
			  </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              {!!HTML::getTenantMessageNotification()!!}
              <!-- Notifications: style can be found in dropdown.less -->
             
              <!-- Tasks: style can be found in dropdown.less -->
             
                {!!HTML::getTenantIssueMessageNotification()!!}
             
              <!-- User Account: style can be found in dropdown.less -->
               <!-- notifications for tenant monthly rent and other expenses-->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				
                  <img src="{{HTML::getTenantProfileImage()}}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ Auth::user('tenant')->getFullName()}} </span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{HTML::getTenantProfileImage()}}" class="img-circle" alt="User Image">
                    <p>
                      {{ Auth::user('tenant')->getFullName()}} 
                      <small>Member since {{Auth::user('tenant')->getCreatedAt()}}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{route('tenants.profile')}}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ route('tenants.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      @include('elements.frontend-inner-tenant-leftsidebar')
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
         @include('elements.frontend-inner-tenant-breadcrumbs')
        <!-- Main content -->
        
        @yield('tenant-inner-content')
        <!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <span>All rights reserved.</span>
        </div>
        Copyright Tenantu © 2014-2017
      </footer>

      
    </div><!-- ./wrapper -->

    @include('elements.frontend-inner-tenant-scripts')
  </body>
</html>
