<!DOCTYPE html>
<html>
    <head>
        <!-- metas & css -->
        @include('elements.frontend-head')

        <!-- page specific css -->
        @yield('page-css')

        <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

        </script>
        <script>
            var baseUrlPath = '{{ asset('public') }}' ;
        </script>
    </head>
    <body>

        <!-- header -->
        @include('elements.frontend-header')

        <!-- banner -->
        @include('elements.frontend-banner')

        <!-- contents -->
        <section class="contents">

            <div class="container">

                <!-- page specific contents -->
                @yield('content')

            </div>

            <!-- clients -->
            @include('elements.frontend-clients')

            <!-- footer -->
            @include('elements.frontend-footer')

        </section>

        <!-- login modal -->
        @include('elements.login-modal')

        <!-- forgotpassword modal -->
        @include('elements.forgotpassword-modal')

        <!-- signup modal -->
        @include('elements.signup-modal')

        <!-- Invite Friends modal -->
         @include('elements.invitefriends-modal')

        <!-- scripts -->
        @include('elements.frontend-scripts')

        <!-- page specific scripts -->
        @yield('page-scripts')

    </body>
</html>
