@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->

  @include('elements.breadcrump-landlord')
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <!-- Poist property -->
        <div class="tnt-box landlord-add-propety edit-landlord-property">
          <div class="box-heads">
            <h4>Edit Property</h4>
          </div>
          @include('elements.validation-message')

          @if (Session::has('message-success'))
          @include('elements.message-success')
          @endif
          {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm','route'=>['property.postedit',$propertyList->id],'files'=>true]) !!}
          {!! Form::hidden('img_name', '', ['class'=>'form-control login-input','id'=>'imgName']) !!}
          {!! Form::hidden('hdPlace', $schoolLocation, ['id'=>'hdPlace']) !!}
          {!! Form::hidden('propId', $id, ['id'=>'propId']) !!}
          <div class="row">
              <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            {!! Form::label('availability_status', 'Current Availability Status:')!!}<span style="color: red;">*</span>
            <div class="radio">
              <label> {!! Form::radio('availability_status', 'Available',($propertyList->property_meta->availability_status==='Available' ? true : false), ['id'=>'optionsRadios3']) !!} <span class="radio-text">Available</span> </label>
            </div>
            <div class="radio">
              <label> {!! Form::radio('availability_status', 'Not available',($propertyList->property_meta->availability_status==='Not available' ? true : false), ['id'=>'optionsRadios4','required']) !!} <span class="radio-text">Unavailable</span> </label>
            </div>
          </div>
	   <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
            {!! Form::label('next_year_availability_status', '2018-2019 Availability Status') !!}<span style="color: red;">*</span>
            <div class="radio">
              <label> {!! Form::radio('next_year_availability_status', 'Available',($propertyList->property_meta->next_year_availability_status==='Available' ? true : false), ['id'=>'optionsRadios3']) !!} <span class="radio-text">Available</span> </label>
            </div>
            <div class="radio">
              <label> {!! Form::radio('next_year_availability_status', 'Not available',($propertyList->property_meta->next_year_availability_status==='Not available' ? true : false), ['id'=>'optionsRadios4','required']) !!} <span class="radio-text">Unavailable</span> </label>
            </div>
          </div>



            <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12"> {!! Form::label('propertytitle', 'Property Title:') !!}<span style="color: red;">*</span> {!! Form::text('property_name', $propertyList->property_name, ['class'=>'form-control login-input','id'=>'propertytitle','placeholder'=>'Property Title','required']) !!} </div>

            {{--<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12"> {!! Form::label('location', 'Location:') !!}<span style="color: red;">*</span>--}}
            {{--<ul id="location">--}}
				{{--<li>--}}
					{{--{{ $propertyList->location }}--}}
				{{--</li>--}}
            {{--</ul>--}}
             {{--</div>--}}
              <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12"> {!! Form::label('street_address', 'Street Address:') !!}<span style="color: red;">*</span> {!! Form::text('street_address', $propertyList->thoroughfare, ['class'=>'form-control login-input','id'=>'street_address','placeholder'=>'Street Address','required']) !!} </div>
              <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12"> {!! Form::label('city', 'City:') !!}<span style="color: red;">*</span> {!! Form::text('city', $propertyList->locality, ['class'=>'form-control login-input','id'=>'city','placeholder'=>'City','required']) !!} </div>
              <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12"> {!! Form::label('state', 'State:') !!}<span style="color: red;">*</span> {!! Form::text('state', $propertyList->administrative_area, ['class'=>'form-control login-input','id'=>'state','placeholder'=>'State','required']) !!} </div>
              <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12"> {!! Form::label('zip_code', 'Zip Code:') !!}<span style="color: red;">*</span> {!! Form::text('zip_code', $propertyList->postal_code, ['class'=>'form-control login-input','id'=>'postal_code','placeholder'=>'Zip Code','required']) !!} </div>
              <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12"> {!! Form::label('schoolname', 'School Name:') !!}<span style="color: red;">*</span> {!! Form::select('school_id',['' => 'Select school']+$school, $propertyList->school_id, ['class'=>'form-control login-input','id'=>'schoolname','required']) !!} </div>

              {!! Form::hidden('schoolCalc', $schoolLocation, array('id' => 'schoolCalc')) !!}
            <div class="clearfix"></div>
            <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-12"> {!! Form::label('bedroom', 'Bedrooms:') !!}<span style="color: red;">*</span> {!! Form::input('number','bedroom_no', $propertyList->bedroom_no, ['class'=>'form-control login-input','id'=>'bedroomNo','placeholder'=>'bedrooms','required']) !!} </div>
            <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-12"> {!! Form::label('bathroom', 'Bathrooms:') !!}<span style="color: red;">*</span> {!! Form::input('number','  bathroom_no', $propertyList->bathroom_no, ['class'=>'form-control login-input','id'=>' bathroomNo','placeholder'=>'bathrooms','required']) !!} </div>
            <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-12"> {!! Form::label('rentprice', 'Rent:') !!}<span style="color: red;">*</span>
              {!! Form::input('number','rent_price', $propertyList->property_meta->rent_price, ['class'=>'form-control login-input','id'=>'rentPrice','placeholder'=>'Rent','required']) !!} </div>
            <div class="clearfix"></div>
            <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-12"> {!! Form::label('charge_per_bed', 'Charge Option:') !!}<span style="color: red;">*</span>
                  {!! Form::select('charge_per_bed', ['0' => 'Charge Per Unit', '1' => 'Charge Per Bed'], $propertyList->property_meta->charge_per_bed, ['class' => 'form-control login-input', 'id'=>'charge_per_bed', 'placeholder' => $propertyList->property_meta->charge_per_bed, 'required']) !!}</div>

            <div class="form-group col-lg-2 col-md-2 col-sm-3 col-xs-12">
              {!! Form::label('max_occupancy', 'Max Occupancy') !!}<span style="color: red;">*</span>
              {!! Form::input('number', 'max_occupancy', $propertyList->max_occupancy, ['class' => 'form-control login-input', 'id' => 'max_occupancy', 'required']) !!}
            </div>

            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12"> {!! Form::label('distance_from_school', 'Distance From School (Miles):') !!}<span style="color: red;">*</span> {!! Form::input('number','distance_from_school', $propertyList->distance_from_school, ['class'=>'form-control login-input','id'=>'distanceFromSchool','placeholder'=>'Distance From School','step'=>'any']) !!} </div>

            <div class="form-group col-lg-7 col-md-7 col-sm-7 col-xs-12"> {!! Form::label('propertydescription', 'Property Description:') !!}<span style="color: red;">*</span> {!! Form::textarea('description', $propertyList->description, ['class'=>'form-control login-input','id'=>'description','placeholder'=>'Description','rows'=>'3','required']) !!} </div>
            <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-12"> {!! Form::label('amenities', 'Amenities:') !!}<span style="color: red;">*</span>
              <p></p>
              {!! Form::select('aminity[]', $aminities, $propertyList->property_amenities, ['class'=>'form-control login-input','id'=>'aminity','multiple']) !!}
            </div>
          </div>
          {!! Form::label('communicationMedium', 'Communication Medium:') !!}<span style="color: red;">*</span>
          <div class="radio-container">
            <div class="radio">
              <label> {!! Form::radio('communication_medium', 'W',($propertyList->property_meta->communication_medium==='W' ? true : false), ['id'=>'optionsRadios1']) !!} <span class="radio-text">Web</span> </label>
            </div>
            <div class="radio">
              <label> {!! Form::radio('communication_medium', 'E',($propertyList->property_meta->communication_medium==='E' ? true : false), ['id'=>'optionsRadios2','required']) !!} <span class="radio-text">Email</span> </label>
            </div>
          </div>
          <!-- <label class="checkbox-text"> -->
          <!-- {!! Form::radio('communication_medium', 'W') !!} -->
          <!-- <span class="lbl padding-8">Web</span> -->
          <!-- </label> -->
          <!-- <label class="checkbox-text">
                  {!! Form::radio('communication_medium', 'E') !!}
                   <span class="lbl padding-8">Email</span>
                  </label> -->
          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box-heads">
                <h4>Uploaded Photos</h4>
              </div>
              <span class="btn btn-success fileinput-button"> <i class="glyphicon glyphicon-plus"></i>
				<span>Add Photos...</span>
              	<input id="upload_img" type="file" name="files[]" multiple>
              </span>
		  </div>
            <div class="col-lg-5 fileupload-progress fade">
              <!-- The global progress bar -->
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
              </div>
              <!-- The extended global progress state -->
              <div class="progress-extended">&nbsp;</div>
            </div>
            <table role="presentation" class="table table-striped">
              <tbody class="files">
              </tbody>
            </table>
            <div class="row">

              <!-- The template to display files available for upload -->
              <script id="template-upload" type="text/x-tmpl">
                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                            <tr class="template-upload fade">
                                <td>
                                    <span class="preview"></span>
                                </td>
                                <td>
                                    <p class="name">{%=file.name%}</p>
                                    <strong class="error text-danger"></strong>
                                </td>
                                <td>
                                    <p class="size">Processing...</p>
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                                </td>
                                <td>
                                    {% if (!i && !o.options.autoUpload) { %}
                                        <button class="btn btn-primary start" disabled>
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Start</span>
                                        </button>
                                    {% } %}
                                    {% if (!i) { %}
                                        <button class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    {% } %}
                                </td>
                            </tr>
                        {% } %}
                        </script>
              <!-- The template to display files available for download -->
              <script id="template-download" type="text/x-tmpl">
                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                            <tr class="template-download fade">
                                <td>
                                    <span class="preview">
                                        {% if (file.thumbnailUrl) { %}
                                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                                        {% } %}
                                    </span>
                                </td>
                                <td>
                                    <p class="name">
                                        {% if (file.url) { %}
                                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                                        {% } else { %}
                                            <span>{%=file.name%}</span>
                                        {% } %}
                                    </p>
                                    {% if (file.error) { %}
                                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                                    {% } %}
                                </td>
                                <td>
                                    {% if (file.deleteUrl) { %}
                                        <button class="btn btn-danger delete 123" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Delete</span>
                                        </button>
                                    {% } else { %}
                                        <button class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    {% } %}
                                </td>
                            </tr>
                        {% } %}
                        </script>
            </div>
          </div>
          <div class="view-property">
            <div>
              <div style="color:green" id="showMsg"></div>
              <div class="row">
               @forelse( $propertyList->property_images as $image )
                <div class="imgColumn" id="imgColumn{{ $image->id }}">
                  <div class="edit-uploaded-image-cntr">
                    <div class="edit-uploaded-image clearfix"> <a href="#" data-tbId="{{ $image->id }}" class="delete imgDel"><i class="fa fa-close" style="font-size:18px;"></i> Remove</a> <span href="#" class="change-image">
                      {!! Form::radio('featured', '',($image->featured==1 ? true : false), ['data-tbId'=>$image->id,'class'=>"featImgSel"]) !!}
                      Featured</span>
                      <!-- <a href="#" class="change-image"><i class="fa fa-upload"></i></a> -->
                    </div>
                  </div>
                  <?php
                    $class="";
                    if($image->featured==1){
                      $class="active";
                    }
                  ?>
                  <div class="uploaded-image {{ $class }}" id="featuredId{{ $image->id }}">
                    <!-- <span>Featured Image:<input data-tbId="{{ $image->id }}" type="radio" class="featImgSel" name="featured" /></span> -->
                    <img src="{{ HTML::getPropertyImage('',$image->image_name ) }}"> </div>
                </div>
                <div id="noImgMessage" style="display: none;"><i class="fa fa-file-picture-o"></i><br></div>
                @empty
                <div class="no-image"> </br>No Images uploaded</div>
                @endforelse
              </div>
            </div>
          </div>
          <!-- image upload ends here -->

          <div class="row" style="display:none">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {!! Form::submit('Save', ['class' => 'btn btn-primary add-button start', 'id' => 'submitActlBtn' ]) !!}
                 {!! Form::submit('Cancel', ['class' => 'btn cancel-button', 'id' => 'cancelActlBtn' ]) !!} </div>
          </div>
          {!! Form::close() !!}

          {!! Form::model('',['method' => 'POST','class'=>'add-doc','id'=>'propertyDoc','route'=>['property.postedit',$propertyList->id],'files'=>true]) !!}
          <div class="row">
            <div class="col-xs-12">
              <div class="box-heads">
                <h4>Uploaded Documents</h4>
              </div>
              <span class="btn btn-success fileinput-button"> <i class="glyphicon glyphicon-plus"></i> <span>Add Documents...</span>
              <input id="upload_doc" type="file" name="files[]" multiple>
              </span> </div>
            <div class="col-lg-5 fileupload-progress fade">
              <!-- The global progress bar -->
              <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
              </div>
              <!-- The extended global progress state -->
              <div class="progress-extended">&nbsp;</div>
            </div>

            <table role="presentation" class="table table-striped">
              <tbody class="files">
              </tbody>
            </table>
            <div class="row">
              <div class="col-xs-12" id="showMsgDoc" style="color:green"></div>
              <!-- The template to display files available for upload -->
              <script id="template-upload" type="text/x-tmpl">
                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                            <tr class="template-upload fade">
                                <td>
                                    <span class="preview"></span>
                                </td>
                                <td>
                                    <p class="name">{%=file.name%}</p>
                                    <strong class="error text-danger"></strong>
                                </td>
                                <td>
                                    <p class="size">Processing...</p>
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                                </td>
                                <td>
                                    {% if (!i && !o.options.autoUpload) { %}
                                        <button class="btn btn-primary start" disabled>
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <span>Start</span>
                                        </button>
                                    {% } %}
                                    {% if (!i) { %}
                                        <button class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    {% } %}
                                </td>
                            </tr>
                        {% } %}
                        </script>
              <!-- The template to display files available for download -->
              <script id="template-download" type="text/x-tmpl">
                        {% for (var i=0, file; file=o.files[i]; i++) { %}
                            <tr class="template-download fade">
                                <td>
                                    <span class="preview">
                                        {% if (file.thumbnailUrl) { %}
                                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                                        {% } %}
                                    </span>
                                </td>
                                <td>
                                    <p class="name">
                                        {% if (file.url) { %}
                                            <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                                        {% } else { %}
                                            <span>{%=file.name%}</span>
                                        {% } %}
                                    </p>
                                    {% if (file.error) { %}
                                        <div><span class="label label-danger">Error</span> {%=file.error%}</div>
                                    {% } %}
                                </td>
                                <td>
                                    {% if (file.deleteUrl) { %}
                                        <button class="btn btn-danger delete 123" data-name="{%=file.name%}" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Delete</span>
                                        </button>
                                    {% } else { %}
                                        <button class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Cancel</span>
                                        </button>
                                    {% } %}
                                </td>
                            </tr>
                        {% } %}
                        </script>
            </div>
          </div>
          <div class="view-property">
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <table class="table table-striped">
                  @forelse( $propertyList->property_docs as $key => $doc )
                  <tr class="docRow" id="docRow{{ $doc->id }}">
                    <td><a href="{{ asset('public/uploads/properties').'/'.$doc->doc_name }}">Document {{ ($key+1) }}</a></td>
                    <td><a href="#"><i class="fa fa-upload"></i></a></td>
                    <td><a href="#" data-tbId="{{ $doc->id }}" class="docDel"><i class="fa fa-close"></i></a></td>
                  </tr>
                  <tr id="noDocMessage" style="display: none;">
                    <td colspan="2"></td>
                  </tr>
                  @empty
                  <tr>
                    <td>No Doc uploaded!</td>
                  </tr>
                  @endforelse
                </table>
              </div>
            </div>
          </div>

          <!-- doc upload ends here -->
          {!! Form::close() !!}
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> {!! Form::submit('Save', ['class' => 'btn btn-primary add-button start propAddBtn', 'id' => 'submitDummyBtn','style'=>'background-color: #00acb8 !important']) !!}
              {!! Form::submit('Delete', ['class' => 'btn delete-button', 'id' => 'deleteDummyBtn','style'=>'background-color: #e85862 !important;border: 0 none !important; border-radius: 0px !important;font-size: 12px;margin-right: 10px;padding: 8px 10px;color:#FFFFFF']) !!}
               </div>
          </div>
        </div>
      </div>
      <!-- ./col -->
    </div>
    <!-- /.row -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
      @section('page-scripts')

      <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>

<!-- -------google location css-js files------------------- -->
<script src="{{ asset('public/plugins/blueimp/js/main.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
<link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
<script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places,geometry"></script>

<script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>

<!-- -------google location css-js files------------------- -->
<script type="text/javascript">
	var statusFlag = 0;
	var statusLocation =0;
	function calculateDistance()
	{

		if($('#schoolCalc').val() == '')
		{
			return false;
		}
		else if($('#hdPlace').val() == '')
		{
			return false;
		}

		var schoolLs = $('#schoolCalc').val();
		var schoolArray = schoolLs.split(",");

		var hdPlace = $('#hdPlace').val();
		var placeArray = hdPlace.split("~");

		var p1 = new google.maps.LatLng(schoolArray[0],schoolArray[1]);
		var p2 = new google.maps.LatLng(placeArray[0],placeArray[1]);
		//console.log(p1,p2);
		var distance =  (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000);
		distance = (distance * 0.621371).toFixed(2);

		$('#distanceFromSchool').val(distance);

	}

	$( document ).ready(function(){


		$( "#schoolname" ).change(function() {

			statusFlag = 1;
		var schoolId = $('#schoolname').val();
		$.ajax({
			type: "GET",
			url : "{{route('index.ajaxLocate')}}",
			dataType :"json",
			data : "schoolId="+schoolId,

			success: function(data){
				//console.log(data);

				$('#schoolCalc').val(data.latitude+','+data.longitude);

				calculateDistance();


			}
		});
	});





		$( "#deleteDummyBtn" ).on( "click", function() {
			 var propertyId = "{{ $id }}";
			 //alert (propertyId);
			 bootbox.confirm("ARE YOU SURE YOU WANT TO DELETE THIS PROPERTY?", function(result){
			  if(result){
			   window.location.href = "{{  route('property.deleteProperty',$id) }}";
		      }
		  });
			return false;
		});
	});

	var data="{{ $propertyList->amenities }}";
			var dataarray=data.split(",");
			$("#aminity").val(dataarray);
			//$("#aminity").multiselect("refresh");

	$('select[multiple]').multiselect({
			columns: 1,
			placeholder: 'Select options'
		});

	var skillFlag = '{{ $skillsFlag}}';
if( window.location.hostname == 'localhost'){
  var baseUrl = 'http://localhost/tenantudev/';
 }else if( window.location.hostname == 'fodof.net') {
  var baseUrl = 'http://fodof.net/tenantu/';
 }

  var jqxhr='';
 $("#location").tagit({
        allowSpaces: true,
        fieldName: "location[]",
        tagLimit: 1,
        autocomplete: {
          delay: 0,
          minLength: 2,
          source: function(request, response) {
            var callback = function (predictions, status) {
              if (status != google.maps.places.PlacesServiceStatus.OK) {
                return;
              }
              var data = $.map(predictions, function(item) {
                return item.description;
              });
              response(data);
            }
            var service = new google.maps.places.AutocompleteService();
            service.getQueryPredictions({ input: request.term }, callback);
          }
        },
        beforeTagAdded: function (event, ui) {

			if(statusFlag || statusLocation)
			{
				getGeoLocations(ui.tagLabel,false);
			}
		},
		beforeTagRemoved: function (event, ui) {
			if(statusFlag){
				getGeoLocations(ui.tagLabel,true);
			}
		}
      });
      statusLocation = 1;
      $("#courseLocation").tagit({
        fieldName: "location[]"
      });
      /*$('#allowSpacesTags').tagit({
                allowSpaces: true,
                fieldName: "amenities[]"
      });*/
      var skills = ( skillFlag == true ) ? [1] : [];




        function getGeoLocations(address, remove) {

                    if (address === '') {
                        return false;
                    }

                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var location = results[0].geometry.location.lat() + '~' + results[0].geometry.location.lng()
                            var storedLocation = $("#hdPlace").val();
                            if (remove !== true) {
                                if (storedLocation === '') {
                                    $("#hdPlace").val(location);
                                    calculateDistance();
                                } else {
                                    $("#hdPlace").val(storedLocation + ',' + location);
                                }
                            }else{

                                storedLocation = storedLocation.replace(location,'');
                                storedLocation = storedLocation.replace(',,',',');
                                $("#hdPlace").val(storedLocation)

                            }

                        }

                    });
                    return false;
                }
                 $('#courseLocation .ui-autocomplete-input').attr('placeholder','Cities, States, Countries, or Zip/Postal Codes');
                 </script>
@endsection
