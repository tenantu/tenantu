@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

    @include('elements.breadcrump-landlord')
    @if (Session::has('message-success'))
          @include('elements.message-success')
    @endif
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="col-lg-12">
            <div class="row">
              <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-4">
                <div class="radio-container radio">
                    <form action="">
                      <input type="radio" name="display-prop-type" value="single-properties" checked="checked">Single Properties
                      <br>
                      <input type="radio" name="display-prop-type" value="apartments">Apartment Complexes
                    </form>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">
                  <a href="{{ route('property.add') }}"><button class="btn btn-primary pull-right"> Click to add more properties </button></a>
                </div>
              </div>
            </div>
          </div>
          <div class="single-property-content">
          <div class="row">
            <div>
              @if (count($properties) != 0)
              <table class="table table-hover">
                  <tr>
                      <th>Address</th>
                      <th>Bedrooms</th>
                      <th>Bathrooms</th>
                      <th>Edit Property Info</th>
                  </tr>
                @endif
                  @forelse($properties as $property)
                    <tr>
                      <td>{!! $property->thoroughfare !!}</td>
                      <td>{!! $property->bedroom_no !!}</td>
                      <td>{!! $property->bathroom_no !!}</td>
                      <td><a href="{{ route('property.edit',$property->id) }}" class="buttons"><i class="fa fa-pencil"></i> Edit Info</a></td>
                    </tr>
                  @empty
                    <p>You have no properties uploaded!</p>
                  @endforelse
                @if (count($properties) != 0)
              </table>
            @endif
            </div>

          </div><!-- /.row -->
        <!--end single property content -->
        </div>

        <div class="apartment-content">
          @if(count($apartmentBuildings) != 0)
            <table class="table table-hover">
              <tr>
                  <th>Apartment Building Name</th>
                  <th>Address</th>
                  <th>Number of Units</th>
                  <th>Edit Individual Units</th>
              </tr>
          @endif
          @forelse($apartmentBuildings as $apartment)
            @if($apartment->property_physical->count() > 0)
            <tr>
              <td>{!! $apartment->building_name !!}</td>
              <td>
                  @if(!empty($apartment->property_physical->first()))
                      {!! $apartment->property_physical->first()->thoroughfare !!}
                  @else
                    Please add units to this complex to view its address.
                  @endif
              </td>
              <td>{!! count($apartment->property_physical) !!}</td>
              <td>
                <a href="{{ route('landlords.getEditApartmentUnits',$apartment->id) }}" class="buttons"><i class="fa fa-pencil"></i> Edit Info</a>
              </td>
            </tr>
          @endif
          @empty
            <p>You have no apartments uploaded!</p>
          @endforelse
          @if(count($apartmentBuildings) != 0)
            </table>
          @endif
        </div>


        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                  document.addEventListener("DOMContentLoaded", function(event) {
                    var singlePropContent = $('.single-property-content'),
                    apartmentContent = $('.apartment-content'),
                    group = $('input[type=radio][name=display-prop-type]');

                    group.change(function() {
                      singlePropContent.toggle(group.filter(':checked').val() === 'single-properties');
                      apartmentContent.toggle(group.filter(':checked').val() === 'apartments');
                    }).change();

                  });


                </script>
      @endsection
