@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

                @include('elements.breadcrump-landlord')
        <!-- Main content -->
        <section class="content">
        @if (Session::has('message-success'))
        @include('elements.message-success')
        @endif
        @if (Session::has('message-error'))
            <div class="alert alert-danger">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <strong>Error!</strong> {{ Session::get('message-error') }}
            </div>
        @endif
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
              <!-- Poist property -->
              <div class="tnt-box landlord-add-propety">
                <div class="box-heads">
                  <h4>Post Property</h4>
                </div>

                {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm','route'=>['property.postadd'],'files'=>true, 'onsubmit' => 'load_address']) !!}
                {!! Form::hidden('img_name', '', ['class'=>'form-control login-input','id'=>'imgName']) !!}
                {{--{!! Form::hidden('hdPlace', '', ['id'=>'hdPlace']) !!}--}}
                  <div class="row">
                                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-cs-12">
                    <div class="radio-container radio">
                   <label class="checkbox-text">
                    {!! Form::radio('propAddType', 'addSingleProperty',true) !!}
                    <span class="">Single property add</span>
                    </label>
                        <label class="checkbox-text">
                            {!! Form::radio('propAddType', 'addApartments') !!}
                            <span class="">Apartment Complex Add</span>
                        </label>
                    <label class="checkbox-text">
                   {!! Form::radio('propAddType', 'massAddProperty') !!}
                    <span class="">Multiple properties add</span>
                    </label>
                    <br>
                    </div>
                    </div>
                    <div class="singlePropContent">



                    <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('schoolname', 'School name:') !!}<span style="color: red;">*</span>
                      <div class="select-box">
                      {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'schoolname','required']) !!}
                      </div>
                    </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" id="locationField">
                            {!! Form::label('full_address_autocomplete', 'Address:') !!}<span style="color: red;">*</span>
                            {!! Form::text('full_address_autocomplete', Input::old('administrative_area'), ['class'=>'form-control login-input','id'=>'autocomplete','placeholder'=>'Address','required']) !!}
                        </div>


                    {!! Form::hidden('schoolCalc', '', array('id' => 'schoolCalc')) !!}
                     <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('bedroom', 'Bedrooms:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','bedroom_no', Input::old('bedroom_no'), ['class'=>'form-control login-input','id'=>'bedroomNo','placeholder'=>'Bedrooms','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('bathroom', 'Bathrooms:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','bathroom_no', Input::old('bathroom_no'), ['class'=>'form-control login-input','id'=>' bathroomNo','placeholder'=>'Bathrooms','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('charge_per_bed', 'Charge Option:') !!}<span style="color: red;">*</span>
                      {!! Form::select('charge_per_bed', ['0' => 'Charge Per Unit', '1' => 'Charge Per Bed'], '', ['class' => 'form-control login-input', 'id'=>'charge_per_bed']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('max_occupancy', 'Max Occupancy:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number', 'max_occupancy', '', ['class'=>'form-control login-input', 'id'=>'maxOccupancy', 'required']) !!}
                    </div>
                     <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('rentprice', 'Rent:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','rent_price', Input::old('rent_price'), ['class'=>'form-control login-input','id'=>'rentPrice','placeholder'=>'Rent','required']) !!}
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('amenities', 'Amenities:') !!} (Select items from the options)<span style="color: red;"></span>
                      <p></p>
                        {!! Form::select('aminity[]',$aminities,'', ['class'=>'form-control login-input','id'=>'aminity','multiple']) !!}
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('Property images and documents', 'Property images and documents:') !!}<span style="color: red;"></span>
                      <div class="bg-danger">
                        After saving, you can add the images and documents
                        </div>
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('description', 'Property description:') !!}<span style="color: red;">*</span>
                      {!! Form::textarea('description', Input::old('description'), ['class'=>'form-control login-input','id'=>'description','placeholder'=>'Description','rows'=>'3','required']) !!}
                    </div>
					 <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {!! Form::label('communicationMedium', 'Communication Medium:') !!}<span style="color: red;">*</span>
                    <div class="radio-container">
                  <div class="radio">
                    <label>
                      {!! Form::radio('communication_medium', 'W', ['id'=>'optionsRadios1']) !!}
                      <span class="radio-text">Web</span>
                    </label>
                  </div>

                  <div class="radio">
                      <label>
                        {!! Form::radio('communication_medium', 'E', ['id'=>'optionsRadios2','required']) !!}
                        <span class="radio-text">Email</span>
                      </label>
                  </div>
				  </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary add-button start']) !!}
                    </div>
                  </div>
                        {!! Form::close() !!}

                    </div>






         <div class="apartmentBuildingAdd">
             {!! Form::open(['route' => 'property.postAddApartments']) !!}

             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::label('Enter the name of this apartment complex', 'Enter the name of this apartment complex:') !!}
                 {!! Form::text('apartment_building_name', '', ['class'=>'form-control login-input','id'=>'apartment_building_name','placeholder'=>'Apartment Complex Name','required']) !!}
             </div>

             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::label('schoolname', 'School name:') !!}<span style="color: red;">*</span>
                 <div class="select-box">
                     {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'schoolname','required']) !!}
                 </div>
             </div>
             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" id="locationField">
                 {!! Form::label('full_address_autocomplete', 'Address of Building:') !!}<span style="color: red;">*</span>
                 {!! Form::text('full_address_autocomplete', Input::old('administrative_area'), ['class'=>'form-control login-input','id'=>'autocomplete_apartment','placeholder'=>'Address','required']) !!}
             </div>

             <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
               {!! Form::label('charge_per_bed', 'Charge Option:') !!}<span style="color: red;">*</span>
               {!! Form::select('charge_per_bed', ['0' => 'Charge Per Unit', '1' => 'Charge Per Bed'], '', ['class' => 'form-control login-input', 'id'=>'charge_per_bed']) !!}
             </div>
             {!! Form::hidden('schoolCalc', '', array('id' => 'schoolCalc')) !!}
             {{--<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
                 {{--{!! Form::label('bedroom', 'Bedrooms:') !!}<span style="color: red;">*</span>--}}
                 {{--{!! Form::input('number','bedroom_no', Input::old('bedroom_no'), ['class'=>'form-control login-input','id'=>'bedroomNo','placeholder'=>'Bedrooms','required']) !!}--}}
             {{--</div>--}}
             {{--<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
                 {{--{!! Form::label('bathroom', 'Bathrooms:') !!}<span style="color: red;">*</span>--}}
                 {{--{!! Form::input('number','  bathroom_no', Input::old('bathroom_no'), ['class'=>'form-control login-input','id'=>' bathroomNo','placeholder'=>'Bathrooms','required']) !!}--}}
             {{--</div>--}}
             {{--<div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
                 {{--{!! Form::label('rentprice', 'Rent:') !!}<span style="color: red;">*</span>--}}
                 {{--{!! Form::input('number','rent_price', Input::old('rent_price'), ['class'=>'form-control login-input','id'=>'rentPrice','placeholder'=>'Rent','required']) !!}--}}
             {{--</div>--}}

             {{--<div id="dynamicInput">--}}
                 {{--Entry 1<br><input type="text" name="myInputs[]">--}}
             {{--</div>--}}
             {{--<input type="button" value="Add another text input" onClick="addInput('dynamicInput');">--}}

             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive">
                <div class="form-group">
                 <table id="unit-data-table" class="table">

                     <thead class="thead-default">
                        <tr>
                            <th>Unit Number</th>
                            <th>Bedroom Count</th>
                            <th>Bathroom Count</th>
                            <th>Rent Price</th>
                            <th>Max Occupancy</th>
                        </tr>
                     </thead>
                     <tbody id="unit-data-table-body">

                     <tr id="row">
                         <td><input type="number" name="unit_number[][" class="apartment-data-input-long"/> </td>
                         <td><input type="number" name="bedroom_count[]" class="apartment-data-input-short"/> </td>
                         <td><input type="number" name="bathroom_count[]" class="apartment-data-input-short"/></td>
                         <td><input type="number" name="rent[]" class="apartment-data-input-long"/></td>
                         <td><input type="number" name="max_occupancy[]" class="apartment-data-input-short"/></td>
                         <td><button id="b1" class="btn add-more" type="button" onClick="addInput('unit-data-table-body');">+</button> </td>

                     </tr>
                     </tbody>
                 </table>
                </div>
             </div>


             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::label('amenities', 'Amenities:') !!} (Select items from the options)<span style="color: red;"></span>
                 <p></p>
                 {!! Form::select('aminity[]',$aminities,'', ['class'=>'form-control login-input','id'=>'aminity','multiple']) !!}
             </div>



             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::label('Property images and documents', 'Property images and documents:') !!}<span style="color: red;"></span>
                 <div class="bg-danger">
                     After saving, you can add the images and documents
                 </div>
             </div>

             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::label('description', 'Property description:') !!}<span style="color: red;">*</span>
                 {!! Form::textarea('description', Input::old('description'), ['class'=>'form-control login-input','id'=>'description','placeholder'=>'Description','rows'=>'3','required']) !!}
             </div>
             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 {!! Form::label('communicationMedium', 'Communication Medium:') !!}<span style="color: red;">*</span>
                 <div class="radio-container">
                     <div class="radio">
                         <label>
                             {!! Form::radio('communication_medium', 'W', ['id'=>'optionsRadios1']) !!}
                             <span class="radio-text">Web</span>
                         </label>
                     </div>

                     <div class="radio">
                         <label>
                             {!! Form::radio('communication_medium', 'E', ['id'=>'optionsRadios2','required']) !!}
                             <span class="radio-text">Email</span>
                         </label>
                     </div>
                 </div>
             </div>


             <div class="row">

                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     {!! Form::submit('Save', ['class' => 'btn btn-primary add-button start']) !!}

                 </div>
             </div>
             {!! Form::close() !!}
         </div>









		<div class="">
                <div class="multiPropertyContent">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 add-tenants">
<div class=”form-group”>
<b>Uploading Multiple Properties</b><br><br>

To upload multiple properties at once, follow the steps listed below. <br>
If adding units to an apartment complex, if you have not yet, click the “Enter New Apartment Complex.” <br>
Click the “Download .csv Form”<br><br>
</div>
<div class=”form-group”>
<b>Apartment ID:</b> <u>Optional</u><br>
When adding units to an apartment complex state the corresponding “Apartment Complex ID” provided in the table below these instructions.
<br>
<br>
</div>
<div class=”form-group”>
<b>Unit Number:</b> <u>Optional</u><br>
This is where you would state the unit number when adding units to an apartment complex.
<br>
<br>
</div>
<div class=”form-group”>
<b>Property Title:</b> <u>Optional</u><br>
This is where you would state the name of the property if it differs from the address (i.e. the name of an apartment complex). The title listed here will be displayed in its listing. If you leave this field blank, the address you provide will substitute for the title.
<br>
<br>
</div>
<div class=”form-group”>
<b>Street Address:</b> <u>Required</u><br>
State here the number and name of the street in which the property is located.
<br>
<br>
</div>
<div class=”form-group”>
<b>State/Province:</b> <u>Required</u><br>
State here the state/province in which the property is located.
<br>
<br>
</div>
<div class=”form-group”>
<b>City/Town:</b> <u>Required</u><br>
State here the city/town in which the property is located.
<br>
<br>
</div>
<div class=”form-group”>
<b>Bedrooms:</b> <u>Required</u><br>
State the number of bedrooms here.
<br>
<br>
</div>
<div class=”form-group”>
<b>Bathrooms:</b> <u>Required</u><br>
State the number of bathrooms here.
<i>Note: If a half-bath is included (i.e. bathroom without a shower), list it as a full-bath.</i>
<br>
<br>
</div>
<div class=”form-group”>
<b>Per-bed/Per-unit:</b> <u>Required</u><br>
If you require rent to be paid at once by the entire unit, state “unit”. If you require each tenant to be responsible for <i>only their portion</i>, state “bed”.
<br>
<br>
</div>
<div class=”form-group”>
<b>Maximum Occupancy:</b> <u>Required</u><br>
State the number of tenants legally allowed to live in the unit.
<br>
<br>
</div>
<div class=”form-group”>
<b>Rent:</b> <u>Required</u><br>
State the amount of rent due each month. If the unit is listed as “Per-unit”, state the total amount due each month. If the unit is listed as “Per-bed”, state the amount due for an individual tenant.
<br>
<br>
</div>
<div class=”form-group”>
<b>Property Description:</b> <u>Optional</u><br>
In this section, you can write a brief description about the property.
<br>
<br>
</div>
<div class=”form-group”>
<b>Amenities:</b> <u>Optional</u><br>
In this section, you can list the available amenities. A list of amenities to choose from are listed below. Please separate amenities with a semi-colon (i.e. Backyard;Balcony).
<br>
<br>
</div>
<div class=”form-group”>
<b>School:</b> <u>Optional</u><br>
In this section, state the school in which you would like your properties marketed to. Please state the entire name of the school. (i.e. University of Delaware). If you leave this field blank, your properties will automatically be set to the school in which you signed up under.
<br>
<br>
</div>
<div class=”form-group”>
If there are any errors in your input you will be returned a document which will show you where the errors occurred. If any further help is necessary contact us at support@tenantu.com<br><br>
</div>
			<span class="btn btn-success fileinput-button" data-toggle="modal" data-target=".upload-apartment-complex" style="cursor: pointer;"<i class="glyphicon glyphicon-plus"></i>Enter New Apartment Complex</span>
				{!! Form::open(['class'=>'edit.tnt-form', 'route'=>['landlords.postApartmentComplexWithoutUnits']])  !!}
				<div class="modal fade bs-example-modal-sm upload-apartment-complex" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				<div class="modal-dialog modal-sm">
				<div class="modal-content">
				<div class="modal-header">
				<button aria-label="Close" data-dismiss="modal" id="closeModal" class="close" type="button"><span aria-hidden="true">X</span>
				</button>
					<h4 id="mySmallModalLabel" class="modal-title">Enter the name of the new apartment complex:</h4>
				</div>
				<div class="modal-body">
					{!! Form::text('Apartment Complex Name', '', ['required'])  !!}
					<br>
				</div>
        <div class="modal-footer">
          {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}

        </div>
				</div>
				</div>
				{!! Form::close()  !!}
				</div>

<div id="dlUlButtons">
<a href=" {{ route('landlords.downloadPropertyCsv') }} " style="color: white;display:inline-block;" class="btn add-btn" type="submit">Download .csv Form</a>
<span class="btn btn-success fileinput-button" data-toggle="modal" data-target=".UploadCsv" style="cursor: pointer;"><i class="glyphicon glyphicon-plus"></i>Upload .csv File</span>
                {!! Form::open(['class'=>'edit-tnt-form','route'=>['landlords.postPropertyCsv'],'files'=>true]) !!}
                <div class="modal fade bs-example-modal-sm UploadCsv" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button aria-label="Close" data-dismiss="modal" id="closeModal"  class="close" type="button"><span aria-hidden="true">X</span></button>
              <h4 id="mySmallModalLabel" class="modal-title">Upload .csv File</h4>
            </div>
            <div class="modal-body">
               {!! Form::file('csv', ['id' => 'csv']) !!}
                <br>
               <button type="submit" id="submitModal" class="btn btn-primary">Submit</button>
            </div>
            </div>
          </div>
        </div>
	<div class="amenitiesTable">
	</div>
	<div class="apartmentTable">
	</div>
	</div>
</div>
{!! Form::close() !!}
</div>

                 </div>

                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
              <!-- small box -->
              <div class="tnt-box view-property add-view-property">
                <div>
                  <div class="box-heads">
                    <h4>View Properties</h4>
                    <a href="#" class="blue">More »</a>
                  </div>
                  <ol>

                  @if($properties->count(0))
                     @foreach($properties as $key => $property)
                  <li>
                  <span class="number">{{ ($key+1) }}.</span>{{ $property->property_name }}
                  <a href="{{ route('property.edit',Crypt::encrypt($property->id)) }}" class="pull-right">Details</a></li>
                  <li>
                    @endforeach
                    @else
                      I don't have any records!
                  @endif

                </ol>
                </div>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      @endsection
      @section('page-scripts')
 <!-- -------google location css-js files------------------- -->
 <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
 <link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css">
 <link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css">
 <link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">

<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>

 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
 <script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places,geometry"></script> -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxLBULPEdVBHFNWT885GNx_C4y0W3g-_s&sensor=true&libraries=places,geometry"></script>
<!-- -------google location css-js files------------------- -->


  <script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>

 <script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
var conditionalContentA = $('.singlePropContent'),
 conditionalContentB = $('.multiPropertyContent'),
 conditionalContentC = $('.apartmentBuildingAdd'),
 group = $('input[type=radio][name=propAddType]');
// binding the change event-handler:
group.change(function() {
  // toggling the visibility of the conditionalContent, which will
  // be shown if the assessment returns true and hidden otherwise:
  conditionalContentA.toggle(group.filter(':checked').val() === 'addSingleProperty');
  conditionalContentB.toggle(group.filter(':checked').val() === 'massAddProperty');
  conditionalContentC.toggle(group.filter(':checked').val() === 'addApartments');
  // triggering the change event on the group, to appropriately show/hide
  // the conditionalContent on page-load/DOM-ready:
}).change();
});


	$( "#schoolname" ).change(function() {
		var schoolId = $('#schoolname').val();
		$.ajax({
			type: "GET",
			url : "{{route('index.ajaxLocate')}}",
			dataType :"json",
			data : "schoolId="+schoolId,

			success: function(data){
				//console.log(data);

				$('#schoolCalc').val(data.latitude+','+data.longitude);

				calculateDistance();


			}
		});
	});

	function calculateDistance()
	{

		if($('#schoolCalc').val() == '')
		{
			return false;
		}
		else if($('#hdPlace').val() == '')
		{
			return false;
		}

		var schoolLs = $('#schoolCalc').val();
		var schoolArray = schoolLs.split(",");

		var hdPlace = $('#hdPlace').val();
		var placeArray = hdPlace.split("~");

		var p1 = new google.maps.LatLng(schoolArray[0],schoolArray[1]);
		var p2 = new google.maps.LatLng(placeArray[0],placeArray[1]);
		var distance =  (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000);
		distance = (distance * 0.621371).toFixed(2);
		$('#distanceFromSchool').val(distance);

	}

function buildHiddenAddressFieldForDistanceLookup() {
	    var fullAddress = document.getElementById('street_address') + " " + document.getElementById('city') + " " + document.getElementById('state') + " " + document.getElementById('zip_code');
	    document.getElementById('hdPlace').value = fullAddress;
	    return fullAddress;
}

 if( window.location.hostname == 'localhost'){
  var baseUrl = 'http://localhost/tenantudev/';
 }else if( window.location.hostname == 'fodof.net') {
  var baseUrl = 'http://fodof.net/tenantu/';
 }
  var jqxhr='';
 //var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'ahdPlacesp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
 $("#hdPlace").tagit({
        allowSpaces: true,
        fieldName: "hdPlace[]",
        tagLimit: 1,
        autocomplete: {
          delay: 0,
          minLength: 2,
          source: function(request, response) {
            var callback = function (predictions, status) {
              if (status != google.maps.places.PlacesServiceStatus.OK) {
                return;
              }
              var data = $.map(predictions, function(item) {
                return item.description;
              });
              response(data);
            }
            var service = new google.maps.places.AutocompleteService();
            service.getQueryPredictions({ input: request.term }, callback);
          }

        },
        beforeTagAdded: function (event, ui) {
                        getGeoLocations(ui.tagLabel,false);
                    },


                    beforeTagRemoved: function (event, ui) {
                        // do something special
                        console.log(ui.tagLabel);
                        getGeoLocations(ui.tagLabel,true);
                    }
      });

      $("#courseLocation").tagit({
        fieldName: "full_address[]"
      });

      var jqxhr='';
      var skills= [];

     $('select[multiple]').multiselect({
			columns: 1,
			placeholder: 'Select options'
		});
//
//
        function getGeoLocations(address, remove) {

                    if (address === '') {
                        return false;
                    }

                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var location = results[0].geometry.location.lat() + '~' + results[0].geometry.location.lng()
                            var storedLocation = $("#hdPlace").val();
                            if (remove !== true) {
                                if (storedLocation === '') {
                                    $("#hdPlace").val(location);
                                    calculateDistance();
                                } else {
                                    $("#hdPlace").val(storedLocation + ',' + location);
                                }
                            }else{

                                storedLocation = storedLocation.replace(location,'');
                                storedLocation = storedLocation.replace(',,',',');
                                $("#hdPlace").val(storedLocation)
                            }

                        }

                    });
                    return false;
                }
var amenitiesArray = [
@foreach ($aminities as $amenity)
        [ "{{$amenity}}"],
@endforeach
];

var apartmentArray = [
	@foreach ($apartments as $apartment)

		["{{$apartment['id']}}", "{{$apartment['building_name']}}"],
	@endforeach

];


var autocomplete;
function initialize() {
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
        { types: ['geocode'] });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
    });
}

function initialize_apartment() {
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {HTMLInputElement} */(document.getElementById('autocomplete_apartment')),
        { types: ['geocode'] });
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
    });
}


window.onload = initialize();
window.onload = initialize_apartment();
function apartmentTableCreate() {
	var tbl = document.createElement('table');
	tbl.style.width = '300px';
	tbl.style.border = '1px solid black';
	tbl.style.marginRight = '20px';
	tbl.style.marginLeft = '20px';
	tbl.style.marginTop = '20px';
	tbl.style.marginBottom = '20px';
	$(tbl).addClass("form-group");
	for (var i = 0; i < apartmentArray.length+1; i++) {
		var tr = tbl.insertRow();
		var tdId = tr.insertCell();
		var tdName = tr.insertCell();
		if (i == 0) {
			tdId.innerHTML = "<b>Apartment Complex ID </b>";
			tdName.innerHTML = "<b>Apartment Complex Name</b>";
		}
		else {
			tdId.innerHTML = apartmentArray[i-1][0];
			tdName.innerHTML = apartmentArray[i-1][1];
		}
		tdId.style.border = '1px solid black';
		tdName.style.border = '1px solid black';
	}
	$(".apartmentTable").append(tbl);
}
apartmentTableCreate();
function amenitiesTableCreate(){
		var tbl  = document.createElement('table');
        tbl.style.width  = '300px';
        tbl.style.border = '1px solid black';
        tbl.style.marginRight = '20px';
        tbl.style.marginLeft = '20px';
        tbl.style.marginTop = '20px';
        tbl.style.marginBottom = '20px';
        $(tbl).addClass("form-group");
        for(var i = 0; i < amenitiesArray.length+1; i++){
		var tr = tbl.insertRow();
                var td = tr.insertCell();
                if(i==0){
                td.innerHTML = "<b>Available Amenities</b>";
                }
                else{
                td.innerHTML = amenitiesArray[i-1][0];
                }
                td.style.border = '1px solid black';
        }
	$(".amenitiesTable").append(tbl);
}
 amenitiesTableCreate();
	$("#submitModal").click(function() {
  		$("#closeModal").click();
	});

                 $('#courseLocation .ui-autocomplete-input').attr('placeholder','Cities, States, Countries, or Zip/Postal Codes');

var counter = 1;
function addInput(divName){
        var newTableRow = document.createElement('tr');
        var newId = "row" + counter;
        newTableRow.setAttribute("id", newId);
        var newRow = '<tr id="row' + counter + '">';
        newRow += '<td><input type="number" name="unit_number[]" class="apartment-data-input-long" /> </td>';
        newRow += '<td><input type="number" name="bedroom_count[]" class="apartment-data-input-short" /> </td>';
        newRow+= '<td><input type="number" name="bathroom_count[]" class="apartment-data-input-short" /> </td>';
        newRow += '<td><input type="number" name="rent[]" class="apartment-data-input-long" /> </td>';
        newRow += '<td><input type="number" name="max_occupancy[]" class="apartment-data-input-short"/></td>';
        newRow += '</tr>';
        newTableRow.innerHTML = newRow;

        document.getElementById(divName).appendChild(newTableRow);
        counter++;
}
 </script>
          <style>
              table {
                  table-layout: fixed;
              }
              td {
                  width: 100%;
              }
            .apartment-data-input-short {
                width: 70%;
            }
              .apartment-data-input-long {
                  width: 80%;
              }
          </style>

      @endsection
