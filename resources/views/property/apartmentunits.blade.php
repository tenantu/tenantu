@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

    @include('elements.breadcrump-landlord')
    @if (Session::has('message-success'))
          @include('elements.message-success')
    @endif
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="form-group">
              <div class="col-lg-2">
                <p class="h3">{{$apartmentBuilding->building_name}}</p>
              </div>
              <div class="btn-toolbar col-lg-10">
                  <div class="btn-group pull-right">
                    <div class="row">
                  {{-- <span class="btn btn-primary fileinput-button col-lg-12" data-toggle="modal" data-target=".addUnits" style="cursor: pointer;" id="add_button"> --}}
                    <div class="col-lg-4">
                    <button class="btn btn-primary fileinput-button" data-toggle="modal" data-target=".addUnits" style="cursor: pointer;" id="add_button">
                      Add units  <i class="fa fa-plus" aria-hidden="true"></i></button>
                    </div>
                    <div class="col-lg-8">
                      <button class="btn btn-danger fileinput-button" data-toggle="modal" data-target=".removeApartmentComplex" style="cursor: pointer;" id="delete_button">
                        Delete Apartment Complex </button>

                </div>
                  </div>
                </div>
              </div>


          </div>
        </div>
          <div class="single-property-content">
          <div class="row">
            <div>
              @if (count($units) != 0)
              <table class="table table-hover">
                  <tr>
                      <th>Unit Number</th>
                      <th>Bedrooms</th>
                      <th>Bathrooms</th>
                      <th>Edit Unit Info</th>
                  </tr>
                @endif
                  @forelse($units as $unit)

                    <tr>
                      <td>#{!! $unit->premise !!}</td>
                      <td>{!! $unit->bedroom_no !!}</td>
                      <td>{!! $unit->bathroom_no !!}</td>
                      <td><a href="{{ route('property.edit',$unit->id) }}" class="buttons"><i class="fa fa-pencil"></i> Edit Info</a></td>
                    </tr>
                  @empty
                    <p>You have no properties uploaded!</p>
                  @endforelse
                @if (!empty($units))
              </table>
                  @endif
            </div>


          </div>
        </div>

        {!! Form::open(['class' => 'edit-tnt-form', 'route' => ['landlords.removeApartmentComplex']]) !!}
        <div class="modal fade bs-example-modal-sm removeApartmentComplex" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
				<button aria-label="Close" data-dismiss="modal" id="closeModal" class="close" type="button"><span aria-hidden="true">X</span>
				</button>
					<h4 id="mySmallModalLabel" class="modal-title">Are you sure you want to remove {!! $apartmentBuilding->building_name !!} from TenantU?</h4>
				</div>
				<div class="modal-body col-lg-12">

                  {!! Form::hidden('apartment_building_id', $apartmentBuilding->id) !!}

					<br>
				</div>
        <div class="modal-footer">
          <button aria-label="Close" data-dismiss="modal" id="closeModal" class="btn btn-primary" type="button"><span aria-hidden="true">Cancel</span></button>

        {!! Form::submit('Delete Apartment Complex', ['class' => 'btn btn-danger']) !!}

                </div>
                </div>
                </div>
        {!! Form::close() !!}
      </div>




        {!! Form::open(['class' => 'edit-tnt-form', 'route' => ['landlords.addUnitsToExistingApartment']]) !!}
        <div class="modal fade bs-example-modal-sm addUnits" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
				<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
				<button aria-label="Close" data-dismiss="modal" id="closeModal" class="close" type="button"><span aria-hidden="true">X</span>
				</button>
					<h4 id="mySmallModalLabel" class="modal-title">Enter the name of the new apartment complex:</h4>
				</div>
				<div class="modal-body col-lg-12">
          <table id="unit-data-table" class="table table-responsive">

              <thead class="thead-default">
                 <tr>
                     <th class="span2">Unit Number</th>
                     <th class="col-lg-2">Bedroom Count</th>
                     <th class="col-lg-2">Bathroom Count</th>
                     <th class="col-lg-2">Rent Price</th>
                     <th class="col-lg-2">Max Occupancy</th>
                 </tr>
              </thead>
              <tbody id="unit-data-table-body">

              <tr id="row">
                  <td><input type="number" name="unit_number[]" class="apartment-data-input-long form-control" required/> </td>
                  <td><input type="number" name="bedroom_count[]" class="apartment-data-input-short form-control" required/> </td>
                  <td><input type="number" name="bathroom_count[]" class="apartment-data-input-short form-control" required/></td>
                  <td><input type="number" name="rent[]" class="apartment-data-input-long form-control" required/></td>
                  <td><input type="number" name="max_occupancy[]" class="apartment-data-input-short form-control" required/></td>
                  <td><button id="b1" class="btn add-more" type="button" onClick="addInput('unit-data-table-body');">+</button> </td>
                  {!! Form::hidden('apartment_building_id', $apartmentBuilding->id) !!}
              </tr>
              </tbody>
          </table>
					<br>
				</div>
        <div class="modal-footer">
          {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}

        </div>
				</div>
				</div>
				{!! Form::close()  !!}
				</div>


          <div>

        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                  $("#submitModal").click(function() {
                  		$("#closeModal").click();
                	});
                  var counter = 1;
                  function addInput(divName){
                          var newTableRow = document.createElement('tr');
                          var newId = "row" + counter;
                          newTableRow.setAttribute("id", newId);
                          var newRow = '<tr id="row' + counter + '">';
                          newRow += '<td><input type="number" name="unit_number[]" class="apartment-data-input-long form-control" /> </td>';
                          newRow += '<td><input type="number" name="bedroom_count[]" class="apartment-data-input-short form-control"/> </td>';
                          newRow+= '<td><input type="number" name="bathroom_count[]" class="apartment-data-input-short form-control" /> </td>';
                          newRow += '<td><input type="number" name="rent[]" class="apartment-data-input-long form-control" /> </td>';
                          newRow += '<td><input type="number" name="max_occupancy[]" class="apartment-data-input-long form-control"'
                          newRow += '</tr>';
                          newTableRow.innerHTML = newRow;

                          document.getElementById(divName).appendChild(newTableRow);
                          counter++;
                  }

                </script>
                <style>
                  #add_button,
                  #delete_button {
                    display: inline-block;
                  }
                </style>
      @endsection
