@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	
	@include('elements.breadcrump-landlord')
	<!-- Main content -->
	<section class="content">
		<div class="single-property">
			<div class="reviews">
				<!-- repeat review with this row -->
				<!-- two reviews per column -->
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-4 select-property-text col-lg-offset-3 col-md-offset-3 col-sm-offset-2">
						Select Property
					</div>
					<div class="col-sm-4">
						{!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'reviewForm','route'=>['properties.review'],'files'=>true]) !!}
						<div class="select property">
							{!! Form::select('property_id',['' => 'Select property']+$propertyDropdown, $slug, ['class'=>'selectpicker show-tick form-control sort-select','id'=>'propertyId','required']) !!}
						</div>
						{!! Form::close() !!}
					</div>
				</div>
				<div class="row">
					<!-- comment single 1 -->
					<!-- //////////////// -->
					@forelse( $reviews as $review )
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 single-review1">
						<div class="tnt-box">
							<div class="user">
								<span class="name">{{ ucwords($review->firstname.' '.$review->lastname) }}</span>
								<span class="subject">Would Recommend! <a href="#">Tenantu.com</a></span>

							</div>
							<div><span style="float:right;">{{ $review->property_name }}</span></div>
							<div class="review-ratings row">
								@foreach( $ratings as $rating)
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="rating-texts">{{ $rating->rating_name }}<img src="{{ HTML::getAverageReviewRatingFromReviewId($review->property_id,$rating->id,$review->id) }}"></div>
								</div>
								@endforeach
							</div>
							<div class="review-text">
								{{ $review->review }}
							</div>
						</div>
					</div>
					@empty
						<p>No reviews!</p>
					@endforelse
				</div>
				<!-- comment single row end -->
				<!-- ////////////////////// -->
			</div>
		</div>
	</section>
</div>
@endsection
@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){
	$(document).on('change','#propertyId',function(){
		var propertyId = $(this).val();
		if(propertyId!=""){
			window.location.href="{{ route('properties.review') }}"+"/"+propertyId;
		}	
		else
		{
			window.location.href="{{ route('properties.review') }}";
		}
	});
});
</script>
@endsection 
