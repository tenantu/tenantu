@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
	<script type="text/javascript" src="{{ asset('public/js/tinymce/tinymce.min.js') }}"></script>
	<script type="text/javascript">
	  tinymce.init({
		selector : "textarea",
		plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
		toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	  }); 
	</script>
    {!! Form::model('',['method' => 'POST','route'=>['feedback.store'],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('ClientName', 'Client Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('clientname', Input::old('clientname'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('CompanyName', 'CompanyName:') !!}<span style="color: red;">*</span>
            {!! Form::text('companyname', Input::old('companyname'), ['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Content', 'Content:') !!}<span style="color: red;">*</span>
            {!! Form::textarea('content',Input::old('content'),['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::checkbox('status', 1, Input::old('status'), ['class'=>'']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
