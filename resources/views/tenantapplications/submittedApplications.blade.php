@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
@if (Session::has('warningMessage'))
   <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
@endif
<section class="content">
  @if(!$submittedApplications->isEmpty() or !$tenantSoloApplications->isEmpty())
    <table class="table">
      <thead>
        <th>Property</th>
        <th>Application Group</th>
        <th>Date Submitted</th>
        <th>Last Updated</th>
        <th>Status</th>
      </thead>
      @forelse($tenantSoloApplications as $soloApplication)
        <td>{!! $soloApplication->propertyPhysical->property_name !!}</td>
        <td><strong>N/A</strong></td>
        <td>{!! $soloApplication->created_at->format('M d Y') !!}</td>
        <td>{!! $soloApplication->updated_at->format('M d Y') !!}</td>
        <td>
          @if($soloApplication->status == 1)
            Submitted
          @elseif($soloApplication->status == 2)
            Approved! Schedule a time to sign your lease
          @elseif($soloApplication->status == 0)
            Denied
          @else
            Lease Signed
          @endif
        </td>
      @empty
      @endforelse
      @forelse($submittedApplications as $group)
        @if(!$group->applicationSubmission->isEmpty())
          <?php $submissions = $group->applicationSubmission; ?>
          @foreach($submissions as $application)
            <tr>
              <td>{!! $application->propertyPhysical->property_name !!}</td>
              <td>{!! $group->group_name !!}</td>
              <td>{!! $application->created_at->format('M d Y') !!}</td>
              <td>{!! $application->updated_at->format('M d Y') !!}</td>
              <td>
                @if($application->status == 1)
                  Submitted
                @elseif($application->status == 2)
                  Approved! Schedule a time to sign your lease
                @elseif($application->status == 0)
                  Denied
                @else
                  Lease Signed
                @endif
              </td>
            </tr>
          @endforeach
        @endif
      @empty
      @endforelse
    </table>
  @else
    <h3>You have not submitted any applications</h3>
  @endif
</section>

@endsection
