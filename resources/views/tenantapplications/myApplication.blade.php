@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
@if (Session::has('warningMessage'))
   <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
@endif
<section class="content">
  {!! Form::open(['route' => 'tenants.updateMyApplication']) !!}
  @foreach($categorizedQuestions as $category => $questions)
    <h2 class='page-header'>{!! $category !!}</h2>
    @foreach($questions as $question)
      <div class="row">
        <div class="col-lg-6">
          <p class="col-lg-6">{!! $question['question'] !!}
            @if($question['required'] == 1)
            <span style="color:red;">*</span>
          @endif</p>

        </div>
        <div class="col-lg-6">
          @if($question['required'] == 1)
            @if($question['data_entry_type'] == 'text')
              {!! Form::text($question['id'], $tenantResponses[$question['id']], ['class' => 'form-control', 'required']) !!}
            @elseif($question['data_entry_type'] == 'numeric')
              {!! Form::input('number', $question['id'], $tenantResponses[$question['id']], ['class' => 'form-control', 'step'=>'any'], 'required') !!}
            @endif

          @else
            @if($question['data_entry_type'] == 'text')
              {!! Form::text($question['id'], $tenantResponses[$question['id']], ['class' => 'form-control']) !!}
            @elseif($question['data_entry_type'] == 'numeric')
              {!! Form::input('number', $question['id'], $tenantResponses[$question['id']], ['class' => 'form-control', 'step'=>'any']) !!}
            @endif
          @endif
        </div>
        <hr />
      </div>
    @endforeach
  @endforeach
  {!! Form::submit('Save my responses', ['class' => 'btn btn-primary save-btn submit-btn']) !!}
  {!! Form::close() !!}
</section>
@endsection
