@extends('layouts.tenant-inner')
@section('tenant-inner-content')
  @if (Session::has('message-success'))
 	@include('elements.message-success')
  @endif
  @if (Session::has('message'))
 	@include('elements.message')
  @endif
  @if (Session::has('message-error'))
    @include('elements.message-error')
  @endif
@if (Session::has('warningMessage'))
   <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
@endif
<section class="content">
  <div class="row">
    <div class="form-group">
      <div class="col-lg-5">
        <p class="h3">Pending Application Group Requests</p>
      </div>
      <div class="btn-toolbar col-lg-7">
        <div class="btn-group pull-right">
          <div class="row">
            <div class="col-lg-6">
              <a href="" class="inquire btn btn-info btn-md" data-toggle="modal" data-target="#myModal">New Application Group <i class="fa fa-plus" aria-hidden="true"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if(!$pendingApplications->isEmpty())
    <hr />
      <table class="table">
        <thead>
          <th>Group Name</th>
          <th>Invited By</th>
          <th>Invite Date</th>
          <th>Accept </th>
          <th>Reject</th>
        </thead>
        @foreach($pendingApplications as $group)
          <tr>
            <td>{!! $group->group_name !!}</td>
            <td>{!! $group->creator->firstname !!} {!! $group->creator->lastname !!}</td>
            <td>{!! $group->created_at->format('M d Y') !!}</td>
            <td><a href="{{route('tenants.acceptApplicationGroupInvitation', [$tenantId, $group->id])}}" class="btn btn-success">Accept</a> </td>
            <td><a href="{{route('tenants.rejectApplicationGroupInvitation', [$tenantId, $group->id])}}" class="btn btn-danger">Reject</a> </td>
          </tr>
        @endforeach
      </table>
  @else
    <p>
      You have no pending application group invitations.
    </p>
  @endif
  <hr />
  <div class="row">
    <div class="form-group">
      <div class="col-lg-4">
        <p class="h3">Application Groups</p>
      </div>
    </div>
  </div>
  @if(!$applicationGroupsMember->isEmpty())
      <table class="table">
        <thead>
          <th>Group Name</th>
          <th>Members </th>
          <th>Owned By Me</th>
          <th>Delete</th>
        </thead>
        @foreach($applicationGroupsMember as $group)
          <tr>
            <td>{!! $group->group_name !!}</td>
            <td>
              @foreach($group->members as $member)
                @if(!($member->id == $tenantId))
                  @if($member != $group->members->last())
                    {!! $member->firstname !!} {!! $member->lastname !!},
                  @else
                      {!! $member->firstname !!} {!! $member->lastname !!}
                  @endif
                @endif
              @endforeach
            </td>
            @if($group->creator_id == $tenantId)
              <td><i class="fa fa-check" aria-hidden="true"></i></td>
              <td>
                {!! Form::open(['route' => 'tenants.deleteApplicationGroup']) !!}
                {!! Form::hidden('applicationGroupId', $group->id) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
              </td>
            @else
              <td><i class="fa fa-times" aria-hidden="true"></i></td>
              <td>
                  <button class="btn btn-danger" data-toggle="tooltip" title="You must be the owner of this group to delete it!" style="pointer-events: none;" disabled>Delete</button>
              </td>
            @endif
          </tr>
        @endforeach
      </table>
  @else
    <p>
      You are not a member of any application groups.
    </p>
  @endif
  <hr />


</section>
{!! Form::open(['class'=>'edit.tnt-form', 'route'=>['tenants.newApplicationGroup']])  !!}
<div class="modal fade bs-example-modal-md" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="myLargeModalLabel">New Application Group</h3>
        </div>

        <div class="modal-body">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="row form-group">
                 <label>Group Name <input type="text" name="group_name" /></label>
               </div>
               <hr />
               <div id="tenant-email-inputs">
                 <div class="row">
                   <label>Tenant .edu e-mail address <input type="text" name="tenant_email[]"/></label>
                   <button id="b1" class="btn add-more" type="button" onClick="addInput('tenant-email-inputs');">+</button>
                 </div>
               </div>
          </div>
        </div>
        <div class="modal-footer">
          {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        </div>
      </div>
  </div>
  {!! Form::close() !!}
</div>

<script>

function addInput(divName){
  $('#tenant-email-inputs').append('<div class="row"><label>Tenant .edu e-mail address <input type="text" name="tenant_email[]"/></label> </div>');
}
$(function() {
    $('.tooltip-wrapper').tooltip({position: "bottom"});
});
</script>
<style>
    table {
        table-layout: fixed;
    }
    td {
        width: 100%;
    }
  .apartment-data-input-short {
      width: 70%;
  }
    .apartment-data-input-long {
        width: 80%;
    }
.modal-body{
background-color: #fff;
!important;
}
</style>
@endsection
