<aside class="main-sidebar">
        <!-- sidebar: s tyle can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel user-left-profile" onclick="location.href='{{ route('landlords.profile')}}'">
            <div class="image">
              <img src="{{ $profileImage }}" class="img-circle" alt="User Image">

            </div>
            <div class="info">
              <p>{{ $profile->firstname.' '. $profile->lastname }}</p>
              <a href="{{ route('landlords.profile') }}">Profile</a>
            </div>

          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Dashboard</li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'dashboard')}} treeview">
              <a href="{{route('landlords.dashboard')}}">
                <i class="fa fa fa-user"></i>
                <span>Account</span>
                <!-- <span class="label label-primary pull-right">4</span> -->
              </a>
            </li>
            {{-- <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-building"></i> <span>Subscription</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'mysubscription')}} treeview">
                  <a href="{{ route('landlords.subscription') }}">
                    <i class="fa fa-building-o"></i>
                    <span>Upgrade Subscription</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'upgradesubscription')}} treeview">
                  <a href="{{ route('landlords.mysubscription') }}">
                    <i class="fa fa-building-o"></i>
                    <span>Active Subscription</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'mysubscriptionpaymentsettings')}} treeview">
                  <a href="{{ route('landlords.subscriptionpaymentsettings') }}">
                    <i class="fa fa-building-o"></i>
                    <span>Subscription Payment Settings</span>
                  </a>
                </li>
          </ul>
        </li> --}}
            <!--<li>
              <a href="pages/widgets.html">
                <i class="fa fa-search"></i> <span>My Searches</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-history"></i>
                <span>Recent Properties</span>
              </a>
            </li>-->


          @if ($profile->plan_type != 1)
            <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-briefcase"></i> <span>Portfolio</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'createapplication')}} treeview">
                  <a href="{{ route('landlords.createlandlordapplication') }}">
                    <i class="fa fa-money"></i>
                    <span white-space=wrap>Bank Account Application</span>
                  </a>
                </li>
                <li class="{{ HTML::getTenantMenuClass($pageMenuName, 'propertymanagementgroups') }} treeview">
                  <a href="{{ route('landlords.getmypropertymanagementgroups') }}">
                    <i class="fa fa-list"></i>
                    <span>Bank Accounts</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'myproperty')}} treeview">
                  <a href="{{ route('landlords.getProperty') }}">
                    <i class="fa fa-home"></i>
                    <span>List / View Properties</span>
                  </a>
                </li>
              </ul>
            </li>
	    <li class="{{HTML::getTenantMenuClass($pageMenuName,'manageSubmittedApplications')}} treeview">
                  <a href="{{ route('landlords.manageApplications') }}">
                    <i class="fa fa-clock-o"></i> <span>Received Applications</span>
                  </a>
            </li>
            @elseif ($profile->plan_type == 1)
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'myproperty')}}  treeview">
              <a href="{{ route('landlords.getProperty') }}">
                <i class="fa fa-home"></i> <span>Properties</span>
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'manageSubmittedApplications')}} treeview">
                  <a href="{{ route('landlords.manageApplications') }}">
                    <i class="fa fa-clock-o"></i> <span>Received Applications</span>
                  </a>
            </li>
            @endif

            <li class="{{HTML::getTenantMenuClass($pageMenuName,'mytenantcontact')}} treeview">
              <a href="{{ route('landlords.getContact', ['Default','Default']) }}">
                <i class="fa fa-handshake-o"></i> <span>Conversations</span>
              </a>
            <!--
            <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-handshake-o"></i> <span>Conversations</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'mytenantcontact')}} treeview">
                  <a href="{{ route('landlords.getContact', ['Default', 'Default']) }}">
                    <i class="fa fa-gears"></i> <span>Individual</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'messageunit')}} treeview">
                  <a href="{{ route('landlords.getContactUnit') }}">
                    <i class="fa fa-folder-open"></i> <span>Unit</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'messageportfolio')}} treeview">
                  <a href="{{ route('landlords.getContactPortfolio') }}">
                    <i class="fa fa-book"></i> <span>Portfolio</span>
                  </a>
                </li>
              </ul>
              -->
            </li>

            @if ($profile->plan_type != 1)

            <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-money"></i> <span>Rent Collection</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'payment')}} treeview">
                  <a href="{{ route('payments.getsettings') }}">
                    <i class="fa fa-gears"></i> <span>Payment Settings</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'mypayment')}} treeview">
                  <a href="{{ route('payment.getTenantPayment') }}">
                    <i class="fa fa-folder-open"></i> <span>Rent Roll</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'renthistory')}} treeview">
                  <a href="{{ route('landlords.allRentPaymentHistory') }}">
                    <i class="fa fa-book"></i> <span>Rent Transaction History</span>
                  </a>
                </li>
              </ul>
            </li>
            @endif
             @if ($profile->plan_type != 1)

               <li class="{{HTML::getTenantMenuClass($pageMenuName,'mytenants')}}  treeview">
              <a href="{{ route('landlords.tenants') }}">
                <i class="fa fa-users"></i> <span>Tenants</span>
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'maintenancerequests')}} treeview">
              <a href="{{ route('landlords.myMaintenanceRequests', ['Default','Default']) }}">
                <i class="fa fa-wrench"></i> <span>Maintenance Requests</span>
              </a>
            </li>
            @endif
            <!--
	    <li class="{{HTML::getTenantMenuClass($pageMenuName,'featured properties')}} treeview">
              <a href="{{ route('landlord.getFeaturedProperties') }}">
                <i class="fa fa-gears"></i> <span>Featured Properties</span>
              </a>
            </li>
		COMMENTED OUT WHILE NOT WORKING-->
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'changepassword')}} treeview">
              <a href="{{ route('landlords.changepassword') }}">
                <i class="fa fa-lock"></i> <span>Change Password</span>
              </a>
            </li>
            <li class="treeview">
              <a href="{{ route('landlords.logout') }}">
                <i class="fa fa-sign-out"></i> <span>Logout</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
