@if(count($errors) > 0)
<div class="alert alert-danger" style="margin: 0 auto; width: 25%">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
                @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                @endforeach
        </ul>
</div>
@endif

