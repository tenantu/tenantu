<div class="container-fluid subpage-top-cntr">
	<div class="container">
	<!-- nav section starts -->
	<!-- //////// nav section ends //////// -->
	<!--- right sidebar comes here -->

    <!-- nav section ends -->
    <!-- //////// ends //////// --> 

    <!-- man section -->
    <!-- //////// starts //////// -->
    <div id="wrapper">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 scl-icons">
					<div class="social-icons">
						<ul>
							<li><a href="{{HTML::getSocialMediaLinks('twitter')}}"><img src="{{ asset('public/img/twitter.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('facebook')}}"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('instagram')}}"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
							<li><a href="{{route('contactUs')}}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">					<span class="tenantu-logo" style="font-size: 40px; "><a href="{{ route('index.index')}}" class="no-hover">TenantU</a></span>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
					<div class="inner-subpage">
						<div class="login-buttons pull-right">
                                                         @if (Auth::check('tenant') || Auth::check('landlord'))
                                                         <a href="{{route('index.login')}}">My Account</a>
                                                        @else
                                                         <a href="{{route('index.login')}}">Login / Signup</a>
                                                         @endif
                                                </div>
					</div>
				</div>
		</div>
		</div>
    </div>
    <!-- man section ends-->
    <!-- //////// ends //////// -->
  </div>
  </div>

<style>
	@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Bold.woff);
		font-weight: 700;
	}@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Thin.woff);
		font-weight: 200;
	}

	@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Regular.woff);
		font-weight: 400;
	}

	@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Medium.woff);
		font-weight: 500;
	}
	.no-hover:hover {
		text-decoration: none;
		color: #ffffff;
		font-family: Metropolis, sans-serif;
		font-weight: 200;
	}
	.no-hover {
		color: #ffffff;
		font-family: Metropolis, sans-serif;
		font-weight: 200;

	}
</style>
