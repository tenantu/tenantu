<div class="alert alert-success text-center">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<strong>Success!</strong> {{ Session::get('message') }}
</div>
