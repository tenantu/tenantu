<div id="push_sidebar">
			<ul class="right-menu">
				<li><a href="{{ route('index.index') }}">Home</a></li>
				<!--<li><a href="{{route('schools.getSchools')}}">Schools</a></li>-->
				@if (HTML::isUserLoggedInBool() == false)
				<li><a href="{{HTML::isUserLoggedIn()}}">Login</a></li>
				@endif
				@if (HTML::isUserLoggedInBool() == true)
				<li><a href="{{HTML::isUserLoggedIn()}}">Logout</a></li>
				@endif
				<li><a href="{{ URL::to('/')}}/blog">Blog</a></li>
				<!--<li><a href="{{route('index.index','#contact')}}">Contact</a></li>-->
				<li><a href="{{route('contactUs')}}">Contact Us</a></li>
			</ul>
			<div class="social-icons">
				<ul>
					<li><a href="{{HTML::getSocialMediaLinks('twitter')}}"><img src="{{ asset('public/img/twitter.png') }}"></a></li>
					<li><a href="{{HTML::getSocialMediaLinks('facebook')}}"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
					<li><a href="{{HTML::getSocialMediaLinks('instagram')}}"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
					<li><a href="{{ route('index.postContactUs') }}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
				</ul>
			</div>
			<div class="address">
				<p>132 E Delaware Ave</p>
				<p>Newark, Delaware 19711 </p>
				<div>Tel : (516) 507-9253 </div>
				
			</div>
		</div>
