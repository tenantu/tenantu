<div class="alert alert-success text-center"  style="margin: 0 auto; width: 25%">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <strong>Success!</strong> {{ Session::get('message') }}
</div>

