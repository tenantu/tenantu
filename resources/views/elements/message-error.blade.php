<div class="alert alert-danger text-center" style="margin: 0 auto; width: 25%">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<strong>Error!</strong> {{ Session::get('message-error') }}
</div>
