<div class="col-md-4">
    <br/>
    <div class="input-group">         
            {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search']) !!}
            <span class="input-group-btn">
            	{!! Form::submit('Go!', ['class' => 'btn btn-info btn-flat']) !!}
            </span>        
    </div>
</div>