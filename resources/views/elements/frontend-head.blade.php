<meta charset="utf-8">
<meta name="_token" content="{!! csrf_token() !!}"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="img/favicon.ico">
<title>{{ Config::get('constants.SITE_NAME') }} - {{ $pageTitle or '' }}</title>
	<!-- Latest compiled and minified CSS -->
<link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet">
<!-- template css -->
<link href="{{ asset('public/css/style.css') }}" rel="stylesheet">
<!-- Font Awesome (font icons) - http://goo.gl/XfBd3 -->
<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
<link href="{{ asset('public/css/bootstrap-select.css') }}" rel="stylesheet">
<link href="{{ asset('public/css/jquery.nouislider.css') }}" rel="stylesheet" />
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="{{ asset('public/sweetalert/js/sweetalert-dev.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<link href="{{ asset('public/sweetalert/css/sweetalert.css') }}" rel="stylesheet" />
<link href="{{ asset('public/css/jquery.bxslider.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" type="text/css">
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">

