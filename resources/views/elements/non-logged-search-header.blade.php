{!! Form::model('',['method' => 'POST','route'=>['index.searchResult'],'class'=>'form-inline','id'=>'searchForm']) !!}

<div class="subpage-top-cntr">
	<div class="container">
	<!-- nav section starts -->
	<!-- //////// nav section ends //////// -->
	<!--- right sidebar comes here -->

    <!-- nav section ends -->
    <!-- //////// ends //////// --> 

    <!-- man section -->
    <!-- //////// starts //////// -->
    <div id="wrapper">
			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
					<span class="tenantu-logo" style="font-size: 40px; "><a href="{{ route('index.index')}}" class="no-hover">TenantU</a></span>
				</div>
				<div class="col-lg-2 col-md-3 col-sm-2 col-xs-12 col-lg-offset-3 col-md-offset-1 scl-icons">
					<div class="social-icons">
						<ul>
							<li><a href="{{HTML::getSocialMediaLinks('twitter')}}" target="_blank"><img src="{{ asset('public/img/twitter.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('facebook')}}" target="_blank"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('instagram')}}" target="_blank"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
							<li><a href="{{ route('index.postContactUs') }}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
					<div class=" top-right-menu">
						<div class="login-buttons">
							 @if (Auth::check('tenant') || Auth::check('landlord'))
							 <div class="pull-right">
							 <a href="{{route('index.login')}}">My Account</a>
							 </div> 
							@else
							 <a href="{{route('index.login')}}">Login / Signup</a>
							 @endif
						</div>
					</div>
				</div>
		</div>
    </div>
    <!-- man section ends-->
    <!-- //////// ends //////// -->

    <!-- filter section -->
    <!-- //////// starts //////// -->
    <div class="filter-search">
	    <div class="row">
	  		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
	  			<div class="form-group">
	  				<div class="select hide landlord-dropdown form-control">
				    	{!! Form::select('landlord_id',['' => 'Select landlord']+$selectedLandlordDropDown, $selectedLandlordId, ['class'=>'selectpicker  show-tick form-control','id'=>'landlordname']) !!}
					</div>
			  	</div>
			  	<div class="form-group bedroom hide">
				    <label class="hide"for="email">Bedrooms</label>
					<div class="select">
					 {!! Form::select('bedroomId',['' => array('1'=>"1",'2'=>"2",'3'=>"3",'4'=>"4",'5'=>"5",'6'=>"6",'7'=>"7",'8'=>"8",'any'=>"Any")], $selectedBedroomNo, ['class'=>'selectpicker hide show-tick form-control','id'=>'bedRoom']) !!}	
					</div>
			  </div>
			  <div class="location hide">
			  	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			  		Location<span class="fa fa-angle-down"></span><span id="distanceShow"> - 1000</span>
	    		</a>
			    <ul class="dropdown-menu">
			    	<li>
			    		<div class="row range-sliders">
			    			<div class="col-sm-5 col-lg-5 col-md-5 col-xs-5">Distance from 
Campus</div>
			              <div class="col-sm-7 col-md-7 col-lg-7 col-xs-7">
			                {!! Form::text('distance',$selectedDistance, ['max'=>"40", 'class'=>'form-control range-texts', 'id'=>'input-to']) !!}	
			                 Miles
			              </div>
			              
			            </div>
		            	<!--<input class="slider" data-slider-max="40" data-slider-min="0" type="text">-->
		            	<div id="slider-handles3"></div>
			    	</li>
			    </ul>
			  </div>
			  <div class="price hide">
			  	<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
			  		Price<span class="fa fa-angle-down"></span><span id="priceShow"> $10 - $20</span>
	    		</a>
			    <ul class="dropdown-menu">
			    	<li>
			    	<div class="row range-sliders">
			    		<div class="col-sm-3 col-lg-3 col-md-3 col-xs-3">Price</div>
			              <div class="col-sm-9 col-lg-9 col-md-9 col-xs-12">
			               {!! Form::text('priceFrom',$selectedPriceFrom, ['min'=>"40", 'class'=>'form-control range-texts', 'id'=>'priceFrom']) !!}
			               {!! Form::text('priceTo',$selectedPriceTo, ['max'=>"100", 'class'=>'form-control range-texts', 'id'=>'priceTo']) !!}	
			              </div>
			              
			            </div>
		            	<!--<input class="slider" data-slider-max="40" data-slider-min="0" type="text">-->
		            	<div id="slider-handles4"></div>
		            	</li>
			    	</li>
			    </ul>
			  </div>
	  		</div>
	  		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
	  			<div class="form-group subpage-search">
							   
				    {!! Form::text('search',$searchKeyWord, ['placeholder'=>'Search','class'=>'form-control', 'id'=>'search']) !!}
				    {!! Form::hidden('sortKey','', ['id'=>'sortKeyUpdate']) !!}
				    {!! Form::hidden('availabilityKey','', ['id'=>'availabilityKeyUpdate']) !!}
				    {!! Form::hidden('propertyType', '', ['id'=>'propertyTypeUpdate'])  !!}
			  	</div>
			  	 
	  		</div>
	  		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
	  			<div class="form-group subpage-search">
				    <button class="btn btn-default search-btn">Search</button>
			  	</div>
	  		</div>
	  	</div>
	  </div>
  </div>
  </div>
  {!! Form::close() !!}
  <script type="text/javascript">
	  $(function(){
		  $(document).on('change','#school',function(event){
			  var school = $("#school").val();
			  $.ajax({
				  type: "GET",
                  url : "{{route('index.indexAjax')}}",
                  dataType :"json",
                  data : "school_id="+school+"&page=search-result",
                  success : function(data){
					  
						var objLandlords 				= data.landlords;
						
						$('#landlordname').html('<option value="">Select landlord </option>');
						$("#landlordname").selectpicker();
						$.each(objLandlords, function (i, item) {
							//console.log(i);
							$('#landlordname').append($('<option>', { 
								value: i,
								text : item 
							}));
						$('#landlordname').selectpicker('refresh');
							
						});
						
						
						
				  }
			  });
		  });
	  });
  </script>

<style>
	@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Bold.woff);
		font-weight: 700;
	}@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Thin.woff);
		font-weight: 200;
	}

	@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Regular.woff);
		font-weight: 400;
	}

	@font-face {
		font-family: Metropolis;
		src: url(../../../public/fonts/Metropolis-Medium.woff);
		font-weight: 500;
	}
	.no-hover:hover {
		text-decoration: none;
		color: #ffffff;
		font-family: Metropolis, sans-serif;
		font-weight: 200;
	}
	.no-hover {
		color: #ffffff;
		font-family: Metropolis, sans-serif;
		font-weight: 200;

	}
</style>
