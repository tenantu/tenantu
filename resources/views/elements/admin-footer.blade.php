<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.2.0
    </div>
    <strong>Copyright &copy; <?php echo date("Y",strtotime("-1 year")).' - '.date("Y"); ?> <a href="http://tenantu.com/">tenantu.com</a>.</strong> All rights reserved.
</footer>
