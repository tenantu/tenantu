<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel user-left-profile" onclick="location.href='{{route('tenants.profile')}}';">

            <div class="image">

              <img src="{{HTML::getTenantProfileImage()}}" class="img-circle" alt="User Image">
            </div>
            <div class="info">
              <p>{{ Auth::user('tenant')->getFullName()}} </p>
              <a href="{{route('tenants.profile')}}"> Profile</a>
            </div>

            </a>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Dashboard</li>
	     @if (Auth::user('tenant')->verified == 1)
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'dashboard')}} treeview">
              <a href="{{ route('tenants.dashboard') }}">
                <i class="fa fa fa-user"></i>
                <span>Account</span>

              </a>
            </li>
            <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-folder"></i> <span>Housing Applications</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <!-- look at second parameter in call to getTenantMenuClass -->
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'myApplication')}} treeview">
                  <a href="{{ route('tenants.myApplication') }}">
                    <i class="fa fa-wpforms"></i> <span>My Application </span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'manageSubmittedApplications')}} treeview">
                  <a href="{{ route('tenants.manageApplications') }}">
                    <i class="fa fa-clock-o"></i> <span>Submitted Applications</span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'applicationGroups')}} treeview">
                  <a href="{{ route('tenants.applicationGroups') }}">
                    <i class="fa fa-wpforms"></i> <span>My Application Groups </span>
                  </a>
                </li>
              </ul>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'mysearch')}}">
              <a href="{{route('search.getMySearchDetails')}}">
                <i class="fa fa-search"></i> <span>Search</span>
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'recentproperties')}} treeview">
              <a href="{{ route('tenants.getRecentProperties') }}">
                <i class="fa fa-history"></i>
                <span>Recently Viewed</span>
              </a>
            </li>
           <li class="{{HTML::getTenantMenuClass($pageMenuName,'mylandlord')}} treeview">
              <a href="{{ route('tenants.getMyLandlords') }}">
                <i class="fa fa-building"></i> <span>Landlord</span>
              </a>
            </li>

            <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-paypal"></i> <span>Payments</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li class="{{HTML::getTenantMenuClass($pageMenuName,'mypayment')}} treeview">
                    <a href="{{ route('payment.getMyPayment') }}">
                      <i class="fa fa-paypal"></i> <span>Active Payments</span>
                    </a>
                  </li>
                  <li class="{{HTML::getTenantMenuClass($pageMenuName,'mypayment')}} treeview">
                    <a href="{{ route('tenants.myPaymentHistory') }}">
                      <i class="fa fa-paypal"></i> <span>Rent Pay History</span>
                    </a>
                  </li>
                  <li class="{{HTML::getTenantMenuClass($pageMenuName,'tenantPaymentSettings')}} treeview">
                    <a href="{{ route('payments.getTenantPaymentSettings') }}">
                      <i class="fa fa-paypal"></i> <span>Payment Settings</span>
                    </a>
                  </li>
                </ul>
              </li>

            <li class="{{HTML::getTenantMenuClass($pageMenuName,'mycontact')}} treeview">
              <a href="{{ route('tenants.getContact') }}">
                <i class="fa fa-handshake-o"></i> <span>Conversations </span>
              </a>
            </li>
            <li {{ setActiveList('admin/users') }}>
              <a href="#"><i class="fa fa-wrench"></i> <span>Maintenance Request</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <!-- look at second parameter in call to getTenantMenuClass -->
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'createMaintenanceRequest')}} treeview">
                  <a href="{{ route('tenants.createMaintenanceRequest') }}">
                    <i class="fa fa-wrench"></i> <span>Create Maintenance Request </span>
                  </a>
                </li>
                <li class="{{HTML::getTenantMenuClass($pageMenuName,'myMaintenanceRequests')}} treeview">
                  <a href="{{ route('tenants.myMaintenanceRequests',['Default', 'Default']) }}">
                    <i class="fa fa-wrench"></i> <span>Maintenance Requests</span>
                  </a>
                </li>
              </ul>
            </li>
	    @endif
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'changepassword')}} treeview">
              <a href="{{ route('tenants.getChangePassword') }}">
                <i class="fa fa-lock"></i> <span>Change Password</span>
              </a>
            </li>

            <li class="treeview">
              <a href="{{ route('tenants.logout') }}">
                <i class="fa fa-sign-out"></i> <span>Sign out</span>
              </a>
            </li>



          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
