@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="row">
        
        <div class="col-md-2 pull-right">
            <br/>
            <a class="btn btn-block btn-primary" href="{{ route('admin.AddAdminUsers') }}">
                <i class="fa fa-plus-circle"></i> &nbsp;New Admin
            </a>
        </div>
    </div>
    
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="col-md-2">#</th>
                <th class="col-md-2">
                     <a href="">Name</a>
                    
                </th>
           
                <th class="col-md-2">
                        <a href="">Email</a>
                </th>
                <th class="col-md-3 text-center">Action</th>
                
            </tr>
        </thead>
        <tbody>
        <?php
//$page = $amenities->currentPage();
$slNo = 1;
?>
        @foreach ($admins as $admin)
            <tr>
               
                <th scope="row">{{$slNo}}</th>
                <td>{{$admin->name}}</td>
                <td>{{$admin->email}}</td>
               
                <td class="text-center">
                     {!! Form::open(['method' => 'DELETE', 'route' => ['admins.destroyAdmin', $admin->id]]) !!}
                        <a href="{{ route('admin.getEditAdmin',$admin->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a> &nbsp;
                         @if($admin->id != '1')
                        <button type="submit" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this admin?');"><i class="fa fa-trash"></i> Delete</a></button>
                        @endif
                    {!! Form::close() !!}
                </td>
                
                
            </tr>
            <?php
$slNo++;
?>
        @endforeach
        </tbody>
    </table>
    <div class="row">

    </div>
</div>
@endsection
