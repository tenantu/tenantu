@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
    @include('elements.validation-message')
	
    {!! Form::model('',['method' => 'POST','route'=>['admin.reassignSave'],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('Select a School', 'Select a school:') !!}<span style="color: red;">*</span>
            {!! Form::select('school',['' => 'Select a school']+$schools, null, ['class'=>'form-control','id'=>'selectSchool']) !!}
        </div>
        <div id="error"></div>
        <div class="loader1" id="status_loader" style="display: none;"><img src="{{ asset('public/img/bx_loader.gif') }}"></div>
        <div class="form-group">
            {!! Form::label('Select Property', 'Select a property:') !!}<span style="color: red;">*</span>
            {!! Form::select('property',['' => 'Select a property'], null, ['class'=>'form-control','id'=>'selectProperty']) !!}
        </div>
         <div id="errorProperty"></div>
        <div class="loader2" id="status_loader" style="display: none;"><img src="{{ asset('public/img/bx_loader.gif') }}"></div>
        <div class="form-group">
            {!! Form::label('Current Landlord', 'Current Landlord:') !!}<span style="color: red;">*</span>
            <span id="currentLandlord"> </span>
            
        </div>
        <div class="form-group">
            {!! Form::label('Select Landlord', 'Select a new landlord:') !!}<span style="color: red;">*</span>
            {!! Form::select('landlord',['' => 'Select a landlord'], null, ['class'=>'form-control','id'=>'selectLandlord']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::submit('Ok', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<script type="text/javascript">
	$(document).ready(function ($) {
		$(document).on('change', '#selectSchool', function () {
			$('#currentLandlord').text('');
			var schholId = $(this).val();
			if(schholId != '')
			{
				$('#error').html('');
				var li="";
				var ui="";
				$.ajax({
					type: "POST",
					url: "{{route('admin.getPropertyLandlord')}}",
					data: "schoolId=" + schholId +"&page=schoolchange",
					beforeSend: function() {
						$('.loader1').show();
					},
					success: function (data) {
						$('.loader1').hide();
						console.log(data);
						
						$.each(data.property,function(i,v){
							li+='<option value="'+i+'">'+v+'</option>';
						});
						$('#selectProperty').empty();
						$('#selectProperty').append('<option value="">Select a property</option>');
						$('#selectProperty').append(li);
						
						$.each(data.landlords,function(i,v){
							console.log(v.fullname);
							ui+='<option value="'+v.id+'">'+v.fullname+'</option>';
						});
						$('#selectLandlord').empty();
						$('#selectLandlord').append('<option value="">Select a landlord</option>');
						$('#selectLandlord').append(ui);
					}
					 
				});
			}
			else {
				$('#error').html('Please select a school');
			}
		});
		
		$(document).on('change', '#selectProperty', function () {
			var propertyId = $(this).val();
			if(propertyId != '')
			{
				$('#errorProperty').html('');
				$.ajax({
					type: "POST",
					url: "{{route('admin.getPropertyLandlord')}}",
					data: "propertyId=" + propertyId +"&page=propertychange",
					beforeSend: function() {
						$('.loader2').show();
					},
					success: function (data) {
						$('.loader2').hide();
						
						$('#currentLandlord').text(data.firstname+" "+data.lastname);
					}
					
				});
			}
			else {
				$('#errorProperty').html('Please select a property');
			}
			
		});
		
	});
</script>

@endsection
