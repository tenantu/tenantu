@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="row">
		
    </div>
    <div class="row article-list">
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
		
            <tr>
                <td class="col-md-2">
                   Tenants ({{ $tenantCountDetails}}) &nbsp; Landlords ({{$landlordCountDetails}}) &nbsp; Properties ({{$propertyCountDetails}}) &nbsp; Schools ({{$schoolCountDetails}}) &nbsp; Total searches({{$searchCountDetails}}) &nbsp; Contact us inquiry ({{$contactCountDetails}})
        
                </td>
              </tr>
    </table>
    <table class="table table-bordered table-striped table-hover article-list">
		<thead>
            <tr>
                <th class="col-md-2">
                   <a> Property views</a>
                </th>
                
                <th class="col-md-2">
                   <a>Property inquires</a>
                </th>
                
                <th class="col-md-2">
                   <a>Searched keyword</a>
                </th>
                
                <th class="col-md-2">
                   <a>Searched price</a>
                </th>
                
                <th class="col-md-2">
                   <a>Searched schools</a>
                </th>
                
                <th class="col-md-2">
                   <a>Monthly search</a>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
					<table  width="100%" height="100%">
					  <tbody>
						  @foreach($mostViewedPropertyDetails as $mostViewedPropertyDetail)
							<tr>
								<td>
									{{ $mostViewedPropertyDetail->property_name }} ({{ $mostViewedPropertyDetail->searchCount }})
								</td>
								
							</tr>
						@endforeach
					  </tbody>
					</table>
                </td>
                {{--<td>--}}
					{{--<table  width="100%" height="100%">--}}
					  {{--<tbody>--}}
						  {{--@foreach($mostInquiredDetails as $mostInquiredDetail)--}}
							{{--<tr>--}}
								{{--<td>--}}
									{{--{{ $mostInquiredDetail->property_name }}  ({{ $mostInquiredDetail->enquirycount }})--}}
								{{--</td>--}}
								{{----}}
							{{--</tr>--}}
						{{--@endforeach--}}
					  {{--</tbody>--}}
					{{--</table>--}}
                {{--</td>--}}
                <td>
					<table  width="100%" height="100%">
					  <tbody>
						  @foreach($mostSearchedDetails as $mostSearchedDetail)
							<tr>
								<td>
									{{ $mostSearchedDetail->search }}  ({{ $mostSearchedDetail->searchCount }})
								</td>
								
							</tr>
						@endforeach
					  </tbody>
					</table>
                </td>
                <td>
					<table  width="100%" height="100%">
					  <tbody>
						  @foreach($mostSearchedPriceDetails as $mostSearchedPriceDetail)
							<tr>
								<?php 
								$pricerange = explode(' ',$mostSearchedPriceDetail->pricerange); 
								$originalPriceRange = '$'.$pricerange[0].' -  $'.$pricerange[1];
								?>
								<td>
									{{ $originalPriceRange }}  ({{ $mostSearchedPriceDetail->searchCount }})
								</td>
								
							</tr>
						@endforeach
					  </tbody>
					</table>
                </td>
                <td>
					<table  width="100%" height="100%">
					  <tbody>
						  @foreach($mostSchoolsSearchDetails as $mostSchoolsSearchDetail)
							<tr>
								<td>
									{{ $mostSchoolsSearchDetail->schoolname }}  ({{ $mostSchoolsSearchDetail->searchCount }})
								</td>
								
							</tr>
						@endforeach
					  </tbody>
					</table>
                </td>
                <td>
					<table  width="100%" height="100%">
					  <tbody>
						  
						  @foreach($mostSearchedMonthYears as $mostSearchedMonthYear)
							<tr>
								<?php
									$month= ''; 
									if($mostSearchedMonthYear->month == '1')
									{
										$month = 'Jan';
									}
									elseif($mostSearchedMonthYear->month == '2')
									{
										$month = 'Feb';
									}
									elseif($mostSearchedMonthYear->month == '3')
									{
										$month = 'Mar';
									}
									elseif($mostSearchedMonthYear->month == '4')
									{
										$month = 'Apr';
									}
									elseif($mostSearchedMonthYear->month == '5')
									{
										$month = 'May';
									}
									elseif($mostSearchedMonthYear->month == '6')
									{
										$month = 'Jun';
									}
									elseif($mostSearchedMonthYear->month == '7')
									{
										$month = 'Jul';
									}
									elseif($mostSearchedMonthYear->month == '8')
									{
										$month = 'Aug';
									}
									elseif($mostSearchedMonthYear->month == '9')
									{
										$month = 'Sep';
									}
									elseif($mostSearchedMonthYear->month == '10')
									{
										$month = 'Oct';
									}
									elseif($mostSearchedMonthYear->month == '11')
									{
										$month = 'Nov';
									}
									elseif($mostSearchedMonthYear->month == '12')
									{
										$month = 'Dec';
									}
								?>
								<td>
									{{ $mostSearchedMonthYear->year }}  {{ $month }} ({{ $mostSearchedMonthYear->searchCount }})
								</td>
								
							</tr>
						@endforeach
					  </tbody>
					</table>
                </td>
                
            </tr>
        </tbody>
    </table>
    </div>
</div>
@endsection
