@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model('',['method' => 'POST','route'=>['admins.storeAdmin']]) !!}

        <div class="form-group">
            {!! Form::label('Admin Name', 'Admin Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('name', Input::old('admin_name'), ['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Admin Email', 'Admin Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email', Input::old('admin_email'), ['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Password', 'Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Confirm Password', 'Confirm Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
