@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($banner,['method' => 'POST','route'=>['admins.updateBannerSettings',$banner->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('BannerName', 'Banner Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('banner_name',null,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('BannerSize', 'Banner Size:') !!}<span style="color: red;"></span>
            {{ $banner->banner_size }}
        </div>
       
        
        <div class="form-group">
            {!! Form::label('BannerPrice', 'Banner Price:') !!}<span style="color: red;">*</span>
            {!! Form::text('banner_price',null,['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Sample Image', 'Sample Image:') !!}<span style="color: red;">*</span>
            <img src="{{ HTML::getSampleBannerImage($banner->sample_image) }}" >
        </div>
        
        <div class="form-group">
            {!! Form::label('Banner Description', 'Banner Description:') !!}<span style="color: red;"></span>
            {!! Form::textarea('banner_description',$banner->description,['class'=>'form-control']) !!}
        </div>
        
      
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

