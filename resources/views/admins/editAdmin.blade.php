@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($admin,['method' => 'POST','route'=>['admins.updateAdmin',$admin->id]]) !!}
         
        <div class="form-group">
            {!! Form::label('Admin', 'Admin:') !!}<span style="color: red;">*</span>
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Email', 'email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('Password', 'Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Confirm Password', 'Confirm Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        </div>
       
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
