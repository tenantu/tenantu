@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
        {!! Form::model($search, ['method' => 'GET','route'=>['admins.bannerPurchases']]) !!}
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                    {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search with name']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>

            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
           
        </div>
        <div class="col-md-8 text-right">
        {!! $bannerPurchases->appends(['search' => $search, 'sortby' => $sortby, 'order' => ($order == 'desc')?'asc':'desc'])->render() !!}
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="">#</th>
                <th class="col-md-2">
                   <a>Name</a>
                </th>
                <th class="col-md-2">
                   <a>Banner Name</a>
                </th>
                
                <th class="col-md-2">
                    <a>Date</a>
                 </th>
                 
                 <th class="col-md-2">
                    <a>Purchase Amount</a>
                 </th>
                 
                 <th class="col-md-2">
                    <a>Banner Status</a>
                 </th>
                 
                 <th class="col-md-2">
                    <a>Payment Status</a>
                 </th>
                 
                 <th class="col-md-4">
                    <a>Banner Image</a>
                 </th>
                    
				<th class="col-md-4">
					<a >Actions</a>
				</th>
                

            </tr>
        </thead>
        <tbody>
        <?php
			$page = $bannerPurchases->currentPage();
			$slNo = (($page - 1) * 10) + 1;
		?>
		
        @foreach ($bannerPurchases as $bannerPurchase)
       
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$bannerPurchase->name}}</td>
                <td>{{$bannerPurchase->banner_name}}</td>
                <td>{{date('d M Y',strtotime($bannerPurchase->created_at))}}</td>
                <td>${{$bannerPurchase->amount_paid}}</td>
                <td>@if($bannerPurchase->status == 1)
					  Active 
					 @else
					  Inactive 
					 @endif
				</td>
				 <td>@if($bannerPurchase->payment_status == '')
					  Not paid 
					 @else
					  {{ $bannerPurchase->payment_status }}
					 @endif
				</td>
				<td> <a href="{{HTML::getBannerImage($bannerPurchase->image)}}" target="_blank">View image</a></td>
				
                <td>
				{!! Form::open(['method' => 'DELETE','route' => ['admins.bannerPurchaseDestroy', $bannerPurchase->id]]) !!}
                <a href="{{ route('admins.bannerPurchaseDetail',$bannerPurchase->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> View </a>
                <button type="submit" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash"></i> Delete</a></button>
                {!! Form::close() !!}
                </td>
                
               
                
            </tr>
            <?php
			$slNo++;
			?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * 10) + 1}} to {{ $slNo-1 }} of {!! $bannerPurchases->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $bannerPurchases->render()) !!}
        </div>
    </div>
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
