@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($featuredPurchasesdetail,['method' => 'POST','files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('LandlordName', 'Landlord Name:') !!}<span style="color: red;"></span>
            {{ucfirst($featuredPurchasesdetail[0]->firstname).' '. ucfirst($featuredPurchasesdetail[0]->lastname) }}
        </div>
       <div class="form-group">
            {!! Form::label('PropertyName', 'Property Name:') !!}<span style="color: red;"></span>
            {{ $featuredPurchasesdetail[0]->property_name }}
        </div>
       
        
        <div class="form-group">
            {!! Form::label('Duration', 'Duration:') !!}<span style="color: red;"></span>
            {{$featuredPurchasesdetail[0]->duration_name}}
        </div>
        
         <div class="form-group">
            {!! Form::label('Amount Paid', 'Amount Paid:') !!}<span style="color: red;"></span>
            {{$featuredPurchasesdetail[0]->amount_paid}}
        </div>
        
        <div class="form-group">
            {!! Form::label('Payment Status', 'Payment Status:') !!}<span style="color: red;"></span>
            {{$featuredPurchasesdetail[0]->payment_status}}
        </div>
         <div class="form-group">
            {!! Form::label('Featured Start', 'Featured Start:') !!}<span style="color: red;"></span>
            {{date('d M Y',strtotime($featuredPurchasesdetail[0]->featured_start))}}
        </div>
        <div class="form-group">
            {!! Form::label('Featured Ends', 'Featured Ends:') !!}<span style="color: red;"></span>
            {{date('d M Y',strtotime($featuredPurchasesdetail[0]->featured_end))}}
        </div>
       
       
        
         <div class="form-group">
            {!! Form::label('Featured status', 'Featured status:') !!}<span style="color: red;"></span>
            @if($featuredPurchasesdetail[0]->featured == 1)
					  Active 
					 @else
					  Inactive 
					 @endif
        </div>
        
        
       

     {!! Form::close() !!}

@endsection

