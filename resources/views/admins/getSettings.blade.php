@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
    @include('elements.validation-message')
	
    {!! Form::model('',['method' => 'POST','route'=>['admin.postSetting'],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('Commision fee($)', 'Commision fee($):') !!}<span style="color: red;">*</span>
            {!! Form::text('commission',$setting[0]->commision, null, ['class'=>'form-control','id'=>'commision']) !!}
        </div>
       
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection
