@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
        <div class="col-md-4">
            <br/>
           
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="">#</th>
                <th class="col-md-4">
                   <a>Subscription Name</a>
                </th>
                <th class="col-md-4">
                   <a>Initial Base Price</a>
                </th>
                <th class="col-md-4">
                   <a>Additional Acct Base Price</a>
                </th>
                <th class="col-md-4">
                   <a>Price Per Property</a>
                </th>
                <th class="col-md-4">
                   <a>Property Max</a>
                </th>
                <th class="col-md-4">
                   <a>Management Acct Max</a>
                </th>
                <th class="col-md-4">
                   <a>Landlord Credit Multiplier</a>
                </th>
                <th class="col-md-4">
                   <a>Tenant Credit Multiplier</a>
                </th>
                <th class="col-md-4">
                    <a>Available</a>
                 </th>
                <th class="col-md-3 text-center" colspan="2">Action</th>

            </tr>
        </thead>
        <tbody>
        <?php
$page = $subscription_plans->currentPage();
$slNo = (($page - 1) * 10) + 1;
?>
        @foreach ($subscription_plans as $subscription_plan)
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$subscription_plan->name}}</td>
                <td>${{$subscription_plan->initial_base_price}}</td>
                <td>${{$subscription_plan->additional_accounts_base_price}}</td>
                <td>${{$subscription_plan->price_per_property}}</td>
                <td>{{$subscription_plan->property_max}}</td>
                <td>{{$subscription_plan->management_account_max}}</td>
                <td>{{$subscription_plan->landlord_credit_multiplier}}x</td>
                <td>{{$subscription_plan->tenant_credit_multiplier}}x</td>
                <td>
                @if ($subscription_plan->available_status == 0)
                    <a href="{{ route('admins.changeAvailability', $subscription_plan->id) }}"><small class="label label-danger"> Unavailable </small></a>
                @else
                    <a href="{{ route('admins.changeAvailability', $subscription_plan->id) }}"><small class="label label-success"> Available </small></a>
                @endif
                </td>
                
                <td class="text-center">
					<a href="#" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a> &nbsp;
                </td>
                <td class="text-center">
                    <a href="#" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this property,This property will remove from landlords and tenants profile?');"><i class="fa fa-trash"></i> Delete</a> &nbsp;
                </td>
            </tr>
            <?php
$slNo++;
?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * 10) + 1}} to {{ $slNo-1 }} of {!! $subscription_plans->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $subscription_plans->render()) !!}
        </div>
    </div>
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
