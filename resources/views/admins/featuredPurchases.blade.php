@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
       
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
           
        </div>
        
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="">#</th>
                <th class="col-md-2">
                   <a>Landlord Name</a>
                </th>
                <th class="col-md-2">
                   <a>Property Name</a>
                </th>
                
                <th class="col-md-2">
                    <a>Duration</a>
                 </th>
                 
                 <th class="col-md-2">
                    <a>Amount Paid</a>
                 </th>
                 
                 <th class="col-md-2">
                    <a>Payment Status</a>
                 </th>
                    
				<th class="col-md-4">
					<a >Actions</a>
				</th>
                

            </tr>
        </thead>
        <tbody>
        <?php
			$page = $featuredPurchases->currentPage();
			$slNo = (($page - 1) * $page) + 1;
			//$slNo = 1;
		?>
		
        @foreach ($featuredPurchases as $featuredPurchase)
       
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{ucfirst($featuredPurchase->firstname).' '. ucfirst($featuredPurchase->lastname) }}</td>
                <td>{{$featuredPurchase->property_name}}</td>
                <td>{{$featuredPurchase->duration_name}}</td>
                <td>${{$featuredPurchase->amount_paid}}</td>
                <td>{{$featuredPurchase->payment_status}}</td>
                <td>
                <a href="{{ route('admins.featuredPurchaseDetail',$featuredPurchase->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> View </a>
                </td>
                
               
                
            </tr>
            <?php
			$slNo++;
			?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
	<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * $page) + 1}} to {{ $slNo-1 }} of {!! $featuredPurchases->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $featuredPurchases->render()) !!}
        </div>
    </div>
    
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
