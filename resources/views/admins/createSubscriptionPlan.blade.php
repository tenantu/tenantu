@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    @if (Session::has('message-error'))
        @include('elements.message-error')
    @endif

    {!! Form::model('',['method' => 'POST','class'=>'edit-property-details row', 'id'=>'tenantAddForm', 'route'=>['admins.storeSubscriptionPlan'],'files'=>true]) !!}

        

        <div class="form-group col-lg-8 col-md-9 col-sm-12 col-xs-7 cols">
            {!! Form::label('SubscriptionName', 'Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('name', Input::old('firstname'), ['class'=>'form-control', 'placeholder'=>'Name of Plan']) !!}
        </div>
        <div class="form-group col-lg-5 col-md-9 col-sm-7 col-xs-7">
            {!! Form::label('PropertyMax', 'Max Properties Supported:') !!}<span style="color: red;">*</span>
            {!! Form::text('property_max', Input::old('property_max'), ['class'=>'form-control','placeholder'=>'# of Properties until New Subscription Plan']) !!}
        </div>
         <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('ManagementAccountMax', 'Max Management Accounts Supported:') !!}<span style="color: red;">*</span>
            {!! Form::text('management_account_max', Input::old('management_account_max'), ['class'=>'form-control','placeholder'=>'# of Management Accounts until New Subscription Plan']) !!}
        </div>
        <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('InitialAccounts', 'Number of Initial Management Accounts:') !!}<span style="color: red;">*</span>
            {!! Form::text('num_management_accounts_pre_new_base_price', Input::old('num_management_accounts_pre_new_base_price'), ['class'=>'form-control','placeholder'=>'# of Management Accounts on the Initial Base Price']) !!}
        </div>
        <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('BasePrice', 'Base Price for Initial Management Accounts:') !!}<span style="color: red;">*</span>
            {!! Form::text('initial_base_price', Input::old('initial_base_price'), ['class'=>'form-control','placeholder'=>'Initial Base Price of Full Priced Management Accounts']) !!}
        </div>
        <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('BasePriceAdditional', 'Base Price for Additional Management Accounts:') !!}<span style="color: red;">*</span>
            {!! Form::text('additional_accounts_base_price', Input::old('additional_accounts_base_price'), ['class'=>'form-control','placeholder'=>'Price of Additional Management Accounts']) !!}
        </div>
        <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('PricePerProperty', 'Price Per Property:') !!}<span style="color: red;">*</span>
            {!! Form::text('price_per_property', Input::old('price_per_property'), ['class'=>'form-control','placeholder'=>'Price for each Managed (Paid) Property Landlord holds']) !!}
        </div>
         <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('LandlordCreditMultiplier', 'Landlord Credit Multiplier:') !!}<span style="color: red;">*</span>
            {!! Form::text('landlord_credit_multiplier', Input::old('landlord_credit_multiplier'), ['class'=>'form-control','placeholder'=>'Landlord Credit Multiplier']) !!}
        </div>
        <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('TenantCreditMultiplier', 'Tenant Credit Multiplier:') !!}<span style="color: red;">*</span>
            {!! Form::text('tenant_credit_multiplier', Input::old('tenant_credit_multiplier'), ['class'=>'form-control','placeholder'=>'Tenant Credit Multiplier']) !!}
        </div>
          <div class="form-group col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
            {!! Form::label('available_status', 'Available:') !!}
            {!! Form::checkbox('available_status', 1, true, Input::old('is_active'), ['class'=>'']) !!}
        </div>

        <div class="form-group col-md-8 col-sm-8 col-xs-8">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<script>
    CKEDITOR.replace('editor');
     $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('input[type="radio"]').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });

    });
    $("#image").fileinput({showUpload: false});

      $("#location").tagit({
            allowSpaces: true,
            fieldName: "locations[]",
            requireAutocomplete: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function(request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function(item) {
                            return item.description;
                        });
                        currentlyValidTags = data;
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({ input: request.term }, callback);
                }
            }
        });
</script>
@endsection
