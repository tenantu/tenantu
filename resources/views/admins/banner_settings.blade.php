@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
     
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
           
        </div>
        
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="">#</th>
                <th class="col-md-4">
                   <a>Banner Name</a>
                </th>
                
                <th class="col-md-4">
                    <a>Banner Size</a>
                 </th>
                    
				<th class="col-md-4">
					<a >Banner Price</a>
				</th>
				<th class="col-md-4">
					<a >Status</a>
				</th>
                <th class="col-md-3 text-center" colspan="2">Action</th>

            </tr>
        </thead>
        <tbody>
        <?php
		//$page = $bannerDetails->currentPage();
		$slNo =  1;
		?>
		
        @foreach ($bannerDetails as $bannerDetail)
       
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$bannerDetail->banner_name}}</td>
                <td>{{$bannerDetail->banner_size}}</td>
                <td> ${{$bannerDetail->banner_price }}</td>
                <td>
                @if ($bannerDetail->status == 0)
                    <a href="{{ route('admins.changeBannerSettingStatus',$bannerDetail->id) }}"><small class="label label-danger"> In Active </small></a>
                @else
                    <a href="{{ route('admins.changeBannerSettingStatus',$bannerDetail->id) }}"><small class="label label-success"> Active </small></a>
                @endif
                </td>
                <td class="text-center">
					<a href="{{ route('admins.bannerEdit',$bannerDetail->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a>
                </td>
                
            </tr>
            <?php
			$slNo++;
			?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
    </div>
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
