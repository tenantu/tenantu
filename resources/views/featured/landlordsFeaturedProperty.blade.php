@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
    @if (Session::has('message-error'))
		@include('elements.message-error')
	@endif
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          @if($statusMessage!='')
          <div class="alert alert-success text-center">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<strong></strong> {{ $statusMessage }}
		  </div>
		  @endif
          
          <div class="row my-properties">
			@forelse( $featuredProperties as $featuredProperty )  
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tnt-box tenants">
                      <div class="property row">
                        <div class="col-l-4 col-md-4 col-sm-5 col-xs-12">
                          <div class="tenant-image"><img src="{{ HTML::getPropertyImage($featuredProperty->id,'',100) }}"></div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                          <h4>{{ $featuredProperty->property_name }}</h4>
                          <div class="tenant-location"><i class="fa fa-map-marker"></i> {{ $featuredProperty->location }}</div>
                          <div class="posted-on">Posted on : <span>{{ date('m-d-Y',strtotime($featuredProperty->created_at)) }}</span></div>
                        </div>
                        <div class="buttons-cntr">Featured status expired on : <span>{{ date('m-d-Y',strtotime($featuredProperty->featured_end)) }}</span></div>
                      </div>
                      
                </div><!-- /.box -->
            </div>
            @empty
				<p></p>
            @endforelse
            
          </div><!-- /.row -->

          <div>
			  <h4>Make properties featured</h4>
			  <div class="make-featured-prts row">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 clmns">
				{!! Form::select('non_featured_property',['' => 'Please select a property']+$nonFeaturedProperties, '',['class'=>'form-control','id'=>'propertytitle']) !!}
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 clmns">
				{!! Form::select('duration',['' => 'Please select a duration']+$featuredSetting,'', ['class'=>'form-control','id'=>'duration']) !!}
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 clmns clmns-text-center">
				 <span id="featuredPrice">$ 0.00</span>
				 <img id="loading-image" src="{{ asset('public/img/bx_loader.gif') }}" style="display:none;"/>
				 <span id="featuredPriceButton" >{!! Form::button('Pay now',['id'=>'purchaseButton', 'class'=>'btn btn-primary pay-btn', 'data-id'=>""]) !!}</span>
         </div>
			  </div>
			 
			  
        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
      @section('page-scripts')
      
      <script type="text/javascript">
			$('#purchaseButton').prop('disabled', true);
			$(document).on('change','#propertytitle',function(event) {
				var propertyId = $(this).val();
				$('#purchaseButton').attr('data-propertyId',propertyId);
				var duration = $('#duration').val();
				if(propertyId!= '' && duration!=""){
					$('#purchaseButton').prop('disabled',false );
				}
				else{
					$('#purchaseButton').prop('disabled', true);
				}
			});
			
			$(document).on('change','#duration',function(event) {
				
				var propertyId = $("#propertytitle").val();
				
				var duration = $(this).val();
				if(propertyId!= '' && duration!=""){
					$('#purchaseButton').prop('disabled', false);
				}
				else{
					$('#purchaseButton').prop('disabled', true);
					
				}
				$.ajax({
						type: "GET",
						url : "{{route('index.indexAjax')}}",
						dataType :"json",
						data : "duration="+duration+"&page=featured",
						beforeSend: function() {
						  $("#loading-image").show();
					    },
									
						success: function(data){
							console.log(data);
							$('#featuredPrice').html('$'+data.price);
							$('#purchaseButton').attr('data-durationId',data.id);
							$("#loading-image").hide();
							//$('#purchaseButton').prop('disabled', false);
                        }
                    });
			});
		$(document).on('click','#purchaseButton',function(){
			var durationId = $('#purchaseButton').attr("data-durationId");
			var PropertyId = $('#purchaseButton').attr("data-propertyId");
			window.location.href = "{{ route('landlord.purchaseFeatured') }}/"+durationId+"/"+PropertyId+"";
		});
      </script>
      @endsection
