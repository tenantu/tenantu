<!-- resources/views/emails/password.blade.php -->

<b> {{ Config::get('constants.SITE_NAME') }}</b>
<hr>
<br>
Hi {{$name}}, 
<br><br>
You are receiving this email to activate your account.
<br>
<br>

Click here /copy paste the url to activate your account: {{ url('index/accountIsActive/'.$token.'/'.$userType) }}
<br><br>
If you haven't made such a request or if you have any questions, just email us at  {{ Config::get('constants.SUPPORT_EMAIL') }}.
<br><br>

<hr>
Please note: This e-mail message is an automated notification. Please do not reply to this message.
