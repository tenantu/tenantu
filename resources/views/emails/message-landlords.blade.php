@extends('layouts.email-master')
@section('email-content')

@if ($messageToLandlord != "")
<tr>    
	   <td style="padding: 0 0 0px;">
                <div align="center" style="padding:0px; margin:0px 0px 0px;"> <br /><strong> {{$landlordName}}, your tenant has messaged you using TenantU.</strong> </div>
        </td>
</tr>
@endif
@if ($messageToLandlord == "")
<tr>
           <td style="padding: 0 0 0px;">
                <div align="center" style="padding:0px; margin:0px 0px 0px;"> <br /><strong> {{$landlordName}}, your tenant has invited you to join TenantU.</strong> </div>
        </td>
</tr>
@endif


<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Name : {{$tenantFullName}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Email : {{$tenantEmail}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> School : {{$tenantSchoolName}} </div>
        </td>
</tr>

	@if ($messageToLandlord != "")
<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Message : {{$messageToLandlord}} </div>
        </td>
</tr>
	@endif
	@if ($messageToLandlord == "")
<tr>
	<td style="padding: 0 0 0px;">
		<div style="padding:0px; margin:0px 0px 0px;"> Message : Your tenant has invited you to join TenantU.
	</td>
</tr>
	@endif
<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"><a href = 'www.tenantu.com/'>Check out TenantU</a> </div>
        </td>
</tr>





@endsection
