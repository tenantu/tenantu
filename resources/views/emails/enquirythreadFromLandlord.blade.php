@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenantName}}, 
	</td>
</tr>

<tr>
	<td  style="padding: 0 0 20px;">
		You have received a message from {{$landLordName}}
	</td>
</tr>

@endsection
