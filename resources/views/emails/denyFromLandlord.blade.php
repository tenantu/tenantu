@extends('layouts.email-master')
@section('email-content')

<tr>
        <td style="padding: 0 0 20px;">
        </td>
</tr>
<tr>
        <td style="padding: 0 0 20px;">
                Hi {{$tenantName}}.
        </td>
</tr>

<tr>
        <td  style="padding: 0 0 20px;">
                Your application to {{$landLordName}}'s property has been denied. <a href="{{ url('/search')}}">Click here</a> to search more listings!
        </td>
</tr>

@endsection


