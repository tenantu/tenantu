@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 0px;">
		<div style="padding:0px; margin:0px 0px 0px;">Hi {{$landlordName}},<br /> A tenant , {{$tenantName}} ,has expressed interest in potentially renting your property, {{$propertyName}} , The details of the tenant can be found below.  </div>
	</td>
</tr>

<tr>
	<td style="padding: 0 0 0px;">
		<div style="padding:0px; margin:0px 0px 0px;">Tenant Name : {{$tenantName}} </div>
	</td>
</tr>

<tr>
	<td style="padding: 0 0 0px;">
		<div style="padding:0px; margin:0px 0px 0px;">Tenant Email : {{$tenantEmail}} </div>
	</td>
</tr>

<tr>
	<td style="padding: 0 0 0px;">
		<div style="padding:0px; margin:0px 0px 0px;">Tenant Phone : {{$tenantPhone}} </div>
	</td>
</tr>


@endsection
