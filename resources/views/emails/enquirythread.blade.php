@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$landLordName}}, You have received a request from your tenant, {{$tenantName}}
	</td>
</tr>
<tr>
	<td  style="padding: 0 0 20px;">
		Note: Please visit the message section of the website for more details
	</td>
</tr>

@endsection
