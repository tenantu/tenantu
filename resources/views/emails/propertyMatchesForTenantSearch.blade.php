@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenantName}}, There is a match with your recent property search.To view the property, please <a href="{{ $propertyUrl }}">click here</a>
		
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		If you haven't made such a request or if you have any questions, just email us at  {{ Config::get('constants.SUPPORT_EMAIL') }}<br/>
		Please note: This e-mail message is an automated notification. Please do not reply to this message.
	</td>
</tr>

@endsection
