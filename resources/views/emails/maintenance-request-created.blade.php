@extends('layouts.email-master')
@section('email-content')

<tr>
           <td style="padding: 0 0 0px;">
                <div align="center" style="padding:0px; margin:0px 0px 0px;"> <br /><strong> Your tenant has submitted a maintenance request using TenantU.</strong> </div>
        </td>
</tr>


<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Name : {{$tenantFullName}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Address : {{$tenantProperty}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Issue Type : {{$maintenanceIssueType}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;">Look at your maintenance requests on TenantU for more details. </div>
        </td>
</tr>





@endsection

