@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
@if($message == 'On Going')
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenantName}}, {{$landLordName}} has responded to your most recent request and is working on solving the issue.	</td>
</tr>
@elseif($message == 'Completed')
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenantName}}, {{$landLordName}} has satisfied your most recent request.
	</td>
</tr>
@endif

@endsection
