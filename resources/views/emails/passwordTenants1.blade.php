<!-- resources/views/emails/password.blade.php -->

Click here or copy and paste the link to reset your password: {{ url('tenants/resetpassword/'.$token) }}
