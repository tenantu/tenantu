@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
		<h3 style="padding:0px; margin:20px 0px 0px;">Name : {{$name}} </h3>
	</td>
</tr>

<tr>
	<td style="padding: 0 0 20px;">
		<h3 style="padding:0px; margin:20px 0px 0px;">Phone : {{$phone}} </h3>
	</td>
</tr>

<tr>
	<td style="padding: 0 0 20px;">
		<h3 style="padding:0px; margin:20px 0px 0px;">Email : {{$emailFrom}} </h3>
	</td>
</tr>

<tr>
	<td style="padding: 0 0 20px;">
		<h3 style="padding:0px; margin:20px 0px 0px;">Property Name : {{$propertyName}} </h3>
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		<h3 style="padding:0px; margin:20px 0px 0px;">Enquiry Date : {{$enquiryDate}} </h3>
	</td>
</tr>

<tr>
	<td  style="padding: 0 0 20px;">
		{{$messageContent}}
	</td>
</tr>


@endsection
