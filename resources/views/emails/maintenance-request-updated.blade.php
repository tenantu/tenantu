@extends('layouts.email-master')
@section('email-content')

<tr>
           <td style="padding: 0 0 0px;">
                <div align="center" style="padding:0px; margin:0px 0px 0px;"> <br /><strong> Your landlord has updated a maintenance request using TenantU.</strong> </div>
        </td>
</tr>
<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Issue Type : {{$maintenanceIssueType}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;">Your landlord has changed the status of this maintenance request to: {{$maintenanceStatus}}. </div>
        </td>
</tr>





@endsection

