@extends('layouts.email-master')
@section('email-content')

<tr>
        <td style="padding: 0 0 0px;">
                <div align="center" style="padding:0px; margin:0px 0px 0px;"> <br /><strong> A landlord from TenantU has invited you to join.</strong> </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Name : {{$name}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Email : {{$email}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> School : {{$school}} </div>
        </td>
</tr>

<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Reason : {{$reason}} </div>
        </td>
</tr>


<tr>
        <td style="padding: 0 0 0px;">
                <div style="padding:0px; margin:0px 0px 0px;"> Invite Link : <a href = {{$link}}>Click here to sign-up</a> </div>
        </td>
</tr>





@endsection



