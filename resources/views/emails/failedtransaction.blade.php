@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenant_firstname}},
	</td>
</tr>

<tr>
	<td  style="padding: 0 0 20px;">
		Your transaction: {{$reference_id}} has failed, please check your payment settings and/or your bank account!
	</td>
</tr>
<tr>
</tr>

@endsection
