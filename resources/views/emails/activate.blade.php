@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$name}}, 
	</td>
</tr>

<tr>
	<td  style="padding: 0 0 20px;">
		Thanks for signing up for Tenantu.com! Please click the link below to confirm your email address.
		<a href="{{ url('index/accountIsActive/'.$token.'/'.$userType) }}">Finish signing up!</a>
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		<br>
		Please note: This e-mail message is an automated notification. Please do not reply to this message.
	</td>
</tr>

@endsection
