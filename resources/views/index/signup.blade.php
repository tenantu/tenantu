
<!DOCTYPE html>
<html>

<head>
        <title>TenantU - Sign up</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/new_style.css') }}">
        <link rel="stylesheet" href="{{ asset('public/plugins/datepicker/datepicker3.css') }}">
        <link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">

</head>

<body>
        <div class="background-wrapper">
                <!-- Nav -->
                <div class="header-wrapper">
                        <header>
                                <div class="logo">
					<a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/bigLogo.png') }}"></a>
                                </div>
                                <nav>
                                    <a href="{{route('index.searchResult')}}">Listings</a>
				    <a href="{{route('index.ourStory')}}">Our Story</a>
                                    <a href="{{route('index.features')}}">Benefits</a>
                                    <div role="separator" class="divider"></div>
                                    <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                                    <a href="#" class="cta border-button">Sign up</a>
                                </nav>
                                <div class="mobile">
                                    <div class="logo">
                                        <a href="{{route('index.index')}}" class="logo-link"><img src="{{ asset('public/img/new_images/smallerLogo.png') }}"></a>
                                    </div>
                                    <nav>
                                        <a href="{{route('index.searchResult')}}">Listings</a>
                                        <a href="{{route('index.ourStory')}}">Our Story</a>
                                        <a href="{{route('index.features')}}">Benefits</a>
                                        <div role="separator" class="divider"></div>
                                        <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                                        <a href="#" class="cta border-button">Sign up</a>
                                    </nav>
                                </div>
                        </header>
                </div>
  <br> 
  @if(Session::has('reg-failed'))
	@include('elements.validation-message-signup')
  @endif
  @if (Session::has('message-error-signup'))
	@include('elements.message-error-signup')
  @endif
  @if (Session::has('message-success'))
	@include('elements.message-success-signup')
  @endif

                <!-- Main content -->
                <div class="page signup">
                        <div class="page-wrapper">
                                <div class="signup-container" id="field">
                                        <div class="container-fluid">
                                                <div class="row">
                                                        <div class="col-lg-4 col-lg-offset-4">
                                                                <h2 class="text-center">Join TenantU</h2>
                                                                <div class="tabs-wrapper">
                                                                        <ul class="tabs">
                                                                                <li>
                                                                                        <a href="javascript:void(0)" class="tablinks" id="defaultOpen" onclick="switchForm(event, 'tenantForm')">Tenant</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a href="javascript:void(0)" class="tablinks" onclick="switchForm(event, 'landlordForm')">Landlord</a>
                                                                                </li>
                                                                        </ul>
                                                                </div>
																
																{!! Form::open(['id'=>'tenantForm','class'=>'tabcontent','route'=>['index.postSignup']]) !!}
                                <div style = "display:none">                                
				{!! Form::radio('usertype', 'student',true) !!}
				</div>								
                                                                        <div class="row">
                                                                                <div class="col-lg-6">
                                                                                        <div class="form-group">
											{!! Form::text('firstname', Input::old('firstname'), ['class'=>'text-form','id'=>'FirstName','aria-describedby'=>'emailHelp','placeholder'=>'First name']) !!}
                                                                                        </div>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                        <div class="form-group">
                                                                                                {!! Form::text('lastname', Input::old('lastname'), ['class'=>'text-form','id'=>'LastName','placeholder'=>'Last name']) !!}
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::email('email',Input::old('email'),['class'=>'text-form','id'=>'InputEmail','placeholder'=>'University email address']) !!}
                                                                        </div>
									<div class="form-group">
                                                                                {!! Form::password('password',['class'=>'text-form','id'=>'InputPassword','placeholder'=>'Password']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::password('password_confirmation',['class'=>'text-form','id'=>'ConfirmPassword','placeholder'=>'Confirm password']) !!}
                                                                        </div>
									<div class="form-group">
                                                                        {!! Form::text('dob', Input::old('dob'), ['class'=>'form-control login-input','id'=>'dob','placeholder'=>'Birthday','data-provide'=>'datepicker']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control','id'=>'school']) !!}
                                                                        </div>
                                                                        <div class="form-group action-wrapper">
                                                                                <div class="row">
                                                                                        <div class="col-lg-8">
                                                                                                <p class="small">By clicking "Submit", I agree to TenantU's <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use.</a></p>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
																								{!! Form::submit('Submit', ['class' => 'solid-button']) !!}
                                                                                        </div>
                                                                                </div>
                                                                        </div>
																{!! Form::close() !!}
																
																{!! Form::open(['id'=>'landlordForm','class'=>'tabcontent','route'=>['index.postSignup']]) !!}
																<div style="display:none">
																{!! Form::radio('usertype', 'landlord', true) !!}
																</div>
                                                                        <div class="form-group">
                                                                                <div class="row">
                                                                                        <div class="col-lg-6">
                                                                                                {!! Form::text('firstname', Input::old('firstname'), ['class'=>'text-form','id'=>'FirstName','aria-describedby'=>'emailHelp','placeholder'=>'First name']) !!}
                                                                                        </div>
                                                                                        <div class="col-lg-6">
                                                                                                {!! Form::text('lastname', Input::old('lastname'), ['class'=>'text-form','id'=>'LastName','placeholder'=>'Last name']) !!}
                                                                                        </div>
                                                                                </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::email('email',Input::old('email'),['class'=>'text-form','id'=>'InputEmail','placeholder'=>'Email address']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::password('password',['class'=>'text-form','id'=>'InputPassword','placeholder'=>'Password']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::password('password_confirmation',['class'=>'text-form','id'=>'ConfirmPassword','placeholder'=>'Confirm password']) !!}
                                                                        </div>
                                                                        <div class="form-group">
                                                                                {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control','id'=>'school']) !!}
                                                                        </div>
                                                                        <div class="form-group action-wrapper">
                                                                                <div class="row">
                                                                                        <div class="col-lg-8">
                                                                                                <p class="small">By clicking "Submit", I agree to TenantU's <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use.</a></p>
                                                                                        </div>
                                                                                        <div class="col-lg-4">
																								{!! Form::submit('Submit', ['class' => 'solid-button']) !!}
                                                                                        </div>
                                                                                </div>
                                                                        </div>
																{!! Form::close() !!}
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

                <!-- Footer -->
                <div class="footer-wrapper">
                        <footer>
                                <section class="desktop">
                                        <div class="logo">
                                                <a href="{{route('index.index')}}">TenantU</a>
                                                <p>2017 © TenantU Services Inc.</p>
                                        </div>
                                        <div>
                                                <ul>
                                                        <li>
                                                                <a href="{{ route('contactUs') }}">Contact</a>
                                                        </li>
                                                        <li>
                                                            <a href="{{ route('index.ourStory') }}">Our Story</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('cms/privacy-policy') }}">Privacy Policy</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{route('pages.faq')}}">FAQ</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('/')}}/blog">Blog</a>
                                                        </li>
                                                </ul>
                                        </div>
                                </section>
                                <section class="mobile">
                                </section>
                        </footer>
                </div>

                <!-- Scripts -->
                <!-- Tab switcher -->
                <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>
		  <!-- jQuery -->
                <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

                <script>
		$(document).ready(function(){
		$('.dob').attr('autocomplete', 'off');
		});
                function switchForm(evt, memberName) {
                    var i, tabcontent, tablinks;
                    tabcontent = document.getElementsByClassName("tabcontent");
                    for (i = 0; i < tabcontent.length; i++) {
                        tabcontent[i].style.display = "none";
                    }
                    tablinks = document.getElementsByClassName("tablinks");
                    for (i = 0; i < tablinks.length; i++) {
                        tablinks[i].className = tablinks[i].className.replace(" active", "");
                    }
                    document.getElementById(memberName).style.display = "block";
                    evt.currentTarget.className += " active";
                }
		
		var makeHiddenField = function(fieldName,fieldValue){
	        var hidden = document.createElement("input");
    		hidden.setAttribute('type','hidden');
    		hidden.name = fieldName;
    		hidden.setAttribute('value',fieldValue);
    		document.getElementById("tenantForm").appendChild(hidden);
	 	}
	        makeHiddenField("hashContent",window.location.hash);
		
                // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen").click();
                </script>


	
		<!-- datepicker -->
	        <script src="{{ asset('public/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
                <!-- Bootstrap JS -->
                <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        </div>
</body>
</html>

