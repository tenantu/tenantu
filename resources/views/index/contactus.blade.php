@extends('layouts.frontend-master')
@section('content')
@if (Session::has('message-success'))
<br>
@include('elements.message-success')
@endif
@if (Session::has('message'))
@include('elements.message')
@endif
@if (Session::has('message-error'))
<br>
  @include('elements.message-error')
@endif
<div class="container">
        <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="contact-details">
                                <h2>Contact</h2>
                                {!! Form::model('',['method' => 'POST','class'=>'form-inline','route'=>['index.postContactUs']]) !!}
                                <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
                                                <div class="input-group col-sm-5 col-xs-12">
                                                        <label>Name</label>
							@if ($loggedName != "")
                                                        {!!Form::text('name',$loggedName,['class'=>'form-control','id' => 'test','placeholder'=>'Name']) !!}
							@else
							{!!Form::text('name','',['class'=>'form-control','id' => 'test','placeholder'=>'Name']) !!}
                                                        @endif

                                                </div>
                                                <div class="input-group col-sm-5 col-xs-12 col-sm-offset-1">
                                                        <label>Email</label>
							@if ($loggedEmail != "")
                                                        {!! Form::text('email', $loggedEmail,['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Email']) !!}
							@else
                                                        {!! Form::text('email','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Email']) !!}
							@endif

                                                </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                                <div class="input-group col-sm-11 col-xs-12">
                                                        <label>Select School</label>
                                                        <div class="select-box">
							@if ($loggedSchoolId != "")
                                                        {!! Form::select('school',['' => 'Select school']+$school, $loggedSchoolId, ['class'=>'form-control school','id'=>'school']) !!}
							@else
                                                        {!! Form::select('school',['' => 'Select school']+$school, null, ['class'=>'form-control school','id'=>'school']) !!}
							@endif
                                                        </div>
                                                </div>
                                        </div>
                                </div>

                                <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                                <div class="input-group col-sm-11 col-xs-12">
                                                        <label>Reason</label>
                                                        <div class="select-box">
							@if (Session::has('message-error'))
                                                        {!! Form::select('reason',['' => array('Suggest a property'=>"Suggest a property",'Issue with website'=>"Issue with website",'Career'=>"Career",'Other'=>"Other")], "Issue with website", ['class'=>'form-control school','id'=>'reason']) !!}
							@else
                                                        {!! Form::select('reason',['' => array('Suggest a property'=>"Suggest a property",'Issue with website'=>"Issue with website",'Career'=>"Career",'Other'=>"Other")], null, ['class'=>'form-control school','id'=>'reason']) !!}
                                                        @endif
							</div>
                                                </div>
                                        </div>
                                </div>

                                <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
                                                <div class="input-group col-sm-11">
                                                <label>Message</label>
                                                {!! Form::textarea('message', null, ['class'=>'form-control text-area','placeholder'=>'Message', 'rows' => 3]) !!}

                                                </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-sm-12 col-xs-12">
                                                <button type="submit" class="btn contact-submit">Submit</button>
                                        </div>
                                </div>
                        {!!Form::close() !!}
                </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="location-container clearfix">
                        <h3>Location</h3>
                                <div id="map-canvas"></div>
                        <!--<img src="{{ asset('public/img/map.png') }}" class="img-responsive" width="100%">-->
                        <div class="lctn-details">
                                <p>1313 N Market St</p>
                                <p>Wilmington, Delaware 19801</p>
                                <p>Tel : (516) 507-9253</p>
                                <p>Email : <a href="mailto:info@tenantu.com">info@tenantu.com</a></p>
                        </div>
                </div>
        </div>
</div>
</div>

<script src="{{ asset('public/js/jquery.infinitescroll.js') }}"></script>
<script src="{{ asset('public/js/manual-trigger.js') }}"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfo5c1VLkhhvEVUb6rUmim3E7N6vtLK1c&signed_in=false&callback=initialize"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxLBULPEdVBHFNWT885GNx_C4y0W3g-_s&signed_in=false&callback=initialize"></script>

<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>

<script>
var myCenter=new google.maps.LatLng(39.7490337,-75.5462122);

function initialize()
{

var mapProp = {
  center:myCenter,
  zoom:10,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<style type="text/css">
     #map-canvas {
        height: 282px;
        width: 100%;
        /*margin: 0px;*/
        /*padding: 0px*/
    }
  </style>
@endsection

