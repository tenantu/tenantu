<?php
   $url = "https://maps.googleapis.com/maps/api/streetview/metadata?size=600x300&location=".str_replace(" ","%20",$propertyDetail->thoroughfare).",".str_replace(" ","%20",$propertyDetail->locality).",".str_replace(" ","%20",$propertyDetail->administrative_area)."&key=AIzaSyAUrzLcIVGqXVabkmW-J8pF50AmbHlZlhI";
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   $response = curl_exec($ch);
   curl_close($ch);
   $doesHavePano = json_decode($response);
   if($doesHavePano->status == "OK"){
   $panoId = $doesHavePano->pano_id;
   }
   else{
   $panoId = "";
   }
?>
 @extends('layouts.frontend-inner-master')
 @section('content')
 @if (Session::has('message-success'))
	@include('elements.message-success')
 @endif
 @if (Session::has('message'))
	@include('elements.message')
 @endif
 @if (Session::has('message-error'))
   @include('elements.message-error')
 @endif
 <div class="container inner-container single-property">
	  {!! HTML::getBanner(3) !!}

  	<div class="row">
  		<div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
  			<ol class="breadcrumb">
			  @if (Auth::check('tenant') || Auth::check('landlord'))
			    <li><a href="{{route('index.login')}}">Home</a></li>
			  @else
			    <li><a href="{{route('index.index')}}">Home</a></li>
			  @endif
			  <li>Property</li>
			</ol>
  		</div>
  	</div>
  	<div class="row property-list">
  			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  				<div class="row">
  					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		  				<h3>{{ $propertyDetail->property_name}}
		  				<div class="lctn">{{$propertyDetail->location}}</div>
	  				</div>
	  				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right">

					@if (Auth::user('tenant'))
					@if($communication == 'web')
						<a href="{{route('tenants.getMessage',[Crypt::encrypt($propertyDetail->landlord_id), Crypt::encrypt($propertyDetail->id)])}}" class="inquire">Inquire</a>
					@else
					  <a href="" class="inquire btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Inquire</a>
					@endif
					@endif
					@if (Auth::user('tenant'))
              <a href="" class="inquire btn btn-info btn-md" data-toggle="modal" data-target="#applyModal">Submit Application</a>
					@endif

	  				@if (Auth::user('tenant'))
						@if(count($reviewExist) == 0)
							<!--a href="#reviewbox" class="reviews-link">Write a REVIEW</a-->
						@endif
	  				@endif
	  				</div>
  				</div>
  			</div>
  		</div>
  		<div class="row property-list">
  		<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">

  			<!-- Single property Starts-->
  			<!-- //////////////////// -->
  			<div class="single-property">
  				<div class="box">
					  	<div class="row row-margin">
						  	<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 single-propert-details">
						  		<div class="image-cntr">
						  			<ul class="bxslider">

										@if(count($propertyImageDetail) > 1)
										@foreach($propertyImageDetail as $propertyImage)
											<li><img src="{{ HTML::getPropertyImage('',$propertyImage->image_name,'') }}" class="img-responsive">
											</li>
										@endforeach
										@elseif(count($propertyImageDetail) === 1)
											<li><img src="{{ HTML::getPropertyImage('',$propertyImageDetail[0]->image_name,'') }}" class="img-responsive">
											</li>

										@elseif(count($propertyImageDetail) === 0)
											@if($panoId == "")
											<li><img src="{{ HTML::getPropertyImage('','',100) }}" class="img-responsive">
											</li>
											@else
											<li><img src="https://maps.googleapis.com/maps/api/streetview?size=562x341&location={{$propertyDetail->thoroughfare}},{{$propertyDetail->locality}},{{$propertyDetail->administrative_area}}&key=AIzaSyCo23ZE0vHZKtLUgGDhqyTI5YSWk-R42GM" class="img-responsive">
                                                                                        </li>
											@endif
					  					@endif

					  				</ul>
						  		</div>
						  	</div>
						  	<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 single-propert-details">
						  		<div class="rent">
						  			<span class="rent-text">Rent
                                                			@if ($propertyDetail->property_meta->charge_per_bed == 1)<sub>(Per Bed)</sub>
                                                			@else <sub>(Per Unit)</sub>
                                                			@endif
									</span>
						  			<div class="amount">
									@if($propertyDetail->property_meta->rent_price != 0)
									$ {{$propertyDetail->property_meta->rent_price}}<sub> /mo</sub></div>
									@else
									$ --<sub /mo</sub></div>
									@endif
						  		</div>
						  		<div class="beds">
						  			<span class="beds-text">Beds</span>
						  			<div class="numbers"> {{ $propertyDetail->bedroom_no}}</div>
						  		</div>
						  		<div class="baths">
						  			<span class="baths-text">Baths</span>
						  			<div class="numbers">{{$propertyDetail->bathroom_no}}</div>
						  		</div>
						  	</div>
						</div>
						<div class="row hide">

							@foreach($allRatings as $avgRate)
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
								<div class="property-rtns">

									<span>{{$avgRate->rating_name}}</span>
									<img src="{{HTML::getAverageReviewRating($propertyDetail->id,$avgRate->id)}}">
								</div>
							</div>
							@endforeach
						</div>
  				</div>
  			</div>
  			<!-- Single property End-->
  			<!-- //////////////////// -->
  		</div>
  		<div class="col-lg-4 col-md-12 col-sm-12 colxs-12 single-propert-right">
  			<div class="row">
  				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  					<div id="map-canvas"></div>
				</div>
  			</div>
  			<div class="row">


          <!-- START PROPERTY APPLY MODAL -->
      <div class="modal fade bs-example-modal-md" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
              {!! Form::open(['route'=>['tenants.submitApplication']])  !!}
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="myLargeModalLabel">Select Application Group</h3>
              </div>
              <div class="modal-body">
                <table class="table">
                  <tr>
                    <td>Apply without a group</td>
                    <td><input type="radio" name="applicationGroup" value="false" style="opacity: 1;"/> </td>
                  </tr>
                  {!! Form::hidden('propertyId', $propertyDetail->id) !!}
                  @if(!empty($tenantApplicationGroups))
                  @forelse($tenantApplicationGroups as $group)
                    <tr>
                      <td>{!! $group->group_name !!}</td>
                      <td><input type="radio" name="applicationGroup" value="{!! $group->id !!}" style="opacity: 1;"/> </td>
                    </tr>
                  @empty
                  @endforelse
                @endif
                </table>
              </div>
              <div class="modal-footer">
                {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
              </div>
              {!! Form::close() !!}
            </div>
        </div>
      </div>

          <!-- END PROPERTY APPLY MODAL -->
  				<!----- popup enquiry form   -->

  				<div class="modal fade bs-example-modal-md" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				  <div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

					  </div>
		  @if(Session::has('reg-failed'))
			@include('elements.validation-message')
		  @endif
		 <div class="modal-body">
		  {!! Form::open(['class'=>'enquiry-form','route'=>['properties.postEnquiry'],'files'=>true]) !!}

						<h3>Submit your query</h3>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								  <div class="form-group">
									<label for="reviewTitle">Name</label>
									{!! Form::text('name',$tenantName, ['class'=>'form-control input-review','id'=>'reviewTitle','placeholder'=>'Enter your name','required' => '']) !!}
								  </div>
								  <div class="form-group">
									<label for="reviewTitle">Email</label>
									{!! Form::text('email', $tenantEmail, ['class'=>'form-control input-review','id'=>'reviewTitle','placeholder'=>'Enter your email','required' => '']) !!}
								  </div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								  <div class="form-group">
									<label for="reviewTitle">Phone</label>
									{!! Form::text('phone', $tenantPhone, ['class'=>'form-control input-review','id'=>'reviewTitle','placeholder'=>'Enter your phone']) !!}
									{!! Form::hidden('landlordEmail', $landlordEmail) !!}
									{!! Form::hidden('tenantId', $tenantId) !!}
									{!! Form::hidden('propertyId', $propertyDetail->id) !!}
									{!! Form::hidden('pageTitle', $pageTitle) !!}
									{!! Form::hidden('slug', $propertyDetail->slug) !!}
								  </div>
								  <div class="form-group">
									<label for="reviewTitle">Subject</label>
									{!! Form::text('subject', null, ['class'=>'form-control input-review','id'=>'reviewTitle','placeholder'=>'Subject']) !!}
								  </div>
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								  <div class="form-group">
									<label for="yourName">Message</label>
									{!! Form::textarea('message', null, ['class' => 'form-control textarea-review','required' => '','placeholder'=>'Message']) !!}

								  </div>
							</div>

						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<button type="submit" class="btn btn-default login-submit">Submit</button>
							</div>
						</div>
				 {!! Form::close() !!}

				</div>


					</div>
				  </div>
				</div>






			<!----- popup enquiry form   -->

  			</div>
  		</div>
  	</div>
  	 {!! HTML::getBanner(5) !!}

  	<div class="row">
  		<div class="col-lg-8">
  			<!-- description  -->
  				<div class="box description">
  					<h3>Description</h3>
  					<p>{{ $propertyDetail->description}}</p><br>
					<h4>Current Availability Status</h4>
					<p>{{ $propertyDetail->property_meta->availability_status }}</p><br>
					<h4>2018-2019 Availability Status</h4>
					<p>{{ $propertyDetail->property_meta->next_year_availability_status }}</p>
					<br>
					<h4>Max. Occupancy</h4>
					@if($propertyDetail->max_occupancy != 0)
                                        <p>{{ $propertyDetail->max_occupancy }}</p>
					@else
					<p>--</p>
					@endif
  				</div>
  		</div>

  		@if(count($propertyAmenities) > 0)
  		<div class="col-lg-4">
  			<div class="box description">
  				<h3>Amenities</h3>
					@foreach($propertyAmenities as $propertyAmenity)
						<span class="aminity">{{$propertyAmenity->amenity}}</span>
  					@endforeach
  			</div>
  		</div>
  		@endif
  	</div>
  	<div class="row hide">
  		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			@if(count($propertyReviewDetails) >0)
			{!! Form::open(['id'=>'reviews']) !!}
  			<div class="box reviews">
  				<h3>Reviews</h3>

  				<!-- repeat review with this row -->
  				<!-- two reviews per column -->
  				<div class="row border-bottom">

  					<!-- comment single 1 -->
  					<!-- //////////////// -->
  					@foreach($propertyReviewDetails->slice(0,2) as $key=>$review)

  					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 single-review">
  						<div class="user">
  							<span class="name">{{ucfirst($review->firstname).' '.ucfirst($review->lastname)}} </span>
  							<span class="subject">{{$review->title}} </span>
  						</div>
  						<div class="review-ratings row">
							@foreach($allRatings as $rate)
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="rating-texts">{{$rate->rating_name}}
									<img src='{{HTML::getReviewRating($review->id,$rate->id)}}'>
								</div>
							</div>
							@endforeach
  						 </div>
  						<div class="review-text">

							{{substr($review->review, 0, 500)}}

  						</div>
  					</div>

  					@endforeach


  					<!-- comment single 2 border-right class removed -->
  					<!-- /////////////////////////////////////////// -->

  				</div>

  				<!-- comment single row end -->
  				<!-- ////////////////////// -->
				@if(count($propertyReviewDetails) > 2)
  				<a href="{{route('properties.reviews',$propertyDetail->slug)}}" class="show-more">Show More</a>
  				@endif
  			</div>
  			{!! Form::close() !!}
  			@endif
  			<!-- write review -->
  			<!-- ============ -->
  			@if (Auth::user('tenant'))
  			 @if(count($reviewExist) == 0)
  			{!! Form::open(['class'=>'write-review box','id'=>'reviewbox','route'=>['properties.postReview']]) !!}
  			@if (Session::has('message-error'))
				@include('elements.message-error')
			@endif
			@if (Session::has('message'))
				@include('elements.message')
			@endif
  			<h3>Write a Review</h3>
  			<div class="row" >
  				<div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
					  <div class="form-group">
					    <label for="reviewTitle">Title</label>
					    {!! Form::text('reviewTitle',null, ['class'=>'form-control input-review','id'=>'reviewTitle','placeholder'=>'Title']) !!}

					  </div>
					  <div class="form-group">
					    <label for="reviewTitle">Review</label>
					    {!! Form::textarea('review', null, ['class' => 'form-control textarea-review','placeholder'=>'Review']) !!}
					    {!! Form::hidden('tenantId', Auth::user('tenant')->id) !!}
					    {!! Form::hidden('slug', $propertyDetail->slug) !!}
					    {!! Form::hidden('propertyId', $propertyDetail->id) !!}


					  </div>

  				</div>

  				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					@foreach($allRatings as $rating)
  					<div class="review-rtns"> {!! Form::hidden('hdRateValue'.$rating->id, 0) !!}<span class="texts">{{$rating->rating_name}}</span>
						<fieldset id='ratingField{{$rating->id}}' class="rating">
							<input class="stars" type="radio" id="star5" name="rating" value="5" />
							<label class = "full" for="star5" title="5"></label>
							<input class="stars" type="radio" id="star4" name="rating" value="4" />
							<label class = "full" for="star4" title="4"></label>
							<input class="stars" type="radio" id="star3" name="rating" value="3" />
							<label class = "full" for="star3" title="3"></label>
							<input class="stars" type="radio" id="star2" name="rating" value="2" />
							<label class = "full" for="star2" title="2"></label>
							<input class="stars" type="radio" id="star1" name="rating" value="1" />
							<label class = "full" for="star1" title="1"></label>

						</fieldset>
                    </div>
                    @endforeach
  				</div>

  			</div>
  			<div class="row">
  				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  					<button type="submit" class="btn btn-default login-submit">Submit</button>
  				</div>
  			</div>
  			{!! Form::close() !!}
  			 @endif
  			@endif
  			<div class="row">


  		</div>
  		</div>
  	</div>
  </div>
  <script>
  $(document).on('click','#expressInterest',function(){

			bootbox.confirm("Express interest will send an email to landlord with your details", function(result) {
				if(result){
					window.location.href="{{route('properties.interest',$propertyDetail->id)}}";
				}

			});
		})
  </script>
  <script src="{{ asset('public/js/jquery.infinitescroll.js') }}"></script>
  <script src="{{ asset('public/js/manual-trigger.js') }}"></script>
  <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfo5c1VLkhhvEVUb6rUmim3E7N6vtLK1c&signed_in=false&callback=initialize"></script>-->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxLBULPEdVBHFNWT885GNx_C4y0W3g-_s&signed_in=false&callback=initialize"></script>


  <script>
var myCenter=new google.maps.LatLng({{ $propertyDetail->latitude }},{{ $propertyDetail->longitude }});

function initialize()
{

var mapProp = {
  center:myCenter,
  zoom:10,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      ' <div style="color:blue">{{ $propertyDetail->property_name }}</div>'+
      '<div id="bodyContent">'+
      '</div>'+
      '</div>';
var infowindow = new google.maps.InfoWindow({
    content: contentString
  });


var marker=new google.maps.Marker({
  position:myCenter,
  map: map,
  title: '{{ $propertyDetail->property_name }}',
  icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
  });

  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

marker.setMap(map);


}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<style type="text/css">
     #map-canvas {
        height: 419px;
        max-width: 100%;
        /*margin: 0px;*/
        /*padding: 0px*/
    }
    input[type="radio"] {
    opacity: 1;
    }
</style>

  @endsection
