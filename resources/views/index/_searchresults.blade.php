<?php
$i = 0;

?>

@forelse($properties as $key=> $property)
<?php
   $class  = "";
   $statusClass = "";
   if($property->featured==1){
     // $class  = "featured";
      $checkArray[]  = $key;
   }
   if($property->availability_status== 'Not available'){
           $statusClass = "not-available";
   }
   //print_r($checkArray);
   if( $i<3 && ($property->featured==1) && $page==1){
      $class  = "featured";


   }
   $i++;
   if(count($property->images) ==  0){
   $url = "https://maps.googleapis.com/maps/api/streetview/metadata?size=600x300&location=".str_replace(" ","%20",$property->thoroughfare).",".str_replace(" ","%20",$property->locality).",".str_replace(" ","%20",$property->administrative_area)."&key=AIzaSyAUrzLcIVGqXVabkmW-J8pF50AmbHlZlhI";
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
   curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
   curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
   $response = curl_exec($ch);
   curl_close($ch);
   $doesHavePano = json_decode($response);
   if($doesHavePano->status == "OK"){
   $panoId = $doesHavePano->pano_id;
   }
   else{
   $panoId = "";
   }
   }
?>
		<!--- Single Listing Starts -->
			@if (empty($property->apartmentName))
			<li class="single-listing row border-bottom-g item indProperty" data-id="{{ $property->id }}" data-slug="{{ $property->slug }}" data-apartmentid="{{$property->apartmentId}}">
				<div class="photos half-width">
				
						@if (count($property->images) > 1)
							<div class="carousel-wrap">
						@endif

						@forelse ($property->images as $key => $value)


							@if ($key > 0)
								<a href="" class="img-wrap"><img src="{{ HTML::getPropertyImage($property->id, $value->image_name,'','square') }}" /></a>


							@else
								<a class="img-wrap" style="cursor: pointer;"><img src="{{ HTML::getPropertyImage($property->id, $value->image_name,'','square') }}" /></a>


							@endif

							@empty
							@if ($panoId == "")
							<a class="img-wrap" style="cursor: pointer;"><img style="max-width: 100%; max-height: 100%;" src="{{ asset('public/img/new_images/stockHouse.png') }}" /></a>
							@else
							<a class="img-wrap" style="cursor: pointer;"><img style="max-width: 100%; max-height: 100%;" src="https://maps.googleapis.com/maps/api/streetview?size=400x200&location={{$property->thoroughfare}},{{$property->locality}},{{$property->administrative_area}}&key=AIzaSyCo23ZE0vHZKtLUgGDhqyTI5YSWk-R42GM"/></a>
							@endif
						@endforelse

						@if (count($property->images) > 1)
							</div>
						@endif
					<a class="row bottom-info white-text" style="cursor: pointer;">
						<div class="col six-col">
							<h3 class="bold"><sup>$</sup>
							@if($property->rent_price != 0)
							{{ $property->rent_price }}<sub>/mo</sub></h3> 
							@else
							-- <sub>/mo</sub></h3>
							@endif
						@if ($property->charge_per_bed == 1) <p>Per Bed</p> 
						@else <p>Per Unit</p>
						@endif
							<p>{{ $property->thoroughfare.", ".$property->locality.", ".$property->administrative_area }}</p>
						</div>
						<div class="col six-col">
							<div class="row center-align border-bottom-g overall-rating hide">
								<i class="material-icons orange-text">star</i>
								<i class="material-icons orange-text">star</i>
								<i class="material-icons orange-text">star_half</i>
								<i class="material-icons orange-text">star_border</i>
								<i class="material-icons orange-text">star_border</i>
							</div>
							<div class="half-width center-align border-right-g">
								<h3 class="bold">{{ $property->bedroom_no }}</h3>
								<h6>Beds</h6>
							</div>
							<div class="half-width center-align">
								<h3 class="bold">{{ $property->bathroom_no }}</h3>
								<h6>Baths</h6>
							</div>
							<div class="row center-align">
							@if($property->max_occupancy != 0)
                                                        <h3 class="bold">{{ $property->max_occupancy }}</h3>
							@else
							<h3 class="bold">--</h3>
                                                        @endif  
							      <h6>Max Occupancy</h6>

							</div>
						</div>
					</a>
				</div>
				<div class="col six-col single-list-info">
					@if (strlen($property->thoroughfare)>25)
					<h4 class="blue-text bold" style="cursor: pointer;">{{ substr($property->thoroughfare,0,25)."..." }} <br><sub>(Currently {{$property->availability_status}})</sub></h4>
					@else
					<h4 class="blue-text bold" style="cursor: pointer;">{{ $property->thoroughfare }} <br><sub>(Currently {{$property->availability_status}})</sub></h4>
					@endif
					@if (strlen($property->description)>50)
					<p>{{ substr($property->description,0,50)."..." }}</p>
					@else
					<p>{{ $property->description }}</p>
					@endif
					<div class="col twelve-col features">
						<span class="bold">AMENITIES</span><br />
						<p>
						@if ($propertyAmenitiesArray[$property->id] != "")
						@if (count($propertyAmenitiesArray[$property->id]) > 0)
                                                @foreach ($propertyAmenitiesArray[$property->id] as $key => $amenity)
						@if ($key <= 2)
						@if ($amenity === end($propertyAmenitiesArray[$property->id])[count($propertyAmenitiesArray[$property->id])-1])
						{{ $amenity->amenity}}
						@else
						@if ($key != 2)
                                                {{ $amenity->amenity  }},
						@elseif ($key == 2)
						{{ $amenity->amenity }}...
						@endif 
						@endif
						@else
						@endif
                                                @endforeach
                                                @endif
						@endif
						
						</p>
					</div>
					<div class="col six-col rating hide">
						<span class="bold">AMENITIES</span><br />
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star_half</i>
						<i class="material-icons orange-text">star_border</i>
						<i class="material-icons orange-text">star_border</i>
					</div>
					<div class="col six-col rating hide">
						<span class="bold">LANDLORD</span><br />
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star_half</i>
						<i class="material-icons orange-text">star_border</i>
						<i class="material-icons orange-text">star_border</i>
					</div>
					<div class="col six-col rating hide">
						<span class="bold">LOCATION</span><br />
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star_half</i>
						<i class="material-icons orange-text">star_border</i>
						<i class="material-icons orange-text">star_border</i>
					</div>
					<div class="col six-col rating hide">
						<span class="bold">CLEANLINESS</span><br />
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star</i>
						<i class="material-icons orange-text">star_half</i>
						<i class="material-icons orange-text">star_border</i>
						<i class="material-icons orange-text">star_border</i>
					</div>
					<div class="row fav-actions border-top-lg">
						<div class="col three-col">
							<i class="material-icons gray-text hide">favorite_border</i> <i class="hide material-icons gray-text">close</i>
							<i class="material-icons blue-text hide">favorite</i> <i class="hide material-icons blue-text">close</i>
						</div>
						<div class="col nine-col right-align">
							<a href="{{url('/property/id')}}/{{ $property->id }}" class="btn btn-small blue-ghost"><i class="material-icons">mail_outline</i></a>
							<a href="{{url('/property/id')}}/{{ $property->id }}" class="btn btn-small blue-ghost">Details</a>
							<a href="{{url('/property/id')}}/{{ $property->id }}" class="hide btn btn-small blue-bg">Bid Now</a>
						</div>
					</div>

				</div>
				
				
			</li>
			@else
			  <li class="single-listing row border-bottom-g item indProperty" data-id="{{ $property->id }}" data-slug="{{ $property->slug }}" data-apartmentid="{{$property->apartmentId}}">
                                <div class="photos half-width">
					
                                                @if (count($property->images) > 1)
                                                        <div class="carousel-wrap">
                                                @endif

                                                @forelse ($property->images as $key => $value)


                                                        @if ($key > 0)
                                                                <a href="" class="img-wrap"><img src="{{ HTML::getPropertyImage($property->id, $value->image_name,'','square') }}" /></a>


                                                        @else
                                                                <a class="img-wrap" style="cursor: pointer;"><img src="{{ HTML::getPropertyImage($property->id, $value->image_name,'','square') }}" /></a>


                                                        @endif

                                                        @empty
                                                        @if ($panoId == "")
                                                        <a class="img-wrap" style="cursor: pointer;"><img style="max-width: 100%; max-height: 100%;" src="{{ asset('public/img/new_images/stockApartment.png') }}" /></a>
                                                        @else
                                                        <a class="img-wrap" style="cursor: pointer;"><img style="max-width: 100%; max-height: 100%;" src="https://maps.googleapis.com/maps/api/streetview?size=600x300&location={{$property->thoroughfare}},{{$property->locality}},{{$property->administrative_area}}&key=AIzaSyCo23ZE0vHZKtLUgGDhqyTI5YSWk-R42GM"/></a>
                                                        @endif
                                                @endforelse

                                                @if (count($property->images) > 1)
                                                        </div>
                                                @endif

					 
					 <a class="row bottom-info white-text" style="cursor: pointer;">
                                                        <h3 class="bold" style="text-align:center;"> {{$property->apartmentName}}<h3>
 					 </a>
                                        <!--a class="row bottom-info white-text" style="cursor: pointer;">
                                                <div class="col six-col">
                                                        <h3 class="bold"><sup>$</sup>{{ $property->rent_price }}<sub>/mo</sub></h3>
                                                        <h6>{{ $property->property_name }}</h6>
                                                        <p>{{ $property->thoroughfare }}</p>
                                                </div>
                                                <div class="col six-col">
                                                        <div class="row center-align border-bottom-g overall-rating hide">
                                                                <i class="material-icons orange-text">star</i>
                                                                <i class="material-icons orange-text">star</i>
                                                                <i class="material-icons orange-text">star_half</i>
                                                                <i class="material-icons orange-text">star_border</i>
                                                                <i class="material-icons orange-text">star_border</i>
                                                        </div>
                                                        <div class="half-width center-align border-right-g">
                                                                <h3 class="bold">{{ $property->bedroom_no }}</h3>
                                                                <h6>Beds</h6>
                                                        </div>
                                                        <div class="half-width center-align">
                                                                <h3 class="bold">{{ $property->bathroom_no }}</h3>
                                                                <h6>Baths</h6>
                                                        </div>

                                                </div>
                                        </a-->
                                </div>
                                <div class="col six-col single-list-info">
                                        <h4 class="blue-text bold" style="cursor: pointer;">{{ $property->apartmentName }}</h4>
                                        <p>View details in order to see available units</p>
                                        <!--div class="col twelve-col features">
                                                <span class="bold">AMENITIES</span><br />
                                                <p>{{ $property->amenities }}</p>
                                        </div>
                                        <div class="col six-col rating hide">
                                                <span class="bold">AMENITIES</span><br />
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star_half</i>
                                                <i class="material-icons orange-text">star_border</i>
                                                <i class="material-icons orange-text">star_border</i>
                                        </div>
                                        <div class="col six-col rating hide">
                                                <span class="bold">LANDLORD</span><br />
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star_half</i>
                                                <i class="material-icons orange-text">star_border</i>
                                                <i class="material-icons orange-text">star_border</i>
                                        </div>
                                        <div class="col six-col rating hide">
                                                <span class="bold">LOCATION</span><br />
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star_half</i>
                                                <i class="material-icons orange-text">star_border</i>
                                                <i class="material-icons orange-text">star_border</i>
                                        </div>
                                        <div class="col six-col rating hide">
                                                <span class="bold">CLEANLINESS</span><br />
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star</i>
                                                <i class="material-icons orange-text">star_half</i>
                                                <i class="material-icons orange-text">star_border</i>
                                                <i class="material-icons orange-text">star_border</i>
                                        </div-->
                                        <div class="row fav-actions border-top-lg">
                                                <div class="col three-col">
                                                        <i class="material-icons gray-text hide">favorite_border</i> <i class="hide material-icons gray-text">close</i>
                                                        <i class="material-icons blue-text hide">favorite</i> <i class="hide material-icons blue-text">close</i>
                                                </div>
                                                <div class="col nine-col right-align"">
                                                        <a href="{{url('/property/apartments/')}}/{{ $property->apartmentId }}" class="btn btn-small blue-ghost"><i class="material-icons">mail_outline</i></a>
                                                        <a href="{{url('/property/apartments/')}}/{{ $property->apartmentId }}" class="btn btn-small blue-ghost">Details</a>
                                                        <a href="{{url('/property/apartments/')}}/{{ $property->apartmentId }}" class="hide btn btn-small blue-bg">Bid Now</a>
                                                </div>
                                        </div>

                                </div>


                        </li>
			@endif
			
			

		<!--- Single Listing Ends -->

@empty
<p>
   <div class="col-lg-12">No Search results found!</div></p>
@endforelse
