<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />

<title>Search Results - TenantU</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>

    <!--link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet"-->
	<link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet"-->
        <link href="{{ asset('public/css/reset.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/css/main.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    
        <link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">

</head>

<body>
{!! Form::model('',['method' => 'POST','route'=>['index.searchResult'],'class'=>'','id'=>'sortForm']) !!}
<header id="header">
        <a href='{{url("/")}}'><img src="{{ asset('public/img/new_images/bigLogo.png') }}" id="nav-logo" /></a>
        <span class="search-box-wrap">
            {!! Form::text('search',$searchKeyWord, ['placeholder'=>'City, State or Zip','class'=>'search-box', 'id'=>'search']) !!}
        {!! Form::hidden('sortKey','', ['id'=>'sortKeyUpdate']) !!}
        {!! Form::hidden('availabilityKey','', ['id'=>'availabilityKeyUpdate']) !!}
        {!! Form::hidden('propertyType', '', ['id'=>'propertyTypeUpdate'])  !!}
	<span class="material-icons top" id="search-btn">search</span>
        </span>

</header>
<div id="results-wrap" class="full-width full-height relative">
        <div  class="map-view">
                <div id="map"></div>
                <a href="" class="hide btn blue-bg" id="save-search-btn">Save Search</a>
                <!--div class="marker">$2500 <div class="bottom-arrow"></div></div>
                <div class="marker hot"><i class="material-icons">whatshot</i> $1200 <div class="bottom-arrow"></div></div>
                <div class="marker favorite"><i class="material-icons">favorite</i> $800 <div class="bottom-arrow"></div></div-->
        </div>
        <div id="list" class="list-view white-bg">
                <div class="col twelve-col"><h4 class="blue-text">{{$searchKeyWord}} Housing Results</h4></div>
                <div id="filters" class="row light-blue-bg flex-wrap flex-end">
                        <div class="col eight-col" id="filter-price-wrap">
                                <span class="bold">Price</span><br />
                                {!! Form::select('priceFrom',['' => array('0'=>"No Min",'100'=>"100",'200'=>"200",'300'=>"300",'400'=>"400",'500'=>"500",'600'=>"600",'700'=>"700",'800'=>"800", '900'=>"900", '1000'=>"1000", '1100'=>"1100", '1200'=>"1200", '1300'=>"1300",'1400'=>"1400", '1500'=>"1500", '1600'=>"1600", '1700'=>"1700", '1800'=>"1800", '1900'=>"1900", '2000'=>"2000", '2100'=>"2100", '2200'=>"2200", '2300'=>"2300", '2400'=>"2400", '2500'=>"2500", '2600'=>"2600",'2700'=>"2700",'2800'=>"2800",'2900'=>"2900",'3000'=>"3000",'3100'=>"3100",'3200'=>"3200",'3300'=>"3300", '3400'=>"3400", '3500'=>"3500", '3600'=>"3600", '3700'=>"3700", '3800'=>"3800",'3900'=>"3900", '4000'=>"4000", '4100'=>"4100", '4200'=>"4200", '4300'=>"4300", '4400'=>"4400", '4500'=>"4500", '4600'=>"4600", '4700'=>"4700", '4800'=>"4800", '4900'=>"4900", '5000'=>"5000")], $selectedPriceFrom, ['class'=>'five-col','id'=>'priceFrom']) !!}
                                <span class="light">to</span>
                                {!! Form::select('priceTo',['' => array('10000'=>"No Max",'100'=>"100",'200'=>"200",'300'=>"300",'400'=>"400",'500'=>"500",'600'=>"600",'700'=>"700",'800'=>"800", '900'=>"900", '1000'=>"1000", '1100'=>"1100", '1200'=>"1200", '1300'=>"1300",'1400'=>"1400", '1500'=>"1500", '1600'=>"1600", '1700'=>"1700", '1800'=>"1800", '1900'=>"1900", '2000'=>"2000", '2100'=>"2100", '2200'=>"2200", '2300'=>"2300", '2400'=>"2400", '2500'=>"2500", '2600'=>"2600",'2700'=>"2700",'2800'=>"2800",'2900'=>"2900",'3000'=>"3000",'3100'=>"3100",'3200'=>"3200",'3300'=>"3300", '3400'=>"3400", '3500'=>"3500", '3600'=>"3600", '3700'=>"3700", '3800'=>"3800",'3900'=>"3900", '4000'=>"4000", '4100'=>"4100", '4200'=>"4200", '4300'=>"4300", '4400'=>"4400", '4500'=>"4500", '4600'=>"4600", '4700'=>"4700", '4800'=>"4800", '4900'=>"4900", '5000'=>"5000")], $selectedPriceTo, ['class'=>'five-col','id'=>'priceTo']) !!}
                        </div>
                        <div class="col four-col right-align">
                                <a href="" id="add-filters-btn" class="underline bold blue-text">More Filters <i class="material-icons">keyboard_arrow_down</i></a>
                        </div>

                        <div class="row add-filters">
                                <span class="top-arrow"></span>
                                <div class="col six-col">
                                        <h5 class="bold blue-text">Availability</h5>
                                        {!! Form::select('sortKey',['' => array('all'=>"All",'Available'=>"Available",'Not available'=>"Not available")], $selectedAvailabilityKey, ['class'=>'five-col','id'=>'availabilityKey']) !!}
                                        <h5 class="bold blue-text">Property Type</h5>
                                        {!! Form::select('sortKey',['' => array('all'=>"All", 'house'=>"House",'Apartment'=>"Apartment")], $selectedPropertyType, ['class'=>'five-col','id'=>'propertyType']) !!}


                                </div>
                                <div class="col six-col">
                                        <div class="half-width">
                                                <h5 class="bold blue-text">Beds</h5>
                                                {!! Form::select('bedroomId',['' => array('any'=>"Any",'1'=>"1",'2'=>"2",'3'=>"3",'4'=>"4",'5'=>"5",'6'=>"6",'7'=>"7",'8'=>"8")], $selectedBedroomNo, ['class'=>'six-col','id'=>'bedRoom']) !!}
                                        </div>
                                        <div class="half-width">
                                                <h5 class="bold blue-text">Baths</h5>
                                                {!! Form::select('bathroomId',['' => array('any'=>"Any",'1'=>"1",'2'=>"2",'3'=>"3",'4'=>"4",'5'=>"5",'6'=>"6",'7'=>"7",'8'=>"8")], $selectedBathroomNo, ['class'=>'six-col','id'=>'bathRoom']) !!}
                                        </div>
                                        {{-- <div class="col twelve-col">
                                                <h5 class="bold blue-text">Parking</h5>
                                                <input type="checkbox"> <label>Has Parking</label><br />
                                                <input type="checkbox"> <label>Has Garage</label>
                                        </div> --}}
                                        <div class="col twelve-col">

                                          <h5 class="bold blue-text">Amenities</h5>
                                          {!! Form::select('aminity[]',$aminity,$selectedAminity, ['class'=>'five-col','id'=>'aminity','multiple']) !!}
                                        </div>
                                </div>
                                <div class="row right-align border-top-b">
                                        <div class="col twelve-col">
                                                <button class="btn btn-small blue-bg update" id="search-btn">Update Listings</button>
                                        </div>
                                </div>
                        </div>
                </div>
                <div class="row border-bottom-g" id="sorting">
                        <div class="col three-col bold" id="sort"><a href="" class="underline bold blue-text" id="sort-btn"><i class="material-icons">sort</i> Sort</a></div>
                        <!--div class="col nine-col right-align">Showing <span class="bold">50</span> of <span class="bold" id="totalRentals"></span> Rentals</div-->
                        <div class="sort-options">
                                <span class="top-arrow"></span>
                                <a class="row active" id="latestSort" style="cursor:pointer">
                                        <span class="col nine-col">Latest</span><span class="col three-col center-align"><i class="material-icons">check</i></span>
                                </a>
                                <a class="row" id="mostViewedSort" style="cursor:pointer">
                                        <span class="col nine-col">Most Viewed</span><span class="col three-col center-align"><i class="material-icons">check</i></span>
                                </a>
                                <a class="row" id="largestSort" style="cursor:pointer">
                                        <span class="col nine-col">Largest</span><span class="col three-col center-align"><i class="material-icons">check</i></span>
                                </a>
                                <a class="row" id="smallestSort" style="cursor:pointer">
                                        <span class="col nine-col">Smallest</span><span class="col three-col center-align"><i class="material-icons">check</i></span>
                                </a>
				</select>
                        </div>
                </div>
		{!! Form::select('sortKey',['' => array('all'=>"Latest",
                                                  'MR'=>"Most Reviewed",
                                                  'MV'=>"Most Viewed",
                                                  'LtoS'=>"Largest to Smallest(No of bedrooms)",
                                                  'StoL'=>"Smallest to Largest(No of bedrooms)",
                                                  'HRL'=>"Highest Rated Landlord",
                                                  'HRLoc'=>"Highest Rated Location",
                                                  'HRCl'=>"Highest Rated Cleanliness",
                                                  'HRA'=>"Highest Rated Amenities",
                                                  'AvgRat'=>"Highest Rated Overall",
                                                  'WR' => "Would recommend")], $selectedSortKey, ['class'=>'hide selectpicker show-tick form-control sort-select','id'=>'sortKey']) !!}
                {!! Form::text('distance',$selectedDistance, ['max'=>"40", 'class'=>'hide', 'id'=>'input-to']) !!}
        {!! Form::select('landlord_id',['' => 'Select landlord']+$selectedLandlordDropDown, $selectedLandlordId, ['class'=>'hide','id'=>'landlordname']) !!}
        @if(Session::has('schoolName'))
            {!! Form::select('school_id',['' => 'Select school']+$selectedSchool, Session::get('schoolName'), ['class'=>'hide','id'=>'school']) !!}
        @else
            {!! Form::select('school_id',['' => 'Select school']+$selectedSchool, $selectedSchoolId, ['class'=>'hide','id'=>'school']) !!}
        @endif
                {!! Form::close() !!}
                <ul id="list-results"class="">
                @include('index._searchresults')
		<img src="{{ asset('public/img/property-preloader.GIF') }}" id="loading-image" class="" style="margin: auto; display: block; height: 120px; width:auto;" />
                </ul><!-- close #list-results -->
		</div>
        </div><!-- close #list -->

</div><!-- close #results-wrap -->
<div id="map-list-toggle" class="full-width center-align">
        <div class="col eight-col">
                <a href="" class="half-width toggle-view-btn" id="map-btn"> Map View</a>
                <a href="" class="half-width toggle-view-btn active" id="list-btn">List View</a>
        </div><!-- close twelve-col -->
</div><!-- close map-list-toggle -->

<script src="{{ asset('public/js/jquery.infinitescroll.js') }}"></script>
<script src="{{ asset('public/js/manual-trigger.js') }}"></script>
<script src="{{ asset('public/js/bootstrap-select.js') }}"></script>
<script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>
<script type="text/javascript">
        var dataSaved="{{ $selectedAminity }}";
        var dataArray=dataSaved.split(",");
        $("#aminity").val(dataArray);

        $(document).ready(function(){
	$(document).ajaxStart(function() {
            $('#loading-image').show();
        });
        $(document).ajaxStop(function() {
            $('#loading-image').hide();
        });


                $('select[multiple]').multiselect({
                        columns: 1,
                        placeholder: 'Select options',

                        });
                        $('#aminity').bind('change', function(){
                                var foo = [];
                                $('#aminity :selected').each(function(i, selected){
                                  foo[i] = $(selected).text();
                                });

                                $('#hd_aminity').val(foo);
                        });

    <?php if($selectedAminity!= ''){
            ?>

                        $('#hd_aminity').val('<?php echo $selectedAminity ?>');
          <?php
           } ?>


  var landlordId      = "{{ $selectedLandlordId }}";
  var location        = "{{ $searchKeyWord }}";
  var bedroomNo       = "{{ $selectedBedroomNo }}";
  var bathroomNo      = "{{ $selectedBathroomNo }}";
  var distance        = "{{ $selectedDistance }}";
  var priceFrom       = "{{ $selectedPriceFrom }}";
  var priceTo         = "{{ $selectedPriceTo }}";
  var schoolId        = "{{ $selectedSchoolId }}";
  var sortKey         = "{{ $selectedSortKey }}";
  var availabilityKey = "{{ $selectedAvailabilityKey }}";
  var propertyType    = "{{ $selectedPropertyType }}";
  var index = 1;
  var containResults = $('#list-results');

//Infinite Scroll Begin
var loadingImage = $("#loading-image");

var didScroll = false;
var minScrollLength = 500;

setInterval(function() {
    if(didScroll){
    	didScroll = false;
    }
}, minScrollLength);

containResults.on('scroll', function(event)
{
     if(!didScroll){
     var element = event.target;
    	if (((Math.ceil(element.scrollHeight - element.scrollTop) ||  Math.floor(element.scrollHeight - element.scrollTop)) >= (element.clientHeight - 100))&&((Math.ceil(element.scrollHeight - element.scrollTop) ||  Math.floor(element.scrollHeight - element.scrollTop)) <= (element.clientHeight + 30)))
    	{
		index = index + 1;
          	$.ajax({
              	url: "?page="+index+"&search="+location+"&landlord_id="+landlordId+"&bedroomId="+bedroomNo+"&distance="+distance+"&priceFrom="+priceFrom+"&priceTo="+priceTo+"&school_id="+schoolId+"&sortKey="+sortKey+"&availabilityKey="+availabilityKey+"&propertyType="+propertyType,
		success: function (data) { //var stuff = $(data).find('.item');
					   //console.log(data);
					   $('#list-results').append(data);	
					   $("#loading-image").remove(); 
					   $('#list-results').append(loadingImage); },
              	dataType: 'html',
		async: true
           	});
	didScroll = true;
    	}
    }
});

//Infinite Scroll End

//search-btn-top work around

$(".top").on("click", function(){
    $(".update").click();
});

$(document).keypress(function(e) {
    if(e.which == 13) {
        $(".update").click();
    }
});

//search-btn-top work around

//Sort front-end work around begin
  if(sortKey == 'all'){
       $(".active").removeClass("active");
       $("#latestSort").addClass("active");
  }
  else if(sortKey == 'MV'){
        $(".active").removeClass("active");
        $("#mostViewedSort").addClass("active");
  }
  else if(sortKey == 'LtoS'){
        $(".active").removeClass("active");
        $("#largestSort").addClass("active");
  }
  else if(sortKey == 'StoL'){
        $(".active").removeClass("active");
        $("#smallestSort").addClass("active");
  }

  $('#latestSort').on('click', function(){
       $(".active").removeClass("active");
       $("#latestSort").addClass("active");
       $('#sortKey').val('all');
       $('#sortKey').change();
       $('#sortKeyUpdate').val($('#sortKey').val());
       $('#availabilityKeyUpdate').val($(this).val());
       $('#propertyTypeUpdate').val($('#propertyType').val());
       $('#search-btn').click();

    });

  $('#mostViewedSort').on('click', function(){
        $(".active").removeClass("active");
        $("#mostViewedSort").addClass("active");
        $('#sortKey').val('MV');
	$('#sortKey').change();
	$('#sortKeyUpdate').val($('#sortKey').val());
        $('#availabilityKeyUpdate').val($(this).val());
        $('#propertyTypeUpdate').val($('#propertyType').val());
        $('#search-btn').click();
    });

  $('#largestSort').on('click', function(){
        $(".active").removeClass("active");
        $("#largestSort").addClass("active");
        $('#sortKey').val('LtoS');
	$('#sortKeyUpdate').val($('#sortKey').val());
        $('#availabilityKeyUpdate').val($(this).val());
        $('#propertyTypeUpdate').val($('#propertyType').val());
        $('#search-btn').click();
    });

  $('#smallestSort').on('click', function(){
        $(".active").removeClass("active");
        $("#smallestSort").addClass("active");
        $('#sortKey').val('StoL');
	$('#sortKeyUpdate').val($('#sortKey').val());
        $('#availabilityKeyUpdate').val($(this).val());
        $('#propertyTypeUpdate').val($('#propertyType').val());
        $('#search-btn').click();
    });

//Sort front-end work around end

  $(document).on('change','#availabilityKey',function(){
       $('#sortKeyUpdate').val($('#sortKey').val());
       $('#availabilityKeyUpdate').val($(this).val());
       $('#propertyTypeUpdate').val($('#propertyType').val());
       $('#searchForm').submit();
    });
    $(document).on('change','#propertyType',function(){
       $('#sortKeyUpdate').val($('#sortKey').val());
       $('#availabilityKeyUpdate').val($('#availabilityKey').val());
       $('#propertyTypeUpdate').val($(this).val());
       $('#searchForm').submit();
    });
    $(document).on('change','#sortKey',function(){
       $('#sortKeyUpdate').val($(this).val());
       $('#propertyTypeUpdate').val($('#propertyType').val());
       $('#availabilityKeyUpdate').val($('#availabilityKey').val());
       $('#searchForm').submit();
    });
    /*$(document).on('mouseover','.properties li.item', function( e ){

      index =  $(this).index();
      if (markers[index].getAnimation() != google.maps.Animation.BOUNCE) {
        markers[index].setAnimation(google.maps.Animation.BOUNCE);
      }
    });
    $(document).on('mouseout','.properties li.item', function( e ){
       index =  $(this).index();
       markers[index].setAnimation(null);
    });
    */
    });
</script>
<script>

$(document).ready(function () {
        $('.carousel-wrap').owlCarousel({
		loop:true,
		nav : true,
		navText: ["<i class='material-icons'>keyboard_arrow_left</i>","<i class='material-icons'>keyboard_arrow_right</i>"],
		items: 1,
		autoplay:false,
    	autoplayTimeout:5000

        });
        $('#add-filters-btn').click(function () {
                $('.add-filters').slideToggle();
                return false;
    });
        $('#sort-btn').click(function () {
                $('.sort-options').slideToggle();
                return false;
    });
        $('#map-list-toggle a').click(function () {
                $('#results-wrap > div').toggle();
                $('#map-list-toggle a').toggleClass('active');
                return false;
    });
        $(function () {

                        if($(window).width() <= 479) {
                                $('#filter-price-wrap').prependTo('.add-filters');
                                $('#sort').prependTo('#filters');
                        }
            });
        $(window).resize(function(){
                        if($(window).width() <= 479) {
                                $('#filter-price-wrap').prependTo('.add-filters');
                                $('#sort').prependTo('#filters');
                        }
                        if($(window).width() >= 480) {
                                $('#filter-price-wrap').prependTo('#filters');
                                $('#sort').prependTo('#sorting');
                        }
        });
});
</script>

<!-- GOOGLE MAPS -->

<script>

     var geocoder;
     var map;
     var markers = [];

     @if(count($googleMapArray) > 1)
     var list = [
      @foreach( $googleMapArray as $key => $val )
             @if($val['lat'] != '')
                [ {{ $val['lat'] }} ,  {{ $val['lng'] }}, '{{ $val['address'] }}', '{{ $val['slug'] }}', '{{ $val['id'] }}' ],
             @endif
      @endforeach
    ];
    @elseif(count($googleMapArray) == 1)
      var list =[
      [ {{ $googleMapArray[0]['lat'] }} ,  {{ $googleMapArray[0]['lng'] }}, '{{ $googleMapArray[0]['address'] }}', '{{ $googleMapArray[0]['slug'] }}', '{{ $googleMapArray[0]['id'] }}'] ];
     @else
    var list = [];
    @endif

     function initMap() {
         map = new google.maps.Map(document.getElementById('map'), {
         mapTypeId: google.maps.MapTypeId.ROADMAP,
	 zoom: 8
        });

     var bounds = new google.maps.LatLngBounds();

     list.forEach(function (data,index,array){

        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(list[index][0],list[index][1]),
          map: map,
	  icon: 'https://maps.google.com/mapfiles/ms/icons/blue-dot.png',
          title: list[index][2],
	  disableDefaultUI: false,
	  url: "{{url('/property/id')}}/"+list[index][4]
        });
        markers.push(marker);
	bounds.extend(marker.position);
      google.maps.event.addListener(marker, 'click', function() {
         window.location.href = this.url;
      });
      });
      map.fitBounds(bounds);
     }

    $(document).on('mouseover','#list-results li.item', function( e ){
      index =  $(this).index();
      if (markers[index].getAnimation() != google.maps.Animation.BOUNCE) {
        markers[index].setAnimation(google.maps.Animation.BOUNCE);
      }


    });
    $(document).on('mouseout','#list-results li.item', function( e ){
       index =  $(this).index();
       markers[index].setAnimation(null);

    });

    $(document).on('click','#list-results li.item',function( e ){
	console.log(event.target.className);
	if (event.target.className == "blue-text bold"){
        if(($(this).data("apartmentid"))){
        var apptId = $(this).data("apartmentid");
        var url = "{{url('/property/apartments')}}/"+apptId;
        window.location.href=url;
        }
        else{
        var id = $(this).data("id");
        var url = "{{url('/property/id')}}/"+id;
        window.location.href=url;
        }
	}
	else{
	if($(window).width() <= 1023){
	if(($(this).data("apartmentid"))){
	var apptId = $(this).data("apartmentid");
	var url = "{{url('/property/apartments')}}/"+apptId;
	window.location.href=url;
	}
	else{
	var id = $(this).data("id");
        var url   = "{{url('/property/id')}}/"+id;
	window.location.href=url;
	}
	}
	else{
        index = $(this).index();
	map.setZoom(17);
        map.panTo(markers[index].position);
	}
	}
    });

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4EGbQKOUHFDH_Xux4zjh4fVa-DEzw7k4 &callback=initMap">
    </script>

</body>
</html>
