@extends('layouts.frontend-master')
@section('content')
<div class="container">
        @if (Session::has('message'))
                @include('elements.message')
        @endif
                <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="about-container">
                                        <h2>Are you a Landlord?</h2>
                                        <p><style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Exo+2:400,600);

body{font-family: 'Exo 2', sans-serif;}

.sliderWrapper{margin:0 auto; width:1024px;}

.block{width:1024px; float:left; margin-top: 100px;}
.plan-name{float: left;width: 100%;margin: 20px 0;font-size: 18px; font-weight: 600;}
.plan-name a{float: left;width: 25%;text-align: center; cursor:pointer;}
.plan-name a.one{text-align: left;}
.plan-name a.two{text-align: left;}
.plan-name a.three{text-align: left;}
.plan-name a.four{text-align: right;}
.inputProperty {}
#signupLink{ 
    margin-top: 20px;
    margin-bottom: 10px;
    font-size: 30px;
    font-family: 'myriadpro-light';
    font-weight: 100;
    color: #fff;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{
background: rgb(63,81,181);
}
.det-bloc{float: left;}
.totPric{width: 162px !important;}
.col-01{float: left;color: #fff; width: 150px; padding: 15px; margin: 10px;
  
	background: rgb(63,81,181); /* Old browsers */
	background: -moz-linear-gradient(top, rgb(63,81,181) 0%, rgb(32,56,141) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgb(63,81,181)), color-stop(100%,rgb(32,56,141))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgb(63,81,181) 0%,rgb(32,56,141) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgb(63,81,181) 0%,rgb(32,56,141) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgb(63,81,181) 0%,rgb(32,56,141) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgb(63,81,181) 0%,rgb(32,56,141) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#88b73e', endColorstr='#699e16',GradientType=0 ); /* IE6-9 */
}

.det-bloc h2{font-size: 22px;margin-top: 15px;}

#details{
  width:1078.67px;
}

#enterProperty{
  width: 120px;
  color: black;
  font-size: 24px;}

input[type=number]::-webkit-inner-spin-button, 
input[type=number]::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
</style>
 <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>
 </div>
<div class="sliderWrapper hidden">
<div class="block">
<div id="slider"></div>

			<ul id="host-plan-grid">
				<li id="hplanblock-1" style="display: block;">
                  <div class="plan-name">
                	  <a class="one">1</a>
                    <a class="two">250</a>
                    <a class="three">500</a>
                    <a class="four">1000</a>
                  </div><!-- plan-name -->
							
<div class="det-bloc">
	<div class="row" id="details">
	<div class="co-lg-3 col-md-4 col-sm-4 col-xs-12">
	<div class="col-01">
		<p>Properties:</p>
                  	<h2 id="propertyNum">1</h2>
	</div><!-- col-01 -->
	</div>
	<div class="co-lg-3 col-md-4 col-sm-4 col-xs-12">
        <div class="col-01 totPric">
		<p>Total Price:</p>
                	<h2 id="totalPrice">$16 / month</h2>
        </div><!-- col-01 -->
	</div>
	<div class="co-lg-3 col-md-4 col-sm-4 col-xs-12">
        <div class="col-01 inputProperty">
		<p>Enter number of properties :</p>
                  <input type="number" id="enterProperty" </input>
	</div><!-- col-01 -->
	</div>
	</div>
</div><!--det-bloc --> 

				</li>  
			</ul>
  </div><!--block-->
</div><!--wrapper-->
<div class="row">
<div class="co-lg-3 col-md-4 col-sm-4 col-xs-12">
<h3>Reach Your Market</h3>
<p>Throw out your &ldquo;For Rent&rdquo; signs! Students spend over 8 hours a day in front of a screen. Make finding your properties painless by listing them right before their eyes.</p>
</div>
<div class="co-lg-3 col-md-4 col-sm-4 col-xs-12">
<h3>Fluid Listings</h3>
<p>Update your listings and receive property inquiries. Let our advanced sorting features filter all the information a student needs to find their perfect home.</p>
</div>
<div class="co-lg-3 col-md-4 col-sm-4 col-xs-12">
<h3>Organized Property Management</h3>
<p>Effortlessly communicate with tenants, receive rent payments and manage maintenance requests.</p>
</div>
<!--
<div class="co-lg-3 col-md-3 col-sm-2 col-xs-12">
<h3>Build Your Brand</h3>
<p>Student&rsquo;s trust each other. Have tenants write reviews and build your brand on your reputation, not on your advertising budget.</p>
</div>
-->
</div>
 <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    </div>
 </div>
<div style="margin-bottom: 0px !important" class="signup-container">
                                                        <h2>Create your free Account</h2>
                                                        @if(Session::has('reg-failed'))
                                                                @include('elements.validation-message')
                                                        @endif
                                                        @if (Session::has('message-error-signup'))
                                                            @include('elements.message-error-signup')
                                                        @endif
                                                        @if (Session::has('message-success'))
                                                            @include('elements.message-success')
                                                        @endif
                                                        {!! Form::open(['class'=>'form-login','route'=>['index.postSignup']]) !!}
                                                                <label class="checkbox-text hidden">
                                                                        {!! Form::radio('usertype', 'landlord',true) !!}
                                                                   <span class="lbl padding-8">Landlord</span>
                                                                </label>
                                                                <div class="form-group">
                                                                {!! Form::text('firstname', Input::old('firstname'), ['class'=>'form-control login-input','id'=>'firstname','placeholder'=>'First Name']) !!}

                                                          </div>
                                                          <div class="form-group">
                                                                  {!! Form::text('lastname', Input::old('lastname'), ['class'=>'form-control login-input','id'=>'lastname','placeholder'=>'Last Name']) !!}

                                                          </div>
                                                          <div class="form-group">
                                                          <div class="select-box">
                                                                  {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'school']) !!}
                                                         </div>

                                                          </div>
                                                          <div class="form-group">
                                                                   {!! Form::email('email',Input::old('email'),['class'=>'form-control login-input','id'=>'email','placeholder'=>'Email']) !!}

                                                          </div>
                                                          <div class="form-group">
                                                                  {!! Form::password('password',['class'=>'form-control login-input','id'=>'password','placeholder'=>'Password']) !!}

                                                          </div>
                                                          <div class="form-group">
                                                                  {!! Form::password('password_confirmation',['class'=>'form-control login-input','id'=>'confirmpassword','placeholder'=>'Confirm Password']) !!}

                                                          </div>
                                                           {!! Form::submit('Create Account', ['class' => 'btn signup-submit']) !!}

                                                          <div class="links">By creating tenantu account, you acknowledge
that you agree to our <a href="{{route('pages.showCmsPages','terms-of-use')}}">Terms of Use</a> and <a href="{{route('pages.showCmsPages','privacy-policy')}}">Privacy Policy</a></div>
                                                        {!! Form::close() !!}
                                                </div>
 </div>
 <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>	 
<script>
$(function(){

			
			$( "#slider" ).slider({
			range: "min",
			animate:true,
			value:1,
			min: 1,
			max: 1000,
			step: 1,
			slide: function(event, ui) {
		   $('#propertyNum').text(ui.value);
		   var totalPrice = 0;
       totalPrice = 15 + (1 * ui.value);
       $('#totalPrice').text("$"+totalPrice+" / month");
       $('#enterProperty').val(ui.value);
			}
		});
		
		$('.one').click(function(e) {
	     moveSlider(e,1);
	    });
		$('.two').click(function(e) {
	     moveSlider(e,250);
	    });
		$('.three').click(function(e) {
	     moveSlider(e,500);
	    });
		$('.four').click(function(e) {
	     moveSlider(e,1000);
	    });
		
		function moveSlider(e, num) {
		   e.preventDefault();
		   $('#slider').slider(
			 'value',
			 [num]
		   );
		   val = num;
	   	   valCheck();
		 } 		 
  $(".inputProperty").on('input', function(e){
  var propertyValue = $("#enterProperty").val();
  if((propertyValue <= 1000) && (propertyValue >= 1)){
    moveSlider(e,propertyValue);
  }
  else if(propertyValue>1000){
    moveSlider(e,1);  
    $("#enterProperty").val("");  
  }
  else if(propertyValue<1){
    moveSlider(e,1);
    $("#enterProperty").val("");
  }
  else if(propertyValue == 0){}     
});
		 function valCheck() {
	    $('#propertyNum').text(val);
		  var totalPrice = 0;
      totalPrice = 15 + (1 * val);
      $('#totalPrice').text("$"+totalPrice+" / month"); 
		  $('#enterProperty').val(val);
     }
		 
		 
		});
$(window).on('load resize', function () {
  var w = $(".about-container").width();
  $("#details").width(w);
  w2 = w * .98;
  w = w * .92;
  $(".block").width(w);
  $("#googleForm").width(w2);
 });

</script>

                                </div>
                        </div>
                </div>
        </div>

        @endsection


