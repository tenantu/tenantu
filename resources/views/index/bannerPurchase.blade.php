@extends('layouts.frontend-master')
@section('content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@if (Session::has('message'))
@include('elements.message')
@endif
@if (Session::has('message-error'))
@include('elements.message-error')
@endif
@include('elements.validation-message')
<div class="container">
	<div class="row">
		<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
			<div class="contact-details purchase-advertisement-container">
				<h2>{{ $pageTitle}} : {{$banner->banner_name}}</h2>
				{!! Form::model('',['method' => 'POST','class'=>'form-inline','route'=>['index.postBannerPurchase'],'files'=>true]) !!}
				<div class="row">
					{!! Form::hidden('bannerId', $banner->id) !!}
					{!! Form::hidden('bannerName', $banner->banner_name) !!}
					{!! Form::hidden('bannerPrice', $banner->banner_price) !!}
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12">
								<label>First Name</label>
								{!!Form::text('firstname','',['class'=>'form-control','id' => 'test','placeholder'=>'FirstName']) !!}
								
								
							</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12">
								<label>Last Name</label>
								{!! Form::text('lastname','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'LastName']) !!}
								
							</div>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12">
								<label>Phone</label>
								{!! Form::text('phone','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Phone']) !!}
								
							</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
							<div class="input-group col-sm-12 col-xs-12">
								<label>Address</label>
								{!! Form::textarea('address', null, ['class'=>'form-control text-area','placeholder'=>'Address', 'rows' => 3]) !!}
								
							</div>
						</div>
						</div>
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12">
									<label>Banner Url</label>
									{!! Form::text('url','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'http://www.example.com']) !!}
									
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="input-group col-sm-12 col-xs-12">
									<label>Upload banner image({{$banner->banner_size}})</label>
									{!! Form::file('image') !!}
									
								</div>
							</div>
							<div class="col-sm-12 col-xs-12">
								<button type="submit" class="btn contact-submit">Submit</button>
							</div>
						{!!Form::close() !!}
					</div>
				</div>
				
			</div>
		</div>
	</div>
	</div>
	</div>
	@endsection
