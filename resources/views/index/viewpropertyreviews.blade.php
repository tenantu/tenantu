@extends('layouts.frontend-master')
@section('content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@if (Session::has('message'))
@include('elements.message')
@endif
<div class="container inner-container">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="single-property">
				<div class="box reviews">
					<h3 class="border-bottom">Review (Skyline Apartments)</h3>
					<!-- repeat review with this row -->
					<!-- two reviews per column -->
					@foreach($reviews as $review)
					<div class="row border-bottom">
						<!-- comment single 1 -->
						<!-- //////////////// -->
						
						<div class="col-sm-12 col-xs-12 single-review1">
							<div class="user">
								<span class="name">{!! ucfirst($review->firstname).' '.ucfirst($review->lastname)!!}</span>
								<span class="subject">{!! $review->title !!} <a href="#">Tenantu.com</a></span>
							</div>
							
							<div class="review-ratings row">
								@foreach($allRatings as $rate)
								<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
									<div class="rating-texts">{!!$rate->rating_name!!} <img src="{{HTML::getReviewRating($review->id,$rate->id)}}"></div>
								</div>
								@endforeach
							</div>
							
							<div class="review-text">
								{!! $review->review !!}
							</div>
						</div>
					</div>
					@endforeach
					
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
