@extends('layouts.frontend-inner-master')
@section('content')
@if (Session::has('message-success'))
 @include('elements.message-success')
@endif
@if (Session::has('message'))
 @include('elements.message')
@endif
<div class="container inner-container single-property">
   {!! HTML::getBanner(3) !!}

   <div class="row">
     <div class="col-lg-9 col-md-9 col-sm-6 col-xs-12">
       <ol class="breadcrumb">
       @if (Auth::check('tenant') || Auth::check('landlord'))
         <li><a href="{{route('index.login')}}">Home</a></li>
       @else
         <li><a href="{{route('index.index')}}">Home</a></li>
       @endif
       <li>Property</li>
     </ol>
     </div>
   </div>
   <div class="row property-list">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="row">
           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
             <h3>{{ $apartmentBuilding->building_name}}</h3>
             <div class="lctn">{{$apartmentInfo->thoroughfare}}, {{$apartmentInfo->locality}} , {{$apartmentInfo->administrative_area}} </div>
           </div>
           <div class="col-lg-4 col-md-4 col-sm- col-xs-12 text-right">

           </div>
         </div>
       </div>
     </div>
     <div class="row property-list">
     <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

       <!-- Single property Starts-->
       <!-- //////////////////// -->
       <div class="single-property">
         <div class="box">

           <div class="row hide">


           </div>
         </div>
       </div>
       <!-- Single property End-->
       <!-- //////////////////// -->
       <table class="table table-hover">
          <tr>
              <th>Unit Number</th>
              <th>Bedrooms</th>
              <th>Bathroom</th>
              <th>Availablity</th>
              <th>Rent Price</th>
              <th>Details</th>
          </tr>
          @foreach($apartmentUnits as $unit)
            <tr>
              <td>{!! $unit->premise !!}</td>
              <td>{!! $unit->bedroom_no !!}</td>
              <td>{!! $unit->bathroom_no !!}</td>
              <td>{!! $unit->property_meta->availability_status !!} </td>
              <td>${!! $unit->property_meta->rent_price !!}</td>
              <td><button class="btn btn-primary"> <a href="{{url('/property/id')}}/{{ $unit->id }}"><span style="color: white;">Unit Details </span> </a></button> </td>

            </tr>
          @endforeach
       </table>
     </div>

     <div class="col-lg-4 col-md-12 col-sm-12 colxs-12 single-propert-right">
       <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
           <div id="map-canvas"></div>
       </div>
       </div>
       <div class="row">


         <!----- popup enquiry form   -->

         <div class="modal fade bs-example-modal-md" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
         <div class="modal-dialog modal-md">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

           </div>
     @if(Session::has('reg-failed'))
     @include('elements.validation-message')
     @endif

         </div>
         </div>
       </div>






     <!----- popup enquiry form   -->

       </div>
     </div>
   </div>
    {!! HTML::getBanner(5) !!}

   <div class="row">
     <div class="col-lg-8">
       <!-- description  -->

     </div>

     {{-- @if(count($propertyAmenities) > 0) --}}
     <div class="col-lg-4">
       {{-- <div class="box description">
         <h3>Amenities</h3>
         @foreach($propertyAmenities as $propertyAmenity)
           <span class="aminity">{{$propertyAmenity->amenity}}</span>
           @endforeach
       </div> --}}
     </div>
     {{-- @endif --}}
   </div>

 </div>

 <script src="{{ asset('public/js/jquery.infinitescroll.js') }}"></script>
 <script src="{{ asset('public/js/manual-trigger.js') }}"></script>
 <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfo5c1VLkhhvEVUb6rUmim3E7N6vtLK1c&signed_in=false&callback=initialize"></script>-->
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxLBULPEdVBHFNWT885GNx_C4y0W3g-_s&signed_in=false&callback=initialize"></script>


 <script>
var myCenter=new google.maps.LatLng({{ $apartmentInfo->latitude }},{{ $apartmentInfo->longitude }});

function initialize()
{

var mapProp = {
 center:myCenter,
 zoom:10,
 mapTypeId:google.maps.MapTypeId.ROADMAP
 };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var contentString = '<div id="content">'+
     '<div id="siteNotice">'+
     '</div>'+
     ' <div style="color:blue">{{ $apartmentBuilding->building_name }}</div>'+
     '<div id="bodyContent">'+
     '</div>'+
     '</div>';
var infowindow = new google.maps.InfoWindow({
   content: contentString
 });


var marker=new google.maps.Marker({
 position:myCenter,
 map: map,
 title: '{{ $apartmentBuilding->building_name }}',
 icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
 });

 marker.addListener('click', function() {
   infowindow.open(map, marker);
 });

marker.setMap(map);


}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

   <style type="text/css">
    #map-canvas {
       height: 419px;
       max-width: 100%;
       /*margin: 0px;*/
       /*padding: 0px*/
   }
 </style>
 @endsection
