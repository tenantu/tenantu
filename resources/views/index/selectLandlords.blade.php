@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    @if (Session::has('message-error'))
		@include('elements.message-error')
	@endif
	<div class="form-group">
		<a href="{{ route('admin.landlordRegister') }}">{!! Form::button('Register as landlord',['class' => 'btn btn-primary']) !!}</a>
	</div>
        
    {!! Form::model('', ['method' => 'POST','route'=>['landlords.loginWithId']]) !!}
         
        <div class="form-group">
            {!! Form::label('SelectLandlord', 'Select Landlord:') !!}
            <span style="color: red;">*</span><br/>
            {!! Form::select('landlord',['' => 'Select landlord']+$landlords, null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection


