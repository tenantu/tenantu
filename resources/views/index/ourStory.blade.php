<!DOCTYPE html>
<html>
<head>
    <title>TenantU - Our Story</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/new_style.css') }}">
    <link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">

</head>
<body>
<div class="background-wrapper">
    <!-- Nav -->
    <div class="header-wrapper">
        <header>
            <div class="logo">
		<a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/bigLogo.png') }}"></a>
            </div>
            <nav>
                <a href="{{route('index.searchResult')}}">Listings</a>
                <a href="{{route('index.ourStory')}}">Our Story</a>
                <a href="{{route('index.features')}}">Benefits</a>
                <div role="separator" class="divider"></div>
                <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
            </nav>
            <div class="mobile">
                <div class="logo">
		<a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/smallerLogo.png') }}"></a>
                </div>
                <nav>
                    <a href="{{route('index.searchResult')}}">Listings</a>
                    <a href="{{route('index.ourStory')}}">Our Story</a>
                    <a href="{{route('index.features')}}">Benefits</a>
                    <div role="separator" class="divider"></div>
                    <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                    <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
                </nav>
            </div>
        </header>
    </div>
    <!-- Main content -->
    <div class="page features">
        <div class="vertical-center">
            {{--
            <section class="intro">
               --}}
            <div class="page-wrapper" style="padding: 32px">
                <div class="container-fluid" style="padding: 32px">
                    <p>
                        Our story is quite simple and begins much like many of your own.
                        <br><br>When provided the opportunity to move out of the dorms, we took it upon ourselves to make the most of it. Year after year, each move brought unforgettable experiences, and more likely than not, a series of recurring headaches. And the more we dealt with these issues, the more we tried to place the blame:
                        <br><br>
                        Are students just irresponsible tenants?
                        <br><br>
                        Do the property managers expect too much out of these first time renters?
                        <br><br>
                        Could the schools do more in offering assistance to off-campus students?
                        <br><br>
                        The answer is yes, and no, but the better answer is this: 
                        Student housing is a unique industry and therefore requires a unique set of tools. Tools that seemingly did not exist, until now. The age-old adage goes "When you only have a hammer, everything looks like a nail", and while many providers try to market their services as built for student housing, they tend to be no more than slight alterations of their other products while missing the mark on what's truly needed. Through our involvement with both the management and renter sides of this market, we've built, and will continue to develop, the first ever rental management platform purpose-built for student housing.
                        <br><br>
                        Our goal is not just to provide you the right tool for the right situation, but a means for streamlining the myriad of time consuming and mundane tasks that seemingly consume your day.
                        <br><br>
                        Our mission is to bring organization, unity and simplicity to an industry sorely lacking in each, improving the living experiences of all student-renters and the efficiency of those who provide the housing.
                        <br><br>

                        Live easy,
                        <br>
                        The TenantU Team
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
{{--</section>--}}
<div class="page-wrapper">
    <div class="container-fluid">
    </div>
</div>
</div>
<!-- Footer -->
<div class="footer-wrapper">
    <footer>
        <section class="desktop">
            <div class="logo">
                <a href="{{route('index.index')}}">TenantU</a>
                <p>2017 © TenantU Services Inc.</p>
            </div>
            <div>
                <ul>
                    <li>
                        <a href="{{ route('contactUs') }}">Contact</a>
                    </li>
                    <li>
                        <a href="{{ route('index.ourStory') }}">Our Story</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('cms/privacy-policy') }}">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="{{route('pages.faq')}}">FAQ</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('/')}}/blog">Blog</a>
                    </li>
                </ul>
            </div>
        </section>
        <section class="mobile">
        </section>
    </footer>
</div>
<!-- Scripts -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-78780279-1', 'auto');
    ga('send', 'pageview');

</script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- Fade in elements -->
<script type="text/javascript">
    $(document).ready(function () {

        /* Every time the window is scrolled ... */
        $(window).scroll(function () {

            /* Check the location of each desired element */
            $('.fadeIn').each(function (i) {

                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                /* If the object is completely visible in the window, fade it it */
                if (bottom_of_window > bottom_of_object) {

                    $(this).animate({'opacity': '1'}, 500);

                }

            });

        });

    });

</script>
<!-- Bootstrap JS -->
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>

</div>
</body>
</html>
