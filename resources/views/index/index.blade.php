<!DOCTYPE html>
<html>

<head>
	<title>TenantU</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/new_style.css') }}">
        <link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">

</head>

<body class="home">

	<!-- Nav -->
	<div class="background-wrapper">
		<div class="header-wrapper">
			<header>
				<div class="logo">
					<a href="#" class="logo-link" ><img src="{{ asset('public/img/new_images/bigLogo.png') }}"></a>
				</div>
				<nav>
					<a href="{{route('index.searchResult')}}">Listings</a>
					<a href="{{route('index.ourStory')}}">Our Story</a>
					<a href="{{route('index.features')}}">Benefits</a>
					<div role="separator" class="divider"></div>
					<a href="{{route('index.login')}}">{{ $loginButton }}</a>
					<a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
				</nav>
				<div class="mobile">
					<div class="logo">
					<a href="#" class="logo-link"><img src="{{ asset('public/img/new_images/smallerLogo.png') }}"></a>
					</div>
					<nav>
						<a href="{{route('index.searchResult')}}">Listings</a>
						<a href="{{route('index.ourStory')}}">Our Story</a>
						<a href="{{route('index.features')}}">Benefits</a>
					<div role="separator" class="divider"></div>
					<a href="{{route('index.login')}}">{{ $loginButton }}</a>
					<a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
					</nav>
					<!--div class="menu">
						<div class="menu-icon">
							<span></span>
							<span></span>
							<span></span>
						</div>
					</div>-->
				</div>
			</header>
		</div>

		<!-- Main content -->
		<div class="page home">
			<div class="page-wrapper">
				<div class="vertical-center">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<h1 class="floating"><span style="font-size: 128px; line-height: .5; color: #369AFF;">live easy</span></h1>
								<p>Rental management and listings, purpose-built for student housing.</p>
								<div class="row">
									<div class="col-lg-12 col-md-12">
										<a href="{{route('index.signup')}}" class="cta border-button">Join now</a>
									</div>
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<img src="{{ asset('public/img/new_images/home/tenantu_landing-page.png') }}" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Footer -->
		<div class="footer-wrapper">
			<footer>
				<section class="desktop">
					<div class="logo">
						<a href="#">TenantU</a>
						<p>2017 © TenantU Services Inc.</p>
					</div>
					<div>
						<ul>
							<li>
								<a href="{{ route('contactUs') }}">Contact</a>
							</li>
							<li>
								<a href="{{ route('index.ourStory') }}">Our Story</a>
							</li>
							<li>
								<a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use</a>
							</li>
							<li>
								<a href="{{ URL::to('cms/privacy-policy') }}">Privacy Policy</a>
							</li>
							<li>
								<a href="{{route('pages.faq')}}">FAQ</a>
							</li>
							<li>
								<a href="{{ URL::to('/')}}/blog">Blog</a>
							</li>
						</ul>
					</div>
				</section>
				<section class="mobile">
				</section>
			</footer>
		</div>
		<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

        </script>
		<!-- Scripts -->
		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
		<!-- Bootstrap JS -->
		<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
		<!-- Carousel -->
		<!-- <script src="js/carousel.js"></script> -->
	</div>
</body>
</html>

