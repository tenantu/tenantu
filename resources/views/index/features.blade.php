<!DOCTYPE html>
<html>

<head>
        <title>TenantU - Benefits</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/new_style.css') }}">
        <link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">

</head>

<body>
        <div class="background-wrapper">
                <!-- Nav -->
        <div class="header-wrapper">
                        <header>
                                <div class="logo">
                                <a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/bigLogo.png') }}"></a>
                                </div>
                                <nav>
                                        <a href="{{route('index.searchResult')}}">Listings</a>
                                        <a href="{{route('index.ourStory')}}">Our Story</a>
                                        <a href="#">Benefits</a>
                                        <div role="separator" class="divider"></div>
                                        <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                                        <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
                                </nav>
                                <div class="mobile">
                                        <div class="logo">
                                        <a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/bigLogo.png') }}"></a>
                                        </div>
                                        <nav>
                                                <a href="{{route('index.searchResult')}}">Listings</a>
                                                <a href="{{route('index.ourStory')}}">Our Story</a>
                                                <a href="{{route('index.features')}}">Benefits</a>
                                                <div role="separator" class="divider"></div>
                                                <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                                                <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
                                        </nav>
                                        <!--div class="menu">
                                            <div class="menu-icon">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </div>-->
                                </div>
                        </header>
                </div>

                <!-- Main content -->
		<br><br>
		<div class="row">
                                                     <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-lg-4 col-lg-offset-4">
                                                                        <div class="text-area"style="margin-bottom: 0px !important;">
                                                                                <h1 class="">College living, simplified.</h1>
                                                                                <div class="underline"></div><br>
                                                                                <p class="feature">We focus on both landlords and tenants to create a seamless, convenient college living experience.</p>

                                                                        </div>
                                                                </div>
		</div>
                <div class="page features">
			<div class="col-sm-7 col-sm-offset-2 col-lg-4 col-lg-offset-4">
			<div class="tabs-wrapper">
                                                                        <ul class="tabs">
                                                                                <li>
                                                                                        <a href="javascript:void(0)" class="tablinks" id="defaultOpen" onclick="switchForm(event, 'tenantForm')">Tenant</a>
                                                                                </li>
                                                                                <li>
                                                                                        <a href="javascript:void(0)" class="tablinks" onclick="switchForm(event, 'landlordForm')">Landlord</a>
                                                                                </li>
                                                                        </ul>
                                                                </div>
			</div>
                        <div class="vertical-center">
				<div class="row">
				<div class="col-xs-8 col-xs-offset-2 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                                <section class="landlords">
				<div id='landlordForm' class='tabcontent'>
<h3><a id="Step_1_Post_Your_Listings_2"></a>Step 1. Post Your Listings</h3><br>
<p><em>The days of hanging a “For Rent” sign on the front door with a vague contact number are over, but fortunately the future is a lot brighter and easier!</em>. Simply add your properties – complete with all the features that make you and your listings stand out – and place them directly in front of your target market. It’s easy and it’s <a href="#table">free!</a></p>
<br><h3><a id="Step_2_Receive_Applications_4"></a>Step 2. Receive Applications</h3><br>
<p>Every application you receive will be from a verified student at your local university, <em>that is our promise to you!</em> Not just that, but with our Student Housing Common Application, you aren’t just receiving their name, but all the information you need to make a leasing decision.</p>
<br><h3><a id="Step_3_Make_a_Decision_6"></a>Step 3. Make a Decision</h3><br>
<p>One click is all it takes! Hit “Accept” to start a dialogue with your next tenants or “Reject” to let them know their search is ongoing.</p>
<br><h3><a id="Step_4_Manage_8"></a>Step 4. Manage</h3><br>
<p>We know students aren’t always the easiest to work with, which is why we’re here. From managing and servicing maintenance requests to collecting rent, TenantU is designed to be simple, easy to use, and built with you and your needs in mind.<br>
<br>Ready to get started? <a href="{{ route('index.signup')}}">Click here!</a></p>
								<br>
								<div class="row">
								<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
								<ul class="pricing_table ">
                                				<li class="price_block">
                                    				<h3>Included</h3>
                                 				  <div class="price">
                                       				   <div class="price_figure">
                                       				<span class="price_number">FREE</span>
                                            			<span class="price_tenure"></span>
                             				           </div>
                              					      </div>
                                    					  <ul class="features">
                                       	 				   <li>Property Upload</li>
                                        				   <li>Individual Tenant Portals</li>
                                        				   <li>On-Site Messaging</li>
                             					           <li>Property Listings in School Marketplace</li>
                                        				   <li>24/7 Email Support</li>
									   <li>Maintenance Request Processing</li>
					                                   <li>Rent Payment Processing</li>
                                      					   <li>Accounting Export</li>
                                 					   </ul>
                                 					   <div class="footer">
                                       					 <a href="{{route('index.signup')}}" class="action_button">Sign Up</a>
                					                    </div>
		
                               					 </li>
					                         </ul>
								</div>
								</div>
								<br>
								<div class="col-xs-8 col-xs-offset-2 col-sm-7 col-sm-offset-2 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
								<div class="cta border-button"><a href="{{route('contactUs')}}"> Questions?</a></div>
								</div>
	                        </div>
				<div id='tenantForm' class='tabcontent'>
<h3><a id="Step_1_Look_Around_2"></a>Step 1. Look Around</h3><br>
<p>So you’re looking for a place to call home? Well luck for you, you’ve come to the right place! Once signed up, you can instantly search through <em>hundreds/thousands</em> of <em>approved</em> listings conveniently located by your school and specifically for students like you.</p>
<br><h3><a id="Step_2_Make_a_Decision_5"></a>Step 2. Make a Decision</h3><br>
<p>So now you’re done looking and you’ve found the perfect home for you and your friends. Applying has never been easier! Fill out your student housing common application ONCE and apply to as many houses as you’d like. It’s that easy! You can even include your friends in applications and make everyones lives a little easier.</p>
<br><h3><a id="Step_3_Get_Accepted_7"></a>Step 3. Get Accepted</h3><br>
<p>Congratulations! You and your friends have just been accepted to your dream home. Once this happens, you can immediately open a dialogue with the landlord and put on the finishing touches to make it official!</p>
<br><h3><a id="Step_4_Live_9"></a>Step 4. Live</h3><br>
<p>Just because it’s signed, sealed and delivered doesn’t mean the job is done. In fact, the fun has only just begun! We know being a college student isn’t easy, and having housing issues doesn’t make it any easier! That’s why we’ve taken the hassle out of being a student renter.</p>
<br><h4><a id="Have_a_maintenance_issue_11"></a>Have a maintenance issue?</h4><br>
<p>Eventually something needs to be fixed, and when it does, our 5-click maintenance system tracks it from request to completion for all to see!</p>
<br><h4><a id="Need_to_pay_your_rent_13"></a>Need to pay your rent?</h4>
<p>Of course you do! But the days of hastily coordinating rent payments with your roommates is a thing of the past!</p>
<br>
<p><span>&#8226;</span>  No more opening up joint bank accounts.</p>
<p><span>&#8226;</span>  No more paper checks</p>
<p><span>&#8226;</span>  No more “Hey, did you pay rent?”<br>
<br>
Just connect your bank account and submit your payment when you’re ready, or put it on auto-pay and forget about it!<br>
<br>Ready to get started? <a href="{{route('index.signup')}}">Click here!</a></p><br>
				<div class="col-sm-7 col-sm-offset-2 col-lg-4 col-lg-offset-4">
                                <div class="cta border-button"><a href="{{route('contactUs')}}"> Questions?</a></div>
                                </div>
  	                        </div>
                                </section>
				</div>
				</div>
                        </div>
                </div>

                <!-- Footer -->
                <div class="footer-wrapper">
                        <footer>
                                <section class="desktop">
                                        <div class="logo">
                                                <a href="{{route('index.index')}}">TenantU</a>
                                                <p>2017 © TenantU Services Inc.</p>
                                        </div>
                                        <div>
                                                <ul>
                                                <li>
                                                                <a href="{{ route('contactUs') }}">Contact</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ route('index.ourStory') }}">Our Story</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('cms/privacy-policy') }}">Privacy Policy</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{route('pages.faq')}}">FAQ</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('/')}}/blog">Blog</a>
                                                        </li>
                                                </ul>
                                        </div>
                                </section>
                                <section class="mobile">
                                </section>
                        </footer>
                </div>

                <!-- Scripts -->

                <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
                <!-- Fade in elements -->
                <script type="text/javascript">
                $(document).ready(function() {

                    /* Every time the window is scrolled ... */
                    $(window).scroll( function(){

                        /* Check the location of each desired element */
                        $('.fadeIn').each( function(i){

                            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                            var bottom_of_window = $(window).scrollTop() + $(window).height();

                            /* If the object is completely visible in the window, fade it it */
                            if( bottom_of_window > bottom_of_object ){

                                $(this).animate({'opacity':'1'},500);

                            }

                        });

                    });

                });
                </script>
                <!-- Bootstrap JS -->
                <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
        </div>
</body>
</html>
<script>
               function switchForm(evt, memberName) {
                    var i, tabcontent, tablinks;
                    tabcontent = document.getElementsByClassName("tabcontent");
                    for (i = 0; i < tabcontent.length; i++) {
                        tabcontent[i].style.display = "none";
                    }
                    tablinks = document.getElementsByClassName("tablinks");
                    for (i = 0; i < tablinks.length; i++) {
                        tablinks[i].className = tablinks[i].className.replace(" active", "");
                    }
                    document.getElementById(memberName).style.display = "block";
                    evt.currentTarget.className += " active";
                }
		document.getElementById("defaultOpen").click();
</script>
<style>
    * {
        margin: 0;
        padding: 0;
    }
    body {
        /*font-family: Metropolis;*/
        /*src: url(../../../public/fonts/Metropolis-Bold.woff);*/
    }
    .col-centered{
        float: none !important;
        margin: 0 auto;
    }
    /*----------
    Blocks
    ----------*/
    /*Pricing table and price blocks*/
    .pricing_table {
        line-height: 150%;
        font-size: 12px;
        margin: 0 auto;
        width: 100%;
        max-width: 2000px;
        padding-top: 10px;
        margin-top: 10px;
    }
    .price_block {
        text-align: center;
        width: 100%;
        color: #fff;
        float: left;
        list-style-type: none;
        transition: all 0.25s;
        position: relative;
        box-sizing: border-box;
        margin-bottom: 10px;
        border-bottom: 1px solid transparent;
    }
    /*Price heads*/
    .pricing_table h3 {
        padding: 10px 0;
        background: #333;
        margin: -10px 0 1px 0;
    }
    /*Price tags*/
    .price {
        display: table;
        background: #444;
        width: 100%;
        height: 70px;
    }
    .price_figure {
        font-size: 24px;
        text-transform: uppercase;
        vertical-align: middle;
        display: table-cell;
    }
    .price_number {
        font-weight: bold;
        display: block;
    }
    .price_tenure {
        font-size: 11px;
    }
    /*Features*/
    .features {
        color: #000;
    }
    .features li {
        padding: 8px 15px;
        font-size: 11px;
        list-style-type: none;
    }
    .footer {
        padding: 15px;
    }
    .action_button {
        text-decoration: none;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        background: linear-gradient(#666, #333);
        padding: 5px 20px;
        font-size: 11px;
        text-transform: uppercase;
    }
    .price_block:hover {
        box-shadow: 0 0 0px 5px rgba(0, 0, 0, 0.5);
        transform: scale(1.04) translateY(-5px);
        z-index: 1;
        border-bottom: 0 none;
    }
    .price_block:hover .price {
        /*background:linear-gradient(#369AFF, #369AFF);*/
        box-shadow: inset 0 0 45px 1px #369AFF;
    }
    .price_block:hover h3 {
        background: #222;
    }
    .price_block:hover .action_button {
        background: linear-gradient(#369AFF, #369AFF);
    }
    @media only screen and (min-width: 1px) and (max-width: 479px) {
        .price_block {
            width: 100%;
        }
        .price_block:nth-child(odd) {
            border-right: 1px solid transparent;
        }
        .price_block:nth-child(3) {
            clear: both;
        }
        .price_block:nth-child(odd):hover {
            border: 0 none;
        }
    }
    @media only screen and (min-width: 480px) and (max-width: 768px) {
        .price_block {
            width: 100%;
        }
        .price_block:nth-child(odd) {
            border-right: 1px solid transparent;
        }
        .price_block:nth-child(3) {
            clear: both;
        }
        .price_block:nth-child(odd):hover {
            border: 0 none;
        }
    }
    @media only screen and (min-width: 768px) {
        .price_block {
            width: 100%;
        }
        .price_block {
            border-right: 1px solid transparent;
            border-bottom: 0 none;
        }
        .price_block:last-child {
            border-right: 0 none;
        }
        .price_block:hover {
            border: 0 none;
        }
    }
    .skeleton, .skeleton ul, .skeleton li, .skeleton div, .skeleton h3, .skeleton span, .skeleton p {
        border: 5px solid rgba(255, 255, 255, 0.9);
        border-radius: 5px;
        margin: 7px !important;
        background: rgba(0, 0, 0, 0.05) !important;
        padding: 0 !important;
        text-align: left !important;
        display: block !important;
        width: auto !important;
        height: auto !important;
        font-size: 10px !important;
        font-style: italic !important;
        text-transform: none !important;
        font-weight: normal !important;
        color: black !important;
    }
    .skeleton .label {
        font-size: 11px !important;
        font-style: italic !important;
        text-transform: none !important;
        font-weight: normal !important;
        color: white !important;
        border: 0 none !important;
        padding: 5px !important;
        margin: 0 !important;
        float: none !important;
        text-align: left !important;
        text-shadow: 0 0 1px white;
        background: none !important;
    }
    .skeleton {
        display: none !important;
        margin: 100px !important;
        clear: both;
    }
</style>


