@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($socialLinks,['method' => 'POST','route'=>['sociallinks.update'],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('Twitter', 'Twitter:') !!}<span style="color: red;">*</span>
            {!! Form::text('twitter',$socialLinks[0]->twitter,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('Facebook', 'Facebook:') !!}<span style="color: red;">*</span>
            {!! Form::text('facebook',$socialLinks[0]->facebook,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('Instagram', 'Instagram:') !!}<span style="color: red;"></span>
            {!! Form::text('instagram',$socialLinks[0]->instagram,['class'=>'form-control']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection


