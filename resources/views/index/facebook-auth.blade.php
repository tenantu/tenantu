
@extends('layouts.frontend-master')
@section('content')
<div class="container">
		@if (Session::has('message'))
			 @include('elements.message')
		@endif
		<div class="row">
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
						<div class="signup-container">
							{!! Form::open(['class'=>'form-login','method'=>'post','route'=>['facebook.signup']]) !!}
							<h2>signup as</h2>
							<div> <!-- tent-lnd-radio-btns -->
								<label class="checkbox-text">
								 {!! Form::radio('usertype', 'tenant',true) !!}
								 <span class="lbl padding-8">Tenant</span>
								 </label>
								 <label class="checkbox-text">
								 {!! Form::radio('usertype', 'landlords') !!}
								 <span class="lbl padding-8">Landlord</span>
								 </label>
							</div>
							<div class="clearfix"></div>
							<div class="select-box">
								  {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'school','required' => '']) !!}
							 </div>
							 {!! Form::hidden('firstname',$inputs['firstname']) !!}
							 {!! Form::hidden('lastname',$inputs['lastname']) !!}
							 {!! Form::hidden('email',$inputs['email']) !!}
							 {!! Form::hidden('fb_id',$inputs['fb_id']) !!}
							 {!! Form::hidden('is_active',$inputs['is_active']) !!}
							 {!! Form::hidden('is_fb_user',$inputs['is_fb_user']) !!}
							 
							 {!! Form::submit('Sign up', ['class' => 'btn login-submit login-button']) !!}
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@endsection
	
	
