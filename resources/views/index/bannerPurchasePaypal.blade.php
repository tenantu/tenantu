@extends('layouts.frontend-master')
@section('content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@if (Session::has('message'))
@include('elements.message')
@endif
@if (Session::has('message-error'))
  @include('elements.message-error')
@endif
@include('elements.validation-message')
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="contact-details">
			<div class="text-center refresh">
				<div><img src="{{ asset('public/img/property-preloader.GIF')}}"></div>
				Please wait....Do not refresh..
			</div>
				{!! Form::model('',['method' => 'POST','class'=>'form-inline','id'=>'bannerPay','url'=>'https://www.paypal.com/cgi-bin/webscr']) !!}
				
				<?php $bussEmail =  Config::get('constants.PAYPAL_BUSS_EMAIL');
					  $cancelUrl =  URL::to('/advertisement').'?paid=0';
					  $successUrl= URL::to('/advertisement').'?paid=1';
					  $notifyUrl = URL::to('/paypal_payment_notify');
				?>
				
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="business" value="{{$bussEmail}}"> 
					<input type="hidden" name="currency_code" value="USD">
					
					<input type="hidden" name="item_number" value="{{$input['bannerId']}}">
					<input type="hidden" name="item_name" value="{{$input['banner_name']}}">
					<input type="hidden" name="amount" value="{{$input['banner_price']}}">
					<input type="hidden" name="first_name" value="{{$firstName}}">
					<input type="hidden" name="last_name" value="{{$lastName}}">
					<input type="hidden" name="custom" value="{{ $bannerPurchaseId }}">
					<input type="hidden" name="cancel_return" value="{{ $cancelUrl }}">
                    <input type="hidden" name="return" value="{{ $successUrl }}"> 
                    <input type="hidden" name="notify_url" value="{{ $notifyUrl}}">
				{!!Form::close() !!}
				</div>
				
			<!--{!!Form::close() !!}-->
			
			<script>
			$(document).ready(function(){
				$("#bannerPay").submit();
			});
			</script>
		</div>
	</div>
	
</div>



@endsection
