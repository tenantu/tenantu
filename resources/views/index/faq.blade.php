@extends('layouts.frontend-master')
@section('content')
<div class="container">
	@if (Session::has('message'))
			 @include('elements.message')
		@endif
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-4 col-xs-12">
				<div class="faq-container">
					<h2>{{$pageTitle}}</h2>
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php $i=0; ?>
					@foreach($faqDetail as $faq)
					<?php $i=$i+1; 
						if($i== 1)
						$class= 'in';
						else
						$class= '';
					?>
					  <div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="headingOne">
					      <h4 class="panel-title">
					        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{$faq->id}}" aria-expanded="true" aria-controls="collapseOne{{$faq->id}}">
					          {{$faq->question}}
					        </a>
					      </h4>
					    </div>
					    <div id="collapseOne{{$faq->id}}" class="panel-collapse collapse {{$class}}" role="tabpanel" aria-labelledby="headingOne">
					      <div class="panel-body">
							  {!!html_entity_decode($faq->answer)!!}
					      </div>
					    </div>
					  </div>
					@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@endsection
	
