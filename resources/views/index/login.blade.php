
<!DOCTYPE html>
<html>

<head>
        <title>TenantU - Log in</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('public/css/new_style.css') }}">
        <link rel="shortcut icon" href="{{ asset('public/img/new_images/favicon1.ico') }}" type="image/x-icon">
        <!--script src="{{ asset('public/js/bootstrap.min.js') }}"></script-->
</head>

<body>
        <div class="background-wrapper">
                <!-- Nav -->

                <div class="header-wrapper">
                        <header>
                                <div class="logo">
				<a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/bigLogo.png') }}"></a>
                                </div>
                                <nav>
                                        <a href="{{route('index.searchResult')}}">Listings</a>
					<a href="{{route('index.ourStory')}}">Our Story</a>
                                        <a href="{{route('index.features')}}">Benefits</a>
                                        <div role="separator" class="divider"></div>
                                        <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                                        <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
                                </nav>
                                <div class="mobile">
                                        <div class="logo">
					<a href="{{route('index.index')}}" class="logo-link" ><img src="{{ asset('public/img/new_images/smallerLogo.png') }}"></a>
                                        </div>
                                        <nav>
                                                <a href="{{route('index.searchResult')}}">Listings</a>
                                                <a href="{{route('index.ourStory')}}">Our Story</a>
                                                <a href="{{route('index.features')}}">Benefits</a>
                                                <div role="separator" class="divider"></div>
                                                <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                                                <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
                                        </nav>
                                        <!--div class="menu">
                                            <div class="menu-icon">
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                            </div>
                                        </div>-->
                                </div>
                        </header>
                </div>
<br>		
 @if (Session::has('message'))
    @include('elements.message-login')
 @endif
 @if(Session::has('login-failed'))
        @include('elements.validation-message-signup')
 @endif
 @if (Session::has('message-error'))
        @include('elements.message-error-login')
 @endif

                <!-- Main content -->
                <div class="page login">
                        <div class="page-wrapper">
                                <div class="login-container">
                                        <div class="container-fluid">
                                                <div class="row">
                                                        <div class="col-lg-4 col-lg-offset-4">
                                                                <h2 class="text-center">Welcome home.</h2>
                                                                {!! Form::model('',['method' => 'POST','class'=>'form-login','id'=>'loginform','route'=>['index.postLogin']]) !!}
																<form>
                                                                        <div class="form-group">
																		{!! Form::email('email', Input::old('email'), ['class'=>'text-form','id'=>'InputEmail','placeholder'=>'Email address', 'aria-describedby'=>'emailHelp']) !!}
                                                                        </div>
                                                                        <div class="form-group">
																		{!! Form::password('password',['class'=>'text-form','id'=>'InputPassword','placeholder'=>'Password']) !!}
                                                                        </div>
                                                                        <div class="action-wrapper">
                                                                                <a href="{{ route('index.forgotPassword') }}" class="forgot-pswd">Forgot password?</a>
																				{!! Form::submit('Log in', ['class' => 'solid-button right']) !!}
                                                                        </div>
                                                                </form>
																{!! Form::close() !!}
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>

                <!-- Footer -->
                <div class="footer-wrapper">
                        <footer>
                                <section class="desktop">
                                        <div class="logo">
                                                <a href="{{route('index.index')}}">TenantU</a>
                                                <p>2017 © TenantU Services Inc.</p>
                                        </div>
                                        <div>
                                                <ul>
                                                        <li>
                                                                <a href="{{ route('contactUs') }}">Contact</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ route('index.ourStory') }}">Our Story</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('cms/privacy-policy') }}">Privacy Policy</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{route('pages.faq')}}">FAQ</a>
                                                        </li>
                                                        <li>
                                                                <a href="{{ URL::to('/')}}/blog">Blog</a>
                                                        </li>
                                                </ul>
                                        </div>
                                </section>
                                <section class="mobile">
                                </section>
                        </footer>
                </div>

                <!-- Scripts -->
                <!-- jQuery -->
                <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
                <!-- Bootstrap JS -->
                <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
                <!-- Carousel -->
        </div>
        <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>
</body>
</html>

