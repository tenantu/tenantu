@extends('layouts.frontend-master')
@section('content')
<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="forgot-password-container">
					@include('elements.validation-message')
					@if (Session::has('message-error'))
						@include('elements.message-error')
					@endif
					@if (Session::has('message-success'))
						@include('elements.message-success')
					@endif
					{!! Form::open(['route'=>['index.postResetPassword']]) !!}
						 
						<h2>Reset Password</h2>
						<input type="hidden" name="token" value="{{ $token }}">
						<input type="hidden" name="userType" value="{{ $userType }}">
						<div class="form-group">
							{!! Form::password('password', ['class'=>'form-control login-input','id'=>'password','placeholder'=>'password']) !!}
					    </div>
					    <div class="form-group">
							{!! Form::password('password_confirmation', ['class'=>'form-control login-input','id'=>'password','placeholder'=>'confirm password']) !!}
					    </div>
					   {!! Form::submit('Submit', ['class' => 'btn btn-default login-submit']) !!}
					  <div class="clearfix"></div>
					  
				   {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
