<!DOCTYPE html>
<html>
<head>
    <title>TenantU - Benefits</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/new_style.css') }}">
</head>
<body>
<div class="background-wrapper">
    <!-- Nav -->
    <div class="header-wrapper">
        <header>
            <div class="logo">
                <a href="{{route('index.index')}}" class="logo-link">TenantU</a>
            </div>
            <nav>
                <a href="{{route('index.searchResult')}}">Listings</a>
                <a href="#">Pricing</a>
                <a href="{{route('index.features')}}">Features</a>
                <div role="separator" class="divider"></div>
                <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
            </nav>
            <div class="mobile">
                <div class="logo">
                    <a href="{{route('index.index')}}">TenantU</a>

                </div>
                <nav>
                    <a href="{{route('index.searchResult')}}">Listings</a>
                    <a href="{{route('index.pricing')}}">Pricing</a>
                    <a href="{{route('index.features')}}">Features</a>
                    <div role="separator" class="divider"></div>
                    <a href="{{route('index.login')}}">{{ $loginButton }}</a>
                    <a href="{{route('index.signup')}}" class="cta border-button">Sign up</a>
                </nav>
            </div>
        </header>
    </div>
    <!-- Main content -->
    <div class="page features">
        <div class="vertical-center">
            {{--
            <section class="intro">
               --}}
            <div class="page-wrapper" style="padding: 32px">
                <div class="container-fluid" style="padding: 32px">
                    <div class="row">
                            <div class="col-lg-4">
                            <div class="row">
                                    <div class="text-area">
                                        <h1 class="page-intro">Plans</h1>
                                        <div class="underline"></div>
                                    </div>

                            </div>
                            <div class="row">
                            <div class="text-area">
                                <p class="feature"> We have a variety of plans for different requirements, and price
                                    points.
                                </p>
                                <br>
                                <p>
                                    <a href="tel:6092403220">
                                        <div class="cta border-button"> Questions? Call us! </div>

                                    </a>
                                </p>
                            </div>
                            </div>
                            </div>
                            <div class="col-lg-8">
                            <ul class="pricing_table">
                                <li class="hide price_block">
                                    <h3>Personal</h3>
                                    <div class="price">
                                        <div class="price_figure">
                                            <span class="price_number">FREE</span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <li>Property Upload</li>
                                        <li>Individual Tenant Portals</li>
                                        <li>On-Site Messaging</li>
                                        <li>Maintenance Request Processing</li>
                                        <li>Property Listings in School Marketplace</li>
                                        <li>24/7 Email Support</li>
                                        <li><del>Rent Payment Processing</del></li>
                                        <li><del>Accounting Export</del></li>
                                        <li><del>Digital Document Signing</del></li>
                                    </ul>
                                    <div class="footer">
                                        <a href="{{route('index.signup')}}" class="action_button">Click Here</a>
                                    </div>
                                </li>
                                <li class="price_block">
                                    <h3>Included</h3>
                                    <div class="price">
                                        <div class="price_figure">
                                            <span class="price_number">FREE</span>
                                            <span class="price_tenure"></span>
                                        </div>
                                    </div>
                                    <ul class="features">
                                        <li>Property Upload</li>
                                        <li>Individual Tenant Portals</li>
                                        <li>On-Site Messaging</li>
                                        <li>Maintenance Request Processing</li>
                                        <li>Property Listings in School Marketplace</li>
                                        <li>24/7 Email Support</li>
                                        <li>Rent Payment Processing</li>
                                        <li>Accounting Export</li>
                                        <li>Digital Document Signing</li>
                                    </ul>
                                    <div class="footer">
                                        <a href="{{route('index.signup')}}" class="action_button">Click Here</a>
                                    </div>

                                </li>
                                </ul>
                            </div>
                        </div>
                </div>
            </div>
        </div>
            </div>
        </div>
    </div>
    {{--</section>--}}
    <div class="page-wrapper">
        <div class="container-fluid">
        </div>
    </div>
</div>
<!-- Footer -->
<div class="footer-wrapper">
    <footer>
        <section class="desktop">
            <div class="logo">
                <a href="{{route('index.index')}}">TenantU</a>
                <p>2017 © TenantU Services Inc.</p>
            </div>
            <div>
                <ul>
                    <li>
                        <a href="{{ route('contactUs') }}">Contact</a>
                    </li>
                    <li>
                        <a href="{{ route('index.ourStory') }}">Our Story</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('cms/terms-of-use') }}">Terms of Use</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('cms/privacy-policy') }}">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="{{route('pages.faq')}}">FAQ</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('/')}}/blog">Blog</a>
                    </li>
                </ul>
            </div>
        </section>
        <section class="mobile">
        </section>
    </footer>
</div>
<!-- Scripts -->
<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>
<script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<!-- Fade in elements -->
<script type="text/javascript">
    $(document).ready(function () {

        /* Every time the window is scrolled ... */
        $(window).scroll(function () {

            /* Check the location of each desired element */
            $('.fadeIn').each(function (i) {

                var bottom_of_object = $(this).offset().top + $(this).outerHeight();
                var bottom_of_window = $(window).scrollTop() + $(window).height();

                /* If the object is completely visible in the window, fade it it */
                if (bottom_of_window > bottom_of_object) {

                    $(this).animate({'opacity': '1'}, 500);

                }

            });

        });

    });
    
</script>
<!-- Bootstrap JS -->
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<style>
    * {
        margin: 0;
        padding: 0;
    }
    body {
        /*font-family: Metropolis;*/
        /*src: url(../../../public/fonts/Metropolis-Bold.woff);*/
    }
    /*----------
    Blocks
    ----------*/
    /*Pricing table and price blocks*/
    .pricing_table {
        line-height: 150%;
        font-size: 12px;
        margin: 0 auto;
        width: 100%;
        max-width: 2000px;
        padding-top: 10px;
        margin-top: 10px;
    }
    .price_block {
        text-align: center;
        width: 100%;
        color: #fff;
        float: left;
        list-style-type: none;
        transition: all 0.25s;
        position: relative;
        box-sizing: border-box;
        margin-bottom: 10px;
        border-bottom: 1px solid transparent;
    }
    /*Price heads*/
    .pricing_table h3 {
        padding: 10px 0;
        background: #333;
        margin: -10px 0 1px 0;
    }
    /*Price tags*/
    .price {
        display: table;
        background: #444;
        width: 100%;
        height: 70px;
    }
    .price_figure {
        font-size: 24px;
        text-transform: uppercase;
        vertical-align: middle;
        display: table-cell;
    }
    .price_number {
        font-weight: bold;
        display: block;
    }
    .price_tenure {
        font-size: 11px;
    }
    /*Features*/
    .features {
        color: #000;
    }
    .features li {
        padding: 8px 15px;
        border-bottom: 1px solid #ccc;
        font-size: 11px;
        list-style-type: none;
    }
    .footer {
        padding: 15px;
    }
    .action_button {
        text-decoration: none;
        color: #fff;
        font-weight: bold;
        border-radius: 5px;
        background: linear-gradient(#666, #333);
        padding: 5px 20px;
        font-size: 11px;
        text-transform: uppercase;
    }
    .price_block:hover {
        box-shadow: 0 0 0px 5px rgba(0, 0, 0, 0.5);
        transform: scale(1.04) translateY(-5px);
        z-index: 1;
        border-bottom: 0 none;
    }
    .price_block:hover .price {
        /*background:linear-gradient(#369AFF, #369AFF);*/
        box-shadow: inset 0 0 45px 1px #369AFF;
    }
    .price_block:hover h3 {
        background: #222;
    }
    .price_block:hover .action_button {
        background: linear-gradient(#369AFF, #369AFF);
    }
    @media only screen and (min-width: 1px) and (max-width: 479px) {
        .price_block {
            width: 100%;
        }
        .price_block:nth-child(odd) {
            border-right: 1px solid transparent;
        }
        .price_block:nth-child(3) {
            clear: both;
        }
        .price_block:nth-child(odd):hover {
            border: 0 none;
        }
    }
    @media only screen and (min-width: 480px) and (max-width: 768px) {
        .price_block {
            width: 100%;
        }
        .price_block:nth-child(odd) {
            border-right: 1px solid transparent;
        }
        .price_block:nth-child(3) {
            clear: both;
        }
        .price_block:nth-child(odd):hover {
            border: 0 none;
        }
    }
    @media only screen and (min-width: 768px) {
        .price_block {
            width: 50%;
        }
        .price_block {
            border-right: 1px solid transparent;
            border-bottom: 0 none;
        }
        .price_block:last-child {
            border-right: 0 none;
        }
        .price_block:hover {
            border: 0 none;
        }
    }
    .skeleton, .skeleton ul, .skeleton li, .skeleton div, .skeleton h3, .skeleton span, .skeleton p {
        border: 5px solid rgba(255, 255, 255, 0.9);
        border-radius: 5px;
        margin: 7px !important;
        background: rgba(0, 0, 0, 0.05) !important;
        padding: 0 !important;
        text-align: left !important;
        display: block !important;
        width: auto !important;
        height: auto !important;
        font-size: 10px !important;
        font-style: italic !important;
        text-transform: none !important;
        font-weight: normal !important;
        color: black !important;
    }
    .skeleton .label {
        font-size: 11px !important;
        font-style: italic !important;
        text-transform: none !important;
        font-weight: normal !important;
        color: white !important;
        border: 0 none !important;
        padding: 5px !important;
        margin: 0 !important;
        float: none !important;
        text-align: left !important;
        text-shadow: 0 0 1px white;
        background: none !important;
    }
    .skeleton {
        display: none !important;
        margin: 100px !important;
        clear: both;
    }
</style>
</div>
</body>
</html>
