                @extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
<!-- Main content -->
  <section class="content my-contacts lndlrd-contacts">
    @include('elements.breadcrump-landlord')
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="add-tenants adds">

          <!-- form submission -->
          <!-- ===================== -->
          @include('elements.validation-message')

           @if (Session::has('message-success'))
              @include('elements.message-success')
           @endif
           @if (Session::has('message-error'))
              @include('elements.message-error')
           @endif
          {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'tenantAddForm','route'=>['landlords.posttenant'],'files'=>true]) !!}
            <div class="row">
              <div class="col-sm-12">
                <!-- heading -->
              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                        <label class="checkbox-text">
                         {!! Form::radio('siteUser', 'addTenantOnSite') !!}
                         <span class="lbl padding-8">Add tenant on site</span>
                         </label>
                        <label class="checkbox-text">
                        {!! Form::radio('siteUser', 'inviteTenantByEmail',true) !!}
                         <span class="lbl padding-8">Invite tenant by email</span>
                         </label>
                         <label class="checkbox-text">
                         {!! Form::radio('siteUser', 'massInviteTenantsByEmail') !!}
                         <span class="lbl padding-8">Mass invite tenants by email</span>
                         </label>


                                                <div class="form-group" id="tenantName">
                                                        <label>Name:<span style="color: red;">*</span></label>
                                                        {!!Form::text('name','',['class'=>'form-control','id' => 'test','placeholder'=>'Name']) !!}
                                                </div>
                                                <div class="form-group" id="tenantEmail">
                                                        <label>Email:<span style="color: red;">*</span></label>
                                                        {!! Form::text('email','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Email']) !!}
                                                </div>

                                <div class="form-group" id="selectTenant">
                  {!! Form::label('selectTenant', 'Tenant Email:') !!}<span style="color: red;">*</span>
                {!! Form::text('email','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Email']) !!}
</div>
                <div class="form-group" id="selectProperty">
                  {!! Form::label('property', 'Property:') !!}<span style="color: red;">*</span>
                  {!! Form::select('poperty_id',['' => 'Select Property']+$propertyDropdown, null, ['class'=>'form-control login-input','id'=>'popertyId']) !!}
                </div>
                <div class="form-group" id="agreementStart">
                  {!! Form::label('agriment_starts_on', 'Agreement starts on:') !!}<span style="color: red;">*</span>
                  {!! Form::text('agriment_start_date', Input::old('agriment_start_date'), ['class'=>'form-control login-input','id'=>'agriment_start_date','placeholder'=>date('Y-m-d'),'data-provide'=>'datepicker']) !!}
                </div>
                <div class="form-group" id="cycleLength">
                  {!! Form::label('cycle', 'Lease Length:') !!}<span style="color: red;">*</span>
                  {!! Form::select('cycle',['' => 'Select One']+$cycleDropdown, null, ['class'=>'form-control login-input','id'=>'cycle']) !!}
                </div>
                <div class="col-sm-9">
                  {!! Form::submit('Submit', ['class' => 'btn add-btn' , 'id' => 'submitButton']) !!}
                </div>
              </div>

            </div>
          {!! Form::close() !!}

          <div class = "row massTenantContent" id="massTenantContent">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<div class=”form-group”>
		<b>Inviting Multiple Tenants</b><br><br>

		To invite multiple tenants at once, follow the steps listed below. <br>
		Click the “Download .csv Form”<br><br>
		</div>
		<div class=”form-group”>
		<b>Property ID:</b> <u>Required</u><br>
		When adding tenants to a property state the corresponding “Property ID” provided in the table below these instructions.
		<br>
		<br>
		</div>
		<div class=”form-group”>
		<b>Name:</b> <u>Required</u><br>
		State the name of the tenant you are inviting to a property.
		<br>
		<br>
		</div>
		<div class=”form-group”>
		<b>Email:</b> <u>Required</u><br>
		State the email of the tenant you are inviting to a property. This must be a .edu email.
		<br>
		<br>
		</div>
		<div class=”form-group”>
		<b>Lease Length:</b> <u>Required</u><br>
		When stating the lease length for a tenant look to the table below for the Valid Lease Input which corresponds to the Lease Length which you require. 
		<br>
		<br>
		</div>
		<div class=”form-group”>
		<b>Agreement Start Date:</b> <u>Required</u><br>
When stating the agreement start date for a tenant you must use the format YYYY-MM-DD.
		<br>
		<br>
		</div>
		<div class=”form-group”>
		If there are any errors in your input you will be returned a document which will show you where the errors occurred. If any further help is necessary contact us at support@tenantu.com<br><br>
		</div>
                <div class id="downloadButton">
                <a href=" {{ route('landlords.downloadCsv') }} " style="color: white;display:inline-block;" class="btn add-btn" type="submit">Download .csv Form</a>
                <span class="btn btn-success fileinput-button" data-toggle="modal" data-target=".UploadCsv" style="cursor: pointer;"><i class="glyphicon glyphicon-plus"></i>Upload .csv File</span>
                {!! Form::open(['class'=>'edit-tnt-form','route'=>['landlords.postCsv'],'files'=>true]) !!}
                <div class="modal fade bs-example-modal-sm UploadCsv" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button aria-label="Close" data-dismiss="modal" id="closeModal"  class="close" type="button"><span aria-hidden="true">X</span></button>
              <h4 id="mySmallModalLabel" class="modal-title">Upload .csv File</h4>
            </div>
            <div class="modal-body">
               {!! Form::file('csv', ['id' => 'csv']) !!}
                <br>
               <button type="submit" id="submitModal" class="btn btn-primary">Submit</button>
            </div>
            </div>
          </div>
        </div>
        {!! Form::close() !!}
        </div>
        </div>
		</div>
 <!-- end row -->
        <div class = "row" id="massTenantContent2">
	<div class = "form-group" id="leaseTable">
	</div>
        </div>
          <!-- form submission Ends -->
          <!-- ========================== -->
        </div>
      </div>
    </div>
  <!-- /.row -->

  </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
      @endsection


<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
// caching a reference to the dependant/conditional content:
var conditionalContentEmailX = $('#tenantName'),
        conditionalContentEmailY = $('#tenantEmail'),
        conditionalContentTenantUX = $('#selectTenant'),
        conditionalContentTenantUY = $('#selectProperty'),
        conditionalContentTenantUZ = $('#agreementStart'),
        conditionalContentTenantUC = $('#cycleLength'),
        conditionalContentMassA = $('#massTenantContent'),
	conditionalContentMassC = $('#massTenantContent2'),
        conditionalContentMassB = $('#submitButton'),
    // caching a reference to the group of inputs, since we're using that
    // same group twice:
    group = $('input[type=radio][name=siteUser]');
// binding the change event-handler:
group.change(function() {
  // toggling the visibility of the conditionalContent, which will
  // be shown if the assessment returns true and hidden otherwise:
  conditionalContentTenantUX.toggle(group.filter(':checked').val() === 'addTenantOnSite');
  conditionalContentEmailX.toggle(group.filter(':checked').val() === 'inviteTenantByEmail');
  conditionalContentEmailY.toggle(group.filter(':checked').val() === 'inviteTenantByEmail');
  conditionalContentMassA.toggle(group.filter(':checked').val() === 'massInviteTenantsByEmail');
  conditionalContentMassC.toggle(group.filter(':checked').val() === 'massInviteTenantsByEmail');
  conditionalContentMassB.toggle(group.filter(':checked').val() !== 'massInviteTenantsByEmail');
  conditionalContentTenantUY.toggle(group.filter(':checked').val() !== 'massInviteTenantsByEmail');
  //conditionalContentTenantUX.toggle(group.filter(':checked').val() !== 'massInviteTenantsByEmail');
  conditionalContentTenantUZ.toggle(group.filter(':checked').val() !== 'massInviteTenantsByEmail');
  conditionalContentTenantUC.toggle(group.filter(':checked').val() !== 'massInviteTenantsByEmail');

  // triggering the change event on the group, to appropriately show/hide
  // the conditionalContent on page-load/DOM-ready:
}).change();

$("#submitModal").click(function() {
  $("#closeModal").click();
});
var createLeaseDiv = function(){
    var leaseDiv = document.createElement('div');
    $(leaseDiv).addClass("form-group");
    $(leaseDiv).attr('id', "leaseTable");
    $('#massTenantContent2').append(leaseDiv);
}
var createRowOfInfo = function(rowNum){
    var rowDiv = document.createElement('div');
	$(rowDiv).addClass("row");
	$('#massTenantContent2').append(rowDiv);
    var formGroup = document.createElement('div');
	$(formGroup).addClass("form-group");
	$(formGroup).attr('id', "propertyTableRow"+rowNum);
    	$(rowDiv).append(formGroup);
}

var propertyArray = [
@foreach ($landlordPropertyList as $landlordProperty)
        [ '{{$landlordProperty->property_name}}',"{{$landlordProperty->id}}" ],
@endforeach
];
function leaseTableCreate(row,collumn,appendToId){
    var body = document.body,
        tbl  = document.createElement('table');
    tbl.style.width  = '300px';
    tbl.style.border = '1px solid black';
    tbl.style.marginLeft = '15px';

    for(var i = 0; i < row; i++){
        var tr = tbl.insertRow();
        for(var j = 0; j < collumn; j++){
                var td = tr.insertCell();
                td.id =appendToId+"_"+ i + "," + j;
                td.style.border = '1px solid black';
        }
    }
    document.getElementById(appendToId).appendChild(tbl);
    document.getElementById("leaseTable_0,0").innerHTML = "<b>Lease Length</b>";
    document.getElementById("leaseTable_0,1").innerHTML = "<b>Valid Lease Input</b>";
    document.getElementById("leaseTable_1,0").innerHTML = "3 Months";
    document.getElementById("leaseTable_1,1").innerHTML = "3months";
    document.getElementById("leaseTable_2,0").innerHTML = "6 Months";
    document.getElementById("leaseTable_2,1").innerHTML = "6months";
    document.getElementById("leaseTable_3,0").innerHTML = "1 Year";
    document.getElementById("leaseTable_3,1").innerHTML = "1year";
    document.getElementById("leaseTable_4,0").innerHTML = "2 Years";
    document.getElementById("leaseTable_4,1").innerHTML = "2year";
    document.getElementById("leaseTable_5,0").innerHTML = "3 Years";
    document.getElementById("leaseTable_5,1").innerHTML = "3year";
}

function propertyTableCreate(row,collumn,appendToId){
    var tableSize =  11;
    var rowSets = Math.ceil(row / tableSize);
    var propertyArrayPos = 0;
    var propertyArrayLength = propertyArray.length;
    var initialAppendDiv;
    var appendCount = 0;
    var rowCount = 0;
    var scrapTables = 0;
    var currentRes = $(window).width();
    if((currentRes<1375) && (currentRes >= 992)){
    $('#massTenantContent2').empty();
    createLeaseDiv();
    leaseTableCreate(6,2,"leaseTable");
    scrapTables = 1;
    numberPerRow = 2;
    }
    if((currentRes>=1375)){
    $('#massTenantContent2').empty();
    createLeaseDiv();
    leaseTableCreate(6,2,"leaseTable");
    scrapTables = 1;
    numberPerRow = 3;
    }
    if(scrapTables == 1){
    for(var k = 0; k < rowSets; k++){
	var tbl  = document.createElement('table');
    	tbl.style.width  = '300px';
   	tbl.style.border = '1px solid black';
	tbl.style.marginRight = '20px';
	tbl.style.marginLeft = '20px';
	tbl.style.marginTop = '20px';
	tbl.style.marginBottom = '20px';
        $(tbl).addClass("col-md-6 col-lg-4 col-xl-4");
	if ((k == (rowSets-1)) && ((propertyArrayLength % 10)!=0)){
        tableSize = (propertyArrayLength % 10);
	tableSize += 1;
        }
	if((appendCount % numberPerRow) == 0){
	rowCount += 1;
	initialAppendDiv = createRowOfInfo(rowCount);
	}
    for(var i = 0; i < tableSize; i++){
        var tr = tbl.insertRow();
        for(var j = 0; j < collumn; j++){
                var td = tr.insertCell();
                if(i==0 && j==0){
                td.innerHTML = "<b>Property Name</b>";
                propertyArrayPos -= 1;
		}
                else if(i==0 && j==1){
                td.innerHTML = "<b>Property ID</b>";
                }
                else{
                //td.innerHTML = "lol";
		td.innerHTML = propertyArray[propertyArrayPos][j];
		}
                td.id =appendToId+"_"+ i + "," + j;
                td.style.border = '1px solid black';
        }
	propertyArrayPos += 1;
    }
    appendCount += 1;
    $('#propertyTableRow'+rowCount).append(tbl);
    }
    scrapTables = 0;
    }
}

var propertyCount = {{$landlordPropertyCount[0]->propert_count}};

leaseTableCreate(6,2,"leaseTable");
if(propertyArray.length > 0){
propertyTableCreate(propertyCount+1,2,"propertyTable");

$(window).resize(function(){
propertyTableCreate(propertyCount+1,2,"propertyTable");
});
}

});
</script>
