@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          @include('elements.breadcrump-landlord')
          <!-- Small boxes (Stat box) -->
          <!--div class="row"-->
	    <table class="table my-tenants table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
        @if ((Request::segment(3) == 'Name') && (Request::segment(4) == 'ASC') )
                   <th class="col-md-2"><a href="{{ route('landlords.getContact', ['Name', 'DESC']) }}">Name ^</a></th>
            @elseif ((Request::segment(3) == 'Name') && (Request::segment(4) == 'DESC') )
                   <th class="col-md-2"><a href="{{ route('landlords.getContact', ['Name', 'ASC']) }}">Name v</a></th>
            @else
               <th class="col-md-2"><a href="{{ route('landlords.getContact', ['Name', 'DESC']) }}">Name</a></th>
        @endif
                <th class="col-md-2">
                   <a>Profile</a>
                </th>
                <th class="col-md-2">
                   <a>Conversation</a>
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($tenants as $tenant)
            <tr>
                <th scope="row">{{ $tenant->lastname.', '.$tenant->firstname }}</th>
				<td><a href="{{route('landlords.getTenantProfileFromLandlord',$tenant->slug)}}" class="message-buttons">View Profile</a></td>
                <td><a href="{{ route('landlords.getMessageList',$tenant->latestMsgThreadId->message_thread_id) }}" class="message-buttons">View Conversation</a></td>     
            </tr>
          @endforeach
        </tbody>
      </table>
        <!--/div--><!-- /.row -->
         </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
