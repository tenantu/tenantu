@extends('layouts.landlord-inner')
@section('inner-content')


<div class="content-wrapper">
	<!-- Content Header (Page header) -->
		@if (Session::has('warningMessage'))
		<div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
		@endif
		@if ($profile->plan_type == 1)
			<a href="{!! route('landlords.subscription') !!}">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>We hope you're enjoying your free TenantU account!</strong>
			<p>If you're enjoying your expereince with TenantU, and want to experience our full feature set, please upgrade your account! <br>Simply click this banner, and you will
				be redirected to our upgrade page!</p>
		</div>
			</a>
		@endif


	<!-- Main content -->
		<section class="content">
		@if (Session::has('message-success'))
		@include('elements.message-success')
		@endif
			@include('elements.breadcrump-landlord')
				<!-- Small boxes (Stat box) -->
				<div class="row">
					<br>
					<div class="col-lg-12 col-sm-12">
		          	<!-- Welcome box -->
						<div class="tnt-box">
			            	<div class="box-heads">
			            		<h4>Welcome, {{$profile->firstname}}!</h4>
			            	</div>
						</div>
					<!-- <div class="payment-links"><i class="fa fa-dollar"></i><a href="{{ route('payment.getTenantPayment') }}">Payment</a></div> -->
		            </div>
					<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
				  		<!-- small box -->
				  		<!--div class="tnt-box">
						    <div class="box-heads">
						        <h4>Plan: {{ $subscriptionPlan->name }}</h4>
						    </div>
					  	</div-->
				<!-- <div class="payment-links"><i class="fa fa-dollar"></i><a href="{{ route('payment.getTenantPayment') }}">Payment</a></div> -->
				    </div>
		        </div>

				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					  <!-- View properties -->
						
                                                <div class="tnt-box post-prts">
                                                <div class="box-heads">
                                                        <h4>Portfolio</h4>
                                                </div>
                                                <div class="post-property-links">
                                                                <a href="{{ route('landlords.getProperty') }}">View Here</a>
                                                        </div>
                                                </div>

					</div><!-- ./col -->
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					<!-- Post property -->
						<div class="tnt-box post-prts">
					    	<div class="box-heads">
					      		<h4>Post Property</h4>
					    	</div>
					    	<div class="post-property-links">
								<a href="{{ route('property.add') }}">Post New</a>
							</div>
				  		</div>
					</div><!-- ./col -->
					@if ($profile->plan_type != 1)

					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
					  <!-- Groups -->
						<div class="tnt-box post-prts">
						    <div class="box-heads">
						    	<h4>Bank Accounts</h4>
						    </div>
						    <div class="post-property-links">
								<a href="{{ route('landlords.createlandlordapplication') }}">Register New</a>
							</div>
						</div>
					</div><!-- ./col -->
					@endif
				</div>
				<!-- /.row -->
				<div class="row">
				<!-- Left col -->

				<!-- right col (We are only adding the ID to make the widgets sortable)-->


				<!-- <section class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

				  <div class="my-property tnt-box">
				     <div class="box-heads">
				        <h4>General Enquiry</h4>
				      </div>
				      <div class="responses row">
				        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				          <ul>
				            @forelse($generalMessages as $key => $generalMessage)
				            <li class="row">
				              <div class="texts"><strong>{{ $generalMessage->thread_name }}:</strong> {{ substr($generalMessage->message,0,20) }}</div>
				              <div class="view">
				                <a href="{{route('landlords.getMessageList',$generalMessage->message_thread_id)}}">Details</a>
				              </div>
				            </li>
				            @empty
				              <p>There are no messages yet!</p>
				            @endforelse
				          </ul>
				        </div>
				      </div>
				  </div>
				  <div class="my-property tnt-box">
				     <div class="box-heads">
				        <h4>Issue Reported</h4>
				      </div>
				      <div class="responses row">
				        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				          <ul>
				            @forelse($issueMessages as $key => $issueMessage)
				            <li class="row">
				              <div class="texts"><strong>{{ $issueMessage->thread_name }}:</strong> {{ substr($issueMessage->message,0,20) }}</div>
				              <div class="view">
				                <a href="{{route('landlords.getMessageList',$issueMessage->message_thread_id)}}">Details</a>
				              </div>
				            </li>
				            @empty
				              <p>There are no messages yet!</p>
				            @endforelse
				          </ul>
				        </div>
				      </div>
				  </div>



				</section>
				<section class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				  <div class="tnt-box my-searches">
				    <div class="box-heads">
				      <h4>Reviews</h4>
				      @if(count( $propertiesReview ))
				      <a href="{{ route('properties.review') }}" class="light-blue">View More »</a>
				      @endif
				    </div>
				    <div class="new-rating">
				      <ul>
				       @forelse( $propertiesReview as $key => $property )
				        <li class="row">
				            <div class="col-lg-5"><a href="{{ route('properties.review',$property->slug) }}">{{ $property->property_name }}</a></div>
				            <div class="col-lg-2">reviews({{ HTML::getReviewCount($property->id) }})</div>
				            <div class="col-lg-5"><img src="{{ HTML::getAverageReviewRatingForLandlord($property->id) }}"></div>
				        </li>
				       @empty
				          <p>There are no reviews yet!</p>
				       @endforelse
				      </ul>
				    </div>
				  </div>
				</section>
			</div> -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

@endsection
