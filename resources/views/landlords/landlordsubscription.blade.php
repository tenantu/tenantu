@extends('layouts.landlord-inner')
@section('inner-content')
    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    @include('elements.validation-message')
    @if (session('success'))
        <div class="flash-message">
            <div class="alert alert-success">

            </div>
        </div>
    @endif


    <div class="content-wrapper">
        <section class="content">
        @include('elements.breadcrump-landlord')
        <div class="row">
            <div class="col-lg-4 col-md-10 col-sm-12 col-xs-12">
                <div class="tnt-box landlord-profile">
                    <div class="box-heads">
                        <h4>Current Subscription Plan</h4>
                    </div>
                    <div class="landlord-name">{{ $subscriptionPlan->name }}</div>
                    <div class="landlord-name">${{ $subscriptionPlan->initial_base_price }} per Management Account</div>
                    <div class="landlord-name">${{ $subscriptionPlan->additional_accounts_base_price }} per Extra Management Account</div>
                    <div class="landlord-name">${{ $subscriptionPlan->price_per_property }} per Property</div>
                    <div class="landlord-name">{{ $subscriptionPlan->num_management_accounts_pre_new_base_price }} Management Account(s) Included</div>
                    <div class="landlord-name">Max Management Accounts: {{ $subscriptionPlan->management_account_max }}</div>
                    <div class="landlord-name">Max Properties: {{ $subscriptionPlan->property_max }} </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-10 col-sm-12 col-xs-12">
                <div class="tnt-box landlord-profile">
                    <div class="box-heads">
                        <h4>My Resources</h4>
                    </div>

                    <div class="landlord-name">{{ $landlordSubscription->management_group_count }} Total Management Accounts</div>
                    <div class="landlord-name">{{ $landlordSubscription->property_count }} Properties</div>

                    <!--div class="box-heads">
                        <h4>My Monthly Investment</h4>
                    </div>
                    <div class="landlord-name">Properties: ${{ $subscriptionPlan->price_per_property * $landlordSubscription->property_count }}</div>
                    <div class="landlord-name">Management Accounts: ${{ (($landlordSubscription->management_group_count) > ($subscriptionPlan->num_management_accounts_pre_new_base_price)) ? ( ($subscriptionPlan->initial_base_price * $subscriptionPlan->num_management_accounts_pre_new_base_price) + (($subscriptionPlan->additional_accounts_base_price * ($landlordSubscription->management_group_count - $subscriptionPlan->num_management_accounts_pre_new_base_price))) ) : $subscriptionPlan->initial_base_price * $landlordSubscription->management_group_count }}</div>

                    @if ($landlordSubscription->active_status == 0)
                    <div class="landlord-name">Subscription Not Activated Yet</div>
                    @else
                    <div class="landlord-name">{{ $landlordSubscription->monthly_payment_amount }}</div>
                    @endif-->
                </div>
            </div>
            <div class="hide col-lg-4 col-md-10 col-sm-12 col-xs-12">
                <div class="tnt-box landlord-profile">
                    <div class="box-heads">
                        <h4>TenantU Credit Multiplier</h4>
                    </div>
                    <div class="landlord-name">My Credit Multiplier: {{ $subscriptionPlan->landlord_credit_multiplier }}x</div>
                    <div class="landlord-name">Tenant Credit Multiplier: {{ $subscriptionPlan->tenant_credit_multiplier }}x</div>
                </div>
            </div>
            </div>
            <div class="row">
            <div class="col-lg-5 col-md-10 col-sm-12 col-xs-12">
                <div class="tnt-box landlord-profile">
                    <div class="box-heads">
                        <h4>Add/Upgrade</h4>
                    </div>
                    <div class="landlord-name"><button onclick="location.href='{{route('landlords.subscription')}}';" class="btn btn-default">Upgrade Subscription</button></div>
                    <div class="landlord-name"><button onclick="location.href='{{route('landlords.createlandlordapplication')}}';" class="btn btn-default">Register Another Management Account</button></div>
                </div>
            </div>
        </div>

    </section>
    </div>
@endsection
