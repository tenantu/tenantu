@extends('layouts.landlord-inner')
@section('inner-content')
    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    @include('elements.validation-message')
    @if (session('success'))
        <div class="flash-message">
            <div class="alert alert-success">

            </div>
        </div>
    @endif

    <div class="content-wrapper">
        <section class="content">
            <table class="table table-bordered table-striped table-hover article-list">
                <thead>
                <tr>

                    <th class="col-md-2">
                        <a>Date Submitted</a>
                    </th>
                    <th class="col-md-2">
                        <a>Issue Type</a>
                    </th>
                    <th class="col-md-2">
                        <a>Issue Description</a>
                    </th>

                    <th class="col-md-2">
                        <a>Status</a>
                    </th>

                </tr>
                </thead>
                <tbody>
                @foreach ($maintenanceRequests as $maintenanceRequest)
                    <tr data-link='{{ action('MaintenanceRequestController@show', $maintenanceRequest ) }}'>

                        <th scope="row">{{ $maintenanceRequest->date_encountered }}</th>
                        <td>{{$maintenanceRequest->severity}}</td>
                        <td>{{$maintenanceRequest->issue_desc}}</td>
                        <td>{{$maintenanceRequest->status}}</td>

                    </tr>

                @endforeach
                </tbody>
            </table>
            <div class="col-md-8 text-right">
                {!! str_replace('/?', '?', $maintenanceRequests->render()) !!}
            </div>
    </div>
    </section>
    </div>
@endsection
