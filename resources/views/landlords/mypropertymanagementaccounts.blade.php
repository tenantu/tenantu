@extends('layouts.landlord-inner')
@section('inner-content')
@include('elements.validation-message')

<script type="text/javascript">
    //This function sets only the currently selected tab as "active", changes value of all checkboxes for
    //adding properties to mgmt account to the name of the currently selected account
    //takes an HTML element as a param
  function setActive(account){
    $(".tab-pane").removeClass("active");
    var tabId = account.getAttribute('id');
    var splitId = tabId.split("%");
    var tabContentId = splitId[0];
    //alert(tabContentId);
    $('#' + tabContentId).addClass('active');
    $(".propertyMgmtAccountId").attr("value", splitId[2]);
    $("#mgmtAccountIdField").val(splitId[2]);
    console.log(splitId[2]);
  }

</script>
<div class="content-wrapper">
    <section class="content">
        @if (Session::has('message-success'))
        @include('elements.message-success')
        @endif
        @if (Session::has('message-error'))
            <div class="alert alert-danger">
              <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
              <strong>Error!</strong> {{ Session::get('message-error') }}
            </div>
        @endif
        <div role="tabpanel">
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <!-- Nav tabs -->
            @foreach ($propertyManagementAccounts as $key => $account)
                <li role="presentation" class="">
                    <a href="#{{ $account->account_name }}" aria-controls="{{ $account->account_name }}" role="tab" data-toggle="tab"
                  onclick="setActive(this)" id="{{ $key }}%{{ $account->account_name }}%{{ $account->id }}">
                    {{ $account->account_name }}
                    </a>
                </li>
            @endforeach

            <li>
              <a href="" aria-controls="newAccount" role="tab" data-toggle="modal" data-target="#newAccountModal">
                New Bank Account
                <i class="fa fa-plus-circle" aria-hidden="true"></i>
              </a>
            </li>
        </ul>
<div class="panel panel-default">
    <div class="panel-body">
        <!-- Tab panes -->
        <div class="tab-content">

        @foreach ($propertyManagementAccounts as $key => $account)
            <div role="tabpanel" class="tab-pane" id="{{ $key }}">
            @if (((array_key_exists($account->id, $propertiesWithMgmtAccount)) && !(empty($propertiesWithMgmtAccount[$account->id]))) || (array_key_exists($account->id, $apartmentsWithMgmtAccount) && !(empty($apartmentsWithMgmtAccount))) )


{{--
            <div class="jumbotron text-left">
            <h1 class="display-3"> {{ $account->account_name }} at a glance: </h1>
            <hr class="my-4">
            <table class="table table-bordered">
                <tr>
                    <th scope="row">Property Count: </th>
                    <td> {{ $account->property_count }} </td>
                </tr>
                <tr>
                    <th scope="row"> </th>
                    <td>  </td>
                </tr>
            </table>
            <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
                <button class="btn btn-primary btn-lg center-block"> Click to add properties to {{ $account->account_name }} </button>
            </a>
            </div>

 --}}
            <!-- Another idea that seemed cool at the time. revisit tomorrow.

            <div class="row bs-downloads">
                <div class="col-sm-4">
                    <div>
                        <h3>Add more properties to <br> {{ $account->account_name }} </h3>
                          <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
                              <button class="btn btn-lg btn-primary">
                              Add properties to {{ $account->account_name }}
                              </button>
                          </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div>
                        <h3>List more properties on <br> TenantU </h3>
                          <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
                              <button class="btn btn-lg btn-primary">
                              List more properties on TenantU
                              </button>
                          </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div>
                        <h3> See which of your properties don't belong to a management account yet </h3>
                          <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
                              <button class="btn btn-lg btn-primary">
                              View properties with no management account
                              </button>
                          </a>
                    </div>
                </div>
            </div> -->

                    <div class="btn-toolbar">
                    <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
                      <button class="btn btn-primary btn-md pull-right" style="margin-left: 8px;"> Click to add properties to {{ $account->account_name }} </button>
                    </a>

                        <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addApartmentModal">
                          <button class="btn btn-primary btn-md pull-right"> Click to add apartment complexes to {{ $account->account_name }} </button>
                        </a>


                    </div>
            <table class="table table-striped table-bordered">
                <thead class="thead-inverse">
                    <th>#</th>
                    <th>Property Name</th>
                    <th>Location</th>
                    <th>Remove from account </th>
                    <th> Edit property </th>
                </thead>
                <tbody>
                        {!! Form::open(array('route' => 'landlords.removepropertyfrommgmtaccount')) !!}
                        <?php $count = 1; ?>

                        @foreach($apartmentsWithMgmtAccount as $key => $apartmentArray)
                          @if($key == $account->id)
                            @foreach($apartmentArray as $apartment)
                            <tr>
                              <th scope="row" class="col-md-1">{{$count}}</th>
                              <td class="col-md-6"> {{$apartment->building_name}}</td>
                              <td class="col-md-6">{{$apartment->property_physical->first()->thoroughfare}}</td>
                              <td><input type="checkbox" name="apartment[]" id="{{$apartment->building_name}}" value="{{$apartment->id}}" /></td>
                              <td><a href="{{route('landlords.getEditApartmentUnits', $apartment->id)}}" class="btn btn-primary">
                                Edit<i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                              </td>
                              <?php $count++; ?>
                            </tr>
                          @endforeach
                          @endif
                        @endforeach

                        @foreach($propertiesWithMgmtAccount as $propertiesInAccount)
                            @foreach($propertiesInAccount as $property)
                            @if ($property->landlord_prop_mgmt_id == $account->id)
                            <tr>
                                <th scope="row" class="col-md-1"> {{ $count }} </th>
                                <td class="col-md-6"> {{ $property->property_name }} </td>
                                <td class="col-md-6">  {{ $property->thoroughfare .", ".$property->locality.", ".$property->administrative_area }}, United States </td>
                                <td> <input type="checkbox" name="property[]"
                                    id="{{$property->property_name}}" value="{{$property->id}}"> </td>

                                <td> <button class="btn btn-primary" name="editProp" id="edit{{$property->id}}" value="{{$property->id}}">
                                    Edit <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button> </td>
                                <?php $count++; ?>
                            </tr>
                            @endif
                            @endforeach
                        @endforeach

                </tbody>
            </table>

            {!! Form::submit('Remove selected properties from current bank account', ['class' => 'btn btn-danger save-btn submit-btn pull-right']) !!}
            {!! Form::close() !!}
          @else
            <div class="jumbotron text-center">
                <p> You don't have any properties under this bank account yet! Click the button below to add some. </p>
                <div class="btn-toolbar text-center">
                    <div class="emptyAccountButton">
                        <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
                          <button class="btn btn-primary btn-lg center-block emptyAccountButton"> Click to add properties to {{ $account->account_name }} </button>
                        </a>
                    </div>

                    <div class="emptyAccountButton">
                        <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addApartmentModal">
                          <button class="btn btn-primary btn-lg center-block emptyAccountButton"> Click to add apartment complexes to {{ $account->account_name }} </button>
                        </a>
                    </div>

                    <div class="emptyAccountButton">
                        <a href="{{ route('property.add') }}">
                            <button class="btn btn-primary btn-lg center-block emptyAccountButton"> Click to add properties to TenantU </button>
                       </a>
                   </div>
                </div>
            </div>
          @endif
          </div>
        @endforeach

        </div>
    </div>

</div>
</div>
</div>


</section>
</div>
</div>




<!-- Modal -->
<div id="newAccountModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Add Account Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please fill out the following fields:</h4>
    </div>
    <div class="modal-body">
        {!! Form::open(array('route' => 'landlords.postmypropertymanagementgroups')) !!}
        <table class="table table-bordered">
        <tr>
            <th scope="row" class="col-lg-4"> {!! Form::label('locationId', 'Location Id') !!} <span style="color: red;">*</span></th>
            <td class="col-lg-8"> {!! Form::text('locationId') !!} </td>
        </tr>
        <tr>
            <th scope="row"> {!! Form::label('accountName', 'Account Name') !!} </th>
            <td> {!! Form::text('accountName') !!} </td>
        </tr>
        </table>


    </div>
    <div class="modal-footer">
<div class="row">
<div class="col-lg-8">
                <span style="color: red;">*</span>Once you have filled out the <i>Bank Account Application</i>, you will be emailed a <b>Location ID</b>.
                Please enter that value above.

            </div>
    <div class="col-lg-4">
        {!! Form::submit('Submit', ['class' => 'btn btn-primary save-btn submit-btn']) !!}
        {!! Form::close() !!}
                <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    </div>

            </div>


    </div>

</div>
</div>

</div>
</div>


<div id="addApartmentModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Add Property Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        @if(!(empty($propertiesWithNoMgmtAccount)))
        <h4 class="modal-title">Select the properties you would like to add to this account:</h4>
    </div>
    <div class="modal-body">


        {!! Form::open(array('route' => 'landlords.postapartmenttomgmtaccount')) !!}
        <input type="hidden" class="propertyMgmtAccountId" name="mgmtAccountId" value="">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th> Apartment Building Name </th>
                    <th> Add to this bank account </th>
                </tr>
            </thead>
            <tbody>

                    @foreach($apartmentBuildingsWithNoMgmtAccount as $apartment)
                        <tr>
                            <td> {{ $apartment->building_name }} </td>
                            <td> <input type="checkbox" name="apartmentBuildings[]"
                                    id="{{$apartment->building_name}}" value="{{$apartment->id}}"> </td>
                        </tr>
                    @endforeach

            </tbody>
        </table>

    </div>
    <div class="modal-footer">
        {!! Form::submit('Add apartments to this bank account', ['class' => 'btn btn-primary save-btn submit-btn']) !!}
        {!! Form::close() !!}
        @else
        <h4 class="modal-title">All of the properties you have listed on TenantU belong to a bank account already.</h4>
                    <a href="{{ route('property.add') }}">
                        <button type="button" class="btn btn-primary">List more properties on TenantU</button>
                    </a>
       @endif
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>

</div>
</div>


<div id="addPropertyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Add Property Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        @if(!(empty($propertiesWithNoMgmtAccount)))
        <h4 class="modal-title">Select the properties you would like to add to this account:</h4>
    </div>
    <div class="modal-body">


        {!! Form::open(array('route' => 'landlords.postpropertytomgmtaccount')) !!}
        <input type="hidden" class="propertyMgmtAccountId" name="mgmtAccountId" value="">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th> Property Name </th>
                    <th> Add to this bank account </th>
                </tr>
            </thead>
            <tbody>

                    @foreach($propertiesWithNoMgmtAccount as $property)
                        <tr>
                            <td> {{ $property->property_name }} </td>
                            <td> <input type="checkbox" name="property[]"
                                    id="{{$property->property_name}}" value="{{$property->id}}"> </td>
                        </tr>
                    @endforeach

            </tbody>
        </table>

    </div>
    <div class="modal-footer">
        {!! Form::submit('Add properties to this bank account', ['class' => 'btn btn-primary save-btn submit-btn']) !!}
        {!! Form::close() !!}
        @else
        <h4 class="modal-title">All of the properties you have listed on TenantU belong to a bank account already.</h4>
                    <a href="{{ route('property.add') }}">
                        <button type="button" class="btn btn-primary">List more properties on TenantU</button>
                    </a>
       @endif
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>

</div>
</div>


<div id="emptyAccountModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Empty Management Account Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">This bank account has no properties!</h4>
    </div>
    <div class="modal-body">
        No properties belong to this bank account!
    </div>
    <div class="modal-footer">
        <a href="" aria-controls="addProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
            <button> Click to add properties </button>
        </a>
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    </div>
</div>

</div>
</div>


<div id="createPropertyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- create property Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Begin managing a new property with TenantU</h4>
    </div>
    <div class="modal-body">
        {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm','route'=>['property.postadd'],'files'=>true]) !!}
                {!! Form::hidden('img_name', '', ['class'=>'form-control login-input','id'=>'imgName']) !!}
                {!! Form::hidden('hdPlace', '', ['id'=>'hdPlace']) !!}
                {!! Form::hidden('isFromPropMgmtAccountPage', 'true', ['id'=>'isFromPropMgmtAccountPage']) !!}
                <input type="hidden" name="mgmtAccountId" id="mgmtAccountIdField" value="">

                <div class="row">
                <div class="form-group col-lg-10 col-md-10 col-sm-12 col-cs-12">
                    {!! Form::label('propertyType', 'Property Type:') !!}<span style="color: red;">*</span>
                    <div class="radio-container">
                      <div class="radio">
                        <label>
                          {!! Form::radio('property_type', 0, ['id'=>'aptRadio']) !!}
                          <span class="radio-text">Apartment</span>
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          {!! Form::radio('property_type', 1, ['id'=>'houseRadio']) !!}
                          <span class="radio-text">House</span>
                        </label>
                      </div>
                    </div>
                    </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {!! Form::label('propertytitle', 'Property title:') !!}<span style="color: red;">*</span>
                    {!! Form::text('property_name', Input::old('property_name'), ['class'=>'form-control login-input','id'=>'propertytitle','placeholder'=>'Property Title','required']) !!}
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('schoolname', 'School name:') !!}<span style="color: red;">*</span>
                    <div class="select-box">
                      {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'schoolname','required']) !!}
                    </div>
                </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('location', 'Location of property:') !!}<span style="color: red;">*</span>
                      {!! Form::text('location', Input::old('location'), ['class'=>'form-control login-input','id'=>'location','placeholder'=>'Location','required']) !!}
                    </div>
                    {!! Form::hidden('schoolCalc', '', array('id' => 'schoolCalc')) !!}
                     <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('bedroom', 'Bedrooms:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','bedroom_no', Input::old('bedroom_no'), ['class'=>'form-control login-input','id'=>'bedroomNo','placeholder'=>'Bedrooms','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('bathroom', 'Bathrooms:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','  bathroom_no', Input::old('bathroom_no'), ['class'=>'form-control login-input','id'=>' bathroomNo','placeholder'=>'Bathrooms','required']) !!}
                    </div>
                     <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('rentprice', 'Rent:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','rent_price', Input::old('rent_price'), ['class'=>'form-control login-input','id'=>'rentPrice','placeholder'=>'Rent','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('distance_from_school', 'Distance from school(Miles):') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','distance_from_school', Input::old('distance_from_school'), ['class'=>'form-control login-input','step'=>'.01','id'=>'distanceFromSchool','placeholder'=>'Distance from school','required']) !!}
                    </div>

                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('amenities', 'Amenities:') !!} (Select items from the options)<span style="color: red;"></span>
                      <p></p>
                        {!! Form::select('amenity[]',$amenities,'', ['class'=>'form-control login-input','id'=>'amenity','multiple']) !!}
                    </div>


                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('description', 'Property description:') !!}<span style="color: red;">*</span>
                      {!! Form::textarea('description', Input::old('description'), ['class'=>'form-control login-input','id'=>'description','placeholder'=>'Description','rows'=>'3','required']) !!}
                    </div>


                  </div>
                    {!! Form::label('communicationMedium', 'Communication Medium:') !!}<span style="color: red;">*</span>
                    <div class="radio-container">
                  <div class="radio">
                    <label>
                      {!! Form::radio('communication_medium', 'W', ['id'=>'optionsRadios1']) !!}
                      <span class="radio-text">Web</span>
                    </label>
                  </div>

                  <div class="radio">
                      <label>
                        {!! Form::radio('communication_medium', 'E', ['id'=>'optionsRadios2','required']) !!}
                        <span class="radio-text">Email</span>
                      </label>
                  </div>
                </div>
                  <!-- <label class="checkbox-text"> -->
                  <!-- {!! Form::radio('communication_medium', 'W') !!} -->
                  <!-- <span class="lbl padding-8">Web</span> -->
                  <!-- </label> -->
                  <!-- <label class="checkbox-text">
                  {!! Form::radio('communication_medium', 'E') !!}
                   <span class="lbl padding-8">Email</span>
                  </label> -->
                  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">


                  </div>

                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary add-button start']) !!}
                    </div>
                  </div>


                {!! Form::close() !!}

              </div>
            </div><!-- ./col -->
    </div>
    <!-- <div class="modal-footer">
        <a href="" aria-controls="createProperty" role="button" data-toggle="modal" data-target="#addPropertyModal">
            <button> Click to add properties </button>
        </a>
        <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
    </div> -->
</div>

</div>
</div>
@endsection

@section('page-scripts')
      <!-- -------google location css-js files------------------- -->
 <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
 <link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css">
 <link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css">
 <link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">
 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
  <script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
 <!-- -------google location css-js files------------------- -->


  <script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>

 <script type="text/javascript">
 if( window.location.hostname == 'localhost'){
  var baseUrl = 'http://localhost/tenantudev/';
 }else if( window.location.hostname == 'fodof.net') {
  var baseUrl = 'http://fodof.net/tenantu/';
 }
  var jqxhr='';
 //var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
 $("#location").tagit({
        allowSpaces: true,
        fieldName: "location[]",
        tagLimit: 1,
        autocomplete: {
          delay: 0,
          minLength: 2,
          source: function(request, response) {
            var callback = function (predictions, status) {
              if (status != google.maps.places.PlacesServiceStatus.OK) {
                return;
              }
              var data = $.map(predictions, function(item) {
                return item.description;
              });
              response(data);
            }
            var service = new google.maps.places.AutocompleteService();
            service.getQueryPredictions({ input: request.term }, callback);
          }
        },
        beforeTagAdded: function (event, ui) {
                        getGeoLocations(ui.tagLabel,false);
                    },
                    beforeTagRemoved: function (event, ui) {
                        // do something special
                        console.log(ui.tagLabel);
                        getGeoLocations(ui.tagLabel,true);
                    }
      });
      $("#courseLocation").tagit({
        fieldName: "location[]"
      });

      var jqxhr='';
      var skills= [];

     $('select[multiple]').multiselect({
            columns: 1,
            placeholder: 'Select options'
        });


        function getGeoLocations(address, remove) {

                    if (address === '') {
                        return false;
                    }

                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var location = results[0].geometry.location.lat() + '~' + results[0].geometry.location.lng()
                            var storedLocation = $("#hdPlace").val();
                            if (remove !== true) {
                                if (storedLocation === '') {
                                    $("#hdPlace").val(location);
                                } else {
                                    $("#hdPlace").val(storedLocation + ',' + location);
                                }
                            }else{

                                storedLocation = storedLocation.replace(location,'');
                                storedLocation = storedLocation.replace(',,',',');
                                $("#hdPlace").val(storedLocation)
                            }

                        }

                    });
                    return false;
                }
                 $('#courseLocation .ui-autocomplete-input').attr('placeholder','Cities, States, Countries, or Zip/Postal Codes');

                 $('#location').on('input', function(){
                    $('#ui-id-1').children('*').zIndex(1000);
                  });
                 </script>
    <style>
    .ui-menu{
        z-index: 69123 !important;
    }

    .emptyAccountButton {
        display: inline-block;
        margin: 0 auto;
    }

    </style>

      @endsection
