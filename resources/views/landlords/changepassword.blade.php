 @extends('layouts.landlord-inner')
  @section('inner-content')
  <div class="content-wrapper">
  <!-- Content Header (Page header) -->

	  @include('elements.validation-message')

	  @if (Session::has('message-success'))
	  @include('elements.message-success')
	  @endif
	{!! Form::model('',['method' => 'POST','route'=>['landlords.postChangepassword']]) !!}
	 <section class="content tenant-contacts">
     @include('elements.breadcrump-landlord')
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-5 col-md-4 col-sm-8 col-xs-12">
            <div class="tnt-box">
              <div class="change-password">
                <div class="box-heads">
                <h4>Change Password</h4>
              </div>
                <form class="change-password-form">
                  <div class="form-group">
                    <label for="oldPasword">Current Password</label>

                    {!! Form::password('current_password', ['class' => 'form-control','id'=>'oldPasword', 'placeholder' => 'Current Password']) !!}

                  </div>
                  <div class="form-group">
                    <label for="newPassword">Password</label>
                    {!! Form::password('password', ['class' => 'form-control','id'=>'newPassword', 'placeholder' => 'Password']) !!}

                  </div>
                  <div class="form-group">
                    <label for="confirmPassword">Confirm Password</label>
                    {!! Form::password('password_confirmation', ['class' => 'form-control','id'=>'confirmPassword', 'placeholder' => 'Password Confirmation']) !!}

                  </div>
                  {!! Form::submit('Submit', ['class' => 'btn btn-default save-btn']) !!}
                </form>
              </div>
            </div>
          </div>
      </div><!-- /.row -->

      </section>
	</div>

 {!! Form::close() !!}
 @endsection
