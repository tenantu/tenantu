@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

    <!-- Main content -->
    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif

		@include('elements.validation-message')

    <section class="content landlord-message-cntr">
      @include('elements.breadcrump-landlord')
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="contacts">

                <div class="user-image"><img src="{{HTML::getTenantProfileImage($tenantId)}}"></div>
                <div class="user-name">{{$tenantName}}</div>
              </div> <!-- contact Heading -->
            </div>
          </div>
          <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 contacts-message">

                <!-- ================================== -->
                <!-- message contacts for desktop starts-->
                <!-- ================================== -->
                <div class="cnts">
                  <div class="create-new-msg">
                      <a href="{{route('landlords.newmessage',$tenantId)}}"><i class="fa fa-plus-square-o"></i> New Message</a>
                  </div>
                  <ul class="contact-list">

					  @forelse($messageThreadsObj as $Thread)
					  <li>
						  <a href="{{route('landlords.getMessageList',['messageThreadId'=>$Thread->id])}}" class="user {{HTML::getThreadMenuClass($messageThreadId,$Thread->id)}}">

							<div class="user-image icons"><i class="{{ $Thread->image }}"></i></div>
							<div class="user-details">
							  <h4>{{$Thread->thread_name}}</h4>
							  <div class="msg-date">{{$Thread->property_name}}</div>
							   <?php
							   if(isset($Thread->created_at))
							   {
								$threadDate = date('d M h:i:s a',strtotime($Thread->created_at));

								?>
							  <div class="msg-date">{{$threadDate}}</div>
							  <?php } ?>
							</div>
						  </a>
                      </li>
                      @empty
						<p>No users</p>
					  @endforelse

                  </ul>
                  </div>
                <!-- message contacts for desktop end-->
                <!-- ================================== -->


                </div>
              @if($messageFlag=='compose')
		         @include('landlords.message-compose')
	          @else
		         @include('landlords.messagelist')
	          @endif
              </div><!-- /.row -->

          <div>

        </section><!-- /.content -->
            </div>
        @endsection
