@extends('layouts.landlord-inner')
@section('inner-content')
@if (Session::has('message-success'))
    @include('elements.message-success')
@endif
@include('elements.validation-message')
@if (session('success'))
  <div class="flash-message">
  <div class="alert alert-success">

  </div>
  </div>
@endif
<!--
<div class="container">
  @foreach ($rent_payments as $rent_payment)
    {{ $rent_payment->id}}
  @endforeach
</div>
-->
<div class="content-wrapper">
  <section class="content">
    @include('elements.breadcrump-landlord')
    <div class="landlord-add-propety">
    <a href="{{route('landlords.exportRentPaymentHistory') }}"class="btn btn-success fileinput-button">Export</a>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>

                <th class="col-md-2">
                   <a>Month Rent</a>
                </th>
                <th class="col-md-2">
                   <a>Property</a>
                </th>
                <th class="col-md-2">
                   <a>Date Submitted</a>
                </th>
                 <th class="col-md-2">
                    <a>Amount</a>
                 </th>

                 <th class="col-md-2">
                    <a>Date Seen</a>
                 </th>
                 <th class="col-md-2">
                    <a>Status</a>
                 </th>

                 <th class="col-md-2">
                    <a>Date Rent Received</a>
                 </th>


            </tr>
        </thead>
        <tbody>
        @foreach ($rent_payments as $rent_payment)

            <tr>
                <th scope="row">{{ date('M', strtotime($rent_payment->date_due)) }}</th>
                <td>{{$rent_payment->property_name}}</td>
                <td>{{$rent_payment->date_posted}}</td>
                <td>{{$rent_payment->authorized_amount}}</td>
                <td>{{$rent_payment->date_received}}</td>
                <td> {{ucwords($rent_payment->status)}} </td>
                <td>{{$rent_payment->date_funded}}</td>


            </tr>

          @endforeach
        </tbody>
      </table>
      <div class="col-md-8 text-right">
          {!! str_replace('/?', '?', $rent_payments->render()) !!}
      </div>
    </div>
  </section>
</div>
@endsection
