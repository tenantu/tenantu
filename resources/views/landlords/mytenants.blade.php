@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          @include('elements.breadcrump-landlord')
          <!-- Small boxes (Stat box) -->
          <div class="row my-tenants">
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                <th class="col-md-2">
                   <a>Name</a>
                </th>
                <th class="col-md-2">
                   <a>Property</a>
                </th>
                <th class="col-md-2">
                   <a>Agreement From</a>
                </th>
                <th class="col-md-2">
                   <a>Agreement To</a>
                </th>
                <th class="col-md-2">
                   <a>Payment Info</a>
                </th>
                <th class="col-md-2">
                   <a>Conversation</a>
                </th>
                <th class="col-md-2">
                   <a>Profile</a>
                </th>

            </tr>
        </thead>
        <tbody>
        @foreach ($tenants as $tenant)
            <tr>
                <th scope="row"><a style="color:black;"href="{{ route('landlords.viewtenant',$tenant->id ) }}">{{ $tenant->tenant->firstname.' '.$tenant->tenant->lastname }}</a></th>
				<td>{{ $tenant->property_physical->property_name }}</td>
				<td>{{ date('m-d-Y',strtotime($tenant->agreement_start_date)) }}</td>
				<td>{{ date("m-d-Y", strtotime("+".$tenant->cycle, strtotime($tenant->agreement_start_date))) }}</td>
				<td><a href="{{ route('payment.getTenantPayment',[$currMonthYear,Crypt::encrypt($tenant->tenant->id)]) }}" class="message-buttons">Payment Info</a></td>
				<td><a href="{{route('landlords.newmessage',Crypt::encrypt($tenant->tenant->id))}}" class="message-buttons">Message Tenant</a></td>
				<td><a href="{{route('landlords.getTenantProfileFromLandlord',$tenant->tenant->slug)}}" class="message-buttons">View Profile</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>
	                  <div class="col-xs-12">
                    <div class="no-tenants text-center add-tenant">
                      <div class="add-tenant-cntr"><a class="add-tenant-btn cleafix" href="{{ route('landlords.addtenant') }}">Add Tenant <i class="fa fa-plus"></i></a></div>
                    </div>
                  </div>
          </div><!-- /.row -->


        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
