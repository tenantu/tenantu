@extends('layouts.landlord-inner')
    @section('inner-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('elements.validation-message')
        @if (session('success'))
            <div class="flash-message">
                <div class="alert alert-success">

                </div>
            </div>
        @endif
        @if(Session::has('displayApplication'))
        @include('elements.validation-message')
        @endif
        @if (Session::has('message-success') || Session::has('successApplication'))
            @include('elements.message-success')
        @endif
        @if (Session::has('message-error') || Session::has('failureApplication'))
            @include('elements.message-error')
        @endif

        <!-- Main content -->
        <section class="content">
            @include('elements.breadcrump-landlord')
            <div class="row">
                <div class="col-lg-6">
                    <h2>Application for Underwriting</h2>
                    <p>Each bank account used to collect rent from your properties must be submitted for underwriting in order to securely use the Rent Collection System. Your information will NOT be stored on this site. All information will be securely sent to Forte Payment Systems for processing.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-sm-12 edit-property-details content-left">
                    <h3>Business Information</h3>
		    @if ($applicationCount == 0)
                     {!! Form::model('',['method' => 'GET','class'=>'','id'=>'tenantAddForm','route'=>['landlords.postlandlordapplication']]) !!}
                    @endif
		    @if ($applicationCount > 0)
	             {!! Form::model('',['method' => 'GET','class'=>'','id'=>'tenantAddForm','route'=>['landlords.postlandlordapplicationpay']]) !!}
		    @endif
		    <div class="row">
                        <div class="form-group col-lg-6 col-sm-6 col-xs-12">
                             {!! Form::label('dba_name', "Business Name:") !!}<span style="color: red;">*</span>
                             {!! Form::text('dba_name',"", ['class'=>'form-control','placeholder'=>'DBA Name (Doing Business As)']) !!}
                        </div>
                        <div class="form-group col-lg-6 col-sm-6 col-xs-12">
                             {!! Form::label('legal_name', 'Legal Name:') !!}<span style="color: red;">*</span>
                             {!! Form::text('legal_name',"", ['class'=>'form-control','placeholder'=>'ABC Company, LLC']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-sm-6 col-xs-6">
                             {!! Form::label('legal_structure', 'Business Ownership Type:') !!}<span style="color: red;">*</span>
                             {!! Form::select('legal_structure', ['null' => 'Please select...', 'sole_proprietorship' => 'Sole Propriertoship (DBA Needed)', 'partnership_general_or_limited' => 'Partnership(Limited or General)', 'limited_liability_corporation' => 'LLC - Limited Liability Corp','tax_exempt_or_non_profit_organization' => 'Non-Profit or Tax-Exempt Org.', 'c_corporation' => 'C-Corporation', 's_corporation' => 'S-Corporation','publicly_held_corporation' => 'Public Corporation', 'government' => 'Governmental Agency', 'other' => "Don't Know/Other"], "", ['class'=>'form-control','placeholder'=>'Business Ownership Type']) !!}
                        </div>
                        <div class="form-group col-lg-6 col-sm-6 col-xs-6">
                             {!! Form::label('tax_id_number', 'Tax ID Number for your Business:') !!}<span style="color: red;">*</span>
                             {!! Form::text('tax_id_number',"", ['class'=>'form-control','placeholder'=>'TIN, EIN, SSN, etc']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12">
                             {!! Form::label('streetaddress1', 'Street Address (Physical Address of your Business - No P.O. Boxes please):') !!}<span style="color: red;">*</span>
                             {!! Form::text('streetaddress1', isset($paymentSettings->bank_name) ? $paymentSettings->bank_name:"", ['class'=>'form-control','placeholder'=>'Street Address']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4 col-sm-6">
                             {!! Form::label('postal_code', 'Postal Code:') !!}<span style="color: red;">*</span>
                             {!! Form::text('postal_code',"", ['class'=>'form-control','placeholder'=>'#####']) !!}
                        </div>
                        <div class="form-group col-lg-4 col-sm-6">
                             {!! Form::label('locality', 'City:') !!}<span style="color: red;">*</span>
                             {!! Form::text('locality',"", ['class'=>'form-control','placeholder'=>'Enter City']) !!}
                        </div>
                        <div class="form-group col-lg-4 col-sm-12">
                             {!! Form::label('region', 'State/Province:') !!}<span style="color: red;">*</span>
                             <select id="region" class="form-control" name="region" data-field="business region" placeholder="TX">
                                  <option value="">Please select...</option>
                                  <option value="AK">Alaska</option>
                                  <option value="AL">Alabama</option>
                                  <option value="AR">Arkansas</option>
                                  <option value="AZ">Arizona</option>
                                  <option value="CA">California</option>
                                  <option value="CO">Colorado</option>
                                  <option value="CT">Connecticut</option>
                                  <option value="DC">District of Columbia</option>
                                  <option value="DE">Delaware</option>
                                  <option value="FL">Florida</option>
                                  <option value="GA">Georgia</option>
                                  <option value="HI">Hawaii</option>
                                  <option value="IA">Iowa</option>
                                  <option value="ID">Idaho</option>
                                  <option value="IL">Illinois</option>
                                  <option value="IN">Indiana</option>
                                  <option value="KS">Kansas</option>
                                  <option value="KY">Kentucky</option>
                                  <option value="LA">Louisiana</option>
                                  <option value="MA">Massachusetts</option>
                                  <option value="MD">Maryland</option>
                                  <option value="ME">Maine</option>
                                  <option value="MI">Michigan</option>
                                  <option value="MN">Minnesota</option>
                                  <option value="MO">Missouri</option>
                                  <option value="MS">Mississippi</option>
                                  <option value="MT">Montana</option>
                                  <option value="NC">North Carolina</option>
                                  <option value="ND">North Dakota</option>
                                  <option value="NE">Nebraska</option>
                                  <option value="NH">New Hampshire</option>
                                  <option value="NJ">New Jersey</option>
                                  <option value="NM">New Mexico</option>
                                  <option value="NV">Nevada</option>
                                  <option value="NY">New York</option>
                                  <option value="OH">Ohio</option>
                                  <option value="OK">Oklahoma</option>
                                  <option value="OR">Oregon</option>
                                  <option value="PA">Pennsylvania</option>
                                  <option value="RI">Rhode Island</option>
                                  <option value="SC">South Carolina</option>
                                  <option value="SD">South Dakota</option>
                                  <option value="TN">Tennessee</option>
                                  <option value="TX">Texas</option>
                                  <option value="UT">Utah</option>
                                  <option value="VI">Virgin Islands</option>
                                  <option value="VA">Virginia</option>
                                  <option value="VT">Vermont</option>
                                  <option value="WA">Washington</option>
                                  <option value="WI">Wisconsin</option>
                                  <option value="WV">West Virginia</option>
                                  <option value="WY">Wyoming</option>
                                  <option value="PR">Puerto Rico</option>
                                  <option value="AS">American Samoa</option>
                                  <option value="FM">Federated States of Micronesia</option>
                                  <option value="GU">Guam</option>
                                  <option value="MH">Marshall Islands</option>
                                  <option value="MP">Northern Mariana Islands</option>
                                  <option value="PW">Palau</option>
                                  <option value="AA">Armed Forces Americas(except Canada)</option>
                                  <option value="AE">Armed Forces Africa, Canada, Europe, Middle East</option>
                                  <option value="AP">Armed Forces Pacific</option>
                                  <option value="AB">Alberta</option>
                                  <option value="BC">British Columbia</option>
                                  <option value="MB">Manitoba</option>
                                  <option value="NB">New Brunswick</option>
                                  <option value="NL">Newfoundland and Labrador</option>
                                  <option value="NT">Northwest Territories</option>
                                  <option value="NS">Nova Scotia</option>
                                  <option value="NU">Nunavut</option>
                                  <option value="ON">Ontario</option>
                                  <option value="PE">Prince Edward Island</option>
                                  <option value="QC">Quebec</option>
                                  <option value="SK">Saskatchewan</option>
                                  <option value="YT">Yukon</option>
                              </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-sm-6">
                             {!! Form::label('annual_volume', 'Anticipated Annual Vol:') !!}<span style="color: red;">*</span>
                             {!! Form::select('annual_volume', ["null" => "Please select...", "120000.00" => "$0.00 - $120,000.00", "360000.00" => "$120,001.00 - $360,000.00", "1200000.00" => "$360,001.00 - $1,200,000.00", "1200000.01" => "$1,200,001.00 or Greater"], "", ['class'=>'form-control','placeholder'=>'Anticipated Annual Vol.']) !!}
                        </div>
                        <div class="form-group col-lg-6 col-sm-6">
                             {!! Form::label('average_transaction_amount', 'Avg. Rent Amount:') !!}<span style="color: red;">*</span>
                             {!! Form::text('average_transaction_amount',"", ['class'=>'form-control','placeholder'=>'$']) !!}
                        </div>
                    </div>
                    <div class="row">
                            <div class="col-lg-12">
                                <h3>Bank Account Info</h3>
                            </div>
                    </div>
                    <div class="row">
                            <div class="form-group col-lg-4 col-sm-6 col-xs-6">
                                {!! Form::label('bank_routing_number', 'Routing Number:') !!}<span style="color: red;">*</span>
                                {!! Form::text('bank_routing_number', "", ['class'=>'form-control','minlength' => "9", 'maxlength' => "9", 'placeholder'=>'9 digit routing number']) !!}
                            </div>
                            <div class="form-group col-lg-4 col-sm-6 col-xs-6">
                                {!! Form::label('bank_account_number', 'Account Number:') !!}<span style="color: red;">*</span>
                                {!! Form::text('bank_account_number', "", ['class'=>'form-control','minlength' => "4", 'maxlength' => "17",'placeholder'=>'']) !!}
                            </div>
                            <div class="form-group col-lg-4 col-sm-12 col-xs-12">
                                {!! Form::label('account_type', 'Account Type:') !!}<span style="color: red;">*</span>
                                {!! Form::select('account_type', ['C' => 'Checking', 'S' => 'Savings'], (isset($paymentSettings->account_type) &&  ($paymentSettings->account_type != "") && ($paymentSettings->account_type == "savings")) ? 'S':'C', ['class'=>'form-control','placeholder'=>'Account Type']) !!}
                            </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-12 edit-property-details content-right">
                    <h3> Owner/Officer Info</h3>
                    <div class="row">
                        <div class="form-group col-lg-6 col-sm-6">
                            {!! Form::label('owner_first_name', 'First Name:') !!}<span style="color: red;">*</span>
                            {!! Form::text('owner_first_name', '', ['class'=>'form-control','placeholder'=>'John']) !!}
                        </div>
                        <div class="form-group col-lg-6 col-sm-6">
                            {!! Form::label('owner_last_name', 'Last Name:') !!}<span style="color: red;">*</span>
                            {!! Form::text('owner_last_name', '', ['class'=>'form-control','placeholder'=>'Doe']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-sm-6">
                            {!! Form::label('owner_email_address', 'Email:') !!}<span style="color: red;">*</span>
                            {!! Form::text('owner_email_address', '', ['class'=>'form-control','placeholder'=>'Email']) !!}
                        </div>
                        <div class="form-group col-lg-6 col-sm-6">
                            {!! Form::label('owner_mobile_phone', 'Mobile Phone Number:') !!}<span style="color: red;">*</span>
                            {!! Form::text('owner_mobile_phone', '', ['class'=>'form-control', 'placeholder'=>'###-###-####']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12">
                            {!! Form::label('owner_streetaddress1', 'Home Address ( No P.O. Boxes please):') !!}<span style="color: red;">*</span>
                            {!! Form::text('owner_streetaddress1', "", ['class'=>'form-control','placeholder'=>'Street Address']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4 col-sm-4">
                             {!! Form::label('owner_postal_code', 'Postal Code:') !!}<span style="color: red;">*</span>
                             {!! Form::text('owner_postal_code',"", ['class'=>'form-control','placeholder'=>'#####']) !!}
                        </div>
                        <div class="form-group col-lg-4 col-sm-4">
                             {!! Form::label('owner_locality', 'City:') !!}<span style="color: red;">*</span>
                             {!! Form::text('owner_locality',"", ['class'=>'form-control','placeholder'=>'Enter City']) !!}
                        </div>
                        <div class="form-group col-lg-4  col-sm-4">
                             {!! Form::label('owner_region', 'State/Province:') !!}<span style="color: red;">*</span>
                             <select id="owner_region" class="form-control" name="owner_region" data-field="business region" placeholder="TX">
                                  <option value="">Please select...</option>
                                  <option value="AK">Alaska</option>
                                  <option value="AL">Alabama</option>
                                  <option value="AR">Arkansas</option>
                                  <option value="AZ">Arizona</option>
                                  <option value="CA">California</option>
                                  <option value="CO">Colorado</option>
                                  <option value="CT">Connecticut</option>
                                  <option value="DC">District of Columbia</option>
                                  <option value="DE">Delaware</option>
                                  <option value="FL">Florida</option>
                                  <option value="GA">Georgia</option>
                                  <option value="HI">Hawaii</option>
                                  <option value="IA">Iowa</option>
                                  <option value="ID">Idaho</option>
                                  <option value="IL">Illinois</option>
                                  <option value="IN">Indiana</option>
                                  <option value="KS">Kansas</option>
                                  <option value="KY">Kentucky</option>
                                  <option value="LA">Louisiana</option>
                                  <option value="MA">Massachusetts</option>
                                  <option value="MD">Maryland</option>
                                  <option value="ME">Maine</option>
                                  <option value="MI">Michigan</option>
                                  <option value="MN">Minnesota</option>
                                  <option value="MO">Missouri</option>
                                  <option value="MS">Mississippi</option>
                                  <option value="MT">Montana</option>
                                  <option value="NC">North Carolina</option>
                                  <option value="ND">North Dakota</option>
                                  <option value="NE">Nebraska</option>
                                  <option value="NH">New Hampshire</option>
                                  <option value="NJ">New Jersey</option>
                                  <option value="NM">New Mexico</option>
                                  <option value="NV">Nevada</option>
                                  <option value="NY">New York</option>
                                  <option value="OH">Ohio</option>
                                  <option value="OK">Oklahoma</option>
                                  <option value="OR">Oregon</option>
                                  <option value="PA">Pennsylvania</option>
                                  <option value="RI">Rhode Island</option>
                                  <option value="SC">South Carolina</option>
                                  <option value="SD">South Dakota</option>
                                  <option value="TN">Tennessee</option>
                                  <option value="TX">Texas</option>
                                  <option value="UT">Utah</option>
                                  <option value="VI">Virgin Islands</option>
                                  <option value="VA">Virginia</option>
                                  <option value="VT">Vermont</option>
                                  <option value="WA">Washington</option>
                                  <option value="WI">Wisconsin</option>
                                  <option value="WV">West Virginia</option>
                                  <option value="WY">Wyoming</option>
                                  <option value="PR">Puerto Rico</option>
                                  <option value="AS">American Samoa</option>
                                  <option value="FM">Federated States of Micronesia</option>
                                  <option value="GU">Guam</option>
                                  <option value="MH">Marshall Islands</option>
                                  <option value="MP">Northern Mariana Islands</option>
                                  <option value="PW">Palau</option>
                                  <option value="AA">Armed Forces Americas(except Canada)</option>
                                  <option value="AE">Armed Forces Africa, Canada, Europe, Middle East</option>
                                  <option value="AP">Armed Forces Pacific</option>
                                  <option value="AB">Alberta</option>
                                  <option value="BC">British Columbia</option>
                                  <option value="MB">Manitoba</option>
                                  <option value="NB">New Brunswick</option>
                                  <option value="NL">Newfoundland and Labrador</option>
                                  <option value="NT">Northwest Territories</option>
                                  <option value="NS">Nova Scotia</option>
                                  <option value="NU">Nunavut</option>
                                  <option value="ON">Ontario</option>
                                  <option value="PE">Prince Edward Island</option>
                                  <option value="QC">Quebec</option>
                                  <option value="SK">Saskatchewan</option>
                                  <option value="YT">Yukon</option>
                              </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6 col-sm-6">
                             {!! Form::label('owner_date_of_birth', 'Date of Birth') !!}<span style="color: red;">*</span>
                             {!! Form::text('owner_date_of_birth',"", ['class'=>'form-control','placeholder'=>'MM/DD/YYYY']) !!}
                        </div>
                        <div class="form-group col-lg-6 col-sm-6">
                             {!! Form::label('owner_last4_ssn', 'Social Security Number') !!}<span style="color: red;">*</span>
                             {!! Form::text('owner_last4_ssn',"", ['class'=>'form-control', 'maxlength'=> "4", 'placeholder'=>'Last 4 digits']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Please double check to ensure your application information is entered correctly in order to have it processed in the quickest time possible.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-4 col-sm-6">
                            <span style="font-size: 18px;font-weight: bold;"><a href="/public/images/MSAStandardACHonly0616.pdf" target="_blank"><i class="fa fa-file"></i> Terms & Conditions</a></span>
                        </div>
                        <div class="form-group col-lg-4 col-sm-6">
                            {!! Form::checkbox('termsconditions', 1,false) !!}
                            {!! Form::label('termsconditions', 'I Agree to Terms and Conditions') !!}
                        </div>
                        <div class="form-group col-lg-4 col-sm-6">
                        <a type="submit" class="btn add-btn" onclick = "confirmPay();">Submit</a>
			    {!! Form::submit('Submit', ['class' => 'hidden', 'id' => 'submitBtn']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div><!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script type="text/javascript">
function confirmPay()
{
        bootbox.confirm({
		@if ($applicationCount == 0)
                message: "Thanks to your early bird plan this first onboarding fee is on us!",
                @endif
		@if ($applicationCount > 0)
		message: "There will be an onboarding fee of $29.99 associated with this application, do you accept?",
		@endif
		buttons:{
                        confirm:{
				@if ($applicationCount == 0)
                                label: 'Thanks',
				@endif
				@if ($applicationCount > 0)
				label: 'Yes',
				@endif
                                className: 'btn-success'
                        },
                        cancel:{
                                @if ($applicationCount == 0)
				label: 'Cancel',
				@endif
				@if($applicationCount > 0)
				label: 'No',
				@endif
                                className: 'btn-danger'
                        }
                },
                callback: function(result) {
                        if(result === true)
                                {
				$("#submitBtn").click();
                                }
                                else
                                {
                                }
                }
        });
}

</script>
@endsection
