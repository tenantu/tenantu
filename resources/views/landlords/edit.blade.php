@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($landlord,['method' => 'POST','route'=>['landlords.update',$landlord->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('FirstName', 'FirstName:') !!}<span style="color: red;">*</span>
            {!! Form::text('firstname',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('LastName', 'LastName:') !!}<span style="color: red;">*</span>
            {!! Form::text('lastname',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('SelectSchool', 'Select School:') !!}
            <span style="color: red;">*</span><br/>
            {!! Form::select('school',['' => 'Please Select']+$school, $landlord->school_id, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email',null,['class'=>'form-control','readonly']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::label('About', 'About:') !!}<span style="color: red;"></span>
            {!! Form::textarea('about',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Birth date', 'Birth date:') !!}<span style="color: red;"></span>
            {!! Form::text('dob',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Location', 'Location:') !!}<span style="color: red;"></span>
            {!! Form::text('location',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Phone', 'Phone:') !!}<span style="color: red;"></span>
            {!! Form::text('phone',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Website', 'Website:') !!}<span style="color: red;"></span>
            {!! Form::text('website',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('State', 'State:') !!}<span style="color: red;"></span>
            {!! Form::text('state',null,['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Address', 'Address:') !!}<span style="color: red;"></span>
            {!! Form::textarea('address',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Language', 'Language:') !!}<span style="color: red;"></span>
            {!! Form::text('language',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('City', 'City:') !!}<span style="color: red;"></span>
            {!! Form::text('city',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Profile Image', 'Profile Image:') !!}<span style="color: red;"></span>
            <img  class="profile-image" src="{{ $profileImage }}">
        </div>
        
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<script>
    var imageUrl = '{{ $imageUrl }}/{{ $landlord->id }}/{{ $landlord->image }}';
    var id= {{ $landlord->id }};
    var image='{{ $landlord->image }}';
    $(document).ready(function() {
        $('.fileinput-remove-button').click(function() {
            if(confirm("Are you sure you want to delete this image?"))
            {
                 $.ajax({
                      url: '../deleteImage',
                      type: "post",
                      data :'user_id='+ id ,
                      success: function(data){
                         alert('Removed Successfully');
                        }
                    });
             }
        });
    });
    CKEDITOR.replace('editor');
    $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('input').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    if(image) {
     $("#image").fileinput({
            showUpload: false,
            allowedFileExtensions:['jpg', 'gif', 'png'],
            initialPreview: [
                '<img src='+imageUrl+' class="file-preview-image" alt="{{ $landlord->image }}" title="{{ $landlord->image }}">'
            ],
            initialCaption: "{{ $landlord->image }}"
        });
    }
    else {
       $("#image").fileinput({
            showUpload: false,
            allowedFileExtensions:['jpg', 'gif', 'png'],
        });
    }
      $("#location").tagit({
            allowSpaces: true,
            fieldName: "locations[]",
            requireAutocomplete: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function(request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function(item) {
                            return item.description;
                        });
                        currentlyValidTags = data;
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({ input: request.term }, callback);
                }
            }
        });
</script>
@endsection
