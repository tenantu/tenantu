@if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            
            {{ Session::get('message') }}
        </div>
    @endif
    
@if (count($errors) > 0)
        <div class="alert alert-danger">
           {{$errors}}
        </div>
        @endif
	{!! Form::model('',['method' => 'POST','route'=>['landlords.postLogin']]) !!}

    <div>
        {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
        {!! Form::text('email', Input::old('name'), ['class'=>'form-control']) !!}
    </div>

    <div>
         {!! Form::label('Email', 'Password:') !!}<span style="color: red;">*</span>
         {!! Form::password('password',['class'=>'form-control']) !!}
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>
    <div>
        <a href="{{ URL::to('landlords/register') }}">Register</a>
    </div>
    <div>
        <a href="{{ URL::to('landlords/forgotpassword') }}">Forgot Your Password?</a>
    </div>
	<!--<div>
        <a href="{{ URL::to('landlords/email') }}">Forgot Your Password?</a>
    </div>-->
    <div>
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}
