@extends('layouts.landlord-inner')
@section('inner-content')
@if (Session::has('message-success'))
    @include('elements.message-success')
@endif
@include('elements.validation-message')
@if (session('success'))
  <div class="flash-message">
  <div class="alert alert-success">

  </div>
  </div>
@endif
<!--
<div class="container">
  @foreach ($maintenanceRequests as $maintenanceRequest)
    {{ $maintenanceRequest->issue_type_id}}
  @endforeach
</div>
-->
<div class="content-wrapper">
  <section class="content">
    @include('elements.breadcrump-landlord')
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>

                <th class="col-md-2">
    	@if ((Request::segment(3) == 'Date') && (Request::segment(4) == 'ASC') )
                   <a href="{{ route('landlords.myMaintenanceRequests', ['Date', 'DESC']) }}">Date Submitted ^</a>
            @elseif ((Request::segment(3) == 'Date') && (Request::segment(4) == 'DESC') )
                   <a href="{{ route('landlords.myMaintenanceRequests', ['Date', 'ASC']) }}">Date Submitted v</a>
            @else
    	       <a href="{{ route('landlords.myMaintenanceRequests', ['Date', 'DESC']) }}">Date Submitted</a>
    	@endif
                </th>
    	    <th class="col-md-2">
    	@if ((Request::segment(3) == 'Property') && (Request::segment(4) == 'DESC'))
                   <a href="{{ route('landlords.myMaintenanceRequests', ['Property', 'DESC']) }}">Property Name v</a>
            @else
    	       <a href="{{ route('landlords.myMaintenanceRequests', ['Property', 'DESC']) }}">Property Name</a>
    	@endif
                </th>
                <th class="col-md-2">
    	@if ((Request::segment(3) == 'Type') && (Request::segment(4) == 'DESC'))
                   <a href="{{ route('landlords.myMaintenanceRequests', ['Type', 'DESC']) }}">Issue Type v</a>
    	@else
                   <a href="{{ route('landlords.myMaintenanceRequests', ['Type', 'DESC']) }}">Issue Type</a>
            @endif
                </th>
                 <th class="col-md-2">
    	@if ((Request::segment(3) == 'Description') && (Request::segment(4) == 'ASC'))
                    <a href="{{ route('landlords.myMaintenanceRequests', ['Description', 'ASC']) }}">Issue Description v</a>
    	@else
                    <a href="{{ route('landlords.myMaintenanceRequests', ['Description', 'ASC']) }}">Issue Description</a>
            @endif
                 </th>
                 <th class="col-md-2">
    	@if ((Request::segment(3) == 'Status') && (Request::segment(4) == 'ASC'))
                     <a href="{{ route('landlords.myMaintenanceRequests', ['Status', 'ASC']) }}">Status v</a>
    	@else
         		 <a href="{{ route('landlords.myMaintenanceRequests', ['Status', 'ASC']) }}">Status</a>
            @endif
                 </th>
    	     <th class="col-md-2">
    		 <a>Available Times</a>
    	     </th>


            </tr>
        </thead>
        <tbody>
        @foreach ($maintenanceRequests as $maintenanceRequest)
            {{--<tr data-link='{{ action('MaintenanceRequestController@show', $maintenanceRequest ) }}'>--}}
            <tr>
                <th scope="row">{{ $maintenanceRequest->date_encountered }}</th>
    	    <td>{{ $maintenanceRequest->property_name }}</td>
                <td>{!! HTML::resolveMaintenanceRequestIssueType($maintenanceRequest->issue_type_id) !!}</td>
                <td>{{$maintenanceRequest->issue_desc}}</td>
                <td> {!! HTML::resolveMaintenanceRequestStatusCode($maintenanceRequest->status) !!} </td>
                {{--<td>{{$maintenanceRequest->status}}</td>--}}
                <td>{!! HTML::grabAvailableTimes($maintenanceRequest->id) !!}</td>
    	    <td>
    	@if ($maintenanceRequest->status == 0)
                    <a href="{{ route('landlords.changeMaintenanceRequestStatusToRead',$maintenanceRequest->id) }}" class="btn btn-link"><i class="fa fa-book"></i> Mark as Read </a>
            @elseif ($maintenanceRequest->status == 1)
                    <a href="{{ route('landlords.changeMaintenanceRequestStatusToInProgress',$maintenanceRequest->id) }}" class="btn btn-link"><i class="fa fa-book"></i> Mark as In Progress </a>
    	@elseif ($maintenanceRequest->status == 2)
    		<a href="{{ route('landlords.changeMaintenanceRequestStatusToCompleted',$maintenanceRequest->id) }}" class="btn btn-link"><i class="fa fa-book"></i> Mark as Completed </a>
    	@elseif ($maintenanceRequest->status == 3)
    		 <div style="cursor:default; pointer-events: none;" class="btn btn-link"><i class="fa fa-book"></i> Completed </div>
    	@endif
    	     </td>

            </tr>

          @endforeach
        </tbody>
      </table>
          <div class="col-md-8 text-right">
              {!! str_replace('/?', '?', $maintenanceRequests->render()) !!}
          </div>
      </div>
  </section>
</div>
@endsection
