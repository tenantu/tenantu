<!-- <!DOCTYPE html>
<html> -->
@extends('layouts.landlord-inner')
@section('inner-content')
    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    @include('elements.validation-message')
    @if (session('success'))
        <div class="flash-message">
            <div class="alert alert-success">

            </div>
        </div>
    @endif


<head>
</head>

<div class="content-wrapper">
<section class="content">
<div class="subscription">
	<div class="container">
		<div class="row">
		<div class="col-sm-2"> </div>
                                                                <div class="col-sm-4 sub-free">
                                                                <ul class="pricing_table" id="table">
                                                                <li class="price_block pull-left">
                                                                <h3>Included</h3>
                                                                  <div class="price">
                                                                   <div class="price_figure">
                                                                <span class="price_number">FREE</span>
                                                                <span class="price_tenure"></span>
                                                                   </div>
                                                                      </div>
                                                                          <ul class="features">
                                                                           <li>Property Upload</li>
                                                                           <li>Individual Tenant Portals</li>
                                                                           <li>On-Site Messaging</li>
                                                                           <li>Property Listings in School Marketplace</li>
                                                                           <li>24/7 Email Support</li>
                                                                           <li><del>Maintenance Request Processing</del></li>
                                                                           <li><del>Rent Payment Processing</del></li>
                                                                           <li><del>Accounting Export</del></li>
                                                                           </ul>
                                                                           <div class="footer">
                                                                         <a href="" class="action_button">Pick this plan</a>
                                                                            </div>

                                                                 </li>
                                                                 </ul>
                                                                </div>
                                                                <div class="col-sm-4 sub-plus">
                                                                <ul class="pricing_table ">
                                                                <li class="price_block pull-right">
                                                                <h3>Included</h3>
                                                                  <div class="price">
                                                                   <div class="price_figure">
                                                                <span class="price_number">$24.99</span>
                                                                <span class="price_tenure"></span>
                                                                   </div>
                                                                      </div>
                                                                          <ul class="features">
                                                                           <li>Property Upload</li>
                                                                           <li>Individual Tenant Portals</li>
                                                                           <li>On-Site Messaging</li>
                                                                           <li>Property Listings in School Marketplace</li>
                                                                           <li>24/7 Email Support</li>
                                                                           <li>Maintenance Request Processing</li>
                                                                           <li>Rent Payment Processing</li>
                                                                           <li>Accounting Export</li>
                                                                           </ul>
                                                                           <div class="footer">
									@if ($landlordSubscription->subscription_plan_id == 1)
					<a href="{{ route('landlords.upgradesubscription', ['subscriptionId'=>4]) }}" class="action_button" id="earlybird" name="earlybird" value="test">Upgrade!</a>
					@else
						<a href="{{ route('landlords.mysubscription', ['subscriptionId'=>4]) }}" class="action_button" id="earlybird" name="earlybird" value="test">View My Subscription!</a>
					@endif
                                                                            </div>

                                                                 </li>
                                                                 </ul>
                                                                </div>

			<div class="col-sm-2"> </div>
		</div>
	</div>
</div>
</section>
</div>

<style>
    * {
        margin: 0;
        padding: 0;
    }
    body {
        /*font-family: Metropolis;*/
        /*src: url(../../../public/fonts/Metropolis-Bold.woff);*/
    }
    .col-centered{
        float: none !important;
        margin: 0 auto;
    }
    /*----------
    Blocks
    ----------*/
    /*Pricing table and price blocks*/
    .pricing_table {
        line-height: 150%;
        font-size: 12px;
        margin: 0 auto;
        width: 100%;
        max-width: 2000px;
        padding-top: 10px;
        margin-top: 10px;
    }
    .price_block {
        text-align: center;
        width: 100%;
        color: #fff;
        float: left;
        list-style-type: none;
        transition: all 0.25s;
        position: relative;
        box-sizing: border-box;
        margin-bottom: 10px;
        border-bottom: 1px solid transparent;
    }
    /*Price heads*/
    .pricing_table h3 {
        padding: 10px 0;
        background: #333;
        margin: -10px 0 1px 0;
    }
    /*Price tags*/
    .price {
        display: table;
        background: #444;
        width: 100%;
        height: 70px;
    }
    .price_figure {
        font-size: 24px;
        text-transform: uppercase;
        vertical-align: middle;
        display: table-cell;
    }
    .price_number {
        font-weight: bold;
        display: block;
    }
    .price_tenure {
        font-size: 11px;
    }
    /*Features*/
    .features {
        color: #000;
    }
    .features li {
        padding: 8px 15px;
        font-size: 11px;
        list-style-type: none;
    }
    .footer {
        padding: 15px;
    }
    .action_button {
        text-decoration: none;
        color: #fff !important;
        font-weight: bold;
        border-radius: 5px;
        background: linear-gradient(#666, #333);
        padding: 5px 20px;
        font-size: 11px;
        text-transform: uppercase;
    }
    .price_block:hover {
        box-shadow: 0 0 0px 5px rgba(0, 0, 0, 0.5);
        transform: scale(1.04) translateY(-5px);
        z-index: 1;
        border-bottom: 0 none;
    }
    .price_block:hover .price {
        /*background:linear-gradient(#369AFF, #369AFF);*/
        box-shadow: inset 0 0 45px 1px #369AFF;
    }
    .price_block:hover h3 {
        background: #222;
    }
    .price_block:hover .action_button {
        background: linear-gradient(#369AFF, #369AFF);
    }
    @media only screen and (min-width: 1px) and (max-width: 479px) {
        .price_block {
            width: 100%;
        }
        .price_block:nth-child(odd) {
            border-right: 1px solid transparent;
        }
        .price_block:nth-child(3) {
            clear: both;
        }
        .price_block:nth-child(odd):hover {
            border: 0 none;
        }
    }
    @media only screen and (min-width: 480px) and (max-width: 768px) {
        .price_block {
            width: 100%;
        }
        .price_block:nth-child(odd) {
            border-right: 1px solid transparent;
        }
        .price_block:nth-child(3) {
            clear: both;
        }
        .price_block:nth-child(odd):hover {
            border: 0 none;
        }
    }
    @media only screen and (min-width: 768px) {
        .price_block {
            width: 100%;
        }
        .price_block {
            border-right: 1px solid transparent;
            border-bottom: 0 none;
        }
        .price_block:last-child {
            border-right: 0 none;
        }
        .price_block:hover {
            border: 0 none;
        }
    }
    .skeleton, .skeleton ul, .skeleton li, .skeleton div, .skeleton h3, .skeleton span, .skeleton p {
        border: 5px solid rgba(255, 255, 255, 0.9);
        border-radius: 5px;
        margin: 7px !important;
        background: rgba(0, 0, 0, 0.05) !important;
        padding: 0 !important;
        text-align: left !important;
        display: block !important;
        width: auto !important;
        height: auto !important;
        font-size: 10px !important;
        font-style: italic !important;
        text-transform: none !important;
        font-weight: normal !important;
        color: black !important;
    }
    .skeleton .label {
        font-size: 11px !important;
        font-style: italic !important;
        text-transform: none !important;
        font-weight: normal !important;
        color: white !important;
        border: 0 none !important;
        padding: 5px !important;
        margin: 0 !important;
        float: none !important;
        text-align: left !important;
        text-shadow: 0 0 1px white;
        background: none !important;
    }
    .skeleton {
        display: none !important;
        margin: 100px !important;
        clear: both;
    }
</style>


<!-- </html> -->

@endsection
