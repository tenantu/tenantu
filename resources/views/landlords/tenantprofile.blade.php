@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
          @include('elements.breadcrump-landlord')
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
              <!-- Landlord View tenant  Profile -->

              <div class="tnt-box tenantu-profile l-t-view">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <div class="profile-image">
                      <img src="{{HTML::getTenantProfileImage($tenantObj->id)}}">
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-8col-xs-12">
                    <div class="tnt-name">{{ucfirst($tenantObj->firstname).' '. ucfirst($tenantObj->lastname) }}</div>
                    <div class="tnt-place">{{$tenantObj->location}}</div>
                    <div class="tnt-contacts">
                      <a href="#">{{$tenantObj->email}}</a>
                      <div>{{$tenantObj->phone}}</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="tenantu-prf-details">

                      <div class="row cnts">
					   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						  <label>Language</label>
						  <div class="language">{{$tenantObj->language}}</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						  <label>School</label>
						  <div class="language">{{ $schoolname->schoolname }}</div>
						</div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>City</label>
                          <div class="city">{{$tenantObj->city}}</div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <label>State</label>
                          <div class="gender">{{$tenantObj->state}}</div>
                        </div>
                        @if($tenantObj->about!='')
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label>About me</label>
                          <div class="dob">{{$tenantObj->about}}</div>
                        </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            <!-- ./col -->
          </div><!-- /.row -->

          <div>

        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
