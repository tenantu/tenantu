@extends('layouts.landlord-inner')
    @section('inner-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
            @include('elements.validation-message')
            @if (session('success'))
                <div class="flash-message">
                    <div class="alert alert-success">

                    </div>
                </div>
        @endif
        @if (Session::has('warningMessage'))
    <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
    @endif
        @if(Session::has('displayBank'))
                            @include('elements.validation-message')
                        @endif
                        @if (Session::has('message-success') && Session::has('successBank'))
                            @include('elements.message-success')
                        @endif
                        @if (Session::has('message-error') && Session::has('failureBank'))
                            @include('elements.message-error')
                        @endif
    <!-- Main content -->
        <section class="content">
            @include('elements.breadcrump-landlord')
            <div class="payments">
                <h2>Payment Options</h2>
                <div class="row">
                    <p>Below you will need to input your credentials till the blank field requirements are satisfied. Upon completion, please hit the “Save” button to finalize the transaction.</p>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">


                        <h3><i class="fa fa-file-text"></i> Pay by ACH / ECheck</h3>
                        {!! Form::model('',['method' => 'GET','class'=>'edit-property-details row','id'=>'tenantAddForm','route'=>['landlords.subscriptionpaymentsettingsubmit']]) !!}
                        <div class="form-group col-sm-12 col-xs-12">
                            			{!! Form::label('bank_name', 'Bank:') !!}<span style="color: red;">*</span>
                            			{!! Form::text('bank_name', isset($paymentSettings->bank_name) ? $paymentSettings->bank_name:"", ['class'=>'form-control','placeholder'=>'Bank Name']) !!}
                                  </div>
                                  <div class="form-group col-sm-12 col-xs-12">
                                  	{!! Form::label('account_type', 'Account Type:') !!}<span style="color: red;">*</span>
                                  	{!! Form::select('account_type', ['C' => 'Checking', 'S' => 'Savings'], (isset($paymentSettings->account_type) &&  ($paymentSettings->account_type != "") && ($paymentSettings->account_type == "savings")) ? 'S':'C', ['class'=>'form-control','placeholder'=>'Account Type']) !!}
                                  </div>
                                  <div class="form-group col-sm-12 col-xs-12">
                                      {!! Form::label('account_holder', 'Account Holder:') !!}<span style="color: red;">*</span>
                                      {!! Form::text('account_holder', isset($paymentSettings->account_holder) ? $paymentSettings->account_holder:"", ['class'=>'form-control','placeholder'=>'Account Holder']) !!}
                                  </div>
                                  <div class="form-group col-sm-12 col-xs-12">
              						{!! Form::label('account_no', 'Account Number:') !!}<span style="color: red;">*</span>
                            			{!! Form::text('account_no', isset($paymentSettings->account_number) ? $paymentSettings->account_number:"", ['class'=>'form-control','placeholder'=>'Account Number']) !!}
              					</div>
              					<div class="form-group col-sm-12 col-xs-12">
                            			{!! Form::label('routing_no', 'Routing Number:') !!}<span style="color: red;">*</span>
                            			{!! Form::text('routing_no', isset($paymentSettings->routing_number) ? $paymentSettings->routing_number:"", ['class'=>'form-control','placeholder'=>'Routing Number']) !!}
              					</div>
              					<div class="form-group col-sm-12 col-xs-12">
                            			{!! Form::label('st_line1', 'Address Line 1:') !!}<span style="color: red;">*</span>
                            			{!! Form::text('st_line1', (isset($paymentSettings->street_line1) && ($paymentSettings->street_line1!="")) ? $paymentSettings->street_line1:"", ['class'=>'form-control','placeholder'=>'Street address, P.O. box, etc']) !!}
              					</div>
              					<div class="form-group col-sm-12">
              			            {!! Form::label('st_line2', 'Address Line 2 (Optional):') !!}<span style="color: red;"></span>
              			            {!! Form::text('st_line2',(isset($paymentSettings->street_line2) && ($paymentSettings->street_line2!="")) ? $paymentSettings->street_line2:"", ['class'=>'form-control','placeholder'=>'Apartment, suite, unit, building, floor, etc.']) !!}
              					 </div>
              					<div class="form-group col-sm-12">
              	              		{!! Form::label('city', 'City/Locality:') !!}<span style="color: red;">*</span>
              	              		{!! Form::text('city', (isset($paymentSettings->city) && ($paymentSettings->city!="")) ? $paymentSettings->city : "", ['class'=>'form-control','placeholder'=>'City']) !!}
              					</div>
              					<div class="form-group col-sm-12">
              	              		{!! Form::label('state', 'State/Region:') !!}<span style="color: red;">*</span>
              	              		{!! Form::text('state', (isset($paymentSettings->state) && ($paymentSettings->state!="")) ? $paymentSettings->state : "", ['class'=>'form-control','placeholder'=>'State']) !!}
              					</div>
              					<div class="form-group col-sm-12">
              	              		{!! Form::label('zipCode', 'Zip code:') !!}<span style="color: red;">*</span>
              	              		{!! Form::text('zip_code', (isset($paymentSettings->zip_code) && ($paymentSettings->zip_code!="")) ? $paymentSettings->zip_code : "", ['class'=>'form-control','placeholder'=>'Zip code']) !!}
              					</div>
                        <div class="form-group col-sm-12 col-xs-12">
                          {!! Form::label('address_type', 'Address Type:') !!}<span style="color: red;">*</span>
                          {!! Form::select('address_type', ['residential' => 'Residential', 'commercial' => 'Commercial'], (isset($paymentSettings->address_type) &&  ($paymentSettings->address_type != "") && ($paymentSettings->address_type == "residential")) ? 'residential':'commercial', ['class'=>'form-control','placeholder'=>'Residential Or Commercial']) !!}
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                            {!! Form::label('type', 'Make as default payment method:') !!}<span style="color: red;">*</span>
                            {!! Form::checkbox('type', 2,((isset($paymentSettings->type) && ($paymentSettings->type) === '2') ? true : false), ['id'=>'options1']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Submit', ['class' => 'btn save-btn']) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
                        <div class="or">OR</div>

                        <h3> Pay by Credit Card</h3>
                        {!! Form::model('',['method' => 'GET','class'=>'edit-property-details row','id'=>'tenantAddForm','route'=>['landlords.subscriptionpaymentsettingsubmit']]) !!}
                        <div class="form-group col-sm-12">
                            {!! Form::label('card_type', 'Card Type:') !!}<span style="color: red;">*</span><br>
                            {!! Form::select('card_type', array( '' => 'Select a Credit Card Type', 'visa' => 'Visa', 'amex' => 'American Express', 'mast' => 'MasterCard', 'disc' => 'Discover'), isset($paymentSettings->card_type) ? $paymentSettings->card_type:'') !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('name_on_card', 'Name on Card:') !!}<span style="color: red;">*</span>
                            {!! Form::text('name_on_card', (isset($paymentSettings->name_on_card) && ($paymentSettings->name_on_card!="")) ? $paymentSettings->name_on_card : '', ['class'=>'form-control','placeholder'=>'Name On Card']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('card_number', 'Credit Card Number:') !!}<span style="color: red;">*</span>
                            {!! Form::text('card_number', (isset($paymentSettings->masked_account_number) && ($paymentSettings->masked_account_number!="")) ? $paymentSettings->masked_account_number : '', ['class'=>'form-control', 'placeholder'=>'Debit/Credit Card Number']) !!}
                        </div>
                        <div class="form-group col-sm-3 col-xs-12">
                            {!! Form::label('expire_month', 'Expiration Month:') !!}<span style="color: red;">*</span>
                            {!! Form::selectMonth('expire_month', isset($paymentSettings->expire_month) ? $paymentSettings->expire_month:'', ['class'=>'form-control'], '%m') !!}
                        </div>
                        <div class="form-group col-sm-12">
                            {!! Form::label('expire_year', 'Expiration Year:') !!}<span style="color: red;">*</span><br>
                            {!! Form::selectYear('expire_year', 2017, 2026, isset($paymentSettings->expire_year) ? $paymentSettings->expire_year:'') !!}
                        </div>
                        <div class="form-group col-sm-5">
                            {!! Form::label('card_verification_value', 'Card verification number:') !!}<span style="color: red;">*</span>
                            {!! Form::text('card_verification_value', isset($paymentSettings->card_verification_value) ? $paymentSettings->card_verification_value : '', ['class'=>'form-control', 'placeholder' => 'Security Code']) !!}
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                                  {!! Form::label('st_line1', 'Address Line 1:') !!}<span style="color: red;">*</span>
                                  {!! Form::text('st_line1', (isset($paymentSettings->street_line1) && ($paymentSettings->street_line1!="")) ? $paymentSettings->street_line1:"", ['class'=>'form-control','placeholder'=>'Street address, P.O. box, etc']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                                {!! Form::label('st_line2', 'Address Line 2 (Optional):') !!}<span style="color: red;"></span>
                                {!! Form::text('st_line2',(isset($paymentSettings->street_line2) && ($paymentSettings->street_line2!="")) ? $paymentSettings->street_line2:"", ['class'=>'form-control','placeholder'=>'Apartment, suite, unit, building, floor, etc.']) !!}
                         </div>
                        <div class="form-group col-sm-12">
                                  {!! Form::label('city', 'City/Locality:') !!}<span style="color: red;">*</span>
                                  {!! Form::text('city', (isset($paymentSettings->city) && ($paymentSettings->city!="")) ? $paymentSettings->city : "", ['class'=>'form-control','placeholder'=>'City']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                                  {!! Form::label('state', 'State/Region:') !!}<span style="color: red;">*</span>
                                  {!! Form::text('state', (isset($paymentSettings->state) && ($paymentSettings->state!="")) ? $paymentSettings->state : "", ['class'=>'form-control','placeholder'=>'State']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                                  {!! Form::label('zipCode', 'Zip code:') !!}<span style="color: red;">*</span>
                                  {!! Form::text('zip_code', (isset($paymentSettings->zip_code) && ($paymentSettings->zip_code!="")) ? $paymentSettings->zip_code : "", ['class'=>'form-control','placeholder'=>'Zip code']) !!}
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                          {!! Form::label('address_type', 'Address Type:') !!}<span style="color: red;">*</span>
                          {!! Form::select('address_type', ['residential' => 'Residential', 'commercial' => 'Commercial'], (isset($paymentSettings->address_type) &&  ($paymentSettings->address_type != "") && ($paymentSettings->address_type == "residential")) ? 'residential':'commercial', ['class'=>'form-control','placeholder'=>'Residential Or Commercial']) !!}
                        </div>
                        <div class="form-group col-sm-9">
                            {!! Form::submit('Submit', ['class' => 'btn save-btn']) !!}
                        </div>
                        {!! Form::close() !!}
                        <p>All TenantU Services, LLC payments are handled and processed by Forte Paymentsⓒ. TenantU Services will have absolutely zero access to customer’s sensitive information. All customer transactions are handled with the utmost protection and security under Forte.</p>
                    </div>
                </div>
            </div>
        </section>
        <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>
    </div><!-- /.content-wrapper -->
@endsection
