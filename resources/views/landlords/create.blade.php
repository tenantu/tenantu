@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    @if (Session::has('message-error'))
        @include('elements.message-error')
    @endif

    {!! Form::model('',['method' => 'POST','route'=>['landlords.store'],'files'=>true]) !!}

        

        <div class="form-group">
            {!! Form::label('FirstName', 'FirstName:') !!}<span style="color: red;">*</span>
            {!! Form::text('firstname', Input::old('firstname'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('LastName', 'LastName:') !!}<span style="color: red;">*</span>
            {!! Form::text('lastname', Input::old('lastname'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('SelectSchool', 'Select School:') !!}
            <span style="color: red;">*</span><br/>
            {!! Form::select('school',['' => 'Select school']+$school, null, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Password', 'Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Confirm Password', 'Confirm Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email', Input::old('name'), ['class'=>'form-control']) !!}
        </div>

       
         
          <div class="form-group">
            {!! Form::label('is_active', 'Status:') !!}
            {!! Form::checkbox('is_active', 1, Input::old('is_active'), ['class'=>'']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<script>
    CKEDITOR.replace('editor');
     $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('input[type="radio"]').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });

    });
    $("#image").fileinput({showUpload: false});

      $("#location").tagit({
            allowSpaces: true,
            fieldName: "locations[]",
            requireAutocomplete: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function(request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function(item) {
                            return item.description;
                        });
                        currentlyValidTags = data;
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({ input: request.term }, callback);
                }
            }
        });
</script>
@endsection
