@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
        <!-- Main content -->
<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
              <!-- Landlord Profile -->
               <div class="pull-right"><button onclick="location.href='{{route('landlords.editprofile')}}';" class="btn btn-default">Profile edit</button></div> 
              <div class="tnt-box landlord-profile">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                    <div class="profile-image">
                      <img src="{{ $profileImage }}">
                    </div>
                    <div class="landlord-name">{{ $profile->firstname.' '. $profile->lastname }}</div>
                    <div class="landlord-place">Unites States of America</div>
                    <div class="landlord-contacts">
                      <a href="#">{{ $profile->email }}</a>
                      <div>{{ ($profile->phone !="") ? $profile->phone : "Phone Not Provided" }}</div>
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="box-heads">
                      <h4>About Us</h4>
                    </div>
                    <p class="texts">{{ $profile->description }}</p>
                    <div class="row">
                      <div class="col-lg-5 col-md-4 col-sm-4 col-xs-12">
			@if ($profile->website)
                        <a href="{{ $profile->website }}" class="weblink">{{ $profile->website }}</a>
                        @endif
			</div>
                      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <!--div class="star-rating"><img src="{{ $rating }}"></div-->
                      </div>
                      <!--div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><a href="#"> Reviews ({{ $review }})</a></div-->
                    </div>
                  </div>
                </div>


              </div>

              <div class="row">
                <div class="col-lg-4">
                  <div class="tnt-box">
                    <div class="box-heads">
                      <h4>Other Details</h4>
                    </div>
                    <div class="other-details">
                      <label>Place</label>
                      <div class="place">{{ $profile->location }}</div>
                      <label>Country</label>
                      <div class="country">United states of America</div>
                      <label>Contact Details</label>
                      <div class="place">{{ $profile->address }}</div>
                    </div>
                  </div>
                </div>
               <!--  <div class="col-lg-8">
                 <div class="tnt-box comments-container">
                   <div class="box-heads">
                     <h4>Comments</h4>
                   </div>
                   <div>
                     <div class="media">
                       <div class="media-left">
                         <a href="#" class="comment-image">
                           <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                         </a>
                       </div>
                       <div class="media-body">
                           <div class="media-heading">
                             <span class="username">Alexander Doe</span><span>Posted on :</span><span class="postedon">10-12-2015 12:0 am</span>
                           </div>
                           <div>
                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie finibus lacinia. Sed id mollis ligula. Pellentesque ut facilisis lacus. Aenean tempus ipsum in mauris ultrices, in mattis metus vestibulum. Sed orci sem, lobortis at lorem in, volutpat gravida ante.</p>
                           </div>
                           <div><img src="{{ asset('public/img/stars.png') }}"></div>
                       </div>
                     </div>
                     <div class="media">
                       <div class="media-left">
                         <a href="#" class="comment-image">
                           <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                         </a>
                       </div>
                       <div class="media-body">
                           <div class="media-heading">
                             <span class="username">Alexander Doe</span><span>Posted on :</span><span class="postedon">10-12-2015 12:0 am</span>
                           </div>
                           <div>
                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie finibus lacinia. Sed id mollis ligula. Pellentesque ut facilisis lacus. Aenean tempus ipsum in mauris ultrices, in mattis metus vestibulum. Sed orci sem, lobortis at lorem in, volutpat gravida ante.</p>
                           </div>
                           <div><img src="{{ asset('public/img/stars.png') }}"></div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div> -->
          </div>
          <!-- <div class="row">
            <div class="col-lg-12 our-properties">
              <div class="box-heads">
                <h4>Our Properties</h4>
              </div>
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 our-propert-list">
                  <div class="media">
                        <div class="media-left">
                          <a href="#" class="our-prts">
                            <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                              <span class="property-name">skyline apartments</span>
                            </div>
                            <div><img src="{{ asset('public/img/stars.png') }}"></div>
                            <div class="property-location">USA</div>
                            <div><i class="fa fa-dollar"></i>100</div>
                        </div>
                      </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 our-propert-list">
                  <div class="media">
                        <div class="media-left">
                          <a href="#" class="our-prts">
                            <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                              <span class="property-name">skyline apartments</span>
                            </div>
                            <div><img src="{{ asset('public/img/stars.png') }}"></div>
                            <div class="property-location">USA</div>
                            <div><i class="fa fa-dollar"></i>100</div>
                        </div>
                      </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 our-propert-list">
                  <div class="media">
                        <div class="media-left">
                          <a href="#" class="our-prts">
                            <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                              <span class="property-name">skyline apartments</span>
                            </div>
                            <div><img src="{{ asset('public/img/stars.png') }}"></div>
                            <div class="property-location">USA</div>
                            <div><i class="fa fa-dollar"></i>100</div>
                        </div>
                      </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 our-propert-list">
                  <div class="media">
                        <div class="media-left">
                          <a href="#" class="our-prts">
                            <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                              <span class="property-name">skyline apartments</span>
                            </div>
                            <div><img src="{{ asset('public/img/stars.png') }}"></div>
                            <div class="property-location">USA</div>
                            <div><i class="fa fa-dollar"></i>100</div>
                        </div>
                      </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 our-propert-list">
                  <div class="media">
                        <div class="media-left">
                          <a href="#" class="our-prts">
                            <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                              <span class="property-name">skyline apartments</span>
                            </div>
                            <div><img src="{{ asset('public/img/stars.png') }}"></div>
                            <div class="property-location">USA</div>
                            <div><i class="fa fa-dollar"></i>100</div>
                        </div>
                      </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 our-propert-list">
                  <div class="media">
                        <div class="media-left">
                          <a href="#" class="our-prts">
                            <img class="media-object" src="{{ asset('public/img/profile-image.jpg') }}" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading">
                              <span class="property-name">skyline apartments</span>
                            </div>
                            <div><img src="{{ asset('public/img/stars.png') }}"></div>
                            <div class="property-location">USA</div>
                            <div><i class="fa fa-dollar"></i>100</div>
                        </div>
                      </div>
                </div>
              </div>
            </div>
          </div> -->
            <!-- ./col -->
          </div><!-- /.row -->

          <div>
          
        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
