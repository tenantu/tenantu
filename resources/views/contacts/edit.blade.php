@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($contactUser,['method' => 'POST','route'=>['contacts.update',$contactUser->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('Name', 'Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('Message', 'Message:') !!}<span style="color: red;"></span>
            {!! Form::textarea('message',null,['class'=>'form-control']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection


