@extends('layouts.tenant-inner')
@section('tenant-inner-content')
<!--<div class="content-wrapper"> -->
	<!-- Content Header (Page header) -->
	
	<!-- Main content -->
	<section class="content">
		<div class="payments">
			<h2>My Payment Settings</h2>
			<div class="row">
				<p>Below you will need to input your credentials till the blank field requirements are satisfied. Upon completion, please hit the “Save” button to finalize the transaction.</p>
			</div>
			<div class="row">
				<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
					   @if(Session::has('displayBank'))
					 	  @include('elements.validation-message')
			           @endif      
			           @if (Session::has('message-success') && Session::has('successBank'))
			              @include('elements.message-success')
			           @endif
			           @if (Session::has('message-error') && Session::has('failureBank'))
			              @include('elements.message-error')
			           @endif
	          
					<h3><i class="fa fa-money"></i> Bank Account and Billing Address</h3>
					{!! Form::model('',['method' => 'GET','class'=>'edit-property-details row','id'=>'tenantAddForm','route'=>['tenants.saveTenantCustomer']]) !!}
					<div class="form-group col-sm-12 col-xs-12">
              			{!! Form::label('bank_name', 'Bank:') !!}<span style="color: red;">*</span>
              			{!! Form::text('bank_name', isset($paymentSettings->bank_name) ? $paymentSettings->bank_name:"", ['class'=>'form-control','placeholder'=>'Bank Name']) !!}
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                    	{!! Form::label('account_type', 'Account Type:') !!}<span style="color: red;">*</span>
                    	{!! Form::select('account_type', ['C' => 'Checking', 'S' => 'Savings'], (isset($paymentSettings->account_type) &&  ($paymentSettings->account_type != "") && ($paymentSettings->account_type == "savings")) ? 'S':'C', ['class'=>'form-control','placeholder'=>'Account Type']) !!}                
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        {!! Form::label('account_holder', 'Account Holder:') !!}<span style="color: red;">*</span>
                        {!! Form::text('account_holder', isset($paymentSettings->account_holder) ? $paymentSettings->account_holder:"", ['class'=>'form-control','placeholder'=>'Account Holder']) !!}
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
						{!! Form::label('account_no', 'Account Number:') !!}<span style="color: red;">*</span>
              			{!! Form::text('account_no', isset($paymentSettings->account_number) ? $paymentSettings->account_number:"", ['class'=>'form-control','placeholder'=>'Account Number']) !!}
					</div>
					<div class="form-group col-sm-12 col-xs-12">
              			{!! Form::label('routing_no', 'Routing Number:') !!}<span style="color: red;">*</span>
              			{!! Form::text('routing_no', isset($paymentSettings->routing_number) ? $paymentSettings->routing_number:"", ['class'=>'form-control','placeholder'=>'Routing Number']) !!}
					</div>

					<div class="form-group col-sm-12 col-xs-12">
              			{!! Form::label('st_line1', 'Address Line 1:') !!}<span style="color: red;">*</span>
              			{!! Form::text('st_line1', (isset($paymentSettings->street_line1) && ($paymentSettings->street_line1!="")) ? $paymentSettings->street_line1:"", ['class'=>'form-control','placeholder'=>'Street address, P.O. box, etc']) !!}
					</div>
					<div class="form-group col-sm-12">
			            {!! Form::label('st_line2', 'Address Line 2 (Optional):') !!}<span style="color: red;"></span>
			            {!! Form::text('st_line2',(isset($paymentSettings->street_line2) && ($paymentSettings->street_line2!="")) ? $paymentSettings->street_line2:"", ['class'=>'form-control','placeholder'=>'Apartment, suite, unit, building, floor, etc.']) !!}
					 </div>			
					<div class="form-group col-sm-12">
	              		{!! Form::label('city', 'City/Locality:') !!}<span style="color: red;">*</span>
	              		{!! Form::text('city', (isset($paymentSettings->city) && ($paymentSettings->city!="")) ? $paymentSettings->city : "", ['class'=>'form-control','placeholder'=>'City']) !!}
					</div>
					<div class="form-group col-sm-12">
	              		{!! Form::label('state', 'State/Region:') !!}<span style="color: red;">*</span>
	              		{!! Form::text('state', (isset($paymentSettings->state) && ($paymentSettings->state!="")) ? $paymentSettings->state : "", ['class'=>'form-control','placeholder'=>'State']) !!}
					</div>
					<div class="form-group col-sm-12">
	              		{!! Form::label('zipCode', 'Zip code:') !!}<span style="color: red;">*</span>
	              		{!! Form::text('zip_code', (isset($paymentSettings->zip_code) && ($paymentSettings->zip_code!="")) ? $paymentSettings->zip_code : "", ['class'=>'form-control','placeholder'=>'Zip code']) !!}
					</div>
					<div class="form-group col-sm-12 col-xs-12">
              			{!! Form::label('auto_payrent', 'Auto-Pay Rent:') !!}<span style="color: red;"></span>
              			{!! Form::checkbox('auto_payrent', 1,((isset($paymentSettings->auto_pay) && ($paymentSettings->auto_pay) === '1') ? true : false)) !!}
            		</div>
						<div class="form-group">
							{!! Form::submit('Submit', ['class' => 'btn save-btn']) !!}
						</div>
					{!! Form::close() !!}
				</div>
				</div>
			</div>
		</div>
	</section>
	<!--</div> --> <!-- /.content-wrapper -->
	@endsection
