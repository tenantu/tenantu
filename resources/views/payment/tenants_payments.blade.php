@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	@include('elements.breadcrump-landlord')
	@if (Session::has('message-success'))
	@include('elements.message-success')
	@endif

	@include('elements.validation-message')
	<section class="content">
		<!-- common head -->
		@if(count($monthlyrentDetails) > 0 )
		<div class="row">
			{!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm','route'=>['payment.getTenantPaymentPostTenantId'],'files'=>true]) !!}
			<div class="col-sm-6 col-xs-12">
				<div class="select-box">
					{!! Form::select('tenant_id',['' => 'Select Tenant']+$tenantDropdown, $tenantId, ['class'=>'form-control login-input','id'=>'tenantId','required']) !!}

				</div>
			</div>
			{!! Form::close(); !!}
			{{-- <select onChange="window.location.href='{{route('payment.getTenantPaymentPostTenantId',[$yearMonthTime])}}' + '/' +this.value;" class="form-control login-input">
                <option>
                    Select A Tenant
                </option>
                @foreach($tenantDropdown as $key => $name)
                        <option value="{{$key}}">
                                {{$name}}
                        </option>
                @endforeach
            </select> --}}
			<div class="col-sm-6 col-xs-12">
				<div class="navigation-buttons">
					<a href="{{route('payment.getTenantPayment',[$prevMonth,$tenantId])}}"><i class="fa fa fa-chevron-left"></i><span>Prev</span></a>
					<a href="{{route('payment.getTenantPayment',[$nextMonth,$tenantId])}}"><span>Next</span><i class="fa fa-chevron-right"></i></a>
				</div>
			</div>
		</div>
		<!-- ends here -->
		<!-- repeat each tenant -->

		@foreach( $monthlyrentDetails as $monthlyrentDetail )

		<div class="tnt-box payment-details">
			<div class="row">
				<div class=" col-sm-4 col-xs-12 tenant-name">{{ $monthlyrentDetail['name'] }}</div>
				<div class="col-sm-5 col-xs-12 property-dtls"><span class="property-name">{{ isset($monthlyrentDetail['payment'][0]['property_name']) ? $monthlyrentDetail['payment'][0]['property_name']:"" }}</span></div>
			</div>
			<div class="table-responsive">
				<table border="0" class="table table-hover">
					<thead class="payment-details-head">
						<tr>
							<td>Item</td>
							<td>Amount</td>
							<td>Due Date</td>
							<td>Status</td>
							<td></td>

						</tr>
					</thead>
					<tbody class="payment-details-body">

						@if(!empty($monthlyrentDetail['payment']))
						@foreach($monthlyrentDetail['payment'] as $payment)
						<tr>

							<td>{{ $payment['title'] }}</td>
							<td>{{ '$ '.$payment['amount']}}</td>
							<td>{{date('d-m-Y',strtotime($payment['bill_on']))}}</td>
							@if( $payment['status']==0)
							<td class="pending pendPay{{ $payment['id'] }}">Pending</td>
							@endif
							@if($payment['status']==1)
							<td class="paid"><i class="fa fa-check-circle"></i>Paid</td>
							@endif

							<td class="payButton{{ $payment['id'] }}">
								@if( $payment['status']==0)
								<a href="#" class="payment-date payNow" data-toggle="modal" data-target=".bs-example-modal-sm" data-pkId="{{ $payment['id'] }}" data-flag="{{ $payment['flag'] }}">Pay</a>
								@endif
							</td>
						</tr>

						@endforeach
						@endif
					</tbody>
				</table>
			</div>
		</div>
		@endforeach
		<!-- ends here -->

		<!-- modal popup -->
		<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Payments</h4>
      </div>
      {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm']) !!}
      <div class="modal-body payment-date-cntr">
      <!-- {!! Form::text('expireon', Input::old('expireon'), ['class'=>'form-control login-input','id'=>'expireon','placeholder'=>date('Y-m-d'),'data-provide'=>'datepicker','required','required' ]) !!} -->

        {!! Form::label('payment_date', 'Payment date:') !!}
        {!! Form::text('payment_date', date('m/d/Y'), ['class'=>'form-control login-input','id'=>'paymentDate','data-provide'=>'datepicker','required' ]) !!}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="updatePayment" data-pkId="" data-flag="">Save changes</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
@else
<div class="col-lg-12 col-md-8 col-sm-8 col-xs-10" align="center">
	<div class="no-contacts text-center"> <img src="{{asset('public/img/no-payments.png')}}"> </div>
</div>
@endif
<!-- modal popup ends -->
	</section>
</div>
@endsection
@section('page-scripts')
<script type="text/javascript">
	if( window.location.hostname == 'localhost'){
		var baseUrl = 'http://localhost/tenantu/';
    }else if( window.location.hostname == 'fodof.net') {
		var baseUrl = 'http://fodof.net/tenantu/';
	}
	else if( window.location.hostname == 'tenantu.com') {
		var baseUrl = 'http://tenantu.com/';
	}else {
		//alert('please set base url in blueimp/js/main.js');
		baseUrl="172.16.117.128/";
	}
	//var date = "{{ $nextMonth }}";
	//alert(date);
	$(document).on('change','#tenantId',function(){
		var selectedTenantId = $(this).val();
		window.location.href="{{ route('payment.getTenantPaymentPostTenantId',[$yearMonthTime]) }}"+"/"+selectedTenantId;
	})
	$(document).on('click','.payNow',function(){
		var pkId = $(this).data('pkid');
		var flag = $(this).data('flag');
		$('#updatePayment').attr('data-pkid',pkId);
		$('#updatePayment').attr('data-flag',flag);
	});
	$(document).on('click','#updatePayment',function(){
		var pkId  		= $(this).attr('data-pkId');
		var paymentDate = $('#paymentDate').val();
		var flag 		= $(this).attr('data-flag');
	     $.ajax({
          url: baseUrl+ 'payment/updatePayment',
          type: "post",
          data :{'paymentId': pkId,'date':paymentDate, 'flag': flag},
          success: function(data){
              if(data==1){
              	$('.pendPay'+pkId).html('<i class="fa fa-check-circle"></i>Paid');
              	$('.pendPay'+pkId).removeClass('pending').addClass('paid');
              	$('.payButton'+pkId).hide();
              	$('#myModal').modal('hide');
              }
              /*$('#showMsg').html("<span>Removed Successfully</span>");
              $('#showMsg span').fadeOut( 4000 );*/
            }
        });
	});

</script>
@endsection
