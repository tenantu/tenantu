@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@include('elements.validation-message')
<section class="content tenant-contacts">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="navigation-buttons">
				<a href="{{route('payment.getMyPayment',$prevMonth)}}"><i class="fa fa fa-chevron-left"></i><span>Prev month</span></a> {{$currentMonthDisplay}}
				<a href="{{route('payment.getMyPayment',$nextMonth)}}"><span>Next month</span><i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
	@if((count($monthlyrentDetails) > 0 ) )
	<div class="tnt-box payment-details">
		<div class="table-responsive">
			<table border="0" class="table table-hover">
				<thead class="payment-details-head">
					<tr>
						<td>Item</td>
						<td>Amount</td>
						<td>Due Date</td>
						<td>Status</td>
						<td>Paid Date</td>
					</tr>
				</thead>
				<tbody class="payment-details-body">

					@forelse($monthlyrentDetails as $monthlyrentDetail)
					<tr>

						<td>{{$monthlyrentDetail->title}}</td>
						<td>{{$monthlyrentDetail->amount}}</td>
						<td>{{date('m/d/Y',strtotime($monthlyrentDetail->bill_on))}}</td>
						{!!HTML::getPaynowButton($monthlyrentDetail->status,$monthlyrentDetail->bill_on, $monthlyrentDetail->unique_id,"month", $monthlyrentDetail->landlord_id, $monthlyrentDetail->tenant_id)!!}

						<td>{!! HTML::getPaymentDate($monthlyrentDetail->status,$monthlyrentDetail->payment_date)  !!}</td>
					</tr>
					@empty
					<td> You have yet to make a payment! </td>
					@endforelse

				</tbody>
			</table>

		</div>
	</div>
	@else
	<div class="col-lg-12 col-md-8 col-sm-8 col-xs-10" align="center">
		<div class="no-contacts text-center"> <img src="{{asset('public/img/no-payments.png')}}"> </div>
    </div>
	@endif
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<script type="text/javascript">

function alertLateRent(uniqueId)
{
	var rentIsLate = '<?php echo $lateRent?>';
	confirmPay(uniqueId);

	if (rentIsLate == 1){
		bootbox.alert("Your rent is late. A late fee, specified by your landlord, has been added to this month's rent.");
	}
}
alertLateRent();
function alertError(){
	bootbox.alert("Set your payment settings!");
}
// This will throw if current landlord loc id is different from tenant's currently stored landlord loc id
function differenceError()
{
	bootbox.alert("You changed landlords, please re-save your payment settings!", function(){ console.log('Re-save your payment settings!')});
}

function confirmPay(uniqueId)
{
	bootbox.confirm({
		message: "A nominal service fee of $1.25 will accompany your transaction. Do you want to confirm this month's rent payment?",
		buttons:{
			confirm:{
				label: 'Yes',
				className: 'btn-success'
			},
			cancel:{
				label: 'No',
				className: 'btn-danger'
			}
		},
		callback: function(result) {
			if(result === true)
				{
					var url = "{{ route('payment.paynowmonthlyrent', ':uniqueId') }}";
					url = url.replace(':uniqueId', uniqueId);
					console.log(uniqueId);
					window.location.href = url;
					console.log('This was picked: ' + result);
				}
				else
				{
					console.log('nope');
				}
		}
	});
}

</script>

<!--<style>
.modal-footer {background:#c00;}
</style>-->
@endsection
