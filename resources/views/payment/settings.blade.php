@extends('layouts.landlord-inner')
@section('inner-content')

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	@if (Session::has('message-success'))
	 @include('elements.message-success')
	@endif
	@if (Session::has('message'))
	 @include('elements.message')
	@endif
	<!-- Main content -->
	<section class="content">
		@include('elements.breadcrump-landlord')
		<div class="payments">

		<table class="table table-striped">
			{!! Form::model($paymentSettings, ['route' => 'payments.postsettings']) !!}
			<tbody>
				<tr>
					<th scope="row">{!! Form::label('org_id', 'Organization Id: ') !!}<span style="color: red;">*</span></th>
					<td> {!! Form::text('org_id') !!} </td>
				</tr>
				<tr>
					<th scope="row">{!! Form::label('late_fee_percent', 'Late fee % for late rent payments: ') !!}</th>
					<td> {!! Form::text('late_fee_percent') !!}%</td>
				</tr>
				<tr>
					<th scope="row">{!! Form::label('schedule_frequency', 'How often per year is rent collected? ') !!}</th>
					<td>{!! Form::select('schedule_frequency', ['0' => 'Monthly', '1' => 'Bi-Monthly', '2' => 'Quarterly', '3' => 'Semi-Annual'], ['class'=>'form-control','placeholder'=>'Collection']) !!}</td>
				</tr>
				<tr>
					<th scope="row">{!! Form::label('rent_due_day_of_month', 'What day of the month is rent due to you? ') !!}</th>
					<td>{!! Form::text('rent_due_day_of_month') !!}</td>
				</tr>
				<tr>
					<th scope="row">{!! Form::label('late_grace_period_day', 'How many days is the grace period for tenants to submit late rent without a late fee? ') !!}</th>
					<td>{!! Form::text('late_grace_period_day') !!}</td>
				</tr>

			</tbody>
		</table>
		{!! Form::submit('Submit', ['class' => 'btn save-btn']) !!}

			{!! Form::close() !!}
			<div class="row">
			<div class="form-group col-lg-9">
			<br>
				<span style="color: red;">*</span>After registering your initial bank account, you will receive an email which will provide an <b>Organization ID </b> and a <b>Location Id</b>. <br>
				You will be assigned one <b>Organization ID </b> which will be linked to you. This value will be entered above. <br>
				<b>Location Ids</b> are entered under <i>Bank Accounts</i> in the <i>Portfolio</i> tab.

			</div>
			</div>
		</div>
	</section>
	</div><!-- /.content-wrapper -->
	@endsection
