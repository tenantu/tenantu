@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
 @include('elements.message-success')
@endif
@include('elements.validation-message')
<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
              <!-- Poist property -->
              <div class="tnt-box add-propety">
                <div class="box-heads">
                  <h4>Search Property</h4>
                </div>
                {!! Form::open(['class'=>'add-property-details','route'=>['index.searchResult'],'files'=>true]) !!}

                  <div class="row">
                    <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                      <label for="propertytitle">School Name</label>
                      <div class="select-box">
                      {!! Form::select('school_id',['' => 'Please Select']+$schools,$savedSearchDetails['school_id'], ['class'=>'form-control','id'=>'propertytitle']) !!}
                      </div>
                    </div>
                     <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                      {!! Form::label('aminities', 'Amenities:') !!}



					  {!! Form::select('aminity[]',$aminities,$savedSearchDetails['aminities'], ['class'=>'form-control','id'=>'aminity','multiple']) !!}
                    </div>

                    <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <label for="propertytitle">Bedrooms</label>
                      {!!Form::select('bedroomId',['any' => 'Any', 1=>1, 2=>2, 3=>3, 4=>4, 5=>5, 6=>6, 7=>7,8=>8],$savedSearchDetails['bedroomId'],['class' => 'form-control', 'id' => 'propertytitle'])!!}

                    </div>
                    </div>

                    <div class="row">
                    <div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-12">
                      <label for="propertytitle">Min. price($)</label>
                      {!! Form::text('priceFrom', 0, ['class'=>'form-control range-texts distance-input','id'=>'priceFrom','placeholder'=>'Price From','readonly']) !!}

                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-5 col-xs-12">
                      <label for="propertytitle">Max. price($)</label>
                      {!! Form::text('priceTo', 10000, ['class'=>'form-control range-texts distance-input','id'=>'priceTo','placeholder'=>'Price To','readonly']) !!}
                    </div>
                    <div class="form-group col-lg-10 col-md-10 col-sm-10 col-xs-12">
						                <div id="slider-handles4"></div>
                    </div>

                    <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                      <label for="propertytitle">Search</label>
                      {!! Form::text('search', $savedSearchDetails['search'], ['class'=>'form-control','placeholder'=>'Search']) !!}
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="btn add-button">Search</button>
                    </div>
                  </div>
                {!! Form::close() !!}
              </div>
            </div><!-- ./col -->
            </div><!-- ./col -->
          </div><!-- /.row -->

        </section>
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
	<link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
	<script src="{{ asset('public/js/jquery.nouislider.min.js') }}"></script>
	<script src="{{ asset('public/js/wNumb.js') }}"></script>
	<script src="{{ asset('public/js/jquery.liblink.js') }}"></script>


	<script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>

		<script>

			var data="{{ $savedSearchDetails['aminities'] }}";
			var dataarray=data.split(",");
			$("#aminity").val(dataarray);
			//$("#aminity").multiselect("refresh");



		if( window.location.hostname == 'localhost'){
		  var baseUrl = 'http://localhost/tenantudev/';
		}else if( window.location.hostname == 'fodof.net') {
		  var baseUrl = 'http://fodof.net/tenantu/';
		 }
		var jqxhr='';
		var skills = [ "{{ $savedSearchDetails['aminities'] }}" ];
		var currentlyValidTags = [];

		$('select[multiple]').multiselect({
			columns: 1,
			placeholder: 'Select options'
		});

	//$('#slider-non-linear-step').Link('lower').to($('#slider-non-linear-step-value'));
	for ( var i = -20; i <= 40; i++ ){
		$('#input-number').append(
			'<option value="'+i+'">'+i+'</option>'
		);
    }


	$("#slider-handles4").noUiSlider({
		start: [0, 10000],
		step: 10,
		connect:  true,
		range: {
			'min': [ 0 ],
			'max': [ 10000 ]
		},
		format: wNumb({
			decimals: 0
		})
	});

$('#slider-handles4').Link('lower').to($('#priceFrom'));
$('#slider-handles4').Link('upper').to($('#priceTo'));

    </script>
@endsection
