@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model('',['method' => 'POST','route'=>['schools.store'],'files'=>true]) !!}
	{!! Form::hidden('hdPlace', '', ['id'=>'hdPlace']) !!}
        <div class="form-group">
            {!! Form::label('SchoolName', 'SchoolName:') !!}<span style="color: red;">*</span>
            {!! Form::text('schoolname', Input::old('schoolname'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Location', 'Location:') !!}<span style="color: red;">*</span>
            {!! Form::text('location', Input::old('location'), ['class'=>'form-control','id'=>'location']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Address', 'Address:') !!}<span style="color: red;"></span>
            {!! Form::textarea('address',Input::old('address'),['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('is_active', 'Status:') !!}
            {!! Form::checkbox('is_active', 1, Input::old('is_active'), ['class'=>'']) !!}
        </div>
        <div class="form-group">
           {!! Form::label('image', 'Image:') !!} (image size should be 1366px width and 721px height)
           {!! Form::file('image') !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
<link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css"> 
<link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css"> 
 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
<script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script> 
<script type="text/javascript">
	$("#location").tagit({
        allowSpaces: true,
        fieldName: "location[]",
        tagLimit: 1,
        autocomplete: {
          delay: 0,
          minLength: 2,
          source: function(request, response) {
            var callback = function (predictions, status) {
              if (status != google.maps.places.PlacesServiceStatus.OK) {
                return;
              }         
              var data = $.map(predictions, function(item) {
                return item.description;
              });
              response(data);
            }   
            var service = new google.maps.places.AutocompleteService();
            service.getQueryPredictions({ input: request.term }, callback);
          }
        },
        beforeTagAdded: function (event, ui) {
                        getGeoLocations(ui.tagLabel,false);
                    },
                    beforeTagRemoved: function (event, ui) {
                        // do something special
                        console.log(ui.tagLabel);
                        getGeoLocations(ui.tagLabel,true);
                    }
      });
      
      $("#courseLocation").tagit({
        fieldName: "location[]"
      });
      
      function getGeoLocations(address, remove) {

                    if (address === '') {
                        return false;
                    }

                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var location = results[0].geometry.location.lat() + '~' + results[0].geometry.location.lng()
                            var storedLocation = $("#hdPlace").val();
                            if (remove !== true) {
                                if (storedLocation === '') {
                                    $("#hdPlace").val(location);
                                } else {
                                    $("#hdPlace").val(storedLocation + ',' + location);
                                }
                            }else{
                                
                                storedLocation = storedLocation.replace(location,'');
                                storedLocation = storedLocation.replace(',,',',');
                                $("#hdPlace").val(storedLocation)
                            }

                        }

                    });
                    return false;
                }
</script>
@endsection
