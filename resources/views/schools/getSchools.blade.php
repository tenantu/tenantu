@extends('layouts.frontend-master')
@section('content')
{!! Form::open(['class'=>'getSchools','files'=>true,'method'=>'get','route'=>'index.searchResult']) !!}
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="schools-container">
				<h3>Schools</h3>
				<table class="table">
					@foreach($schools as $school)
					<tr>
						<td class="school-name"><div>{{$school->schoolname}}</div> <div class="school-location"> {{$school->address}}</div></td>
						<td><a style="cursor:pointer;" class="property_view" data-sId="{{$school->id}}">View Properties({{$school->propertyCount}})</a></td>
					</tr>
					@endforeach
					{!! Form::hidden('school_id', '',['id'=>'school_id']) !!}
				</table>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".property_view").click(function(){
		var sId = $(this).attr('data-sId');
		$("#school_id").val(sId);
		$( ".getSchools" ).submit();
	});
});
</script>
@endsection
