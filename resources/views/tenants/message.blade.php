 @extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
		@include('elements.validation-message')
 <section class="content tenant-message-cntr">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="contacts">
				  
                <div class="user-image"><img src="{{HTML::getLandlordProfileImage($landlordId)}}"></div>
                <div class="user-name">{{$landlordName}}</div>
              </div> <!-- contact Heading -->
            </div>
          </div>
          <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 contacts-message">

                <!-- ================================== -->
                <!-- message contacts for desktop starts-->
                <!-- ================================== -->
                <div class="cnts">
                  <div class="create-new-msg">
                      <a href="{{route('tenants.getMessage',$landlordId)}}"><i class="fa fa-plus-square-o"></i> New Message</a>
                  </div>
                  <ul class="contact-list">
					  
					  @forelse($messageThreadsObj as $Thread)
					  <li>   
						  <a href="{{route('tenants.getMessageList',['messageThreadId'=>$Thread->id])}}" class="user {{HTML::getThreadMenuClass($messageThreadId,$Thread->id)}}">
						 
							<div class="user-image icons"><i class="{{ $Thread->image }}"></i></div>
							<div class="user-details">
							  <h4>{{ $Thread->thread_name }} </h4>
							  <div class="msg-date">{!! $Thread->property_name !!}</div>

								@if(isset($Thread->created_at))

									<div class="msg-date">{{ $threadDate = date('d M h:i:s a',strtotime($Thread->created_at)) }} </div>

								@endif
							</div>
						  </a>
                      </li>
                      @empty
						<p></p>
					  @endforelse




                    
                  </ul>
                  </div>
                <!-- message contacts for desktop end-->
                <!-- ================================== -->
                </div>
                @if($messageFlag=='compose')
					         @include('tenants.message-compose')
				        @else
					         @include('tenants.messagelist')
				        @endif
                <!-- /.col -->
              </div><!-- /.row -->
          <!-- Main row -->

        </section>
        
    
		<script>
			$(function() {
				 $(".direct-chat-messages").animate({ scrollTop: $('.direct-chat-messages').prop("scrollHeight")}, 1000);
		    });
		    
		    $(document).ready(function(){
				var newArray  = [];
				var threadWithFlag = '<?php echo $threadWithFlag; ?>';
				var jsonNewData = JSON.parse(threadWithFlag);
				$.each(jsonNewData.threads,function(i,v){
					newArray.push(parseInt(v));
					//array[v.id] = v.type;
				});
				
				if($("#messageTypeDropDown").val() == '')
				{ 
					$("#durationTime").hide();
				}
				else
				{
					$("#durationTime").show();
				}
				
					$('#messageTypeDropDown').change(function(){
						
						if( $.inArray( parseInt($(this).val()), newArray) != -1 ){
						$("#durationTime").show();
					}
					else{
						$("#durationTime").hide();
					}
					
				});
			});
       
        </script>


        
        @endsection
        
