@if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            
            {{ Session::get('message') }}
        </div>
    @endif
	

@if (count($errors) > 0)
	<div class="alert alert-danger">
		{{$errors}}
	</div>
@endif
	{!! Form::model('',['method' => 'POST','route'=>['tenants.postLogin']]) !!}

    <div>
        {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
        {!! Form::text('email', Input::old('name'), ['class'=>'form-control']) !!}
    </div>

    <div>
         {!! Form::label('Email', 'Password:') !!}<span style="color: red;">*</span>
         {!! Form::password('password',['class'=>'form-control']) !!}
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>
    <div>
        <a href="{{ URL::to('tenants/register') }}">Register</a>
    </div>
    <div>
        <a href="{{ URL::to('tenants/forgotpassword') }}">Forgot Your Password?</a>
    </div>
    

    <div>
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}

<script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                  ga('create', 'UA-78780279-1', 'auto');
                  ga('send', 'pageview');

                </script>
