@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
@if (Session::has('warningMessage'))
   <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
@endif
<section class="content">
  <div class="modal fade bs-example-modal-md" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h3 class="myLargeModalLabel">Hey there!</h3>
          </div>

          <div class="modal-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                 <p>
                   We noticed you haven't filled out your application yet. Tenants with a completed application have a higher chance of
                   being approved for the property they want.
                 </p>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-primary" type="button" onclick="window.location='{{ route("tenants.myApplication") }}'">Go to my application</button>
          </div>
        </div>
    </div>
  </div>
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
              <!-- recent properties -->
              <div class="tnt-box rcnt-prts">
                <div class="box-heads">
                  <h4>Recent messages</h4>

                </div>
                <ol>
				 @forelse($generalMessages as $key => $message)
				  <li>
					  <span class="number">{{$key+1}}.</span>
					  <strong>{{$message->thread_name}}: </strong>
					  @if(strlen($message->message) > 80 )
					   {{substr($message->message,0,80).'...'}}
					  @else
					   {{ $message->message }}
					  @endif
					  <a href="{{route('tenants.getMessageList',Crypt::encrypt($message->message_thread_id))}}" class="pull-right">Details</a>
				  </li>
				  @empty
					<p>No messages</p>

				 @endforelse

                </ol>
              </div>
              @if($isTenantAssociatedWithLandlord == 1)
              <div class="tnt-box rcnt-prts">
                <div class="box-heads">
                  <h4>Issues reported</h4>

                </div>
                <ol>
				 @forelse($issueMessages as $key => $message)
				  <li>
					  <span class="number">{{$key+1}}.</span>
					  <strong>{{$message->thread_name}}: </strong>
					  @if(strlen($message->message) > 80 )
					   {{substr($message->message,0,80).'...'}}
					  @else
					   {{ $message->message }}
					  @endif
					  <a href="{{route('tenants.getMessageList',$message->message_thread_id)}}" class="pull-right">Details</a>
				  </li>
				  @empty
					<p>No messages</p>

				 @endforelse

                </ol>
              </div>
              @endif
            </div><!-- ./col -->
           <!-- ./col -->
            <!-- ./col -->
            <!-- ./col -->

<!-- START UNCOMMENTING HERE FOR PAYMENT -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <!-- small box -->
              <div class="tnt-box">
                <div>
                  <div class="box-heads">
      <!--              <h4>My Payments</h4> -->
                  </div>
                  <div class="payment-links"><i class="fa fa-dollar"></i><a href="{{route('payment.getMyPayment')}}">Payment</a></div>
                </div>
              </div>
            </div><!-- ./col -->
            <!-- ./col -->
          </div>
          <!-- Main row -->
          @if(!$tenantInManagementGroup)
            <script type="text/javascript">
              $(window).on('load',function(){
              $('#myModal').modal('show');
            });
            </script>
          @endif
        </section>
@endsection
