@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif

@include('elements.validation-message')

<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <!-- Landlord Profile -->
      <div class="tnt-box tenantu-profile">
        {!! Form::open(['class'=>'edit-tnt-form','route'=>['tenants.postEditProfile'],'files'=>true]) !!}
        
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="profile-image">
              
              <img src="{{ $profileImage }}">
              <a class="change-tnt-profile-img" data-toggle="modal" data-target=".ChangeProfileImg">Change Photo</a>
            </div>
          </div>
        </div>
        <!-- edit landlord  profile input -->
        
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="firstName">First Name *</label>
              {!! Form::text('firstname', $profile->firstname, ['class'=>'form-control input-text','id'=>'firstName','placeholder'=>'First Name']) !!}
              
            </div>
            <div class="form-group">
              <label for="lastName">Last Name *</label>
              {!! Form::text('lastname', $profile->lastname, ['class'=>'form-control input-text','id'=>'lastName','placeholder'=>'Last Name']) !!}
              
            </div>
            
            <div class="form-group">
              <label for="Location">Location</label>
              {!! Form::text('location', $profile->location, ['class'=>'form-control input-text','id'=>'Location','placeholder'=>'Location']) !!}
              
            </div>
            <div class="form-group">
              <label for="Phone">Phone *</label>
              {!! Form::text('phone', $profile->phone, ['class'=>'form-control input-text','id'=>'Phone','placeholder'=>'Phone Number']) !!}
              
            </div>
            <div class="form-group">
              <label for="Location">Gender</label>
              <br />
              
              {!! Form::radio('gender', 'M',($profile->gender == 'M')) !!} Male
              {!! Form::radio('gender', 'F',($profile->gender == 'F')) !!} Female
              
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="aboutus">About Us</label>
              {!! Form::textarea('about', $profile->about, ['class'=>'form-control text-area','placeholder'=>'Say Something','size' => '10x3']) !!}
              
            </div>
            <div class="form-group">
              <label for="aboutus">School *</label>
              
              {!! Form::select('school_id',['' => 'Please Select']+$schools,$profile->school_id, ['class'=>'form-control input-text']) !!}
              
            </div>
            <div class="form-group">
              <label for="website">Website</label>
              {!! Form::text('website', $profile->website, ['class'=>'form-control input-text','id'=>'website','placeholder'=>'http://www.example.com']) !!}
              
            </div>
            <div class="form-group">
              <label for="aboutus">Date of Birth</label>
              @if($profile->dob=='0000-00-00')
              {!! Form::text('dob','', ['class'=>'form-control login-input','id'=>'dob','placeholder'=>date('Y-m-d'),'data-provide'=>'datepicker']) !!}
              @else
              {!! Form::text('dob',date('m/d/Y',strtotime($profile->dob)), ['class'=>'form-control input-text','id'=>'dob','data-provide'=>'datepicker' ]) !!}
              @endif
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box-heads">
              <h4>Other Details</h4>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6 col-cs-12">
            <div class="form-group">
              <label for="contactDetails">Contact Details</label>
              {!! Form::text('address', $profile->address, ['class'=>'form-control input-text','id'=>'contactDetails','placeholder'=>'Address']) !!}
              
            </div>
            <div class="form-group">
              <label for="Place">State</label>
              {!! Form::text('state', $profile->state, ['class'=>'form-control input-text','id'=>'Place','placeholder'=>'State']) !!}
              
            </div>
            
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="form-group">
              <label for="country">Language</label>
              {!! Form::text('language', $profile->language, ['class'=>'form-control input-text','id'=>'country','placeholder'=>'Language']) !!}
              
            </div>
            <div class="form-group">
              <label for="contactDetails">City</label>
              {!! Form::text('city',$profile->city, ['class'=>'form-control input-text','id'=>'city','placeholder'=>'City']) !!}
              
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <button type="submit" class="btn btn-primary save-btn">Save</button>
          </div>
        </div>
        
        {!! Form::close() !!}
        
        {!! Form::open(['class'=>'edit-tnt-form','route'=>['tenants.postEditProfileImage'],'files'=>true]) !!}
        <div class="modal fade bs-example-modal-sm ChangeProfileImg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                <h4 id="mySmallModalLabel" class="modal-title">Upload profile image</h4>
              </div>
              <div class="modal-body">
                {!! Form::file('image', ['id' => 'image']) !!}
                <button type="submit" class="btn btn-primary">OK</button>
              </div>
            </div>
          </div>
        </div>
        {!! Form::close() !!}
        
      </div>
      <!-- ./col -->
      </div><!-- /.row -->
      <div>
        
        
        
        <!-- change profile image -->
      </section>
      @endsection
