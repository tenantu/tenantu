@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@include('elements.validation-message')
<section class="content my-landlord">
	<div class="agreement-details">
		<h2>Agreement Details</h2>
		<div class="agreements">
			<div class="row">
				<div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
					<a class="property-image" href="#"> <img src="{{HTML::getPropertyImage($agreementDetails[0]->property_id,'','','thumb')}}">
				</a>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<h4>{{$agreementDetails[0]->property_name}}</h4>
						<div class="location"><i class="fa fa-map-marker"></i> {{$agreementDetails[0]->propertyLocation}}</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="landlord-details">
							<a class="landlord-image" href="#">
								<img src="{{HTML::getLandlordProfileImage($agreementDetails[0]->landlord_id)}}">
							</a>
							<div class="landlords-name">{{ucfirst($agreementDetails[0]->firstname).' '. ucfirst($agreementDetails[0]->lastname) }}</div>
							<div class="location"><i class="fa fa-map-marker"></i> {{$agreementDetails[0]->location}}</div>
						</div>
				</div>
			</div>
			<div class="row agreement-other-details landlords">
				<div class="col-sm-3 col-xs-12">Agreement from : <span>{{ date("M d Y", strtotime($agreementDetails[0]->agreement_start_date))}}</span></div>
				<div class="col-sm-3 col-xs-12">Agreement to : <span>{{ date('M d Y', strtotime("+".$agreementDetails[0]->cycle, strtotime($agreementDetails[0]->agreement_start_date))) }}</span></div>
			</div>
			<div class="row agreement-other-details">
				<div class="col-lg-11 col-sm-11 col-xs-12">
					<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Title</th>
							<th>Bill Date</th>
							<th>Amount</th>
						</tr>
						</thead>
						<tbody>
						@forelse($monthlyRents as $monthlyRent)
						<tr>
							<td><i class="fa fa-angle-right"></i> {{$monthlyRent->title}}</td>
							<td>{{$monthlyRent->bill_on}}</td>
							<td>${{$monthlyRent->amount}}</td>
						</tr>
						@empty
						<tr>
							<td>No MonthlyRent</td>
						</tr>
						@endforelse
						@forelse($otherPayments as $otherPayment)
						<tr>
							<td><i class="fa fa-angle-right"></i> {{$otherPayment->title}}</td>
							<td>{{$otherPayment->bill_on}}</td>
							<td>${{$otherPayment->otheramount}}</td>
						</tr>
						@empty
						<tr>
							<td>No Other Payments</td>
						</tr>
						@endforelse
						</tbody>
					</table>
					</div>
				</div>
			</div>
			<div class="document-details">
				<h3>Document Details</h3>
				<div class="table-responsive">
					<table class="table">
						<thead>
						<tr>
							<th>Document Title</th>
							<th>FIle Name</th>
						</tr>
						</thead>
						<tbody>
						@forelse($documentDetails as $documentDetail)
						<tr>
							<td><i class="fa fa-angle-right"></i> {{$documentDetail->doc_title}}</td>
							<td><a href="{{ asset('public/uploads/tenantdocs/').'/'.$documentDetail->file_name }}">{{$documentDetail->file_name}}</a></td>
						</tr>@empty
						<tr>
							<td>No Documents</td>
						</tr>
						@endforelse
						</tbody>
					</table>
					</div>
			</div>
		</div>

		
	</div>
</section>
@endsection
