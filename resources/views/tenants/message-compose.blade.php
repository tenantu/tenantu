<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					{!! Form::open(['method' => 'POST','route'=>['tenants.postMessage']]) !!}

                      <div class="create-msg">
                        <h3>New Message</h3>
                        <div class="form-group">

						@if(count($propertyList) > 1)
						  <div class="select-box">
							{!! Form::select('property', $propertyList, $selectedPropertyId,['class' => 'form-control hidden','id'=>'propertyId']) !!}
						  </div>
						@else
							{!! Form::hidden('property', $propertyKey) !!}
						@endif
						</div>

                        <div class="form-group">
							{!! Form::hidden('tenant_id', $profile->id) !!}
							{!! Form::hidden('sender', 'T') !!}
							{!! Form::hidden('landlord_id', $landlordId) !!}
							<div class="select-box">
	                 {!! Form::select('messageType',['' => 'Select message type']+$threads, 7, ['class'=>'form-control hidden','id'=>'messageTypeDropDown']) !!}
              </div>

                        </div>
                         <div class="form-group">
							 {!! Form::text('duration', Input::old('duration'), ['class'=>'form-control hidden','placeholder'=>'Specify a duration','id'=>'durationTime']) !!}
						</div>
                        <div class="form-group">
							{!! Form::textarea('message_content', null, ['class' => 'form-control','placeholder'=>'Say Something']) !!}
                        </div>
                        <div class="form-group">
                          <button class="btn btn-primary pull-right send-message-btn">Send</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                      {!! Form::close() !!}
                    </div>
                  </div>
                <!-- ================================== -->
                <!-- DIRECT CHAT START-->
                <!-- ================================== -->

                </div>
                <script>
					var landlordId = '{{ $landlordId }}';

                $(document).ready( function(){
					$(document).on('change','#propertyId',function(){
						//alert(landlordId);
						var propertyId = $(this).val();
						var url = "{{ route('tenants.getMessage') }}"+"/"+landlordId+"/"+propertyId;
						window.location.href= url;

					});
				});
                </script>
