
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model('',['method' => 'POST','route'=>['tenants.getChangePassword']]) !!}
    <input type="hidden" name="token" value="{{ $token }}">

    <div class="form-group has-feedback">
            {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => 'Current Password']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
       
        <div class="form-group has-feedback">
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'New Password']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password Confirmation']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        
        <div class="row">
            <div class="col-xs-4">
                {!! Form::submit('Change Password', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
{!! Form::close() !!}

