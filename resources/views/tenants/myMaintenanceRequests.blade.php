@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
    @include('elements.message-success')
@endif
@include('elements.validation-message')
@if (session('success'))
  <div class="flash-message">
  <div class="alert alert-success">

  </div>
  </div>
@endif
<!--
<div class="container">
  @foreach ($maintenanceRequests as $maintenanceRequest)
    {{ $maintenanceRequest->issue_type_id}}
  @endforeach
</div>
-->
<section class ="content">

<table class="table table-bordered table-striped table-hover article-list">
    <thead>
        <tr>

            <th class="col-md-2">
	@if ((Request::segment(3) == 'Date') && (Request::segment(4) == 'ASC') )
               <a href="{{ route('tenants.myMaintenanceRequests', ['Date', 'DESC']) }}">Date Submitted ^</a>
        @elseif ((Request::segment(3) == 'Date') && (Request::segment(4) == 'DESC') )
               <a href="{{ route('tenants.myMaintenanceRequests', ['Date', 'ASC']) }}">Date Submitted v</a>
        @else 
	       <a href="{{ route('tenants.myMaintenanceRequests', ['Date', 'DESC']) }}">Date Submitted</a>
	@endif

	    </th>
	    <th class="col-md-2">
	@if ((Request::segment(3) == 'Property') && (Request::segment(4) == 'DESC'))
               <a href="{{ route('tenants.myMaintenanceRequests', ['Property', 'DESC']) }}">Property Name v</a>
        @else
	       <a href="{{ route('tenants.myMaintenanceRequests', ['Property', 'DESC']) }}">Property Name</a>   
	@endif   
	    </th>
            <th class="col-md-2">
	@if ((Request::segment(3) == 'Type') && (Request::segment(4) == 'DESC'))
               <a href="{{ route('tenants.myMaintenanceRequests', ['Type', 'DESC']) }}">Issue Type v</a>
	@else
               <a href="{{ route('tenants.myMaintenanceRequests', ['Type', 'DESC']) }}">Issue Type</a>
        @endif   
	    </th>
             <th class="col-md-2">
	@if ((Request::segment(3) == 'Description') && (Request::segment(4) == 'ASC'))
                <a href="{{ route('tenants.myMaintenanceRequests', ['Description', 'ASC']) }}">Issue Description v</a>
	@else
                <a href="{{ route('tenants.myMaintenanceRequests', ['Description', 'ASC']) }}">Issue Description</a>
        @endif     
	     </th>
             <th class="col-md-2">
	@if ((Request::segment(3) == 'Status') && (Request::segment(4) == 'ASC'))
                 <a href="{{ route('tenants.myMaintenanceRequests', ['Status', 'ASC']) }}">Status v</a>          
	@else
     		 <a href="{{ route('tenants.myMaintenanceRequests', ['Status', 'ASC']) }}">Status</a>
        @endif
             </th>

        </tr>
    </thead>
    <tbody>
    @foreach ($maintenanceRequests as $maintenanceRequest)

        <tr>

            <th scope="row">{{ $maintenanceRequest->date_encountered }}</th>
	    <td>{{ $propertyName }}</td>
            <td>{!! HTML::resolveMaintenanceRequestIssueType($maintenanceRequest->issue_type_id) !!}</td>
            <td>{{$maintenanceRequest->issue_desc}}</td>
            <td> {!! HTML::resolveMaintenanceRequestStatusCode($maintenanceRequest->status) !!} </td>

            {{--<td>{{$maintenanceRequest->status}}</td>--}}

        </tr>

      @endforeach
    </tbody>
  </table>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $maintenanceRequests->render()) !!}
        </div>
    </div>

</section>
@endsection
