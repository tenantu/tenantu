  @extends('layouts.tenant-inner')
  @section('tenant-inner-content')
  @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
    @include('elements.validation-message')
	
	{!! Form::model('',['method' => 'POST','route'=>['tenants.postChangePassword']]) !!}
	 <section class="content tenant-contacts">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-5 col-md-4 col-sm-8 col-xs-12">
              <div class="tnt-box">
                <div class="change-password">
                  <form class="change-password-form">
                    <div class="form-group">
                      <label for="oldPasword">Current Password</label>
                      
                      {!! Form::password('current_password', ['class' => 'form-control','id'=>'oldPasword', 'placeholder' => 'Current Password']) !!}
                      
                    </div>
                    <div class="form-group">
                      <label for="newPassword">Password</label>
                      {!! Form::password('password', ['class' => 'form-control','id'=>'newPassword', 'placeholder' => 'Password']) !!}
                      
                    </div>
                    <div class="form-group">
                      <label for="confirmPassword">Confirm Password</label>
                      {!! Form::password('password_confirmation', ['class' => 'form-control','id'=>'confirmPassword', 'placeholder' => 'Password Confirmation']) !!}
                      
                    </div>
                    {!! Form::submit('Submit', ['class' => 'btn btn-default save-btn']) !!}
                  </form>
                </div>
              </div>
            </div>
        </div><!-- /.row -->

        </section>
	
	
 {!! Form::close() !!}
 @endsection
