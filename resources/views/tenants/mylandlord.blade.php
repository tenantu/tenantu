@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
@if (Session::has('warningMessage'))
   <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
@endif
		@include('elements.validation-message')
	<section class="content my-landlord">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@forelse($tenantLandlords as $tenantLandlord)
                <div class="contact-box">
                    <div class="row landlord-name-cntr">
                        <div class="col-md-1 col-sm-2">
                            <div class="prf-img">
                              <img alt="image" class="img-circle img-responsive" src="{{HTML::getLandlordProfileImage('',$tenantLandlord->image)}}">
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-4">
                          <div class="my-landlord-name">{{ucfirst($tenantLandlord->firstname).' '. ucfirst($tenantLandlord->lastname) }}</div>
                          <div class="my-landlord-location">{{$tenantLandlord->location}}</div>
			  <div id = "messageLandlordButton">
				 <a href="{{route('tenants.getMessage',Crypt::encrypt($tenantLandlord->landlord_id))}}" class="buttons">Message Landlord</a>
			  </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="my-landlord-btn-cntr">
                            <a href="{{route('tenants.getLandlordProfileFromTenant',$tenantLandlord->landlordSlug)}}" class="buttons">Profile</a>
                            <a href="{{route('payment.getMyPayment')}}" class="buttons">Payment information</a>
                            <a href="{{route('tenants.getAgreementDetails',$tenantLandlord->id)}}" class="buttons">Agreement details</a>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
							<div class="col-md-1 col-sm-2">
                            <div class="proprty-image">
                              <img alt="image" class="img-responsive" src="{{HTML::getPropertyImage($tenantLandlord->property_id,'','','thumb')}}">
                            </div>
                        </div>
                          <div class="col-sm-5 col-xs-12">
                            <h4>{{$tenantLandlord->property_name}}</h4>
                            <p><i class="fa fa-map-marker"></i>  {{$tenantLandlord->propertyLocation}}</p>
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <div class="agreement-duration">Agreement From</div>
                            <div>{{ date("d-m-Y", strtotime($tenantLandlord->agreement_start_date))}}</div>


                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <div class="agreement-duration">Agreement To</div>
                            <div>{{ date('d-m-Y', strtotime("+".$tenantLandlord->cycle, strtotime($tenantLandlord->agreement_start_date))) }}</div>
                          </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @empty
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="no-contacts text-center" id="no-landlord" style="display: none"><img src="{{asset('public/img/no-landlords.png')}}"></div>
				</div>
				{!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'messageLandlordForm','route'=>['tenants.messageout'],'files'=>true]) !!}
                        <div class="row">
				<div id="messageOutForm" style="display: none">
                                <div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
                                        <div class="input-group col-sm-5 col-xs-12" id="labelText">
						<h4>Landlord not on TenantU yet? Invite or message them by using the form below.</h4>
                                        </div>
                                        <div class="input-group col-sm-5 col-xs-12" id="tenantName">
                                                <label>Name:<span style="color: red;">*</span></label>
                                                {!!Form::text('name','',['class'=>'form-control','id' => 'test','placeholder'=>"Landlord's Name"]) !!}
                                        </div>
                                        <div class="input-group col-sm-5 col-xs-12" id="tenantEmail">
                                                <label>Email:<span style="color: red;">*</span></label>
                                                {!! Form::text('email','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>"Landlord's Email"]) !!}
                                        </div>
                                        <div class="input-group col-sm-5 col-xs-12" id="tenantEmail">
                                                <label>Message:<span style="color: red;"></span></label>
                                                {!! Form::text('message','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Optional, leave blank to invite your Landlord']) !!}
                                        </div>
					<div class="col-sm-9">
						{!! Form::submit('Submit', ['class' => 'btn add-btn']) !!}
					</div>
                               	 </div>
				 </div>
                       </div>
				{!! Form::close() !!}
			   @endforelse
            </div>
        </div><!-- /.row -->

        </section><!-- /.content -->
        @endsection

<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
// caching a reference to the dependant/conditional content:
var hasLandlord = $('#no-landlord').attr("id");
var messageOutForm = $('#messageOutForm');
if(hasLandlord){
messageOutForm.show();
}
else if(!hasLandlord){
messageOutForm.hide();
}
});
</script>
