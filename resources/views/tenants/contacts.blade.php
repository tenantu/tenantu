@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif

<!--div class="content-wrapper"-->
<section class="content">
	<table class="table my-tenants table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                <th class="col-md-2">
			    <a>Name</a>
			    </th>
                <th class="col-md-2">
                   <a>Profile</a>
                </th>
                <th class="col-md-2">
                   <a>Conversation</a>
                </th>
            </tr>
        </thead>
        <tbody>
        @foreach ($landlordsObj as $landlord)
            <tr>
                <th scope="row">{{ $landlord->lastname.', '.$landlord->firstname }}</th>
                                <td><a href="{{route('tenants.getLandlordProfileFromTenant',$landlord->slug)}}" class="message-buttons">View Profile</a></td>
                <td><a href="{{ route('tenants.getMessageList',$landlord->latestMsgThreadId->message_thread_id) }}" class="message-buttons">View Conversation</a></td>
            </tr>
          @endforeach
        </tbody>
    </table>
</section>
<!--/div--><!-- /.row -->

@endsection
