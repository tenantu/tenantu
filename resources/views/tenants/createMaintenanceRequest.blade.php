@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
    @include('elements.message-success')
@endif
    @include('elements.validation-message')
    @if (session('success'))
      <div class="flash-message">
      <div class="alert alert-success">

      </div>
      </div>
  @endif
<section class = "content">
<link rel="stylesheet" href="../public/css/maintenance-request.css">
<main id="main-maint">

  <h2 id="h2-maint">Maintenance Request Form</h2>
  <input class="input" id="toggle-type"  type="checkbox">
  <label id = "label-type" for="toggle-type">Issue Type</label>
  <div id="expand-maint-type">
    <section id="section-maint">
      <div id="radioIssue">
	{!! Form::model('',['method' => 'POST','class'=>'submit-maintenance-request','id'=>'submitMaintenanceRequestForm','route'=>['tenants.postCreateMaintenanceRequest'],'files'=>true]) !!}
		{!! Form::radio('maintType','0',false,['id' => 'radio']) !!}Plumbing<br>
		{!! Form::radio('maintType','1',false,['id' => 'radio']) !!}Air Conditioning<br>
		{!! Form::radio('maintType','2',false,['id' => 'radio']) !!}Electric<br>
    {!! Form::radio('maintType','3',false,['id' => 'radio']) !!}Other<br>
    </div>
  </section>
  </div>
  <input class="input" id="toggle-desc"  type="checkbox">
  <label id = "label-desc" for="toggle-desc">Issue Description</label>
  <div id="expand-maint-desc">
    <section id="section-maint">
      Issue Description: {!! Form::textarea('issueDescription','',['id' => 'issueText', 'type' => 'text', 'placeholder' => 'Describe the issue.']) !!}<br>
    </section>
  </div>
  <input class="input"id="toggle-enc"  type="checkbox">
  <label id = "label-enc" for="toggle-enc">Date Encountered</label>
  <div id="expand-maint-enc">
    <section id="section-maint">
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
  <div id="dateTimePickerWrapper">
   <div id="datetimepicker2" class="input-append date">
        {!! Form::text('dateTimeEnc','',['id' => 'dateTimeInput', 'data-format' => 'MM/dd/yyyy', 'type' => 'text']) !!}
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.pt-BR.js">
    </script>
    <script type="text/javascript">
        var currentDate = new Date();
          $('#datetimepicker2').datetimepicker({
      language: 'en',
      pick12HourFormat: true,
      pickTime: false,
      endDate: currentDate
      });
    </script>
  </div>
    </section>
  </div>
  <input class="input" id="toggle-date"  type="checkbox">
  <label id = "label-date" for="toggle-date">Dates / Times Available for Maintenance</label>
  <div id="expand-maint-date">
    <section id="section-maint">
  <div id="dateTimePickerWrapper">
   <div id="datetimepicker3" class="input-append date">
        {!! Form::text('dateTimeAvailable1','',['id' => 'dateTimeInput2', 'data-format' => 'MM/dd/yyyy HH:mm PP', 'type' => 'text']) !!}
      <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript">
          $('#datetimepicker3').datetimepicker({
      language: 'en',
      pick12HourFormat: true,
      pickSeconds: false,
      startDate: currentDate
      });
    </script>
  </div>
   <div id="datetimepicker4" class="input-append date">
        {!! Form::text('dateTimeAvailable2','',['id' => 'dateTimeInput3', 'data-format' => 'MM/dd/yyyy HH:mm PP', 'type' => 'text']) !!}
	  <span class="add-on">
        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
      </span>
    </div>
    <script type="text/javascript">
          $('#datetimepicker4').datetimepicker({
      language: 'en',
      pick12HourFormat: true,
      pickSeconds: false,
      startDate: currentDate
      });
    </script>
    </section>
  </div>
  <input class="input" id="toggle-sev"  type="checkbox">
  <label id="label-sev" for="toggle-sev">Severity</label>
  <div id="expand-maint-sev">
    <section id="section-maint">
     <div id="q1">
     Does this provide a hazard to you or your roommates?  {!! Form::radio('q1','Yes',false,['id' => 'radio']) !!}Yes {!! Form::radio('q1','No',false,['id' => 'radio']) !!}No<br>
     </div>
     <div id="q2">
     Does this need to be completed within the next 3 days?  {!! Form::radio('q2','Yes',false,['id' => 'radio']) !!}Yes {!! Form::radio('q2','No',false,['id' => 'radio']) !!}No<br>
	 </div>
     <div id="q3">
     Does this issue seem to be a reoccuring problem?  {!! Form::radio('q3','Yes',false,['id' => 'radio']) !!}Yes {!! Form::radio('q3','No',false,['id' => 'radio']) !!}No<br>
	 </div>
    </section>
  </div>
  <section id="section-maint">

     {!! Form::submit('Submit Maintenance Request', ['class' => 'btn btn-primary save-btn submit-btn']) !!}
  </section>


</main>
   {!! Form::close() !!}
</section>
<script type="text/javascript">
$(document).ready(function(){
var checkOnReload = function(){
    var issueInput = $('#issueText').val();
    var dateWidgetInput = $('#dateTimeInput');
    var dateWidgetInput2 = $('#dateTimeInput2');
    var dateWidgetInput3 = $('#dateTimeInput3');
    if (issueInput != ""){
    $('#label-desc').css('background-color', '#369AFF');
    $('#label-desc').addClass('white');
    }
    if(dateWidgetInput.val() != ""){
    $('#label-enc').css('background-color', '#369AFF');
    $('#label-enc').addClass('white');
    }
    if((dateWidgetInput2.val() != "") && (dateWidgetInput3.val() != "")){
    $('#label-date').css('background-color', '#369AFF');
    $('#label-date').addClass('white');
    }
}
checkOnReload();
$(":radio").change(function() {
if ($('#q1 input[type=radio]:checked')[0] != undefined && $('#q2 input[type=radio]:checked')[0] != undefined && $('#q3 input[type=radio]:checked')[0] != undefined) {
   $('#label-sev').addClass('white');
   $('#label-sev').css('background-color', '#369AFF');
}
if ($('#radioIssue input[type=radio]:checked')[0] != undefined){
   $('#label-type').css('background-color', '#369AFF');
   $('#label-type').addClass('white');
}
}).change();

$('#issueText').on('pageload input propertychange paste', function() {
    var issueInput = $('#issueText').val();
    if (issueInput != ""){
    $('#label-desc').css('background-color', '#369AFF');
    $('#label-desc').addClass('white');
    }
    else if(issueInput == ""){
    $('#label-desc').css('background-color', '#EEE');
    $('#label-desc').removeClass('white');
    }
});

var dateWidgetOne = document.getElementsByClassName("bootstrap-datetimepicker-widget dropdown-menu")[0];
var dateWidgetTwo = document.getElementsByClassName("bootstrap-datetimepicker-widget dropdown-menu")[1];
var dateWidgetThree = document.getElementsByClassName("bootstrap-datetimepicker-widget dropdown-menu")[2];

dateWidgetOne.addEventListener('click', function (event) {
    var dateWidgetInput = $('#dateTimeInput');
    if(dateWidgetInput.val() != ""){
    $('#label-enc').css('background-color', '#369AFF');
    $('#label-enc').addClass('white');
    }
});

dateWidgetTwo.addEventListener('click', function (event) {
    var dateWidgetInput2 = $('#dateTimeInput2');
    var dateWidgetInput3 = $('#dateTimeInput3');
    if((dateWidgetInput2.val() != "") && (dateWidgetInput3.val() != "")){
    $('#label-date').css('background-color', '#369AFF');
    $('#label-date').addClass('white');
    }
});

dateWidgetThree.addEventListener('click', function (event) {
    var dateWidgetInput2 = $('#dateTimeInput2');
    var dateWidgetInput3 = $('#dateTimeInput3');
    if((dateWidgetInput2.val() != "") && (dateWidgetInput3.val() != "")){
    $('#label-date').css('background-color', '#369AFF');
    $('#label-date').addClass('white');
    }
});

$('#dateTimeInput').on('pageload input propertychange paste', function() {
    if($('#dateTimeInput').val() == ""){
    $('#label-enc').css('background-color', '#EEE');
    $('#label-enc').removeClass('white');
    }
});
$('#dateTimeInput2').on('pageload input propertychange paste', function() {
    if(($('#dateTimeInput2').val() == "") || ($('#dateTimeInput3').val() == "")){
    $('#label-date').css('background-color', '#EEE');
    $('#label-date').removeClass('white');
    }
});
$('#dateTimeInput3').on('pageload input propertychange paste', function() {
    if(($('#dateTimeInput2').val() == "") || ($('#dateTimeInput3').val() == "")){
    $('#label-date').css('background-color', '#EEE');
    $('#label-date').removeClass('white');
    }
});

});
</script>

@endsection
