@extends('layouts.frontend-master')

@section('content')
<div class="container inner-contents">
<div class="well">
    <div class="row">
        <div class="col-md-12 text-center inner-heading">
            <h3>Reset Password</h3>
        </div>
    </div>
    <div class="row contact-wrap">
         <div class="col-md-2">&nbsp;</div>
        
        <div class="col-md-8">
        @if (session('status'))                
                    <div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <strong>Success!</strong> {{ session('status') }}.
                    </div>
        @elseif (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
        @endif
            {!! Form::open( ['route' => 'users.postReset'] ) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    {!! Form::email('email1', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                </div>
                <div class="form-group">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                </div>
                <div class="form-group">
                   {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password Confirmation']) !!}
                </div>
                 {!! Form::submit('Reset', ['class' => 'btn btn-primary btn-default col-md-4 pull-right']) !!} 
                <!-- {!! Form::submit('Reset', ['class' => 'btn btn-primary btn-block btn-flat']) !!} -->
             {!! Form::close() !!}
        </div>
    </div>
    <br><br>
</div>
</div>
@endsection