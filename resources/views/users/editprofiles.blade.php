
 <div class="container inner-contents">
    <div class="row">
        <div class="col-md-8 col-sm-9">
            <div class="tabs-wrap">
                <div class="tab-content">

                    {!! Form::model($user,['method' => 'POST','route'=>['users.update',$user->id],'files'=>true]) !!}
                    <div class="form-group">
                        {!! Form::label('Name', 'Name:') !!}<span style="color: red;">*</span>
                        {!! Form::text('name',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
                        {!! Form::text('email',null,['class'=>'form-control']) !!}
                    </div>
                   <div class="form-group">
                        {!! Form::label('Image', 'Image:') !!}
                        {!! Form::file('image', ['class'=>'form-control', 'id' => 'image']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('gender', 'Gender:') !!}
                         Male
                        {!! Form::radio('gender', 'M', null, ['class'=>'iradio_minimal-blue']) !!}
                        Female
                        {!! Form::radio('gender', 'F', null, ['class'=>'iradio_minimal-blue']) !!}
                    </div>
                   <div class="form-group">
                        {!! Form::label('DOB', 'DOB:') !!}<br>
                        {!! Form::selectDays('day',['class'=>'col-md-3']) !!}
                        {!! Form::selectMonths('month',['class'=>'col-md-4']) !!}
                        {!! Form::selectYears('year',['class'=>'col-md-4']) !!}
                    </div>
                     <div class="form-group">
                        {!! Form::label('Location', 'Location:') !!}
                        <ul id="location">
                            @if (count($locations))
                                @foreach ($locations as $location)
                                    <li>{{ $location }}</li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Description', 'Description:') !!}
                        {!! Form::textarea('description',null,['class'=>'form-control', 'id' => 'editor']) !!}
                    </div>
                     <div class="form-group">
                        {!! Form::label('Personal Link', 'Personal Link:') !!}
                        {!! Form::text('personal_link',null,['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>


@section('page-scripts')

<script>
 var imageUrl = '{{ $imageUrl }}/{{ $user->id }}/{{ $user->image }}';
 var image='{{ $user->image }}';
 var id= {{ $user->id }};

$(document).ready(function() {
   CKEDITOR.replace('editor');
     $('.fileinput-remove-button').click(function() {
            if(confirm("Are you sure you want to delete this image?"))
            {
                 $.ajax({
                      url: '../admin/users/deleteImage',
                      type: "post",
                      data :'user_id='+ id ,
                      success: function(data){
                         alert('Removed Successfully');
                        }
                    });
             }
        });

});
if(image) {
     $("#image").fileinput({
            showUpload: false,
            allowedFileExtensions:['jpg', 'gif', 'png'],
            initialPreview: [
                '<img src='+imageUrl+' class="file-preview-image" alt="{{ $user->image }}" title="{{ $user->image }}">'
            ],
            initialCaption: "{{ $user->image }}"
        });
    }
    else {
       $("#image").fileinput({
            showUpload: false,
            allowedFileExtensions:['jpg', 'gif', 'png'],
        });
    }
$(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('input').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    $("#location").tagit({
            allowSpaces: true,
            fieldName: "locations[]",
            requireAutocomplete: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function(request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function(item) {
                            return item.description;
                        });
                        currentlyValidTags = data;
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({ input: request.term }, callback);
                }
            }
        });
</script>
@endsection
