@extends('layouts.admin-master')

@section('admin-content')
<div class="article-index">
    <div class="row">
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" >
                <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                </div>
            </div>
        </div>
        <div class="col-md-2 pull-right">
            <br/>
            <a class="btn btn-block btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;New Types</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
            <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">Action <span class="fa fa-caret-down"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Delete</a></li>
                </ul>
            </div>
        </div>
         <div class="col-md-8 text-right">
            <nav>
            <ul class="pagination">
                <li class="disabled">
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active">
                    <a href="#">1</a>
                </li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                </a>
                </li>
            </ul>
            </nav>
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                <th class="">
                    <div id="selectAllArticle">
                        <input type="checkbox" />
                    </div>
                </th>
                <th class="">#</th>
                <th class="col-md-7">Title</th>
                <th class="col-md-1">Created</th>
                <th class="col-md-2">Author</th>
                <th class="col-md-2 text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">1</th>
                <td><a href="#">This plane? This plane can fly for 118 hours</a></td>
                <td>08/18/2015</td>
                <td><a href="#">Kate Torgovnick May</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-4 dataTables_info"><br/>Showing 1 to 10 of 57 entries</div>
        <div class="col-md-8 text-right">
            <nav>
            <ul class="pagination">
                <li class="disabled">
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active">
                    <a href="#">1</a>
                </li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                </a>
                </li>
            </ul>
            </nav>
        </div>
    </div>
</div>
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('admin-profile') !!}
@endsection

@section('page-scripts')
<script>
    $(function() {       
        $('.article-index input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });

        //Check/uncheck all
        $('#selectAllArticle input[type="checkbox"]').on('ifChecked', function(event){
               $('.article-index input[type="checkbox"]').iCheck('check');
        });
        $('#selectAllArticle input[type="checkbox"]').on('ifUnchecked', function(event){
               $('.article-index input[type="checkbox"]').iCheck('uncheck');
        });
    }); 

</script>	
@endsection