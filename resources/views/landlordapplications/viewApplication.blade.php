@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
	<h2>{!! $tenantName !!}</h2><br>
  @foreach($categorizedQuestions as $category => $questions)
    <h2 class='page-header'>{!! $category !!}</h2>
    @if($category == "Basic Information")
      <div class="row">
        <div class="col-lg-6">
          <p class="col-lg-6">Email Address</p>
        </div>
        <div class="col-lg-6">
          <textbox class="col-lg-6">
                {!! $tenantProfile->email !!} 
          </textbox>
        </div>
        <hr />
      </div>
      <div class="row">
        <div class="col-lg-6">
          <p class="col-lg-6">Date of Birth</p>
        </div>
        <div class="col-lg-6">
          <textbox class="col-lg-6">
                {!! $tenantProfile->dob !!}
          </textbox>
        </div>
        <hr />
	</div>
	@endif
    @foreach($questions as $question)
      <div class="row">
        <div class="col-lg-6">
          <p class="col-lg-6">{!! $question['question'] !!}</p>
        </div>
        <div class="col-lg-6">
          <textbox class="col-lg-6">
		{!! $tenantResponses[$question['id']] !!}
	  </textbox>
        </div>
        <hr />
      </div>
    @endforeach
  @endforeach
        <!--/div--><!-- /.row -->
         </section><!-- /.content -->
                <!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection

