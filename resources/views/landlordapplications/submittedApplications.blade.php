@extends('layouts.landlord-inner')
@section('inner-content')


<div class="content-wrapper">
        <!-- Content Header (Page header) -->
                @if (Session::has('warningMessage'))
                <div class="alert alert-info">{{ Session::get('warningMessage') }}</div>
                @endif


        <!-- Main content -->
       <section class="content">
                
		@if (Session::has('message-success'))
                @include('elements.message-success')
                @endif
                @include('elements.breadcrump-landlord')
                
		
  	  <table class="table">
    	  	<thead>
      		<th>Property</th>
      		<th>Single Applicants /<br>Application Groups</th>
      		<th>Date Submitted</th>	
	        <th>Last Updated</th>
      		<th>Status</th>
		<th></th>
    		</thead>

	 @forelse($applications as $oneApplication)
	 @foreach($oneApplication->applicationSubmissions as $applicationSubmission)
	<tr>
        <td>{!! $oneApplication->property_name !!}</td>
	@if($applicationSubmission->group_name != null)
        <td><a href="#{!! str_replace(' ','',$applicationSubmission->group_name.$applicationSubmission->id) !!}Modal" data-target="#{!! str_replace(' ','',$applicationSubmission->group_name.$applicationSubmission->id) !!}Modal" data-toggle="modal">{!! $applicationSubmission->group_name !!}</a></td>
	@else
	<td><a href="{!! URL::to('/').'/landlords/viewApplication/'.Crypt::encrypt($applicationSubmission->tenant_id) !!}">{!! $applicationSubmission->tenant_name !!}</a></td>
	@endif
	<!--td>{!! $oneApplication->group_name !!}</td-->
        <td>{!! $applicationSubmission->created_at->format('M d Y') !!}</td>
        <td>{!! $applicationSubmission->updated_at->format('M d Y') !!}</td>
        <td>
          @if($applicationSubmission->status == 1)
            Submitted
	    </td>
	    <td>
                    <a href="{!! URL::to('/').'/landlords/manageApplications/approve/approve/approve/' .Crypt::encrypt($oneApplication->id).'/'.Crypt::encrypt($applicationSubmission->application_group_id).'/'.Crypt::encrypt($applicationSubmission->id) !!}" class="btn btn-link"><i class="fa fa-book"></i>Approve</a><br>
		    <a href="{!! URL::to('/').'/landlords/manageApplications/deny/deny/deny/' .Crypt::encrypt($applicationSubmission->application_group_id).'/'.Crypt::encrypt($applicationSubmission->id) !!}" class="btn btn-link"><i class="fa fa-book"></i>Deny</a>
  
          @elseif($applicationSubmission->status == 2)
            Approved! Schedule a lease signing with your tenants. 
          @elseif($applicationSubmission->status == 0)
            Denied
          @else
            Lease Signed
          @endif
        </td>
	</tr>
	@endforeach
      @empty
      @endforelse
      </table>        
	@forelse($applications as $oneApplication)
	@foreach($oneApplication->applicationSubmissions as $applicationSubmission)
	<div class="modal fade bs-example-modal-md" id="{!! str_replace(' ','',$applicationSubmission->group_name.$applicationSubmission->id) !!}Modal" tabindex="-1" role="dialog" aria-hidden="true"  aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2 class="myLargeModalLabel">{!! $applicationSubmission->group_name !!}</h2>
        </div>

        <div class="modal-body">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	      <br><h3>Click the names below to see individual applications from this group!</h3>
                @if(count($applicationSubmission->onSiteMembers)!=0)
                        @if(count($applicationSubmission->onSiteMembers)>1)
                        @foreach ($applicationSubmission->onSiteMembers as $member)
                        <a href="{!! URL::to('/').'/landlords/viewApplication/'.Crypt::encrypt($member['tenant_id']) !!}">{!! $member['tenant_name'] !!}</a><br>
                        @endforeach
                        @else
                        <a href="{!! URL::to('/').'/landlords/viewApplication/'.Crypt::encrypt($member['tenant_id']) !!}">{!! $applicationSubmission->onSiteMembers[0]['tenant_id'] !!}</a>
                        @endif
                @else
                @endif
	      <br><h3>Below are emails of those applying with this group but not on TenantU yet!</h3>
		@if(count($applicationSubmission->offSiteMembers)!=0)
                	@if(count($applicationSubmission->offSiteMembers)>1)
                	@foreach ($applicationSubmission->offSiteMembers as $member)
                	{!! $member['tenant_email'] !!}<br>
                	@endforeach
                	@else
                	{!! $applicationSubmission->offSiteMembers[0]['tenant_email'] !!}
                	@endif
 	        @else
        	@endif
	  </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
  </div>
  </div>
	@endforeach
	@empty
	@endforelse
</section>
</div>
<style>
    table {
        table-layout: fixed;
    }
    td {
        width: 100%;
    }
  .apartment-data-input-short {
      width: 70%;
  }
    .apartment-data-input-long {
        width: 80%;
    }
.modal-body{
background-color: #fff;
padding-left: 15px !important;
padding: 0px !important;
!important;
}
</style>
@endsection
