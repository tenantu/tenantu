<?php namespace Gvb\Whoops;

use Exception;
use Illuminate\Foundation\Exceptions\Handler;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Debug\Exception\FatalErrorException;
use Illuminate\Database\QueryException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Contracts\Encryption\DecryptException;

class ExceptionHandler extends Handler
{

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        'Symfony\Component\HttpKernel\Exception\HttpException'
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * When debugging is enabled, we make the error pretty using Whoops
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception                $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {

	if (config('app.debug')) {
            $whoops = new Run;
            $whoops->pushHandler($request->ajax() ? new JsonResponseHandler : new PrettyPageHandler);
            $whoops->allowQuit(false);
            $whoops->writeToOutput(false);

            return response($whoops->handleException($e), $whoops->sendHttpCode());
        }

        if ($this->isHttpException($e)) {
            return redirect('/404/');
	    //return $this->renderHttpException($e);
        }
	if($e instanceof TokenMismatchException){
            return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }

        if(get_class($e) == "ErrorException"){
            return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }

        if($e instanceof DecryptException){
            return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }

        if($e instanceof NotFoundHttpException)
        {
            return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }
        if ($e instanceof ModelNotFoundException) {
            return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }
        if ($e instanceof FatalErrorException) {
          return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }

        if ($e instanceof QueryException) {
            return redirect('/contactus/')->with('message-error', 'Whoops! Looks like you ran into an issue, either fill out the contact form below (describing the issue encountered in as much detail as possible) or call the number on this page so we can get on fixing that!');
        }

        return parent::render($request, $e);
    }

}
