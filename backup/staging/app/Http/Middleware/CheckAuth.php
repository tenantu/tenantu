<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
use Redirect;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
        //dd(Auth::user('landlord'));
        if( !empty(Auth::user('landlord')) && Auth::user('landlord')->is_active == 1 ){
            return redirect()->guest('landlords/dashboard');
            
        }
        else if( !empty(Auth::user('tenant')) && Auth::user('tenant')->is_active == 1 ){
            return Redirect::route('tenants.dashboard'); 
        }
        else{
            return $next($request);
        }
        
    }
}
