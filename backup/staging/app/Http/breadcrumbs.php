<?php

// Home
Breadcrumbs::register('profile', function ($breadcrumbs) {
    $breadcrumbs->push('profile', route('tenants.profile'));
});

// Home > Dashboard
Breadcrumbs::register('admin-dashboard', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Dashboard', route('users.adminDashboard'));
});

// Home > Profile > Edit
Breadcrumbs::register('admin-profile-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Profile', route('users.adminProfile'));
    $breadcrumbs->push('Edit', route('users.adminProfile'));
});

// Home > Profile > Chnage password
Breadcrumbs::register('admin-profile-change-password', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Profile', route('users.adminProfile'));
    $breadcrumbs->push('Change Password', route('users.getChangePassword'));
});

// Home > Articles > List
Breadcrumbs::register('admin-articles', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Articles', url('admin/articles/index'));
    $breadcrumbs->push('List', url('admin/articles/index'));
});

// Home > Articles > New
Breadcrumbs::register('admin-articles-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Articles', url('admin/articles/index'));
    $breadcrumbs->push('New', url('admin/articles/create'));
});

// Home > Articles > Edit
Breadcrumbs::register('admin-articles-edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Articles', url('admin/articles/index'));
    $breadcrumbs->push('Edit', url('admin/articles/edit/' . $post->id));
});

// Home > Featured Articles > List
Breadcrumbs::register('admin-featured_articles', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Articles', url('admin/articles/index'));
    $breadcrumbs->push('Featured Articles', url('admin/featured_articles/index'));
    $breadcrumbs->push('List', url('admin/featured_articles/index'));
});

// Home > Featured Articles > New
Breadcrumbs::register('admin-featured_articles-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Articles', url('admin/articles/index'));
    $breadcrumbs->push('Featured Articles', url('admin/featured_articles/index'));
    $breadcrumbs->push('New', url('admin/featured_articles/create'));
});

// Home > Featured Articles > Edit
Breadcrumbs::register('admin-featured_articles-edit', function ($breadcrumbs, $featured_post) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Articles', url('admin/articles/index'));
    $breadcrumbs->push('Featured Articles', url('admin/featured_articles/index'));
    $breadcrumbs->push('Edit', url('admin/featured_articles/edit/' . $featured_post->id));
});

// Home > Images > List
Breadcrumbs::register('admin-images', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Images', url('admin/images/index'));
    $breadcrumbs->push('List', url('admin/images/index'));
});

// Home > Images > New
Breadcrumbs::register('admin-images-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Images', url('admin/images/index'));
    $breadcrumbs->push('New', url('admin/images/create'));
});

// Home > Images > Edit
Breadcrumbs::register('admin-images-edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Images', url('admin/images/index'));
    $breadcrumbs->push('Edit', url('admin/images/edit/' . $post->id));
});

// Home > Featured Images > List
Breadcrumbs::register('admin-featured_images', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Images', url('admin/images/index'));
    $breadcrumbs->push('Featured Images', url('admin/featured_images/index'));
    $breadcrumbs->push('List', url('admin/featured_images/index'));
});

// Home > Featured Images > New
Breadcrumbs::register('admin-featured_images-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Images', url('admin/images/index'));
    $breadcrumbs->push('Featured Images', url('admin/featured_images/index'));
    $breadcrumbs->push('New', url('admin/featured_images/create'));
});

// Home > Featured Images > Edit
Breadcrumbs::register('admin-featured_images-edit', function ($breadcrumbs, $featured_post) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Images', url('admin/images/index'));
    $breadcrumbs->push('Featured Images', url('admin/featured_images/index'));
    $breadcrumbs->push('Edit', url('admin/featured_images/edit/' . $featured_post->id));
});

// Home > Categories > List
Breadcrumbs::register('admin-categories', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Categories', url('admin/categories/'));
    $breadcrumbs->push('List', url('admin/categories/'));
});

// Home > Categories > New
Breadcrumbs::register('admin-categories-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Categories', url('admin/categories/'));
    $breadcrumbs->push('New', url('admin/categories/create'));
});

// Home > Categories > Edit
Breadcrumbs::register('admin-categories-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Categories', url('admin/categories/'));
    $breadcrumbs->push('Edit', url('admin/categories/edit'));
});

// Home > Types > List
Breadcrumbs::register('admin-types', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Types', url('admin/types/'));
    $breadcrumbs->push('List', url('admin/types/'));
});

// Home > Types > New
Breadcrumbs::register('admin-types-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Types', url('admin/types/'));
    $breadcrumbs->push('New', url('admin/types/create'));
});
// Home > Types > Edit
Breadcrumbs::register('admin-types-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Types', url('admin/types/'));
    $breadcrumbs->push('Edit', url('admin/types/edit'));
});

// Home > Categories Type > List
Breadcrumbs::register('admin-category-types', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Category Content Type', url('admin/category-types/'));
    $breadcrumbs->push('List', url('admin/category-types/'));
});

// Home > Categories Type > New
Breadcrumbs::register('admin-category-types-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Category Content Type', url('admin/category-types/'));
    $breadcrumbs->push('New', url('admin/category-types/create'));
});
// Home > Categories Type > Edit
Breadcrumbs::register('admin-category-types-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Category Content Type', url('admin/category-types/'));
    $breadcrumbs->push('Edit', url('admin/category-types/edit'));
});
// Home > Newsletter > List
Breadcrumbs::register('admin-newsletters', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Newsletter', url('admin/newsletters/'));
    $breadcrumbs->push('List', url('admin/newsletters/'));
});

// Home > Newsletter > New
Breadcrumbs::register('admin-newsletters-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Newsletter', url('admin/newsletters/'));
    $breadcrumbs->push('New', url('admin/newsletters/create'));
});
// Home > Newsletter > Edit
Breadcrumbs::register('admin-newsletters-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Newsletter', url('admin/newsletters/'));
    $breadcrumbs->push('Edit', url('admin/newsletters/edit'));
});
// Home > Forums > List
Breadcrumbs::register('admin-forums', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Forums', url('admin/forums/'));
    $breadcrumbs->push('List', url('admin/forums/'));
});

// Home > Forums > New
Breadcrumbs::register('admin-forums-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Forums', url('admin/forums/'));
    $breadcrumbs->push('New', url('admin/forums/create'));
});
// Home > Forums > Edit
Breadcrumbs::register('admin-forums-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Forums', url('admin/forums/'));
    $breadcrumbs->push('Edit', url('admin/forums/edit'));
});
// Home > Forum Comments > List
Breadcrumbs::register('admin-forum-comments', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Forum', url('admin/forums/'));
    $breadcrumbs->push('Comments', url('admin/forums/'));
});

// Home > Forum Comments > New
Breadcrumbs::register('admin-forum-comments-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Forum Comments', url('admin/forum-comments/'));
    $breadcrumbs->push('New', url('admin/forum-comments/create'));
});
// Home > Forums > Edit
Breadcrumbs::register('admin-forum-comments-edit', function ($breadcrumbs, $forumComment) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Forum Comments', url('admin/forum-comments/index', $forumComment->forum_id));
    $breadcrumbs->push('Edit', url('admin/forum-comments/edit'));
});
// Home > Users > New
Breadcrumbs::register('admin-user-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('User', route('users.adminIndex'));
    $breadcrumbs->push('New', route('users.adminCreate'));
});
// Home > Users > Edit
Breadcrumbs::register('admin-user-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('User', route('users.adminIndex'));
    $breadcrumbs->push('Edit', route('users.adminProfile'));
});
// Home > Users > List
Breadcrumbs::register('admin-users', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Users', route('users.adminIndex'));
    $breadcrumbs->push('List', route('users.adminIndex'));

});
// Home > Preferred Countries > New
Breadcrumbs::register('admin-preferred-countries-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Preferred Countries', route('preferred_countries.adminIndex'));
    $breadcrumbs->push('New', route('preferred_countries.adminCreate'));
});
// Home > Preferred Countries > Edit
Breadcrumbs::register('admin-preferred-countries-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Preferred Countries', route('preferred_countries.adminIndex'));
    $breadcrumbs->push('Edit', route('preferred_countries.adminEdit'));
});

// Home > Preferred Countries > List
Breadcrumbs::register('admin-preferred-countries', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Preferred Countries', route('preferred_countries.adminIndex'));
    $breadcrumbs->push('List', route('preferred_countries.adminIndex'));

});
// Home > Points > New
Breadcrumbs::register('admin-point-system-new', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Point System', route('point_system.adminIndex'));
    $breadcrumbs->push('New', route('point_system.adminCreate'));
});
// Home > Points > Edit
Breadcrumbs::register('admin-point-system-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Point System', route('point_system.adminIndex'));
    $breadcrumbs->push('Edit', route('point_system.adminEdit'));
});

// Home > Points > List
Breadcrumbs::register('admin-point-system', function ($breadcrumbs) {
    $breadcrumbs->parent('admin-home');
    $breadcrumbs->push('Point System', route('point_system.adminIndex'));
    $breadcrumbs->push('List', route('point_system.adminIndex'));

});

//FRONT END

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('posts.welcome'));
});

// Home > Profile > Edit
Breadcrumbs::register('home-profile-edit', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Profile', route('users.dashboard'));
    $breadcrumbs->push('Edit', route('users.dashboard'));

});
// Home > Forum > List
Breadcrumbs::register('home-forum', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Forum', route('forums.index'));

});

// Home > Forum->Show
Breadcrumbs::register('home-forum-show', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Forum', route('forums.index'));
    $breadcrumbs->push('Show', route('forums.index'));

});

// Home > Articles > List
Breadcrumbs::register('home-articles', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Destinations', route('posts.index'));
});

// Home > Imagess > List
Breadcrumbs::register('home-images', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Images', route('posts.index'));
});

// Home > About Us
Breadcrumbs::register('home-about-us', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About Us', route('about-us'));
});

// Home > Contact Us
Breadcrumbs::register('home-contact-us', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Contact Us', route('contact-us'));
});

// Home > Terms and Conditions
Breadcrumbs::register('home-terms-conditions', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Terms and Conditions', route('terms-conditions'));
});

/*
// Home > Blog
Breadcrumbs::register('blog', function($breadcrumbs)
{
$breadcrumbs->parent('home');
$breadcrumbs->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function($breadcrumbs, $category)
{
$breadcrumbs->parent('blog');
$breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Page]
Breadcrumbs::register('page', function($breadcrumbs, $page)
{
$breadcrumbs->parent('category', $page->category);
$breadcrumbs->push($page->title, route('page', $page->id));
});
 */
