<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\MessageThread;
use App\Http\Models\Thread;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\Rating;
use App\Http\Models\RatingUser;
use App\Http\Models\Mysearch;
use App\Http\Models\LandlordTenant;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;
    
    public function getTenantProfile($id)
    {
		$tenant 	= Tenant::find($id);
		
		/*$tenant 	= Tenant::with('school')
							->where('id',$id)->get();*/
		return $tenant;
	}
	
	public function getLandlordProfile($id)
	{
		
		$landlord 	= Landlord::findOrFail($id);
		return $landlord;
	}
	public function getLandlordProfileFromSlug($slug)
	{
		
		$landlord 	= Landlord::where('slug',$slug)->firstorfail();
		return $landlord;
	}
	
	public function getTenantProfileFromSlug($slug)
	{
		
		$tenant 	= Tenant::where('slug',$slug)->firstorfail();
		return $tenant;
	}
	
	public function getGender($gender)
	{
		if($gender=='')
		{
			$genderDetail = 'Not specified';
		}
		if($gender=='M')
		{
			$genderDetail = 'Male';
		}
		if($gender=='F')
		{
			$genderDetail = 'Female';
		}
		return $genderDetail;
	}
	public function getDob($dob)
	{
		if($dob=='0000-00-00')
		{
			$birthDate = 'Not specified';
		}
		else
		{
			$birthDate = $dob;
		}
		return $birthDate;
	}
	public function getState($state)
	{
		if($state=='')
		{
			$stateDetail = 'Not specified';
		}
		else
		{
			$stateDetail= $state;
		}
		return $stateDetail;
	}
	public function getCity($city)
	{
		
		if($city=='')
		{
			$cityDetail = 'Not specified';
		}
		else
		{
			$cityDetail= $city;
		}
		return $cityDetail;
	}
	public function getLanguage($language)
	{
		if($language=='')
		{
			$languageDetail = 'Not specified';
		}
		else
		{
			$languageDetail= $language;
		}
		return $languageDetail;
	}
	
	public function getPropertyCount($landlordId)
	{
		
		$propertyCount = DB::table('properties')
                     ->select(DB::raw('count(*) as propert_count'))
                     ->where('landlord_id', '=',$landlordId)
                     ->get();
        return $propertyCount;
	}
	
	public function getPropertyList($landlordId)
	{
		$propertyList = Property::where('landlord_id', '=', $landlordId)
								->where('status',1)
								->select('id','property_name')
								->get()
								->toArray();
		
        return $propertyList;
	}
	
	public function getTenantName($profile)
	{
		$name = ucfirst($profile->firstname)." ".ucfirst($profile->lastname);
		return $name;
	}
	
	public function getPropertyMessageThread($tenantId)
	{
		
			$propertyCount = DB::table('message_threads')
						->select(DB::raw('count(*) as threadcount'))
						->where('tenant_id', '=',$tenantId)
						->get();
						
			return $propertyCount;
	}
	
	public function getListMessageThreads($tenantId)
	{
		
        
       $listMessageThreads=  DB::table('message_threads')
            ->join('threads', 'message_threads.thread_id', '=', 'threads.id')
            ->select('message_threads.thread_id')
            ->addSelect('message_threads.created_at')
            ->addSelect('threads.thread_name')
            ->where('message_threads.tenant_id', '=', $tenantId)
            
            ->get();
        
        
        //$array = (array) $listMessageThreads;
        //print_r($array);exit;
        return $listMessageThreads;
		
	}
	
	public function getAllMessagesWithThread($threadId,$tenantId)
	{
		
		$listOfMessages = Message::where('message_thread_id', '=', $threadId)
									->where('tenant_id', '=', $tenantId)
									->select('message')
									->addSelect('message_thread_id')
									->addSelect('sender')
									->addSelect('tenant_id')
									->addSelect('landlord_id')
									->addSelect('created_at')
									->orderBy('created_at','ASC')
									->get()
									->toArray();
		
        return $listOfMessages;
	}
	public function getAllMessagesWithThreadLandlord($threadId,$landlordId)
	{
		$listOfMessages = Message::where('message_thread_id', '=', $threadId)
									->where('landlord_id', '=', $landlordId)
									->select('message')
									->addSelect('message_thread_id')
									->addSelect('sender')
									->addSelect('tenant_id')
									->addSelect('landlord_id')
									->addSelect('created_at')
									->orderBy('created_at','ASC')
									->get()
									->toArray();
        return $listOfMessages;
	}
	
	public function getLandLordName($landlordId)
	{
		
		
		$landlordName = Landlord::where('id', '=', $landlordId)
						->select('firstname')
						->addSelect('lastname')
						->get()->first();
		if(empty($landlordName))
		{
			return redirect('/404');
		}
		$name = $landlordName->getFullName();
						
		return $name;
		
	}
	public function getTenantNameFromId($tenantId)
	{
		$tenantName = Tenant::where('id', '=', $tenantId)
						->select('firstname')
						->addSelect('lastname')
						->first();
						
		return $tenantName->firstname." ".$tenantName->lastname;
		
	}
	public function getPropertiesForDropDown( $landLordId){
		$properties = Property::where('landlord_id', $landLordId)
								->where('status',1)
								->orderBy('id')
								->get();
		return $properties;
	}
	public function getMessageThreadName( $mgThreadId )
    {
    	$threads = Thread::where('id',$mgThreadId)
						   ->select('thread_name')
						   ->addSelect('created_at')->first();
		return $threads;
	}
	public function getThreads($tenantId=null)
	{
	    $threads = Thread::select('id')
						   -> addSelect('thread_name')
						   -> orderBy('order_status')
						   -> get();


		return $threads;
	}
	
	public function getThreadsForFlag()
	{
		$threads = Thread::select('id')
						   -> where('flag',1)
						   -> orderBy('order_status')
						   -> get()->toArray();
		return $threads;
	}
	
	 public function getTenantThread($tenantId=null,$landlordId,$propertyId=null)
	 {
		 //echo $landlordId;exit;
             if($tenantId==""){
                $tenantId = Auth::user('tenant')->id;
            }
            if($this->isTenantAssociatedWithThisProperty($tenantId,$landlordId,$propertyId)){
				$threads = Thread::select('id')
						   -> addSelect('thread_name')
						   -> orderBy('order_status')
						   -> get();
			    return $threads;
                
            }
            else
            {
				$threads = Thread::select('id')
						   -> addSelect('thread_name')
						   -> where('flag',0)
						   -> orderBy('order_status')
						   -> get();
			    return $threads;
			}
            
            
     }
     
     public function isTenantAssociatedWithLandlord($tenantId)
     { 
			 $landlordTenantWithOutProperty = LandlordTenant::where('tenant_id',$tenantId)
														->get();
			if(count($landlordTenantWithOutProperty) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		 
                       
     }
     
     public function isTenantAssociatedWithThisProperty($tenantId,$landlordId,$propertyId=null)
     {
		 //echo $landlordId;exit;
		 if($propertyId!='')
		 {
			$landlordTenantWithProperty = LandlordTenant::where('property_id',$propertyId) 
														->where('tenant_id',$tenantId)
														->get();
			if(count($landlordTenantWithProperty) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		 }
		 else
		 {
			 $landlordTenantWithOutProperty = LandlordTenant::where('landlord_id',$landlordId) 
														->where('tenant_id',$tenantId)
														->get();
			if(count($landlordTenantWithOutProperty) > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		 }
            //TODO
            
            //LandlordTenant::
            //return true;
                       
     }
     
     public function isTenantThread($messageThreadId,$tenantId)
     {
		 $messages = Message::where('message_thread_id', '=', $messageThreadId)
									->where('tenant_id', '=', $tenantId)
									->select('message')
									->get();
		 if(count($messages) )
		 {
			return true;
		 }
		else
		{
		    return false;
		}
	 }
	 
	 public function getRating()
	 {
		 $ratings = Rating::select('id')
					->addSelect('rating_name')
					->get();
				
		return $ratings;
	 }
	 public function getProperties($landlordId){
	 	$properties  = Property::where('status', 1)
                                        -> where('landlord_id', $landlordId)
                                        -> orderBy('created_at','DESC')
                                        -> take(3)
                                        -> get();
        return  $properties;                               
	 }
	 public function findProperty( $propertyId ){
	 		$property  = Property::where('id', $propertyId)
                                        -> with('property_images')
                                        -> with('property_amenities')
                                        -> with('property_docs')
                                        -> first();
            return  $property;
	 }
	 
	 //this method is for fetch all the properties foa a landlord with status 1
	 public function getAllProperties($landlordId)
	 {
		 $propertyDetails = Property::from('properties as P')
							->select('P.property_name')
							->addSelect('P.location')
							->addSelect('P.rent_price')
							->addSelect('P.rent_price')
							->addSelect('PI.image_name')
							->join('property_images as PI','P.id','=','PI.property_id')
							->where('P.landlord_id',$landlordId)
							->where('P.status',1)
							->where('PI.featured',1)
							->orderBy('P.created_at','DESC')
							->get();
							
		return  $propertyDetails;
	}				

	 /*public function findProperty( $propertyId ){
	 		$property  = Property::where('id', $propertyId)
                                        -> with('property_images')
                                        -> with('property_amenities')
                                        -> with('property_docs')
                                        -> first();
            return  $property;
	 }*/
	 public function getAllProperty( $landlordId ){
	 		$properties  = Property::where('landlord_id', $landlordId)
										-> where('status',1)
                                        -> with('property_images')
                                        -> with('property_amenities')
                                        -> with('property_docs')
                                        -> get();
            return  $properties;

	 }
	
	//this method is for the listing of schools
	public function  getSchoolDetails()
     {
		 $school  = array();
		 $school  = DB::table('schools')->where('is_active',1)->orderBy('schoolname', 'asc')->lists('schoolname', 'id');
		 return $school ;
	 }
	 
	 //this method is for the fetch school with an id
	 
	 public function getSchoolWithId($schoolId)
	 {
		 $schoolName = School::where('id',$schoolId)
							->select('schoolname')
							->get()->first();
		 return $schoolName;
	 }
	
	//this method is for fetch all the landlords
	
	public function getLandlords(){
		$landlords 	= Landlord::select('firstname')
							   ->addSelect('lastname')
							   ->addSelect('id')
							   ->where('is_active',1)
							   ->get();
		return $landlords;
	}
	
	public function getAllLandlords(){
		$landlords  = array();
		if(Session::has('schoolName'))
		{
			$sessionSchoolId = Session::get('schoolName');
			$landlords = DB::table('landlords')->select(DB::raw("CONCAT(firstname,' ',lastname) as fullname,id"))->where('is_active',1)->where('school_id',$sessionSchoolId)->lists('fullname', 'id');
		}else{
		  $landlords = DB::table('landlords')->select(DB::raw("CONCAT(firstname,' ',lastname) as fullname,id"))->where('is_active',1)->lists('fullname', 'id');
	    }
		return $landlords;
	}
	public function getReviewCount( $propertyId ){
		
		$reviewsCount = DB::select("SELECT COUNT(*) AS cnt FROM property_review WHERE property_id IN ($propertyId)");
         
        return $reviewsCount[0]->cnt;            
	}
	public function getTenantsPayments( $landLordId ){
		$tenants  = LandlordTenant::where('landlord_id', $landLordId)
                                        -> with('tenant')
                                        -> get();
        return  $tenants;
	}
	public function getlandlordsPayments(){
		$landlords  = LandlordTenant::with('landlord')
										->groupBy('landlord_id')
										-> get();
        return  $landlords;
	}
	public function getTenantsPaymentsForLandlord( $landLordId ){
		$tenants  = LandlordTenant::where('landlord_id', $landLordId)
                                        -> with('tenant')
                                        -> get();
        return  $tenants;
	}
	
	public function logMsg($msg)
	{
		$x='';
		$x.= "" . $msg . "\n \n";
		$myFile = file_put_contents('/home/fodofnet/www/tenantu/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
		//$myFile = file_put_contents('/var/www/tenantudev/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
	}
	
	public function updateLandlordRating($landlordId)
	{
		$this->logMsg("posted landlord id =".$landlordId);
		$this->logMsg("query =SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id IN(SELECT id FROM properties WHERE landlord_id='.$landlordId.') AND rating_id=2");
		$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id IN(SELECT id FROM properties WHERE landlord_id='.$landlordId.') AND rating_id=2 ');
		$avgRateValue = $avgReviewRatings[0]->rate_value;
		$this->logMsg("avg rate value before ceil =".$avgRateValue);
		$avgRateValueRounded = ceil($avgRateValue);
		$this->logMsg("avg rate value after ceil =".$avgRateValueRounded);
		$rating = 0;
		
		
		if($avgRateValueRounded >= 4)
		{
			$rating = 1;
		}
		else
		{
			$rating = 0;
		}
		
		$updatePropertiesRecomented = DB::table('properties')
								 ->where('landlord_id',$landlordId)
								 ->update(['recommented_flag'=>$rating]);
		$this->logMsg("updated data =".$updatePropertiesRecomented);
		return $updatePropertiesRecomented;
	}
	
	
	
	
	
}
