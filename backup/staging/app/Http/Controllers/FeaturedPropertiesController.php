<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\PropertyEnquiry;
use App\Http\Models\PropertyReview;
use App\Http\Models\FeaturedSetting;
use App\Http\Models\FeaturedPurchase;



use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use URL;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;

class FeaturedPropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function getFeaturedProperties(Request $request)
    {
		$loggedLandlordId   = Auth::user('landlord')->id;
		$landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
		$landlordsNonFeaturedProperty = $this->getNonFeaturedProperties($loggedLandlordId);
		//print_r($landlordsNonFeaturedProperty);exit;
		$landlordsFeaturedProperty = $this->getAllFeaturedProperties($loggedLandlordId);
		
		$featuredSetting = $this->getFeaturedSetting();
		
		$originalProfile=HTML::getLandlordProfileImage();
		$statusMessage = '';
		if(isset($request->paid))
		{
			
			$paidStatus = $request->paid;
			if($paidStatus== '1')
			{
				$statusMessage = 'Your transaction is completed';
			}
			else
			{
				$statusMessage = 'Your transaction is not completed';
			}
		} 
		
		return view('featured.landlordsFeaturedProperty', [
	            'pageTitle'     			=> 'My Featured Properties',
	            'pageHeading'   			=> 'My Featured Properties',
	            'pageMenuName'				=> 'featured properties',
	            'nonFeaturedProperties'    	=> $landlordsNonFeaturedProperty,
	            'featuredProperties' 		=> $landlordsFeaturedProperty,
	             'profileImage'  			=> $originalProfile,
	             'profile'       			=> $landlordProfile,
	             'featuredSetting'			=> $featuredSetting,
	             'statusMessage'			=> $statusMessage
	        ]);
	}
	
	
	
	private function getFeaturedStatusDates()
	{
		
	}
	
	public function getLandlordProfile($id)
	{
		
		$landlord 	= Landlord::findOrFail($id);
		return $landlord;
	}
	
	private function getFeaturedSetting()
	{
		$featuredSetting = array();
		$featuredSetting  = DB::table('featured_settings')->where('status',1)->orderBy('id', 'asc')->lists('duration', 'id');
		return $featuredSetting;
	}
	
	private function getNonFeaturedProperties($loggedLandlordId)
	{
		$nonFeatured = array();
		$nonFeatured  = DB::table('properties')->where('landlord_id',$loggedLandlordId)->where('status',1)->where('featured',0)->orderBy('property_name', 'asc')->lists('property_name', 'id');
		return $nonFeatured;
	}
	
	public function getAllFeaturedProperties($landlordId)
	{
		$properties  = Property::where('landlord_id', $landlordId)
										-> where('featured',1)
                                        -> with('property_images')
                                        -> with('property_amenities')
                                        -> with('property_docs')
                                        -> get();
        return  $properties;
	}
	
	public function featuredPurchase($durationId,$propertyId)
	{
		//echo "hiii";exit;
		$propertySchoolId = Property::where('id',$propertyId)
								 ->select('school_id')
								 ->first();
		
		$count = DB::select('SELECT count(*) as count 
						FROM featured_purchase AS FP
						JOIN properties AS P ON FP.property_id = P.id
						WHERE P.school_id ='.$propertySchoolId->school_id.'
						AND FP.featured_end > NOW()
						AND FP.featured_end !="0000-00-00 00:00:00"');		
							
		
		if($count[0]->count > 19)
		{
			return Redirect::route('landlord.getFeaturedProperties')->with('message-error','You cannot activate this feature now. Because featured propery slots are already filled.');
		}
		
		$loggedLandlord = Auth::user('landlord');
		$loggedLandlordId = $loggedLandlord->id;
		$featured = FeaturedSetting::where('id',$durationId)
									->select('id')
									->addSelect('duration')
									->addSelect('price')
									->first();
									
		$property =  Property::where('id',$propertyId)
								 ->select('id')
								 ->addSelect('property_name')
								 ->first();
								 
		if((count($featured) <= 0) && (count($property)<=0))
		{
			return redirect('/404');
		}
		$originalProfile=HTML::getLandlordProfileImage(); 
		$landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
		
		$featuredPurchase = $this->saveFeaturedPurchase($loggedLandlordId,$property->id,$featured->id,$featured->duration,$featured->price);
		
		return view('featured.featuredPurchase', ['pageTitle' => 'Purchase Featured Property',
            'pageHeading'                                => 'Purchase Advertisement',
            'pageMenuName'								 => 'featured properties',
            'featured'                                   => $featured,
            'property'									 => $property,
            'loggedLandlord'							 => $loggedLandlord,
            'profileImage'       					     => $originalProfile,
            'profile'       							 => $landlordProfile,
            'featuredPurchase'							 => $featuredPurchase
        ]);
	}
	
	private function saveFeaturedPurchase($landlordId,$propertyId,$durationId,$durationName,$durationPrice)
	{
		$input= array();
		$input['landlord_id'] 	= $landlordId;
		$input['property_id'] 	= $propertyId;
		$input['duration_id'] 	= $durationId;
		$input['duration_name'] = $durationName;
		$input['actual_amount'] = $durationPrice;
		
		$featuredPurchase = FeaturedPurchase::create($input);
		return $featuredPurchase;
	}
	
	public function logMsg($msg)
	{
		$x='';
		$x.= "" . $msg . "\n \n";
		$myFile = file_put_contents('/home/fodofnet/www/tenantu/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
		//$myFile = file_put_contents('/var/www/tenantudev/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
	}
	
	public function featuredPaypalNotify(Request $request)
	{
		$x = print_r($_REQUEST,1);
		
		//$myFile = file_put_contents('/home/fodofnet/www/tenantu/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
		//$myFile = file_put_contents('/var/www/html/tenantudev/public/' . "log.txt", $x . PHP_EOL, FILE_APPEND) or die("error");
		
		$debug = 1;
		$useSandbox = 0;
		$logfile = Config::get('constants.LOG_FILE');
		//define("LOG_FILE", "/home/fodofnet/www/tenantu/public/log.txt");
		$raw_post_data = file_get_contents('php://input');
		$a = print_r($raw_post_data,1);
		$this->logMsg("row post data =".$a);
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		$this->logMsg("before 1st foreach");
		foreach ($raw_post_array as $keyval) {
			$keyval = explode ('=', $keyval);
			if (count($keyval) == 2)
				$myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		$this->logMsg("after 1st foreach");
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		$this->logMsg("before 1st if get_magic_quotes_gpc ");
		if(function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = true;
		}
		$this->logMsg("after 1st if get_magic_quotes_gpc ");
		foreach ($myPost as $key => $value) {
			if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}
		$this->logMsg("after 2nd foreach myPost ");
		// Post IPN data back to PayPal to validate the IPN data is genuine
		// Without this step anyone can fake IPN data
		$this->logMsg("before if useSandbox true");
		if($useSandbox == 1) {
			$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		} else {
			$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
		}
		$this->logMsg("paypal url =".$paypal_url);
		$this->logMsg("after if useSandbox true");
		$ch = curl_init($paypal_url);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		if($debug == 1) {
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
		}
		// CONFIG: Optional proxy configuration
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
		// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
		// of the certificate as shown below. Ensure the file is readable by the webserver.
		// This is mandatory for some environments.
		//$cert = __DIR__ . "./cacert.pem";
		//curl_setopt($ch, CURLOPT_CAINFO, $cert);
		$res = curl_exec($ch);
		if (curl_errno($ch) != 0) // cURL error
			{
			if($debug == 1) {	
				error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, $logfile);
			}
			curl_close($ch);
			exit;
		} else {
				// Log the entire HTTP response if debug is switched on.
				if($debug == 1) {
					error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, $logfile);
					error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, $logfile);
				}
				curl_close($ch);
		}
		// Inspect IPN validation result and act accordingly
		// Split response headers and payload, a better way for strcmp
		$this->logMsg("before explode token");
		$tokens = explode("\r\n\r\n", trim($res));
		$res = trim(end($tokens));
		$this->logMsg("before if VERIFIED==0 ");
		if (strcmp ($res, "VERIFIED") == 0) {
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment and mark item as paid.
			// assign posted variables to local variables
			//$item_name = $_POST['item_name'];
			$puchaseId = trim($_POST['custom']);
			$payment_status = trim($_POST['payment_status']);
			$payment_date = trim($_POST['payment_date']);
			$payment_amount = trim($_POST['mc_gross']);
			$txn_id = $_POST['txn_id'];
			
			$featuredStartDate = date('Y-m-d H:i:s');
			$duration = $this->selectDuration($puchaseId);
			$this->logMsg("duration=".$duration);
			$durationName = $duration->duration_name;
			if($durationName == '1 week')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+7 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '2 weeks')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+14 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '3 weeks')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+21 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '1 month')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+30 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '2 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+60 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '3 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+90 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '4 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+120 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '5 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+150 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '6 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+180 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '7 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+210 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '8 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+240 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '9 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+270 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '10 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+300 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '11 months')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+330 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			elseif($durationName == '1 year')
			{
				$strtotimeFeaturedStartDate = strtotime($featuredStartDate);
				$date = strtotime("+364 day", $strtotimeFeaturedStartDate);
				$featuredEndDate = date('Y-m-d H:i:s',$date);
			}
			
			$updateFeaturedPurchase = $this->updateFeaturedPurchase($puchaseId,$payment_status,$payment_date,$payment_amount,$txn_id,$featuredStartDate,$featuredEndDate);
			$propertyId = $this->selectPropertyId($puchaseId);
			$updatePropertyFeature = $this->updatePropertyFeatured($propertyId->property_id,$featuredStartDate,$featuredEndDate);
			//$payment_currency = $_POST['mc_currency'];
			
			//$receiver_email = $_POST['receiver_email'];
			//$payer_email = $_POST['payer_email'];
			
			if($debug == 1) {
				error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, $logfile);
			}
		}
		 else if (strcmp ($res, "INVALID") == 0) {
			// log for manual investigation
			// Add business logic here which deals with invalid IPN messages
			if($debug == 1) {
				error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, $logfile);
			}
		}
		$this->logMsg("after if INVALID==0 ");
	}
	
	private function selectDuration($featuredPurchaseId)
	{
		$featuredDuration = FeaturedPurchase::where('id',$featuredPurchaseId)
							->select('duration_name')
							->first();
		return $featuredDuration;
	}
	
	private function selectPropertyId($featuredPurchaseId)
	{
		$featuredPropertyId = FeaturedPurchase::where('id',$featuredPurchaseId)
							->select('property_id')
							->first();
							
		return $featuredPropertyId;
	}
	
	private function updatePropertyFeatured($propertyId,$featuredStartDate,$featuredEndDate)
	{
		$updateProperty = DB::table('properties')
							 ->where('id',$propertyId)
							 ->update(['featured'=>1,'featured_start'=>$featuredStartDate ,'featured_end'=>$featuredEndDate]);
	}
	
	private function updateFeaturedPurchase($featuredPurchaseId,$paymentStatus,$paymentDate,$amountPaid,$txn_id,$featuredStartDate,$featuredEndDate)
	{
		
		$updateFeaturedPurchase = DB::table('featured_purchase')
							 ->where('id',$featuredPurchaseId)
							 ->update(['purchase_date_from_paypal'=>$paymentDate,'payment_status'=>$paymentStatus,'amount_paid'=>$amountPaid,'txn_id'=>$txn_id,'featured_start'=>$featuredStartDate,'featured_end'=>$featuredEndDate]);
		return  $updateFeaturedPurchase;
	}
}
