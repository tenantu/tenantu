<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\MessageThread;
use App\Http\Models\Thread;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\PropertyAmenity;
use App\Http\Models\RatingUser;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\PropertyReview;
use App\Http\Models\Mysearch;
use DB;
use Input;
use Config;
use Validator;
use File;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use View;
use Redirect;
use Session;
use HTML;
use Mail;
use URL;
use Carbon\Carbon;
use App\Http\Helpers\UploadHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class LandlordsController extends Controller
{
	
	public function getLogin(){
		return view('landlords.login');
	}
	
	public function postLogin( Request $request ){
		
		$rules = array(
			'email'            => 'required|email|',     // required and must be unique in the ducks table
			'password'         => 'required'
		);

    
		$validator = Validator::make($request->all(), $rules);
		
		if ($validator->fails()) {
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}else{
			
			if (Auth::attempt("landlord", ['email' => $request->input('email'), 'password' => $request->input('password'),'is_active'=>1])){
				
				return redirect('landlords/dashboard');
			}
	    else{
				return redirect('landlords/login')->with('errors', 'please check the login credentials');
			}
		}
	}
	
	public function logout(){
		Auth::logout('landlord');
		return redirect('/login');
	}
	
	public function dashboard()
	{
		//echo "1";exit;
        $loggedLandlord   = Auth::user('landlord');
        $loggedLandlordId = $loggedLandlord->id;
        $landlordProfile  = $this->getLandlordProfile($loggedLandlordId);
        $properties       = $this->getProperties( $loggedLandlordId );
        $propertiesReview = PropertyReview::from('property_review AS PR')
                                 -> select('P.id')
                                 -> addSelect('P.property_name')
                                 -> addSelect('P.slug')
                                 -> join ('properties AS P','PR.property_id','=','P.id')
                                 -> where('P.landlord_id', $loggedLandlordId)
                                 -> where('PR.status', 1)
                                 -> orderBy('PR.created_at','DESC')
                                 -> groupBy('PR.property_id')
                                 -> take(4)
                                 -> get(); 
        //dd($propertiesReview);                         
        $pageTitle        = "Dashboard"; 
        $originalProfile  = HTML::getLandlordProfileImage();
        $generalMessages  = Message::from('messages AS M')
                                 -> select('M.message')
                                 -> addSelect('M.message_thread_id')
                                 -> addSelect('T.thread_name')
                                 -> join ('message_threads AS MT','MT.id','=','M.message_thread_id')
                                 -> join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 -> where('M.landlord_id', $loggedLandlordId)
                                 -> where('T.flag',0)
                                 -> where('T.status',1)
                                 -> where('M.sender','T')
                                 -> orderBy('M.created_at','DESC')
                                 -> take(4)
                                 -> get();

        $issueMessages  = Message::from('messages AS M')
                                 -> select('M.message')
                                 -> addSelect('M.message_thread_id')
                                 -> addSelect('T.thread_name')
                                 -> join ('message_threads AS MT','MT.id','=','M.message_thread_id')
                                 -> join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 -> where('M.landlord_id', $loggedLandlordId)
                                 -> where('T.flag',1)
                                 -> where('T.status',1)
                                 -> where('M.sender','T')
                                 -> orderBy('M.created_at','DESC')
                                 -> take(4)
                                 -> get(); 

        //dd($issueMessages);
        $school                     = $this->getSchoolDetails();                            
		return view('landlords.dashboard',['loggedLandlordId'=> $loggedLandlordId,
                                           'pageTitle'       => $pageTitle,
                                           'pageMenuName'	 => 'dashboard',
                                           'profileImage'    => $originalProfile,
                                           'profile'         => $landlordProfile,
                                           'properties'      => $properties,
                                           'generalMessages' => $generalMessages,
                                           'issueMessages'   => $issueMessages,
                                           'propertiesReview'=> $propertiesReview,
                                           'schools'         => $school       
                                         ]);
	}
    public function profile()
    {
        $loggedLandlord     = Auth::user('landlord');
        $imageUrl           = Config::get('constants.LANDLORD_IMAGE_URL');
        $noImageUrl         = Config::get('constants.LANDLORD_NOT_PROFILE_IMAGE_URL');
        $landlordProfile    = $this->getLandlordProfile($loggedLandlord->id);
        $gender             = $this->getGender($landlordProfile->gender);
        $dob                = $this->getDob($landlordProfile->dob);
        $state              = $this->getState($landlordProfile->state);
        $city               = $this->getCity($landlordProfile->city);
        $language           = $this->getLanguage($landlordProfile->language);
        $properties         = $this->getAllProperty( $loggedLandlord->id );
        //dd( $properties );
        if(count( $properties )){
            foreach( $properties as $property ){
                $propertyIdArray[]  = $property->id;
            }
             $propertyIdString = implode(',',$propertyIdArray);
             $rating           = HTML::getAverageReviewRatingForLandlord( $propertyIdString );
             $review           = $this->getReviewCount($propertyIdString);
        }
        else{
            $rating  = asset('public/img/stars0.png');
            $review  = 0;
        }
               
        if($landlordProfile->image!='')
        {
            $profileImage=$imageUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noImageUrl."/landlord-blank.png";
        }
        $originalProfile=HTML::getLandlordProfileImage();

                
         return view('landlords.profile', [
            'pageTitle'    => 'Profile',
            'pageHeading'  => 'Profile',
            'pageMenuName' => 'Profile',
            'imageUrl'     => $imageUrl,
            'profile'      => $landlordProfile,
            'birthdate'    => $dob,
            'gender'       => $gender,
            'state'        => $state,
            'city'         => $city,
            'language'     => $language,
            'profileImage' => $originalProfile,
            'rating'       => $rating,
            'review'       => $review 
        ]);
    }

	public function editprofile()
    {
        $imageUrl         = Config::get('constants.LANDLORD_IMAGE_URL');
        $noImageUrl       = Config::get('constants.TENANT_NOT_PROFILE_IMAGE_URL');
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $schools          = $this->getSchoolDetails();
        
        if($landlordProfile->image!='')
        {
            $profileImage=$imageUrl."/".$landlordProfile->image;
        }
        else
        {
            $profileImage=$noImageUrl."/landlord-blank.png";
        }
        $originalProfile=HTML::getLandlordProfileImage();
        return view('landlords.editprofile', [
            'pageTitle'     => 'EditProfile',
            'pageHeading'   => 'EditProfile',
            'pageMenuName'  => 'editprofile',
            'imageUrl'      => $imageUrl,
            'profile'       => $landlordProfile,
            'schools'       => $schools, 
            'profileImage'  => $originalProfile      
        ]);
    }
	public function getChangePassword(Request $request)
	{
		$id = $request->id;
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
		return view('landlords.changepassword',[
            'pageTitle'     => 'ChangePassword',
            'pageMenuName'  => 'changepassword',
            'profile'         => $landlordProfile,
            'profileImage'    => $originalProfile,
            ]);
	}
	
	public function postChangePassword(Request $request)
	{
		$this->validate($request, [
            'current_password' => 'required',
            'password'         => 'required|confirmed|min:6',
        ]);
        
        $userUpdate = $request->all();
        $value      = $userUpdate['current_password'];
        $landlord     = Auth::User('landlord');
       // print_r($tenant->password);exit;
        if (Hash::check($value, $landlord->password))
        {
			$userpassword = Hash::make($userUpdate['password']);
			$landlor      = DB::table('landlords')
                ->where('id', $landlord->id)
                ->update(['password' => $userpassword]);
            return redirect()->route('landlords.dashboard')->with('message-success', 'Your password has been updated.');

		}
	}
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $landlordsQuery = DB::table('landlords');
        if ($request->has('search')) {
            $search = $request->input('search');
            $landlordsQuery->where('firstname', 'like', '%' . $request->input('search') . '%');
        }
        $landlordsQuery->orderBy($sortby, $order);
        $landlords = $landlordsQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('landlords.index', ['pageTitle' => 'Landlords',
            'pageHeading'                                => 'Landlords',
            'pageDesc'                                   => 'Manage Landlords here',
            'landlords'                                  => $landlords,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
    }
    public function posteditprofile(Request $request)
    {
        //dd($request);
        $loggedLandlord = Auth::user('landlord');
        $rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'phone'                 => 'required',
            'school_id'             => 'required',
            'description'           => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails())
        {
            $input['firstname'] = $request->firstname;
            $input['lastname']  = $request->lastname;
            $input['location']  = $request->location;
            $input['phone']     = $request->phone;
            $input['about']     = $request->about;
            $input['school_id'] = $request->school_id;
            $input['website']   = $request->website;
            $input['state']     = $request->state;
            $input['address']   = $request->address;
            $input['language']  = $request->language;
            $input['description']  = $request->description;
            $input['city']      = $request->city;
            //this code is used to upload the image
            if($request->file('image'))
            {
                
                $imagePath = Config::get('constants.LANDLORD_IMAGE_URL');
                
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                //$imagePath = $imagePath . $loggedTenant->id;
                
                
                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(144, 144)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
            }
            
            
            $updateTenantDetails = Landlord::where('id', '=', $loggedLandlord->id)->update($input);
            
            //end of the image upload code
            return redirect()->route('landlords.profile')->with('message-success', 'Details are updated.');
        }
        else{
            return redirect()->route('landlords.editprofile')->withErrors($validator->errors())->withInput();
        }
    }
    public function posteditprofileimage(Request $request)
    {
        //dd($request->all());
        $loggedLandlord = Auth::user('landlord');
        
        if($request->file('image'))
            {
                
                $imagePath = Config::get('constants.LANDLORD_IMAGE_PATH');
                
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                //$imagePath = $imagePath . $loggedTenant->id;
                
                
                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(144, 144)->save($imagePath . "/".$filename);
                $input['image'] = $filename;
                $updateTenantDetails = Landlord::where('id', '=', $loggedLandlord->id)->update($input);
            
                //end of the image upload code
                return redirect()->route('landlords.editprofile')->with('message-success', 'Profile image has been updated.');
            }
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$school = $this->getSchoolDetails();
        return view('landlords.create', ['pageTitle' => 'Create Landlord',
            'pageHeading'                   => 'Create Landlord',
            'pageDesc'                      => 'Create Landlord here',
            'school'                        => $school,
        ]);
    }
    
    //these func is used for getting the school
    public function  getSchoolDetails()
     {
		 $school = DB::table('schools')->where('is_active',1)->lists('schoolname', 'id');
		 if(!empty($school))
		 {
			 return $school;
		 }else
		 {
			 return false;
		 }
	 }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
         $rules = [
            'firstname'             => 'required',
            'lastname'              => 'required',
            'school'                => 'required',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'email'                 => 'required|email|unique:landlords',
        ];
        $input     = $request->except('image');
        $imagePath = Config::get('constants.LANDLORD_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
			$input['firstname'] = $request->firstname;
			$input['lastname'] = $request->lastname;
			$input['school_id'] = $request->school;
			$input['email'] = $request->email;
            $input['password'] = bcrypt($input['password']);
            
            $registeredEmail = $this->isRegistered($input['email']);    
			if(count($registeredEmail) > 0)
			{
				return redirect()->route('landlords.create')->with('message-error', 'This email is already registered !');
			}
            $landlord = Landlord::create($input);

            if ($request->file('image')) {
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                $imagePath = $imagePath . $landlord['id'];
                if (!File::exists($imagePath)) {
                    // path does not exist
                    File::makeDirectory($imagePath, $mode = 0777, true, true);
                    File::copy(Config::get('constants.HTACCESS_FILE'), $imagePath . DS . '.htaccess');
                    File::makeDirectory($imagePath . '/60_x_60', $mode = 0777, true, true);

                }

                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(60, 60)->save($imagePath . '/60_x_60/' . $filename);
                // $userid = $user->id;
                $landlord = Landlord::find($landlord->id);
                $landlord->update(['image' => $filename]);

            }
            return redirect()->route('landlords.index')->with('message', 'Landlord Created.');
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }


	private function isRegistered($email)
    {
		$users = DB::select('SELECT email
							FROM tenants
							WHERE email ="'.$email.'"
							UNION
							SELECT email
							FROM landlords
							WHERE email = "'.$email.'"');
		return $users;
	}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $imageUrl       	= Config::get('constants.LANDLORD_IMAGE_URL');
        $noImageUrl			= Config::get('constants.LANDLORD_NOT_PROFILE_IMAGE_URL');
        $landlord           = Landlord::find($id);
        $school 			= $this->getSchoolDetails();
        
        list($landlord['year'], $landlord['month'], $landlord['day']) = explode('-', $landlord['dob']);
        $locations        = [];
        if ($landlord->location) {
            $locations = explode('//', $landlord->location);
        }
		
		
		if($landlord->image!='')
		{
			$profileImage=$imageUrl."/".$landlord->image;
		}
		else
		{
			$profileImage=$noImageUrl."/tenant-blank.png";
		}
        return view('landlords.edit', ['pageTitle' => 'Edit Landlord',
            'pageHeading'                               => 'Edit Landlord',
            'pageDesc'                                  => 'Edit Landlord here',
            'landlord'                                  => $landlord,
            'imageUrl'                                  => $imageUrl,
            'profileImage'								=> $profileImage,
            'school'                                    => $school,
            'locations'                                 => $locations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $rules = [
            'firstname'  => 'required',
            'lastname'	 => 'required',
            'school'	 => 'required',
            'email' 	 => 'required|email',
        ];

        $messages = [
            'firstname.required'  => 'First name is required.',
            'lastname.required'   => 'Last name is required.',
            'school.required'  	  => 'Please select the school.',
            'email.required' 	  => 'A valid email is required.',
        ];

       

        $input     = $request->except('image');
        $imagePath = Config::get('constants.LANDLORD_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {

            $landlord         = Landlord::find($id);
            
            $landlord->update($input);
            
            $landlordId = Landlord::find($landlord->id);
            
            if ($landlordId['id']) {
                 return redirect()->route('landlords.index')->with('message-success', 'Landlord Profile is updated.');
            } 
        
        }else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $landlord = Landlord::find($id);
        $landlord->delete();
        return redirect()->route('landlords.index')->with('message', 'Landlord Deleted.');
    }
    
    public function changeStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $landlord = Landlord::find($id);
        if ($landlord->is_active) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }

        $landlord->update($data);
        return redirect()->route('landlords.index')->with('message', 'Landlord Status Updated');
    }
    
    public function getForgotPassword(){
		$pageTitle = 'Forgot Password';
        return view('landlords.forgotpassword')->with(compact('pageTitle'));
	}
	
	public function postForgotPassword(Request $request){
		$this->validate($request, ['email' => 'required|email']);
        $postedEmail = $request['email'];
        $dataWithEmail = Landlord::where('email', '=', $postedEmail)->first();
        //$dataWithEmail = DB::table('landlords')->where('email',$postedEmail)->first();
        if(!empty($dataWithEmail)){
			$currentDate = Carbon::now();
			$key = $currentDate->getTimestamp();
			$password_token = hash_hmac('sha256', str_random(40), $key);
			$originalToken = $key."_".$password_token;
			$dataWithEmail->password_token = $originalToken;
			$dataWithEmail->save();
			$emailFromAddress = Config::get('constants.FORGOT_PASSWORD_EMAIL_FROM');
			$emailFromName = Config::get('constants.FORGOT_PASSWORD_EMAIL_FROM_NAME');
			
			\Mail::send('emails.passwordLandlords', ['token' => $originalToken], function($message) use ($dataWithEmail,$emailFromAddress,$emailFromName){
				$message->from($emailFromAddress,$emailFromName);
                $message->to($dataWithEmail->email, $dataWithEmail->name)
                        ->subject('reset your password');
            });
            return redirect()->route('landlords.forgotpassword')->with('message', 'Mail sent  please check your email');
            
		}else{
			return redirect()->back()->withErrors('message', 'not a valid email');
		}
	}
	
	public function getResetPassword($token){
		if($token!=''){
		$pageTitle = 'Password reset';
        return view('landlords.resetpassword')->with(compact(array('pageTitle','token')));
			
		}else{
			return redirect()->back()->withErrors('message', 'Token must be needed');
		}
	}
	
	public function postResetPassword(Request $request)
	{
		
		 $this->validate($request, [
            'token' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
        $credentials = $request->only(
            'password', 'password_confirmation', 'token'
        );
        
		$updatedPassword = DB::table('landlords')
						->where('password_token',$request->token)
						->update(['password' =>bcrypt($request->password)]);
						
		$updatedPassword = DB::table('landlords')
						->where('password_token',$request->token)
						->update(['password_token' =>'']);
		
		return redirect('landlords/login');
		
	}
	
	public function register(){
		return view('landlords.register');
	}
	
	public function signup(Request $request){
		
		$this->validate($request, [
            'name' 					=> 'required',
            'password' 				=> 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            'email'                 => 'required|email|unique:tenants'
        ]);
        
            $input['name']     = $request->name;
            $input['password'] = bcrypt($request->password);
            $input['email']    = $request->email;
            $input['gender']   = $request->gender;
            $input['dob']      = $request->year. "-" . $request->month . "-" . $request->day;
            $input['location'] = $request->location;
			
			$tenant = Landlord::create($input);
		
		
	}
    public function addProperty(){

        $loggedLandlordId = Auth::user('landlord')->id;
        $pageTitle        = "Property Add";
		$pageMenuName	  = 'dashboard';
        $school           = $this->getSchoolDetails();
        $originalProfile  = HTML::getLandlordProfileImage();
        $profileImage     = $originalProfile;
        $profile          = $this->getLandlordProfile($loggedLandlordId);
        $properties       = $this->getProperties( $loggedLandlordId );
        $aminities  	  = $this->getAminityDetails();
                                          
        return view('property.add')->with(compact('loggedLandlordId','pageTitle','pageMenuName','school','properties','profileImage','profile','aminities'));
    }
    
	 private function getAminityDetails()
	 {
		 $aminity = array();
		 $aminity  = DB::table('amenities')->where('status',1)->orderBy('amenity_name', 'asc')->lists('amenity_name', 'amenity_name');
		 return $aminity;
	 }
	 
    public function postProperty(Request $request){
        //dd($request->all());
        $rules = array(
            'property_name'             => 'required',
            'school_id'                 => 'required',
            'location'                  => 'required',
            'bedroom_no'                => 'required|numeric',
            'rent_price'                => 'required|numeric',
            'description'               => 'required',
            'communication_medium'      => 'required', 
            'distance_from_school'      => 'required',
        );
        $messages = [
                        'property_name.required'             => 'Property Name field is Required',
                        'school_id.required'                 => 'School Name field is Required',
                        'location.required'                  => 'Location field is Required',
                        'bedroom_no.required'                => 'Bedroom field is Required',
                        'bedroom_no.numeric'                 => 'Please enter a valid number in Bedroom',
                        'rent_price.required'                => 'Rent field is Required',
                        'rent_price.numeric'                 => 'Please enter a valid amount as Rent',
                        'description.required'               => 'Description field is Required',
                        'communication_medium.required'      => 'Communication Medium field is Required',
                        'distance_from_school.required'      => 'Please enter a valid distance'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            $loggedLandlord                      =  Auth::user('landlord');
            $landLordName                        =  $loggedLandlord->firstname.','.$loggedLandlord->lastname;
            $loggedLandlordId                    =  $loggedLandlord->id;    
            $insertArray                         =  array();
            $insertArray['property_name']        =  $request->property_name;
            $insertArray['landlord_id']          =  $loggedLandlordId;   
            $insertArray['school_id']            =  $request->school_id;
            $insertArray['location']             =  $request->location;
            $insertArray['bedroom_no']           =  $request->bedroom_no;
            $insertArray['bathroom_no']          =  $request->bathroom_no;
            $insertArray['rent_price']           =  $request->rent_price;
            $insertArray['description']          =  $request->description;
            $insertArray['communication_medium'] =  $request->communication_medium;
            $insertArray['distance_from_school'] =  $request->distance_from_school;
            //list($month, $day, $year)            =  explode('/', $request->expireon);
            list($latitude, $longitude)          =  explode('~', $request->hdPlace);
            //$insertArray['expireon']             =  $year.'-'.$month.'-'.$day;
            $insertArray['latitude']             =  $latitude;
            $insertArray['longitude']            =  $longitude;
            //dd($insertArray);
            $school    = School::where('id',$request->school_id)
                                ->select('schoolname')->first();
            $searchField  = $request->property_name.','.$school->schoolname.','.$request->description.','.$request->location.','.$landLordName;                    
            $insertArray['search_field']       =  $searchField;
            $property = Property::create($insertArray);
            //mail sending to tenants if the property matches there saved search criteria and the school is mandatory
            $matches  = $this->getMatchesTenants( $request->school_id, $loggedLandlordId, $request->bedroom_no, $request->rent_price, $request->distance_from_school);
            //dd( $matches );
            if( count($matches) ){
               foreach( $matches as $match ){
                    $landLordName  = $loggedLandlord->firstname.' '.$loggedLandlord->lastname;
                    $tenantName    = $match->tenant->firstname.' '.$match->tenant->lastname;
                    $tenantEmail   = $match->tenant->email;
                    Mail::send('emails.propertyMatchesForTenantSearch', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject('TenantU - Matching property found.');
                     });
               }    
            }
            if( $property && $request->img_name!="" ){
                $images   = explode(',',$request->img_name);
                //dd($images);
                foreach ($images as $image) {
                    $propertyImage = new propertyImage;
                    $propertyImage->property_id = $property->id;
                    $propertyImage->image_name  = $image;
                    $propertyImage->status = 1;
                    $propertyImage->save();
                }
                /* code for image resize */
                $propertyImages = PropertyImage::where('property_id',$property->id)
												->select('image_name')
												->get();
				
				$imagePath 			= Config::get('constants.PROPERTY_IMAGE_PATH');
				
				foreach($propertyImages as $propertyImage)
				{
					$img = Image::make($imagePath.$propertyImage->image_name)->resize(800,485);
					$img->save();
					$imgSquare = Image::make($imagePath.'/square/'.$propertyImage->image_name)->resize(300,300);
					$imgSquare->save();
					$imgThumb = Image::make($imagePath.'/thumb/'.$propertyImage->image_name)->resize(300,300);
					$imgThumb->save();
				}
				/* end of image resize */

            }
            //dd($request->amenities);exit;
            $amenityArray = [];
            if($property && $request->aminity!=""){
                foreach ($request->aminity as $amenity) {
                    $amenityArray[] = $amenity;
                    $data[] = array('property_id'=>$property->id,
                                'amenity'=>$amenity,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'));
                }
                    
                $success = PropertyAmenity::insert($data);
            }
            if(!empty($amenityArray)){
               $amenities = implode(',', $amenityArray); 
               $propertyUpdate  = Property::where('id',$property->id)
                                            ->update(['amenities'=>$amenities]); 
            }
            //return redirect()->back()->with('message-success','Property posted successfully!');
            return redirect()->route('property.edit',[$property->id])->with('message-success','You can add more details about the property here!');
        }
        
    }
    public function getMatchesTenants( $schoolId, $landlordId, $bedRoom, $price, $distance ){
        //DB::connection()->enableQueryLog(); 
        $matches = Mysearch::where('school_id',$schoolId)
                        ->where('bedrooms','<=',$bedRoom)
                        ->whereRaw( "$distance <= distance" )
                        ->whereRaw( "$price between price_form and price_to" )
                        ->with('tenant')
                        ->select('tenant_id')
                        ->addSelect('landlord_id')
                        ->get();
        return $matches;               
                        
        //dd($matches);                
        /*$queries = DB::getQueryLog();
        dd($queries);*/                
    }
    public function upload(){
        //require('UploadHandler.php');
        $upload_handler = new UploadHandler();
        //$reponseArray   = json_decode($upload_handler);
        //print_r($upload_handler->testVar);
        // print_r("sssssssssssssssssssssssss");
    }
    public function getMyContact(){
     $loggedLandlord = Auth::user('landlord');
     $loggedLandlordId     = $loggedLandlord->id;
     $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
     $pageTitle            = "Contacts"; 
     $originalProfile=HTML::getLandlordProfileImage(); 
	//this method calling is for the landlords frm msg thread 
	$tenants = $this->getLandTenantCorrespondingLandlord($loggedLandlord->id);
                
        return view('landlords.contacts', [
            'pageTitle'     => 'My Contacts',
            'pageHeading'   => 'My Contacts',
            'pageMenuName'	=> 'mytenantcontact',
            'tenants'       => $tenants,
            'profileImage'    => $originalProfile,
            'profile'         => $landlordProfile
        ]);
    
    }
    public function getLandTenantCorrespondingLandlord( $landlordId )
    {
  
     $tenats = MessageThread::from('message_threads AS MT')
                                 ->select('MT.tenant_id','T.firstname','T.lastname','T.about','T.slug','T.city')
                                 ->leftjoin('tenants AS T', 'MT.tenant_id', '=', 'T.id')
                                 ->where('MT.landlord_id', $landlordId)
                                 ->groupBy('MT.tenant_id')
                                 ->get();
      //code by sooraj for getting the latest msg thread id
      foreach($tenats as $tenant)
      {
		  $tenantId = $tenant->tenant_id;
		  $tenant->latestMsgThreadId = $this->getLatestMessageThreadId($tenantId,$landlordId);
	  }
      //end of the code
      return $tenats;                              
                               
    }
    
    //code by sooraj for fetch latest msg thread id
    public function getLatestMessageThreadId($tenantId,$landlordId)
	{
		 $landlords = DB::select('SELECT message_thread_id FROM messages WHERE tenant_id ='.$tenantId.' AND landlord_id='.$landlordId.' ORDER BY created_at DESC LIMIT 1 ');
         return  $landlords[0];
	}
    
    
    public function getMyTenants( $landlordId, $id=null )
    {
  
     $myTenant = LandlordTenant::where('landlord_id',$landlordId)
                               ->with('tenant') 
                               ->with('property');
                              
     //dd( $tenants );
      if($id){
            $myTenant->where( 'property_id', $id );
      }                        
      $tenants =  $myTenant->get();
      return $tenants;                              
                               
    }
    public function newmessage( $id,$propertyId=null ){
        $loggedLandlord   = Auth::user('landlord');
        
        $name = $this->getTenantNameFromId($id);
        
        $messageThreads = $this->getMyMessageThreads($id);
        $properties     = $this->getPropertiesForDropDown( $loggedLandlord->id );
        $threads        = $this->getThreads();

        foreach ($properties as $property)
        {
            $items[$property->id] = $property->property_name;
        }
        foreach ($threads as $thread)
        {
            $threadDropdown[$thread->id] = $thread->thread_name;
        }
        $proId = "";
        if( count( $properties ) ==1 ){
            $proId  = $properties[0]->id;
        }

        if($propertyId=='')
        {
            $propertyCount = $this->getPropertyCount($id);
            $landLordPropertyList = $this->getPropertyList($id);
            
            $propertyList= array();
            $propertyKey = array();
            foreach($landLordPropertyList as $key=>$val)
            {
                
                $propertyList[$val['id']]=$val['property_name'];
                $propertyKey=$val['id'];
            }
            $originalProfile=HTML::getLandlordProfileImage();
            return view('landlords.newmessage', [
                    'pageTitle'             => 'newmessage',
                    'pageMenuName'			=> 'newmessage',
                    'pageHeading'           => 'newmessage',
                    'landLordPropertyList'  => $landLordPropertyList,
                    'profile'               => $loggedLandlord,
                    'tenantId'              => $id,
                    'tenantName'            => $name,
                    'propertyList'          => $propertyList,
                    'propertyKey'           => $propertyKey,
                    'messageThreadsObj'     => $messageThreads,
                    'messageFlag'           => 'compose',
                    'profileImage'          => $originalProfile,
                    'properties'            => $items,
                    'threads'               => $threadDropdown,
                    'messageThreadId'       => "",
                    'propertyId'           => $proId
                ]);
        }
    }
        //this function is used for the  saving the message to message and message thread tables
    public function postMessage(Request $request)
    {
        
        //dd($request);
        
        $rules = array(
            'property'         => 'required',
            'messageType'      => 'required',     // required and must be unique in the ducks table
            'message_content'  => 'required'
        );
        
        $validator = Validator::make($request->all(), $rules);
        
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            //$input['message_thread_id'] = $request->messageType;
            $input['message']           = $request->message_content;
            $input['tenant_id']         = $request->tenantId;
            $input['sender']            = $request->sender;
            $input['landlord_id']       = Auth::user('landlord')->id;            
            
            $inputArr['thread_id']      = $request->messageType;
            $inputArr['property_id']    = $request->property;
            $inputArr['landlord_id']    = Auth::user('landlord')->id;      
            $inputArr['tenant_id']      = $request->tenantId;
            //dd($inputArr);exit;
            $tenants   = Tenant::find( $request->tenantId );
            $landlords = Landlord::find( Auth::user('landlord')->id );
            $landLordName = $landlords->firstname.' '.$landlords->lastname;
            $tenantName   = $tenants->firstname.' '.$tenants->lastname;
            $tenantEmail = $tenants->email;
            $messageThread = MessageThread::create($inputArr);
            if(isset($messageThread))
            {
                $message = new Message;
                $message->message_thread_id = $messageThread->id;
                $message->message           = $input['message'];
                $message->landlord_id       = $input['landlord_id'];
                $message->tenant_id         = $input['tenant_id'];
                $message->sender            = $input['sender'];
                
                $message->save();
                $messageFlag = '1';
                $threads = Thread::find( $request->messageType );
                $subject="";
                if($threads->flag==0){
                    $subject = "TenantU - You have received an enquiry message.";     
                }
                else{
                    $subject = "TenantU - You have received an issue message.";
                }
                Mail::send('emails.enquirythreadFromLandlord', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail,$subject) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject($subject);
                });
            }
            //print_r(route('tenants.getMessageList',[$messageThread->id]));exit;
            return redirect()->route('landlords.getMessageList',[$messageThread->id]);
        }
        
        
    }
    public function getMessageList(Request $request,$messageThreadId)
    {
        $loggedLandlord   = Auth::user('landlord');
        $tenants  = MessageThread::where('id',$messageThreadId)
                                    ->select('tenant_id')
                                    ->addSelect('thread_id')
                                    ->addSelect('property_id')
                                    ->addSelect('created_at')
                                    ->addSelect('status')
                                    ->first();
        //dd($tenants);  
        $threadFlag            = Thread::where('id',$tenants->thread_id)
                                    ->select('flag')
                                    ->first();    

        $name = $this->getTenantNameFromId($tenants->tenant_id);
        $threads  = $this->getMessageThreadName( $tenants->thread_id );
        $msgPropertyName = $this->getMessageThreadPropertyName($tenants->property_id);
        $formattedMsgThreadCreatedDate 	= $this->getFormattedMsgThreadCreatedDate($tenants->created_at);
        //dd($threads);
        
        $allMessages = $this->getAllMessagesWithThreadLandlord($messageThreadId,$loggedLandlord->id);
        $originalProfile=HTML::getLandlordProfileImage();

     
        $landlordProfile      = $this->getLandlordProfile($loggedLandlord->id);
        
        // $landlordName = $this->getLandLordName($allMessages[0]['landlord_id']);
        
        // $landlordCity = $this->getLandLordCity($allMessages[0]['landlord_id']);
        
        $messageThreads = $this->getMyMessageThreads($tenants->tenant_id);
        //dd( $messageThreads );
        $readFlag       = $this->getChangeReadFlag( $messageThreadId );
        if(!empty($allMessages))
        {
            return view('landlords.newmessage', [
                    'pageTitle'             => 'Messages',
                    'allMessages'           => $allMessages,
                    'tenantName'            => $name,
                    'landlordId'            => $loggedLandlord->id,
                    'pageHeading'           => 'messagelist',
                    'tenantId'              => $allMessages[0]['tenant_id'],
                    'messageThreadId'       => $messageThreadId,
                    'messageThreadsObj'     => $messageThreads,
                    'pageMenuName'          => 'messagelist',
                    'messageFlag'           => 'list',
                    'profileImage'          => $originalProfile,
                    'profile'               => $landlordProfile,
                    'threads'               => $threads,
                    'threadStatus'          => $tenants->status,
                    'threadTypeFlag'        => $threadFlag->flag,
                    'msgThreadCreatedDate'  => $formattedMsgThreadCreatedDate,
                    'msgPropertyName'		=> $msgPropertyName->property_name,
                ]);
        }
    }
    
    public function composeMessageList(Request $request,$messageThreadId)
    {
        //dd($request->all());
        
        $loggedLandlord   = Auth::user('landlord');
        $tenantId         = $request->tenantId;
        $tenants   = Tenant::find( $tenantId );
        $tenantEmail = $tenants->email;
        $tenantName   = $tenants->firstname.' '.$tenants->lastname;
        $landLordName = $loggedLandlord->firstname.' '.$loggedLandlord->lastname;
        if($request->buttonValue=='Processing'){
            $messageThreads = MessageThread::find($messageThreadId);
            $messageThreads->status = 1;
            $messageThreads->save();
            $message = "On Going";
            Mail::send('emails.enquirythreadCompleted', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject('TenantU - Enquiry response-processing.');
            });
        }
        else if($request->buttonValue=='Completed'){
            $messageThreads = MessageThread::find($messageThreadId);
            $Actialduration = $messageThreads->duration;
            $createdDate    = $messageThreads->created_at;
            $endDate        = time();
            $yourDate       = strtotime($createdDate);
            $datediff       = $endDate - $yourDate;
            $duration       = floor($datediff/(60*60*24));
            $data['rating_id']     =  2;
            $data['tenant_id']     = $messageThreads->tenant_id;
            $data['property_id']   = $messageThreads->property_id;
            if( $duration <= $Actialduration ){
                $data['rate_value']   = 5; 
                //here comes the logic for recomended property
                //land lord id comes here from property id
                
			
                
            }
            else{

               $data['rate_value']    = 2; 
            }
            //for showing 'would recommend' sort option in search
            $updateRecomended = $this->updateLandlordRating($loggedLandlord->id);
            
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $success = RatingUser::insert($data);
            $messageThreads->status = 2;
            $messageThreads->save();

            $message = "Completed";
            $messageRating = "Tenantu.com has given a rating(Landlord) of ".$data['rate_value']." out of 5 for solving this issue.";
            Mail::send('emails.enquirythreadCompleted', ['landLordName'=>$landLordName,'tenantName'=>$tenantName], function ($message) use ($tenantName,$tenantEmail) {
                    $message->to($tenantEmail, $tenantName)
                            ->subject('TenantU - Enquiry response-Completed.');
            });
            $avgRateVal = $this->getAverageRating($messageThreads->property_id,2);
            $ratingproperties = DB::update('UPDATE properties SET rating_2='.$avgRateVal.' WHERE id='.$messageThreads->property_id); 
            $totalAvgRateVal = $this->getAverageRating( $messageThreads->property_id );
            $ratingproperties = DB::update('UPDATE properties SET rating_avg='.$totalAvgRateVal.' WHERE id='.$messageThreads->property_id); 
            
            //system generating rating value to landlord
			DB::table('messages')->insert(
					['message_thread_id' => $messageThreadId, 'message' =>$messageRating,'landlord_id'=>$loggedLandlord->id,'tenant_id'=>$tenantId,'sender'=> 'T','created_at'=>Carbon::now()]
			);
        }
        else{
            $message          = $request->message;
        }
            
        
        DB::table('messages')->insert(
            ['message_thread_id' => $messageThreadId, 'message' =>$message,'landlord_id'=>$loggedLandlord->id,'tenant_id'=>$tenantId,'sender'=> 'L','created_at'=>Carbon::now()]
        );
        
        
        return redirect()->route('landlords.getMessageList',[$messageThreadId]);
    }
   
    
    private function getAverageRating($propertyId,$rateId=null)
    {
        if($rateId!='')
        {
            $whereCondition = ' WHERE property_id='.$propertyId.' AND rating_id='.$rateId;
        }
        else
        {
            $whereCondition = ' WHERE property_id='.$propertyId;
        }
        
        $avgRate = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user'.$whereCondition );
        $avgRateValue = $avgRate[0]->rate_value;
        $avgRateValueRounded = ceil($avgRateValue);
        
        return $avgRateValueRounded;
    }
    public function getChangeReadFlag($messageThreadId)
    {
        
        $message = Message::where('message_thread_id', $messageThreadId)
                            ->where('sender','T')
                            ->update(['read_flag' => 1]);
    }
    private function getMyMessageThreads($tenantId)
    {
        $loggedLandlord   = Auth::user('landlord');
        $messageThreads = MessageThread::from('message_threads AS MT')
                                 ->select('MT.id','MT.property_id','MT.created_at','MT.tenant_id', 'T.thread_name','T.image','P.property_name')
                                 ->join('threads AS T', 'MT.thread_id', '=', 'T.id')
                                 ->join('properties AS P','MT.property_id','=','P.id')
                                 ->where('MT.landlord_id', $loggedLandlord->id)
                                 ->where('MT.tenant_id',$tenantId)
                                 ->orderBy('MT.created_at')
                                 ->get();
                            
        return $messageThreads;
    }
    public function addTenant(){
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
        
        $tenants          = MessageThread::where('landlord_id',$loggedLandlord->id)
                                          ->with(['tenant'=>function($q){
													$q->where( 'is_active',1 );		
												 }])
                                          ->groupBy('tenant_id')
                                          ->get();
                                           
       
        for($i=1;$i<31;$i++){
            $dueOn[$i] = $i.' Every month';
        }
        $tenantDropDown = array();
        $propertyDropdown =array();
        if(count($tenants)){
			foreach ($tenants as $tenant)
			{
				if(!empty($tenant->tenant)){
					$tenantDropDown[$tenant->tenant->id] = $tenant->tenant->firstname.' '.$tenant->tenant->lastname;
				}
			}
		}
        $properties = $this->getPropertiesForDropDown( $loggedLandlord->id );
        foreach ($properties as $property)
        {
            $propertyDropdown[$property->id] = $property->property_name;
        }
       $cycleDropdown = ['3months'=>"3 Month",
                         '6months'=>"6 Month",
                         '1year' =>"1 Year",
                         '2year' =>"2 Year",
                         '3year' =>"3 Year"];
        //dd( $tenantDropDown );                               
            return view('landlords.addtenant', [
                'pageTitle'       => 'Add Tenant',
                'pageMenuName'	  => 'mytenants',
                'pageHeading'     => 'Add Tenant',
                'profile'         => $landlordProfile,
                'profileImage'    => $originalProfile,
                'tenantDropDown'  => $tenantDropDown,
                'dueOn'           => $dueOn,
                'propertyDropdown'=> $propertyDropdown,
                'cycleDropdown'   => $cycleDropdown      
            ]);
    }
    public function postTenant( Request $request ){
           $rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'month_rent'                => 'required|numeric',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'month_rent.numeric'                 => 'Please enter a valid number in Monthly rent',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            //dd($request->all());
            //DB::connection()->enableQueryLog(); 
            $loopCount = "";
            if(($request->cycle)!=""){
                switch($request->cycle){
                       case "3months":
                         $loopCount = 3;
                       break; 
                       case "6months":
                         $loopCount = 6;
                       break;
                       case "1year":
                         $loopCount = 12;   
                       break;
                       case "2year":
                         $loopCount = 24;
                       break;
                       case "3year":
                          $loopCount =36;
                       break;    
                }
            }

            $insertArray                             =  [];
            $insertArray['landlord_id']              =  Auth::user('landlord')->id;
            $insertArray['tenant_id']                =  $request->tenant_id;   
            $insertArray['property_id']              =  $request->poperty_id;
            $insertArray['month_rent_initial']       =  $request->month_rent;
            $insertArray['due_on']                   =  $request->due_on;
            $insertArray['cycle']                    =  $request->cycle;
            list($monthStart, $dayStart, $yearStart) =  explode('/', $request->agriment_start_date);
            $startYear                               =  $yearStart.'-'.$monthStart.'-'.$dayStart;
            $insertArray['agreement_start_date']     =  $startYear;

            $totalDurationStart  = date("Y-m-d", strtotime("+0 month", strtotime($startYear)));

            $totalDurationEnd  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
            $insertArray['agreement_end_date']     =  $totalDurationEnd;
            $myTenantExist                           =  LandlordTenant::where('tenant_id',$request->tenant_id)
                                                        //->where('agreement_start_date','<=',$totalDurationEnd)
                                                        ->where(function($q) use ( $totalDurationStart, $totalDurationEnd){
                                                          $q->whereBetween('agreement_start_date', array($totalDurationStart, $totalDurationEnd));
                                                          $q->orWhereBetween('agreement_end_date', array($totalDurationStart, $totalDurationEnd));  
                                                        })
                                                        
                                                        ->where('landlord_id',Auth::user('landlord')->id)
                                                        ->count(); 
            //$queries = DB::getQueryLog(); 
            //dd($queries);                                            
            //dd($myTenantExist); 
            
            if( $myTenantExist == 0 ){
                $property = LandlordTenant::create($insertArray);
                if($loopCount!=""){
                    $billDate = $yearStart.'-'.$monthStart.'-'.$request->due_on;
                    //echo $billDate;exit;
                    for($i=1;$i<=$loopCount;$i++){
                        $uId = uniqid();
                        $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));
                        
                        $data[] = array('landlord_tenant_id'=>$property->id,
                                        'bill_on'=>$nextBillDate,
                                        'amount'=>$request->month_rent,
                                        'unique_id'=>$uId,
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'));
                    }
                    $success = landlordTenantMonthlyrent::insert($data);
                }
                
                return redirect()->route('landlords.viewtenant',$property->id)->with('message-success','Tenant added successfully!');        
            }
        else{
           return redirect()->route('landlords.addtenant')->with('message-error','You cannot add this tenant for this period!'); 

        }
        }

    }
    public function viewTenant( $id ){

        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
                
        //dd( $tenantDropDown ); 
        $landlordTenant     =  LandlordTenant::where('landlord_id',$loggedLandlord->id)
                                            ->where('id',$id)
                                            ->with('landlord_tenant_monthlyrents')
                                            ->with('landlord_tenant_docs')
                                            ->with('landlord_tenant_otherpayments')
                                            ->with('landlord_tenant_otherpayment_details')
                                            ->with('tenant')
                                            ->with('property')
                                            ->first(); 
        //dd( $landlordTenant );                                   
        if(count($landlordTenant) > 0){

            return view('landlords.viewtenant', [
                'pageTitle'       => 'Add Tenant',
                'pageHeading'     => 'Add Tenant',
                'pageMenuName'    => 'viewtenant', 
                'profile'         => $landlordProfile,
                'profileImage'    => $originalProfile,
                'landlordTenant'  => $landlordTenant       
            ]);
        }
        else{
            return redirect('/404');
        }
    }
    public function editTenant($id){
        $loggedLandlord   = Auth::user('landlord');
        $landlordProfile  = $this->getLandlordProfile($loggedLandlord->id);
        $originalProfile  = HTML::getLandlordProfileImage();
        $tenants          = MessageThread::where('landlord_id',$loggedLandlord->id)
                                          ->with('tenant')
                                          ->groupBy('tenant_id')
                                          ->get();
        //dd( $tenants );
        for($i=1;$i<31;$i++){
            $dueOn[$i] = $i.' Every month';
        }
        $tenantDropDown = array();
        foreach ($tenants as $tenant)
        {
            $tenantDropDown[$tenant->tenant->id] = $tenant->tenant->firstname.' '.$tenant->tenant->lastname;
        }
        $properties = $this->getPropertiesForDropDown( $loggedLandlord->id );
        foreach ($properties as $property)
        {
            $propertyDropdown[$property->id] = $property->property_name;
        }
       $cycleDropdown = ['3months'=>"3 Month",
                         '6months'=>"6 Month",
                         '1year' =>"1 Year",
                         '2year' =>"2 Year",
                         '3year' =>"3 Year"];
        //dd( $tenantDropDown ); 
        $landlordTenant     =  LandlordTenant::where('landlord_id',$loggedLandlord->id)
                                            ->where('id',$id)
                                            ->with('landlord_tenant_monthlyrents')
                                            ->with('landlord_tenant_docs')
                                            ->with('landlord_tenant_otherpayments')
                                            ->with('tenant')
                                            ->with('property')
                                            ->with('landlord_tenant_otherpayment_details')
                                            ->first(); 
        //dd( $landlordTenant );                                                                  
            return view('landlords.edittenant', [
                'pageTitle'       => 'Edit Tenant',
                'pageHeading'     => 'Edit Tenant',
                'pageMenuName'    => 'edittenant', 
                'profile'         => $landlordProfile,
                'profileImage'    => $originalProfile,
                'tenantDropDown'  => $tenantDropDown,
                'dueOn'           => $dueOn,
                'propertyDropdown'=> $propertyDropdown,
                'cycleDropdown'   => $cycleDropdown,
                'landlordTenant'  => $landlordTenant      
            ]);
    }
    public function changeRent( Request $request ){
        //print_r($request->all());
        $id                                   = $request->pkId;
        $amount                               = $request->amount;
        $landlordTenantRent                   = landlordTenantMonthlyrent::find( $id );
        if($landlordTenantRent->update(['amount' => $amount])){
           return "1"; 
        }
        else{
           return "0";
        }
    }
    public function updateDocData( Request $request ){
        //print_r($request->all());
        $id                                   = $request->pkId;
        $amount                               = $request->amount;
        $title                                = $request->title;
        $flag                                 = $request->flag;
        $description                          = $request->description;
        $landlordTenantDoc                    = landlordTenantDoc::find( $id );
        if($flag=='update'){
            if($landlordTenantDoc->update(['doc_title' => $title,'doc_description'=>$description])){
               return "1"; 
            }
            else{
               return "0";
            }
        }
        else{
            $docName  = $landlordTenantDoc->file_name;
            $destinationPath = Config::get('constants.TENANT_DOC_PATH');
            unlink($destinationPath.$docName);
            if($landlordTenantDoc->delete()){
                return "1";
            }
            else{
                return "0";
            }
        }    
    }
    public function updateItineraryData( Request $request ){
        //print_r($request->all());
        $id                                   = $request->pkId;
        $amount                               = $request->amount;
        $title                                = $request->title;
        $landlordTenantOtherpaymentDetail     = landlordTenantOtherpaymentDetail::find( $id );
        if($landlordTenantOtherpaymentDetail->update(['amount' => $amount,'title'=>$title])){
           return "1"; 
        }
        else{
           return "0";
        }
    }
    public function postEditTenant( Request $request, $id ){
        //dd($request->all());
           $rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
            //dd($request->all());
            $loopCount = "";
            if(($request->cycle)!=""){
                switch($request->cycle){
                       case "3months":
                         $loopCount = 3;
                       break; 
                       case "6months":
                         $loopCount = 6;
                       break;
                       case "1year":
                         $loopCount = 12;   
                       break;
                       case "2year":
                         $loopCount = 24;
                       break;
                       case "3year":
                          $loopCount =36;
                       break;    
                }
            }
            $landlordTenantRent                       = LandlordTenant::find( $id );
            $landlordTenantRent->landlord_id          =  Auth::user('landlord')->id;
            $landlordTenantRent->tenant_id            =  $request->tenant_id;   
            $landlordTenantRent->property_id          =  $request->poperty_id;
            $landlordTenantRent->month_rent_initial   =  $request->month_rent;
            $landlordTenantRent->due_on               =  $request->due_on;
            $landlordTenantRent->cycle                =  $request->cycle;
            list($monthStart, $dayStart, $yearStart)  =  explode('/', $request->agriment_start_date);
            $startYear                                =  $yearStart.'-'.$monthStart.'-'.$dayStart;
            $landlordTenantRent->agreement_start_date =  $startYear;

            //$totalDuration  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
            $totalDurationStart  = date("Y-m-d", strtotime("+0 month", strtotime($startYear)));

            $totalDurationEnd  = date("Y-m-d", strtotime("+".$loopCount." month", strtotime($startYear)));
            $landlordTenantRent->agreement_end_date     =  $totalDurationEnd;
            //DB::connection()->enableQueryLog();
            $myTenantExist                           =  LandlordTenant::where('tenant_id',$request->tenant_id)
                                                        //->where('agreement_start_date','<=',$totalDuration)
                                                        ->where(function($q) use ( $totalDurationStart, $totalDurationEnd){
                                                          $q->whereBetween('agreement_start_date', array($totalDurationStart, $totalDurationEnd));
                                                          $q->orWhereBetween('agreement_end_date', array($totalDurationStart, $totalDurationEnd));  
                                                        })
                                                        ->where('landlord_id',Auth::user('landlord')->id)
                                                        ->where('id','!=',$id)
                                                        ->count();
            //$queries = DB::getQueryLog(); 
            //dd($queries);                                            
            //dd($myTenantExist);                                                
            if($myTenantExist==0){
            $property = $landlordTenantRent->update();
            if($loopCount!=""){
                $billDate = $yearStart.'-'.$monthStart.'-'.$request->due_on;
                $landlordTenantMonthlyRent = landlordTenantMonthlyrent::where('landlord_tenant_id', $id ); 
                //landlordTenantOtherpaymentDetail code starts here
                $this->updateLandlordTenantOtherPaymentdetails( $id, $startYear, $request->cycle, $request->due_on );
                $landlordTenantMonthlyRent->delete();
                for($i=1;$i<=$loopCount;$i++){
                    $uId  = uniqid();
                    $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));
                    
                    $data[] = array('landlord_tenant_id'=>$id,
                                    'bill_on'=>$nextBillDate,
                                    'amount'=>$request->month_rent,
                                    'unique_id'=>$uId,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s'));
                }
                $success = landlordTenantMonthlyrent::insert($data);
            }
            
            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Property posted successfully!');        
        }
        else{
            return redirect()->route('landlords.edittenant',$id)->with('message-error','You cannot add this tenant for this period!');        
        }
        }
    }
    public function updateLandlordTenantOtherPaymentdetails( $id, $startYear, $cycle, $due ){
        $landlordTenantRent                  = landlordTenantOtherpayment::where( 'landlord_tenant_id',$id )
                                                                          ->get(); 
        if(count($landlordTenantRent)){  

            landlordTenantOtherpayment::where('landlord_tenant_id', $id)          
                                        ->update(['agreement_start_date' => $startYear,
                                                  'due_on'               => $due ,
                                                  'cycle'                => $cycle
                                            ]);    
            $landlordTenantOtherpayments                  = landlordTenantOtherpayment::where( 'landlord_tenant_id',$id )
                                                                              ->get();
             //dd($landlordTenantOtherpayments);
             foreach( $landlordTenantOtherpayments as $landlordTenantOtherpayment){
                    $deleteSuccess = landlordTenantOtherpaymentDetail::where('landlord_tenant_otherpayment_id', $landlordTenantOtherpayment->id)->delete();

             //} 
             

                /*$landlordTenantOtherpayments = landlordTenantOtherpayment::where('landlord_tenant_id', $id )->get();*/                                                                                          
                //foreach($landlordTenantOtherpayment as $landlordTenantOtherpayment){
                
                /*$data[] = array('landlord_tenant_id'=>$id,
                                'agreement_start_date'=>$landlordTenantRent->agreement_start_date,
                                'cycle'=>$landlordTenantRent->cycle,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'));*/
                /*$insertArray['landlord_tenant_id']   = $id;
                $insertArray['agreement_start_date'] = $landlordTenantOtherpayment->agreement_start_date;
                $insertArray['cycle']                = $landlordTenantOtherpayment->cycle;
                $insertArray['title']                = $landlordTenantOtherpayment->title ;
                $insertArray['due_on']               = $landlordTenantOtherpayment->due_on;
                $insertArray['initial_amount']       = $landlordTenantOtherpayment->initial_amount;
                $success                             = landlordTenantOtherpayment::create($insertArray);*/
                  
                //$success = landlordTenantOtherpayment::insert( $data );
                //if( $deleteSuccess ){
                    //echo "1";exit;
                    $loopCount = "";
                    if(($landlordTenantOtherpayment->cycle)!=""){
                        switch($landlordTenantOtherpayment->cycle){
                               case "3months":
                                 $loopCount = 3;
                               break; 
                               case "6months":
                                 $loopCount = 6;
                               break;
                               case "1year":
                                 $loopCount = 12;   
                               break;
                               case "2year":
                                 $loopCount = 24;
                               break;
                               case "3year":
                                  $loopCount =36;
                               break;    
                        }
                    }
                    if($loopCount!=""){
                        list( $yearStart, $monthStart, $dayStart ) =  explode('-', $landlordTenantOtherpayment->agreement_start_date);
                        $billDate = $yearStart.'-'.$monthStart.'-'.$landlordTenantOtherpayment->due_on;
                        //echo $billDate;exit;
                        for($i=1;$i<=$loopCount;$i++){
                            
                            $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));
                            
                            $data[] = array('landlord_tenant_otherpayment_id'=>$landlordTenantOtherpayment->id,
                                            'bill_on'=>$nextBillDate,
                                            'amount'=>$landlordTenantOtherpayment->initial_amount,
                                            'title'=>$landlordTenantOtherpayment->title,
                                            'created_at'=>date('Y-m-d H:i:s'),
                                            'updated_at'=>date('Y-m-d H:i:s'));
                        }
                        
                    }
                //}
            }
            $success = landlordTenantOtherpaymentDetail::insert($data);
            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Data successfully posted!');
        }
    }
    public function postEditTenantDoc( Request $request, $id ){
        //dd($request->all());
           /*$rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {*/
            $files = Input::file('file_name');
            
            // Making counting of uploaded images
            $fileCount = count($files);
            // start count how many uploaded
            foreach( $files as $key=>$file ) {
              $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
              $validator = Validator::make(array('file'=> $file), $rules);
              if($validator->passes()){
                $destinationPath = Config::get('constants.TENANT_DOC_PATH');
                $extension = $file->getClientOriginalExtension();
                $filename  = trim(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename.uniqid(), "-") . '.' . $extension;
                $upload_success = $file->move($destinationPath, $filename);
                $data[] = array('landlord_tenant_id'=>$id,
                                        'doc_title'=>$request->doc_title[$key],
                                        'file_name'=>$filename,
                                        'doc_description'=>$request->doc_description[$key],
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'));
                $success = landlordTenantDoc::insert($data);
              }
            }
            
            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Documents successfully posted!');
        //}
    }
     public function postEditTenantItinerary( Request $request, $id ){
        //dd($request->all());
           /*$rules = array(
            'tenant_id'                 => 'required',
            'due_on'                    => 'required',
            'poperty_id'                => 'required',
            'agriment_start_date'       => 'required',
            'cycle'                     => 'required',
        );
        $messages = [
                        'tenant_id.required'                 => 'Tenant field is Required',
                        'due_on.required'                    => 'Due on field is Required',
                        'poperty_id.required'                => 'Property field is Required',
                        'month_rent.required'                => 'Monthly rent field is Required',
                        'agriment_start_date.required'       => 'Start date field is Required',
                        'cycle.required'                     => 'Cycle field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {*/
            $titles  = $request->title;
            $amounts = $request->amount;
            $landlordTenantRent                  = LandlordTenant::find( $id );
            foreach($titles as $key => $title){
            
            /*$data[] = array('landlord_tenant_id'=>$id,
                            'agreement_start_date'=>$landlordTenantRent->agreement_start_date,
                            'cycle'=>$landlordTenantRent->cycle,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s'));*/
            $insertArray['landlord_tenant_id']   = $id;
            $insertArray['agreement_start_date'] = $landlordTenantRent->agreement_start_date;
            $insertArray['cycle']                = $landlordTenantRent->cycle;
            $insertArray['title']                = $titles[$key] ;
            $insertArray['due_on']               = $landlordTenantRent->due_on;
            $insertArray['initial_amount']       = $amounts[$key];
            $success                             = landlordTenantOtherpayment::create($insertArray);
              
            //$success = landlordTenantOtherpayment::insert( $data );
            if( $success ){
                $loopCount = "";
                if(($landlordTenantRent->cycle)!=""){
                    switch($landlordTenantRent->cycle){
                           case "3months":
                             $loopCount = 3;
                           break; 
                           case "6months":
                             $loopCount = 6;
                           break;
                           case "1year":
                             $loopCount = 12;   
                           break;
                           case "2year":
                             $loopCount = 24;
                           break;
                           case "3year":
                              $loopCount =36;
                           break;    
                    }
                }
                if($loopCount!=""){
                    list( $yearStart, $monthStart, $dayStart ) =  explode('-', $landlordTenantRent->agreement_start_date);
                    $billDate = $yearStart.'-'.$monthStart.'-'.$landlordTenantRent->due_on;
                    //echo $billDate;exit;
                    for($i=1;$i<=$loopCount;$i++){
                        $uId  = uniqid();
                        $nextBillDate = date("Y-m-d", strtotime("+".$i." month", strtotime($billDate)));
                        
                        $data[] = array('landlord_tenant_otherpayment_id'=>$success->id,
                                        'bill_on'=>$nextBillDate,
                                        'amount'=>$amounts[$key],
                                        'unique_id'=>$uId, 
                                        'title'=>$titles[$key],
                                        'created_at'=>date('Y-m-d H:i:s'),
                                        'updated_at'=>date('Y-m-d H:i:s'));
                    }
                    
                }
            }
        }
        $success = landlordTenantOtherpaymentDetail::insert($data);
            return redirect()->route('landlords.viewtenant',$id)->with('message-success','Data successfully posted!');
        //}
    }
    
    
    public function getTenant( $id=null ){
         $loggedLandlord = Auth::user('landlord');
         $loggedLandlordId     = $loggedLandlord->id;
         $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
         $pageTitle            = "Contacts"; 
         $originalProfile      = HTML::getLandlordProfileImage(); 
            //this method calling is for the landlords frm msg thread 
         if($id!=""){
            $tenants = $this->getMyTenants( $loggedLandlord->id, $id );
         }
         else{
            $tenants = $this->getMyTenants($loggedLandlord->id);
         }
         $yearMonthTime =  strtotime(date("Y-m"));
         $currMonthYear =  str_replace('-','',date("Y-m", strtotime("+0 month", $yearMonthTime)));
            
                    
            return view('landlords.mytenants', [
                'pageTitle'     => 'My Contacts',
                'pageHeading'   => 'My Contacts',
                'pageMenuName'	=> 'mytenants',
                'tenants'       => $tenants,
                'profileImage'  => $originalProfile,
                'profile'       => $landlordProfile,
                'id'            => $id,
                'currMonthYear' => $currMonthYear
            ]);
    }
    
    public function getTenantProfileFromLandlord($slug)
    {
		$slugCheck = $this->checkTheSlugForTenant($slug);
		if(count($slugCheck) < 1)
		{
			return redirect('/404');
		}
			$loggedLandlordId 		= Auth::user('landlord')->id;
			$landlordProfile      	= $this->getLandlordProfile($loggedLandlordId);
			$tenantProfileFromLandlord = $this->getTenantProfileFromSlug($slug);
			//dd($tenantProfileFromLandlord);
			$schoolName = $this->getSchoolWithId($tenantProfileFromLandlord->school_id);
			$originalProfile=HTML::getLandlordProfileImage(); 
			//$landlordProperties = $this->getAllProperties($landlordProfileFromTenant->id);
			
			return view('landlords.tenantprofile', [
				'pageTitle'   		=> 'Tenant Profile',
				'pageHeading' 		=> 'Tenant Profile',
				'pageMenuName'		=> 'mytenantcontact',
				'tenantObj' 		=> $tenantProfileFromLandlord,
				'schoolname'        => $schoolName,
				'profileImage'    	=> $originalProfile,
				'profile'         	=> $landlordProfile
			]);
		
	}
	private function checkTheSlugForTenant($slug)
	{			
		$tenantCount = Tenant::where('slug', '=', $slug)->first();
		return $tenantCount;
	}
	
	private function getMessageThreadPropertyName($propertyId)
	{
		$msgThreadPropertyName = Property::where('id',$propertyId)
										->select('property_name')
										->get()->first();
		return $msgThreadPropertyName;
	}
	
	private function getFormattedMsgThreadCreatedDate($threadDate)
	{
		
		$formattedMsgThreadCreatedDate = date('d M Y h:i:s a',strtotime($threadDate));
		//print_r($formattedMsgThreadCreatedDate);exit;
		return $formattedMsgThreadCreatedDate;
	}
    
}
