<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Cmspage;
use App\Http\Models\Faq;
use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;



class CmspagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $pagesQuery = DB::table('pages');
        if ($request->has('search')) {
            $search = $request->input('search');
            $pagesQuery->where('title', 'like', '%' . $request->input('search') . '%');
        }
        $pagesQuery->orderBy($sortby, $order);
        $pages = $pagesQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('cmspages.index', ['pageTitle' => 'Cmspages',
            'pageHeading'                          => 'Cmspages',
            'pageDesc'                             => 'Manage pages here',
            'pages'                                => $pages,
            'search'                               => $search,
            'sortby'                               => $sortby,
            'order'                                => $order,
        ]);
    }
    
    public function getFaq(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $faqQuery = DB::table('faq');
        if ($request->has('search')) {
            $search = $request->input('search');
            $faqQuery->where('question', 'like', '%' . $request->input('search') . '%');
        }
        $faqQuery->orderBy($sortby, $order);
        $faqs = $faqQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('faq.index', ['pageTitle' => 'FAQ',
            'pageHeading'                                => 'FAQ',
            'pageDesc'                                   => 'Manage FAQ here',
            'faqs'                                    	 => $faqs,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        return view('cmspages.create', ['pageTitle' => 'Create Page',
            'pageHeading'                     => 'Create Page',
            'pageDesc'                        => 'Create Page here',
        ]);
    }
    
    public function createFaq()
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		return view('faq.create', ['pageTitle' => 'Create FAQ',
            'pageHeading'                     => 'Create FAQ',
            'pageDesc'                        => 'Create FAQ here',
        ]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $rules = [
            'title'            => 'required',
            'content'          => 'required',
        ];
        $messages = [
			'title.required' => 'Pagename is required',
            'content.required'  => 'Content is required.'
        ];
        $validator = Validator::make($request->all(), $rules,$messages);
         if (!$validator->fails())
         {
			//print_r($request->all());exit;
			$input['title'] 	= $request->title;
			$input['content']	= $request->content;
			$input['status']	= $request->status;
			
			$page = Cmspage::create($input);
			return redirect()->route('pages.index')->with('message', 'Page Created.');
		 }
		 else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }
    
    public function storeFaq(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$rules = [
            'question'         	=> 'required',
            'answer'          	=> 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
         if (!$validator->fails())
         {
			//print_r($request->all());exit;
			$input['question'] 	= $request->question;
			$input['answer']	= $request->answer;
			$input['status']	= $request->status;
			
			$page = Faq::create($input);
			return redirect()->route('faq.index')->with('message', 'Page Created.');
		 }
		 else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $page   = Cmspage::find($id);
        return view('cmspages.edit', ['pageTitle' => 'Edit page',
            'pageHeading'                               => 'Edit page',
            'pageDesc'                                  => 'Edit page here',
            'page'                                    	=> $page,
        ]);
    }
    
    public function editFaq($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$faq   = Faq::find($id);
        return view('faq.edit', ['pageTitle' => 'Edit faq',
            'pageHeading'                               => 'Edit faq',
            'pageDesc'                                  => 'Edit faq here',
            'faq'                                    	=> $faq,
        ]);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		//dd($request->all());
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $rules = [
            'title'  		=> 'required',
            'content'    	=> 'required',
        ];
        
         $messages = [
            'title.required'  	=> 'Pagename is required.',
            'content.required' 	=> 'Content is required.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) 
        {
			$input		= array();
            $page   	= Cmspage::find($id);
            
            $input  	= $request->all();
            $input['footerpage'] =  isset($request->footerpage) ? $request->footerpage:0;
            //print_r($input);exit;

            $page->update($input);

            $pageId 	= Cmspage::find($page->id);
            
            if ($pageId['id']) 
            {
                 return redirect()->route('pages.index')->with('message-success', 'Page details are updated.');
            } 
        
        }
        else
        {
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}
    }
    
    public function updateFaq(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$rules = [
            'question'  	=> 'required',
            'answer'    	=> 'required',
        ];
        
         $messages = [
            'question.required' => 'Question is required.',
            'answer.required' 	=> 'Answer is required.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) 
        {
			$input		= array();
            $faq   		= Faq::find($id);
            
            $input  	= $request->all();

            $faq->update($input);

            $faqId 		= Faq::find($faq->id);
            
            if ($faqId['id']) 
            {
                 return redirect()->route('faq.index')->with('message-success', 'Page details are updated.');
            } 
        
        }
        else
        {
			 return redirect()->back()->withErrors($validator->errors())->withInput();
		}
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cmspage = Cmspage::find($id);
        $cmspage->delete();
        return redirect()->route('pages.index')->with('message', 'Page Deleted.');
    }
    
    public function changeStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $page = Cmspage::find($id);
        if ($page->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $page->update($data);
        return redirect()->route('pages.index')->with('message', 'Page status updated');
    }
    
    public function changeFaqStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$page = Faq::find($id);
        if ($page->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $page->update($data);
        return redirect()->route('faq.index')->with('message', 'Faq status updated');
	}
}
