<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Socialite;
use Illuminate\Routing\Controller;
//use Illuminate\Http\Request;
//use App\Http\Requests;
use App\Http\Controllers;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Property;
use App\Http\Models\PropertyReview;
use App\Http\Models\School;
use App\Http\Models\ContactUser;
use App\Http\Models\Cmspage;
use App\Http\Models\Faq;
use App\Http\Models\Mysearch;
use App\Http\Models\Feedback;
use App\Http\Models\Enquiry;

use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Http\Controllers\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $contactsQuery = DB::table('contact_users');
        
        if ($request->has('search')) {
            $search = $request->input('search');
            $contactsQuery->where('name', 'like', '%' . $request->input('search') . '%');
        }
        $contactsQuery->orderBy($sortby, $order);
        $contacts = $contactsQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('contacts.index', ['pageTitle' => 'Contact US',
            'pageHeading'                                => 'Inquiries from contact us page',
            'pageDesc'                                   => 'Manage Contact Us here',
            'contactUsers'                               => $contacts,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $contactUser   = ContactUser::find($id);
        return view('contacts.show', ['pageTitle' => 'Edit ContactUs',
            'pageHeading'                               => 'Edit Inquiries from contact us page',
            'pageDesc'                                  => 'Edit Contact Us here',
            'contactUser'                               => $contactUser,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $contactUser   = ContactUser::find($id);
        return view('contacts.edit', ['pageTitle' => 'Edit ContactUs',
            'pageHeading'                               => 'Edit Inquiries from contact us page',
            'pageDesc'                                  => 'Edit Contact Us here',
            'contactUser'                               => $contactUser,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		$rules = [
            'name'  => 'required',
            'email' => 'required|email',
            'message'=> 'required'
        ];
        
        $messages = [
            'name.required'  	=> 'name is required.',
            'email.required' 	=> 'email is required.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) 
        {
			$input			= array();
            $contactUser	= ContactUser::find($id);
            
            $input  		= $request->all();
            //print_r($input);exit;

            $contactUser->update($input);

            $contactUserId 	= ContactUser::find($contactUser->id);
            
            if ($contactUserId['id']) 
            {
                 return redirect()->route('admins.contactUsers')->with('message-success', 'User details are updated.');
            } 
		}
		else
		{
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
