<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Routing\Controller;

class UsersController extends Controller
{

    /**
     * Display user profile.
     *
     * @return Response
     */
    public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }
}
