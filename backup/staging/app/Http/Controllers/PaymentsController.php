<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\PaymentSetting;
use DB;
use DateTime;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_MerchantAccount;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
     public function getMyPaymentDetails($dateString=null)
     {
		$currentDateView = date("Y-m-d");
	
		$loggedTenantId =  Auth::user('tenant')->id;
		$currentDate ='';
		if($dateString!='')
		{
			$requestDate = substr($dateString,0,4).'-'.substr($dateString,-2);
			$currentDate = $requestDate;
		}
		else
		{
			$currentDate 	=  date("Y-m");
		}
		
		$yearMonthTime 	=  strtotime($currentDate);
		
		$nextMonth 		=  str_replace('-','',date("Y-m", strtotime("+1 month", $yearMonthTime)));
		$prevMonth 		=  str_replace('-','',date("Y-m", strtotime("-1 month", $yearMonthTime)));
		
		
												 
		$myPaymentForMonthlyRents = DB::select('SELECT   "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LTM.payment_date,LT.id,LT.property_id,P.property_name, LTM.unique_id, LT.landlord_id
											FROM landlord_tenant_monthlyrents AS LTM
											INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
											INNER JOIN properties AS P ON LT.property_id = P.id
											WHERE LT.tenant_id ='.$loggedTenantId.' 
											AND LTM.bill_on LIKE "'.$currentDate.'%'.'"  ORDER BY LTM.bill_on ASC');
		
																		
		$myOtherPayments = DB::select('SELECT LTO.title, LTO.initial_amount, LTO.agreement_start_date, LTOD.bill_on, LTOD.amount AS otheramount,LTOD.status,LTOD.payment_date, LTOD.id, LTOD.unique_id, LT.landlord_id
										FROM landlord_tenant_otherpayments AS LTO
										INNER JOIN landlord_tenant_otherpayment_details AS LTOD ON LTO.id = LTOD.landlord_tenant_otherpayment_id
										INNER JOIN landlord_tenants AS LT ON LT.id=LTO.landlord_tenant_id
										WHERE LT.tenant_id ='.$loggedTenantId.' 
										AND LTOD.bill_on LIKE "'.$currentDate.'%'.'" 
										ORDER BY LTOD.bill_on ASC');
		//print_r($myPaymentForMonthlyRents);exit;
			
		return view('payment.mypayment_details', [
            'pageTitle'   			=> 'MyPayment',
            'pageHeading' 			=> 'MyPayment',
            'pageMenuName'			=> 'mypayment',
            'monthlyrentDetails' 	=> $myPaymentForMonthlyRents,
            'otherPayments'			=> $myOtherPayments,
            'nextMonth'				=> $nextMonth,
            'prevMonth'				=> $prevMonth,
            'currentDateView'		=> $currentDateView
        ]);
		
		
	 } 
     
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getSettings(){
         $loggedLandlord = Auth::user('landlord');
         $loggedLandlordId     = $loggedLandlord->id;
         $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
         $pageTitle            = "Contacts"; 
         $originalProfile      = HTML::getLandlordProfileImage();
         $paymentSettings      = PaymentSetting::where('landlord_id',$loggedLandlordId)
                                                 ->first();
         //dd($paymentSettings);  
               
            
                    
            return view('payment.settings', [
                'pageTitle'       => 'Payment settings',
                'pageHeading'     => 'Payment settings',
                'profileImage'    => $originalProfile,
                'profile'         => $landlordProfile,
                'pageMenuName'    => "payment",
                'paymentSettings' => $paymentSettings,
                'loggedLandlord'  => $loggedLandlord
            ]);
    }
    public function postAddPaypal(Request $request){
        $loggedLandlordId = Auth::user('landlord')->id;
        //dd($request->all());
        $rules = array(
            'paypal_id'   => 'required',
        );
        $messages = [
                        'paypal_id.required'             => 'Paypal id field is Required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->with('displayPaypal',1);
        }
        else
        {
            $user = PaymentSetting::firstOrNew(array('landlord_id' => $loggedLandlordId));
            $user->type = isset($request->type)  ? $request->type : 2;
            $user->paypal_id = $request->paypal_id;
            $user->save();
            return redirect()->route('payments.getsettings')->with('message-success', 'Paypal details are updated successfully.')->with('successPaypal',1); 
        }
    }
    public function postAddBank(Request $request){
        //dd($request->all());
        $loggedLandlordId = Auth::user('landlord')->id;
        $rules = array(
            'bank_name'   => 'required',
            'account_no'  => 'required',
            'unique_code' => 'required',
            'first_name'  => 'required',
            'last_name'   => 'required',
            'address'     => 'required',
        );
        $messages = [
                        'bank_name.required'   => 'Bank name field is required',
                        'account_no.required'  => 'Account number field is required',
                        'unique_code.required' => 'IFSC or Routing code field is required',
                        'first_name.required'  => 'First name field is required',
                        'last_name.required'   => 'Last name field is required',
                        'address.required'     => 'Address field is required'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->with('displayBank',1);
        }
        else
        {
             $payment              = PaymentSetting::firstOrNew(array('landlord_id' => $loggedLandlordId));
             //dd($request->all());
             $payment->type        = isset($request->type)  ? $request->type : 1;
             $payment->bank_name   = $request->bank_name;
             $payment->account_no  = $request->account_no;
             $payment->unique_code = $request->unique_code;
             $payment->first_name  = $request->first_name;
             $payment->last_name   = $request->last_name;
             $payment->address     = $request->address;
             //dd($user);
             if($payment->save()){
              $submerchants  = PaymentSetting::where('landlord_id',$loggedLandlordId)
                                               ->select('braintree_SubMerchantAccountId') 
                                               ->first();
              //dd( $submerchants );

              if($submerchants->braintree_SubMerchantAccountId==""){                                  
                  $submerchantId  = $request->first_name.time();
                  //Create submerchant in Braintree
                  $merchantAccountParams = [
                    'individual' => [
                      'firstName' => $request->first_name,
                      'lastName' => $request->last_name,
                      'email' => 'jane@14ladders.com',
                      'phone' => '5553334444',
                      'dateOfBirth' => '1981-11-19',
                      'ssn' => '456-45-4567',
                      'address' => [
                        'streetAddress' => '111 Main St',
                        'locality' => 'Chicago',
                        'region' => 'IL',
                        'postalCode' => '60622'
                      ]
                    ]/*,
                    'business' => [
                      'legalName' => 'Jane\'s Ladders',
                      'dbaName' => 'Jane\'s Ladders',
                      'taxId' => '98-7654321',
                      'address' => [
                        'streetAddress' => '111 Main St',
                        'locality' => 'Chicago',
                        'region' => 'IL',
                        'postalCode' => '60622'
                      ]
                    ]*/,
                    'funding' => [
                      'descriptor' => 'Blue Ladders',
                      'destination' => Braintree_MerchantAccount::FUNDING_DESTINATION_BANK,
                      'email' => 'funding@blueladders.com',
                      'mobilePhone' => '5555555555',
                      'accountNumber' => '1123581321',
                      'routingNumber' => '071101307'
                    ],
                    'tosAccepted' => true,
                    'masterMerchantAccountId' => "offshorentsolutions",
                    'id' => $submerchantId
                  ];
                  $result = Braintree_MerchantAccount::create($merchantAccountParams);
                  $submerchantAccountId = $result->merchantAccount->_attributes['id'];
                  $payment->braintree_SubMerchantAccountId = $submerchantAccountId;
                  $payment->update();
                }
                else{
                  $result = Braintree_MerchantAccount::update(
                        $submerchants->braintree_SubMerchantAccountId,
                        [
                          'individual' => ['firstName' => $request->first_name,
                                           'lastName'  =>$request->last_name]
                        ]
                      );
                }
             } 

             return redirect()->route('payments.getsettings')->with('message-success', 'Bank details are updated successfully.')->with('successBank',1); 
        }
    }
    public function getTenantPayment( $dateString=null, $id=null ){
        $loggedLandlordId = Auth::user('landlord')->id;
        $landlordProfile  = $this->getLandlordProfile($loggedLandlordId);
        $originalProfile  = HTML::getLandlordProfileImage();
        $currentDate ='';
        if($dateString!='')
        {
            $requestDate = substr($dateString,0,4).'-'.substr($dateString,-2);
            $currentDate = $requestDate;
            
        }
        else
        {
            $currentDate    =  date("Y-m");

        }
        $yearMonthTime =  strtotime($currentDate);
        $currMonthYear =  str_replace('-','',date("Y-m", strtotime("+0 month", $yearMonthTime)));
        $tenantId      = ( $id!="" ) ? $id:"";        
        $nextMonth      =  str_replace('-','',date("Y-m", strtotime("+1 month", $yearMonthTime)));
        $prevMonth      =  str_replace('-','',date("Y-m", strtotime("-1 month", $yearMonthTime)));
                                                 
        $myMonthlyRentOfTenants = $this->getAllMyTenantPayments( $loggedLandlordId, $id);
        //dd( $myMonthlyRentOfTenants );
        $myOtherPayments=array();
        $getTenantsWithPayments  = $this->getTenantsPayments( $loggedLandlordId );
        //dd( $getTenantsWithPayments );
        $tenantDropdown = [];
        if(count( $getTenantsWithPayments )){
            foreach ($getTenantsWithPayments as $payment)
            {
                $tenantDropdown[$payment->tenant->id] = $payment->tenant->firstname.' '.$payment->tenant->lastname;
            }
        }
        //dd( $tenantDropdown );
        
        $mainArray = array();
        $keyFlagMain = 0;
        foreach($myMonthlyRentOfTenants as $myMonthlyRentOfTenant)                                  
        { 

            $tenantArary  = array('tenantId'=>$myMonthlyRentOfTenant->tenantId,'name'=>$myMonthlyRentOfTenant->firstname.' '.$myMonthlyRentOfTenant->lastname,'propertyName'=>$myMonthlyRentOfTenant->property_name);                                
            $myPaymentForMonthlyRents = $this->getAllMonthlyRentOfTenant( $loggedLandlordId, $currentDate, $myMonthlyRentOfTenant->tenantId );
            //print_r( $myPaymentForMonthlyRents );exit;
            $myOtherPayments = $this->getAllOtherPayments( $loggedLandlordId, $currentDate, $myMonthlyRentOfTenant->tenantId );
            //print_r( $myOtherPayments );
            $payment = array();
            $keyFlag = 0;
            foreach( $myPaymentForMonthlyRents as $key => $myPaymentForMonthlyRent ){

                
                $payment[$keyFlag]['title']         = $myPaymentForMonthlyRent->title;
                $payment[$keyFlag]['id']            = $myPaymentForMonthlyRent->payId;
                $payment[$keyFlag]['bill_on']       = $myPaymentForMonthlyRent->bill_on;
                $payment[$keyFlag]['amount']        = $myPaymentForMonthlyRent->amount;
                $payment[$keyFlag]['property_name'] = $myPaymentForMonthlyRent->property_name;
                $payment[$keyFlag]['status']        = $myPaymentForMonthlyRent->status;  
                $payment[$keyFlag]['flag']          = "month";

                $keyFlag++;
            }
            foreach( $myOtherPayments as $key => $myOtherPayment ){

                
                $payment[$keyFlag]['title']         = $myOtherPayment->title;
                $payment[$keyFlag]['id']            = $myOtherPayment->payId;
                $payment[$keyFlag]['bill_on']       = $myOtherPayment->bill_on;
                $payment[$keyFlag]['amount']        = $myOtherPayment->otheramount;
                $payment[$keyFlag]['property_name'] = $myPaymentForMonthlyRent->property_name;
                $payment[$keyFlag]['status']        = $myOtherPayment->status;
                $payment[$keyFlag]['flag']          = "iter"; 
                $keyFlag++; 
            }

            $tenantArary['payment'] = $payment;
            $mainArray[$keyFlagMain] = $tenantArary;
            $keyFlagMain++;
        }
        //print_r( $tenantArary );
        //exit;
        //print_r($myPaymentForMonthlyRents);exit;
            
        return view('payment.tenants_payments', [
            'pageTitle'             => 'MyPayment',
            'pageHeading'           => 'MyPayment',
            'pageMenuName'          => 'mypayment',
            'profile'               => $landlordProfile,
            'profileImage'          => $originalProfile,
            'yearMonthTime'         => $currMonthYear,
            'monthlyrentDetails'    => $mainArray,
            'nextMonth'             => $nextMonth,
            'prevMonth'             => $prevMonth,
            'tenantDropdown'        => $tenantDropdown,
            'tenantId'              => $tenantId 
        ]); 
    }
    public function getAllPayment( $dateString=null, $lid=null, $id=null ){
        $adminUser = Auth::User('admin');
        if(empty($adminUser)){
            return redirect()->route('admins.getLogin');
        }
        $currentDate ='';
        if($dateString!='')
        {
            $requestDate = substr($dateString,0,4).'-'.substr($dateString,-2);
            $currentDate = $requestDate;
            
        }
        else
        {
            $currentDate    =  date("Y-m");

        }
        $yearMonthTime =  strtotime($currentDate);
        $currMonthYear =  str_replace('-','',date("Y-m", strtotime("+0 month", $yearMonthTime)));
        $tenantId      = ( $id!="" ) ? $id:"";
        $landLordId    = ( $lid!="" ) ? $lid:"";           
        $nextMonth      =  str_replace('-','',date("Y-m", strtotime("+1 month", $yearMonthTime)));
        $prevMonth      =  str_replace('-','',date("Y-m", strtotime("-1 month", $yearMonthTime)));
                                                 
        $tenantPayments = $this->getPayments( $lid, $id);
        //dd( $tenantPayments );
        $myOtherPayments=array();
        $getlandlordsPayments  = $this->getlandlordsPayments();
        //dd( $getlandlordsPayments );
        //print_r($getlandlordsPayments);exit;
        //dd( $getTenantsWithPayments );
        $landlordDropdown = [];
        if(count( $getlandlordsPayments )){
            foreach ($getlandlordsPayments as $landlord)
            {
				//dd ($landlord->landlord->firstname);
				if(count($landlord->landlord)){
					//echo 1;
					$landlordDropdown[$landlord->landlord->id] = $landlord->landlord->firstname.' '.$landlord->landlord->lastname;
				}
                
            }
        }
        $tenantDropdown = [];
        if( $landLordId !="" ){
            $tenants  = LandlordTenant::where('landlord_id', $landLordId)
                                        -> with('tenant')
                                        -> groupBy('tenant_id')
                                        -> get();
            
            if(count( $tenants )){
                foreach ($tenants as $tenant)
                {
                    $tenantDropdown[$tenant->tenant->id] = $tenant->tenant->firstname.' '.$tenant->tenant->lastname;
                }
            } 
        }
        //dd( $landlordDropdown );
        
        $mainArray = array();
        $keyFlagMain = 0;
        foreach($tenantPayments as $tenantPayment)                                  
        { 

            $tenantArary  = array('tenantId'=>$tenantPayment->tenantId,
                                  'tenantName'=>$tenantPayment->firstname.' '.$tenantPayment->lastname,
                                  'propertyName'=>$tenantPayment->property_name,
                                  'landLordName'=>$tenantPayment->LfirstName.' '.$tenantPayment->LlastName );                                
            $myPaymentForMonthlyRents = $this->getAllMonthlyRentOfTenantOfAllLandLords( $currentDate, $tenantPayment->ltId );
            //dd( $myPaymentForMonthlyRents );
            $myOtherPayments = $this->getAllOtherPaymentsOfTenantOfAllLandLords( $currentDate, $tenantPayment->ltId );
            //print_r( $myOtherPayments );
            $payment = array();
            $keyFlag = 0;
            foreach( $myPaymentForMonthlyRents as $key => $myPaymentForMonthlyRent ){

                
                $payment[$keyFlag]['title']         = $myPaymentForMonthlyRent->title;
                $payment[$keyFlag]['id']            = $myPaymentForMonthlyRent->payId;
                $payment[$keyFlag]['bill_on']       = $myPaymentForMonthlyRent->bill_on;
                $payment[$keyFlag]['amount']        = $myPaymentForMonthlyRent->amount;
                $payment[$keyFlag]['property_name'] = $myPaymentForMonthlyRent->property_name;
                $payment[$keyFlag]['status']        = $myPaymentForMonthlyRent->status;  
                $payment[$keyFlag]['flag']          = "month";

                $keyFlag++;
            }
            foreach( $myOtherPayments as $key => $myOtherPayment ){

                
                $payment[$keyFlag]['title']         = $myOtherPayment->title;
                $payment[$keyFlag]['id']            = $myOtherPayment->payId;
                $payment[$keyFlag]['bill_on']       = $myOtherPayment->bill_on;
                $payment[$keyFlag]['amount']        = $myOtherPayment->otheramount;
                $payment[$keyFlag]['property_name'] = $myPaymentForMonthlyRent->property_name;
                $payment[$keyFlag]['status']        = $myOtherPayment->status;
                $payment[$keyFlag]['flag']          = "iter"; 
                $keyFlag++; 
            }

            $tenantArary['payment'] = $payment;
            $mainArray[$keyFlagMain] = $tenantArary;
            $keyFlagMain++;
        }
        //print_r( $mainArray );exit;
        //exit;
        //print_r($myPaymentForMonthlyRents);exit;
        /*return view('property.index', ['pageTitle' => 'Properties',
            'pageHeading'                                => 'Properties',
            'pageDesc'                                   => 'Manage properties here',
            'properties'                                 => $properties,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);*/    
        return view('payment.admin-allpayments', [
            'pageTitle'             => 'MyPayment',
            'pageHeading'           => 'MyPayment',
            'yearMonthTime'         => $currMonthYear,
            'monthlyrentDetails'    => $mainArray,
            'nextMonth'             => $nextMonth,
            'prevMonth'             => $prevMonth,
            'landlordDropdown'      => $landlordDropdown,
            'tenantDropdown'        => $tenantDropdown,
            'tenantId'              => $tenantId,
            'landLordId'            => $landLordId 
        ]); 
    }
    public function getAllMyTenantPayments( $landlordId, $tenantId ){
        $cond = "";
        if( $tenantId!="" ){
            $cond.= " AND LT.tenant_id='".$tenantId."'"; 
        }
        $myPaymentForMonthlyRents = DB::select('SELECT T.firstname,T.lastname,LT.id as ltId,T.id as tenantId,P.property_name
                                            FROM  landlord_tenants AS LT 
                                            INNER JOIN tenants AS T ON T.id = LT.tenant_id
                                            INNER JOIN properties AS P ON LT.property_id = P.id
                                            WHERE LT.landlord_id ='.$landlordId.$cond.' GROUP BY LT.tenant_id  ORDER BY LT.created_at ASC');
        return $myPaymentForMonthlyRents;

    }
     public function getPayments( $landlordId, $tenantId ){
        $cond = "1 ";
        if( $tenantId!="" ){
            $cond.= " AND LT.tenant_id='".$tenantId."'"; 
        }
        if( $landlordId!="" ){
            $cond.= " AND LT.landlord_id='".$landlordId."'"; 
        }
        $myPaymentForMonthlyRents = DB::select('SELECT L.firstname as LfirstName,L.lastname as LlastName,T.firstname,T.lastname,LT.id as ltId,T.id as tenantId,P.property_name
                                            FROM  landlord_tenants AS LT 
                                            INNER JOIN tenants AS T ON T.id = LT.tenant_id
                                            INNER JOIN landlords AS L ON L.id = LT.landlord_id
                                            INNER JOIN properties AS P ON LT.property_id = P.id
                                            WHERE '.$cond.' ORDER BY LT.created_at ASC');
        //dd($myPaymentForMonthlyRents);
        return $myPaymentForMonthlyRents;

    }
    public function getAllMonthlyRentOfTenant( $landlordId, $currentDate, $tenantId ){
        $cond = "";
        if( $tenantId!="" ){
            $cond.= " AND LT.tenant_id='".$tenantId."'"; 
        }
        /*echo 'SELECT "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LT.id,LT.property_id,P.property_name,T.firstname,T.lastname,T.id as tenantId, LTM.id as payId
                                            FROM landlord_tenant_monthlyrents AS LTM
                                            INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
                                            INNER JOIN properties AS P ON LT.property_id = P.id
                                            INNER JOIN tenants AS T ON T.id = LT.tenant_id
                                            WHERE LT.landlord_id ='.$landlordId.$cond.' AND LTM.bill_on LIKE "'.$currentDate.'%'.'" ORDER BY LTM.bill_on ASC';exit;*/
        $myPaymentForMonthlyRents = DB::select('SELECT "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LT.id,LT.property_id,P.property_name,T.firstname,T.lastname,T.id as tenantId, LTM.id as payId
                                            FROM landlord_tenant_monthlyrents AS LTM
                                            INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
                                            INNER JOIN properties AS P ON LT.property_id = P.id
                                            INNER JOIN tenants AS T ON T.id = LT.tenant_id
                                            WHERE LT.landlord_id ='.$landlordId.$cond.' AND LTM.bill_on LIKE "'.$currentDate.'%'.'" GROUP BY LT.tenant_id  ORDER BY LTM.bill_on ASC');
        return $myPaymentForMonthlyRents;
    }
    public function getAllMonthlyRentOfTenantOfAllLandLords( $currentDate, $ltId ){
        $cond = " 1 ";
        if( $ltId!="" ){
            $cond.= " AND LTM.landlord_tenant_id='".$ltId."'"; 
        }
        /*echo 'SELECT "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LT.id,LT.property_id,P.property_name,T.firstname,T.lastname,T.id as tenantId, LTM.id as payId
                                            FROM landlord_tenant_monthlyrents AS LTM
                                            INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
                                            INNER JOIN properties AS P ON LT.property_id = P.id
                                            INNER JOIN tenants AS T ON T.id = LT.tenant_id
                                            WHERE '.$cond.' AND LTM.bill_on LIKE "'.$currentDate.'%'.'" ORDER BY LTM.bill_on ASC';*/
        $myPaymentForMonthlyRents = DB::select('SELECT "Monthly Rent" AS title,LTM.bill_on,LTM.amount,LTM.status,LT.id,LT.property_id,P.property_name,T.firstname,T.lastname,T.id as tenantId, LTM.id as payId
                                            FROM landlord_tenant_monthlyrents AS LTM
                                            INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
                                            INNER JOIN properties AS P ON LT.property_id = P.id
                                            INNER JOIN tenants AS T ON T.id = LT.tenant_id
                                            WHERE '.$cond.' AND LTM.bill_on LIKE "'.$currentDate.'%'.'" ORDER BY LTM.bill_on ASC');
        return $myPaymentForMonthlyRents;
    }
    public function getAllOtherPayments( $landlordId, $currentDate, $tenantId ){
        $cond = "";
        if( $tenantId!="" ){
            $cond.= " AND LT.tenant_id='".$tenantId."'"; 
        }
        $myOtherPayments = DB::select('SELECT LTO.title, LTO.initial_amount, LTO.agreement_start_date, LTOD.bill_on, LTOD.amount AS otheramount,LTOD.status,LTOD.id as payId 
                                        FROM landlord_tenant_otherpayment_details AS LTOD                                         
                                        INNER JOIN landlord_tenant_otherpayments AS LTO ON LTOD.landlord_tenant_otherpayment_id =LTO.id
                                        INNER JOIN landlord_tenants AS LT ON LT.id = LTO.landlord_tenant_id
                                        WHERE LT.landlord_id ='.$landlordId.$cond.' AND LTOD.bill_on LIKE "'.$currentDate.'%'.'" 
                                        ORDER BY LTOD.bill_on ASC ');
        
        return $myOtherPayments;
    }
    public function getAllOtherPaymentsOfTenantOfAllLandLords( $currentDate, $ltId ){
        $cond = " 1 ";
        if( $ltId!="" ){
            $cond.= " AND LTO.landlord_tenant_id='".$ltId."'"; 
        }
        $myOtherPayments = DB::select('SELECT LTO.title, LTO.initial_amount, LTO.agreement_start_date, LTOD.bill_on, LTOD.amount AS otheramount,LTOD.status,LTOD.id as payId 
                                        FROM landlord_tenant_otherpayment_details AS LTOD                                         
                                        INNER JOIN landlord_tenant_otherpayments AS LTO ON LTOD.landlord_tenant_otherpayment_id =LTO.id
                                        INNER JOIN landlord_tenants AS LT ON LT.id = LTO.landlord_tenant_id
                                        WHERE'.$cond.' AND LTOD.bill_on LIKE "'.$currentDate.'%'.'" 
                                        ORDER BY LTOD.bill_on ASC ');
        
        return $myOtherPayments;
    }
    public function getPayNowMonthlyrent( $id ){
        $payments = landlordTenantMonthlyrent::where( 'unique_id',$id )
                                    ->with('landlord_tenant')
                                    ->first();
        //dd($payments);                            
        

        // $result = Braintree_Transaction::releaseFromEscrow("gvd72j");
        /*$result = Braintree_Transaction::holdInEscrow('8tc6n4');*/
        /*$merchantAccountParams = [
  'individual' => [
    'firstName' => 'Jane',
    'lastName' => 'Doe',
    'email' => 'jane@14ladders.com',
    'phone' => '5553334444',
    'dateOfBirth' => '1981-11-19',
    'ssn' => '456-45-4567',
    'address' => [
      'streetAddress' => '111 Main St',
      'locality' => 'Chicago',
      'region' => 'IL',
      'postalCode' => '60622'
    ]
  ],
  'business' => [
    'legalName' => 'Jane\'s Ladders',
    'dbaName' => 'Jane\'s Ladders',
    'taxId' => '98-7654321',
    'address' => [
      'streetAddress' => '111 Main St',
      'locality' => 'Chicago',
      'region' => 'IL',
      'postalCode' => '60622'
    ]
  ],
  'funding' => [
    'descriptor' => 'Blue Ladders',
    'destination' => Braintree_MerchantAccount::FUNDING_DESTINATION_BANK,
    'email' => 'funding@blueladders.com',
    'mobilePhone' => '5555555555',
    'accountNumber' => '1123581321',
    'routingNumber' => '071101307'
  ],
  'tosAccepted' => true,
  'masterMerchantAccountId' => "offshorentsolutions",
  'id' => "blue_ladders_store"
];*/
// $result = Braintree_MerchantAccount::create($merchantAccountParams);
//         // $merchantAccount = Braintree_MerchantAccount::find('offshorentsolutions');
        // dd($result);
        if( !empty($payments) ){
            return view('payment.pay',['id'=>$id]);
        }
        else{
          return redirect('/404');
        }

    }
    public function postPayNowMonthlyrent( Request $request){

        //dd($request->all());
        $payments = landlordTenantMonthlyrent::where( 'unique_id',$request->uniqueId )
                                    ->with('landlord_tenant')
                                    ->first();
        //dd($payments);                            
        $landlordId  = $payments->landlord_tenant->landlord_id;
        $paymentSettings = PaymentSetting::where( 'landlord_id', $landlordId )
                                                 ->first();
        //dd($paymentSettings);
        $result = Braintree_Transaction::sale(
                  [
                    'amount' => $payments->amount,
                    'merchantAccountId' => $paymentSettings->braintree_SubMerchantAccountId,
                    'paymentMethodNonce' => $request->payment_method_nonce,
                    'options' => [
                      'submitForSettlement' => true
                    ],
                    'serviceFeeAmount' => "10.00"
                  ]
                );
         
        
         if($result->success==1){
            $payments->transaction_id = $result->transaction->_attributes['id'];
            $payments->status = 1;
            $dateTime = (array)($result->transaction->_attributes['createdAt']);
            $payments->payment_date = $dateTime['date'];
         }
         else{
            $payments->transaction_id = "Error";
         }
         $status  = $payments->update();
         return redirect(route('payment.getMyPayment'));
         
        //dd($request->all());
        //echo $request->payment_method_nonce;exit;
        /*$result = Braintree_Customer::create([
                 'firstName' => 'Mike',
                 'lastName' => 'Jones',
                 'company' => 'Jones Co.',
                 'email' => 'mike.jones@example.com',
                 'phone' => '281.330.8004',
                 'fax' => '419.555.1235',
                 'website' => 'http://example.com'
        ]);*/
        /*$result = Braintree_Customer::create([
            'firstName' => 'Mike',
            'lastName' => 'Jones',
            'company' => 'Jones Co.',
            'paymentMethodNonce' => $request->payment_method_nonce
        ]);*/
        /*$result = Braintree_Customer::create([
            'firstName' => 'Fred',
            'lastName' => 'Jones',
            'creditCard' => [
                'paymentMethodNonce' => $request->payment_method_nonce,
                'options' => [
                    'verifyCard' => true
                ]
            ]
        ]);
        $result = Braintree_Transaction::releaseFromEscrow("d788p8");
        print_r( $result );exit;*/
        /*$result = Braintree_Transaction::sale([
                 'amount' => '50.00',
                'paymentMethodNonce' => $request->payment_method_nonce,
                'merchantAccountId' => 'blue_ladders_store',
                'options' => [
                    'submitForSettlement' => True,
                    'holdInEscrow' => true
                ],
                'serviceFeeAmount' => "10.00"
        ]);*/
        /*$result = Braintree_Transaction::sale([
          'amount' => '1000.00',
          'merchantAccountId' => 'blue_ladders_store',
          'paymentMethodNonce' => $request->payment_method_nonce,
          'customer' => [
            'firstName' => 'Kushal',
            'lastName' => 'Edwin',
            'company' => 'Braintree',
            'phone' => '312-555-1234',
            'fax' => '312-555-1235',
            'website' => 'http://www.example.com',
            'email' => 'kushal855@gmail.com'
          ],
          'options' => [
            'submitForSettlement' => true,
            'holdInEscrow' => true
          ],
          'serviceFeeAmount' => "10.00"
        ]);*/
       /* $result = Braintree_Transaction::sale([
          'amount' => '126.00',
          'paymentMethodNonce' => $request->payment_method_nonce,
          'customer' => [
            'id' => '88025538'
          ]
        ]);*/
        /*$result = Braintree_Transaction::sale([
          'amount' => '700.00',
          'orderId' => '654',
          'merchantAccountId' => '88025538',
          'paymentMethodNonce' => $request->payment_method_nonce,
          'customer' => [
            'firstName' => 'George',
            'lastName' => 'Thomas',
            'company' => 'Braintree',
            'phone' => '312-555-1234',
            'fax' => '312-555-1235',
            'website' => 'http://www.example.com',
            'email' => 'drew@example.com'
          ],
          'billing' => [
            'firstName' => 'Paul',
            'lastName' => 'Smith',
            'company' => 'Braintree',
            'streetAddress' => '1 E Main St',
            'extendedAddress' => 'Suite 403',
            'locality' => 'Chicago',
            'region' => 'IL',
            'postalCode' => '60622',
            'countryCodeAlpha2' => 'US'
          ],
          'shipping' => [
            'firstName' => 'Jen',
            'lastName' => 'Smith',
            'company' => 'Braintree',
            'streetAddress' => '1 E 1st St',
            'extendedAddress' => 'Suite 403',
            'locality' => 'Bartlett',
            'region' => 'IL',
            'postalCode' => '60103',
            'countryCodeAlpha2' => 'US'
          ],
          'options' => [
            'submitForSettlement' => true,
             'holdInEscrow' => true
          ]
        ]);*/
            // $result = Braintree_Transaction::sale(
            //   [
            //     'amount' => '1501.00',
            //     'merchantAccountId' => 'blue_ladders_store',
            //     'paymentMethodNonce' => $request->payment_method_nonce,
            //     'options' => [
            //       'submitForSettlement' => true
            //     ],
            //     'serviceFeeAmount' => "10.00"
            //   ]
            // );
        /*$result = Braintree_Transaction::releaseFromEscrow("d788p8");*/
        // $result = Braintree_Transaction::holdInEscrow('8tc6n4');
        print_r($result->transaction->_attributes['id']);
    }
    public function getPayNowItinerary( $id ){
        $payments = landlordTenantOtherpaymentDetail::where( 'unique_id',$id )
                                    ->first();
        //dd($payments);                            
        

        // $result = Braintree_Transaction::releaseFromEscrow("gvd72j");
        /*$result = Braintree_Transaction::holdInEscrow('8tc6n4');*/
        /*$merchantAccountParams = [
  'individual' => [
    'firstName' => 'Jane',
    'lastName' => 'Doe',
    'email' => 'jane@14ladders.com',
    'phone' => '5553334444',
    'dateOfBirth' => '1981-11-19',
    'ssn' => '456-45-4567',
    'address' => [
      'streetAddress' => '111 Main St',
      'locality' => 'Chicago',
      'region' => 'IL',
      'postalCode' => '60622'
    ]
  ],
  'business' => [
    'legalName' => 'Jane\'s Ladders',
    'dbaName' => 'Jane\'s Ladders',
    'taxId' => '98-7654321',
    'address' => [
      'streetAddress' => '111 Main St',
      'locality' => 'Chicago',
      'region' => 'IL',
      'postalCode' => '60622'
    ]
  ],
  'funding' => [
    'descriptor' => 'Blue Ladders',
    'destination' => Braintree_MerchantAccount::FUNDING_DESTINATION_BANK,
    'email' => 'funding@blueladders.com',
    'mobilePhone' => '5555555555',
    'accountNumber' => '1123581321',
    'routingNumber' => '071101307'
  ],
  'tosAccepted' => true,
  'masterMerchantAccountId' => "offshorentsolutions",
  'id' => "blue_ladders_store"
];*/
// $result = Braintree_MerchantAccount::create($merchantAccountParams);
//         // $merchantAccount = Braintree_MerchantAccount::find('offshorentsolutions');
        // dd($result);
        if( !empty($payments) ){
            return view('payment.payItinerary',['id'=>$id]);
        }
        else{
          return redirect('/404');
        }

    }
    public function postPayNowItinerary( Request $request){

        //dd($request->all());
        $payments = landlordTenantOtherpaymentDetail::where( 'unique_id',$request->uniqueId )
                                    ->with('landlord_tenant_otherpayment')
                                    ->first();
        //dd($payments);                            
        $landlordTenantId  = $payments->landlord_tenant_otherpayment->landlord_tenant_id;
        $landLordTenants   = LandlordTenant::where('id',$landlordTenantId)
                                              ->first();
        $landlordId  = $landLordTenants->landlord_id;                                      
        $paymentSettings   = PaymentSetting::where( 'landlord_id', $landlordId )
                                                 ->first();
        //dd($paymentSettings);
        $result = Braintree_Transaction::sale(
                  [
                    'amount' => $payments->amount,
                    'merchantAccountId' => $paymentSettings->braintree_SubMerchantAccountId,
                    'paymentMethodNonce' => $request->payment_method_nonce,
                    'options' => [
                      'submitForSettlement' => true
                    ],
                    'serviceFeeAmount' => "10.00"
                  ]
                );
         if($result->success==1){
            $payments->transaction_id = $result->transaction->_attributes['id'];
            $payments->status = 1;
            $dateTime = (array)($result->transaction->_attributes['createdAt']);
            $payments->payment_date = $dateTime['date'];
         }
         else{
            $payments->transaction_id = "Error";
         }
         $status  = $payments->update();
         return redirect(route('payment.getMyPayment'));
         
         
        //dd($request->all());
        //echo $request->payment_method_nonce;exit;
        /*$result = Braintree_Customer::create([
                 'firstName' => 'Mike',
                 'lastName' => 'Jones',
                 'company' => 'Jones Co.',
                 'email' => 'mike.jones@example.com',
                 'phone' => '281.330.8004',
                 'fax' => '419.555.1235',
                 'website' => 'http://example.com'
        ]);*/
        /*$result = Braintree_Customer::create([
            'firstName' => 'Mike',
            'lastName' => 'Jones',
            'company' => 'Jones Co.',
            'paymentMethodNonce' => $request->payment_method_nonce
        ]);*/
        /*$result = Braintree_Customer::create([
            'firstName' => 'Fred',
            'lastName' => 'Jones',
            'creditCard' => [
                'paymentMethodNonce' => $request->payment_method_nonce,
                'options' => [
                    'verifyCard' => true
                ]
            ]
        ]);
        $result = Braintree_Transaction::releaseFromEscrow("d788p8");
        print_r( $result );exit;*/
        /*$result = Braintree_Transaction::sale([
                 'amount' => '50.00',
                'paymentMethodNonce' => $request->payment_method_nonce,
                'merchantAccountId' => 'blue_ladders_store',
                'options' => [
                    'submitForSettlement' => True,
                    'holdInEscrow' => true
                ],
                'serviceFeeAmount' => "10.00"
        ]);*/
        /*$result = Braintree_Transaction::sale([
          'amount' => '1000.00',
          'merchantAccountId' => 'blue_ladders_store',
          'paymentMethodNonce' => $request->payment_method_nonce,
          'customer' => [
            'firstName' => 'Kushal',
            'lastName' => 'Edwin',
            'company' => 'Braintree',
            'phone' => '312-555-1234',
            'fax' => '312-555-1235',
            'website' => 'http://www.example.com',
            'email' => 'kushal855@gmail.com'
          ],
          'options' => [
            'submitForSettlement' => true,
            'holdInEscrow' => true
          ],
          'serviceFeeAmount' => "10.00"
        ]);*/
       /* $result = Braintree_Transaction::sale([
          'amount' => '126.00',
          'paymentMethodNonce' => $request->payment_method_nonce,
          'customer' => [
            'id' => '88025538'
          ]
        ]);*/
        /*$result = Braintree_Transaction::sale([
          'amount' => '700.00',
          'orderId' => '654',
          'merchantAccountId' => '88025538',
          'paymentMethodNonce' => $request->payment_method_nonce,
          'customer' => [
            'firstName' => 'George',
            'lastName' => 'Thomas',
            'company' => 'Braintree',
            'phone' => '312-555-1234',
            'fax' => '312-555-1235',
            'website' => 'http://www.example.com',
            'email' => 'drew@example.com'
          ],
          'billing' => [
            'firstName' => 'Paul',
            'lastName' => 'Smith',
            'company' => 'Braintree',
            'streetAddress' => '1 E Main St',
            'extendedAddress' => 'Suite 403',
            'locality' => 'Chicago',
            'region' => 'IL',
            'postalCode' => '60622',
            'countryCodeAlpha2' => 'US'
          ],
          'shipping' => [
            'firstName' => 'Jen',
            'lastName' => 'Smith',
            'company' => 'Braintree',
            'streetAddress' => '1 E 1st St',
            'extendedAddress' => 'Suite 403',
            'locality' => 'Bartlett',
            'region' => 'IL',
            'postalCode' => '60103',
            'countryCodeAlpha2' => 'US'
          ],
          'options' => [
            'submitForSettlement' => true,
             'holdInEscrow' => true
          ]
        ]);*/
            // $result = Braintree_Transaction::sale(
            //   [
            //     'amount' => '1501.00',
            //     'merchantAccountId' => 'blue_ladders_store',
            //     'paymentMethodNonce' => $request->payment_method_nonce,
            //     'options' => [
            //       'submitForSettlement' => true
            //     ],
            //     'serviceFeeAmount' => "10.00"
            //   ]
            // );
        /*$result = Braintree_Transaction::releaseFromEscrow("d788p8");*/
        // $result = Braintree_Transaction::holdInEscrow('8tc6n4');
        
    }
    public function updatePayment(Request $request){
        //print_r($request->all());
        if($request->flag=='month'){
            $rent =landlordTenantMonthlyrent::find($request->paymentId);

        }
        else{
            $rent =landlordTenantOtherpaymentDetail::find($request->paymentId);
        }
        //dd($rent);
        $rent->payment_date = date('Y-m-d H:i:s',strtotime($request->date)); 
        $rent->status = 1;
        if($rent->update()){
            echo "1";
        }
        else{
            echo "0";
        }
    }
    public function getTenantFromLandlord( Request $request ){
        $landlordId  = $request->landLordId;
        $tenants  = LandlordTenant::where('landlord_id', $landlordId)
                                        -> with('tenant')
                                        -> groupBy('tenant_id')
                                        -> get();
        $tenantDropdown = [];
        if(count( $tenants )){
            foreach ($tenants as $tenant)
            {
                $tenantDropdown[$tenant->tenant->id] = $tenant->tenant->firstname.' '.$tenant->tenant->lastname;
            }
        } 
        
        return view('payment.tenantDropdown', [
                'tenantDropdown'     => $tenantDropdown
            ]);
    }
}
