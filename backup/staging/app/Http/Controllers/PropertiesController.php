<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tenant;
use App\Http\Models\Landlord;
use App\Http\Models\LandlordTenant;
use App\Http\Models\landlordTenantDoc;
use App\Http\Models\landlordTenantMonthlyrent;
use App\Http\Models\landlordTenantOtherpayment;
use App\Http\Models\landlordTenantOtherpaymentDetail;
use App\Http\Models\Message;
use App\Http\Models\School;
use App\Http\Models\MessageThread;
use App\Http\Models\PropertyViewed;
use App\Http\Models\Property;
use App\Http\Models\PropertyImage;
use App\Http\Models\PropertyEnquiry;
use App\Http\Models\PropertyReview;
use App\Http\Models\PropertyComment;
use App\Http\Models\PropertyRating;
use App\Http\Models\Rating;
use App\Http\Models\RatingUser;
use App\Http\Models\PropertyExpressInterest;
use App\Http\Models\PropertyDoc;
use App\Http\Models\PropertyAmenity;
use App\Http\Models\Amenity;

use DB;
use Config;
use Validator;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;
use Hash;
use View;
use Redirect;
use Session;
use Mail;
use Input;
use URL;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use HTML;


class PropertiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $propertiesQuery = DB::table('properties')
								->join('landlords', 'properties.landlord_id', '=', 'landlords.id')
								->select('properties.*', 'landlords.firstname', 'landlords.lastname');
		//dd($propertiesQuery);exit;
        if ($request->has('search')) {
            $search = $request->input('search');
            $propertiesQuery->where('property_name', 'like', '%' . $request->input('search') . '%');
        }
        
		$propertiesQuery->orderBy($sortby, $order);
        $properties = $propertiesQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('property.index', ['pageTitle' => 'Properties',
            'pageHeading'                                => 'Properties',
            'pageDesc'                                   => 'Manage properties here',
            'properties'                                 => $properties,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
    }
    
    public function reviewIndex(Request $request)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $propertiesQuery = DB::table('property_review as PR')
								->join('tenants as T', 'PR.tenant_id', '=', 'T.id')
								->join('properties as P','P.id','=','PR.property_id')
								->select('PR.id','PR.status','PR.property_id','PR.title','PR.created_at', 'T.firstname', 'T.lastname','P.property_name');
		//dd($propertiesQuery);exit;
        if ($request->has('search')) {
            $search = $request->input('search');
            $propertiesQuery->where('P.property_name', 'like', '%' . $request->input('search') . '%');
        }
        
		$propertiesQuery->orderBy($sortby, $order);
        $properties = $propertiesQuery->paginate(10);
        //dd($properties);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('property.reviewindex', ['pageTitle' => 'PropertyReview',
            'pageHeading'                                => 'PropertyReview',
            'pageDesc'                                   => 'Manage PropertyReview here',
            'properties'                                 => $properties,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $property = Property::find($id);
		
		$landlord = Landlord::where('id', $property->landlord_id)
								->select('firstname')
								->addSelect('lastname')
								->addSelect('id')
								->get()->first();
		$propertyImageArray = PropertyImage::where('property_id',$id)	
									->select('image_name')
									->addSelect('featured')
									->get();
							
		//print_r($propertyImageArray);exit;
        return view('property.edit', ['pageTitle' => 'Edit property',
            'pageHeading'                         => 'Edit property',
            'pageDesc'                            => 'Edit property here',
            'property'                            => $property,
            'landlord'                            => $landlord,
            'propertyImages'                      => $propertyImageArray,
        ]);
    }
    
    public function reviewEdit($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $propertyReview = PropertyReview::find($id);
        $ratingProperty = RatingUser::from('rating_user AS RU')
								->select('RU.rating_id')
								->addSelect('RU.rate_value')
								->addSelect('R.rating_name')
								->join('rating AS R','RU.rating_id','=','R.id')
								->where('RU.review_id',$id)
								->orderBy('R.id','ASC')
								->get();
								
		//print_r($ratingProperty);exit;
        
        $property 		= Property::where('id',$propertyReview->property_id)
								->select('property_name')
								->get()->first();
		
		$tenant 		= Tenant::where('id', $propertyReview->tenant_id)
								->select('firstname')
								->addSelect('lastname')
								->addSelect('id')
								->get()->first();				
		//dd($landlord);
        return view('property.reviewEdit', ['pageTitle' => 'Edit propertyreview',
            'pageHeading'                         => 'Edit propertyreview',
            'pageDesc'                            => 'Edit propertyreview here',
            'propertyReview'                      => $propertyReview,
            'property'                     		  => $property,
            'tenant'                              => $tenant,
            'propertyRating'					  => $ratingProperty
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		//dd($request);
        $rules = [
            'property_name'  => 'required',
            'location'    	=> 'required',
            'rent_price'  => 'required',
            'bathroom_no'  => 'required',
            'bedroom_no'  => 'required',
        ];
        
         $messages = [
            'property_name.required' 	=> 'Propertyname is required.',
            'location.required' 		=> 'Location is required.',
            'rent_price.required' 		=> 'RentPrice is required.',
            'bathroom_no.required' 		=> 'Bathroom is required.',
            'bedroom_no.required' 		=> 'Bedroom is required.',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) 
        {
            $updatedProperty = Property::where('id', $id)
									->update(['property_name'=>$request->property_name,'location'=>$request->location,'rent_price'=>$request->rent_price,'bathroom_no'=>$request->bathroom_no,'bedroom_no'=>$request->bedroom_no,'featured'=>$request->featured]);
			
            return redirect()->route('properties.index')->with('message', 'Property details are updated.');
            
        }
        else
        {
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}
    }
    
    public function reviewUpdate(Request $request, $id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
		
		 $rules = [
            'title'  => 'required',
            'review' => 'required',
        ];
         $messages = [
            'title.required' => 'Title is required.',
            'review.required' 	=> 'Review message is required.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) 
        {
		
			$updatedPropertyReview = PropertyReview::where('id', $id)
										->update(['title'=>$request->title,'review'=>$request->review]);
				
			return redirect()->route('propertyreview.index')->with('message', 'PropertyReview details are updated.');
	    } 
	    else
	    {
			return redirect()->back()->withErrors($validator->errors())->withInput();
		}  
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $property = Property::findOrFail($id);
		if($property->delete())
		{
			if( $property->delete() ){
        	$LandlordTenants = LandlordTenant::where('property_id',$id)
        									   ->get();
        	if( count($LandlordTenants) > 0 ){
        		$deleteSuccess = LandlordTenant::where( 'property_id', $id )->delete();
        		foreach( $LandlordTenants as $LandlordTenant){
	        		
	        		$landlordTenantDocs = landlordTenantDoc::where('landlord_tenant_id',$LandlordTenant->id)
	        												 ->get();
	        		$landlordTenantMonthlyrents = landlordTenantMonthlyrent::where('landlord_tenant_id',$LandlordTenant->id)
	        												 ->get();
	        		$landlordTenantOtherpayments = landlordTenantOtherpayment::where('landlord_tenant_id',$LandlordTenant->id)
	        												 ->get();										 										 
	        		if( count($landlordTenantDocs) > 0 ){
	        			$deleteSuccess = landlordTenantDoc::where( 'landlord_tenant_id', $LandlordTenant->id )->delete();
	        		}
	        		if( count($landlordTenantMonthlyrents) > 0 ){
	        			$deleteSuccess = landlordTenantMonthlyrent::where( 'landlord_tenant_id', $LandlordTenant->id )->delete();
	        		}
	        		if( count($landlordTenantOtherpayments) > 0 ){
	        			$deleteSuccess = landlordTenantOtherpayment::where( 'landlord_tenant_id', $LandlordTenant->id )->delete();
	        			foreach( $landlordTenantOtherpayments as $landlordTenantOtherpayment ){
		        			$landlordTenantOtherpaymentDetails  = landlordTenantOtherpaymentDetail::where('landlord_tenant_otherpayment_id',$landlordTenantOtherpayment->id)
		        												  ->get();
		        			if( count($landlordTenantOtherpaymentDetails) >0 ){
		        				$deleteSuccess = landlordTenantOtherpaymentDetail::where( 'landlord_tenant_otherpayment_id', $landlordTenantOtherpayment->id )->delete();
		        			}	
	        			}								  	
	        		}
        	}

        	}
        	$messageThreads = MessageThread::where('property_id',$id)
        									   ->get();								   	
        	if( count( $messageThreads ) > 0 ){
        		$deleteSuccess = MessageThread::where( 'property_id', $id )->delete();
        		foreach( $messageThreads as $messageThread ){
	        		$messages 	   = Message::where('message_thread_id',$messageThread->id)
	        									   ->get();
	        		if( count( $messages ) > 0 ){
	        			$deleteSuccess = Message::where( 'message_thread_id', $messageThread->id )->delete();
	        		}
        		} 							   				
        	}
        	$propertyDocs = PropertyDoc::where('property_id',$id)
        									   ->get();
        	if( count( $propertyDocs ) > 0 ){
        		$deleteSuccess = PropertyDoc::where( 'property_id', $id )->delete();
        	}
        	$propertyAmenity = PropertyAmenity::where('property_id',$id)
        									   ->get();
        	if( count( $propertyAmenity ) > 0 ){
        		$deleteSuccess = PropertyAmenity::where( 'property_id', $id )->delete();
        	}
        	$propertyComment = PropertyComment::where('property_id',$id)
        									   ->get();
        	if( count( $propertyComment ) > 0 ){
        		$deleteSuccess = PropertyComment::where( 'property_id', $id )->delete();
        	}
        	$propertyEnquiry = PropertyEnquiry::where('property_id',$id)
        									   ->get();
        	if( count( $propertyEnquiry ) > 0 ){
        		$deleteSuccess = PropertyEnquiry::where( 'property_id', $id )->delete();
        	}
        	$propertyExpressInterest = PropertyExpressInterest::where('property_id',$id)
        									   ->get();
        	if( count( $propertyExpressInterest ) > 0 ){
        		$deleteSuccess = PropertyExpressInterest::where( 'property_id', $id )->delete();
        	}
        	$propertyImage = PropertyImage::where('property_id',$id)
        									   ->get();
        	if( count( $propertyImage ) > 0 ){
        		$deleteSuccess = PropertyImage::where( 'property_id', $id )->delete();
        	}
        	$propertyRating = PropertyRating::where('property_id',$id)
        									   ->get();
        	if( count( $propertyRating ) > 0 ){
        		$deleteSuccess = PropertyRating::where( 'property_id', $id )->delete();
        	}
        	$propertyReview = PropertyReview::where('property_id',$id)
        									   ->get();
        	if( count( $propertyReview ) > 0 ){
        		$deleteSuccess = PropertyReview::where( 'property_id', $id )->delete();
        	}
        	$propertyViewed = PropertyViewed::where('property_id',$id)
        									   ->get();
        	if( count( $propertyViewed ) > 0 ){
        		$deleteSuccess = PropertyViewed::where( 'property_id', $id )->delete();
        	}
        	$ratingUser = RatingUser::where('property_id',$id)
        									   ->get();
        	if( count( $ratingUser ) > 0 ){
        		$deleteSuccess = RatingUser::where( 'property_id', $id )->delete();
        	}								   					
        	
        }
        	
		}

		return Redirect::route('landlords.getProperty')->with('message-success','Property has been deleted from the database');
    }
    
    public function getPropertyDetail(Request $request, $slug)
	 {
		
		$school    = $this->getSchoolDetails();
		$landlords = $this->getLandlords();
		
		foreach ($landlords as $landlord)
        {
            $landlordDropDown[$landlord->id] = $landlord->firstname.' '.$landlord->lastname;
        }
        
		$propertyDetail = Property::where('slug',$slug)
									-> select('id')
									-> addSelect('property_name')
									-> addSelect('slug')
									-> addSelect('rent_price')
									-> addSelect('landlord_id')
									-> addSelect('bedroom_no')
									-> addSelect('bathroom_no')
									-> addSelect('location')
									-> addSelect('description')
									-> addSelect('created_at')
									-> addSelect('communication_medium')
									-> addSelect('latitude')
									-> addSelect('longitude')
									-> get()->first();
		if(empty($propertyDetail))
		{ 
			return redirect('/404');
		}
		
		$propertyAmenities = PropertyAmenity::where('property_id',$propertyDetail->id)
									-> select('amenity')
									-> get();
		
		
		$propertyViewed = $this->logPropertyView($propertyDetail->id);//this method is for insert in to property viewed if not exist					
		
		$propertyReviewDetails = $this->getPropertyReviewDetails($propertyDetail->id);//this method is for fetch latest data frm property review
						
		$propertyImageDetail  = PropertyImage:: where('property_id',$propertyDetail->id)
												->select('image_name')
												->get();
		//echo count($propertyImageDetail);exit;
												
		$landlordEmail = Landlord::where('id',$propertyDetail->landlord_id)
					->select('email')
					->get()->first();
					
												
		
			if($propertyDetail->communication_medium == 'E')
			{
				$communication='email';
			}
			else
			{
				$communication='web';
			}
			
			$tenantName 	= $tenantEmail 	= $tenantPhone = $tenantId ='';

			if(Auth::user('tenant'))
			{
				$loggedTenant 	= Auth::user('tenant');
				$tenantName 	= $loggedTenant->getFullName();
				$tenantId 		= $loggedTenant->id;
				$tenantEmail 	= $loggedTenant->email;
				$tenantPhone 	= $loggedTenant->phone;
		    }
			
			$reviewsExist = $this->checkReviewExistTenantProperty($tenantId,$propertyDetail->id);//check if a review exist for a particular property for logged tenant
			
			$allRatings = $this->getRating();
			
			$tenantPropertyInterest = PropertyExpressInterest::where('property_id',$propertyDetail->id)
									->where('tenant_id',$tenantId)
									->get()->first();
			
			 return view('index.viewproperty',[
			 'pageTitle'   				=> $propertyDetail->property_name,
			 'propertyDetail'   		=> $propertyDetail,
			 'propertyImageDetail'		=> $propertyImageDetail,
			 'communication'   			=> $communication,
			 'landlordEmail'   			=> $landlordEmail->email,
			 'tenantName'   			=> $tenantName,
			 'tenantEmail'   			=> $tenantEmail,
			 'tenantPhone'   			=> $tenantPhone,
			 'tenantId'   				=> $tenantId,
			 'selectedSchool'			=> $school,
			 'selectedLandlordDropDown'	=> $landlordDropDown,
			 'selectedSchoolId'			=> "",
			 'selectedLandlordId'		=> "",
			 'selectedBedroomNo'		=> "",
			 'selectedDistance'			=> 10,
			 'selectedPriceFrom'		=> 0,
			 'selectedPriceTo'			=> 10000,
			 'searchKeyWord'			=> $propertyDetail->location,
			 'propertyReviewDetails'	=> $propertyReviewDetails,
			 'reviewExist'				=> $reviewsExist,
			 'allRatings'				=> $allRatings,
			 'tenantPropertyInterest'	=> $tenantPropertyInterest,
			 'propertyAmenities'		=> $propertyAmenities,
			 
			 ]);
		
		
	 }
	 
	 public function postEnquiry(Request $request)
	 {
		 $rules = [
            'name'             => 'required',
            'email'            => 'required|email',
            'subject'		   => 'required',
            'message'          => 'required'

        ];
         $validator = Validator::make($request->all(), $rules);
		 if ($validator->fails()) 
		 {
			return redirect()->back();
		 }else{
			$inputArr				= array();
			$inputArr['property_id']= $request->propertyId;
			$inputArr['tenant_id']	= $request->tenantId;
			$inputArr['tenant_phone'] =  $request->phone;
			$inputArr['message']	= $request->message;
			
			
			$input					=  array();
			$input['name'] 			=  $request->name;
			$input['email'] 		=  $request->email;
			$input['phone'] 		=  $request->phone;
			$input['landlordEmail'] =  $request->landlordEmail;
			$input['subject'] 		=  $request->subject;
			$input['message'] 		=  $request->message;
			$input['propertyName']	=  $request->pageTitle;
			$input['slug']			=  $request->slug;
			
			
			$propertyEnquiry 		= PropertyEnquiry::create($inputArr);
			
			$enquiryEmailSend 		= $this->enquiryEmail($input,$propertyEnquiry->created_at);
			if($enquiryEmailSend)
			{
				return redirect()->route('properties.getPropertyDetail',$input['slug'])->with('message-success', 'Your enquiry has been send!');
			}
		}
		
	 }
	 
	 
	
	//this method is used for send the enquiry email to the landlord
	public function enquiryEmail($input,$enquiryDate)
	{
		//print_r($input);exit;
		Mail::send('emails.enquiry',['enquiryDate'=>$enquiryDate,'propertyName'=>$input['propertyName'],'name'=>$input['name'],'messageContent' => $input['message'],'phone'=>$input['phone'],'emailFrom'=>$input['email']], function($message) use ($input){
					
					$message->to($input['landlordEmail'])
							->subject($input['subject']);
				});
		return true;
	}
	
	//this method is for insert in to property viewed
	private function logPropertyView($propertyId)
	{
		$sessionId = Session::getId();
		
		$loggedTenant= '';
		if(Auth::user('tenant'))
		{
			$loggedTenant = Auth::user('tenant')->id;
		}
		
		$propertyViewedData	= PropertyViewed::where('property_id',$propertyId)
							   ->where('session_id',$sessionId)
							   ->get()->first();
		
							   
		if(empty($propertyViewedData))
		{
			$propertyLog = PropertyViewed::create(['tenant_id'=>$loggedTenant,'property_id'=>$propertyId,'session_id'=>$sessionId]);
			
			$propertyViewed = Property::where('id', $propertyId)
					  ->increment('most_viewed', 1);
			 
			return true;
		}
		return false;
		
	}
	
	public function postReview(Request $request)
	{
		//dd($request);
		 $rules = [
            'reviewTitle'       => 'required',
            'review'            => 'required',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) 
		 {
			return redirect()->back()->withErrors($validator->errors())->withInput()->with('message-error','Please fill all the input fields');
			
		 }
		$propertyReviewd = PropertyReview::create(['property_id'=>$request->propertyId,'title'=>$request->reviewTitle ,'review'=>$request->review ,'tenant_id'=>$request->tenantId]);
		
		$propertyViewed = Property::where('id', $request->propertyId)
					  ->increment('most_reviewed',1);
					  
		$ratings = $this->getRating();
		
		$properties = Property::where('id', $request->propertyId)
								->with('landlord')
								->first();
		//save ratings (tble: rating_user)
		$i=1;
		foreach($ratings  as $key=>$val)
		{
			$ratingId	= $val['id'];
			$ratingName = $val['rating_name'];
			$rateValue 	= $request->{'hdRateValue'.$ratingId};
			
			
			$myReview 	= $this->saveMyRating($propertyReviewd->id,$ratingId,$rateValue,$request->propertyId,$request->tenantId);
			$avgRateVal = $this->getAverageRating($request->propertyId,$ratingId);
			$ratingproperties = DB::update('UPDATE properties SET rating_'.$ratingId.'='.$avgRateVal.' WHERE id='.$request->propertyId); 
			if($i==4)
			{
				$totalAvgRateVal = $this->getAverageRating($request->propertyId);
				$ratingproperties = DB::update('UPDATE properties SET rating_avg='.$totalAvgRateVal.' WHERE id='.$request->propertyId); 
			}
			$i++;
		}
		
		
		$updateRecomended = $this->updateLandlordRating($properties->landlord_id);
			
		
		
		
		$tenants    = Tenant::find( $request->tenantId );
		$tenantName = $tenants->firstname.' '.$tenants->lastname;
		
		//$updateRecomended = $this->updateAllProertiesRecomended($properties->landlord_id);
		
		$landLordName  = $properties->landlord->firstname.' '.$properties->landlord->lastname;							
		$landLordEmail = $properties->landlord->email;
		$propertyReviewUrl = URL::to('/').'/property/'.$properties->slug.'#reviews';
		
		Mail::send('emails.reviewToLandlord', ['landLordName'=>$landLordName,'tenantName'=>$tenantName,'propertyReviewUrl'=>$propertyReviewUrl], function ($message) use ($landLordName,$landLordEmail) {
                    $message->to($landLordEmail, $landLordName)
                            ->subject('TenantU - A tenant has reviewd your property.');
                });
		return Redirect::to('property/'.$request->slug)->with('message','Your review has been submitted!');
	}
	
	//this method is for find the avg rating 
	
	private function getAverageRating($propertyId,$rateId=null)
	{
		if($rateId!='')
		{
			$whereCondition = ' WHERE property_id='.$propertyId.' AND rating_id='.$rateId;
		}
		else
		{
			$whereCondition = ' WHERE property_id='.$propertyId;
		}
		
		$avgRate = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user'.$whereCondition );
		$avgRateValue = $avgRate[0]->rate_value;
		$avgRateValueRounded = ceil($avgRateValue);
		
		return $avgRateValueRounded;
	}
	
	private function getPropertyReviewDetails($propertyId)
	{
		

		$propertyReviewDetails = PropertyReview::from('property_review AS PR')
                                 -> select('PR.title')
                                 -> addSelect('PR.id')
                                 -> addSelect('PR.review')
                                 -> addSelect('T.firstname')
                                 -> addSelect('T.lastname')
                                 -> join ('tenants AS T','PR.tenant_id','=','T.id')
                                 -> where ('PR.property_id',$propertyId)
                                 -> where ('PR.status',1)
                                 -> orderBy('PR.created_at','DESC')
                                 -> take(5)
                                 -> get();
		
        return $propertyReviewDetails;                        
	}
	
	private function getMyRating($tenantId,$propertyId)
	{
		
		$tenantRating = RatingUser:: where('property_id',$propertyId)
					-> where('tenant_id',$tenantId)
					-> select('rating_id')
					-> addSelect('rate_value')
					->addSelect('review_id')
					->get();
		return $tenantRating;
	}
	
	private function checkReviewExistTenantProperty($tenantId,$propertyId)
	{
		$review = PropertyReview::where('property_id',$propertyId)
						->where('tenant_id',$tenantId)
						->select('title')
						->addSelect('review')
						->get();
		
		return $review;
	}
	
	private function saveMyRating($reviewId,$ratingId,$rateValue,$propertyId,$tenantId)
	{
		
		$ratingSaved = RatingUser::create(['rating_id'=>$ratingId,'property_id'=>$propertyId,'tenant_id'=>$tenantId,'review_id'=>$reviewId,'rate_value'=>$rateValue]);
		return true;
	}
	
	public function interest($propertyId)
	{
		$tenantId = Auth::user('tenant')->id;
		$interestedTenant = Tenant::where('id',$tenantId)
								->select('firstname')
								->addSelect('lastname')
								->addSelect('email')
								->addSelect('phone')
								->get()->first();
								
		$tenantFullName = $interestedTenant ->getFullName();
		
		$interestedProperty = Property::where('id',$propertyId)
										->select('property_name')
										->addSelect('landlord_id')
										->addSelect('slug')
										->get()->first();
		$interestLandlord = Landlord::where('id',$interestedProperty->landlord_id)
							->select('firstname')
							->addSelect('lastname')
							->addSelect('email')
							->get()->first();
			
		$landlordEmail = $interestLandlord->email;
		$landlordFullName = $interestLandlord->getFullName();
		
		$propertyExpressInterest = PropertyExpressInterest::create(['property_id'=>$propertyId,'tenant_id'=>$tenantId,'landlord_id'=>$interestedProperty->landlord_id]);
						
		Mail::send('emails.interest',['tenantName'=>$tenantFullName,'landlordName'=>$landlordFullName,
										'landlordEmail'=>$interestLandlord->email,'propertyName'=>$interestedProperty->property_name,
										'tenantEmail'=>$interestedTenant->email,'tenantPhone' => $interestedTenant->phone], 
										function($message) use ($landlordEmail) {
					
					$message->to($landlordEmail)
							->subject("Express Interest");
				});
		
		return redirect()->route('properties.getPropertyDetail',$interestedProperty->slug)->with('message-success', 'Your interest has been expressed to the landlord.');
						
	}
public function editProperty( $id ){
	
		$loggedLandlordId = Auth::user('landlord')->id;
        $pageTitle        = "Property Add";
        $pageMenuName	  = "myproperty";
        $school           = $this->getSchoolDetails();
        
        $originalProfile  = HTML::getLandlordProfileImage();
        $profileImage     = $originalProfile;
        $profile          = $this->getLandlordProfile($loggedLandlordId);
        $properties       = $this->getProperties( $loggedLandlordId );
        
        //$schoolDetails = $this->getSchoolLocationDetails($id);
        //dd($schoolDetails);
        $aminities  	  = $this->getAminityDetails();
        $propertyList     = $this->findProperty( $id );
		//print_r($propertyList);exit;
        $schoolLocation = $this->getSchoolLocationDetails($propertyList->school_id);
        //dd($schoolLocation);
        $skillsFlag = false;
        if(count( $propertyList) > 0)
        {
			$skillsFlag = true;
		}

                                          
        return view('property.editproperty')->with(compact('loggedLandlordId','pageTitle','pageMenuName','school','properties','profileImage','profile','propertyList','id','skillsFlag','aminities','schoolLocation'));
	}
	
	private function getSchoolLocationDetails($schoolId){
		$school = School::where('id',$schoolId)
		->select('latitude')
		->addSelect('longitude')
		->first();
		return $school->latitude.','.$school->longitude;
	}
	
	private function getAminityDetails()
	 {
		 $aminity = array();
		 $aminity  = DB::table('amenities')->where('status',1)->orderBy('amenity_name', 'asc')->lists('amenity_name', 'amenity_name');
		 return $aminity;
	 }
	
	public function postEditProperty( Request $request, $id ){
        //dd($request->all());
        $rules = array(
            'property_name'             => 'required',
            'school_id'                 => 'required',
            'location'                  => 'required',
            'bedroom_no'                => 'required|numeric',
            'rent_price'                => 'required|numeric',
            'description'               => 'required',
            'communication_medium'      => 'required', 
            'distance_from_school'      => 'required',
        );
        $messages = [
                        'property_name.required'             => 'Property Name field is Required',
                        'school_id.required'                 => 'School Name field is Required',
                        'location.required'                  => 'Location field is Required',
                        'bedroom_no.required'                => 'Bedroom field is Required',
                        'bedroom_no.numeric'                 => 'Please enter a valid number in Bedroom',
                        'rent_price.required'                => 'Rent field is Required',
                        'rent_price.numeric'                 => 'Please enter a valid amount as Rent',
                        'description.required'               => 'Description field is Required',
                        'communication_medium.required'      => 'Communication Medium field is Required',
                        'distance_from_school.required'      => 'Please enter a valid distance'
                    ];
        $validator = Validator::make($request->all(), $rules, $messages );
        if ($validator->fails()) 
        {
             return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        else
        {
        	//dd($request->all());
            $loggedLandlord                      =  Auth::user('landlord');
            $landLordName                        =  $loggedLandlord->firstname.','.$loggedLandlord->lastname;
            $loggedLandlordId                    =  $loggedLandlord->id;    
            $property                  			 =  Property::find( $id );
			$property->property_name        	 =  $request->property_name;
			$property->landlord_id          	 =  $loggedLandlordId;   
			$property->school_id            	 =  $request->school_id;
			$property->location             	 =  $request->location[0];
			$property->bedroom_no           	 =  $request->bedroom_no;
			$property->bathroom_no          	 =  $request->bathroom_no;
			$property->rent_price           	 =  $request->rent_price;
			$property->description          	 =  $request->description;
			$property->communication_medium 	 =  $request->communication_medium;
			$property->distance_from_school 	 =  $request->distance_from_school;
            //list($month, $day, $year)          =  explode('/', $request->expireon);
            list($latitude, $longitude)          =  explode('~', $request->hdPlace);
            //$property->expireon             	 =  $year.'-'.$month.'-'.$day;
            $property->latitude             	 =  $latitude;
            $property->longitude            	 =  $longitude;
            $property->availability_status       =  $request->availability_status;
            //dd($insertArray);
            $school    = School::where('id',$request->school_id)
                                ->select('schoolname')->first();
            $searchField  = $request->property_name.','.$school->schoolname.','.$request->description.','.$request->location[0].','.$landLordName;                    
            $property->search_field       =  $searchField;
            $property = $property->update();
            //dd($property);
            if( $property && $request->img_name!="" ){
                $images   = explode(',',$request->img_name);
                //dd($images);
                foreach ($images as $image) {
                    $propertyImage = new propertyImage;
                    $propertyImage->property_id = $id;
                    $propertyImage->image_name  = $image;
                    $propertyImage->status = 1;
                    $propertyImage->save();
                }
                
               

            }
            //dd($request->amenities);exit;
            $amenityArray = [];
            if($property && $request->aminity!=""){
            	$propertyAmenity = PropertyAmenity::where('property_id', $id );                
                $propertyAmenity->delete();
                foreach ($request->aminity as $amenity) {
                	$amenityArray[] = $amenity;
                    $data[] = array('property_id'=>$id,
                                'amenity'=>$amenity,
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s'));
                }
                    
                $success = PropertyAmenity::insert($data);
                if(!empty($amenityArray)){
	               $amenities = implode(',', $amenityArray); 
	               $propertyUpdate  = Property::where('id',$id)
	                                            ->update(['amenities'=>$amenities]); 
	            }
            }
            return redirect()->route('property.add')->with('message-success','Property updated successfully!');        
        }
        
    }
    public function getProperty(){
	     $loggedLandlord = Auth::user('landlord');
	     $loggedLandlordId     = $loggedLandlord->id;
	     $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
	     $pageTitle            = "Contacts"; 
	     $originalProfile=HTML::getLandlordProfileImage(); 
	        //this method calling is for the landlords frm msg thread 
	     $properties = $this->getAllProperty( $loggedLandlord->id );
	     //dd( $properties );   
	                
	        return view('property.properties', [
	            'pageTitle'     => 'My Contacts',
	            'pageHeading'   => 'My Contacts',
	            'pageMenuName'	=> 'myproperty',
	            'properties'    => $properties,
	            'profileImage'  => $originalProfile,
	            'profile'       => $landlordProfile
	        ]);
    }
	
	public function changeStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $property = Property::find($id);
        if ($property->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $property->update($data);
        return redirect()->route('properties.index')->with('message', 'Property status updated');
    }
    public function deleteProperty($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $property = Property::find( $id );
        if( $property->delete() ){
        	$LandlordTenants = LandlordTenant::where('property_id',$id)
        									   ->get();
        	if( count($LandlordTenants) > 0 ){
        		$deleteSuccess = LandlordTenant::where( 'property_id', $id )->delete();
        		foreach( $LandlordTenants as $LandlordTenant){
	        		
	        		$landlordTenantDocs = landlordTenantDoc::where('landlord_tenant_id',$LandlordTenant->id)
	        												 ->get();
	        		$landlordTenantMonthlyrents = landlordTenantMonthlyrent::where('landlord_tenant_id',$LandlordTenant->id)
	        												 ->get();
	        		$landlordTenantOtherpayments = landlordTenantOtherpayment::where('landlord_tenant_id',$LandlordTenant->id)
	        												 ->get();										 										 
	        		if( count($landlordTenantDocs) > 0 ){
	        			$deleteSuccess = landlordTenantDoc::where( 'landlord_tenant_id', $LandlordTenant->id )->delete();
	        		}
	        		if( count($landlordTenantMonthlyrents) > 0 ){
	        			$deleteSuccess = landlordTenantMonthlyrent::where( 'landlord_tenant_id', $LandlordTenant->id )->delete();
	        		}
	        		if( count($landlordTenantOtherpayments) > 0 ){
	        			$deleteSuccess = landlordTenantOtherpayment::where( 'landlord_tenant_id', $LandlordTenant->id )->delete();
	        			foreach( $landlordTenantOtherpayments as $landlordTenantOtherpayment ){
		        			$landlordTenantOtherpaymentDetails  = landlordTenantOtherpaymentDetail::where('landlord_tenant_otherpayment_id',$landlordTenantOtherpayment->id)
		        												  ->get();
		        			if( count($landlordTenantOtherpaymentDetails) >0 ){
		        				$deleteSuccess = landlordTenantOtherpaymentDetail::where( 'landlord_tenant_otherpayment_id', $landlordTenantOtherpayment->id )->delete();
		        			}	
	        			}								  	
	        		}
        	}

        	}
        	$messageThreads = MessageThread::where('property_id',$id)
        									   ->get();								   	
        	if( count( $messageThreads ) > 0 ){
        		$deleteSuccess = MessageThread::where( 'property_id', $id )->delete();
        		foreach( $messageThreads as $messageThread ){
	        		$messages 	   = Message::where('message_thread_id',$messageThread->id)
	        									   ->get();
	        		if( count( $messages ) > 0 ){
	        			$deleteSuccess = Message::where( 'message_thread_id', $messageThread->id )->delete();
	        		}
        		} 							   				
        	}
        	$propertyDocs = PropertyDoc::where('property_id',$id)
        									   ->get();
        	if( count( $propertyDocs ) > 0 ){
        		$deleteSuccess = PropertyDoc::where( 'property_id', $id )->delete();
        	}
        	$propertyAmenity = PropertyAmenity::where('property_id',$id)
        									   ->get();
        	if( count( $propertyAmenity ) > 0 ){
        		$deleteSuccess = PropertyAmenity::where( 'property_id', $id )->delete();
        	}
        	$propertyComment = PropertyComment::where('property_id',$id)
        									   ->get();
        	if( count( $propertyComment ) > 0 ){
        		$deleteSuccess = PropertyComment::where( 'property_id', $id )->delete();
        	}
        	$propertyEnquiry = PropertyEnquiry::where('property_id',$id)
        									   ->get();
        	if( count( $propertyEnquiry ) > 0 ){
        		$deleteSuccess = PropertyEnquiry::where( 'property_id', $id )->delete();
        	}
        	$propertyExpressInterest = PropertyExpressInterest::where('property_id',$id)
        									   ->get();
        	if( count( $propertyExpressInterest ) > 0 ){
        		$deleteSuccess = PropertyExpressInterest::where( 'property_id', $id )->delete();
        	}
        	$propertyImage = PropertyImage::where('property_id',$id)
        									   ->get();
        	if( count( $propertyImage ) > 0 ){
        		$deleteSuccess = PropertyImage::where( 'property_id', $id )->delete();
        	}
        	$propertyRating = PropertyRating::where('property_id',$id)
        									   ->get();
        	if( count( $propertyRating ) > 0 ){
        		$deleteSuccess = PropertyRating::where( 'property_id', $id )->delete();
        	}
        	$propertyReview = PropertyReview::where('property_id',$id)
        									   ->get();
        	if( count( $propertyReview ) > 0 ){
        		$deleteSuccess = PropertyReview::where( 'property_id', $id )->delete();
        	}
        	$propertyViewed = PropertyViewed::where('property_id',$id)
        									   ->get();
        	if( count( $propertyViewed ) > 0 ){
        		$deleteSuccess = PropertyViewed::where( 'property_id', $id )->delete();
        	}
        	$ratingUser = RatingUser::where('property_id',$id)
        									   ->get();
        	if( count( $ratingUser ) > 0 ){
        		$deleteSuccess = RatingUser::where( 'property_id', $id )->delete();
        	}								   					
        	
        }
        
        return redirect()->route('properties.index')->with('message', 'Property deleted successfully');
    }
    
    public function changeReviewStatus($id)
    {
		$adminUser = Auth::User('admin');
		if(empty($adminUser)){
			return redirect()->route('admins.getLogin');
		}
		
        $propertyReview = PropertyReview::find($id);
        if ($propertyReview->status) {
            $data['status'] = 0;
        } else {
            $data['status'] = 1;
        }

        $propertyReview->update($data);
        return redirect()->route('propertyreview.index')->with('message', 'PropertyReview status updated');
    }
    public function saveImage( Request $request ){
    
    	$updateFeaturedImage  = propertyImage::where('property_id',$request->propId)
    									->where('featured',1)
    									->update(['featured'=>0]);							
    	$propertyImage = new propertyImage;
		$propertyImage->property_id  = $request->propId;
		$propertyImage->image_name   = $request->imgName;
		$propertyImage->status   = 1;
		$propertyImage->featured   = 1;
        if($propertyImage->save()){
           
            /* code for image resize */
                
				//dd($propertyImages);
				$imagePath 			= Config::get('constants.PROPERTY_IMAGE_PATH');
				
				
					$imgSquare = Image::make($imagePath.'/square/'.$propertyImage->image_name)->resize(300,300);
					$imgSquare->save();
					$imgThumb = Image::make($imagePath.'/thumb/'.$propertyImage->image_name)->resize(300,300);
					$imgThumb->save();
					$img = Image::make($imagePath.$propertyImage->image_name)->resize(800,485);
					$img->save();
					
				
               /* end of image resize */ 
               return "1"; 
        }
        else{
           return "0";
        }
    }
    public function deleteImage( Request $request ){
    	$deleteSuccess = propertyImage::where('image_name', $request->imgName)
    									->where('property_id',$request->propId)
    									->delete();
        if( $deleteSuccess ){
          
           return "1"; 
        }
        else{
           return "0";
        }
    }
    public function deleteImageDB( Request $request ){
    	$propertyImgId  = $request->propImgId;
    	$propertyId     = $request->propId;
    	//echo $propertyImgId;exit;

    	$propertyImage = propertyImage::find( $propertyImgId );
    	//dd($propertyImage);
    	if( $propertyImage->delete() ){
           $destinationPath = Config::get('constants.PROPERTY_IMAGE_PATH');
           unlink($destinationPath.$propertyImage->image_name);
           $propertyFeaturedCount = propertyImage::where('property_id', $propertyId)
    									 ->where('featured', 1)
    									 ->count();

    	   if(  $propertyFeaturedCount  == 0){
    	   		$propertyFeatured = propertyImage::where('property_id', $propertyId)
    	   										  ->first();
    	   		//print_r($propertyFeatured);exit;								  
    	   									  
    	   		if( count($propertyFeatured) ){
    	   			$pkId = $propertyFeatured->id;
    	   			$updateFeaturedImage  = propertyImage::where('id',$pkId)
    									->update(['featured'=>1]);
    	   		}								  	
    									 
    	   }								 	
           return "1"; 
        }
        else{
           return "0";
        }
    	 
    }
    public function setImageFeatured( Request $request ){
    	/*$propertyImgCount  = propertyImage::where('image_name', $request->imgName)
    									->where('property_id',$request->propId)
    									->where('featured',1)
    									->count(); 
    	echo $propertyImgCount;exit;*/

    	$updateFeaturedImage  = propertyImage::where('property_id',$request->propId)
    									->where('featured',1)
    									->update(['featured'=>0]);
    	$propertyImage   = 	propertyImage::find($request->propImgId);							
    	$propertyImage->featured=1;
        if($propertyImage->update()){
           return "1"; 
        }
        else{
           return "0";
        }
    }
     public function saveDoc( Request $request ){
    
    	$propertyDoc = new PropertyDoc;
		$propertyDoc->property_id  = $request->propId;
		$propertyDoc->doc_name   = $request->docName;
		$propertyDoc->status   = 1;
        if($propertyDoc->save()){
           return "1"; 
        }
        else{
           return "0";
        }
    }
    public function deleteDoc( Request $request ){
    	$deleteSuccess = PropertyDoc::where('doc_name', $request->docName)
    									->where('property_id',$request->propId)
    									->delete();
        if( $deleteSuccess ){
          
           return "1"; 
        }
        else{
           return "0";
        }
    }
    public function deleteDocDB( Request $request ){
    	$propertyDocId  = $request->propDocId;
    	$propertyDoc = PropertyDoc::find( $propertyDocId );
    	if( $propertyDoc->delete() ){
           $destinationPath = Config::get('constants.PROPERTY_IMAGE_PATH');
           unlink($destinationPath.$propertyDoc->doc_name); 	
           return "1"; 
        }
        else{
           return "0";
        }
    	 
    }
    
    public function getAllReviewsOfProperty($slug)
    {
		$propertyId = $this->getPropertyIdFromSlug($slug);
		
		$allReviews = $this->getPropertyReviewDetails($propertyId->id);
		$allRatings = $this->getRating();
		//print_r($allReviews);exit;
		return view('index.viewpropertyreviews',[
			 'pageTitle'   				=> 'Property Reviews',
			 'reviews'   				=> $allReviews,
			 'allRatings'				=> $allRatings
			 ]);
			
	}
	
	private function getPropertyIdFromSlug($slug)
	{
		$proId = Property::where('slug',$slug)
				->select('id')
				->get()->first();
		return $proId;
	}
	
public function getPropertyReviews($slug=null){
    	
    	$loggedLandlord = Auth::user('landlord');
	    $loggedLandlordId     = $loggedLandlord->id;
	    $landlordProfile      = $this->getLandlordProfile($loggedLandlordId);
	    $originalProfile=HTML::getLandlordProfileImage(); 
	    //this method calling is for the landlords frm msg thread 
	    
	    $reviewsQuery  = PropertyReview::from('property_review AS PR')
                                 -> select('PR.property_id')
                                 -> addSelect('PR.title')
                                 -> addSelect('PR.review')
                                 -> addSelect('PR.id')
                                 -> addSelect('T.firstname')
                                 -> addSelect('T.lastname')
                                 -> addSelect('P.property_name')
                                 -> join ('properties AS P','PR.property_id','=','P.id')
                                 -> join('tenants AS T', 'PR.tenant_id', '=', 'T.id')
                                 -> where('P.landlord_id', $loggedLandlordId)
                                 -> where('PR.status', 1)
                                 -> orderBy('PR.created_at','DESC');
        if($slug!=""){
        	$reviewsQuery->where('P.slug',$slug);
        }                         
        $reviews = $reviewsQuery-> get();
        $ratings = Rating::get();
        
        $properties = PropertyReview::from('property_review AS PR')
                                 -> select('P.id')
                                 -> addSelect('P.property_name')
                                 -> addSelect('P.slug')
                                 -> join ('properties AS P','PR.property_id','=','P.id')
                                 -> where('P.landlord_id', $loggedLandlordId)
                                 -> where('PR.status', 1)
                                 -> orderBy('PR.created_at','DESC')
                                 -> get();                         
        //dd( $properties ); 
        $propertyDropdown =[];                         
        foreach ($properties as $property)
        {
            $propertyDropdown[$property->slug] = $property->property_name;
        }                       
    	return view('property.propertyreviews', [
				'pageTitle'   		=> 'Review',
				'pageHeading' 		=> 'Review',
				'pageMenuName'		=> 'Review',
				'profileImage'    	=> $originalProfile,
				'profile'         	=> $landlordProfile,
				'reviews'			=> $reviews,
				'propertyDropdown'  => $propertyDropdown,
				'slug'				=> $slug,
				'ratings'			=> $ratings
			]);

    }

    public function fetchAmenities( Request $request ){
		
    	$searchKey  = $request->search;
    	$amenities = Amenity::where('status',1)
    						->Where('amenity_name', 'like', '%' . $searchKey . '%')
    						->get();		
    	   $amenityArry = array();
		   if( count($amenities) ){
			  
			  foreach($amenities as $amenity){
				$amenityArry[] =  $amenity->amenity_name;
			  	
			  } 
			  
		   }
		   echo json_encode($amenityArry);
		   exit;
    }
	
	
}
