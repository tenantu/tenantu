<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\CategoryType;
use App\Http\Models\CompetitionPost;
use App\Http\Models\FeaturedPost;
use App\Http\Models\Post;
use App\Http\Models\PostCountry;
use App\Http\Models\User;
use App\Http\Models\UserFollower;
use App\Http\Models\UserPoint;
use Auth;
use Config;
use DB;
use File;
use Hash;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Input;
use Intervention\Image\ImageManagerStatic as Image;
use Lang;
use Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Validator;
use View;
use Crypt;

class UsersController extends Controller
{

    /**
     * Display user profile.
     *
     * @return Response
     */
    public function dashboard(Request $request)
    {
        $pageTitle = 'Dashboard';

        // Set actvie navigation tab in view file.
        $activeTab = ($request->has('tab')) ? $request->input('tab') : '';

        $articles = Post::with('category')
            ->orderBy('created_at', 'desc')
            ->where('type_id', 1)
            ->where('user_id', Auth::User()->id)
            ->paginate(10, ['*'], 'page_article');

        $images = Post::with('category')
            ->orderBy('created_at', 'desc')
            ->where('type_id', 2)
            ->where('user_id', Auth::User()->id)
            ->paginate(10, ['*'], 'page_image');

        $imageUrl       = Config::get('constants.USER_IMAGE_URL');
        $user           = Auth::User();
        $locations      = [];
        $user_followers = UserFollower::where('follower_id', $user->id)->count();
        $user_following = UserFollower::where('user_id', $user->id)->count();
        $user_points    = DB::table('user_points')
            ->leftjoin('points', 'user_points.point_id', '=', 'points.id')
            ->select('user_points.user_id')
            ->where('user_id', $user->id)
            ->sum('points.points');

        list($user['year'], $user['month'], $user['day']) = explode('-', $user['dob']);

        if ($user->location) {
            $locations = explode('//', $user->location);
        }

        $parentCategories = CategoryType::with('category')
            ->where('type_id', 2)
            ->where('is_active', 1)
            ->get();

        return view('users.dashboard')->with(compact(['pageTitle', 'articles', 'activeTab', 'user', 'imageUrl', 'locations', 'user_followers', 'user_following', 'user_points', 'images', 'parentCategories']));
    }

    /**
     * Edit user profile.
     *
     * @return Response
     */
    public function editProfile()
    {
        $pageTitle                                        = 'Edit Profile';
        $imageUrl                                         = Config::get('constants.USER_IMAGE_URL');
        $user                                             = Auth::User();
        list($user['year'], $user['month'], $user['day']) = explode('-', $user['dob']);
        $locations                                        = [];
        if ($user->location) {
            $locations = explode('//', $user->location);
        }
        return view('users.dashboard', ['pageTitle' => $pageTitle,
            'user'                                      => $user,
            'imageUrl'                                  => $imageUrl,
            'locations'                                 => $locations,
        ]);
    }

    /**
     * Admin landing page after login.
     *
     * @return View
     */
    public function adminDashboard()
    {
        return view('users.adminDashboard', ['pageTitle' => 'Dashboard',
            'pageHeading'                                    => 'Dashboard',
            'pageDesc'                                       => 'it all starts here',
        ]);
    }

    /**
     * Profile edit for admin.
     *
     * @return View
     */
    public function adminProfile()
    {
        $imageUrl                                         = Config::get('constants.USER_IMAGE_URL');
        $user                                             = Auth::User();
        list($user['year'], $user['month'], $user['day']) = explode('-', $user['dob']);
        $locations                                        = [];
        if ($user->location) {
            $locations = explode('//', $user->location);
        }

        return view('users.adminProfile', [
            'pageTitle'   => 'Edit Profile',
            'pageHeading' => 'Edit Profile',
            'pageDesc'    => 'manage your profile here',
            'user'        => $user,
            'imageUrl'    => $imageUrl,
            'locations'   => $locations,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return View
     */
    public function adminStore(Request $request)
    {
        $rules = [
            'name'                  => 'required',
            'password'              => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3',
            'email'                 => 'required|email|unique:users',
            'image'                 => 'mimes:jpeg,jpg,gif,png',

        ];
        $input     = $request->except('image');
        $imagePath = Config::get('constants.USER_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            $input['password'] = bcrypt($input['password']);

            $input['dob'] = $input['year'] . "-" . $input['month'] . "-" . $input['day'];
            if ($request->has('locations')) {
                $locations         = $input['locations'];
                $input['location'] = implode('//', $locations);

            }

            $user = User::create($input);

            if ($request->file('image')) {
                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                $imagePath = $imagePath . $user['id'];
                if (!File::exists($imagePath)) {
                    // path does not exist
                    File::makeDirectory($imagePath, $mode = 0777, true, true);
                    File::copy(Config::get('constants.HTACCESS_FILE'), $imagePath . DS . '.htaccess');
                    File::makeDirectory($imagePath . '/60_x_60', $mode = 0777, true, true);

                }

                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(60, 60)->save($imagePath . '/60_x_60/' . $filename);
                // $userid = $user->id;
                $user = User::find($user->id);
                $user->update(['image' => $filename]);

            }
            return redirect()->route('users.adminIndex')->with('message', 'User Created.');
        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteImage(Request $request)
    {
        $user_id           = $request->input('user_id');
        $user              = User::find($user_id);
        $filename          = $user['image'];
        $path              = public_path('uploads/users/' . $user_id . '/60_x_60/');
        $orginalPath       = public_path('uploads/users/' . $user_id . '/');
        $pathToFile        = $path . $filename;
        $pathToOrginalFile = $orginalPath . $filename;

        File::delete($pathToFile);
        File::delete($pathToOrginalFile);

        $user->image = '';
        $updated     = $user->save();

        if ($updated) {
            return 'ok';
        } else {
            return 'ko';
        }

    }

    /**
     * Display the password reset view for the given token.
     *
     * @return \Illuminate\Http\Response
     */
    public function getChangePassword()
    {
        return view('users.adminChangePassword', [
            'pageTitle'   => 'Change Password',
            'pageHeading' => 'Change Password',
            'pageDesc'    => 'manage your login password',
        ]);
    }

    /**
     * Reset the given user's password.
     *
     */
    public function postChangePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required',
            'password'         => 'required|confirmed|min:6',
        ]);
        $userUpdate = $request->all();
        $value      = $userUpdate['current_password'];
        $user       = Auth::User();
        if (Hash::check($value, $user->password)) {
            $userpassword = Hash::make($userUpdate['password']);
            $user         = DB::table('users')
                ->where('id', $user->id)
                ->update(['password' => $userpassword]);
            return redirect()->route('users.postChangePassword')->with('message-success', 'Your password has been updated.');

        } else {
            return redirect()->route('users.getChangePassword')->with('message-error', 'Your current password is wrong.');
        }
    }

    public function adminIndex(Request $request)
    {
        $search     = ($request->has('search')) ? $request->input('search') : '';
        $sortby     = ($request->has('sortby')) ? $request->input('sortby') : 'created_at';
        $order      = ($request->has('order')) ? $request->input('order') : 'desc';
        $usersQuery = DB::table('users');
        if ($request->has('search')) {
            $search = $request->input('search');
            $usersQuery->where('name', 'like', '%' . $request->input('search') . '%');
        }
        $usersQuery->orderBy($sortby, $order);
        $users = $usersQuery->paginate(10);
        $order = ($order == 'asc') ? 'desc' : 'asc';
        return view('users.adminIndex', ['pageTitle' => 'Users',
            'pageHeading'                                => 'Users',
            'pageDesc'                                   => 'Manage users here',
            'users'                                      => $users,
            'search'                                     => $search,
            'sortby'                                     => $sortby,
            'order'                                      => $order,
        ]);
    }

    public function adminCreate()
    {
        $role = DB::table('roles')->lists('name', 'id');
        return view('users.adminCreate', ['pageTitle' => 'Create User',
            'pageHeading'                                 => 'Create User',
            'pageDesc'                                    => 'Create users here',
            'role'                                        => $role,
        ]);
    }

    public function adminChangeStatus($id)
    {
        $user = User::find($id);
        if ($user->is_active) {
            $data['is_active'] = 0;
        } else {
            $data['is_active'] = 1;
        }

        $user->update($data);
        return redirect()->route('users.adminIndex')->with('message', 'User Status Updated');
    }

    public function adminDestroy($id)
    {
        $user = User::find($id);

        $competitions = $user->competitions()->get();
        foreach ($competitions as $competition) {
            CompetitionPost::where('competition_id', $competition->id)->delete();
        }

        $user->competitions()->delete();
        $user->competition_winners()->delete();

        $featuredPosts = $user->posts()->get();
        foreach ($featuredPosts as $featuredPost) {
            FeaturedPost::where('post_id', $featuredPost->id)->delete();
        }

        $postCountries = $user->posts()->get();
        foreach ($postCountries as $postCountry) {
            PostCountry::where('post_id', $postCountry->id)->delete();
        }

        $forumcomments = $user->forum_comments()->delete();

        foreach ($postCountries as $postCountry) {
            PostCountry::where('post_id', $postCountry->id)->delete();
        }

        $user->posts()->delete();
        $user->forums()->delete();
        $user->likes()->delete();
        $user->user_followers()->delete();
        $user->user_points()->delete();
        $user->delete();
        return redirect()->route('users.adminIndex')->with('message', 'User Deleted.');
    }

    public function adminBulkDestroy(Request $request)
    {
        $rules = [
            'selected-users' => 'required',
        ];
        $messages = [
            'selected-users.required' => 'Please select the Users.',
        ];

        $input     = $request->all();
        $validator = Validator::make($input, $rules, $messages);
        if (!$validator->fails()) {
            $users = explode(",", $request->input('selected-users'));
            if ($request->input('selected-users') != '') {
                foreach ($users as $id) {
                    $user = User::find($id);
                    $user->delete();
                }
                return redirect()->route('users.adminIndex')->with('message', 'Selected Users Deleted.');
            }
        }
        return redirect()->back()->withErrors($validator->errors());
    }

    public function adminEdit($id)
    {
        $imageUrl                                         = Config::get('constants.USER_IMAGE_URL');
        $user                                             = User::find($id);
        $role                                             = DB::table('roles')->lists('name', 'id');
        list($user['year'], $user['month'], $user['day']) = explode('-', $user['dob']);
        $locations                                        = [];
        if ($user->location) {
            $locations = explode('//', $user->location);
        }

        return view('users.adminEdit', ['pageTitle' => 'Edit User',
            'pageHeading'                               => 'Edit User',
            'pageDesc'                                  => 'Edit user here',
            'user'                                      => $user,
            'imageUrl'                                  => $imageUrl,
            'role'                                      => $role,
            'locations'                                 => $locations,
        ]);
    }

    /**
     * Updates user profiles for both back-end and front-end.
     *
     */
    public function adminUpdate(Request $request, $id)
    {
        $rules = [
            'name'  => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
        ];

        $messages = [
            'name.required'  => 'Name is required.',
            'email.required' => 'A valid email is required.',
        ];

        if ($request->file('image') || $request->input('image') == '') {
            $rules = [
                'image' => 'required|mimes:jpeg,jpg,gif,png',
            ];

            $messages = [
                'image.required' => 'Image is required.',
            ];
        }

        $input     = $request->except('image');
        $imagePath = Config::get('constants.USER_IMAGE_PATH');
        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {

            $user         = User::find($id);
            $input['dob'] = $input['year'] . "-" . $input['month'] . "-" . $input['day'];

            if ($request->has('locations')) {
                $locations         = $input['locations'];
                $input['location'] = implode('//', $locations);

            } else {
                $input['location'] = '';
            }

            $user->update($input);

            if ($request->file('image')) {

                $image     = $request->file('image');
                $extension = $image->getClientOriginalExtension();
                $filename  = trim(pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME));
                $filename  = str_slug($filename, "-") . '.' . $extension;
                $imagePath = $imagePath . $user['id'];

                if (!File::exists($imagePath)) {
                    // path does not exist
                    File::makeDirectory($imagePath, $mode = 0777, true, true);
                    File::copy(Config::get('constants.HTACCESS_FILE'), $imagePath . DS . '.htaccess');
                    File::makeDirectory($imagePath . '/60_x_60', $mode = 0777, true, true);

                }

                $image->move($imagePath, $filename);
                Image::configure(array('driver' => 'imagick'));
                Image::make($imagePath . '/' . $filename)->resize(60, 60)->save($imagePath . '/60_x_60/' . $filename);
                $user->update(['image' => $filename]);
            }

            if (Auth::User()->id == $user->id) {

                if (!$request->is('admin/*')) {
                    return redirect()->route('users.dashboard')->with('message-success', 'Profile is updated.');
                } else {
                    return redirect()->route('users.adminProfile')->with('message-success', 'Profile is updated.');
                }

            } else {
                return redirect()->route('users.adminIndex')->with('message-success', 'User Updated.');
            }

        } else {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
    }

    /**
     * Signup a newly created user signup.
     *
     * @param  Request  $request
     * @return Response
     */
    public function signup(Request $request,$id=null)
    {
        $method = $request->method();
        if ($request->isMethod('post')) {

            $rules = [
                'name'                  => 'required',
                'email'                 => 'required|email|unique:users',
                'password'              => 'required|min:3|confirmed',
                'password_confirmation' => 'required|min:3',

            ];

            //$this->validate($request,$rules,$messages);
            $input = $request->all();

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $request->session()->flash('signupError', 1);
                return redirect()->route('posts.welcome')->withInput()->withErrors($validator->errors());
            } else {
                $input['password'] = bcrypt($input['password']);
                $input['role_id']  = 3;
                $user              = User::create($input);

                $key                  = \Config::get('app.key');
                $password_token       = hash_hmac('sha256', str_random(40), $key);
                $user->password_token = $password_token;
                $user->save();
                \Mail::send('emails.activate', ['token' => $password_token, 'name' => $user->name, 'email' => $user->email], function ($message) use ($user) {
                    $message->to($user->email, $user->name)
                        ->subject('Activate your Notify account');
                });
                $request->session()->flash('signupError', 1);
                return redirect()->route('posts.welcome')->with('message', 'You are successfully registered and activation link is mailed to you!');
            }
        }
        $request->session()->flash('signupError',$id);
        //$request->session()->flash('token', $id);
        return redirect()->route('posts.welcome');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::validate($credentials)) {
            $user = Auth::getLastAttempted();
            if ($user->is_active) {
                Auth::login($user, $request->has('remember'));
                return redirect()->intended($this->redirectPath());
            } else {
                return redirect($this->loginPath()) // Change this to redirect elsewhere
                ->withInput($request->only('email', 'remember'))
                    ->withErrors([
                        'active' => 'You are not authorized to login.',
                    ]);
            }
        }

        if (Auth::attempt($credentials, $request->has('remember'))) {
            Session::set('loginError', 1);
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername()
    {
        Session::set('loginError', 1);
        return property_exists($this, 'username') ? $this->username : 'email';
    }

    protected function isUsingThrottlesLoginsTrait()
    {
        return in_array(
            ThrottlesLogins::class, class_uses_recursive(get_class($this))
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {

        return $request->only($this->loginUsername(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleUserWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }
        Session::set('loginError', 1);
        return redirect()->intended($this->redirectPath());
    }

    public function loginPath()
    {
        return property_exists($this, 'loginPath') ? $this->loginPath : '';
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {

        Session::set('loginError', 1);
        return Lang::has('auth.failed')
        ? Lang::get('auth.failed')
        : 'These credentials do not match our records.';

    }

    public function redirectPath()
    {

        Session::forget('loginError');
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : 'users/dashboard';
    }
    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function signout()
    {
        Auth::logout();

        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {

        $rules = [
            'email' => 'required|email',
        ];

        $input     = $request->all();
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $request->session()->flash('passwordError', 1);
            return redirect()->route('posts.welcome')->withInput()->withErrors($validator->errors());
        }
        Session::forget('passwordError');
        $email = $input['email'];

        $user = DB::table('users')->where('email', $email)->first();
        if (isset($user)) {
            $role = $user->role_id;
            View::composer('emails.password', function ($view) use ($role) {
                $view->with(['role' => $role]);
            });
        }
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        Session::forget('passwordError');

        switch ($response) {
            case Password::RESET_LINK_SENT:
                Session::set('passwordError', 1);
                return redirect()->route('posts.welcome')->with('message', 'Password reset url is mailed to you!');
            case Password::INVALID_USER:
                Session::set('passwordError', 1);
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Get the e-mail subject line to be used for the reset link email.
     *
     * @return string
     */
    protected function getEmailSubject()
    {
        return isset($this->subject) ? $this->subject : 'Your Password Reset Link';
    }

    public function profile(Request $request, $id)
    {
        $pageTitle = 'Dashboard';

        // Set actvie navigation tab in view file.
        $activeTab = ($request->has('tab')) ? $request->input('tab') : '';

        $articles = Post::with('category')
            ->orderBy('created_at', 'desc')
            ->where('type_id', 1)
            ->where('user_id', $id)
            ->paginate(10, ['*'], 'page_article');

        $images = Post::with('category')
            ->orderBy('created_at', 'desc')
            ->where('type_id', 2)
            ->where('user_id', $id)
            ->paginate(10, ['*'], 'page_image');

        $imageUrl       = Config::get('constants.USER_IMAGE_URL');
        $user           = User::find($id);
        $locations      = [];
        $user_followers = UserFollower::where('follower_id', $user->id)->count();
        $user_following = UserFollower::where('user_id', $user->id)->count();
        $user_points    = UserPoint::with('point')->where('user_id', $user->id)->get();
        $user_points    = DB::table('user_points')
            ->leftjoin('points', 'user_points.point_id', '=', 'points.id')
            ->select('user_points.user_id')
            ->where('user_id', $user->id)
            ->sum('points.points');
        if (Auth::check()) {
            $follow = UserFollower::where('follower_id', $user->id)
                ->where('user_id', Auth::User()->id)->count();
        }

        list($user['year'], $user['month'], $user['day']) = explode('-', $user['dob']);

        if ($user->location) {
            $locations = explode('//', $user->location);
        }

        return view('users.profile')->with(compact(['pageTitle', 'articles', 'activeTab', 'user', 'imageUrl', 'locations', 'user_followers', 'user_following', 'user_points', 'images', 'follow']));

    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return \Illuminate\Http\Response
     */
    public function getReset($token = null)
    {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        $pageTitle = 'Reset Password';
        return view('users.reset')->with(compact('pageTitle', 'token'));
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        switch ($response) {
            case Password::PASSWORD_RESET:
                return redirect($this->redirectPath());

            default:
                return redirect()->back()
                    ->withInput($request->only('email'))
                    ->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();

        Auth::login($user);
    }

    public function accountIsActive($token)
    {
        $user                 = User::where('password_token', '=', $token)->first();
        $user->is_active      = 1;
        $user->password_token = '';
        if ($user->save()) {
            \Auth::login($user);
        }
        return redirect()->route('posts.welcome');
    }

    public function inviteFriends(Request $request)
    {
        $rules = [
            'email' => 'required|email',
        ];
        $input = $request->all();
        $validator = Validator::make($input, $rules);
        if (!$validator->fails()) {
            $name  = Auth::User()->name;
            $email = explode(',', $input['email']);
            $getId = Auth::User()->id;
            $id    = Crypt::encrypt($getId);
            
            \Mail::send('emails.invitefriends', ['email' => $email, 'name' => $name, 'id' => $id], function ($message) use ($email) {
                $message->bcc($email)
                    ->subject('Travel Portal');
            });
            return response()->json(['message' => 'Your Invitation has been sent.']);
        } else {
            return response()->json(['errors' => 'Invalid Email address']);
        }

    }
}
