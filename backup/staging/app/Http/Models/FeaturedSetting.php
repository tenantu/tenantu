<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturedSetting extends Model
{
    protected $table = 'featured_settings';
    protected $fillable = ['duration','price','status'];
}
