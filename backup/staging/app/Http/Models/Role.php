<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    // DEFINE RELATIONSHIPS
    public function users()
    {
        return $this->hasMany('App\Http\Models\User');
    }

}