<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class MessageThread extends Model
{
     protected $table = 'message_threads';
     
     protected $fillable = ['thread_id','property_id','landlord_id','tenant_id','duration','created_at','updated_at','status'];
     
    public function threads()
    {
        return $this->hasMany('App\Http\Models\Thread');
    }
    
    public function messages()
    {
        return $this->hasMany('App\Http\Models\Message');
    }
    
    public function property()
    {
        return $this->belongsTo('App\Http\Models\Property','property_id');
    }
    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord','landlord_id');
    }
    public function tenant()
    {
        return $this->belongsTo('App\Http\Models\Tenant','tenant_id');
    }
    
}

