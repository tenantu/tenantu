<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyExpressInterest extends Model
{
     protected $table 		= 'property_express_interest';
     protected $fillable 	= ['property_id','tenant_id','landlord_id'];
}
