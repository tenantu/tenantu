<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table = 'enquiries';
    
    protected $fillable = ['email'];
}
