<?php

namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Landlord extends Model implements SluggableInterface, AuthenticatableContract, CanResetPasswordContract
{
	use Authenticatable, CanResetPassword, SluggableTrait;
	
	protected $table = 'landlords';
	
    protected $fillable = ['fb_id','firstname','lastname','school_id', 'slug', 'email', 'password', 'image', 'gender', 'description', 'dob', 'location',
       'phone','about','website','state','address','language','city', 'is_active','is_fb_user','is_normal_user'];
        
    protected $hidden = ['password', 'remember_token'];
    
    protected $sluggable = [
        'build_from' => 'fullname',
        'save_to' => 'slug',
    ];
    
    public function getFullnameAttribute() {
        return $this->firstname . ' ' . $this->lastname;
    }
    
    public function getFullName(){
        
        return ucfirst($this->firstname)." ".ucfirst($this->lastname);
    }
    public function getCreatedAt()
    {
		$landlordCreated = date('d M Y',strtotime($this->created_at));
		return $landlordCreated;
	}
}
