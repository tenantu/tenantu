<?php

namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Illuminate\Database\Eloquent\Model;

class Property extends Model implements SluggableInterface
{
    use SluggableTrait;
	
	protected $table = 'properties';
	
    protected $fillable = ['property_name','landlord_id','school_id', 'description', 'bedroom_no','bathroom_no','location', 'rent_price', 'expireon', 'status', 'communication_medium', 'slug', 'search_field', 'distance_from_school','latitude','longitude','featured','availability_status','amenities'];

    protected $sluggable = [
        'build_from' => 'property_name',
        'save_to' => 'slug',
    ];

    // DEFINE RELATIONSHIPS
    public function landlord()
    {
        return $this->belongsTo('App\Http\Models\Landlord');
    }
    public function school()
    {
        return $this->belongsTo('App\Http\Models\School');
    }
    public function message_threads()
    {
        return $this->hasMany('App\Http\Models\MessageThread');
    }
    public function property_comments()
    {
        return $this->hasMany('App\Http\Models\PropertyComment');
    }
    public function property_docs()
    {
        return $this->hasMany('App\Http\Models\PropertyDoc');
    }
    public function property_images()
    {
        return $this->hasMany('App\Http\Models\PropertyImage', 'property_id');
    }
    public function property_amenities()
    {
        return $this->hasMany('App\Http\Models\PropertyAmenity', 'property_id');
    }
    public function property_ratings()
    {
        return $this->hasMany('App\Http\Models\PropertyRating');
    }
        

}
