<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BannerPurchase extends Model
{
    protected $table = 'banner_purchases';
    protected $fillable = ['bannerId','name','phone','address','banner_name','banner_price','banner_url','purchase_date','status','payment_status','image','amount_paid','txn_id'];
}
