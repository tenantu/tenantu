<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;


class Tenant extends Model implements AuthenticatableContract, CanResetPasswordContract,SluggableInterface
{
	use Authenticatable, CanResetPassword,SluggableTrait;
	
	protected $table = 'tenants';
	
    protected $fillable = ['fb_id','firstname','lastname','school_id', 'slug', 'email', 'password', 'image', 'gender', 'description', 'dob', 'location',
       'phone','about','website','state','address','language','city', 'is_active','is_fb_user','is_normal_user'];
        
    protected $hidden = ['password', 'remember_token'];
    
    protected $sluggable = [
        'build_from' => 'fullname',
        'save_to' => 'slug',
    ];
    
    public function getFullnameAttribute() {
        return $this->firstname . ' ' . $this->lastname;
    }
    
    public function getFullName(){
        
        return ucfirst($this->firstname)." ".ucfirst($this->lastname);
    }
    
    public function getCreatedAt()
    {
		$tenantCreated = date('d M Y',strtotime($this->created_at));
		return $tenantCreated;
	}
    
    public function school()
    {
        return $this->belongsTo('App\Http\Models\School');
    }
    
    
}
