<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    protected $table = 'threads';
	
    protected $fillable = ['thread_name','status','flag','image','order_status'];
    
    /*public function message_threads()
    {
        return $this->belongsTo('App\Http\Models\MessageThread');
    }*/
}
