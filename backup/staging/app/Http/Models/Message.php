<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';
    
    protected $fillable = ['message_thread_id','message','landlord_id','tenant_id','sender','read_flag','created_at','updated_at'];
    
    
}
