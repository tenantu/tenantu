<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturedPurchase extends Model
{
    protected $table = 'featured_purchase';
    protected $fillable = ['landlord_id','property_id','duration_id','duration_name','actual_amount','txn_id','amount_paid','payment_status','status','purchase_date_from_paypal'];
}
