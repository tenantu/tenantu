<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class landlordTenantDoc extends Model
{
    protected $table = 'landlord_tenant_docs';
	
    protected $fillable = ['landlord_tenant_id','doc_title','doc_description', 'file_name'];
    public function landlord_tenant()
    {
        return $this->belongsTo('App\Http\Models\LandlordTenant');
    }
}
