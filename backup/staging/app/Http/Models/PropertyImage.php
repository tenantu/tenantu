<?php

namespace App\Http\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cviebrock\EloquentSluggable\SluggableInterface;

use Illuminate\Database\Eloquent\Model;

class PropertyImage extends Model
{
    use Authenticatable, CanResetPassword;
	
	protected $table = 'property_images';
	
    protected $fillable = ['property_id','image_name','status','featured'];

    // DEFINE RELATIONSHIPS
    public function properties()
    {
        return $this->belongsTo('App\Http\Models\Property');
    }
}
