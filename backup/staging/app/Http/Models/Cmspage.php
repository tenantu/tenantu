<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Cmspage extends Model implements SluggableInterface
{
	use SluggableTrait;
	
    protected $table = 'pages';
    
    protected $fillable = ['title','content','pageslug','status','footerpage'];
    
    protected $sluggable = [
        'build_from' => 'title',
        'save_to' => 'pageslug',
    ];
}
