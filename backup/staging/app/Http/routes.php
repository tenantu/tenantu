<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// Patterns
Route::pattern('id', '\d+');
Route::pattern('hash', '[a-z0-9]+');
Route::pattern('hex', '[a-f0-9]+');
Route::pattern('uuid', '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
Route::pattern('base', '[a-zA-Z0-9]+');
Route::pattern('slug', '[a-z0-9-]+');
Route::pattern('username', '[a-z0-9_-]{3,16}');


######################################## BACK-END ROUTES STARTS ########################################
Route::get('admin', ['as' => 'admins.getLogin', 'uses' => 'AdminsController@getLogin']);
Route::get('admin/login', ['as' => 'admins.getLogin', 'uses' => 'AdminsController@getLogin']);
Route::post('admin/login', ['as' => 'admins.postLogin', 'uses' => 'AdminsController@postLogin']);
Route::get('admin/dashboard', ['as' => 'admins.dashboard', 'uses' => 'AdminsController@dashboard','middleware'=>'auth']);
Route::get('admin/logout', ['as' => 'admins.logout', 'uses' => 'AdminsController@logout']);
//Route::get('admin/profile', ['as' => 'admins.profile', 'uses' => 'AdminsController@profile']);
Route::get('admin/changePassword', ['as' => 'admins.changePassword', 'uses' => 'AdminsController@getChangePassword']);
Route::get('admin/tenants', ['as' => 'admins.tenants', 'uses' => 'TenantsController@index']);
Route::get('admin/tenants/index', ['as' => 'tenants.index', 'uses' => 'TenantsController@index']);
Route::get('admin/tenants/create', ['as' => 'tenants.create', 'uses' => 'TenantsController@create']);
Route::post('admin/tenants/create', ['as' => 'tenants.store', 'uses' => 'TenantsController@store']);
Route::get('admin/tenants/bulkdestroy', ['as' => 'tenants.bulkDestroy', 'uses' => 'TenantsController@bulkdestroy']);
Route::delete('admin/tenants/destroy/{id}', ['as' => 'tenants.destroy', 'uses' => 'TenantsController@destroy']);
Route::get('admin/tenants/edit/{id}', ['as' => 'tenants.edit', 'uses' => 'TenantsController@edit']);
Route::post('admin/tenants/update/{id}', ['as' => 'tenants.update', 'uses' => 'TenantsController@update']);
Route::get('admin/tenants/changeStatus/{id}', ['as' => 'tenants.changeStatus', 'uses' => 'TenantsController@changeStatus']);

Route::get('admin/profile', ['as' => 'admin.profile', 'uses' => 'AdminsController@profile']);
Route::post('admin/editprofile', ['as' => 'admins.adminEditProfile', 'uses' => 'AdminsController@editprofile']);

Route::get('admin/landlords', ['as' => 'admins.landlords', 'uses' => 'LandlordsController@index']);
Route::get('admin/landlords/index', ['as' => 'landlords.index', 'uses' => 'LandlordsController@index']);
Route::get('admin/landlords/create', ['as' => 'landlords.create', 'uses' => 'LandlordsController@create']);
Route::post('admin/landlords/create', ['as' => 'landlords.store', 'uses' => 'LandlordsController@store']);
Route::get('admin/landlords/bulkdestroy', ['as' => 'landlords.bulkDestroy', 'uses' => 'LandlordsController@bulkdestroy']);
Route::delete('admin/landlords/destroy/{id}', ['as' => 'landlords.destroy', 'uses' => 'LandlordsController@destroy']);
Route::get('admin/landlords/edit/{id}', ['as' => 'landlords.edit', 'uses' => 'LandlordsController@edit']);
Route::post('admin/landlords/update/{id}', ['as' => 'landlords.update', 'uses' => 'LandlordsController@update']);
Route::get('admin/landlords/changeStatus/{id}', ['as' => 'landlords.changeStatus', 'uses' => 'LandlordsController@changeStatus']);

Route::get('admin/schools', ['as' => 'admins.schools', 'uses' => 'SchoolsController@index']);
Route::get('admin/schools/index', ['as' => 'schools.index', 'uses' => 'SchoolsController@index']);
Route::get('admin/schools/create', ['as' => 'schools.create', 'uses' => 'SchoolsController@create']);
Route::post('admin/schools/create', ['as' => 'schools.store', 'uses' => 'SchoolsController@store']);
Route::delete('admin/schools/destroy/{id}', ['as' => 'schools.destroy', 'uses' => 'SchoolsController@destroy']);
Route::get('admin/schools/edit/{id}', ['as' => 'schools.edit', 'uses' => 'SchoolsController@edit']);
Route::post('admin/schools/update/{id}', ['as' => 'schools.update', 'uses' => 'SchoolsController@update']);
Route::get('admin/schools/changeStatus/{id}', ['as' => 'schools.changeStatus', 'uses' => 'SchoolsController@changeStatus']);

Route::get('admin/properties', ['as' => 'admins.properties', 'uses' => 'PropertiesController@index']);
Route::get('admin/properties/index', ['as' => 'properties.index', 'uses' => 'PropertiesController@index']);
Route::get('admin/properties/edit/{id}', ['as' => 'properties.edit', 'uses' => 'PropertiesController@edit']);
Route::post('admin/properties/update/{id}', ['as' => 'properties.update', 'uses' => 'PropertiesController@update']);
Route::get('admin/properties/changeStatus/{id}', ['as' => 'properties.changeStatus', 'uses' => 'PropertiesController@changeStatus']);
Route::get('admin/properties/delete/{id}', ['as' => 'properties.delete', 'uses' => 'PropertiesController@deleteProperty']);

Route::get('admin/propertyreview', ['as' => 'admins.propertyreview', 'uses' => 'PropertiesController@reviewIndex']);
Route::get('admin/propertyreview/index', ['as' => 'propertyreview.index', 'uses' => 'PropertiesController@reviewIndex']);
Route::get('admin/propertyreview/edit/{id}', ['as' => 'propertyreview.edit', 'uses' => 'PropertiesController@reviewEdit']);
Route::post('admin/propertyreview/update/{id}', ['as' => 'propertyreview.update', 'uses' => 'PropertiesController@reviewUpdate']);
Route::get('admin/propertyreview/changeStatus/{id}', ['as' => 'propertyreview.changeStatus', 'uses' => 'PropertiesController@changeReviewStatus']);

Route::get('admin/cms', ['as' => 'admins.cms', 'uses' => 'CmspagesController@index']);
Route::get('admin/cms/index', ['as' => 'pages.index', 'uses' => 'CmspagesController@index']);
Route::get('admin/cms/create', ['as' => 'pages.create', 'uses' => 'CmspagesController@create']);
Route::post('admin/cms/create', ['as' => 'pages.store', 'uses' => 'CmspagesController@store']);
Route::get('admin/cms/edit/{id}', ['as' => 'pages.edit', 'uses' => 'CmspagesController@edit']);
Route::post('admin/cms/update/{id}', ['as' => 'pages.update', 'uses' => 'CmspagesController@update']);
Route::get('admin/cms/changeStatus/{id}', ['as' => 'pages.changeStatus', 'uses' => 'CmspagesController@changeStatus']);
Route::delete('admin/cms/destroy/{id}', ['as' => 'pages.destroy', 'uses' => 'CmspagesController@destroy']);

Route::get('admin/faq', ['as' => 'admins.faq', 'uses' => 'CmspagesController@getFaq']);
Route::get('admin/faq/index', ['as' => 'faq.index', 'uses' => 'CmspagesController@getFaq']);
Route::get('admin/faq/create', ['as' => 'faq.create', 'uses' => 'CmspagesController@createFaq']);
Route::post('admin/faq/create', ['as' => 'faq.store', 'uses' => 'CmspagesController@storeFaq']);
Route::get('admin/faq/edit/{id}', ['as' => 'faq.edit', 'uses' => 'CmspagesController@editFaq']);
Route::post('admin/faq/update/{id}', ['as' => 'faq.update', 'uses' => 'CmspagesController@updateFaq']);
Route::get('admin/faq/changeStatus/{id}', ['as' => 'faq.changeStatus', 'uses' => 'CmspagesController@changeFaqStatus']);

Route::get('admin/feedback', ['as' => 'admins.feedback', 'uses' => 'FeedbackController@index']);
Route::get('admin/feedback/index', ['as' => 'feedback.index', 'uses' => 'FeedbackController@index']);
Route::get('admin/feedback/create', ['as' => 'feedback.create', 'uses' => 'FeedbackController@create']);
Route::post('admin/feedback/create', ['as' => 'feedback.store', 'uses' => 'FeedbackController@store']);
Route::get('admin/feedback/edit/{id}', ['as' => 'feedback.edit', 'uses' => 'FeedbackController@edit']);
Route::post('admin/feedback/update/{id}', ['as' => 'feedback.update', 'uses' => 'FeedbackController@update']);
Route::delete('admin/feedback/destroy/{id}', ['as' => 'feedback.destroy', 'uses' => 'FeedbackController@destroy']);
Route::get('admin/feedback/changeStatus/{id}', ['as' => 'feedback.changeStatus', 'uses' => 'FeedbackController@changeStatus']);

Route::get('admin/contact-users', ['as' => 'admins.contactUsers', 'uses' => 'ContactsController@index']);
Route::get('admin/contact-users/view/{id}', ['as' => 'contacts.Edit', 'uses' => 'ContactsController@show']);
Route::post('admin/contact-users/update/{id}', ['as' => 'contacts.update', 'uses' => 'ContactsController@update']);

Route::get('admin/getlandlord', ['as' => 'admins.signinlandlord', 'uses' => 'IndexController@getAllActiveLandlords']);
Route::post('admin/landlords/{landlordId}', ['as' => 'landlords.loginWithId', 'uses' => 'IndexController@landlordLoginWithId']);

Route::get('admin/landlord-registration', ['as' => 'admin.landlordRegister', 'uses' => 'AdminsController@registerAsLandlord']);
Route::post('admin/landlord-registration', ['as' => 'admins.createLandlord', 'uses' => 'AdminsController@postRegisterAsLandlord']);

Route::get('admin/sociallinks', ['as' => 'admins.socialmedia', 'uses' => 'IndexController@getSocialMediaLinks']);
Route::post('admin/sociallinks', ['as' => 'sociallinks.update', 'uses' => 'IndexController@postSocialMediaLinks']);

Route::get('admin/bannersettings', ['as' => 'admins.bannerSettings', 'uses' => 'AdminsController@getBannerSettings']);
Route::get('admin/bannersettingsedit/{id}', ['as' => 'admins.bannerEdit', 'uses' => 'AdminsController@editBannerSettings']);
Route::post('admin/bannersettingupdate/{id}', ['as' => 'admins.updateBannerSettings', 'uses' => 'AdminsController@updateBannerSettings']);
Route::get('admin/bannerchangestatus/{id}', ['as' => 'admins.changeBannerSettingStatus', 'uses' => 'AdminsController@changeBannerSettingStatus']);

Route::get('admin/featuredpropertysettings', ['as' => 'admins.featuredPropertySettings', 'uses' => 'AdminsController@getFeaturedPropertySettings']);
Route::get('admin/featuredpropertysettingedit/{id}', ['as' => 'admins.featuredEdit', 'uses' => 'AdminsController@editFeaturedPropertySettings']);
Route::post('admin/featuredsettingupdate/{id}', ['as' => 'admins.updateFeaturedSettings', 'uses' => 'AdminsController@updateFeaturedPropertySettings']);
Route::get('admin/featuredchangestatus/{id}', ['as' => 'admins.changeFeaturedSettingStatus', 'uses' => 'AdminsController@changeFeaturedSettingStatus']);

Route::get('admin/featuredpurchases', ['as' => 'admins.featuredPurchases', 'uses' => 'AdminsController@getFeaturedPurchases']);
Route::get('admin/featuredpurchases/{id}', ['as' => 'admins.featuredPurchaseDetail', 'uses' => 'AdminsController@getFeaturedPurchasesDetail']);

Route::get('admin/bannerpurchases', ['as' => 'admins.bannerPurchases', 'uses' => 'AdminsController@getBannerPurchases']);
Route::get('admin/bannerchangestatus/{bannerId}', ['as' => 'admins.changeBannerStatus', 'uses' => 'AdminsController@changeBannerStatus']);
Route::get('admin/bannerdetails/{bannerId}', ['as' => 'admins.bannerPurchaseDetail', 'uses' => 'AdminsController@getBannerPurchaseDetails']);
Route::get('admin/activatebannerdetails/{bannerId}', ['as' => 'admins.activateBannerPurchase', 'uses' => 'AdminsController@activateBannerPurchase']);
Route::get('admin/deactivatebannerdetails/{bannerId}', ['as' => 'admins.deActivateBannerPurchase', 'uses' => 'AdminsController@deActivateBannerPurchase']);

Route::delete('admin/bannerpuchases/destroy/{bannerId}', ['as' => 'admins.bannerPurchaseDestroy', 'uses' => 'AdminsController@bannerPurchaseDestroy']);


Route::get('admin/enquiries', ['as' => 'admins.enquiries', 'uses' => 'IndexController@getEnquiries']);
Route::get('admin/payments/{dateString?}/{lid?}/{id?}', ['as' => 'admins.getAllPayment', 'uses' => 'PaymentsController@getAllPayment']);
Route::post('admin/payment/getTenantFromLandlord', ['as' => 'payment.getTenantFromLandlord', 'uses' => 'PaymentsController@getTenantFromLandlord']);
Route::get('admin/amenities', ['as' => 'admins.amenities', 'uses' => 'AdminsController@getAmenities']);
Route::post('admin/amenities', ['as' => 'admins.postamenities', 'uses' => 'AdminsController@postAmenities']);
Route::get('admin/amenities/create', ['as' => 'admins.createAmenity', 'uses' => 'AdminsController@createAmenity']);
Route::post('admin/amenities/create', ['as' => 'admins.storeAmenity', 'uses' => 'AdminsController@storeAmenity']);
Route::get('admin/amenities/edit/{id}', ['as' => 'admins.editAmenity', 'uses' => 'AdminsController@editAmenity']);
Route::post('admin/amenities/update/{id}', ['as' => 'admins.updateAmenity', 'uses' => 'AdminsController@updateAmenity']);
Route::get('admin/amenities/changeStatus/{id}', ['as' => 'admins.changeStatus', 'uses' => 'AdminsController@changeStatusAmenity']);
Route::delete('admin/amenities/destroy/{id}', ['as' => 'admins.destroy', 'uses' => 'AdminsController@destroyAmenity']);
Route::get('admin/trend', ['as' => 'admins.trend', 'uses' => 'AdminsController@getTrend']);
######################################## BACK-END ROUTES ENDS #########################################

########################################frontend homepage route########################################
Route::get('/', ['as' => 'index.index', 'uses' => 'IndexController@index']);
Route::get('/ajaxPostSchool', ['as' => 'index.indexAjax', 'uses' => 'IndexController@ajaxPostSchool']);

Route::get('calculatelocation', ['as' => 'index.ajaxLocate', 'uses' => 'SchoolsController@ajaxSchoolLatitudeLongitude']);

Route::post('/contactus', ['as' => 'index.postContactUs', 'uses' => 'IndexController@postContactUs']);
Route::post('/enquiry', ['as' => 'index.postEnquiry', 'uses' => 'IndexController@postEnquiry']);
########################################frontend homepage route ends #################################

########################################frontend home page search page ########################################
Route::get('/search', ['as' => 'index.searchResult', 'uses' => 'IndexController@index']);
Route::any('/search', ['as' => 'index.searchResult', 'uses' => 'IndexController@postSearch']);
########################################frontend search page ends ###################################

########################################common login routes starts#####################################
Route::get('/login', ['as' => 'index.login', 'uses' => 'IndexController@login','middleware'=>'checkauth']);
Route::post('/login', ['as' => 'index.postLogin', 'uses' => 'IndexController@postLogin']);
Route::post('/signup', ['as' => 'index.postSignup', 'uses' => 'IndexController@postSignup']);
Route::get('/logout', ['as' => 'index.logout', 'uses' => 'IndexController@logout']);

Route::get('/contactus', ['as' => 'contactUs', 'uses' => 'IndexController@getContactUs']);
########################################common login routes ends#######################################

######################################## facebook login routes ########################################
Route::get('auth/facebook/{id}', ['as' => 'index.getFacebookLogin', 'uses' => 'IndexController@redirectToProvider']);
Route::get('auth/facebook/callback', ['as' => 'index.postFacebookLogin', 'uses' => 'IndexController@handleProviderCallback']);

Route::get('/facebookauth/{id}', ['as' => 'facebook.auth', 'uses' => 'IndexController@facebookAuth']);
Route::post('/facebooksignup', ['as' => 'facebook.signup', 'uses' => 'IndexController@facebookSignUp']);
######################################## facebook login routes ends ###################################


########################################account activation route starts#################################

Route::get('index/accountIsActive/{token}/{userType}', ['as' => 'index.accountActivate', 'uses' => 'IndexController@accountIsActive']);
########################################account activation route ends ##################################
######################################## new forgot password starts here ###############################
Route::get('/forgotpassword', ['as' => 'index.forgotPassword', 'uses' => 'IndexController@getForgotPassword']);
Route::post('/forgotpassword', ['as' => 'index.postForgotPassword','uses' => 'IndexController@postForgotPassword']);
Route::get('index/resetpassword/{token}/{userType}', ['as' => 'index.getResetPassword', 'uses' => 'IndexController@getResetPassword']);
Route::post('index/resetpassword/{token}/{userType}', ['as' => 'index.postResetPassword', 'uses' => 'IndexController@postResetPassword']);
######################################## new forgot password ends here #################################

Route::get('tenants/login', ['as' => 'tenants.getLogin', 'uses' => 'TenantsController@getLogin']);
Route::post('tenants/login', ['as' => 'tenants.postLogin', 'uses' => 'TenantsController@postLogin']);
Route::get('tenants/logout', ['as' => 'tenants.logout', 'uses' => 'TenantsController@logout']);
Route::get('tenants/dashboard', ['as' => 'tenants.dashboard', 'uses' => 'TenantsController@dashboard','middleware'=>'auth']);
Route::get('tenants/profile', ['as' => 'tenants.profile', 'uses' => 'TenantsController@getProfile','middleware'=>'auth']);
Route::get('tenants/editprofile', ['as' => 'tenants.editprofile', 'uses' => 'TenantsController@getEditProfile','middleware'=>'auth']);
Route::post('tenants/editprofile', ['as' => 'tenants.postEditProfile', 'uses' => 'TenantsController@postEditProfile','middleware'=>'auth']);
Route::post('tenants/editprofileimage', ['as' => 'tenants.postEditProfileImage', 'uses' => 'TenantsController@postEditProfileImage','middleware'=>'auth']);
Route::get('tenants/contact', ['as' => 'tenants.getContact', 'uses' => 'TenantsController@getMyContact','middleware'=>'auth']);
Route::get('tenants/message/{id}/{propertyId?}', ['as' => 'tenants.getMessage', 'uses' => 'TenantsController@getMessage','middleware'=>'auth']);
Route::post('tenants/message/', ['as' => 'tenants.postMessage', 'uses' => 'TenantsController@postMessage','middleware'=>'auth']);
Route::get('tenants/messagelist/{messageThreadId}', ['as' => 'tenants.getMessageList', 'uses' => 'TenantsController@getMessageList','middleware'=>'auth']);
Route::post('tenants/newmessage/{messageThreadId}', ['as' => 'tenants.composeMessageList', 'uses' => 'TenantsController@composeMessageList','middleware'=>'auth']);
Route::get('tenants/landlord/{slug}', ['as' => 'tenants.getLandlordProfileFromTenant', 'uses' => 'TenantsController@getLandlordProfileFromTenant','middleware'=>'auth']);
Route::get('tenants/recent-properties', ['as' => 'tenants.getRecentProperties', 'uses' => 'TenantsController@getRecentProperties','middleware'=>'auth']);
Route::get('tenants/mylandlords', ['as' => 'tenants.getMyLandlords', 'uses' => 'TenantsController@getMyLandlords','middleware'=>'auth']);
Route::get('tenants/agreementdetails/{landlordTenantId}', ['as' => 'tenants.getAgreementDetails', 'uses' => 'TenantsController@getAgreementDetails','middleware'=>'auth']);
//Route::get('tenants/download/{filename}', ['as' => 'tenants.getDownload', 'uses' => 'TenantsController@getDownloadDocuments','middleware'=>'auth']);


Route::get('property/{slug}', ['as' => 'properties.getPropertyDetail', 'uses' => 'PropertiesController@getPropertyDetail']);
Route::post('property/enquiry', ['as' => 'properties.postEnquiry', 'uses' => 'PropertiesController@postEnquiry']);
Route::post('property/review', ['as' => 'properties.postReview', 'uses' => 'PropertiesController@postReview']);
Route::get('property/interest/{propertyId}', ['as' => 'properties.interest', 'uses' => 'PropertiesController@interest']);
Route::get('property-reviews/{slug}', ['as' => 'properties.reviews', 'uses' => 'PropertiesController@getAllReviewsOfProperty']);
Route::any('properties/review/{slug?}', ['as' => 'properties.review', 'uses' => 'PropertiesController@getPropertyReviews']);

Route::get('tenants/mysearch', ['as' => 'search.getMySearchDetails', 'uses' => 'MysearchController@getMySearchDetails','middleware'=>'auth']);
Route::post('tenants/mysearch', ['as' => 'search.postMySearchDetails', 'uses' => 'MysearchController@postMySearchDetails','middleware'=>'auth']);

Route::get('tenants/mypayments/{dateString?}', ['as' => 'payment.getMyPayment', 'uses' => 'PaymentsController@getMyPaymentDetails','middleware'=>'auth']);


##################################### 404 page #####################################
Route::get('/404',  'IndexController@getPageNotFound');
##################################### 404 page ends ################################
#######################################tenants changePassword starts##################################
Route::get('tenants/changepassword', ['as' => 'tenants.getChangePassword', 'uses' => 'TenantsController@getChangePassword']);
Route::post('tenants/changepassword', ['as' => 'tenants.postChangePassword', 'uses' => 'TenantsController@postChangePassword']);

#######################################tenants changePassword end ####################################

#######################################landlords changePassword starts##################################
Route::get('landlords/changepassword', ['as' => 'landlords.changepassword', 'uses' => 'LandlordsController@getChangePassword']);
Route::post('landlords/changepassword', ['as' => 'landlords.postChangepassword', 'uses' => 'LandlordsController@postChangePassword']);
Route::get('landlords/logout', ['as' => 'landlords.logout', 'uses' => 'LandlordsController@logout']);
Route::get('landlords/profile', ['as' => 'landlords.profile', 'uses' => 'LandlordsController@profile','middleware'=>'auth']);
Route::get('landlords/editprofile', ['as' => 'landlords.editprofile', 'uses' => 'LandlordsController@editprofile','middleware'=>'auth']);
Route::post('landlords/editprofile', ['as' => 'landlords.posteditprofile', 'uses' => 'LandlordsController@posteditprofile','middleware'=>'auth']);
Route::post('landlords/editprofileimage', ['as' => 'landlords.posteditprofileimage', 'uses' => 'LandlordsController@posteditprofileimage','middleware'=>'auth']);
Route::get('landlords/property/add', ['as' => 'property.add', 'uses' => 'LandlordsController@addProperty','middleware'=>'auth']);
Route::post('landlords/property/add', ['as' => 'property.postadd', 'uses' => 'LandlordsController@postProperty','middleware'=>'auth']);
Route::any('landlords/property/upload/{imagename?}', ['as' => 'property.upload', 'uses' => 'LandlordsController@upload','middleware'=>'auth']);
Route::get('landlords/property/add', ['as' => 'property.add', 'uses' => 'LandlordsController@addProperty','middleware'=>'auth']);
Route::get('landlords/property/edit/{id}', ['as' => 'property.edit', 'uses' => 'PropertiesController@editProperty','middleware'=>'auth']);
Route::post('landlords/property/edit/{id}', ['as' => 'property.postedit', 'uses' => 'PropertiesController@postEditProperty','middleware'=>'auth']);
Route::get('landlords/property/delete/{id}', ['as' => 'property.deleteProperty', 'uses' => 'PropertiesController@destroy','middleware'=>'auth']);
Route::get('landlords/contact', ['as' => 'landlords.getContact', 'uses' => 'LandlordsController@getMyContact','middleware'=>'auth']);
Route::get('landlords/message/{id}/{propertyId?}', ['as' => 'landlords.newmessage', 'uses' => 'LandlordsController@newmessage','middleware'=>'auth']);
Route::post('landlords/message/', ['as' => 'landlords.postMessage', 'uses' => 'LandlordsController@postMessage','middleware'=>'auth']);
Route::get('landlords/messagelist/{messageThreadId}', ['as' => 'landlords.getMessageList', 'uses' => 'LandlordsController@getMessageList','middleware'=>'auth']);
Route::post('landlords/newmessage/{messageThreadId}', ['as' => 'landlords.composeMessageList', 'uses' => 'LandlordsController@composeMessageList','middleware'=>'auth']);
Route::get('tenants/add', ['as' => 'landlords.addtenant', 'uses' => 'LandlordsController@addTenant','middleware'=>'auth']);
Route::post('tenants/add', ['as' => 'landlords.posttenant', 'uses' => 'LandlordsController@postTenant','middleware'=>'auth']);
Route::get('tenants/view/{id}', ['as' => 'landlords.viewtenant', 'uses' => 'LandlordsController@viewTenant','middleware'=>'auth']);
Route::get('tenants/edit/{id}', ['as' => 'landlords.edittenant', 'uses' => 'LandlordsController@editTenant','middleware'=>'auth']);
Route::post('tenants/changerent', ['as' => 'landlords.changerent', 'uses' => 'LandlordsController@changeRent','middleware'=>'auth']);
Route::post('tenants/updateDocData', ['as' => 'landlords.updateDocData', 'uses' => 'LandlordsController@updateDocData','middleware'=>'auth']);
Route::post('tenants/updateItineraryData', ['as' => 'landlords.updateItineraryData', 'uses' => 'LandlordsController@updateItineraryData','middleware'=>'auth']);
Route::post('tenants/edit/{id}', ['as' => 'landlords.postedittenant', 'uses' => 'LandlordsController@postEditTenant','middleware'=>'auth']);
Route::post('tenants/editdoc/{id}', ['as' => 'landlords.postedittenantDoc', 'uses' => 'LandlordsController@postEditTenantDoc','middleware'=>'auth']);
Route::post('tenants/edititinerary/{id}', ['as' => 'landlords.postedittenantItinerary', 'uses' => 'LandlordsController@postEditTenantItinerary','middleware'=>'auth']);
Route::get('payments/paynowmonthlyrent/{string}', ['as' => 'payment.getpaynowmonthlyrent', 'uses' => 'PaymentsController@getPayNowMonthlyrent','middleware'=>'auth']);
Route::post('payments/paynowmonthlyrent/{string}', ['as' => 'payment.postpaynowmonthlyrent', 'uses' => 'PaymentsController@postPayNowMonthlyrent','middleware'=>'auth']);
Route::get('payments/paynowitinerary/{string}', ['as' => 'payment.getpaynowitinerary', 'uses' => 'PaymentsController@getPayNowItinerary','middleware'=>'auth']);
Route::post('payments/paynowitinerary/{string}', ['as' => 'payment.postpaynowitinerary', 'uses' => 'PaymentsController@postPayNowItinerary','middleware'=>'auth']);
Route::get('landlords/tenants/{id?}', ['as' => 'landlords.tenants', 'uses' => 'LandlordsController@getTenant','middleware'=>'auth']);
Route::get('landlords/property', ['as' => 'landlords.getProperty', 'uses' => 'PropertiesController@getProperty','middleware'=>'auth']);
Route::get('payments/settings', ['as' => 'payments.getsettings', 'uses' => 'PaymentsController@getSettings','middleware'=>'auth']);
Route::post('payments/paypal', ['as' => 'payments.postaddpaypal', 'uses' => 'PaymentsController@postAddPaypal','middleware'=>'auth']);
Route::post('payments/bank', ['as' => 'payments.postaddbank', 'uses' => 'PaymentsController@postAddBank','middleware'=>'auth']);
Route::post('property/saveImage', ['as' => 'property.saveimage', 'uses' => 'PropertiesController@saveImage','middleware'=>'auth']);
Route::post('property/deleteImage', ['as' => 'property.deleteimage', 'uses' => 'PropertiesController@deleteImage','middleware'=>'auth']);
Route::post('property/deleteImageDB', ['as' => 'property.deleteimagedb', 'uses' => 'PropertiesController@deleteImageDB','middleware'=>'auth']);
Route::post('property/setImageFeatured', ['as' => 'property.setimagefeatured', 'uses' => 'PropertiesController@setImageFeatured','middleware'=>'auth']);
Route::post('property/saveDoc', ['as' => 'property.savedoc', 'uses' => 'PropertiesController@saveDoc','middleware'=>'auth']);
Route::post('property/deleteDoc', ['as' => 'property.deletedoc', 'uses' => 'PropertiesController@deleteDoc','middleware'=>'auth']);
Route::post('property/deleteDocDB', ['as' => 'property.deletedocdb', 'uses' => 'PropertiesController@deleteDocDB','middleware'=>'auth']);
Route::post('payment/updatePayment', ['as' => 'payment.updatePayment', 'uses' => 'PaymentsController@updatePayment','middleware'=>'auth']);
Route::post('property/fetchAmenities', ['as' => 'property.fetchamenities', 'uses' => 'PropertiesController@fetchAmenities','middleware'=>'auth']);

Route::get('landlords/tenant/{slug}', ['as' => 'landlords.getTenantProfileFromLandlord', 'uses' => 'LandlordsController@getTenantProfileFromLandlord','middleware'=>'auth']);
Route::get('landlords/payments/{dateString?}/{id?}', ['as' => 'payment.getTenantPayment', 'uses' => 'PaymentsController@getTenantPayment','middleware'=>'auth']);

Route::any('landlords/featuredproperties', ['as' => 'landlord.getFeaturedProperties', 'uses' => 'FeaturedPropertiesController@getFeaturedProperties','middleware'=>'auth']);
Route::get('landlords/featuredpurchase/{durationId?}/{propertyId?}', ['as' => 'landlord.purchaseFeatured', 'uses' => 'FeaturedPropertiesController@featuredPurchase']);

Route::any('/featured_paypal_payment_notify', ['as' => 'landlord.paypalNotify', 'uses' => 'FeaturedPropertiesController@featuredPaypalNotify']);

#######################################tenants changePassword end ####################################

####################################### reset password for tenants ####################################
Route::get('tenants/forgotpassword', ['as' => 'tenants.forgotpassword', 'uses' => 'TenantsController@getForgotPassword']);
Route::post('tenants/forgotpassword', ['as' => 'tenants.forgotpassword', 'uses' => 'TenantsController@postForgotPassword']);
Route::get('tenants/resetpassword/{token}', ['as' => 'tenants.resetpassword', 'uses' => 'TenantsController@getResetPassword']);
Route::post('tenants/resetpassword', ['as' => 'tenants.resetpassword', 'uses' => 'TenantsController@postResetPassword']);
####################################### reset password for tenants end ################################

###################################### register a new tenant###########################################
Route::get('tenants/register', ['as' => 'tenants.register', 'uses' => 'TenantsController@register']);
Route::post('tenants/register', ['as' => 'tenants.register', 'uses' => 'TenantsController@signup']);
###################################### register a new tenant ends###########################################

###################################### register a new landlord ###############################################
Route::get('landlords/register', ['as' => 'landlords.register', 'uses' => 'LandlordsController@register']);
Route::post('landlords/register', ['as' => 'landlords.register', 'uses' => 'LandlordsController@signup']);
###################################### register a new landlord ends ##########################################

Route::get('landlords/login', ['as' => 'landlords.getLogin', 'uses' => 'LandlordsController@getLogin']);
Route::post('landlords/login', ['as' => 'landlords.postLogin', 'uses' => 'LandlordsController@postLogin']);
Route::get('landlords/logout', ['as' => 'landlords.logout', 'uses' => 'LandlordsController@logout']);
Route::get('landlords/dashboard', ['as' => 'landlords.dashboard', 'uses' => 'LandlordsController@dashboard','middleware'=>'auth']);
Route::post('landlords/updateShool', ['as' => 'landlords.updateShool', 'uses' => 'LandlordsController@updateShool','middleware'=>'auth']);

###################################### reset password for landlords ########################################
//Route::get('landlords/email', ['as' => 'landlords.email', 'uses' => 'LandlordsController@getEmail']);
Route::get('landlords/forgotpassword', ['as' => 'landlords.forgotpassword', 'uses' => 'LandlordsController@getForgotPassword']);
Route::post('landlords/forgotpassword', ['as' => 'landlords.forgotpassword', 'uses' => 'LandlordsController@postForgotPassword']);
Route::get('landlords/resetpassword/{token}', ['as' => 'landlords.resetpassword', 'uses' => 'LandlordsController@getResetPassword']);
Route::post('landlords/resetpassword', ['as' => 'landlords.resetpassword', 'uses' => 'LandlordsController@postResetPassword']);
###################################### reset password for landlords end ####################################
//static pages  routes atarts here
Route::get('cms/{pageslug}', ['as' => 'pages.showCmsPages', 'uses' => 'IndexController@showCmsPages']);
Route::get('/faq', ['as' => 'pages.faq', 'uses' => 'IndexController@showFaq']);
Route::get('/schools', ['as' => 'schools.getSchools', 'uses' => 'SchoolsController@getSchools']);

Route::any('/advertisement', ['as' => 'index.banners', 'uses' => 'IndexController@showBanners']);
Route::get('/advertisement_purchase/{bannerId}', ['as' => 'index.purchase', 'uses' => 'IndexController@bannerPurchase']);
Route::post('/advertisement_purchase', ['as' => 'index.postBannerPurchase', 'uses' => 'IndexController@postBannerPurchase']);
//Route::post('/advertisement_cancel', ['as' => 'index.bannerCancel', 'uses' => 'IndexController@bannerCancel']);
Route::any('/paypal_payment_notify', ['as' => 'index.paypalNotify', 'uses' => 'IndexController@paypalNotify']);
########################################test route##################################

Route::get('index/test', ['as' => 'index.test', 'uses' => 'IndexController@test']);

Route::get('index/cleartable/{tablename}', ['as' => 'index.clearTable', 'uses' => 'IndexController@clearTable']);
