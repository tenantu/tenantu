<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    	Form::macro('selectMonths', function($name, $options = array(), $format = '%B')
        {
            $months = array(0 => 'Month');

            foreach (range(1, 12) as $month)
            {
                $months[$month] = strftime($format, mktime(0, 0, 0, $month, 1));
            }

            return Form::select($name, $months, null, $options);
        });
        
        Form::macro('selectDays', function($name, $options = array(), $format = '%B')
        {
            $days = array(0 => 'Day');

            foreach (range(1, 31) as $day)
            {
                $days[$day] = $day;
            }

            return Form::select($name, $days, null, $options);
        });

        Form::macro('selectYears', function($name, $options = array(), $format = '%B')
        {
            $years = array(0 => 'Year');

            foreach (range(1950, 2015) as $year)
            {
                $years[$year] = $year;
            }

            return Form::select($name, $years, null, $options);
        });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}