<?php

use App\Http\Models\Post;
use App\Http\Models\User;
use App\Http\Models\Country;
use App\Http\Models\Point;
use App\Http\Models\PostCountry;
use App\Http\Models\UserPoint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class PostTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $count = 250;
        $contentType = $pointId = 2;

        $faker = Faker\Factory::create('en_GB');

        $faker->addProvider(new Faker\Provider\en_GB\Address($faker));
        $faker->addProvider(new Faker\Provider\en_GB\Internet($faker));
        $faker->addProvider(new Faker\Provider\Uuid($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));

        $this->command->info('Inserting ' . $count . ' sample records using Faker ...');

        for ($x = 0; $x < $count; $x++) {

            //$width = mt_rand(800, 1600);
            //$height = mt_rand(400, 800);

            $width = mt_rand(300, 1600);
            $height = mt_rand(300, 800);

            $category = DB::table('categories')->select('categories.id')->addSelect('categories.name')->leftJoin('category_types', 'category_types.category_id', '=', 'categories.parent_id')->where('category_types.type_id', '=', $contentType)->orderByRaw("RAND()")->first();
            $user = User::select('id')->where('is_active', '=', 1)->orderByRaw("RAND()")->first();

            if ($contentType == 1) {
                $long_desc = '<p>' . implode('</p><p>', $faker->paragraphs($nb = 10)) . '</p>';
            } else {
                $long_desc = '';
            }

            $locations  = $faker->country;
            $randomDate = $faker->date($format = 'Y-m-d H:i:s', $max = 'now');

            $post = Post::create([
                'type_id' => $contentType,
                'category_id' => $category->id,
                'user_id' => $user->id,
                'title' => $faker->text($maxNbChars = 140),
                'short_desc' => $faker->text($maxNbChars = 280),
                'long_desc' => $long_desc,
                'locations' => $locations,
                'is_active' => 1,
                'is_published' => 1,
                'created_at' => $randomDate,
                'updated_at' => $randomDate
            ]);

            if ($contentType == 2) {
                $dir = "public/uploads/images/{$post->id}";
                File::makeDirectory($dir, $mode = 0777, true, true);
                $filePath = $faker->image($dir, $width, $height);
                $fileName = explode("/", $filePath);

                // Update Image
                $post->update(['image' => end($fileName)]);
            } else {
                $dir = "public/uploads/articles/{$post->id}";
                File::makeDirectory($dir, $mode = 0777, true, true);
                $filePath = $faker->image($dir, $width, $height);
                $fileName = explode("/", $filePath);

                // Update Image
                $post->update(['image' => end($fileName)]);
            }

            $country     = Country::where('name', $locations)->first();
            if ($country) {
                $data = new PostCountry(['country_id' => $country->id]);
                $post->post_countries()->save($data);
            }

            $point = Point::where('id', $pointId)->first();
            $user_points = UserPoint::create(['user_id' => $user->id, 'point_id' => $point->id]);

        }

        $this->command->info('Post table seeded using Faker ...');
    }

}
