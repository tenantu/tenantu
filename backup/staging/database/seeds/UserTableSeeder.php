<?php

use App\Http\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class UserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $count = 150;

        // $this->command->info('Deleting existing Person table ...');
        // DB::table('person')->delete();

        $faker = Faker\Factory::create('en_GB');

        $faker->addProvider(new Faker\Provider\en_GB\Address($faker));
        $faker->addProvider(new Faker\Provider\en_GB\Internet($faker));
        $faker->addProvider(new Faker\Provider\Uuid($faker));

        $this->command->info('Inserting ' . $count . ' sample records using Faker ...');

        for ($x = 0; $x < $count; $x++) {

            $width = mt_rand(100, 800);
            $height = mt_rand(100, 800);

            $user = User::create([
                'role_id' => 3,
                'name' => $faker->name,
                'email' => $faker->companyEmail,
                'password' => bcrypt('123456'),
                'gender' => $faker->randomElement($array = array('M', 'F')),
                'description' => '<p>' . implode('</p><p>', $faker->paragraphs(2)) . '</p>',
                'dob' => $faker->dateTimeThisCentury->format('Y-m-d'),
                'location' => $faker->country,
                'is_active' => $faker->randomElement($array = array('0', '1')),
                'personal_link' => $faker->url,
            ]);

            $dir = "public/uploads/users/{$user->id}";
            File::makeDirectory($dir, $mode = 0777, true, true);
            $filePath = $faker->image($dir, $width, $height);
            $fileName = explode("/", $filePath);

            // Update Image
            $user = User::find($user->id);
            $user->update(['image' => end($fileName)]);

            // Create Avater
            File::makeDirectory($dir . '/60_x_60', $mode = 0777, true, true);
            Image::make($filePath)->resize(60, 60)->save($dir . '/60_x_60/' . end($fileName));
        }

        $this->command->info('User table seeded using Faker ...');
    }

}