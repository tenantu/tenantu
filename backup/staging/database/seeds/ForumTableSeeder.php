<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Forum;

class ForumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 100;
 
        $this->command->info('Deleting existing Forum table ...');
        DB::table('forums')->delete();
 
        $faker = Faker\Factory::create('en_GB');
 
        $faker->addProvider(new Faker\Provider\en_GB\Address($faker));
        $faker->addProvider(new Faker\Provider\en_GB\Internet($faker));
        $faker->addProvider(new Faker\Provider\Uuid($faker));
 
        $this->command->info('Inserting '.$count.' sample records using Faker ...');
        // $faker->seed(1234);
 
        for( $x=0 ; $x<$count; $x++ )
        {
        	$user = App\Http\Models\User::select('id')->where('is_active', '=', 1)->orderByRaw("RAND()")->first();
            $forum = Forum::create([
                'user_id' => $user->id,
                'title' =>  $faker->text($maxNbChars = 200),
                'short_desc' => $faker->text($maxNbChars = 200),
                'long_desc' => '<p>'.  implode('</p><p>', $faker->paragraphs(5)) .'</p>',
                'is_active' => $faker->randomElement($array = array ('0','1')),
                'is_published' => $faker->randomElement($array = array ('0','1')),
            ]);
            
        }
 
        $this->command->info('Forum table seeded...');
    }
}
