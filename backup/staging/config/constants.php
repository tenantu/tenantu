<?php

return [
    'SITE_NAME' => 'Tenantu',
    'SUPPORT_EMAIL' => 'support@tenantu.com',
    'CONTACT_EMAIL' => 'jacob.jeifa@tenantu.com',
    'HTACCESS_FILE' => public_path('.htaccess'),
    'BANNER_IMAGE_PATH'=>public_path('uploads/banners/'),
    'BANNER_IMAGE_URL'=>asset('public/uploads/banners/'),
    'TENANT_IMAGE_PATH' => public_path('uploads/tenants/'),
    'LANDLORD_IMAGE_PATH' => public_path('uploads/landlords/'),
    'TENANT_IMAGE_URL' => asset('public/uploads/tenants/'),
    'TENANT_NOT_PROFILE_IMAGE_URL' => asset('public/uploads/noimage/'),
    'LANDLORD_NOT_PROFILE_IMAGE_URL' => asset('public/uploads/noimage/'),
    'LANDLORD_IMAGE_URL' => asset('public/uploads/landlords/'),
    'PROPERTY_IMAGE_PATH' => public_path('uploads/properties/'),
    'PROPERTY_IMAGE_URL' => asset('public/uploads/properties/'),
    'SCHOOL_IMAGE_PATH' => public_path('uploads/schools/'),
    'SCHOOL_IMAGE_URL' => asset('public/uploads/schools/'),
    'TENANT_DOC_PATH' => public_path('uploads/tenantdocs/'),
    'TENANT_DOC_URL' => asset('public/uploads/tenantdocs/'),
    'PROPERTY_NO_IMAGE_PATH' => public_path('uploads/properties/'),
    'PROPERTY_NO_IMAGE_URL' => asset('public/uploads/noimage/'),
    'FORGOT_PASSWORD_EMAIL_FROM' => 'sooraj@gmail.com',
    'FORGOT_PASSWORD_EMAIL_FROM_NAME' => 'sooraj',
    'PAGING_COUNT' => 25,
    
    'LOG_FILE' => public_path('log.txt'),
    'USE_SANDBOX'=> 1,
    'DEBUG' => 1,
    'PAYPAL_BUSS_EMAIL' => 'jacob.jeifa@tenantu.com',
    
    
];

