<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Enviroment
    |--------------------------------------------------------------------------
    |
    | Please provide the enviroment you would like to use for braintree.
    | This can be either 'sandbox' or 'production'.
    |
    */
	'environment' => 'sandbox',

	/*
    |--------------------------------------------------------------------------
    | Merchant ID
    |--------------------------------------------------------------------------
    |
    | Please provide your Merchant ID.
    |
    */
	'merchantId' => '8ybxws7743pr8w7d',

	/*
    |--------------------------------------------------------------------------
    | Public Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Public Key.
    |
    */
	'publicKey' => 's6gx4y6yv2r56br2',

	/*
    |--------------------------------------------------------------------------
    | Private Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Private Key.
    |
    */
	'privateKey' => '52b7e05724dba4fbe1bd6cb4f316caab',

	/*
    |--------------------------------------------------------------------------
    | Client Side Encryption Key
    |--------------------------------------------------------------------------
    |
    | Please provide your CSE Key.
    |
    */
	'clientSideEncryptionKey' => 'MIIBCgKCAQEAqWddZ5qp2XD3pQLjvwq2tRLhNo2fr+dTKsEVQ+hRtQEOxcUhAmq+s6fltZ7tY/igPw0Euvn6Tllfkc/n2XDYcAm+eXFTSksfV4QqeNFYyGtivVDbvSVqy8MB0dI5zo6AEeLCcNLxau+rtKkg+v5PAwJgpIEavMxpjVF1IphaYWMKohJiGHqRnK2RUAUhCqIfBBVKichj64wT7jzprk/S0OBBWzBnS28fcys4AuLEJ9Y2OnYo0QmR3PnwrK6wp74ltIGlEB2c6qS5d28YcG2KMdYXZUaWwYfVXFiybUUAQs2SaiQGttV/GYjetb3LCT9Ly2zq+f03Bb7onisRaVUPHQIDAQAB',
	
];
