@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
        <!-- Main content -->
<!-- Main content -->
        <section class="content my-contacts lndlrd-contacts">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="add-tenants adds"> 
          
          <!-- form submission --> 
          <!-- ===================== -->
          @include('elements.validation-message')
               
           @if (Session::has('message-success'))
              @include('elements.message-success')
           @endif
           @if (Session::has('message-error'))
              @include('elements.message-error')
           @endif
          {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'tenantAddForm','route'=>['landlords.posttenant'],'files'=>true]) !!}
            <div class="row">
              <div class="col-sm-12">
                <h2>Step 1</h2>
                <!-- heading --> 
              </div>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <div class="form-group">
                  {!! Form::label('selectTenant', 'Select Tenant:') !!}<span style="color: red;">*</span>
                  {!! Form::select('tenant_id',['' => 'Select Tenant']+$tenantDropDown, null, ['class'=>'form-control login-input','id'=>'tenantId','required']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('property', 'Property:') !!}<span style="color: red;">*</span>
                  {!! Form::select('poperty_id',['' => 'Select Property']+$propertyDropdown, null, ['class'=>'form-control login-input','id'=>'popertyId','required']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('agriment_starts_on', 'Agreement starts on:') !!}<span style="color: red;">*</span>
                  {!! Form::text('agriment_start_date', Input::old('agriment_start_date'), ['class'=>'form-control login-input','id'=>'agriment_start_date','placeholder'=>date('Y-m-d'),'data-provide'=>'datepicker','required','required' ]) !!}
                </div>
                <div class="col-sm-9">
                  {!! Form::submit('Save', ['class' => 'btn add-btn']) !!}
                  {!! Form::submit('Cancel', ['class' => 'btn cancel-btn']) !!}
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-0">
                <div class="form-group">
                  {!! Form::label('dueon', 'Due on:') !!}<span style="color: red;">*</span>
                  {!! Form::select('due_on',['' => 'Select One']+$dueOn, null, ['class'=>'form-control login-input','id'=>'dueOn','required']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('rentprice', 'Monthly Rent:') !!}<span style="color: red;">*</span>
                  {!! Form::input('number','month_rent', Input::old('month_rent'), ['class'=>'form-control login-input','id'=>'monthRent','placeholder'=>'Monthly Rent','required']) !!}
                </div>
                <div class="form-group">
                  {!! Form::label('cycle', 'Cycle:') !!}<span style="color: red;">*</span>
                  {!! Form::select('cycle',['' => 'Select One']+$cycleDropdown, null, ['class'=>'form-control login-input','id'=>'cycle','required']) !!}
                </div>
              </div>

            </div>
          {!! Form::close() !!}

          
          <!-- form submission Ends --> 
          <!-- ========================== --> 
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
  
  </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
      @endsection
