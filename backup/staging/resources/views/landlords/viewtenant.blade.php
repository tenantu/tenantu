@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
        <!-- Main content -->
<!-- Main content -->
        <section class="content my-contacts lndlrd-contacts">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-sm-2 edit-btn-cntr">
                  <a href="{{ route('landlords.edittenant',$landlordTenant->id) }}" class="btn edit-tenant-btn">Edit </a>
                  <a href="{{ route('landlords.tenants') }}" class="btn cancel-tenant-btn">Cancel </a>
              </div>
        </div>
        <div class="add-tenants"> 
          
          <!-- form submission --> 
          <!-- ===================== -->
          @include('elements.validation-message')
               
           @if (Session::has('message-success'))
              @include('elements.message-success')
           @endif
          <form>
            <div class="row">
              <div class="col-sm-12">
                <h2>Tenant Details</h2>
                <!-- heading --> 

                <h3><i class="fa fa-angle-double-right"></i> Step 1</h3>
              </div>
            </div>
            <div class="row tnt-dtls-top">
              <div class="col-sm-4 col-xs-12">
                <div class="tenant-dtls text-center">
                    <div class="tnt-profile-img"><img src="{{ HTML::getTenantProfileImage($landlordTenant->tenant->id,'') }}"></div>
                    <div class="tenant-dtls-name"> {{ $landlordTenant->tenant->firstname.' '.$landlordTenant->tenant->lastname }}</div>
                </div>
                <div class="tenant-dtls text-center">
                    <label>Property</label>
                    <div class="tenant-dtls-property-name">{{ $landlordTenant->property->property_name  }}</div>
                  </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                <h4 class="monthly-rent">Monthly Rent</h4>
                <table class="table">
                  <tr class="head-txt">
                    <td> Sl. No.</td>
                    <td><i class="fa fa-dollar"></i> Bill Amount</td>
                    <td><i class="fa fa-calendar"></i> Bill Date</td>
                  </tr>
                  @forelse( $landlordTenant->landlord_tenant_monthlyrents as $key=> $monthlyRent )
                  <tr>
                    <td>{{ ($key+1) }}</td>
                     <td>{{ $monthlyRent->amount }}</td>
                    <td>{{ $monthlyRent->bill_on }}</td>
                  </tr>
                  @empty
                  <tr><td>No data</td></tr>
                  @endforelse
                </table>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-xs-12">
              </div>
            </div>
            <div class="row margin-top40">
              <div class="col-sm-12 col-xs-12">
                <h3><i class="fa fa-angle-double-right"></i> Step 2</h3>
                <!-- heading --> 
              </div>
              <div class="step-2"><!-- repeat the contents -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                      <div class="col-sm-12">

                        <table class="table">
                          <tbody>
                            <tr class="head-txt">
                              <td>Title</td>
                              <td><i class="fa fa-comment"></i> Description</td>
                              <td><i class="fa fa-file-text"></i> Document</td>
                            </tr>
                            @forelse($landlordTenant->landlord_tenant_docs as $doc)
                            <tr>
                              <td>{{ $doc->doc_title }}</td>
                              <td>{{ $doc->doc_description }}</td>
                              <td><a href="{{ asset('public/uploads/tenantdocs/'.$doc->file_name) }}" target="_blank">{{ HTML::getDocExtn( $doc->file_name ) }} </a></td>
                            </tr>
                            @empty
                            <tr>
                              <td>No records found!</td>
                            </tr>
                            @endforelse
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <!-- <div class="col-sm-5 col-xs-12 col-sm-offset-1">
                    <label>Documents</label>
                    <table class="table">
                        <tbody>
                          <tr>
                            <td><a href="#">Privacypolicy.doc</a></td>
                            <td><a href="#"><i class="fa fa-close"></i></a></td>
                          </tr>
                          <tr>
                            <td><a href="#">Privacypolicy.doc</a></td>
                            <td><a href="#"><i class="fa fa-close"></i></a></td>
                          </tr>
                        </tbody>
                    </table>
                  </div> -->
                </div>
              </div>
            <div class="row margin-top40">
              <div class="col-sm-12">
                <h3><i class="fa fa-angle-double-right"></i> Step 3 -  payment itineraries</h3>
                <!-- heading --> 
              </div>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <h4 class="Itinerary">Itinerary</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                <table class="table">
                  <tr class="head-txt">
                    <td>
                      Sl. No.
                    </td>
                    <td>
                      Itinerary
                    </td>
                    <td>
                     <i class="fa fa-calendar"></i> Bill Date
                    </td>
                    <td>
                      <i class="fa fa-dollar"></i> Amount
                    </td>

                    
                  </tr>
                  @forelse( $landlordTenant->landlord_tenant_otherpayment_details as $key=> $detail )
                  <tr>
                    <td>{{ ($key+1) }}</td>
                    <td>
                      {{ $detail->title }}
                    </td>
                     <td>
                      {{ $detail->bill_on }}
                    </td>
                     <td>
                      {{ $detail->amount }}
                    </td>
                   
                  </tr>
                  @empty
                  <tr><td>No data</td></tr>
                  @endforelse
                </table>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-5 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-0"> </div>
            </div>
          </form>
          <!-- form submission Ends --> 
          <!-- ========================== --> 
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
  
  </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
      @endsection
