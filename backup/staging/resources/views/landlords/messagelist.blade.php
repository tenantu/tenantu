<div class="col-md-9 col-md-9 col-sm-8 col-xs-12">
  <!-- ================================== -->
  <!-- DIRECT CHAT START-->
  <!-- ================================== -->
  <div class="box direct-chat landlord-message">
    <!-- box-header -->
    <div class="box-header with-border">
      <div class="media">
        <div class="media-body">
         <!--  <div class="add-to-list pull-right">
           <button class="btn btn-box-tool add-to-wish" data-toggle="tooltip" title="Add to List">Add to Wish List <i class="fa fa-plus"></i></button>
           <button class="btn btn-box-tool msg-cnts" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
         </div> -->
          <div class="col-sm-6">
            <h4 class="media-heading">{{ $threads->thread_name }} <font color='#58bec7'>({{ $msgPropertyName }})</font></h4>
            <span class="college">Created at: {{ $msgThreadCreatedDate }}</span>
          </div>
          <div class="col-sm-5">
            <div class="pull-right">
              @if($threadTypeFlag==1 && $threadStatus!=2)
              {!! Form::model(null,['method' => 'POST','route'=>['landlords.composeMessageList',$messageThreadId]]) !!}
              {!! Form::hidden('tenantId', $tenantId) !!}
              <input type="submit" class="btn btn-warning btn-flat processing" name="buttonValue" value="Processing">
              <input type="submit" class="btn btn-warning btn-flat completed" name="buttonValue" value="Completed">
              {!! Form::close() !!}
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <!-- Conversations are loaded here -->
      <div class="direct-chat-messages">
        
        @foreach ($allMessages as $message)
        <?php
        $name='';
        $msgCls= '';
        ?>
        
        @if($message['sender']=='T')
        <?php $name=$tenantName;
        $msgCls='recieved';
        ?>
        @else
        <?php $name=$profile->firstname.' '.$profile->lastname;
        $msgCls='send';
        ?>
        @endif
        
        <div class="direct-chat-msg {{$msgCls}}">
          <div class="direct-chat-info clearfix">
            <div class="direct-chat-name">{{$name}} :</div>
            <?php
                            $createdDate = date('d M h:i:s a',strtotime($message['created_at']));
            ?>
            <div class="direct-chat-timestamp">{{$createdDate}}</div>
            </div><!-- /.direct-chat-info -->
            <div class="direct-chat-text">
              {{$message['message']}}
              </div><!-- /.direct-chat-text -->
            </div>
            
            @endforeach
            </div><!--/.direct-chat-messages-->
            
            </div><!-- /.box-body -->
            <!-- compose message -->
            <!-- ====================== COMPOSE MESSAGE ============== -->
            <div class="box-footer">
              {!! Form::model(null,['method' => 'POST','route'=>['landlords.composeMessageList',$messageThreadId]]) !!}
              <div class="input-group">
                {!! Form::hidden('tenantId', $tenantId) !!}
                {!! Form::text('message',null,['class'=>'form-control write-message','placeholder'=>'Type Message ...','required' => '']) !!}
                
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-warning btn-flat send-message-btn">Send</button>
                </span>
              </div>
              {!! Form::close() !!}
              </div><!-- /.box-footer-->
            </div>
            <!-- ================================== -->
            <!-- DIRECT CHAT END-->
            <!-- ================================== -->
            </div><!-- /.col -->
