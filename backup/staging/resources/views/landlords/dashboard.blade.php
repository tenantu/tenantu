@extends('layouts.landlord-inner')
@section('inner-content')
@if (Session::has('message-success'))
	@include('elements.message-success')
@endif
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
		@include('elements.breadcrump-landlord')
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
              <!-- Poist property -->
              <div class="tnt-box post-prts">
                <div class="box-heads">
                  <h4>Post Property</h4>
                </div>
                <div class="post-property-links"><a href="{{ route('property.add') }}">Post New</a></div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-6 col-md-5 col-sm-8 col-xs-12">
              <!-- small box -->
              <div class="tnt-box view-property">
                <div>
                  <div class="box-heads">
                    <h4>View Properties</h4>
                    @if(count( $properties ))
                    <a href="{{ route('landlords.getProperty') }}" class="blue">More »</a>
                    @endif
                  </div>
                  <ol>
                    @forelse($properties as $key => $property)
                  <li>
                  <span class="number">{{ ($key+1) }}</span>{{ $property->property_name }}
                  <a href="{{ route('property.edit',$property->id) }}" class="pull-right">Details</a></li>
                  @empty
                    <p>There are no records yet!</p>
                   @endforelse 
                </ol>
                </div>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
              <!-- small box -->
              <div class="tnt-box">
                <div>
                  <div class="box-heads">
                    <h4>My Payments</h4>
                  </div>
                  <div class="payment-links"><i class="fa fa-dollar"></i><a href="{{ route('payment.getTenantPayment') }}">Payment</a></div>
                </div>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            
            
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            

            <section class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><!-- connectedSortable -->
              <!-- my property -->
              <div class="my-property tnt-box">
                 <div class="box-heads">
                    <h4>General Enquiry</h4>
                    <!-- <a href="#" class="light-blue">View More »</a> -->
                  </div>
                  <div class="responses row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <ul>
                        @forelse($generalMessages as $key => $generalMessage)
                        <li class="row">
                          <div class="texts"><strong>{{ $generalMessage->thread_name }}:</strong> {{ substr($generalMessage->message,0,20) }}</div>
                          <div class="view">
                            <a href="{{route('landlords.getMessageList',$generalMessage->message_thread_id)}}">Details</a>
                          </div>
                        </li>
                        @empty
                          <p>There are no messages yet!</p>
                        @endforelse 
                      </ul>
                    </div>
                  </div>
              </div>
              <!-- my property -->
              <div class="my-property tnt-box">
                 <div class="box-heads">
                    <h4>Issue Reported</h4>
                    <!-- <a href="#" class="light-blue">View More »</a> -->
                  </div>
                  <div class="responses row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <ul>
                        @forelse($issueMessages as $key => $issueMessage)
                        <li class="row">
                          <div class="texts"><strong>{{ $issueMessage->thread_name }}:</strong> {{ substr($issueMessage->message,0,20) }}</div>
                          <div class="view">
                            <a href="{{route('landlords.getMessageList',$issueMessage->message_thread_id)}}">Details</a>
                          </div>
                        </li>
                        @empty
                          <p>There are no messages yet!</p>
                        @endforelse 
                      </ul>
                    </div>
                  </div>
              </div>

              

            </section><!-- /.Left col -->
            <section class="col-lg-6 col-md-6 col-sm-12 col-xs-12"><!-- connectedSortable -->
              <div class="tnt-box my-searches">
                <div class="box-heads">
                  <h4>Reviews</h4>
                  @if(count( $propertiesReview ))
                  <a href="{{ route('properties.review') }}" class="light-blue">View More »</a>
                  @endif
                </div>
                <div class="new-rating">
                  <ul>
                   @forelse( $propertiesReview as $key => $property )
                    <li class="row">
                        <div class="col-lg-5"><a href="{{ route('properties.review',$property->slug) }}">{{ $property->property_name }}</a></div>
                        <div class="col-lg-2">reviews({{ HTML::getReviewCount($property->id) }})</div>
                        <div class="col-lg-5"><img src="{{ HTML::getAverageReviewRatingForLandlord($property->id) }}"></div>
                    </li>
                   @empty
                      <p>There are no reviews yet!</p> 
                   @endforelse
                  </ul>
                </div>
              </div>
            </section><!-- right col -->
			<!--@include('elements.landlord-rightarea')-->
          </div><!-- /.row (main row) -->
          
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

@endsection
