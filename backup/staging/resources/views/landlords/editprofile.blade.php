@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
        <!-- Main content -->
<!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <!-- Landlord Profile -->

              <div class="tnt-box landlord-profile">

              {!! Form::model('',['method' => 'POST','class'=>'edit-profile','id'=>'editProfileForm','route'=>['landlords.posteditprofile'],'files'=>true]) !!}
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="profile-image">
                      <img src="{{ $profileImage }}">
                      <a class="change-profile-img" data-toggle="modal" data-target=".ChangeProfileImg" style="cursor: pointer;">Change Photo</a>
                      </div>
                  </div>
                </div>
                <!-- edit landlord  profile input -->
                <div class="row">
                  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                     @include('elements.validation-message')
               
                     @if (Session::has('message-success'))
                        @include('elements.message-success')
                     @endif
                   </div>  
                 </div>    
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                     
                    <div class="form-group">
                      {!! Form::label('firstName', 'First Name:') !!}<span style="color: red;">*</span>
                      {!! Form::text('firstname', $profile->firstname, ['class'=>'form-control login-input','id'=>'firstName','placeholder'=>'First Name']) !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('lastname', 'Last Name:') !!}<span style="color: red;">*</span>
                      {!! Form::text('lastname', $profile->lastname, ['class'=>'form-control login-input','id'=>'lastName','placeholder'=>'Last Name']) !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('location', 'Location:') !!}<span style="color: red;">*</span>
                      {!! Form::text('location', $profile->location, ['class'=>'form-control login-input','id'=>'location','placeholder'=>'Location']) !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('phone', 'Phone:') !!}<span style="color: red;">*</span>
                      {!! Form::text('phone', $profile->phone, ['class'=>'form-control login-input','id'=>'phone','placeholder'=>'Phone']) !!}
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                    {!! Form::label('description', 'Description:') !!}<span style="color: red;">*</span>
                      {!! Form::textarea('description', $profile->description, ['class'=>'form-control login-input','id'=>'description','placeholder'=>'Description','rows'=>3]) !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('website', 'Website:') !!}
                      {!! Form::text('website', $profile->website, ['class'=>'form-control login-input','id'=>'website','placeholder'=>'www.example.com']) !!}
                    </div>
                    <div class="form-group">
                    {!! Form::label('school', 'School:') !!}<span style="color: red;">*</span>
                    {!! Form::select('school_id',['' => 'Select school']+$schools, $profile->school_id, ['class'=>'form-control login-input','id'=>'school']) !!}
                  </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="box-heads">
                      <h4>Other Details</h4>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-6 col-cs-12">
                    <div class="form-group">
                      {!! Form::label('city', 'City:') !!}
                      {!! Form::text('city', $profile->city, ['class'=>'form-control login-input','id'=>'city','placeholder'=>'City']) !!}
                    </div>
                    <div class="form-group">
                      {!! Form::label('address', 'Contact Details:') !!}
                      {!! Form::text('address', $profile->address, ['class'=>'form-control login-input','id'=>'address','placeholder'=>'Address']) !!}
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                      {!! Form::label('state', 'State:') !!}
                      {!! Form::text('state', $profile->state, ['class'=>'form-control login-input','id'=>'state','placeholder'=>'State']) !!}
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    {!! Form::submit('Submit', ['class' => 'btn btn-primary save-btn']) !!}
                    {!! Form::submit('Cancel', ['class' => 'btn cancel-btn']) !!}
                  </div>
                </div>
                {!! Form::close() !!}
              </div>
            <!-- ./col -->
          </div><!-- /.row -->

          <div>
          
        </section><!-- /.content -->

        {!! Form::open(['class'=>'edit-tnt-form','route'=>['landlords.posteditprofileimage'],'files'=>true]) !!}
                <div class="modal fade bs-example-modal-sm ChangeProfileImg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm">
          <div class="modal-content">

            <div class="modal-header">
              <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
              <h4 id="mySmallModalLabel" class="modal-title">Upload profile image</h4>
            </div>
            <div class="modal-body">
               {!! Form::file('image', ['id' => 'image']) !!}
               <button type="submit" class="btn btn-primary">OK</button>
            </div>
            </div>
          </div>
        </div>
        {!! Form::close() !!}
           
		
      </div><!-- /.content-wrapper -->
      @endsection
