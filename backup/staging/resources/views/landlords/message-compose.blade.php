  <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::open(['method' => 'POST','route'=>['landlords.postMessage']]) !!}
                      <div class="create-msg">
                        <h3>New Message</h3>
                        <div class="form-group">
              {!! Form::hidden('sender', 'L') !!}
              {!! Form::hidden('tenantId', $tenantId) !!}

              <div class="select-box">
              {!! Form::select('messageType', ['' => 'Select Thread']+$threads,null,['class' => 'form-control']) !!}
              </div>
                          
                        </div>
                        @if(count($properties) > 1)
                        <div class="form-group">
                        <div class="select-box">
                            {!! Form::select('property', ['' => 'Select Property']+$properties,null,['class' => 'form-control']) !!}
                            </div>

                        </div>
                        @else
                         {!! Form::hidden('property', $propertyId) !!}
                        @endif
                        <div class="form-group">
                            {!! Form::textarea('message_content', null, ['class' => 'form-control','placeholder'=>'Say Something']) !!}
                        </div>
                        <div class="form-group">
                          <button class="btn btn-primary pull-right send-message-btn">Send</button>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                       {!! Form::close() !!}
                    </div>
                  </div>
                </div><!-- /.col -->