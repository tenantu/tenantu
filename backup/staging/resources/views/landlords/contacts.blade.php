@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
        <!-- Main content -->
        <section class="content my-contacts lndlrd-contacts">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            @forelse($tenants as $tenant)
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="contact-box clearfix">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <div class="prf-img"><img alt="image" class="img-circle img-responsive" src="{{HTML::getTenantProfileImage($tenant->tenant_id)}}"></div>
                            <!--<div class="m-t-xs font-bold">Graphics designer</div>-->
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h3>{{ $tenant->firstname.' '.$tenant->lastname }}</h3>
                        <p class="cnt-ltn"><i class="fa fa-map-marker"></i> {{ $tenant->city }}</p>
                        <div class="profile-description">
							@if(strlen($tenant->about) > 100)
								{{substr($tenant->about, 0, 100). '...'}}
							@else
							 {{$tenant->about}}
							 @endif
                        </div>
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8 col-sm-offset-0 col-xs-offset-2">
                            <a href="{{ route('landlords.getMessageList',$tenant->latestMsgThreadId->message_thread_id) }}" class="links"><i class="fa fa-comments"></i></a>
                            <a href="{{ route('landlords.newmessage',$tenant->tenant_id) }}" class="links"><i class="fa fa-envelope"></i></a>
                            <a href="{{route('landlords.getTenantProfileFromLandlord',$tenant->slug)}}" class="view-profile">View Profile</a>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="no-contacts text-center"><img src="{{asset('public/img/no-contacts.png')}}"></div>
			</div>
            @endforelse

        </div><!-- /.row -->
         </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
