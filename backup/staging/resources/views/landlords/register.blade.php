   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    {!! Form::model('',['method' => 'POST','route'=>['landlords.register']]) !!}
        <div class="form-group">
            {!! Form::label('Name', 'Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Email', 'Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Confirm Password', 'Confirm Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email', Input::old('name'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('gender', 'Gender:') !!}
             Male
            {!! Form::radio('gender', 'M', 'M', ['class'=>'iradio_minimal-blue']) !!}
            Female
            {!! Form::radio('gender', 'F', null, ['class'=>'iradio_minimal-blue']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('DOB', 'DOB:') !!}<br>
            {!! Form::selectRange('day', 1, 31, Input::old('day'), ['class'=>'col-md-3']) !!}
            {!! Form::selectMonth('month', Input::old('month'), ['class'=>'col-md-4']) !!}
            {!! Form::selectYear('year', '1950', date("Y"), Input::old('year'), ['class'=>'col-md-4']) !!}
         </div>
        <div class="form-group">
            {!! Form::label('Location', 'Location:') !!}
            {!! Form::text('location', Input::old('location'), ['class'=>'form-control']) !!}
        </div>
        
         
         
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}
