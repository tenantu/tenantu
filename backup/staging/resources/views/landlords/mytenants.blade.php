@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row my-tenants">
            @forelse($tenants as $tenant)
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
             
                <div class="tnt-box">
                      <div class="row tenant-name-container">
                        <div class="col-sm-3 col-xs-12">
                          <div class="tenant-image"><a href="{{ route('landlords.viewtenant',$tenant->id ) }}"><img src="{{ HTML::getTenantProfileImage($tenant->tenant->id) }}"></a></div>
                        </div>
                        <div class="col-sm-9 col-xs-12">
                          <div class="tenant-name"><a href="{{ route('landlords.viewtenant',$tenant->id ) }}">{{ $tenant->tenant->firstname.' '.$tenant->tenant->lastname }}</a></div>
                          <div class="grey-text"><i class="fa fa-map-marker"></i>{{ ($tenant->location!="") ? $tenant->location : "Not provided" }}</div>
                          <div class="grey-text"><i class="fa fa-building"></i> {{ $tenant->property->property_name }}</div>
                        </div>
                      </div>
                      <div class="row duration-container">
                        <div class="col-sm-6 col-xs-6">
                          <div class="agreement-duration">Agreement From</div>
                          <div>{{ date('m-d-Y',strtotime($tenant->agreement_start_date)) }}</div>
                        </div>
                        <div class="col-sm-6 col-xs-6">
                          <div class="agreement-duration">Agreement To</div>
                          <div>{{ date("m-d-Y", strtotime("+".$tenant->cycle, strtotime($tenant->agreement_start_date))) }}</div>
                        </div>
                      </div>
                      <div class="buttons-cntr">
                        <a href="{{route('landlords.getTenantProfileFromLandlord',$tenant->tenant->slug)}}" class="buttons"><i class="fa fa-user"></i>Profile</a>
                        
                        <a href="{{ route('payment.getTenantPayment',[$currMonthYear,$tenant->tenant->id]) }}" class="buttons"><i class="fa fa-paypal"></i>Payment Info</a>
                        <!-- <a href="#" class="buttons"><i class="fa fa-file-text"></i>Agreement Details</a>
                        <a href="#" class="buttons"><i class="fa fa-comments"></i><span class="label label-warning">10</span></a> -->
                      </div>
                </div><!-- /.box -->
                
            </div>
            @empty
                <div class="no-tenants text-center">
                  <img class="clearfix" src="{{ asset('public/img/no-tenants.png') }}">
                    <div><a class="add-tenant-btn cleafix" href="{{ route('landlords.addtenant') }}">Add Tenant</a>
                    </div>
                </div>
                @endforelse
                @if($id=="" && (count($tenants)!=0))
                  <div class="col-sm-4">
                    <div class="no-tenants text-center add-tenant">
                      <div class="add-tenant-cntr"><a class="add-tenant-btn cleafix" href="{{ route('landlords.addtenant') }}">Add Tenant <i class="fa fa-plus"></i></a></div>
                    </div>
                  </div>
               @endif
          </div><!-- /.row -->

          <div>
          
        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
