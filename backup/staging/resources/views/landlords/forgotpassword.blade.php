@if (Session::has('message'))
	<div class="alert alert-success alert-dismissable">
		
		{{ Session::get('message') }}
	</div>
@endif
{!! Form::model('',['method' => 'POST','route'=>['landlords.forgotpassword']]) !!}
    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        <button type="submit">
            Send Password Reset Link
        </button>
    </div>
{!! Form::close() !!}
