@extends('layouts.admin-master')

@section('admin-content')
<div class="article-index">
    <div class="row">
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" >
                <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                </div>
            </div>
        </div>
        <div class="col-md-2 pull-right">
            <br/>
            <a class="btn btn-block btn-primary"><i class="fa fa-plus-circle"></i> &nbsp;New Article</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
            <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">Action <span class="fa fa-caret-down"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Delete</a></li>
                </ul>
            </div>
        </div>
         <div class="col-md-8 text-right">
            <nav>
            <ul class="pagination">
                <li class="disabled">
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active">
                    <a href="#">1</a>
                </li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                </a>
                </li>
            </ul>
            </nav>
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                <th class="">
                    <div id="selectAllArticle">
                        <input type="checkbox" />
                    </div>
                </th>
                <th class="">#</th>
                <th class="col-md-7">Title</th>
                <th class="col-md-1">Created</th>
                <th class="col-md-2">Author</th>
                <th class="col-md-2 text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">1</th>
                <td><a href="#">This plane? This plane can fly for 118 hours</a></td>
                <td>08/18/2015</td>
                <td><a href="#">Kate Torgovnick May</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">2</th>
                <td><a href="#">What genetic engineering and organic farming have in common</a></td>
                <td>08/13/2015</td>
                <td><a href="#">Jeffry</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">3</th>
                <td><a href="#">The jaw-dropping promise — and brain-twisting challenge — of quantum computing</a></td>
                <td>08/12/2015</td>
                <td><a href="#">Karen Eng</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">4</th>
                <td><a href="#">What it takes to be a great leader: A recommended reading list</a></td>
                <td>02/04/2015</td>
                <td><a href="#">Thu-Huong Ha</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">5</th>
                <td><a href="#">Books to help you answer big questions about yourself</a></td>
                <td>05/06/2015</td>
                <td><a href="#">Thu-Huong Ha</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">6</th>
                <td><a href="#">Books worth reading, as recommended by Bill Gates, Susan Cain and more…</a></td>
                <td>12/16/2014</td>
                <td><a href="#">Thu-Huong Ha</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">7</th>
                <td><a href="#">David Kelley: The designs I love most</a></td>
                <td>12/12/2014</td>
                <td><a href="#">Laura McClure</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">8</th>
                <td><a href="#">Why we need creative confidence</a></td>
                <td>10/16/2013</td>
                <td><a href="#">Helen Walters</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">9</th>
                <td><a href="#">A biologist, an engineer, a designer and a musical robot builder walk into a room</a></td>
                <td>12/19/2013</td>
                <td><a href="#">Helen Walters</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
            <tr>
                <th class=""><input type="checkbox" /></th>
                <th scope="row">10</th>
                <td><a href="#">The surprising thing robots can’t do yet: housework</a></td>
                <td>10/02/2014</td>
                <td><a href="#">Thu-Huong Ha</a></td>
                <td class="text-center">
                    <a href=""><i class="fa fa-edit"></i> Edit</a> &nbsp;
                    <a href=""><i class="fa fa-eye"></i> View</a>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-4 dataTables_info"><br/>Showing 1 to 10 of 57 entries</div>
        <div class="col-md-8 text-right">
            <nav>
            <ul class="pagination">
                <li class="disabled">
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li class="active">
                    <a href="#">1</a>
                </li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                </a>
                </li>
            </ul>
            </nav>
        </div>
    </div>
</div>
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('admin-articles') !!}
@endsection

@section('page-scripts')
<script>
    $(function() {       
        $('.article-index input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });

        //Check/uncheck all
        $('#selectAllArticle input[type="checkbox"]').on('ifChecked', function(event){
               $('.article-index input[type="checkbox"]').iCheck('check');
        });
        $('#selectAllArticle input[type="checkbox"]').on('ifUnchecked', function(event){
               $('.article-index input[type="checkbox"]').iCheck('uncheck');
        });
    }); 

</script>	
@endsection