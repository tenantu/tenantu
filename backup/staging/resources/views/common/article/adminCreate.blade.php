@extends('layouts.admin-master')

@section('admin-content')
@if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model('',['method' => 'POST','route'=>['users.updateProfile'],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('title', 'Title:') !!}<span style="color: red;">*</span>
            {!! Form::text('title',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('short_description', 'Short Description:') !!}<span style="color: red;">*</span>
            {!! Form::text('short_description',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description:') !!}<span style="color: red;">*</span>
            {!! Form::textarea('description',null,['class'=>'form-control', 'id' => 'editor']) !!}
        </div>        
        <div class="form-group">
            {!! Form::label('Image', 'Image:') !!}
            {!! Form::file('image',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('location', 'Location:') !!}<span style="color: red;">*</span>
            <ul id="courseLocation">
                <li>dsfdsfds</li>
            </ul>
        </div>
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('admin-profile') !!}
@endsection

@section('page-scripts')
 <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
 <link href="http://localhost/travelportal/public/tag-it/css/jquery.tagit.css" rel="stylesheet" type="text/css"> 
 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
 <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
 <script src="http://localhost/travelportal/public/tag-it/js/tag-it.min.js" type="text/javascript" charset="utf-8"></script>
 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script>
    CKEDITOR.replace('editor');
    $("#courseLocation").tagit({
        allowSpaces: true,
        fieldName: "location[]",
        autocomplete: {
            delay: 0,
            minLength: 2,
            source: function(request, response) {
                var callback = function (predictions, status) {
                    if (status != google.maps.places.PlacesServiceStatus.OK) {
                        return;
                    }                   
                    var data = $.map(predictions, function(item) {
                        return item.description;
                    });
                    response(data);
                }       
                var service = new google.maps.places.AutocompleteService();
                service.getQueryPredictions({ input: request.term }, callback);
            }
        }
    });
    
</script>	
@endsection