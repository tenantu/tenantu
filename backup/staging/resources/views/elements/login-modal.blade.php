<div class="modal fade loginpopup" id="LoginPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title login-heading" id="myModalLabel">LOG IN</h4>
           </div>
            <div class="modal-body login-box">
                 @if ($errors->any() and (Session::has('loginError'))) 
                    <div class="alert alert-danger alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ( $errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                 @endif
                {!! Form::open( ['route' => 'users.login'] ) !!}
                    <div class="form-group">
                        {!! Form::label('Email', 'Email') !!}
                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                   </div>
                    <div class="form-group">
                         {!! Form::label('Password', 'Password') !!}
                         {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <a href="javascript:void(0);" class="forgot-password">Forgot Password?</a>
                        </div>
                        <div class="form-group col-sm-6 text-right">
                            {!! Form::submit('Log In', ['class' => 'btn btn-primary btn-default col-md-4 pull-right']) !!}
                        </div>
                    </div>
               {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>