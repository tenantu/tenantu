<div id="push_sidebar">
			<ul class="right-menu">
				<li><a href="{{ route('index.index') }}">Home</a></li>
				<li><a href="{{route('schools.getSchools')}}">Schools</a></li>
				<li><a href="{{route('index.login')}}">Login/Signup</a></li>
				<li><a href="{{ URL::to('/')}}/blog">Blog</a></li>
				<!--<li><a href="{{route('index.index','#contact')}}">Contact</a></li>-->
				<li><a href="{{route('contactUs')}}">Contact Us</a></li>
			</ul>
			<div class="social-icons">
				<ul>
					<li><a href="{{HTML::getSocialMediaLinks('twitter')}}"><img src="{{ asset('public/img/twitter.png') }}"></a></li>
					<li><a href="{{HTML::getSocialMediaLinks('facebook')}}"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
					<li><a href="{{HTML::getSocialMediaLinks('instagram')}}"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
					<li><a href="{{ route('index.postContactUs') }}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
				</ul>
			</div>
			<div class="address">
				<p>293 Broadway</p>
				<p>Bethpage, New York 11714 </p>
				<div>Tel : (516) 507-9253 </div>
				
			</div>
		</div>
