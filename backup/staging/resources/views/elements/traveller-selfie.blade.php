<section class="traveller-day">
    <div class="row">
        <div class="col-md-3">
            <h2><span>Traveller of the DAY</span></h2>
            <div class="traveller-day-profile">
                <div class="traveller-pic">
                    <img src="{{ asset('public/images/traveller.jpg') }}" class="img-responsive" alt="" width="149" height="149">
                </div>
                <p>Marlene W. Patrick</p>
                <span>1212 Elmwood Avenue <br>Tempe, AZ 85283 </span>
                <a href="#" class="btn btn-default">View Profile</a>
            </div>
        </div>
        @if($selfieOfTheDay != null && count($selfieOfTheDay) >0)
        <div class="col-md-9 selfie-day">
            <h2><span>Selfie of the Day</span></h2>
            <figure>
                <div class="img-wrap" style="max-height: 362px;overflow: hidden;">
                    <a class="fancybox-media" href="{{ asset('public/uploads/images/'). '/' . $selfieOfTheDay->user_id . '/' . $selfieOfTheDay->image }}">
                        <img src="{{ asset('public/uploads/images/'). '/' . $selfieOfTheDay->id . '/' . $selfieOfTheDay->image }}" class="img-responsive" alt="" width="870" height="355">
                    </a>
                </div>
                <figcaption>
                    <h6>{{ $selfieOfTheDay->title }}</h6>
                    <p>{{ $selfieOfTheDay->short_desc }}</p>
                </figcaption>
            </figure>
        </div>
        @endif
    </div>
</section>
@section('page-css')
<!-- FANCYBOX CSS -->
<link href="{{ asset('public/css/jquery.fancybox.css') }}" rel="stylesheet">
@endsection

@section('page-scripts')
<!--  FANCYBOX PLUGIN -->
<script src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
<!-- CUSTOM SCRIPTS REQIRED-->
<script src="{{ asset('public/js/custom.js') }}"></script>
@endsection
