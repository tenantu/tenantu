<!-- Bootstrap core JavaScript
    ================================================== --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
	<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/js/bootstrap-select.js') }}"></script>
	
	<script>
		$(".nav_trigger").click(function() {
			$("body").toggleClass("show_sidebar");
			$(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); // toggle 2 classes in Jquery: http://goo.gl/3uQAFJ - http://goo.gl/t6BQ9Q
		});
		$(function() {
			$(document).on('click','.fb-login',function(){
				var userTypeVal = $('input[name=usertype]:checked', '#loginform').val();
				 if(userTypeVal=='student')
				 {
					 var type=1;
				 }
				 else
				 {
					 var type=2;
				 }
				 
				 var url = "{{ asset('auth/facebook/') }}";
				  url+="/"+type;
				 
				 window.location.href = url;
				
			});
		});
	</script>
	<script src="{{ asset('public/js/jquery.bxslider.js') }}" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function(){
  		$('.bxslider').bxSlider({
  			slideWidth: 240,
    minSlides: 1,
    maxSlides: 4,
    slideMargin: 30
  		});
	});
</script>
	
	
