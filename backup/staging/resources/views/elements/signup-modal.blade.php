<div class="modal fade signuppopup" id="SignupPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title sigup-heading" id="myModalLabel">SIGN UP </h4>
            </div>
            <div class="modal-body sigup-box">
      
             @if(Session::has('message')) 
                <div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <strong>Success!</strong> {{ Session::get('message') }} .
                </div>

             @elseif ($errors->any() and (Session::has('signupError'))) 
                    <div class="alert alert-danger alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ( $errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
            @endif
               {!! Form::model('',['method' => 'POST','route'=>['users.signup'],'files'=>true]) !!}
                    <div class="form-group">
                        {!! Form::text('id',Session::get('token'),['class'=>'form-control']) !!}
                    </div>  
                    <div class="form-group">
                        {!! Form::label('Full Name', 'Full Name:') !!}
                        {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Full Name']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Email', 'Email:') !!}
                        {!! Form::text('email',null,['class'=>'form-control','placeholder'=>'Email' ,'id'=>'inputEmail3']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Password', 'Password:') !!}
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'Password','id'=>'inputPassword3']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Confirm Password', 'Confirm Password:') !!}
                        {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'Confirm Password','id'=>'inputPassword3']) !!}
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <span class="members-login">Already Registered? <a href="javascript:void(0);"> Members Login Here</a></span>
                        </div>
                        <div class="form-group col-sm-6 text-right">
                           {!! Form::button('Sign Up', ['class' => 'btn btn-default signup-btn', 'type' =>'submit']) !!}
                         </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<style>
.btn-default {
    color: #fff !important;
}
</style>