<!-- jQuery 2.1.4 -->
<script src="{{ asset('public/admin/plugins/jQuery/jQuery-2.1.4.min.js') }}" type="text/javascript"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('public/admin/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- iCheck -->

<!-- SlimScroll -->
<script src="{{ asset('public/admin/plugins/slimScroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<!-- FastClick -->
<script src="{{ asset('public/admin/plugins/fastclick/fastclick.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/admin/dist/js/app.min.js') }}" type="text/javascript"></script>


<!-- Select2 -->
<script src="{{ asset('public/admin/plugins/select2/select2.full.min.js') }}" type="text/javascript"></script>
<!-- Jquery Ui -->
<script src="{{ asset('public/admin/plugins/jQueryUI/jquery-ui.min.js') }}" type="text/javascript" charset="utf-8"></script>
<!-- Tag It -->
<script src="{{ asset('public/tag-it/js/tag-it.min.js') }}" type="text/javascript" charset="utf-8"></script>

<!-- Common admin js -->
<script src="{{ asset('public/js/admin.js') }}" type="text/javascript"></script>
<!-- Bootstrap Switch -->
<script src="{{ asset('public/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
<!-- Bootbox Alerts-->
