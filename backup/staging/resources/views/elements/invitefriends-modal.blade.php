<div class="modal fade loginpopup" id="InviteFriends" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title login-heading" id="myModalLabel">Invite Friends</h4>
           </div>
            <div class="modal-body login-box">
            <div id="result"></div>
                {!! Form::open( ['route' => 'users.invitefriends'] ) !!}
                    <div class="form-group">
                        Type email addresses below, separated by commas.
                        {!! Form::textarea('email', null, ['class' => 'form-control', 'placeholder' => 'Email','id'=>'emailFriend']) !!}
                   </div>
                    <div class="row">
                        <div class="form-group col-sm-6 text-right">
                            {!! Form::button('Send Invitations', ['class' => 'btn btn-primary btn-default col-md-6 pull-right','id'=>'referFriend','data-loading-text'=>"Sending Invitation...",'autocomplete'=>"off"]) !!}
                        </div>
                    </div>

               {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
