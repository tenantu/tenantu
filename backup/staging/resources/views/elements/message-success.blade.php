<div class="alert alert-success text-center">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<strong></strong> {{ Session::get('message-success') }}
</div>
