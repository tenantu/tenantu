<div class="container-fluid subpage-top-cntr">
	<div class="container">
	<!-- nav section starts -->
	<!-- //////// nav section ends //////// -->
	<!--- right sidebar comes here -->
	@include('elements.right-sidebar')	

    <!-- nav section ends -->
    <!-- //////// ends //////// --> 

    <!-- man section -->
    <!-- //////// starts //////// -->
    <div id="wrapper">
    	<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 scl-icons">
					<div class="social-icons">
						<ul>
							<li><a href="{{HTML::getSocialMediaLinks('twitter')}}"><img src="{{ asset('public/img/twitter.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('facebook')}}"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('instagram')}}"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
							<li><a href="{{route('contactUs')}}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><a href="{{ route('index.index') }}" class="tenantu-logo"><img src="{{ asset('public/img/subpage-logo.png') }}"></a></div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
					<div class="sub-top-menu">
						<span class="nav_trigger"><i class="fa fa-navicon"></i></span>
					</div>
				</div>
		</div>
		</div>
    </div>
    <!-- man section ends-->
    <!-- //////// ends //////// -->
  </div>
  </div>
