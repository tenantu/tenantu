<div class="alert alert-danger">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<strong>Error!</strong> {{ Session::get('message-error-signup') }}
</div>
