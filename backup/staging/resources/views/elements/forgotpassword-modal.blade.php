<div class="modal fade forgotpopup" id="ForgotPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title forgot-heading" id="myModalLabel">FORGOT PASSWORD</h4>
            </div>
            <div class="modal-body forgot-box">
                
                @if(Session::has('message')) 
                    <div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <strong>Success!</strong> {{ Session::get('message') }} .
                    </div>               
                @elseif ($errors->any() and (Session::has('passwordError'))) 
                    <div class="alert alert-danger alert-dismissable">
                        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                        @foreach ( $errors->all() as $error )
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                 @endif

                 {!! Form::open( ['route' => 'users.postEmail'] ) !!}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Registered Email</label>
                         {!! Form::email('email', null, ['class' => 'form-control', 'id' =>'inputEmail3','placeholder' => 'Email']) !!}
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <span class="remember-pass pull-left">Remember Password? <a href="javascript:void(0);" class="remember-login"> Login Here</a></span>
                        </div>
                        <div class="form-group col-sm-6">
                             {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-default col-md-4 pull-right']) !!}
                        </div>                        
                    </div>
                 {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>