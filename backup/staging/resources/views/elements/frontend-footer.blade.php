	<footer>
  		<div class="container">
			{!! HTML::getBanner(2) !!}
  			
	  		<div class="row">
	  			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		  			<ul class="left-menu">
		  				<li><a href="{{route('index.index')}}">Home</a></li>       
						<li><a href="{{route('schools.getSchools')}}">Schools</a></li>       
						<li><a href="{{route('index.login')}}">Login</a></li>
						<!--<li><a href="{{route('index.index','#contact')}}">Contact</a></li>-->
						<li><a href="{{ route('contactUs') }}">Contact Us</a></li>
						<li><a href="{{route('pages.faq')}}">Faq</a></li>
						<li><a href="{{route('index.banners')}}">Advertisement</a></li>
					</ul>
						{!! HTML::getCmsPages() !!}
				</div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 col-lg-offset-1 col-md-offset-1 footer-logo"><img src="{{ asset('public/img/footer-logo.png') }}"></div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<div class="social-icons">
						<ul>
							<li><a href="{{HTML::getSocialMediaLinks('twitter')}}"><img src="{{ asset('public/img/twitter.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('facebook')}}"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('instagram')}}"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
							<li><a href="{{route('contactUs')}}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
						</ul>
					</div>
					<div class="copyright">©tenantu.com,  All rights reserved</div>
				</div>
			</div>
  		</div>
  		
  	</footer>
  	
