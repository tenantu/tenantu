<meta charset="UTF-8">
<meta name="_token" content="{!! csrf_token() !!}"/>
<title>{{ Config::get('constants.SITE_NAME') }} - {{ $pageTitle or '' }}</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.4 -->
<link href="{{ asset('public/admin/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- kartik-v/bootstrap-fileinput -->
<link href="{{ asset('vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<!-- Select2 -->
<link href="{{ asset('public/admin/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Jquery Ui -->
<link rel="stylesheet" type="text/css" href="{{ asset('public/admin/plugins/jQueryUI/jquery-ui.min.css') }}">
<!-- Tagit -->
<link href="{{ asset('public/tag-it/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css"> 
<!-- Theme style -->
<link href="{{ asset('public/admin/dist/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
<link href="{{ asset('public/admin/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet" type="text/css" />
<!-- iCheck -->
<link href="{{ asset('public/admin/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />
<!-- bootstrap-switch -->
<link href="{{ asset('public/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->