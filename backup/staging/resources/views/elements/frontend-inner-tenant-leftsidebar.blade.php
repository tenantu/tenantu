<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel user-left-profile" onclick="location.href='{{route('tenants.profile')}}';">
			  
            <div class="image">
				
              <img src="{{HTML::getTenantProfileImage()}}" class="img-circle" alt="User Image">
            </div>
            <div class="info">
              <p>{{ Auth::user('tenant')->getFullName()}} </p>
              <a href="{{route('tenants.profile')}}"> Profile</a>
            </div>
            
            </a>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Dashboard</li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'dashboard')}} treeview">
              <a href="{{ route('tenants.dashboard') }}">
                <i class="fa fa fa-user"></i>
                <span>My Account</span>
                
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'mysearch')}}">
              <a href="{{route('search.getMySearchDetails')}}">
                <i class="fa fa-search"></i> <span>My Search</span> 
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'recentproperties')}} treeview">
              <a href="{{ route('tenants.getRecentProperties') }}">
                <i class="fa fa-history"></i>
                <span>Recently Viewed</span>
              </a>
            </li>
           <li class="{{HTML::getTenantMenuClass($pageMenuName,'mylandlord')}} treeview">
              <a href="{{ route('tenants.getMyLandlords') }}">
                <i class="fa fa-building"></i> <span>My Landlord</span>
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'mypayment')}} treeview">
              <a href="{{ route('payment.getMyPayment') }}">
                <i class="fa fa-paypal"></i> <span>My Payments</span>
              </a>
            </li>
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'mycontact')}} treeview">
              <a href="{{ route('tenants.getContact') }}">
                <i class="fa fa-users"></i> <span>My Contacts </span>
              </a>
            </li>
            
            <li class="{{HTML::getTenantMenuClass($pageMenuName,'changepassword')}} treeview">
              <a href="{{ route('tenants.getChangePassword') }}">
                <i class="fa fa-lock"></i> <span>Change Password</span>
              </a>
            </li>
            
            <li class="treeview">
              <a href="{{ route('tenants.logout') }}">
                <i class="fa fa-sign-out"></i> <span>Sign out</span>
              </a>
            </li>
            
            
           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
