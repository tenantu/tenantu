<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
               <img src="{{ HTML::getAdminImage()}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ ucwords(Auth::user('admin')->name) }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li {{ setActive('admin/dashboard') }}>
                <a href="{{ route('admins.dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
            
            <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.trend')}}"><i class="fa fa-file-o"></i> <span>User activity/Trends</span> <i class="fa fa-angle-left pull-right"></i></a>
            </li>
           
             
             

            <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-users"></i> <span>Tenants</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{ url('admin/tenants') }}"><i class="fa fa-circle-o"></i>Tenantu</a>
                    </li>
                    <li {{ setActive('admin/tenants/create') }}>
                        <a href="{{ url('admin/tenants/create') }}"><i class="fa fa-circle-o"></i>Add Tenantu</a>
                    </li>
                </ul>
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-building-o"></i> <span>Landlords</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{ url('admin/landlords') }}"><i class="fa fa-circle-o"></i>Landlord</a>
                    </li>
                    <li {{ setActive('admin/tenants/create') }}>
                        <a href="{{ url('admin/landlords/create') }}"><i class="fa fa-circle-o"></i>Add Landlord</a>
                    </li>
                </ul>
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa-graduation-cap"></i> <span>Schools</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{ url('admin/schools') }}"><i class="fa fa-circle-o"></i>school</a>
                    </li>
                    <li {{ setActive('admin/tenants/create') }}>
                        <a href="{{ url('admin/schools/create') }}"><i class="fa fa-circle-o"></i>Add school</a>
                    </li>
                </ul>
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-building"></i> <span>Properties</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{route('admins.properties')}}"><i class="fa fa-circle-o"></i>Properties</a>
                    </li>
                    <!--<li {{ setActive('admin/tenants/create') }}>
                        <a href="#"><i class="fa fa-circle-o"></i>Add property</a>
                    </li>-->
                </ul>
            </li>
            
            <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-building"></i> <span>Review/Rating</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{route('admins.propertyreview')}}"><i class="fa fa-circle-o"></i>Property review/rating</a>
                    </li>
                    <!--<li {{ setActive('admin/tenants/create') }}>
                        <a href=""><i class="fa fa-circle-o"></i>Add property</a>
                    </li>-->
                </ul>
            </li>
            
            <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-credit-card"></i> <span>Payment Details</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{route('admins.getAllPayment')}}"><i class="fa fa-circle-o"></i>Pay details</a>
                    </li>
                    
                </ul>
            </li>
            
            <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-credit-card"></i> <span>Testimonials</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{ route('admins.feedback') }}"><i class="fa fa-circle-o"></i>Testimonials</a>
                    </li>
                    <li {{ setActive('admin/tenants/create') }}>
                        <a href="{{route('feedback.create')}}"><i class="fa fa-circle-o"></i>Add testimonials</a>
                    </li>
                </ul>
            </li>
            
            
            
             <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-file-o"></i> <span>Static Pages</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{route('admins.cms')}}"><i class="fa fa-circle-o"></i>Pages</a>
                    </li>
                    <li {{ setActive('admin/tenants/create') }}>
                        <a href="{{route('pages.create')}}"><i class="fa fa-circle-o"></i>Add Pages</a>
                    </li>
                </ul>
            </li>
                 <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-file-o"></i> <span>FAQ</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{route('admins.faq')}}"><i class="fa fa-circle-o"></i>FAQ's</a>
                    </li>
                    <li {{ setActive('admin/tenants/create') }}>
                        <a href="{{route('faq.create')}}"><i class="fa fa-circle-o"></i>Add FAQ</a>
                    </li>
                </ul>
            </li>
               <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-building"></i> <span>Amenities</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{ route('admins.amenities') }}"><i class="fa fa-circle-o"></i>Amenities</a>
                    </li>
                    <!--<li {{ setActive('admin/tenants/create') }}>
                        <a href="#"><i class="fa fa-circle-o"></i>Add property</a>
                    </li>-->
                </ul>
            </li>
            <li {{ setActiveList('admin/users') }}>
                <a href="#"><i class="fa fa-file-o"></i> <span>Contact Us</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li {{ setActive('admin/tenants') }}>
                        <a href="{{route('admins.contactUsers')}}"><i class="fa fa-circle-o"></i>Users</a>
                    </li>
                    
                </ul>
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.enquiries')}}"><i class="fa fa-file-o"></i> <span>Be the first to find out</span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.signinlandlord')}}"><i class="fa fa-file-o"></i> <span>Sign in as a landlord</span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            
            <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.socialmedia')}}"><i class="fa fa-file-o"></i> <span>Social media links</span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.bannerSettings')}}"><i class="fa fa-file-o"></i> <span>Banner settings </span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.bannerPurchases')}}"><i class="fa fa-file-o"></i> <span>Banner Purchases </span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            
            <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.featuredPropertySettings')}}"><i class="fa fa-file-o"></i> <span>Featured Property settings </span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            
             <li {{ setActiveList('admin/users') }}>
                <a href="{{route('admins.featuredPurchases')}}"><i class="fa fa-file-o"></i> <span>Featured Properties </span> <i class="fa fa-angle-left pull-right"></i></a>
                
            </li>
            
       
            <li {{ setActive('admin/logout') }}>
                <a href="{{ route('admins.logout') }}"><i class="fa fa-sign-out"></i> <span>Log Out</span></a>
            </li>
            <li>&nbsp;</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
