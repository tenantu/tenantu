<!DOCTYPE html>
<html>
<head>
	@include('elements.inner-head')
</head>	
  <body class="hold-transition skin-green sidebar-mini fixed">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{ route('index.index') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="{{ asset('public/img/landlord-small-logo.png') }}" class="img-responsive"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="{{ asset('public/img/landlord-subpage-logo.png') }}"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <!--Here the macro will come the genrale messages are pulled from macro-->
            {!!HTML::getLandLordGeneralMessageNotification()!!}
              <!-- Notifications: style can be found in dropdown.less -->
             <!--  <li class="dropdown notifications-menu">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 <i class="fa fa-bell-o"></i>
                 <span class="label label-warning">10</span>
               </a>
               <ul class="dropdown-menu">
                 <li class="header">You have 10 notifications</li>
                 <li>
                   inner menu: contains the actual data
                   <ul class="menu">
                     <li>
                       <a href="#">
                         <i class="fa fa-users text-aqua"></i> 5 new members joined today
                       </a>
                     </li>
                     <li>
                       <a href="#">
                         <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                       </a>
                     </li>
                     <li>
                       <a href="#">
                         <i class="fa fa-users text-red"></i> 5 new members joined
                       </a>
                     </li>
                     <li>
                       <a href="#">
                         <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                       </a>
                     </li>
                     <li>
                       <a href="#">
                         <i class="fa fa-user text-red"></i> You changed your username
                       </a>
                     </li>
                   </ul>
                 </li>
                 <li class="footer"><a href="#">View all</a></li>
               </ul>
             </li> -->
              <!-- Tasks: style can be found in dropdown.less -->
             {!!HTML::getLandLordIssueMessageNotification()!!}
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{ $profileImage }}" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{ $profile->firstname.' '. $profile->lastname }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{ $profileImage }}" class="img-circle" alt="User Image">
                    <p>
                      {{ Auth::user('landlord')->getFullName()}} 
                      <small>Member since {{Auth::user('landlord')->getCreatedAt()}}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ route('landlords.profile')}}" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{route('landlords.logout')}}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      @include('elements.left-area-landlord')

      <!-- Content Wrapper. Contains page content -->
      @yield('inner-content')
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <span>All rights reserved.</span>
        </div>
        Copyright Tenantu © 2014-2015
      </footer>

      
    </div><!-- ./wrapper -->
	@include('elements.inner-landlord-script')
  @yield('page-scripts') 
  </body>
</html>
