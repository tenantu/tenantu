<!DOCTYPE html>
<html lang="en">
<head>
	@include('elements.frontend-head')
	<!-- page specific css -->
        @yield('page-css')
</head>
<body class="subpage">
	<!--  nonlogged header will comes here -->
	@include('elements.non-logged-header')

	<!-- sign up -->
	<!-- //////// starts //////// -->
	<!-- Sign up starts here  -->
	@yield('content')
  	<!--- footer page starts here -->
  	@include('elements.frontend-footer')
	</div>

	<!-- Bootstrap core JavaScript
    ================================================== --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	
	@include('elements.frontend-scripts')
	
</body>
</html>
