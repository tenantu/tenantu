<!DOCTYPE html>
<html>
    <head>
        <!-- metas & css -->
        @include('elements.head')

        <!-- page specific css -->
        @yield('page-css')

        <script>
            var baseUrlPath = '{{ asset('public') }}' ;
        </script>
    </head>
    <body class="skin-blue sidebar-mini">
        <!-- Site wrapper -->
        <div class="wrapper">

            <!-- header -->
            @include('elements.admin-header')

            <!-- Left side column. contains the sidebar -->
            @include('elements.admin-sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">

                    <h1>{{ $pageHeading or '' }} <small>{{ $pageDesc or '' }}</small></h1>

                    <!-- page specific breadcrumbs -->
                    @yield('breadcrumbs')

                </section>

                <!-- Main content -->
                <section class="content">

                    <!-- Default box -->
                    <div class="box">
                        <div class="box-header with-border">                            
                            <h3 class="box-title">
                                <!-- page specific back button -->
                                @yield('back-btn-top')
                            </h3>                            
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i>
                                </button>
                                <button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            <!-- page specific contents -->
                            @yield('admin-content')
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <h3 class="box-title"><!-- Footer --></h3>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /.box -->

                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- footer -->
            @include('elements.admin-footer')

        </div>
        <!-- ./wrapper -->

        <!-- scripts -->
        @include('elements.scripts')

        <!-- page specific scripts -->
        @yield('page-scripts')

    </body>
</html>