
<table style="background-color: #f6f6f6; width: 100%; font-family:arial, sans-serif; font-size:14px; line-height:20px; color:#737373;">
    <tr>
        <td></td>
        <td width="600" style="display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;">
            <div style="max-width: 600px; margin: 0 auto; display: block; padding: 20px;">
                <table width="100%" cellpadding="0" cellspacing="0" style="background: #fff; border: 1px solid #e9e9e9; border-radius: 3px;">
                    <tr>
                        <td style="padding: 20px;">
                            <table  cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <img class="img-responsive" src="{{ asset('public/img/header-logo.png') }}"/>
                                    </td>
                                </tr>
                                @yield('email-content')
                                
                              </table>
                        </td>
                    </tr>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tr>
                            <td style="padding: 0 0 20px;" align="center"><a href="#" style="color:#737373;">@tenantu.com</a></td>
                        </tr>
                    </table>
                </div></div>
        </td>
        <td></td>
    </tr>
</table>

