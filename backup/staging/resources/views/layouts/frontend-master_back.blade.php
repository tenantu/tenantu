<!DOCTYPE html>
<html>
    <head>
        <!-- metas & css -->
        @include('elements.frontend-head')

        <!-- page specific css -->
        @yield('page-css')

        <script>
            var baseUrlPath = '{{ asset('public') }}' ;
        </script>
    </head>
    <body>

        <!-- header -->
        @include('elements.frontend-header')

        <!-- banner -->
        @include('elements.frontend-banner')

        <!-- contents -->
        <section class="contents">

            <div class="container">

                <!-- page specific contents -->
                @yield('content')

            </div>

            <!-- clients -->
            @include('elements.frontend-clients')

            <!-- footer -->
            @include('elements.frontend-footer')

        </section>

        <!-- login modal -->
        @include('elements.login-modal')

        <!-- forgotpassword modal -->
        @include('elements.forgotpassword-modal')

        <!-- signup modal -->
        @include('elements.signup-modal')

        <!-- Invite Friends modal -->
         @include('elements.invitefriends-modal')

        <!-- scripts -->
        @include('elements.frontend-scripts')

        <!-- page specific scripts -->
        @yield('page-scripts')

    </body>
</html>
