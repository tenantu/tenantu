<!DOCTYPE html>
<html lang="en">
<head>
	@include('elements.frontend-head')
	<!-- page specific css -->
</head>
<body class="home" style="background-image:url('{{$schoolImage }}')">
	<div class="{{ $schoolDiv }}" id="school_div" ></div>
	
	@yield('content')
	@include('elements.frontend-footer')

	<!--- scripts----->
	@include('elements.frontend-scripts')
	<!-- page specific scripts -->
   @yield('page-scripts')     
</body>
</html>
