<!DOCTYPE html>
<html lang="en">
<head>
	@include('elements.frontend-head')
	<!-- page specific css -->
        @yield('page-css')
</head>
<body class="inner-subpage">
	
	@include('elements.non-logged-search-header')

	@yield('content')
	
	<!-- footer -->
	<!-- //////// starts//////// -->
  	@include('elements.frontend-footer')
  	<!-- footer -->
	<!-- //////// ends//////// -->
	</div>

	<!-- Bootstrap core JavaScript
    ================================================== --> 
	<!-- Placed at the end of the document so the pages load faster --> 
	@include('elements.frontend-search-scripts')
	@yield('page-scripts')
	@yield('page-css')

</body>
</html>
