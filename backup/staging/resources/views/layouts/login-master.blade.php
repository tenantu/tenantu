<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- metas & css -->
        @include('elements.head')
    </head>
    <body class="login-page">

        <!-- page content -->
        @yield('content')

        <!-- scripts -->
        @include('elements.scripts')

        <!-- page specific scripts -->
        @yield('page-scripts')

    </body>
</html>