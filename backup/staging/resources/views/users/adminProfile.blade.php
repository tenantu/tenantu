@extends('layouts.admin-master')
@section('admin-content')

    @if ($errors->any())
        @include('elements.validation-message')
    @endif

    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif

    {!! Form::model($user,['method' => 'POST','route'=>['users.adminUpdate',$user->id],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('Name', 'Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('name',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Image', 'Image:') !!}<span style="color: red;">*</span>
            {!! Form::file('image', ['class'=>'form-control', 'id' => 'image']) !!}

            @if ($user->image)
                {!! Form::hidden('image') !!}
            @endif
        </div>

        <div class="form-group">
            {!! Form::label('gender', 'Gender:') !!}
             Male
            {!! Form::radio('gender', 'M', null, ['class' => 'i-radio']) !!}
            Female
            {!! Form::radio('gender', 'F', null, ['class' => 'i-radio']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('DOB', 'DOB:') !!}<br>
            {!! Form::selectDays('day',['class'=>'col-md-3']) !!}
            {!! Form::selectMonths('month',['class'=>'col-md-4']) !!}
            {!! Form::selectYears('year',['class'=>'col-md-4']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Location', 'Location:') !!}
            <ul id="location">
                @if (count($locations))
                    @foreach ($locations as $location)
                        <li>{{ $location }}</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="form-group">
            {!! Form::label('Description', 'Description:') !!}
            {!! Form::textarea('description',null,['class'=>'form-control ckeditor']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Personal Link', 'Personal Link:') !!}
            {!! Form::text('personal_link',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
    <script>
        // page specific varaiables.
        var currentlyValidTags;
        var imageUrl = '{{ $imageUrl }}/{{ $user->id }}/{{ $user->image }}';
        var image    = '{{ $user->image }}';
        var id       = '{{ $user->id }}';
        var ajaxUrl  = '{{ route('users.deleteImage') }}';        
    </script>
    <script src="{{ asset('public/js/users/admin-profile.js') }}" type="text/javascript"></script>
@endsection
