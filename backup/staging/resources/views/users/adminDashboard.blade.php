@extends('layouts.admin-master')

@section('admin-content')
	Welcome {{ ucwords(Auth::user()->name) }}, <br/>
    You can use the navigation links on the left navigation bar to manage your account.
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('admin-dashboard') !!}
@endsection	