@extends('layouts.admin-master')

@section('breadcrumbs')
    {!! Breadcrumbs::render('admin-profile-change-password') !!}
@endsection

@section('admin-content')
            
    @if (count($errors) > 0)
        @include('elements.validation-message')
    @endif

    @if (Session::has('message-success'))
        @include('elements.message-success')
    @endif

    @if (Session::has('message-error'))
        @include('elements.message-error')
    @endif

    {!! Form::open( ['route' => 'users.postChangePassword'] ) !!}
        
        <div class="form-group has-feedback">
            {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => 'Current Password']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
       
        <div class="form-group has-feedback">
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'New Password']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password Confirmation']) !!}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        
        <div class="row">
            <div class="col-xs-4">
                {!! Form::submit('Change Password', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>

    {!! Form::close() !!}          
      
@endsection