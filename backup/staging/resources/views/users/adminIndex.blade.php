@extends('layouts.admin-master')
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin-users') !!}
@endsection
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="row">
        {!! Form::model($search, ['method' => 'GET','route'=>['users.adminIndex']]) !!}
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                    {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>

            </div>
        </div>
        {!! Form::close() !!}
        <div class="col-md-2 pull-right">
            <br/>
            <a class="btn btn-block btn-primary" href="{{ route('users.adminCreate') }}">
                <i class="fa fa-plus-circle"></i> &nbsp;New User
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
            <div class="input-group-btn">
                <button data-toggle="dropdown" class="btn btn-danger dropdown-toggle" type="button">Action <span class="fa fa-caret-down"></span></button>
                <ul class="dropdown-menu">
                    <li>
                        {!! Form::open(['method' => 'POST','route'=>['users.adminBulkDestroy'], 'name' => 'bulk-delete']) !!}
                            {!! Form::hidden('selected-users') !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-link', 'onclick'=>"return confirm('Are you sure you want to delete this item?');"]) !!}
                        {!! Form::close() !!}
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-8 text-right">
        {!! $users->appends(['search' => $search, 'sortby' => $sortby, 'order' => ($order == 'desc')?'asc':'desc'])->render() !!}
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                <th class="">
                    <div id="selectAllUser">
                        <input type="checkbox" name="selected-user" value="" />
                    </div>
                </th>
                <th class="">#</th>
                <th class="col-md-4">
                    @if ($sortby == 'name')
                        <a href="{{ url('admin/users/index?sortby=name&order=' . $order .'&search=' . $search) }}">Name
                            <i class="pull-right fa fa-fw fa-sort-alpha-{!!($order == 'desc')?'asc':'desc'!!}"></i>
                        </a>
                    @else
                        <a href="{{ url('admin/users/index?sortby=name&order=asc' .'&search=' . $search ) }}">Name</a>
                    @endif
                </th>
                <th class="col-md-4">
                @if ($sortby == 'email')
                        <a href="{{ url('admin/users/index?sortby=email&order=' . $order .'&search=' . $search) }}">Email
                            <i class="pull-right fa fa-fw fa-sort-alpha-{!!($order == 'desc')?'asc':'desc'!!}"></i>
                        </a>
                    @else
                        <a href="{{ url('admin/users/index?sortby=email&order=asc' .'&search=' . $search ) }}">Email</a>
                    @endif</th>
                <th class="col-md-1">
                @if ($sortby == 'is_active')
                        <a href="{{ url('admin/users/index?sortby=is_active&order=' . $order .'&search=' . $search ) }}">Status
                            <i class="fa fa-fw fa-sort-alpha-{!! $order !!}"></i>
                        </a>
                    @else
                        <a href="{{ url('admin/users/index?sortby=is_active&order=asc' .'&search=' . $search ) }}">Status</a>
                    @endif</th>
                <th class="col-md-3 text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
$page = $users->currentPage();
$slNo = (($page - 1) * 10) + 1;
?>
        @foreach ($users as $user)
            <tr>
                <th class=""><input type="checkbox" name="user[]" value="{{$user->id}}" /></th>
                <th scope="row">{{$slNo}}</th>
                <td><a href="#">{{$user->name}}</a></td>
                <td>{{$user->email}}</td>
                <td>
                @if ($user->is_active == 0)
                    <a href="{{ url('admin/users/change_status/' . $user->id) }}"><small class="label label-danger"> In Active </small></a>
                @else
                    <a href="{{ url('admin/users/change_status/' . $user->id) }}"><small class="label label-success"> Active </small></a>
                @endif
                </td>
                <td class="text-center">
                     {!! Form::open(['method' => 'DELETE', 'route' => ['users.adminDestroy', $user->id]]) !!}
                        <a href="{{ url('admin/users/edit/'.$user->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a> &nbsp;
                        <button type="submit" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash"></i> Delete</a></button>
                    {!! Form::close() !!}
                </td>
            </tr>
            <?php
$slNo++;
?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * 10) + 1}} to {{ $slNo-1 }} of {!! $users->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $users->render()) !!}
        </div>
    </div>
    </div>
</div>
@endsection


@section('page-scripts')
<script>
    $(function() {
        $('.users-index input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '20%'
        });
        var selected_user = [];
        //selected_user.length = 0;

        // Check/Uncheck all
        $('#selectAllUser input[type="checkbox"]').on('ifChecked', function(event){
            $('.users-index input[type="checkbox"]').iCheck('check');
        });
        $('#selectAllUser input[type="checkbox"]').on('ifUnchecked', function(event){
            $('input[type="hidden"][name="selected-users"]').val('');
            selected_user.length = 0;
            $('.users-index input[type="checkbox"]').iCheck('uncheck');
        });

        // Check/Uncheck individually
        $('input[type="checkbox"]').on('ifChecked', function(event){
            if($(this).val() !='')
                selected_user.push($(this).val());
            $('input[type="hidden"][name="selected-users"]').val(selected_user);

        });
        $('input[type="checkbox"]').on('ifUnchecked', function(event){
            selected_user.splice(selected_user.indexOf($(this).val()),1);
            $('input[type="hidden"][name="selected-users"]').val(selected_user);
        });
    });

</script>
@endsection