@extends('layouts.frontend-master')

@section('breadcrumbs')
    {!! Breadcrumbs::render('home-profile-edit') !!}
@endsection

@section('content')<br>
  @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


<div class="container inner-contents">
    <div class="row">
        @if (Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {{ Session::get('message') }}
                </div>
        @endif
        <div class="col-md-3 col-sm-3 profile-section">
         @if ($user->image !='')
            <img src="{{ asset('public/uploads/users/'.$user->id.'/'.$user->image) }}" class="img-responsive thumbnail" alt="">
         @else
            <img src="{{ asset('public/images/default-profile.png')}}" class="img-responsive thumbnail" alt="User Image" />
        @endif

            <div class="follow">Followers: {!! $user_followers !!}</div>
            <div class="follow">Following: {!! $user_following !!}</div>

            <div class="follow">Credit Points : {!! $user_points !!}</div>
            <span class="signup" data-toggle="modal" data-target="#InviteFriends">
                <a class="btn btn-default">Invite Friends</a>
            </span>
        </div>
        <div class="col-md-9 col-sm-9">
            <div class="tabs-wrap">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#aboutme" aria-controls="aboutme" role="tab" data-toggle="tab">
                    @if(Request::is('users/dashboard'))
                        About Me
                    @else
                        Edit Profile
                        </a>
                    </li>
                     @endif
                    <li role="presentation"><a href="#published-articles" aria-controls="published-articles" role="tab" data-toggle="tab">Published Articles</a>
                    </li>
                    <li role="presentation"><a href="#images-uploaded" aria-controls="images-uploaded" role="tab" data-toggle="tab">Images Uploaded</a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="aboutme">
                        @if(Request::is('users/dashboard'))
                        <a href="{{ route('users.editprofile') }}" class="btn btn-primary pull-right">Edit Profile</a>
                        <div class="clearfix"></div>
                        <h4>{!! $user->name !!}</h4>
                         <h4>{!! $user->location !!}</h4></br>
                        {!! $user->description !!}
                        @else
                            <a href="{{ route('users.dashboard') }}" class="btn btn-primary pull-right">About Me</a>
                            <div class="clearfix"></div>
                            @include('users.editprofiles')
                        @endif
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="published-articles">
                        <div class="row">
                            <div class="col-md-3 text-left">
                                {!! $articles->appends(['tab' => 'published-articles'])->render() !!}
                            </div>
                            <div class="col-md-3 pull-right">
                                <a class="btn btn-block btn-primary" href="{{ route('posts.create') }}">
                                    <i class="fa fa-plus-circle"></i> &nbsp;New Article
                                </a>
                            </div>
                        </div>
                        @foreach ($articles as $article)
                            <div class="media small-article">
                                <div class="media-left">
                                    <div class="img-wrap" style="max-height: 90px;overflow: hidden;">
                                        <img src="{!! asset('public/uploads/articles/') . "/" . $article->id . "/" . $article->image  !!}" class="img-responsive media-object" alt="" width="118" height="90">
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h3><a href="{{ route('posts.show', ['id' => $article->id, 'slug' => $article->slug]) }}"> {!! $article->title !!}</a></h3>
                                    <p>{!! $article->short_desc !!}</p>
                                    @if(Auth::check() && Auth::user()->id == $article->user_id)
                                         {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $article->id]]) !!}
                                            <a href="{{ route('posts.edit', ['id' => $article->id, 'slug' => $article->slug]) }}" class="btn-link"><i class="fa fa-edit"></i>Edit</a>
                                            <button type="submit" class="btn-link" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i> Delete</a></button>
                                        {!! Form::close() !!}
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="images-uploaded">
                        <div class="row">
                            {!! Form::open( ['url' => 'images/store','files' => true] ) !!}
                                <div class="col-md-6">
                                        <div class="form-group">
                                            {!! Form::label('title', 'Image Title') !!} <span style="color: red;">*</span>
                                            {!! Form::text('title',null,['class'=>'form-control counter', 'placeholder'=>'Image Title', 'maxlength' => 140]) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('short_desc', 'Discription (100)') !!} <span style="color: red;">*</span>
                                            {!! Form::textarea('short_desc',null,['class'=>'form-control counter', 'maxlength' => 100, 'rows'=>2, 'placeholder'=>'Discription']) !!}
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    @if ($parentCategories->count())
                                        @foreach ($parentCategories as $parentCategory)
                                            {{--*/ $options = App\Http\Models\Category::findListByParentId($parentCategory->category->id) /*--}}
                                            <div class="form-group">
                                                {!! Form::label('category_id', 'Choose Option') !!}
                                                <span style="color: red;">*</span><br/>
                                                {!! Form::select('category_id', $options, null, ['class'=>'form-control', 'placeholder' => ' -- Choose One -- ',
                                                ]) !!}
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="form-group">
                                        {!! Form::file('image', ['class'=>'form-control', 'id' => 'image']) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::submit('Upload Image', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="image-wrap media small-article">
                            <div class="row text-center isotope" id="portfolio-div">
                                 @foreach($images as $image)
                                    <div class="col-md-4 col-sm-4 cat_{!! $image->category_id !!} isotope-item isotope-hidden">
                                        <div class="photo-wrap">
                                            <a class="fancybox-media" title="{!! $image->title !!}" href="{{ asset('public/uploads/images/'). '/' . $image->id . '/' . $image->image }}">
                                                <img src="{{ asset('public/uploads/images/'). '/' . $image->id . '/' . $image->image }}" class="img-responsive " alt="">
                                            </a>
                                            <h4>{!! $image->title !!}</h4>
                                            <h5>{!! $image->short_desc !!}</h5>
                                            <p>{{ $image->category->name }}</p>
                                            @if(Auth::check() && Auth::user()->id == $image->user_id)
                                                {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $image->id]]) !!}
                                                <a href="{{ route('posts.edit', ['id' => $image->id, 'slug' => $image->slug]) }}" class="btn-link"><i class="fa fa-edit"></i>Edit</a>
                                                <button type="submit" class="btn-link" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i> Delete</a></button>
                                                {!! Form::close() !!}
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 pull-right">
                                {!! $images->appends(['tab' => 'images-uploaded'])->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-css')
<!-- FANCYBOX CSS -->
<link href="{{ asset('public/css/jquery.fancybox.css') }}" rel="stylesheet">
@endsection

@section('page-scripts')
<!--  FANCYBOX PLUGIN -->
<script src="{{ asset('public/js/jquery.fancybox.js') }}"></script>
<!-- ISOTOPE SCRIPTS -->
<script src="{{ asset('public/js/jquery.isotope.js') }}"></script>
<!-- CUSTOM SCRIPTS REQIRED-->
<script src="{{ asset('public/js/custom.js') }}"></script>
<!-- https://github.com/CreativeDream/jquery.filer -->
<link href="{{ asset('public/css/jquery.filer.css') }}" rel="stylesheet">
@endsection

<script>
var $container = $('#portfolio-div');
$container.isotope({
    filter: '*',
    animationOptions: {
        duration: 750,
        easing: 'linear',
        queue: false
    }
});
// tabs
@if($activeTab != '')
    $('.nav-tabs a[href="#{!! $activeTab !!}"]').tab('show')
    $container.isotope('reLayout');
@endif

$('.nav-tabs a[href="#images-uploaded').on('shown.bs.tab', function (e) {
    e.target // newly activated tab
    $container.isotope('reLayout');
})
// something wrong with template rendering. Please check with the actual template.
// some code removed to render template.
$(document).ready(function() {
    $("#image").fileinput({
        showUpload: true,
        showRemove: false,
        allowedFileExtensions:['jpg', 'gif', 'png'],
    });


});

</script>
@endsection
