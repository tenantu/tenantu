@extends('layouts.admin-master')

@section('breadcrumbs')
    {!! Breadcrumbs::render('admin-user-new') !!}
@endsection

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model('',['method' => 'POST','route'=>['users.adminStore'],'files'=>true]) !!}

         <div class="form-group">
            {!! Form::label('user_id', 'Select Role:') !!}
            <span style="color: red;">*</span><br/>
            {!! Form::select('role_id',['0' => 'Please Select']+$role, 3, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Name', 'Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('name', Input::old('name'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Email', 'Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Confirm Password', 'Confirm Password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
            {!! Form::text('email', Input::old('name'), ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('Image', 'Image:') !!}
            {!! Form::file('image', ['class'=>'form-control', 'id' => 'image']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('gender', 'Gender:') !!}
             Male
            {!! Form::radio('gender', 'M', 'M', ['class'=>'iradio_minimal-blue']) !!}
            Female
            {!! Form::radio('gender', 'F', null, ['class'=>'iradio_minimal-blue']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('DOB', 'DOB:') !!}<br>
            {!! Form::selectRange('day', 1, 31, Input::old('day'), ['class'=>'col-md-3']) !!}
            {!! Form::selectMonth('month', Input::old('month'), ['class'=>'col-md-4']) !!}
            {!! Form::selectYear('year', '1950', date("Y"), Input::old('year'), ['class'=>'col-md-4']) !!}
         </div>
        <div class="form-group">
            {!! Form::label('Location', 'Location:') !!}
            <ul id="location">
                @if (count(Input::old('locations')))
                    @foreach (Input::old('locations') as $location)
                        <li>{!! $location !!}</li>
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="form-group">
            {!! Form::label('Description', 'Description:') !!}
            {!! Form::textarea('description', Input::old('description'), ['class'=>'form-control', 'id' => 'editor']) !!}
        </div>
         <div class="form-group">
            {!! Form::label('Personal Link', 'Personal Link:') !!}
            {!! Form::text('personal_link', Input::old('personal_link'), ['class'=>'form-control']) !!}
        </div>
          <div class="form-group">
            {!! Form::label('is_active', 'Status:') !!}
            {!! Form::checkbox('is_active', 1, Input::old('is_active'), ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<script>
    CKEDITOR.replace('editor');
     $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('input[type="radio"]').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });

    });
    $("#image").fileinput({showUpload: false});

      $("#location").tagit({
            allowSpaces: true,
            fieldName: "locations[]",
            requireAutocomplete: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function(request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function(item) {
                            return item.description;
                        });
                        currentlyValidTags = data;
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({ input: request.term }, callback);
                }
            }
        });
</script>
@endsection
