@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	
	@include('elements.breadcrump-landlord')
	<!-- Main content -->
	<section class="content">
		<div class="payments">
			<h2>Payment Options</h2>
			<div class="row">
				<p>Below you will need to input your credentials till the blank field requirements are satisfied. Upon completion, please hit the “Save” button to finalize the transaction.</p>
			</div>
			<div class="row">
				<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
					 @if(Session::has('displayBank'))
					 		@include('elements.validation-message')
           @endif      
           @if (Session::has('message-success') && Session::has('successBank'))
              @include('elements.message-success')
           @endif
	          
					<h3><i class="fa fa-money"></i> Bank Account</h3>
					{!! Form::model('',['method' => 'POST','class'=>'edit-property-details row','id'=>'tenantAddForm','route'=>['payments.postaddbank']]) !!}
					<div class="form-group col-sm-12 col-xs-12">
              {!! Form::label('bank_name', 'Bank name:') !!}<span style="color: red;">*</span>
              {!! Form::text('bank_name', isset($paymentSettings->bank_name) ? $paymentSettings->bank_name:"", ['class'=>'form-control','placeholder'=>'Bank Name']) !!}
						</div>
						<div class="form-group col-sm-12 col-xs-12">
              {!! Form::label('unique_code', 'Routing Code:') !!}<span style="color: red;">*</span>
              {!! Form::text('unique_code', isset($paymentSettings->unique_code) ? $paymentSettings->unique_code:"", ['class'=>'form-control','placeholder'=>'Routing Code']) !!}
						</div>
						<div class="form-group col-sm-12">
							{!! Form::label('account_no', 'Account Number:') !!}<span style="color: red;">*</span>
              {!! Form::text('account_no', isset($paymentSettings->account_no) ? $paymentSettings->account_no:"", ['class'=>'form-control','placeholder'=>'Account Number']) !!}
						</div>
						<div class="form-group col-sm-6">
              {!! Form::label('first_name', 'First Name:') !!}<span style="color: red;">*</span>
              {!! Form::text('first_name', (isset($paymentSettings->first_name) && ($paymentSettings->first_name!="")) ? $paymentSettings->first_name: $loggedLandlord->firstname, ['class'=>'form-control','placeholder'=>'First Name']) !!}
						</div>
						<div class="form-group col-sm-6">
              {!! Form::label('last_name', 'Last Name:') !!}<span style="color: red;">*</span>
              {!! Form::text('last_name', (isset($paymentSettings->last_name) && ($paymentSettings->last_name!="")) ? $paymentSettings->last_name : $loggedLandlord->lastname, ['class'=>'form-control','placeholder'=>'Last Name']) !!}
						</div>
						<div class="form-group col-sm-12 col-xs-12">
              {!! Form::label('address', 'Address:') !!}<span style="color: red;">*</span>
              {!! Form::textarea('address', (isset($paymentSettings->address) && ($paymentSettings->address!="")) ? $paymentSettings->address : $loggedLandlord->address, ['class'=>'form-control','placeholder'=>'Address']) !!}
						</div>
						<div class="form-group col-sm-12 col-xs-12">
              {!! Form::label('type', 'Make as default payment method:') !!}<span style="color: red;">*</span>
              {!! Form::checkbox('type', 2,((isset($paymentSettings->type) && ($paymentSettings->type) === '2') ? true : false), ['id'=>'options1']) !!}
            </div>
						<div class="form-group">
							{!! Form::submit('Submit', ['class' => 'btn save-btn']) !!}
						</div>
					{!! Form::close() !!}
				</div>
				<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 cols">
					<div class="or">OR</div>
					@if(Session::has('displayPaypal'))
					 		@include('elements.validation-message')
           @endif      
           @if ( Session::has('message-success') && Session::has('successPaypal') )
              @include('elements.message-success')
           @endif
					<h3><i class="fa fa-cc-paypal"></i> Paypal Details</h3>
					{!! Form::model('',['method' => 'POST','class'=>'edit-property-details row','id'=>'tenantAddForm','route'=>['payments.postaddpaypal']]) !!}
						<div class="form-group">
							{!! Form::label('paypal_id', 'Paypal ID:') !!}<span style="color: red;">*</span>
              {!! Form::text('paypal_id', isset($paymentSettings->paypal_id) ? $paymentSettings->paypal_id:"", ['class'=>'form-control','placeholder'=>'Enter Paypal ID']) !!}
						</div>
						<div class="form-group col-sm-12 col-xs-12">
              {!! Form::label('type', 'Make as default payment method:') !!}<span style="color: red;">*</span>
              {!! Form::checkbox('type', 1,((isset($paymentSettings->type) && ($paymentSettings->type) === '1') ? true : false), ['id'=>'options2']) !!}
						</div>
						<div class="form-group">
							<button class="btn save-btn">Save</button>
						</div>
					{!! Form::close() !!}
					<p>All TenantU Services, LLC payments are handled and processed by Braintree Paymentsⓒ. TenantU Services will have absolutely zero access to customer’s sensitive information. All customer transactions are handled with the utmost protection and security under Braintree Payments & PayPal.</p>
				</div>
			</div>
		</div>
	</section>
	</div><!-- /.content-wrapper -->
	@endsection
