@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@include('elements.validation-message')
<section class="content tenant-contacts">
	<div class="row">
		<div class="col-sm-12 col-xs-12">
			<div class="navigation-buttons">
				<a href="{{route('payment.getMyPayment',$prevMonth)}}"><i class="fa fa fa-chevron-left"></i><span>Prev month</span></a>
				<a href="{{route('payment.getMyPayment',$nextMonth)}}"><span>Next month</span><i class="fa fa-chevron-right"></i></a>
			</div>
		</div>
	</div>
	@if((count($monthlyrentDetails) > 0 ) || (count($otherPayments) > 0))
	<div class="tnt-box payment-details">
		<div class="table-responsive">
			<table border="0" class="table table-hover">
				<thead class="payment-details-head">
					<tr>
						<td>Item</td>
						<td>Amount</td>
						<td>Due Date</td>
						<td>Status</td>
						<td>Paid Date</td>
					</tr>
				</thead>
				<tbody class="payment-details-body">
					
					@forelse($monthlyrentDetails as $monthlyrentDetail)
					<tr>
						
						<td>{{$monthlyrentDetail->title}}</td>
						<td>{{$monthlyrentDetail->amount}}</td>
						<td>{{date('d-m-Y',strtotime($monthlyrentDetail->bill_on))}}</td>
						{!!HTML::getPyanowButton($monthlyrentDetail->status,$monthlyrentDetail->bill_on, $monthlyrentDetail->unique_id,"month", $monthlyrentDetail->landlord_id )!!}
						
						<td>{!! HTML::getPaymentDate($monthlyrentDetail->status,$monthlyrentDetail->payment_date)  !!}</td>
					</tr>
					@empty
					
					@endforelse
					
					
					@forelse($otherPayments as $otherPayment)
					<tr>
						<td>{{$otherPayment->title}}</td>
						<td>{{$otherPayment->otheramount}}</td>
						<td>{{date('d-m-Y',strtotime($otherPayment->bill_on))}}</td>
						{!!HTML::getPyanowButton($otherPayment->status,$otherPayment->bill_on, $otherPayment->unique_id, "iter", $otherPayment->landlord_id)!!}
						<td>{!! HTML::getPaymentDate($otherPayment->status,$otherPayment->payment_date)  !!}</td>
					</tr>
					@empty
					
					@endforelse
				</tbody>
			</table>
			
		</div>
	</div>
	@else
	<div class="col-lg-12 col-md-8 col-sm-8 col-xs-10" align="center">
		<div class="no-contacts text-center"> <img src="{{asset('public/img/no-payments.png')}}"> </div>
	</div>
	@endif
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script type="text/javascript">

function alertError(){
	bootbox.alert("You cannot make payment as the landlord has not yet set the payment gateways !");
}	
</script>
<style>
.modal-footer {background:#c00;}
</style>
@endsection
