@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">
    @if ($errors->any())
    <div class="alert alert-danger alert-dismissable">
        <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ( $errors->all() as $error )
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if (Session::has('message'))
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        {{ Session::get('message') }}
    </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <br/>
            
        </div>
        
    </div>
    <div class="row">
        <div class="col-sm-3 col-xs-12 pull-right">
            <div class="navigation-buttons">
                <a href="{{route('admins.getAllPayment',[$prevMonth,$landLordId,$tenantId])}}"><i class="fa fa fa-chevron-left"></i><span>Prev</span></a>
                <a href="{{route('admins.getAllPayment',[$nextMonth,$landLordId,$tenantId])}}"><span>Next</span><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
    </div>
    <div class="row">
        {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm','route'=>['admins.getAllPayment']]) !!}
        <div class="col-sm-5 col-xs-12">
            <div class="select-box">
                {!! Form::select('landlord_id',['' => 'Select Landlord']+$landlordDropdown, $landLordId, ['class'=>'form-control login-input','id'=>'landlordId','required']) !!}
            </div>
        </div>
        <div class="col-sm-5 col-xs-12">
            <div class="select-box" id="tenantDropDown">
                {!! Form::select('tenant_id',['' => 'Select Tenant']+$tenantDropdown, $tenantId, ['class'=>'form-control login-input','id'=>'tenantId','required']) !!}
            </div>
        </div>
        <div class="col-sm-2 col-xs-12">
            <div>{!! Form::button('Search', ['class' => 'btn btn-primary','id'=>'searchBttn']) !!}</div>
        </div>
        {!! Form::close(); !!}
    </div>
    <!-- ends here -->
    <!-- repeat each tenant -->
    
    @foreach( $monthlyrentDetails as $monthlyrentDetail )
    
    <div class="tnt-box payment-details">
        <div class="row pament-texts">
            <div class="col-sm-4 col-xs-12"><div class="tenant-name">{{ $monthlyrentDetail['landLordName'] }}</div></div>
            <div class="col-sm-4 col-xs-12"><div class="property-name">{{ $monthlyrentDetail['tenantName'] }}</div></div>
            <div class="col-sm-4 col-xs-12"><div class="user">{{ $monthlyrentDetail['propertyName'] }}</div></div>
        </div>
        <div class="table-responsive">
            <table border="0" class="table table-striped">
                <thead class="payment-details-head">
                    <tr>
                        <td>Item</td>
                        <td>Amount</td>
                        <td>Due Date</td>
                        <td>Status</td>
                        <td></td>
                        
                    </tr>
                </thead>
                <tbody class="payment-details-body">
                    
                    @if(!empty($monthlyrentDetail['payment']))
                    @foreach($monthlyrentDetail['payment'] as $payment)
                    <tr>
                        
                        <td>{{ $payment['title'] }}</td>
                        <td>{{ '$ '.$payment['amount']}}</td>
                        <td>{{date('d-m-Y',strtotime($payment['bill_on']))}}</td>
                        @if( $payment['status']==0)
                        <td class="pending pendPay{{ $payment['id'] }}">Pending</td>
                        @endif
                        @if($payment['status']==1)
                        <td class="paid"><i class="fa fa-check-circle"></i>Paid</td>
                        @endif
                        
                    </tr>
                    
                    @endforeach
                    
                    @else
                        <tr><td colspan="5">No payment on this period!</td></tr>
                    @endif

                </tbody>
            </table>
        </div>
    </div>
    @endforeach
    
    
</div>
@endsection
@section('page-scripts')
<script type="text/javascript">
if( window.location.hostname == 'localhost'){
var baseUrl = 'http://localhost/tenantudev/';
}else if( window.location.hostname == 'fodof.net') {
var baseUrl = 'http://fodof.net/tenantu/';
}else if(window.location.hostname == 'tenantu.com'){
var baseUrl = 'http://tenantu.com/staging';
}
$(document).ready(function(){
$(document).on('change','#landlordId',function(){
var landlordId = $(this).val();
$.ajax({
url: baseUrl + 'admin/payment/getTenantFromLandlord',
type: "post",
data :{'landLordId': landlordId},
success: function(data){
$('#tenantDropDown').html(data)
}
});
})
$(document).on('click','#searchBttn',function(){
var landLordId = $('#landlordId').val();
var tenantId   = $('#tenantId').val();
window.location.href="{{ route('admins.getAllPayment',[$yearMonthTime]) }}"+"/"+landLordId+"/"+tenantId;
})
})
</script>
<style type="text/css">
/* The common CSS for select box */
/* ================= starts ============ */
.select-box select { padding: 8px 20px 8px 10px; margin: 0; height: 40px; background: transparent; color: #404040; border: none; outline: none; display: inline-block; -webkit-appearance: none; -moz-appearance: none; appearance: none; cursor: pointer; border: 1px solid #e0e0e0; font-family: 'Open Sans'; min-width: 170px; }
.select-box { background: #fff; color: #404040; }
/* Targetting Webkit browsers only. FF will show the dropdown arrow with so much padding. */
@media screen and (-webkit-min-device-pixel-ratio:0) {
.select-box select { padding-right: 18px; }
}
.select-box { position: relative; background-color: #fff; }
.select-box:after { content: "\f107"; display: block; color: #727272; font-family: 'Glyphicons Halflings'; width: 20px; position: absolute; right: 10px; top: 10px; display: inline-block; font: normal normal normal 18px/1 FontAwesome; font-size: inherit; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-size: 24px; font-weight: 100; }
/* The common CSS for select box */
/* ============== Ends =============== */
</style>
@endsection