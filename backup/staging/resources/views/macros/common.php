<?php
//use HTML;
use Carbon\Carbon;
use App\Http\Models\PaymentSetting;

HTML::macro('getTenantProfileImage', function( $tenantId=null, $profileImage=null )
{
	$tenantImage='';
	if( $tenantId=='' && $profileImage=='' )
	{
		
		if(Auth::user('tenant'))
		{
			$tenantImage = Auth::user('tenant')->image;
		}
		else
		{
			$tenantImage='';
		}
	}
	elseif($tenantId!='')
	{
		$objTenant = DB::table('tenants')
            ->select('image')
            ->where('id', '=', $tenantId)
            ->get();
          $tenantImage =  $objTenant[0]->image;
	}
    elseif($profileImage!='')
	{
		$landlordImage= $profileImage;
	} 
		$originalProfile='';
				if($tenantImage!='')
				{
					$imageUrl=Config::get('constants.TENANT_IMAGE_URL');
					$originalProfile=$imageUrl."/".$tenantImage;
				}
				else
				{
					$imageUrl=Config::get('constants.TENANT_NOT_PROFILE_IMAGE_URL');
					$originalProfile=$imageUrl."/tenant-blank.png";
				}
				return $originalProfile;
});

HTML::macro('getLandlordProfileImage', function($landlordId=null,$profileImage=null)
{
	
	$landlordImage='';
	if($landlordId=='' && $profileImage=='')
	{
		if(Auth::user('landlord'))
		{
			$landlordImage = Auth::user('landlord')->image;
		}
		else
		{
			$landlordImage='';
		}
	}
	
	elseif($landlordId!='')
	{
		$objLandlord = DB::table('landlords')
            ->select('image')
            ->where('id', '=', $landlordId)
            ->get();
          $landlordImage =  $objLandlord[0]->image;
	}
	elseif($profileImage!='')
	{
		$landlordImage= $profileImage;
	}
      
		$originalProfile='';
				if($landlordImage!='')
				{
					$imageUrl=Config::get('constants.LANDLORD_IMAGE_URL');
					$originalProfile=$imageUrl."/".$landlordImage;
				}
				else
				{
					$imageUrl=Config::get('constants.LANDLORD_NOT_PROFILE_IMAGE_URL');
					$originalProfile=$imageUrl."/landlord-blank.png";
				}
				return $originalProfile;
});

HTML::macro('getTenantMenuClass',function($pageName,$menuName){
	if($pageName==$menuName)
	{
		return 'active';
	}
	else
	{
		return '';
		
	}
	});
	
	
HTML::macro('getThreadMenuClass',function($messageThreadId,$currentThreadId){
	if($messageThreadId==$currentThreadId)
	{
		return 'active';
	}
	else
	{
		return '';
	}
	});
	
	
HTML::macro('getTenantMessageNotification',function(){
	$tenantId = Auth::user('tenant')->id;
	
	$nonReadMessages = DB::select('SELECT message,message_thread_id,firstname,lastname,m.created_at,l.image FROM messages m,landlords l WHERE m.landlord_id=l.id AND  tenant_id='.$tenantId.' AND sender="L"  AND read_flag=0 AND message_thread_id IN(SELECT MT.id FROM message_threads as MT,threads as T where MT.thread_id=T.id AND T.flag=0) order by m.created_at DESC');
	$countNonReadMessages = count($nonReadMessages);
	
	$messageNotification="<li class='dropdown messages-menu'>
				  
                <a title='Messages' href='#' class='dropdown-toggle' data-toggle='dropdown'>
                  <i class='fa fa-envelope-o'></i>
                  <span class='label label-success'>".$countNonReadMessages ."</span>
                </a>
                <ul class='dropdown-menu'>";
                if($countNonReadMessages==0)
                {
                  $messageNotification.="<li class='header'>You have no messages</li>";
				}
				else
				{
					$messageNotification.="<li class='header'>You have ".$countNonReadMessages." messages</li>";
				}
                  $messageNotification.="<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class='menu'>";
                    $latesMessageThreadId='';
                    
                  foreach($nonReadMessages as $key=>$nonReadMessage){
					  $profileImage=HTML::getLandlordProfileImage('',$nonReadMessage->image);
					  $url=URL::to('tenants/messagelist/'.$nonReadMessage->message_thread_id);
					  $messageNotificationTime=HTML::getMessageNotificationDateTime($nonReadMessage->created_at);
					  $latesMessageThreadId=$nonReadMessages[0]->message_thread_id;
						//~ if($key==0)
						//~ {
							//~ $latesMessageThreadId=$nonReadMessage->message_thread_id;
						//~ }else
						//~ {
							//~ $latesMessageThreadId=$key;
						//~ }
						  
					   $messageNotification.= "
						  <li>
							<a href='".$url."'>
							  <div class='pull-left'>
								<img src='".$profileImage."' class='img-circle' alt='User Image'>
							  </div>
							  <h4>
								".$nonReadMessage->firstname." ".$nonReadMessage->lastname."
								<small><i class='fa fa-clock-o'></i> ".$messageNotificationTime." </small>
							  </h4>
							  <p>".substr($nonReadMessage->message,0,20)."</p>
							</a>
						  </li>
						  ";
				}
				$urlAll=URL::to('tenants/messagelist/'.$latesMessageThreadId);
				
               $messageNotification.= " </ul> </li>";
               if($latesMessageThreadId!='')
               {
					$messageNotification.= " <li class='footer'><a href='".$urlAll."'>See All Messages</a></li>";
			   }
                $messageNotification.= "</ul></li>";
	
	return $messageNotification;
	});
HTML::macro('getLandLordGeneralMessageNotification',function(){
	$landlordId = Auth::user('landlord')->id;
	$nonReadMessages = DB::select('SELECT message,message_thread_id,firstname,lastname,m.created_at,t.image FROM messages m,tenants t, message_threads mt, threads th WHERE m.tenant_id=t.id AND m.message_thread_id=mt.id AND mt.thread_id=th.id AND m.landlord_id='.$landlordId.' AND sender="T"  AND m.read_flag=0 AND th.flag=0 order by m.created_at DESC ');
	//print_r($nonReadMessages);exit;
	$countNonReadMessages = count($nonReadMessages);
	//print HTML::getLandlordProfileImage('','a.jpg');
	$messageNotification="<li class='dropdown messages-menu'>
				  
                <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                  <i class='fa fa-envelope-o'></i>
                  <span class='label label-success'>".$countNonReadMessages ."</span>
                </a>
                <ul class='dropdown-menu'>";
                if($countNonReadMessages==0)
                {
                  $messageNotification.="<li class='header'>You have no messages</li>";
				}
				else
				{
					$messageNotification.="<li class='header'>You have ".$countNonReadMessages." message(s)</li>";
				}
                  $messageNotification.="<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class='menu'>";
                    $latesMessageThreadId='';
                    
                  foreach($nonReadMessages as $key=>$nonReadMessage){
					  $profileImage=HTML::getTenantProfileImage('',$nonReadMessage->image);
					  $url=URL::to('landlords/messagelist/'.$nonReadMessage->message_thread_id);
					  $messageNotificationTime=HTML::getMessageNotificationDateTime($nonReadMessage->created_at);
					  $latesMessageThreadId   = $nonReadMessages[0]->message_thread_id;
						/*if($key==0)
						{
							$latesMessageThreadId=$nonReadMessage->message_thread_id;
						}else
						{
							$latesMessageThreadId=$key;
						}*/
						  
					   $messageNotification.= "
						  <li>
							<a href='".$url."'>
							  <div class='pull-left'>
								<img src='".$profileImage."' class='img-circle' alt='User Image'>
							  </div>
							  <h4>
								".$nonReadMessage->firstname." ".$nonReadMessage->lastname."
								<small><i class='fa fa-clock-o'></i> ".$messageNotificationTime." </small>
							  </h4>
							  <p>".substr($nonReadMessage->message,0,20)."</p>
							</a>
						  </li>
						  ";
				}
				$urlAll=URL::to('landlords/messagelist/'.$latesMessageThreadId);
				
               $messageNotification.= " </ul> </li>";
               if($latesMessageThreadId!='')
               {
					$messageNotification.= " <li class='footer'><a href='".$urlAll."'>See All Messages</a></li>";
			   }
                $messageNotification.= "</ul></li>";
	
	return $messageNotification;
	});
	
HTML::macro('getMessageNotificationDateTime',function($dt){
		$actualTime				= '';
		$currentDateTime 		= Carbon::now();
		//echo $currentDateTime;exit;
		$currentDateTimeStamp 	= strtotime($currentDateTime);
		$messageDateTimeStamp 	= strtotime($dt);
		$timeDifference 		= $currentDateTimeStamp - $messageDateTimeStamp;
		
		if($timeDifference < 60)
		{
			 $actualTime = $timeDifference;
			 return $actualTime."Sec ago";
		}
		elseif($timeDifference > 60 && $timeDifference < 3600)
		{
			$actualTime = round($timeDifference/60);
			return $actualTime."Minute ago";
		}
		elseif($timeDifference >3600 && $timeDifference < 43200)
		{
			$actualTime = round($timeDifference/3600);
			return $actualTime."Hour ago";
		}
		elseif($timeDifference > 43200 && $timeDifference < 86400)
		{
			$actualTime = round($timeDifference/3600);
			return "Today";
		}
		elseif($timeDifference > 86400)
		{
			$dt = date('d M Y h:i:s a',strtotime($dt));
			return $dt;
		}
	
	});

//this is for taking only one image of the property
HTML::macro('getPropertyImage', function($propertyId=null,$propertyImage=null,$imageSize=null,$folderName=null)
{
	$customFolderName ='';
	
	$image ='';
	if(($folderName=='thumb')||($folderName=='square'))
	{
		$customFolderName="/".$folderName;
	}
	if($propertyId!='')
	{
		$objProperty = DB::table('property_images')
						->select('image_name')
						->where('property_id', '=', $propertyId)
						->where('featured','=',1)
						->get();
		
		if($objProperty)
		{
			$image =  $objProperty[0]->image_name;
		}
	}
	elseif($propertyImage!='')
	{
		$image = $propertyImage;
	}
	
	$originalPropertyImage='';
	if($image!='')
	{
		$imageUrl=Config::get('constants.PROPERTY_IMAGE_URL').$customFolderName;
		$originalPropertyImage=$imageUrl."/".$image;
	}
	else
	{
		$imageUrl=Config::get('constants.PROPERTY_NO_IMAGE_URL');
		$originalPropertyImage=$imageUrl."/propert-blank".$imageSize.".png";//this image is used in different sizes
	}
	
	return $originalPropertyImage;
});

HTML::macro('getReviewRating',function($reviewId,$rateId){
	$reviewRatings = DB::select('SELECT rate_value, rating_id FROM rating_user WHERE  review_id ='.$reviewId.' AND rating_id='.$rateId);
	
	$rateValue = $reviewRatings[0]->rate_value;
	
	$rateImage = asset('public/img/stars'.$rateValue.'.png');
  						
  	return $rateImage;
});

HTML::macro('getAdminReviewRating',function($rateValue){
	
	$rateImage = asset('public/img/stars'.$rateValue.'.png');
  						
  	return $rateImage;
});


HTML::macro('getDocExtn',function( $docName ){
	 $temp = explode(".", $docName);
	 $file_ext = end($temp);
	 switch( $file_ext ){
	 	case "doc":
	 		return "doc";
	 	break;
	 	case "DOC":
	 		return "doc";
	 	break;
	 	case "pdf":
	 		return "pdf";
	 	break;
	 	case "PDF":
	 		return "pdf";
	 	break;
	 	case "docx":
	 		return "docx";
	 	break;
	 	default:
	 		return "pdf";
	 	break;
	 }
});

HTML::macro('getAverageReviewRating',function($propertyId,$rateId){
	$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id ='.$propertyId.' AND rating_id='.$rateId);
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');
  	return $avgRateImage;
});



HTML::macro('getAverageReviewRatingFromReviewId',function($propertyId, $rateId, $reviewId){
	$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id ='.$propertyId.' AND rating_id='.$rateId.' AND review_id='.$reviewId);
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');
  						
  	return $avgRateImage;
});
HTML::macro('getAverageReviewRatingForLandlord',function( $propertyId ){
	$avgReviewRatings = DB::select("SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id IN($propertyId)");
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');
  						
  	return $avgRateImage;
});

HTML::macro('getLandLordIssueMessageNotification',function(){
	$landLordId = Auth::user('landlord')->id;
	$nonReadMessages = DB::select('SELECT message,message_thread_id,firstname,lastname,m.created_at,t.image,thread_name,mt.status FROM messages m,tenants t, message_threads mt, threads th WHERE m.tenant_id=t.id AND m.message_thread_id=mt.id AND mt.thread_id=th.id AND m.landlord_id='.$landLordId.' AND sender="T"  AND m.read_flag=0 AND th.flag=1 order by m.created_at DESC ');
	//print_r($nonReadMessages);exit;
	$countNonReadMessages = count($nonReadMessages);
	//print HTML::getLandlordProfileImage('','a.jpg');
	$messageNotification='<li class="dropdown tasks-menu">
		                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                  <i class="fa fa-flag-o"></i>
		                  <span class="label label-danger">'.$countNonReadMessages.'</span>
		                </a>
						<ul class="dropdown-menu">';
					if($countNonReadMessages>0){
						$messageNotification.='<li class="header">You have '. $countNonReadMessages.' message(s)</li>';
					}
					else{
						$messageNotification.='<li class="header">You have no messages</li>';
					}
                  
                  $messageNotification.='<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">';
                    //if($countNonReadMessages>0){
                    foreach($nonReadMessages as $key=>$nonReadMessage){	
                      $messageNotification.='<li><!-- Task item -->
                        <a href="#">
                          <h3> '.$nonReadMessage->thread_name.' <small class="pull-right">50%</small>
                          </h3>
                          <div class="progress xs">';
                          	switch($nonReadMessage->thread_name){

                          	}
                            $messageNotification.='<div class="progress-bar progress-bar-yellow" style="width: 50%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">50% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->';
                      	}
                    $messageNotification.='</ul>
                  </li>';
                  if($countNonReadMessages>0){
                  $messageNotification.='<li class="footer">
                    <a href="#">View all tasks</a>
                  </li>';
                  	}
                $messageNotification.='</ul></li>';
	
	return $messageNotification;
	});
HTML::macro('getTenantIssueMessageNotification',function(){
	$tenantId = Auth::user('tenant')->id;
	$nonReadMessages = DB::select('SELECT message,message_thread_id,mt.status,firstname,lastname,m.created_at,t.image,thread_name 
	FROM messages m,tenants t, message_threads mt, threads th 
	WHERE m.tenant_id=t.id 
	AND m.message_thread_id=mt.id 
	AND mt.thread_id=th.id 
	AND m.tenant_id='.$tenantId.' 
	AND sender="L"  
	AND m.read_flag=0 
	AND th.flag=1 order by m.created_at DESC ');
	//print_r($nonReadMessages);exit;
	$countNonReadMessages = count($nonReadMessages);
	//print HTML::getLandlordProfileImage('','a.jpg');
	$messageNotification='<li class="dropdown tasks-menu">
		                <a title="Responses" href="#" class="dropdown-toggle" data-toggle="dropdown">
		                  <i class="fa fa-flag-o"></i>
		                  <span class="label label-danger">'.$countNonReadMessages.'</span>
		                </a>
						<ul class="dropdown-menu">';
					if($countNonReadMessages>0){
						$messageNotification.='<li class="header">You have '. $countNonReadMessages.' message(s)</li>';
					}
					else{
						$messageNotification.='<li class="header">You have no messages</li>';
					}
                  
                  $messageNotification.='<li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">';
                    //if($countNonReadMessages>0){
                    $latesMessageThreadId='';
                    //$progressBarClass='';
                    //$progressBarPercentage='';
                    foreach($nonReadMessages as $key=>$nonReadMessage){
						switch($nonReadMessage->status)
						{
							case "0":
									$progressBarClass='red';
									$progressBarPercentage='20%';
							break;
							
							case "1":
									$progressBarClass='yellow';
									$progressBarPercentage='60%';
							break;
							
							case "2":
									$progressBarClass='green';
									$progressBarPercentage='100%';
							break;
						}
					$latesMessageThreadId=$nonReadMessages[0]->message_thread_id;
					$url=URL::to('tenants/messagelist/'.$nonReadMessage->message_thread_id);	
                      $messageNotification.='<li><!-- Task item -->
                        <a href='.$url.'>
                          <h3> '.$nonReadMessage->thread_name.' <small class="pull-right">'.$progressBarPercentage.'</small>
                          </h3>
                          <div class="progress xs">
                            <div class="progress-bar progress-bar-'.$progressBarClass.'" style="width: '.$progressBarPercentage.'" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">'.$progressBarPercentage.' Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->';
                      
                      	}
                    $messageNotification.='</ul>
                  </li>';
                  $urlAll=URL::to('tenants/messagelist/'.$latesMessageThreadId);
                  if($countNonReadMessages>0){
                  $messageNotification.='<li class="footer">
                    <a href="'.$urlAll.'">View all tasks</a>
                  </li>';
                  	}
                $messageNotification.='</ul></li>';
	
	return $messageNotification;
	});
	
	HTML::macro('getAverageReviewRatingProperty',function($propertyId){
	
	$avgReviewRatings = DB::select('SELECT avg(rate_value) as rate_value, rating_id FROM rating_user WHERE  property_id ='.$propertyId);
	//dd($avgReviewRatings);
	$avgRateValue = $avgReviewRatings[0]->rate_value;
	$avgRateValueRounded = ceil($avgRateValue);
	$avgRateImage = asset('public/img/stars'.$avgRateValueRounded.'.png');
  						
  	return $avgRateImage;
});

//this method is for the notifications in tenant side 
HTML::macro('getNotificationTenantSide',function(){
	
$loggedTenantId = Auth::user('tenant')->id;

$notificationMonthlyRentAlerts = DB::select('SELECT "Monthly Rent" AS title, LTM.bill_on, LTM.amount, LTM.status, LT.id
FROM landlord_tenant_monthlyrents AS LTM
INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTM.bill_on
BETWEEN CURDATE( )
AND DATE_ADD( CURDATE( ) , INTERVAL 10
DAY ) AND LTM.status=0
ORDER BY LTM.bill_on ASC ');

$notificationOtherPaymentAlerts = DB::select('SELECT LTOD.bill_on, LTOD.title, LTOD.status, LTOD.amount
FROM landlord_tenant_otherpayment_details AS LTOD
INNER JOIN landlord_tenant_otherpayments AS LTO ON LTOD.landlord_tenant_otherpayment_id = LTO.id
INNER JOIN landlord_tenants AS LT ON LT.id = LTO.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTOD.bill_on
BETWEEN CURDATE( )
AND DATE_ADD( CURDATE( ) , INTERVAL 10
DAY )
AND LTOD.status =0
ORDER BY LTOD.bill_on ASC');

$notificationMonthlyRentNotPaidAlerts=DB::select('SELECT "Monthly Rent" AS title, LTM.bill_on, LTM.amount, LTM.status, LT.id
FROM landlord_tenant_monthlyrents AS LTM
INNER JOIN landlord_tenants AS LT ON LT.id = LTM.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTM.bill_on < CURDATE( )
AND LTM.status =0
ORDER BY LTM.bill_on ASC ');

$notificationOtherPaymentNotPaidAlerts=DB::select('SELECT LTOD.bill_on, LTOD.title, LTOD.status, LTOD.amount
FROM landlord_tenant_otherpayment_details AS LTOD
INNER JOIN landlord_tenant_otherpayments AS LTO ON LTOD.landlord_tenant_otherpayment_id = LTO.id
INNER JOIN landlord_tenants AS LT ON LT.id = LTO.landlord_tenant_id
WHERE LT.tenant_id = '.$loggedTenantId.'
AND LTOD.bill_on < CURDATE( )
AND LTOD.status =0
ORDER BY LTOD.bill_on ASC');

$notificationMonthlyRentAlertsCount 		= count($notificationMonthlyRentAlerts);
$notificationOtherPaymentAlertsCount 		= count($notificationOtherPaymentAlerts);
$notificationMonthlyRentNotPaidAlertsCount 	= count($notificationMonthlyRentNotPaidAlerts);
$notificationOtherPaymentNotPaidAlertsCount = count($notificationOtherPaymentNotPaidAlerts);

$totalAlertsCount = $notificationMonthlyRentAlertsCount + $notificationOtherPaymentAlertsCount + $notificationMonthlyRentNotPaidAlertsCount + $notificationOtherPaymentNotPaidAlertsCount;

$notification= '<li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning">'.$totalAlertsCount.'</span>
                </a>
                <ul class="dropdown-menu">';
   if($totalAlertsCount > 0)
   {
	   $notification.='<li class="header">You have '.$totalAlertsCount.' notification(s)</li>';
   }
   else
   {
	   $notification.='<li class="header">You have no notifications</li>';
   }
   
   $notification.='</li>
				<li>
				 <ul class="menu">';
				foreach($notificationMonthlyRentAlerts as $notificationMonthlyRentAlert)
				{
					$billDate 					= $notificationMonthlyRentAlert->bill_on;
					$billDateFormat 			= date('d M Y',strtotime($billDate));
					$billDateString 			= str_replace("-", "", $billDate);
					$billDateStringMonthlyRent  = substr($billDateString,0,6);
					
					$url=URL::to('tenants/mypayments/'.$billDateStringMonthlyRent);
					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-users text-aqua"></i> Your '.strtolower($notificationMonthlyRentAlert->title).' is due on '.$billDateFormat.'
										</a>
									</li>';
				}
				foreach($notificationOtherPaymentAlerts as $notificationOtherPaymentAlert)
				{
					$billDate 					 = $notificationOtherPaymentAlert->bill_on;
					$billDateFormat 			 = date('d M Y',strtotime($billDate));
					$billDateString 			 = str_replace("-", "", $billDate);
					$billDateStringOtherPayment  = substr($billDateString,0,6);
					
					$url=URL::to('tenants/mypayments/'.$billDateStringOtherPayment);
					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-warning text-yellow"></i> Your '.strtolower($notificationOtherPaymentAlert->title).' is due on '.$billDateFormat.'
										</a>
									</li>';
				}
				foreach($notificationMonthlyRentNotPaidAlerts as $notificationMonthlyRentNotPaidAlert)
				{
					$billDate 					 		= $notificationMonthlyRentNotPaidAlert->bill_on;
					$billDateFormat 			 		= date('d M Y',strtotime($billDate));
					$billDateString 			 		= str_replace("-", "", $billDate);
					$billDateStringMonthlyRentNotPaid  = substr($billDateString,0,6);
					$url=URL::to('tenants/mypayments/'.$billDateStringMonthlyRentNotPaid);
					
					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-exclamation-circle"></i> Your '.strtolower($notificationMonthlyRentNotPaidAlert->title).' ('.$billDateFormat.') payment is pending
										</a>
									</li>';
				}
				
				foreach($notificationOtherPaymentNotPaidAlerts as $notificationOtherPaymentNotPaidAlert)
				{
					$billDate 					 		= $notificationOtherPaymentNotPaidAlert->bill_on;
					$billDateFormat 			 		= date('d M Y',strtotime($billDate));
					$billDateString 			 		= str_replace("-", "", $billDate);
					$billDateStringOtherPaymentNotPaid  = substr($billDateString,0,6);
					$url=URL::to('tenants/mypayments/'.$billDateStringOtherPaymentNotPaid);
					
					$notification.='<li>
										<a href='.$url.'>
											<i class="fa fa-exclamation"></i> Your '.strtolower($notificationOtherPaymentNotPaidAlert->title).' ('.$billDateFormat.') payment is pending
										</a>
									</li>';
				}
				$notification.='</ul>
                  </li>';
               $notification.='</ul></li>';
      return $notification;
	
});

HTML::macro('getCmsPages',function(){
	$cmsPagesArray = DB::select('SELECT * FROM pages WHERE status=1 AND footerpage=1 ');
	$cmsPagesHtml= '<ul class="left-menu">';
	for($i=0;$i<count($cmsPagesArray);$i++)
	{
		$url=URL::to('cms/'.$cmsPagesArray[$i]->pageslug);
		$cmsPagesHtml.= '<li><a href='.$url.'>'.$cmsPagesArray[$i]->title.'</a></li>';
		$max = count($cmsPagesArray)-1;
		if($i==$max)
		{
			$cmsPagesHtml.='</ul>';
		}
		elseif(($i%5==0)&&($i>0)){
			$cmsPagesHtml.='</ul><ul class="left-menu">';
		}
	}
	return $cmsPagesHtml;
});

HTML::macro('getTenantBreadcrumbs',function($menuName){
	
	//$url='';
	//dd($url);
	$homeUrl = URL::to('tenants/dashboard');
	if($menuName == 'dashboard')
	{
		$pageHeading='Dashboard';
		$breadCrumb = 'Home';
	}elseif($menuName == 'mysearch')
	{
		$pageHeading='My Search';
		$breadCrumb = 'My search';
	}
	elseif($menuName == 'recentproperties')
	{
		$pageHeading='Recently viewed properties';
		$breadCrumb = 'Recent Properties';
	}
	elseif($menuName == 'mylandlord')
	{
		$pageHeading='My Landlord';
		$breadCrumb = 'My landlord';
	}
	elseif($menuName == 'mypayment')
	{
		$pageHeading='My Payment';
		$breadCrumb = 'My payment';
	}
	elseif($menuName == 'mycontact')
	{
		$pageHeading='My Contact';
		$breadCrumb = 'My contact';
	}
	
	elseif($menuName == 'changepassword')
	{
		$pageHeading='Change Password';
		$breadCrumb = 'Change Password';
	}
	elseif($menuName == 'profile')
	{
		$pageHeading='Profile';
		$breadCrumb = 'Profile';
	}
	elseif($menuName == 'schools')
	{
		$pageHeading='Schools';
		$breadCrumb = 'Schools';
	}
	elseif($menuName == 'newmessage')
	{
		$pageHeading='Messages';
		$breadCrumb = 'Messages';
	}
	elseif($menuName == 'messagelist')
	{
		$pageHeading = 'List message';
		$breadCrumb  = 'List message';
	}
	$breadCrumbHtml='';
	//dd($menuName);
	if($menuName=='dashboard')
	{
		$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'
						
					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
						
					  </ol>
					</section>';
	}
	else
	{
	$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'
						
					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active"><a href="">'.$breadCrumb.'</a></li>
					  </ol>
					</section>';
	}
	return $breadCrumbHtml;
	});
	
	
	HTML::macro('getLandlordBreadcrumbs',function($menuName){
		
	$homeUrl = URL::to('landlords/dashboard');
	if($menuName == 'dashboard')
	{
		$pageHeading='Dashboard';
		$breadCrumb = 'Home';
	}elseif($menuName == 'myproperty')
	{
		$pageHeading='My Property';
		$breadCrumb = 'My property';
	}
	elseif($menuName == 'mypayment')
	{
		$pageHeading='My Payment';
		$breadCrumb = 'My payment';
	}
	elseif($menuName == 'mytenantcontact')
	{
		$pageHeading='My Contact';
		$breadCrumb = 'My contact';
	}
	elseif($menuName == 'mytenants')
	{
		$pageHeading='My Tenant';
		$breadCrumb = 'My tenant';
	}
	elseif($menuName == 'payment')
	{
		$pageHeading='Payment Settings';
		$breadCrumb = 'Payment settings';
	}
	elseif($menuName == 'featured properties')
	{
		$pageHeading='Featured Properties';
		$breadCrumb = 'Featured Properties';
	}
	
	elseif($menuName == 'changepassword')
	{
		$pageHeading='Change Password';
		$breadCrumb = 'Change Password';
	}
	elseif($menuName == 'viewtenant')
	{
		$pageHeading='View Tenant';
		$breadCrumb = 'View Tenant';
	}
	elseif($menuName == 'edittenant')
	{
		$pageHeading='Edit Tenant';
		$breadCrumb = 'Edit Tenant';
	}
	elseif($menuName == 'Profile')
	{
		$pageHeading='Profile';
		$breadCrumb = 'Profile';
	}
	elseif($menuName == 'editprofile')
	{
		$pageHeading='Edit profile';
		$breadCrumb = 'Edit profile';
	}
	elseif($menuName == 'Review')
	{
		$pageHeading='Property Review';
		$breadCrumb = 'Property Review';
	}
	elseif($menuName == 'newmessage')
	{
		$pageHeading='Messages';
		$breadCrumb = 'Messages';
	}
	elseif($menuName == 'messagelist')
	{
		$pageHeading='List message';
		$breadCrumb = 'List message';
	}
	if($menuName=='dashboard')
	{
		$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'
						
					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
					  </ol>
					</section>';
	}
	else
	{
	//dd($menuName);
	$breadCrumbHtml = '<section class="content-header">
					  <h1>
						'.$pageHeading.'
						
					  </h1>
					  <ol class="breadcrumb">
						<li><a href='.$homeUrl.'><i class="fa fa-dashboard"></i> Home</a></li>
						<li class="active"><a href="">'.$breadCrumb.'</a></li>
					  </ol>
					</section>';
	}
	return $breadCrumbHtml;
	});
	
HTML::macro('getPyanowButton',function( $status, $billDate, $uniqueId, $flag, $landlordId ){
	$onClick = "";
	if($flag=="month"){
		$url=URL::to('payments/paynowmonthlyrent').'/'.$uniqueId;
	}
	else{
		$url=URL::to('payments/paynowitinerary').'/'.$uniqueId;
	}
	$settings  = PaymentSetting::where( 'landlord_id', $landlordId )
								->first();
    if(empty( $settings )){
    	$url="javascript:void(0)";
    	$onClick = 'onclick=alertError();';

    }


	$curDate 			= date('Y-m-d');
	$currentDateTime 	= strtotime($curDate);
	$billDateTime 		= strtotime($billDate);
	if(($status==0) && ($billDateTime <= $currentDateTime))
	{
		$payNowHtml = '<td><a href="'.$url.'" class="paynow paynow-exp"'.$onClick.'>Pay Now</a></td>';
	}
	elseif($status==0)
	{
		$payNowHtml = '<td><a href="'.$url.'" class="paynow"'.$onClick.'>Pay Now</a></td>';
	}
	elseif($status==1)
	{
		$payNowHtml = '<td class="paid">Paid</td>';
	}
	
	return $payNowHtml;
});
HTML::macro('getReviewCount',function( $propertyId ){
		
		$reviewsCount = DB::select("SELECT COUNT(*) AS cnt FROM property_review WHERE property_id IN ($propertyId)");
         
        return $reviewsCount[0]->cnt;            
});

HTML::macro('getSocialMediaLinks',function( $socialLink ){
	$link='';
	if($socialLink == 'twitter')
	{
		$social = DB::select("SELECT twitter FROM `social_links` ");
		
		$link 	= $social[0]->twitter;
		if($link=='')
		{
			$link='#';
		}
	}
	elseif($socialLink == 'facebook')
	{
		$social = DB::select("SELECT facebook FROM `social_links` ");
		
		$link 	= $social[0]->facebook;
		if($link=='')
		{
			$link='#';
		}
	}
	elseif($socialLink == 'instagram')
	{
		$social = DB::select("SELECT instagram FROM `social_links` ");
		
		$link 	= $social[0]->instagram;
		if($link=='')
		{
			$link='#';
		}
	}
	
	return $link;
		       
});

HTML::macro('getPaymentDate',function( $status,$paymentDate){
	if(($status==1) &&($paymentDate!='0000-00-00 00:00:00'))
	{
		
		$date =  date("d-m-Y", strtotime($paymentDate));
	}
	else
	{
		$date =  '';
	}
	return $date;
});

HTML::macro('getSchoolImage',function($schoolId){
	
	$objSchool = DB::table('schools')
            ->select('image')
            ->where('id', '=', $schoolId)
            ->get();
           
    $schoolImage =  $objSchool[0]->image;
   // dd($schoolImage);
    $originalSchoolImage='';
	if($schoolImage!='')
	{
		$imageUrl=Config::get('constants.SCHOOL_IMAGE_URL');
		$originalSchoolImage=$imageUrl."/".$schoolImage;
	}
	
	return $originalSchoolImage;

});

HTML::macro('getAdminImage',function(){
	$imageUrl     = Config::get('constants.TENANT_NOT_PROFILE_IMAGE_URL').'/landlord-blank.png';
	return $imageUrl;
});

HTML::macro('getBannerImage',function($imageName){
	$imageUrl     = Config::get('constants.BANNER_IMAGE_URL');
	$fullImage    = $imageUrl.'/'.$imageName;
	return $fullImage;
});

HTML::macro('getSampleBannerImage',function($sampleImageName){
	$imageUrl     = asset('public/img/');
	$fullImage    = $imageUrl.'/'.$sampleImageName;
	return $fullImage;
});

HTML::macro('getBanner',function($bannerId){
	$imageHtml='';
	$fullImage = '';
	$objBanner = DB::table('banner_purchases')
            ->select('image')
            ->addSelect('banner_url')
            ->where('bannerId', '=', $bannerId)
            ->where('status','=',1)
            ->get();
    
	$imageUrl     = asset('public/uploads/banners');
	if(count($objBanner) > 0)
	{
		$fullImage    = $imageUrl.'/'.$objBanner[0]->image;
	}
	if(($bannerId==2) && (count($objBanner)>0))
	{
	$imageHtml = '<div class="row ad-container">
  				<div class="col-sm-12 col-xs-12">
					<a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
				</div>
  			</div>';
	}
	elseif(($bannerId==1) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container">
          <div class="col-sm-12 col-xs-12">
			  <a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
			  </div>
        </div>';
	}
	elseif(($bannerId==4) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container map">
          <div class="col-sm-12 col-xs-12">
			 <a href="'.$objBanner[0]->banner_url.'" target="_blank"> <img src="'.$fullImage.'"></a>
			  </div>
        </div>';
	}
	elseif(($bannerId==3) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container">
  				<div class="col-sm-12 col-xs-12">
					<a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
				</div>
  			</div>';
	}
	
	elseif(($bannerId==5) && (count($objBanner)>0))
	{
		$imageHtml='<div class="row ad-container">
		<div class="col-sm-12 col-xs-12">
			<a href="'.$objBanner[0]->banner_url.'" target="_blank"><img src="'.$fullImage.'"></a>
		</div>
  	</div>';
	}
	return $imageHtml;
});
	
