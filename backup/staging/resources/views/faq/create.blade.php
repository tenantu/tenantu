@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model('',['method' => 'POST','route'=>['faq.store'],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('Question', 'Question:') !!}<span style="color: red;">*</span>
            {!! Form::text('question', Input::old('question'), ['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Answer', 'Answer:') !!}<span style="color: red;">*</span>
            {!! Form::textarea('answer',Input::old('answer'),['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::checkbox('status', 1, Input::old('status'), ['class'=>'']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
