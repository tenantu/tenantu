@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    <script type="text/javascript" src="{{asset('public/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
	  tinymce.init({
		selector : "textarea",
		  }); 
	</script>

    {!! Form::model($faq,['method' => 'POST','route'=>['faq.update',$faq->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('Question', 'Question:') !!}<span style="color: red;">*</span>
            {!! Form::text('question',null,['class'=>'form-control ckeditor']) !!}
        </div>
       
       <div class="form-group">
            {!! Form::label('Answer', 'Answer:') !!}<span style="color: red;">*</span>
            {!! Form::textarea('answer',null,['class'=>'form-control']) !!}
        </div>
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
