@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
        {!! Form::model($search, ['method' => 'GET','route'=>['admins.contactUsers']]) !!}
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                    {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>

            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
            
        </div>
        <div class="col-md-8 text-right">
        {!! $contactUsers->appends(['search' => $search, 'sortby' => $sortby, 'order' => ($order == 'desc')?'asc':'desc'])->render() !!}
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="">#</th>
                <th class="col-md-4">
                        <a href=""> Name</a>
                </th>
                <th class="col-md-4">
                        <a href="">Email</a>
                   </th>
                    
                
                <th class="col-md-3 text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
$page = $contactUsers->currentPage();
$slNo = (($page - 1) * 10) + 1;
?>
        @foreach ($contactUsers as $contactUser)
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$contactUser->name}}</td>
                <td>{{$contactUser->email}}</td>
                <td class="text-center">
                   <a href="{{route('contacts.Edit',$contactUser->id)}}" class="btn btn-link"> View</a> &nbsp;
                </td>
            </tr>
            <?php
$slNo++;
?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * 10) + 1}} to {{ $slNo-1 }} of {!! $contactUsers->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $contactUsers->render()) !!}
        </div>
    </div>
    </div>
</div>
@endsection

