 @extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
		@include('elements.validation-message')
	<section class="content my-landlord">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@forelse($tenantLandlords as $tenantLandlord)
                <div class="contact-box">
                    <div class="row landlord-name-cntr">
                        <div class="col-md-1 col-sm-2">
                            <div class="prf-img">
                              <img alt="image" class="img-circle img-responsive" src="{{HTML::getLandlordProfileImage('',$tenantLandlord->image)}}">
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-4">
                          <div class="my-landlord-name">{{ucfirst($tenantLandlord->firstname).' '. ucfirst($tenantLandlord->lastname) }}</div>
                          <div class="my-landlord-location">{{$tenantLandlord->location}}</div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="my-landlord-btn-cntr">
                            <a href="{{route('properties.getPropertyDetail',$tenantLandlord->slug)}}" class="buttons">Profile</a>
                            <a href="{{route('payment.getMyPayment')}}" class="buttons">Payment information</a>
                            <a href="{{route('tenants.getAgreementDetails',$tenantLandlord->id)}}" class="buttons">Agreement details</a>
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
							<div class="col-md-1 col-sm-2">
                            <div class="proprty-image">
                              <img alt="image" class="img-responsive" src="{{HTML::getPropertyImage($tenantLandlord->property_id,'','','thumb')}}">
                            </div>
                        </div>
                          <div class="col-sm-5 col-xs-12">
                            <h4>{{$tenantLandlord->property_name}}</h4>
                            <p><i class="fa fa-map-marker"></i>  {{$tenantLandlord->propertyLocation}}</p>
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <div class="agreement-duration">Agreement From</div>
                            <div>{{ date("d-m-Y", strtotime($tenantLandlord->agreement_start_date))}}</div>
                            
                            
                          </div>
                          <div class="col-sm-3 col-xs-6">
                            <div class="agreement-duration">Agreement To</div>
                            <div>{{ date('d-m-Y', strtotime("+".$tenantLandlord->cycle, strtotime($tenantLandlord->agreement_start_date))) }}</div>
                          </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @empty
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="no-contacts text-center"><img src="{{asset('public/img/no-landlords.png')}}"></div>
				</div>
			   @endforelse                
            </div>
        </div><!-- /.row -->

        </section><!-- /.content -->
        @endsection
        
