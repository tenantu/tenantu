  <div class="col-md-9 col-md-9 col-sm-8 col-xs-12">

                <!-- ================================== -->
                <!-- DIRECT CHAT START-->
                <!-- ================================== -->
                  <div class="box direct-chat tenant-message">

                  <!-- box-header -->
                    <div class="box-header with-border">
                      <div class="media">
                        
                        <div class="media-body">
                        <div class="add-to-list pull-right">
							<button class="btn btn-box-tool msg-cnts" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                        </div>
                          <h4 class="media-heading">{{$msgThreadName}} <font color='#ea5662'>({{ $msgPropertyName }})</font></h4>
                          
							<span class="college">Created at: {{ $msgThreadCreated }}</span>
                          
                        </div>
                      </div>
                    </div>
                    <!-- /.box-header -->


                    <div class="box-body">
                      <!-- Conversations are loaded here -->
                      <div class="direct-chat-messages">
						
					@foreach ($allMessages as $message)
					<?php
					$name='';
					$msgCls= '';
					?>
					
						@if($message['sender']=='T')
							<?php $name=$tenantName; 
							$msgCls='send';
							?>
						@else
						 <?php $name=$landlordName;
						 $msgCls='recieved';
						  ?>
						@endif 
							
						<div class="direct-chat-msg {{$msgCls}}">
                          <div class="direct-chat-info clearfix">
                            <div class="direct-chat-name">{{$name}} :</div>
                            <?php
								$createdDate = date('d M h:i:s a',strtotime($message['created_at']));
                            ?>
                            <div class="direct-chat-timestamp">{{$createdDate}}</div>
                          </div><!-- /.direct-chat-info -->

                          <div class="direct-chat-text">
                            {{$message['message']}}
                          </div><!-- /.direct-chat-text -->

                        </div>
						  
						  @endforeach

                      </div><!--/.direct-chat-messages-->


                   

                    </div><!-- /.box-body -->

                    <!-- compose message -->
                    <!-- ====================== COMPOSE MESSAGE ============== -->
                    <div class="box-footer">
						
                       {!! Form::model(null,['method' => 'POST','route'=>['tenants.composeMessageList',$messageThreadId]]) !!}
                        <div class="input-group">
							{!! Form::hidden('landlordId', $landlordId) !!}
							{!! Form::text('message',null,['class'=>'form-control write-message','placeholder'=>'Type Message ...','required' => '']) !!}
                          
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat send-message-btn">Send</button>
                          </span>
                        </div>
                        
                {!! Form::close() !!}
                    </div><!-- /.box-footer-->
                  </div>
                  <!-- ================================== -->
                  <!-- DIRECT CHAT END-->
                  <!-- ================================== -->

                </div>
                
          
