@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-7 col-xs-12">
              <!-- recent properties -->
              <div class="tnt-box rcnt-prts">
                <div class="box-heads">
                  <h4>General queries</h4>
                  
                </div>
                <ol>
				 @forelse($generalMessages as $key => $message)
				  <li>
					  <span class="number">{{$key+1}}.</span>
					  <strong>{{$message->thread_name}}: </strong>
					  @if(strlen($message->message) > 80 )
					   {{substr($message->message,0,80).'...'}}
					  @else
					   {{ $message->message }}
					  @endif  
					  <a href="{{route('tenants.getMessageList',$message->message_thread_id)}}" class="pull-right">Details</a>
				  </li>
				  
				  @empty
					<p>No messages</p>
                  
				 @endforelse
                  
                </ol>
              </div>
              @if($isTenantAssociatedWithLandlord == 1)
              <div class="tnt-box rcnt-prts">
                <div class="box-heads">
                  <h4>Issues reported</h4>
                  
                </div>
                <ol>
				 @forelse($issueMessages as $key => $message)
				  <li>
					  <span class="number">{{$key+1}}.</span>
					  <strong>{{$message->thread_name}}: </strong> 
					  @if(strlen($message->message) > 80 )
					   {{substr($message->message,0,80).'...'}}
					  @else
					   {{ $message->message }}
					  @endif 
					  <a href="{{route('tenants.getMessageList',$message->message_thread_id)}}" class="pull-right">Details</a>
				  </li>
				  @empty
					<p>No messages</p>
                  
				 @endforelse
                  
                </ol>
              </div>
              @endif
            </div><!-- ./col -->
           <!-- ./col -->
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
              <!-- small box -->
              <div class="tnt-box">
                <div>
                  <div class="box-heads">
                    <h4>My Payments</h4>
                  </div>
                  <div class="payment-links"><i class="fa fa-dollar"></i><a href="{{route('payment.getMyPayment')}}">Payment</a></div>
                </div>
              </div>
            </div><!-- ./col -->
            <!-- ./col -->
          </div>
          <!-- Main row -->
         

        </section>
@endsection
