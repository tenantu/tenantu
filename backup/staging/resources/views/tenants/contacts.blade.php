@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
		@include('elements.validation-message')
<section class="content tenant-contacts">
          <!-- Small boxes (Stat box) -->
         @forelse( $landlordsObj as $landlord  )
          <div class="row">
            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <div class="prf-img"><img alt="image" class="img-circle img-responsive" src="{{HTML::getLandlordProfileImage($landlord->landlord_id)}}"></div>
                            <div class="m-t-xs font-bold"></div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h3>{{ $landlord->firstname." ".$landlord->lastname}}</h3>
                        <p><i class="fa fa-map-marker"></i> {{$landlord->city}}</p>
                        <div class="profile-description">
							@if(strlen($landlord->about) > 100)
								{{substr($landlord->about, 0, 100). '...'}}
							@else
							 {{$landlord->about}}
							 @endif
                        </div>
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6 col-sm-offset-0 col-xs-offset-3">
							  
                            <a href="{{ route ('tenants.getMessageList',$landlord->latestMsgThreadId->message_thread_id)}}" class="links"><i class="fa fa-comment-o"></i></a>
                            
                            <a href="{{ route('tenants.getMessage',$landlord->landlord_id)}}" class="links"><i class="fa fa-edit"></i></a>
                            <a href="{{ route ('tenants.getLandlordProfileFromTenant',$landlord->slug)}}" class="view-profile">View Profile</a>
                          </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            @empty
            <section class="content my-contacts lndlrd-contacts">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="no-contacts text-center"><img src="{{asset('public/img/no-contacts.png')}}"></div>
            </div>
        </div><!-- /.row -->
          
        </section>
             @endforelse

            
        </div><!-- /.row -->

        </section>
@endsection
