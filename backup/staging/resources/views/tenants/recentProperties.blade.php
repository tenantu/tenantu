 @extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
    
		@include('elements.validation-message')
	<section class="content tenant-contacts">
          <!-- Small boxes (Stat box) -->
          <div class="row">
			
			@forelse($properties as $key=>$property)  
			<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="contact-box">
                    <div class="col-sm-4">
                        <div class="text-center">
                            <div class="recent-properties-image"><img alt="image" class="img-responsive" src="{{HTML::getPropertyImage($property->property_id,'','','thumb')}}"></div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <h3>{{$property->property_name}}</h3>
                        <p class="cnt-ltn"><i class="fa fa-map-marker"></i> {{$property->location}}</p>
                        
                        @if(strlen($property->schoolName) > 30)
                        <div class="profile-description">
							{{substr($property->schoolName, 0, 30). '...'}}
                        </div>
                        @else
                        <div class="profile-description">
							{{$property->schoolName}}
                        </div>
                        @endif
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-8 col-sm-offset-0 col-xs-offset-2">
                            <a href="{{route('properties.getPropertyDetail',$property->slug)}}" class="view-profile">View Property</a>
                          </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
			@empty
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="no-contacts text-center"><img src="{{asset('public/img/no-properties.png')}}"></div>
			</div>
			@endforelse
           </div><!-- /.row -->

        </section>
        
        @endsection
        
