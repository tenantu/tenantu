@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
   @include('elements.message-success')
@endif
@include('elements.validation-message')
 <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
              <!-- Landlord View tenant  Profile -->

              <div class="tnt-box tenantu-profile l-t-view">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <div class="profile-image">
                      <img src="{{HTML::getLandlordProfileImage($landlordsObj->id)}}">
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-8col-xs-12">
                    <div class="tnt-name">{{ucfirst($landlordsObj->firstname).' '. ucfirst($landlordsObj->lastname) }}</div>
                    <div class="tnt-place">{{$landlordsObj->location}}</div>
                    <div class="tnt-contacts">
                      <a href="#">{{$landlordsObj->email}}</a>
                      <div>{{$landlordsObj->phone}}</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="tenantu-prf-details">

                      <div class="row cnts">
					   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						  <label>Language</label>
						  <div class="language">{{ ($landlordsObj->language!="") ? $landlordsObj->language : "NotSpecified"}}</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
						  <label>School</label>
						  <div class="language">{{ $schoolname->schoolname}}</div>
						</div>
						
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <label>City</label>
                          <div class="city">{{ ($landlordsObj->city!="") ? $landlordsObj->city  : "NotSpecified"}}</div>
                        </div>
                       
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <label>State</label>
                          <div class="gender"> {{ ($landlordsObj->state!="") ? $landlordsObj->state  : "NotSpecified"}}</div>
                        </div>
                        @if($landlordsObj->about!='')
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                          <label>About me</label>
                          <div class="dob">{{ ($landlordsObj->about!="") ? $landlordsObj->about  : "NotSpecified" }}</div>
                        </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            <!-- ./col -->
          </div><!-- /.row -->

          <div>
          
        </section>
        
        @endsection
        
