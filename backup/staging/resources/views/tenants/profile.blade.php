@extends('layouts.tenant-inner')
@section('tenant-inner-content')
@if (Session::has('message-success'))
        @include('elements.message-success')
    @endif
<section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <!-- Landlord Profile -->
				<div class="pull-right"><button onclick="location.href='{{route('tenants.editprofile')}}';" class="btn btn-default tenant-profile-edit-btn">Profile edit</button></div>
              <div class="tnt-box tenantu-profile">
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
                    <div class="profile-image">
						
                      <img src="{{ $profileImage}}">
                    </div>
                    
                    <div class="tnt-name">{{ucfirst($profile->firstname).' '. ucfirst($profile->lastname) }}</div>
                    <div class="tnt-place">{{$profile->location}}</div>
                    <div class="tnt-contacts">
                      <a href="#">{{ $profile->email}}</a>
                      <div>{{ $profile->phone}}</div>
                    </div>
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="box-heads">
                      <h4>About me</h4>
                    </div>
                    <p class="texts">{{$profile->about}}</p>
                    <div class="tnt-school-label">School</div>
                    <div class="tnt-school-name">{{$schoolName}}</div>
                  </div>
                </div>
                </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="tenantu-prf-details">
                    <div class="box-heads">
                      <h4>Profile Details</h4>
                    </div>
                    <div class="row cnts">
                      <div class="col-lg-4">
                        <label>State</label>
                        <div class="place">{{ $state }}</div>
                        <label>City</label>
                        <div class="city">{{ $city }}</div>
                        <label>Language</label>
                        <div class="language">{{ $language }}</div>
                      </div>
                      <div class="col-lg-4">
                        <label>Gender</label>
                        <div class="gender">{{ $gender}}</div>
                        <label>Birth Date</label>
                        <div class="dob">{{$birthdate}}</div>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
          
            <!-- ./col -->
          </div><!-- /.row -->
          <div>
        </section>
                    
@endsection
