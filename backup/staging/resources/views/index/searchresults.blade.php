 @extends('layouts.frontend-inner-master')
 @section('content')
 
 <div class="container inner-container">
	 {!! HTML::getBanner(1) !!}
    
    <div class="row">
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
        <ol class="breadcrumb">
        <li><a href="{{ route('index.index') }}">Home</a></li>
        <li class="active">Search Results</li>
      </ol>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
        {!! Form::model('',['method' => 'POST','route'=>['index.searchResult'],'class'=>'form-inline pull-right sort-form srch','id'=>'sortForm']) !!}
        
        
        <div class="form-group">
        {!! Form::label('aminities', 'Amenities:') !!}
        
         
      {!! Form::select('aminity[]',$aminity,$selectedAminity, ['class'=>'form-control text-left','id'=>'aminity','multiple']) !!}
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Availability</label>
          <div class="select sort">
          {!! Form::select('sortKey',['' => array('all'=>"All",'Available'=>"Available",'Not available'=>"Not available")], $selectedAvailabilityKey, ['class'=>'selectpicker show-tick form-control sort-select','id'=>'availabilityKey']) !!}              
          </div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Sort</label>
          <div class="select sort availability">
          {!! Form::select('sortKey',['' => array('all'=>"Latest",
                                                  'MR'=>"Most Reviewed",
                                                  'MV'=>"Most Viewed",
                                                  'LtoS'=>"Largest to Samllest(No of bedrooms)",
                                                  'StoL'=>"Samllest to Largest(No of bedrooms)",
                                                  'HRL'=>"Highest Rated Landlord",
                                                  'HRLoc'=>"Highest Rated Location",
                                                  'HRCl'=>"Highest Rated Cleanliness",
                                                  'HRA'=>"Highest Rated Amenities",
                                                  'AvgRat'=>"Highest Rated Overall",
                                                  'WR' => "Would recommend")], $selectedSortKey, ['class'=>'selectpicker show-tick form-control sort-select','id'=>'sortKey']) !!}              
          </div>
        </div>
        {!! Form::close() !!}
      </div>
    </div>

    <div class="row property-list">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h3>Search Results</h3>
        </div>
      <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
        <ul class="row properties" id="portfolio-div">
          @include('index._searchresults')
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <!-- <div id="map-canvas" style="border: 2px solid #3872ac;"></div> -->
        <div id="map-canvas"></div>
         {!! HTML::getBanner(4) !!}
        
      </div>
    </div>
  </div>
  <div class="row hidden" >
        {!! $properties->render() !!}
    </div>
    
  @endsection
  @section('page-scripts')
	<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
	<link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css"> 
	<link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css">  
	<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script> 
  
  <script src="{{ asset('public/js/jquery.infinitescroll.js') }}"></script>
  <script src="{{ asset('public/js/manual-trigger.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfo5c1VLkhhvEVUb6rUmim3E7N6vtLK1c&signed_in=false"></script>
  
  <script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>
  
  <script type="text/javascript">
	  
	var dataSaved="{{ $selectedAminity }}";
	var dataArray=dataSaved.split(",");
	$("#aminity").val(dataArray);
	
	$(document).ready(function(){
		
		$('select[multiple]').multiselect({
			columns: 1,
			placeholder: 'Select options',
			
			}); 
			$('#aminity').bind('change', function(){
				var foo = []; 
				$('#aminity :selected').each(function(i, selected){ 
				  foo[i] = $(selected).text(); 
				});
				
				$('#hd_aminity').val(foo);
			});
		 
	
	  
    <?php if($selectedAminity!= ''){
            ?>
			
			$('#hd_aminity').val('<?php echo $selectedAminity ?>');
          <?php 
	   } ?>
	   
		
		  
  var landlordId      = "{{ $selectedLandlordId }}";
  var location        = "{{ $searchKeyWord }}";
  var bedroomNo       = "{{ $selectedBedroomNo }}";
  var distance        = "{{ $selectedDistance }}";
  var priceFrom       = "{{ $selectedPriceFrom }}";
  var priceTo         = "{{ $selectedPriceTo }}";
  var schoolId        = "{{ $selectedSchoolId }}";
  var sortKey         = "{{ $selectedSortKey }}";
  var availabilityKey = "{{ $selectedAvailabilityKey }}";
  $('.properties').infinitescroll({
    navSelector     : "ul.pagination",
    nextSelector    : "li a:last",
    itemSelector: ".item",
    debug: false,
    dataType: 'html',/*
    maxPage: 6,*/
    path: function(index) {
      //console.log(index);
      // return "index" + index + ".html";
      return "?page="+index+"&search="+location+"&landlord_id="+landlordId+"&bedroomId="+bedroomNo+"&distance="+distance+"&priceFrom="+priceFrom+"&priceTo="+priceTo+"&school_id="+schoolId+"&sortKey="+sortKey+"&availabilityKey="+availabilityKey;
      // return false;
    }
    // appendCallback : false, // USE FOR PREPENDING
    }, function(newElements, data, url){

      $('#infscr-loading').remove();
      // used for prepending data
      // $(newElements).css('background-color','#ffef00');
      // $(this).prepend(newElements);
    });

    $(document).on('change','#sortKey',function(){
       $('#sortKeyUpdate').val($(this).val());
       $('#availabilityKeyUpdate').val($('#availabilityKey').val());
       $('#searchForm').submit();
    });
    $(document).on('change','#availabilityKey',function(){
       $('#sortKeyUpdate').val($('#sortKey').val());
       $('#availabilityKeyUpdate').val($(this).val());
       $('#searchForm').submit();
    });

    // google map
    var geocoder;
    var map;
    var markers = [];
    // var list = [
    //       [51.503454, -0.119562, 'ttest ew'],
    //       [51.499633, -0.124755, 'ttest ew']
    //   ];
      @if(count($googleMapArray) > 1)
     var list = [
      @foreach( $googleMapArray as $key => $val ) 
      [ {{ $val['lat'] }} ,  {{ $val['lng'] }}, '{{ $val['address'] }}' ],
      @endforeach 
    ];
    @elseif(count($googleMapArray) == 1)
      var list =[ 
       
      [ {{ $googleMapArray[0]['lat'] }} ,  {{ $googleMapArray[0]['lng'] }}, '{{ $googleMapArray[0]['address'] }}'] ];
     @else 
    var list = [];
    @endif
   
    function initialize() {
    console.log(list);
        map = new google.maps.Map(document.getElementById('map-canvas'), {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom:10
        });
        
        var bounds = new google.maps.LatLngBounds();
        list.forEach(function (data, index, array) {

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(list[index][0], list[index][1]),
                map: map,
                icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                title: list[index][2],
                disableDefaultUI: false
            });
            markers.push(marker);

            bounds.extend(marker.position);
        });
        map.fitBounds(bounds);

    }
    google.maps.event.addDomListener(window, "load", initialize);

    // showme = function ( index_val ) {
    //   console.log( $('.properties li').index() );
    //   for (i = 0; i < list.length; i++) {
    //      markers[i].setAnimation(null);
    //   }
    //   // markers[index].setAnimation(null);
    //     // if (markers[index].getAnimation() != google.maps.Animation.BOUNCE) {
    //     //     markers[index].setAnimation(google.maps.Animation.BOUNCE);
    //     // }
    //     //  else {
    //     //     markers[index].setAnimation(null);
    //     // }
    // }

    $(document).on('mouseover','.properties li.item', function( e ){

      index =  $(this).index();
      if (markers[index].getAnimation() != google.maps.Animation.BOUNCE) {
        markers[index].setAnimation(google.maps.Animation.BOUNCE);
      }


    });
    $(document).on('mouseout','.properties li.item', function( e ){
       index =  $(this).index();
       markers[index].setAnimation(null);
        
    });
    $(document).on('click','.indProperty',function( e ){
      var slug  = $(this).data("slug");
      var url   = "{{url('/property')}}/"+slug;
      window.location.href=url;
      
    });
    
    
   if( window.location.hostname == 'localhost'){
		  var baseUrl = 'http://localhost/tenantudev/';
		}else if( window.location.hostname == 'fodof.net') {
		  var baseUrl = 'http://fodof.net/tenantu/';
		 }
		
		
		 
    

});
    </script>
    <style type="text/css">
     #map-canvas {
        height: 400px;
        max-width: 100%;
        /*margin: 0px;*/
        /*padding: 0px*/
    }
  </style>
  
  

  @endsection
