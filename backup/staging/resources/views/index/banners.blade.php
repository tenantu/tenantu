@extends('layouts.frontend-master')
@section('content')
<div class="container">
	@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@if (Session::has('message'))
@include('elements.message')
@endif
@if (Session::has('message-error'))
  @include('elements.message-error')
@endif
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="ad-table">
					<h2>{{$pageTitle}}</h2>
					<p>Banner advertisement offered on TenantU is a unique opportunity for a business to market to local students, at the click of a button. We will help you get your product/service in the face of the people you want to reach when you want to reach them.</p>
					<div class="panel-group ad-table-container table-responsive">
						{{$statusMessage}}
						<table border="0" class="table table-hover">
							<thead class="payment-details-head">
								<tr>
									<th>Banner Name</th>
									<th>Banner Size</th>
									<th>Banner Price</th>
									<th>Sample Image</th>
									<th>Action</th>
								</tr>
						   </thead>
						   <tbody class="payment-details-body">
							   @forelse($bannerDetails as $bannerDetail)
								<tr>
									
									<td class="banner-name">{{$bannerDetail->banner_name}}</td>
									<td>{{$bannerDetail->banner_size}}</td>
									<td><span class="banner-price">${{$bannerDetail->banner_price}}</span>
									<div class="sub-texts">per 30 days</div>
									</td>
									<td><a href="{{ HTML::getSampleBannerImage($bannerDetail->sample_image)}}" target="_blank">View</a></td>
									@if($bannerDetail->availability=='unavailable')
									<td><a class="banner-button ua">{!! Form::button('Unavailable',['id'=>'purchaseButton']) !!}</a></td>
									@else
									<td><a href="{{route('index.purchase',$bannerDetail->id)}}" class="banner-button">{!! Form::button('Buy now',['id'=>'purchaseButton']) !!}</a></td>
									@endif
								</tr>
								@empty
								
								@endforelse
						   </tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
				<div class="sample-banner">
					<img src="{{ asset('public/img/sample-banner.png') }}" class="img-responsive">
					<span>Your image will be look like this</span>
				</div>
			</div>
		</div>
	</div>
	
	@endsection
	
