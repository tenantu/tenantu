@extends('layouts.frontend-master')
@section('content')
<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="forgot-password-container">
					@include('elements.validation-message')
					@if (Session::has('message-error'))
						@include('elements.message-error')
					@endif
					@if (Session::has('message-success'))
						@include('elements.message-success')
					@endif
					{!! Form::open(['route'=>['index.postForgotPassword']]) !!}
						 
						<h2>Forgot Password</h2>
						<!-- <label class="checkbox-text">
						  {!! Form::radio('usertype', 'student') !!}
						   <span class="lbl padding-8">Tenant</span>
						 </label>
						<label class="checkbox-text">
							{!! Form::radio('usertype', 'landlord') !!}
						   <span class="lbl padding-8">Landlord</span>
						</label>-->
						<p>Enter the email address associated with your account. Instructions to reset your password will be sent to you.</p>
						<div class="form-group">
							{!! Form::text('email', Input::old('email'), ['class'=>'form-control login-input','id'=>'email','placeholder'=>'email']) !!}
					    </div>
					   {!! Form::submit('Submit', ['class' => 'btn btn-default login-submit']) !!}
					  <div class="clearfix"></div>
					  <a href="{{ route('index.login') }}" class="link">Back to Login</a>
				   {!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection
