@extends('layouts.frontend-master')
@section('content')
<div class="container">
		@if (Session::has('message'))
			 @include('elements.message')
		@endif
		<div class="row">
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<div class="login-container">
							<h2>Login</h2>
							
							{!! Form::model('',['method' => 'POST','class'=>'form-login','id'=>'loginform','route'=>['index.postLogin']]) !!}
							<label class="checkbox-text">
								<!--{!! Form::radio('usertype', 'student',true) !!}
							   <span class="lbl padding-8">Tenant</span>-->
							</label>
							<label class="checkbox-text">
								<!--{!! Form::radio('usertype', 'landlord') !!}
							   <span class="lbl padding-8">Landlord</span>-->
							</label>
							<img class="fb-login" src="{{ asset('public/img/facebook-login.png') }}" alt="">
							<div class="or">OR</div>
							@if(Session::has('login-failed'))
								@include('elements.validation-message')
							@endif	
							@if (Session::has('message-error'))
							    @include('elements.message-error')
							@endif
							

							
							<div class="form-group">
							{!! Form::text('email', Input::old('email'), ['class'=>'form-control login-input','id'=>'exampleInputEmail1','placeholder'=>'Email']) !!}
						     </div>
						  <div class="form-group">
							 {!! Form::password('password',['class'=>'form-control login-input','id'=>'exampleInputPassword1','placeholder'=>'Password']) !!}
						    
						  </div>
						  {!! Form::submit('Submit', ['class' => 'btn login-submit']) !!}
						  <div class="clearfix"></div>
						  <label class="checkbox-text forgot-password" onclick="#">
							   <span class="lbl padding-8"><a href="{{ route('index.forgotPassword') }}">Forgot Password</a></span>
							</label>
							{!! Form::close() !!}
						</div>
					</div>
					<div class="col-lg-6 col-lg-offset-1 col-md-6 col-sm-7 col-xs-12">
						<div class="signup-container">
							<h2>Create an Account</h2>
							@if(Session::has('reg-failed'))
								@include('elements.validation-message')
							@endif
							@if (Session::has('message-error-signup'))
							    @include('elements.message-error-signup')
							@endif
							@if (Session::has('message-success'))
							    @include('elements.message-success')
							@endif
							{!! Form::open(['class'=>'form-login','route'=>['index.postSignup']]) !!}
								<label class="checkbox-text">
								   {!! Form::radio('usertype', 'student',true) !!}
								   <span class="lbl padding-8">Tenant</span>
								</label>
								<label class="checkbox-text">
									{!! Form::radio('usertype', 'landlord') !!}
								   <span class="lbl padding-8">Landlord</span>
								</label>
								<div class="form-group">
								{!! Form::text('firstname', Input::old('firstname'), ['class'=>'form-control login-input','id'=>'firstname','placeholder'=>'First Name']) !!}
							   
							  </div>
							  <div class="form-group">
								  {!! Form::text('lastname', Input::old('lastname'), ['class'=>'form-control login-input','id'=>'lastname','placeholder'=>'Last Name']) !!}
							    
							  </div>
							  <div class="form-group">
							  <div class="select-box">
								  {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'school']) !!}
							 </div>
							
							  </div>
							  <div class="form-group">
								   {!! Form::email('email',Input::old('email'),['class'=>'form-control login-input','id'=>'email','placeholder'=>'Email']) !!}
							    
							  </div>
							  <div class="form-group">
								  {!! Form::password('password',['class'=>'form-control login-input','id'=>'password','placeholder'=>'Password']) !!}
							    
							  </div>
							  <div class="form-group">
								  {!! Form::password('password_confirmation',['class'=>'form-control login-input','id'=>'confirmpassword','placeholder'=>'Confirm Password']) !!}
							    
							  </div>
							   {!! Form::submit('Create Account', ['class' => 'btn signup-submit']) !!}
							  
							  <div class="links">By creating tenantu account, you acknowledge  
that you agree to our <a href="{{route('pages.showCmsPages','terms-of-use')}}">Terms of Use</a> and <a href="{{route('pages.showCmsPages','privacy-policy')}}">Privacy Policy</a></div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@endsection
	
	
