<?php
$i = 0;

?>

@forelse($properties as $key=> $property)
<?php
   
   $class  = "";
   $statusClass = "";
   if($property->featured==1){
     // $class  = "featured";
      $checkArray[]  = $key;
   }
   if($property->availability_status== 'Not available'){
	   $statusClass = "not-available";
   }
   //print_r($checkArray);
   if( $i<3 && ($property->featured==1) && $page==1){
      $class  = "featured";
      
      
   }
   $i++;

?>
<li class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item indProperty {{ $class }} {{ $statusClass }}" data-slug="{{ $property->slug }}" style="cursor: pointer;">
  
   <div class="propery-img"><img src="{{ HTML::getPropertyImage($property->id,'','','square') }}"></div>
   <div class="details">
      <h4>{{ $property->property_name }}</h4>
      <div>
         <div class="subtitle">{{ $property->landlord->firstname.' '.$property->landlord->lastname }}</div>
         @if(strlen($property->schoolName) > 30)
         <div class="place">
			 {{substr($property->schoolName, 0, 30). '...'}}
		</div>
		 @else
		<div class="place">
			{{$property->schoolName}}
		</div>
		@endif
      </div>
      <div class="property-statistics">
         <div title="Number of views" class="user">{{ $property->most_viewed }}</div>
         <div title="Over all rating" class="rating"><img src="{{ HTML::getAverageReviewRatingProperty($property->id) }}"></div>
         <div title="Expected rent" class="amount">{{ $property->rent_price }}</div>
      </div>
   </div>
</li>

@empty
<p>
   <div class="col-lg-12">No Search results found!</div></p>
@endforelse
