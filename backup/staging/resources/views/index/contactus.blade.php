@extends('layouts.frontend-master')
@section('content')
@if (Session::has('message-success'))
@include('elements.message-success')
@endif
@if (Session::has('message'))
@include('elements.message')
@endif
@if (Session::has('message-error'))
  @include('elements.message-error')
@endif
<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-4 col-xs-12">
			<div class="contact-details">
				<h2>Contact</h2>
				{!! Form::model('',['method' => 'POST','class'=>'form-inline','route'=>['index.postContactUs']]) !!}
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
						<div class="input-group col-sm-5 col-xs-12">
							<label>Name</label>
							{!!Form::text('name','',['class'=>'form-control','id' => 'test','placeholder'=>'Name']) !!}
							
							
						</div>
						<div class="input-group col-sm-5 col-xs-12 col-sm-offset-1">
							<label>Email</label>
							{!! Form::text('email','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Email']) !!}
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="input-group col-sm-11 col-xs-12">
							<label>Select School</label>
							<div class="select-box">
								{!! Form::select('school',['' => 'Select school']+$school, null, ['class'=>'form-control school','id'=>'school']) !!}
								
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="input-group col-sm-11 col-xs-12">
							<label>Reason</label>
							<div class="select-box">
								 {!! Form::select('reason',['' => array('Suggest a property'=>"Suggest a property",'Issue with website'=>"Issue with website",'Career'=>"Career",'Other'=>"Other")], null, ['class'=>'form-control school','id'=>'reason']) !!}
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
						<div class="input-group col-sm-11">
						<label>Message</label>
						{!! Form::textarea('message', null, ['class'=>'form-control text-area','placeholder'=>'Message', 'rows' => 3]) !!}
						
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<button type="submit" class="btn contact-submit">Submit</button>
					</div>
				</div>
			{!!Form::close() !!}
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="location-container clearfix">
			<h3>Location</h3>
				<div id="map-canvas"></div>
			<!--<img src="{{ asset('public/img/map.png') }}" class="img-responsive" width="100%">-->
			<div class="lctn-details">
				<p>293 Broadway</p>
				<p>Bethpage, New York 11714</p>
				<p>Tel : (516) 507-9253</p>
				<p>Email : <a href="mailto:info@tenantu.com">info@tenantu.com</a></p>
			</div>
		</div>
	</div>
</div>
</div>

<script src="{{ asset('public/js/jquery.infinitescroll.js') }}"></script>
<script src="{{ asset('public/js/manual-trigger.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfo5c1VLkhhvEVUb6rUmim3E7N6vtLK1c&signed_in=false&callback=initialize"></script>
  
<script>
var myCenter=new google.maps.LatLng(40.740237,-73.479985);

function initialize()
{
	
var mapProp = {
  center:myCenter,
  zoom:10,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("map-canvas"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<style type="text/css">
     #map-canvas {
        height: 282px;
        width: 100%;
        /*margin: 0px;*/
        /*padding: 0px*/
    }
  </style>
@endsection
