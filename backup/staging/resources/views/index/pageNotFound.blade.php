@extends('layouts.frontend-master')
@section('content')
<div class="container">
		@if (Session::has('message'))
			 @include('elements.message')
		@endif
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-4 pgntfn">
				<div class="clearfix"><img src="{{ asset('public/img/404.png') }}"></div>
				<a href="{{URL::to('/')}}">Return to homepage</a>
			</div>
		</div>
	</div>
	@endsection
	
	
