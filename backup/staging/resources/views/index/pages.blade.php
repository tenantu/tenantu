@extends('layouts.frontend-master')
@section('content')
<div class="container">
	@if (Session::has('message'))
		@include('elements.message')
	@endif
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="about-container">
					<h2>{{ucfirst($pageTitle)}}</h2>
					<p>{!!html_entity_decode($pageContent)!!}</p>
				</div>
			</div>
		</div>
	</div>
	
	@endsection
	
	
