<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<h2>Featured Properties</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<ul class="bxslider">
					
					@forelse( $featuredProperties as $featuredProperty )
					<li>
						<div class="row">
							<div class="col-sm-12 col-xs-12">
								<div class="property">
									<div class="image">
										<a href="{{route('properties.getPropertyDetail',$featuredProperty->slug)}}"><img src="{{ HTML::getPropertyImage($featuredProperty->id,'','','') }}"></a>
									</div>
									<div class="texts">
										<h3><a href="{{route('properties.getPropertyDetail',$featuredProperty->slug)}}">{{ $featuredProperty->property_name }}</a></h3>
										<div class="rt-rws"><img src="{{ HTML::getAverageReviewRatingForLandlord($featuredProperty->id) }}"><a href="{{route('properties.getPropertyDetail',[$featuredProperty->slug,'#reviews'])}}" class="pull-right">Reviews({{$featuredProperty->reviewCount}})</a></div>
									</div>
								</div>
							</div>
						</div>
					</li>
					@empty
					<p>No featured properties!</p>
					@endforelse
				</ul>
			</div>
		</div>
	</div>
