@extends('layouts.frontend-home-master')
<!--  a new section added  latest-->
@section('content')

<div class="container">
	<!-- nav section starts -->
	<!-- //////// nav section ends //////// -->
	@include('elements.right-sidebar')
	<!-- nav section ends -->
	<!-- //////// ends //////// -->
	<!-- man section -->
	<!-- //////// starts //////// -->
	{!! Form::model('',['method' => 'POST','route'=>['index.searchResult'], 'class'=>'home-texts']) !!}
	<div id="wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 logo-small"><a href="index.html" class="tenantu-logo"><img src="{{ asset('public/img/tenantu-logo.png') }}"></a></div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 scl-icons">
					<div class="social-icons">
						<ul>
							<li><a href="{{HTML::getSocialMediaLinks('twitter')}}"><img src="{{ asset('public/img/twitter.png')}}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('facebook')}}"><img src="{{ asset('public/img/facebook.png') }}"></a></li>
							<li><a href="{{HTML::getSocialMediaLinks('instagram')}}"><img src="{{ asset('public/img/instagram.png') }}"></a></li>
							<li><a href="{{route('contactUs')}}"><img src="{{ asset('public/img/mail.png') }}"></a></li>
						</ul>
					</div>
					<div class="top-left-menu">
						<div class="select">
							
							@if(Session::has('schoolName'))
							   {!! Form::select('school_id',['' => 'Select school']+$school,Session::get('schoolName') , ['class'=>'selectpicker show-tick form-control','id'=>'school']) !!}
							@else
						       {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'selectpicker show-tick form-control','id'=>'school']) !!}
						    @endif   
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 logo-large"><a href="{{route('index.index')}}" class="tenantu-logo"><img src="{{ asset('public/img/tenantu-logo.png') }}"></a></div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class=" top-right-menu">
						<div class="login-buttons">
							
							<a href="{{route('index.login')}}">Login / Signup</a>
						</div>
						<span class="nav_trigger"><i class="fa fa-navicon"></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- man section ends-->
	<!-- //////// ends //////// -->
</div>
<!-- home page top -->
<!-- //////// starts //////// -->
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="home-heading">Let's Head Home</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center">
			
			<div class="search-container">
				{!! Form::text('search','', ['placeholder'=>'Search Landlords, Properties, city','id'=>'keyWords']) !!}
				<button type="submit" id="search">Submit</button>
				<div class="signup-text">Landlords can create property listings, manage their rentals, and much more!  <a href="{{route('index.login')}}" class="signup-txt">Sign up here</a></div>
				<div class="signup-text"><a style="font-size:14px; text-transform:none" href="{{ route('pages.showCmsPages','are-you-a-landlord') }}" class="link">Are you a Landlord ?</a> <a style="font-size:14px; text-transform:none" href="{{ route('pages.showCmsPages','are-you-a-tenant') }}" class="link">Are you a Tenant ?</a></div>
			</div>
			
			<div class="homepage-down-arrow">
				<a href="#howitworks"><img src="{{ asset('public/img/down-arrow.png') }}"></a>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}
<!-- home page top -->
<!-- //////// ends //////// -->
<!-- how it works -->
<!-- //////// starts //////// -->
<div class="how-it-works" id="howitworks">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2">
				<h2>What is TenantU?
				<p>We are your one-stop shop for off-campus student housing</p></h2>
				<p class="hw-texts">
					Here at TenantU, we believe that a Happy Landlord = a Happy 
Tenant. That’s why we want to give you the tools to take the stress out of the whole process and assist you by putting everything you need in one, easy to access location.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-5 col-sm-offset-1">
				<h4>Students</h4>
<p class="p-texts">Making your move out of the dorms should be exciting, not draining. Avoid the stressors such as driving around searching for properties and leaving unreturned voicemails to housing offices. From the day you decide to move off-campus to the day you walk across the stage to grab your diploma, make TenantU your only tool for handling all of your living needs!</p>
			</div>
			<div class="col-sm-5 col-sm-offset-1">
				<h4>Landlords</h4>
<p class="p-texts">Connecting with your current and future tenants has never been easier. Keep an eye on your properties in an organized digital space, and be confident in knowing that your tenants are only a click away! Clear communication with your many tenants is vital for both parties, and here at TenantU we provide you with an easy-to-use channel for just that.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 hw-arrows">
				<div class="hw-cntr">
					<div class="text"><img src="{{ asset('public/img/search.png') }}" width="55">
						<span>Search</span></div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 hw-arrows">
					<div class="hw-cntr">
						<div class="text"><img src="{{ asset('public/img/select-arrow.png') }}" width="40"><span>Select</span>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<div class="hw-cntr">
					<div class="text"><img src="{{ asset('public/img/tick.png') }}" width="55"><span>Done</span></div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- how it works -->
<!-- //////// ends //////// -->
<!-- featured projects -->
<!-- //// starts //// -->
@if(count($featuredProperties) > 0)
<a id="featured"> </a>
<div class="featured-properties" id="featuredArea">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-xs-12">
				<h2>Featured Properties</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<ul class="bxslider">
					
					@forelse( $featuredProperties as $featuredProperty )
					<li class="featured">
						<div class="row">
							<div class="col-sm-12 col-xs-12">
								<div class="property">
									<div class="image">
										<a href="{{route('properties.getPropertyDetail',$featuredProperty->slug)}}"><img src="{{ HTML::getPropertyImage($featuredProperty->id,'','','') }}"></a>
									</div>
									<div class="texts">
										<h3><a href="{{route('properties.getPropertyDetail',$featuredProperty->slug)}}">{{ $featuredProperty->property_name }}</a></h3>
										<div class="rt-rws"><img src="{{ HTML::getAverageReviewRatingForLandlord($featuredProperty->id) }}"><a href="{{route('properties.getPropertyDetail',[$featuredProperty->slug,'#reviews'])}}" class="pull-right">Reviews({{$featuredProperty->reviewCount}})</a></div>
									</div>
								</div>
							</div>
						</div>
					</li>
					@empty
					<p>No featured properties!</p>
					@endforelse
				</ul>
			</div>
		</div>
	</div>
</div>
@endif
<!-- featured projects -->
<!-- //// ends //// -->
<!-- contact -->
<!-- //////// starts //////// -->
<div class="contact" id="contact">
	@if (Session::has('message-success'))
	@include('elements.message-success')
	@endif
	@if (Session::has('message-error'))
	@include('elements.message-error')
	@endif
	
	{!! Form::model('',['method' => 'POST','class'=>'container form-inline','id'=>'enquiry','route'=>['index.postEnquiry']]) !!}
	
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
			<h2>Be the first to find out about new houses in your area!</h2>
			<div class="row address">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right txts">293 Broadway,
				Bethpage,<br> New York 11714</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<img src="{{ asset('public/img/mail.png') }}" width="100">
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-left txts">
					<div>Tel : (516) 507-9253</div>
					<div>Email : info@tenantu.com</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
			<!-- <div class="input-group">
							<div class="input-group-addon">
											<span class="glyphicon glyphicon-user" aria-hidden="true"></span>
							</div>
							{!!Form::text('name','',['class'=>'form-control','id' => 'test','placeholder'=>'Name']) !!}
				<!--<input type="text" class="form-control" id="exampleInputAmount" placeholder="Name">-->
				<!-- </div> -->
				<div class="row">
					<div class="col-lg-6 col-sm-10 col-xs-10 col-lg-offset-3 col-sm-offset-1 col-xs-offset-1">
						<div class="input-group col-lg-9 col-sm-8 col-xs-10">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
							</div>
							{!! Form::text('email','',['id' => 'exampleInputAmount','class'=>'form-control','placeholder'=>'Email']) !!}
							
						</div>
						<button type="submit" class="btn btn-primary contact-submit">Submit</button>
					</div>
					<!-- <div class="col-sm-2">
							<button type="submit" class="btn btn-primary contact-submit">Submit</button>
					</div> -->
				</div>
				<!-- <div class="input-group">
								<div class="input-group-addon">
												<span class="glyphicon glyphicon-education" aria-hidden="true"></span>
								</div>
								{!! Form::select('school',['' => 'Select school']+$school, null, ['class'=>'form-control school','id'=>'school','placeholder'=>'School']) !!}
				</div> -->
			</div>
		</div>
		<!-- <div class="row">
						<div class="col-lg-12 col-md-12 col-sm-8 col-xs-12 col-sm-offset-2 col-md-offset-0 col-xs-offset-0 col-lg-offset-0">
										{!! Form::textarea('message', null, ['class'=>'form-control text-area','placeholder'=>'Message', 'rows' => 3]) !!}
						</div>
		</div> -->
		<!-- <div class="row">
						<div class="col-lg-12 text-center">
										<button type="submit" class="btn btn-primary contact-submit">Submit</button>
						</div>
		</div> -->
		{!!Form::close() !!}
	</div>
	<!-- testimonials -->
	@if(count($testimonials)>0)
	<div class="testimonial-container">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-xs-12">
					<h2>Testimonials</h2>
				</div>
			</div>
			
			 <div class="row testimonials">
				 @foreach($testimonials as $testimonial)
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col">
					<p class="texts"> {{ strip_tags($testimonial->description) }} </p>
					<div class="test-name">{{ $testimonial->client_name }}</div>
					<div class="test-company-name">{{ $testimonial->company_name }}</div>
				</div>
				@endforeach
			</div>
			
		</div>
	</div>
	@endif
	@endsection
	@section('page-scripts')
	<script type="text/javascript">
		
		$(function(){
		$(document).on('change','#school',function(event){
			var school = $("#school").val();
				
				$.ajax({
					type: "GET",
                    url : "{{route('index.indexAjax')}}",
                    dataType :"json",
                    data : "school_id="+school+"&page=home",
                    success : function(data){
						
						var objFeaturedProperties 		= data.featuredProperties;
						var objFeaturedPropertiesCount 	= data.featuredPropertiesCount;
						var objSchoolImage 				= data.schoolImage;
						
						$('.home').css({backgroundImage: "url("+objSchoolImage+")"});
						$('#school_div').addClass('home-school-bg');
						
						if(objFeaturedPropertiesCount == '0')
						{
							$('#featuredArea').hide();
						}
						else
						{
							$('#featuredArea').show();
							$('#featuredArea').html(objFeaturedProperties);
							$('.bxslider').bxSlider({
									slideWidth: 240,
									minSlides: 2,
									maxSlides: 4,
									slideMargin: 30
								});
						}
						
                    }
				});
			});
			
		});
		
	</script>
	
	@endsection
	
