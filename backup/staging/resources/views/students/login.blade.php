@if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>        
            @endforeach
        </div>
        @endif
	{!! Form::model('',['method' => 'POST','route'=>['students.postLogin']]) !!}

    <div>
        {!! Form::label('Email', 'Email:') !!}<span style="color: red;">*</span>
        {!! Form::text('email', Input::old('name'), ['class'=>'form-control']) !!}
    </div>

    <div>
         {!! Form::label('Email', 'Password:') !!}<span style="color: red;">*</span>
         {!! Form::password('password',['class'=>'form-control']) !!}
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div>
        {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
    </div>
{!! Form::close() !!}
