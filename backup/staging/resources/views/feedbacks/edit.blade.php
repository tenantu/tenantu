@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    <script type="text/javascript" src="{{ asset('public/js/tinymce/tinymce.min.js') }}"></script>
	<script type="text/javascript">
	  tinymce.init({
		selector : "textarea",
		plugins : ["advlist autolink lists link image charmap print preview anchor", "searchreplace visualblocks code fullscreen", "insertdatetime media table contextmenu paste"],
		toolbar : "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	  }); 
	</script>

    {!! Form::model($feedback,['method' => 'POST','route'=>['feedback.update',$feedback->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('ClientName', 'Client Name:') !!}<span style="color: red;">*</span>
            {!! Form::text('clientname', $feedback->client_name, ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('CompanyName', 'CompanyName:') !!}<span style="color: red;">*</span>
            {!! Form::text('companyname',$feedback->company_name, ['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Content', 'Content:') !!}<span style="color: red;">*</span>
            {!! Form::textarea('content',$feedback->description,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
