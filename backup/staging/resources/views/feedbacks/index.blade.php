@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
        {!! Form::model($search, ['method' => 'GET']) !!}
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                    {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>

            </div>
        </div>
        {!! Form::close() !!}
        <div class="col-md-2 pull-right">
            <br/>
            <a class="btn btn-block btn-primary" href="{{ route('feedback.create') }}">
                <i class="fa fa-plus-circle"></i> &nbsp;New Testimonial
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
            
        </div>
        <div class="col-md-8 text-right">
        {!! $feedbacks->appends(['search' => $search, 'sortby' => $sortby, 'order' => ($order == 'desc')?'asc':'desc'])->render() !!}
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
               
                <th class="">#</th>
                <th class="col-md-4">
                    <a >Client Name</a>
                </th>
                
                <th class="col-md-4">
                        <a >Company Name</a>
                </th>
                    
				<th class="col-md-1">
					<a >Status</a>
			    </th>
                
                <th class="col-md-3 text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
			$feedback = $feedbacks->currentPage();
			$slNo = (($feedback - 1) * 10) + 1;
		?>
        @foreach ($feedbacks as $feedback)
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$feedback->client_name}}</td>
                <td>{{$feedback->company_name}}</td>
                <td>
                @if ($feedback->status == 0)
                    <a href="{{route('feedback.changeStatus',$feedback->id)}}"><small class="label label-danger"> In Active </small></a>
                @else
                    <a href="{{route('feedback.changeStatus',$feedback->id)}}"><small class="label label-success"> Active </small></a>
                @endif
                </td>
                <td class="text-center">
				{!! Form::open(['method' => 'DELETE', 'route' => ['feedback.destroy', $feedback->id]]) !!}
					<a href="{{route('feedback.edit',$feedback->id)}}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a> &nbsp;
					<button type="submit" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash"></i> Delete</a></button>
				{!! Form::close() !!}
                </td>
            </tr>
            <?php
			$slNo++;
			?>
        @endforeach
        </tbody>
    </table>
   
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
