@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
@if($message == 'On Going')
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenantName}}, {{$landLordName}} has responded to your most recent request and is working on solving the issue.	</td>
</tr>
@elseif($message == 'Completed')
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$tenantName}}, {{$landLordName}} has satisfied your most recent request.
	</td>
</tr>
@endif
<tr>
	<td style="padding: 0 0 20px;">
		If you haven't made such a request or if you have any questions, just email us at  {{ Config::get('constants.SUPPORT_EMAIL') }}<br/>
		Please note: This e-mail message is an automated notification. Please do not reply to this message.
	</td>
</tr>

@endsection
