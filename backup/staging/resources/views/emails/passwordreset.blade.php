@extends('layouts.email-master')
@section('email-content')

<tr>
	<td style="padding: 0 0 20px;">
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		Hi {{$name}}, 
	</td>
</tr>

<tr>
	<td  style="padding: 0 0 20px;">
		Click here or copy and paste the link to reset your password: <a href="{{ url('index/resetpassword/'.$token.'/'.$userType) }}">{{ url('index/resetpassword/'.$token.'/'.$userType) }}</a>
	</td>
</tr>
<tr>
	<td style="padding: 0 0 20px;">
		If you haven't made such a request or if you have any questions, just email us at  {{ Config::get('constants.SUPPORT_EMAIL') }}<br/>
			Please note: This e-mail message is an automated notification. Please do not reply to this message.
	</td>
</tr>

@endsection
