<!-- resources/views/emails/password.blade.php -->

<b> {{ Config::get('constants.SITE_NAME') }}</b>
<hr>
<br>
Hi, 
<br><br>
You are receiving this email because we have received a password reset request for your account.
<br>
<br>
@if(isset($role)&&($role==3))
	Click here to reset your password: {{ url('users/reset/'.$token) }}
@else
	Click here to reset your password: {{ url('admin/password/reset/'.$token) }}
@endif
<br><br>
If you haven't made such a request or if you have any questions, just email us at  {{ Config::get('constants.SUPPORT_EMAIL') }}.
<br><br>

<hr>
Please note: This e-mail message is an automated notification. Please do not reply to this message.
