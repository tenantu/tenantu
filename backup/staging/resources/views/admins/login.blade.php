@extends('layouts.login-master')

@section('content')

    <div class="login-box">

		<div class="login-logo">
			<a href="{{ url('admin') }}"><b>Tenantu</b></a>
		</div>

        <div class="login-box-body">

        	@if (count($errors) > 0)
				@include('elements.validation-message')
			@else
			@if (Session::has('message-error'))
				@include('elements.message-error')
			@endif
				<p class="login-box-msg">Log in to start your session.</p>
			@endif

            {!! Form::open( ['route' => 'admins.postLogin'] ) !!}

                <div class="form-group has-feedback">
                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    
                    <!-- /.col -->
                    <div class="col-xs-4">
                        {!! Form::submit('Log In', ['class' => 'btn btn-primary btn-block btn-flat']) !!}
                    </div>
                    <!-- /.col -->
                </div>

            {!! Form::close() !!}

            

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@endsection

@section('page-scripts')
    <script src="{{ asset('public/js/auth/login.js') }}" type="text/javascript"></script>
@endsection
