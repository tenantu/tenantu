@extends('layouts.admin-master')


@section('admin-content')
@if (Session::has('message-success'))
 @include('elements.message-success')
@endif
	Welcome {{ ucwords(Auth::user('admin')->name) }}, <br/>
    You can use the navigation links on the left navigation bar to manage your account.
@endsection

