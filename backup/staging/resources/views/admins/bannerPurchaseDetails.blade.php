@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($bannerPurchaseDetails,['method' => 'POST','files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('Name', 'Name:') !!}<span style="color: red;"></span>
            {{ $bannerPurchaseDetails->name }}
        </div>
       <div class="form-group">
            {!! Form::label('BannerName', 'Banner Name:') !!}<span style="color: red;"></span>
            {{ $bannerPurchaseDetails->banner_name }}
        </div>
       
        
        <div class="form-group">
            {!! Form::label('Purchase Date', 'Purchase Date:') !!}<span style="color: red;"></span>
            {{date('d M Y',strtotime($bannerPurchaseDetails->created_at))}}
        </div>
        
        @if(($bannerPurchaseDetails->status == 1) && ($bannerPurchaseDetails->activated_date!='0000-00-00'))
         <div class="form-group">
            {!! Form::label('Activated Date', 'Activated Date:') !!}<span style="color: red;"></span>
            {{ date('d M Y',strtotime($bannerPurchaseDetails->activated_date)) }}
        </div>
        <?php 
			$date = strtotime($bannerPurchaseDetails->activated_date);
			$date = strtotime("+30 day", $date);
			//echo date('M d, Y', $date);
        ?>
         <div class="form-group">
            {!! Form::label('Expired On', 'Expired On:') !!}<span style="color: red;"></span>
           {{ date('d M Y', $date)}}
        </div>
        @endif
        <div class="form-group">
            {!! Form::label('Purchase amount', 'Purchase amount:') !!}<span style="color: red;"></span>
            ${{$bannerPurchaseDetails->amount_paid}}
        </div>
        
         <div class="form-group">
            {!! Form::label('Banner status', 'Banner status:') !!}<span style="color: red;"></span>
            @if($bannerPurchaseDetails->status == 1)
					  Active 
					 @else
					  Inactive 
					 @endif
        </div>
        <div class="form-group">
            {!! Form::label('Payment status', 'Payment status:') !!}<span style="color: red;"></span>
            @if($bannerPurchaseDetails->payment_status == '')
					  Not paid  
					 @else
					  {{ $bannerPurchaseDetails->payment_status }}
					 @endif
        </div>
        
        <div class="form-group">
            {!! Form::label('Banner image', 'Uploaded Banner image:') !!}<span style="color: red;"></span>
            <a href="{{$bannerImage}}" target="_blank"><img src="{{HTML::getBannerImage($bannerPurchaseDetails->image)}}" style="width:85px;height:65px;"></a>
        </div>
        
      
       
        <div class="form-group">
			@if($bannerPurchaseDetails->status == '0')
			<a href= "{{ route('admins.activateBannerPurchase',$bannerPurchaseDetails->id) }}">{!! Form::button('Activate',['class' => 'btn btn-primary','title'=>'Click here to activate the advertisement purchase','onClick' => 'return confirm("Do you really want to activate this banner?after activate it will appear on userside");']) !!}</a>
			@else
			<a href= "{{ route('admins.deActivateBannerPurchase',$bannerPurchaseDetails->id) }}">{!! Form::button('Deactivate',['class' => 'btn btn-primary','title'=>'Click here to Inactivate the advertisement purchase','onClick' => 'return confirm("Do you really want to deactivate this banner ?");']) !!}</a>
			@endif
			
			 <a href= "{{ route('admins.bannerPurchases') }}">{!! Form::button('Go Back',['class' => 'btn btn-primary']) !!}</a>
        </div>
        
       

     {!! Form::close() !!}

@endsection

