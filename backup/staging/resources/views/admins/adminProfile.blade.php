@extends('layouts.admin-master')

@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message-error'))
		@include('elements.message-error')
	@endif

    {!! Form::model('',['method' => 'POST','route'=>['admins.adminEditProfile'],'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('Username(Email address)', 'Username(Email address):') !!}<span style="color: red;">*</span>
            {!! Form::text('email',$admin->email , ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('New Password', 'New password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password', ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Confirm new password', 'Confirm new password:') !!}<span style="color: red;">*</span>
            {!! Form::password('password_confirmation',  ['class'=>'form-control']) !!}
        </div>
        
        
        <div class="form-group">
            {!! Form::submit('Change', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

