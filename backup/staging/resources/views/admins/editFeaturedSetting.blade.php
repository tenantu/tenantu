@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($featured,['method' => 'POST','route'=>['admins.updateFeaturedSettings',$featured->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('Duration', 'Duration:') !!}<span style="color: red;">*</span>
            {{ $featured->duration }}
        </div>
       <div class="form-group">
            {!! Form::label('price', 'Price:') !!}<span style="color: red;"></span>
            {!! Form::text('price',null,['class'=>'form-control']) !!}
        </div>
       
        
      
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

