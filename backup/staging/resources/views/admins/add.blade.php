@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
		@include('elements.breadcrump-landlord')
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-8 col-xs-12">
              <!-- Poist property -->
              <div class="tnt-box landlord-add-propety">
                <div class="box-heads">
                  <h4>Post Property</h4>
                </div>
                
                @include('elements.validation-message')
               
                @if (Session::has('message-success'))
                  @include('elements.message-success')
               @endif
                {!! Form::model('',['method' => 'POST','class'=>'add-property-details','id'=>'propertyAddForm','route'=>['property.postadd'],'files'=>true]) !!}
                {!! Form::hidden('img_name', '', ['class'=>'form-control login-input','id'=>'imgName']) !!}
                {!! Form::hidden('hdPlace', '', ['id'=>'hdPlace']) !!}
                  <div class="row">
                    <div class="form-group col-lg-10 col-md-10 col-sm-12 col-xs-12">
                      {!! Form::label('propertytitle', 'Property Title:') !!}<span style="color: red;">*</span>
                      {!! Form::text('property_name', Input::old('property_name'), ['class'=>'form-control login-input','id'=>'propertytitle','placeholder'=>'Property Title','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('schoolname', 'School Name:') !!}<span style="color: red;">*</span>
                      <div class="select-box">
                      {!! Form::select('school_id',['' => 'Select school']+$school, null, ['class'=>'form-control login-input','id'=>'schoolname','required']) !!}
                      </div>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('distance_from_school', 'Distance From School:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','distance_from_school', Input::old('distance_from_school'), ['class'=>'form-control login-input','id'=>'distanceFromSchool','placeholder'=>'Distance from school','required']) !!}
                    </div>
                     <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('bedroom', 'Bedrooms:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','bedroom_no', Input::old('bedroom_no'), ['class'=>'form-control login-input','id'=>'bedroomNo','placeholder'=>'Bedrooms','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('bathroom', 'Bathrooms:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','  bathroom_no', Input::old('bathroom_no'), ['class'=>'form-control login-input','id'=>' bathroomNo','placeholder'=>'Bathrooms','required']) !!}
                    </div>
                     <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('rentprice', 'Rent:') !!}<span style="color: red;">*</span>
                      {!! Form::input('number','rent_price', Input::old('rent_price'), ['class'=>'form-control login-input','id'=>'rentPrice','placeholder'=>'Rent','required']) !!}
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      {!! Form::label('location', 'Location:') !!}<span style="color: red;">*</span>
                      {!! Form::text('location', Input::old('location'), ['class'=>'form-control login-input','id'=>'location','placeholder'=>'Location','required']) !!}
                    </div>

                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      {!! Form::label('expireon', 'Expire on:') !!}<span style="color: red;">*</span>
                      {!! Form::text('expireon', Input::old('expireon'), ['class'=>'form-control login-input','id'=>'expireon','placeholder'=>date('Y-m-d'),'data-provide'=>'datepicker','required','required' ]) !!}
                      <!-- <label for="expireon">Expire on</label> -->
                      <!-- <input type="text" data-provide="datepicker"  data-date-format="mm/dd/yyyy" class="form-control date" id="expireon" placeholder="00/00/00"> -->
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('amenities', 'Amenities:') !!} (Select items from the options)<span style="color: red;"></span>
                      <p></p>
                        {!! Form::select('aminity[]',$aminities,'', ['class'=>'form-control login-input','id'=>'aminity','multiple']) !!}
                    </div>
                    
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('Property images and documents', 'Property images and documents:') !!}<span style="color: red;"></span>
                      <div class="bg-danger">
                        After saving, you can add the images and documents
                        </div>
                    </div>
                    
                    <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::label('description', 'Property description:') !!}<span style="color: red;">*</span>
                      {!! Form::textarea('description', Input::old('description'), ['class'=>'form-control login-input','id'=>'description','placeholder'=>'Description','rows'=>'3','required']) !!}
                    </div>
                    
                   
                  </div>
                    {!! Form::label('communicationMedium', 'Communication Medium:') !!}<span style="color: red;">*</span>
                    <div class="radio-container">
                  <div class="radio">
                    <label>
                      {!! Form::radio('communication_medium', 'W', ['id'=>'optionsRadios1']) !!}
                      <span class="radio-text">Web</span>
                    </label>
                  </div>

                  <div class="radio">
                      <label>
                        {!! Form::radio('communication_medium', 'E', ['id'=>'optionsRadios2','required']) !!}
                        <span class="radio-text">Email</span>
                      </label>
                  </div>
                </div>
                  <!-- <label class="checkbox-text"> -->
                  <!-- {!! Form::radio('communication_medium', 'W') !!} -->
                  <!-- <span class="lbl padding-8">Web</span> -->
                  <!-- </label> -->
                  <!-- <label class="checkbox-text">
                  {!! Form::radio('communication_medium', 'E') !!}
                   <span class="lbl padding-8">Email</span>
                  </label> -->
                  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    

                  </div>

                  <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      {!! Form::submit('Save', ['class' => 'btn btn-primary add-button start']) !!}
                      {!! Form::submit('Cancel', ['class' => 'btn cancel-button']) !!}
                    </div>
                  </div>

                  
                {!! Form::close() !!}

              </div>
            </div><!-- ./col -->
            <div class="col-lg-5 col-md-5 col-sm-4 col-xs-12">
              <!-- small box -->
              <div class="tnt-box view-property add-view-property">
                <div>
                  <div class="box-heads">
                    <h4>View Properties</h4>
                    <a href="#" class="blue">More »</a>
                  </div>
                  <ol>

                  @if($properties->count(0))
                     @foreach($properties as $key => $property)
                  <li>
                  <span class="number">{{ ($key+1) }}.</span>{{ $property->property_name }}
                  <a href="{{ route('property.edit',$property->id) }}" class="pull-right">Details</a></li>
                  <li>
                    @endforeach
                    @else
                      I don't have any records!
                  @endif

                </ol>
                </div>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          
          <!-- test div -->
        

          </div>
          <!-- test div ends here -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      @endsection
      @section('page-scripts')
      <!-- -------google location css-js files------------------- -->
 <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
 <link href="{{ asset('public/css/jquery.tagit.css') }}" rel="stylesheet" type="text/css"> 
 <link href="{{ asset('public/css/tagit.ui-zendesk.css') }}" rel="stylesheet" type="text/css"> 
 <link href="{{ asset('public/css/jquery.multiselect.css') }}" rel="stylesheet" type="text/css"> 
 <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
  <script src="{{ asset('public/js/tag-it.js') }}" type="text/javascript" charset="utf-8"></script>
 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script> 
 <!-- -------google location css-js files------------------- -->
 
 
  <script src="{{ asset('public/js/jquery.multiselect.js') }}"></script>
 
 <script type="text/javascript">
 if( window.location.hostname == 'localhost'){
  var baseUrl = 'http://localhost/tenantudev/';
 }else if( window.location.hostname == 'fodof.net') {
  var baseUrl = 'http://fodof.net/tenantu/';
 }
  var jqxhr='';
 //var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];
 $("#location").tagit({
        allowSpaces: true,
        fieldName: "location[]",
        tagLimit: 1,
        autocomplete: {
          delay: 0,
          minLength: 2,
          source: function(request, response) {
            var callback = function (predictions, status) {
              if (status != google.maps.places.PlacesServiceStatus.OK) {
                return;
              }         
              var data = $.map(predictions, function(item) {
                return item.description;
              });
              response(data);
            }   
            var service = new google.maps.places.AutocompleteService();
            service.getQueryPredictions({ input: request.term }, callback);
          }
        },
        beforeTagAdded: function (event, ui) {
                        getGeoLocations(ui.tagLabel,false);
                    },
                    beforeTagRemoved: function (event, ui) {
                        // do something special
                        console.log(ui.tagLabel);
                        getGeoLocations(ui.tagLabel,true);
                    }
      });
      $("#courseLocation").tagit({
        fieldName: "location[]"
      });
      
      var jqxhr='';
      var skills= [];
      
     $('select[multiple]').multiselect({
			columns: 1,
			placeholder: 'Select options'
		});
      
      
        function getGeoLocations(address, remove) {

                    if (address === '') {
                        return false;
                    }

                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'address': address}, function (results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var location = results[0].geometry.location.lat() + '~' + results[0].geometry.location.lng()
                            var storedLocation = $("#hdPlace").val();
                            if (remove !== true) {
                                if (storedLocation === '') {
                                    $("#hdPlace").val(location);
                                } else {
                                    $("#hdPlace").val(storedLocation + ',' + location);
                                }
                            }else{
                                
                                storedLocation = storedLocation.replace(location,'');
                                storedLocation = storedLocation.replace(',,',',');
                                $("#hdPlace").val(storedLocation)
                            }

                        }

                    });
                    return false;
                }
                 $('#courseLocation .ui-autocomplete-input').attr('placeholder','Cities, States, Countries, or Zip/Postal Codes');
                 </script>
      @endsection
