@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
        {!! Form::model($search, ['method' => 'GET','route'=>['propertyreview.index']]) !!}
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                    {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>

            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
            
        </div>
        <div class="col-md-8 text-right">
        {!! $properties->appends(['search' => $search, 'sortby' => $sortby, 'order' => ($order == 'desc')?'asc':'desc'])->render() !!}
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
               
                <th class="">#</th>
                <th class="col-md-4">
					<a >Property Name
					</a>
                </th>
                <th class="col-md-4">
                     <a >Review title</a>
                 </th>
                    
				<th class="col-md-4">
					<a >Posted by
					</a>
				</th>
				
				<th class="col-md-4">
					<a >Posted date
					</a>
				</th>
                    
				<th class="col-md-1">
					<a >Status</a>
				</th>
                
                <th class="col-md-3 text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
			$page = $properties->currentPage();
			$slNo = (($page - 1) * 10) + 1;
		?>
        @foreach ($properties as $property)
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$property->property_name}}</td>
                <td>{{$property->title}}</td>
                <td> {{ucfirst($property->firstname).' '. ucfirst($property->lastname) }}</td>
                <td> {{date('d M Y',strtotime($property->created_at))}}</td>
                
                <td>
                @if ($property->status == 0)
                    <a href="{{ route('propertyreview.changeStatus',$property->id) }}"><small class="label label-danger"> In Active </small></a>
                @else
                    <a href="{{ route('propertyreview.changeStatus',$property->id) }}"><small class="label label-success"> Active </small></a>
                @endif
                </td>
                
                <td class="text-center">
					<a href="{{ route('propertyreview.edit',$property->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a> &nbsp;
                </td>
            </tr>
            <?php
$slNo++;
?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * 10) + 1}} to {{ $slNo-1 }} of {!! $properties->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $properties->render()) !!}
        </div>
    </div>
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
