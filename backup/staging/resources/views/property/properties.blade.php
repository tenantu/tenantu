@extends('layouts.landlord-inner')
@section('inner-content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
   
    @include('elements.breadcrump-landlord')
    @if (Session::has('message-success'))
          @include('elements.message-success')
    @endif
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row my-properties">
			@forelse( $properties as $property )  
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tnt-box tenants">
                      <div class="property row">
                        <div class="col-l-4 col-md-4 col-sm-5 col-xs-12">
                          <div class="tenant-image"><img src="{{ HTML::getPropertyImage($property->id,'',100) }}"></div>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                          <h4>{{ $property->property_name }}</h4>
                          <div class="tenant-location"><i class="fa fa-map-marker"></i> {{ $property->location }}</div>
                          <div class="posted-on">Posted on : <span>{{ date('m-d-Y',strtotime($property->created_at)) }}</span></div>
                        </div>
                      </div>
                      <div class="buttons-cntr">
                        <a href="{{ route('property.edit',$property->id) }}" class="buttons"><i class="fa fa-pencil"></i> Edit Info</a>
                        <a href="{{ route('landlords.tenants',$property->id) }}" class="buttons"><i class="fa fa-users"></i> Tenants</a>
                        <a href="{{ route('payment.getTenantPayment') }}" class="buttons"><i class="fa fa-paypal"></i> Payment</a>
                        <!-- <a href="#" class="buttons"><i class="fa fa-comments"></i></a> -->
                      </div>
                </div><!-- /.box -->
            </div>
            @empty
				<p>No Properties yet!</p>
            @endforelse
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
              <div class="add-property-links">
                <div class="add-prpty"><a href="{{ route('property.add') }}">Post New</a></div>
              </div>
            </div>
          </div><!-- /.row -->

          <div>
          
        </section><!-- /.content -->
		<!-- @include('elements.landlord-rightarea') -->
      </div><!-- /.content-wrapper -->
      @endsection
