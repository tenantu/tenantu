@extends('layouts.admin-master')


@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($property,['method' => 'POST','route'=>['properties.update',$property->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('PropertyName', 'PropertyName:') !!}<span style="color: red;">*</span>
            {!! Form::text('property_name',null,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('Location', 'Location:') !!}<span style="color: red;">*</span>
            {!! Form::text('location',null,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('LandlordName', 'LandlordName:') !!}<span style="color: red;"></span>
            <a href="{{route('landlords.edit',$landlord->id)}}">{{ucfirst($landlord->firstname).' '. ucfirst($landlord->lastname) }}</a>
        </div>
        
        <div class="form-group">
            {!! Form::label('Rent Price', 'Rent Price:') !!}<span style="color: red;">*</span>
            {!! Form::text('rent_price',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Bath room', 'Bath room:') !!}<span style="color: red;">*</span>
            {!! Form::text('bathroom_no',null,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Bed room', 'Bed room:') !!}<span style="color: red;">*</span>
            {!! Form::text('bedroom_no',null,['class'=>'form-control']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('featured', 'Featured:') !!}
            {!! Form::checkbox('featured', 1, Input::old('featured'), ['class'=>'']) !!}
        </div>
        
        <div class="form-group">
            {!! Form::label('Property Images', 'Property Images:') !!}
            @forelse($propertyImages as $propertyImage)
				<?php
				$style="";
				$title = "";
				$imgUrl = Config::get('constants.PROPERTY_IMAGE_URL').'/thumb/'.$propertyImage->image_name; 
				if($propertyImage->featured== 1)
				{
					$style = "border:2px solid red;";
					$title = "featured image";
				}
				
				?>
				<img style="width:150px;height:150px; <?php echo $style ?>" title="<?php echo $title ?>"  src="{{ $imgUrl }}">
            @empty
				<div>No images for this property</div>
            @endforelse
        </div>
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')
<script>
	{{-- var imageUrl = '{{ $imageUrl }}/{{ $tenant->id }}/{{ $tenant->image }}';
    var id= {{ $tenant->id }};
    var image='{{ $tenant->image }}';
    $(document).ready(function() {
        $('.fileinput-remove-button').click(function() {
            if(confirm("Are you sure you want to delete this image?"))
            {
                 $.ajax({
                      url: '../deleteImage',
                      type: "post",
                      data :'user_id='+ id ,
                      success: function(data){
                         alert('Removed Successfully');
                        }
                    });
             }
        });
    });
    CKEDITOR.replace('editor');
    $(function() {
        $('input[type="radio"].minimal').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('input').iCheck({
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
    if(image) {
     $("#image").fileinput({
            showUpload: false,
            allowedFileExtensions:['jpg', 'gif', 'png'],
            initialPreview: [
                '<img src='+imageUrl+' class="file-preview-image" alt="{{ $tenant->image }}" title="{{ $tenant->image }}">'
            ],
            initialCaption: "{{ $tenant->image }}"
        });
    }
    else {
       $("#image").fileinput({
            showUpload: false,
            allowedFileExtensions:['jpg', 'gif', 'png'],
        });
    } --}}
    
      $("#location").tagit({
            allowSpaces: true,
            fieldName: "locations[]",
            requireAutocomplete: true,
            autocomplete: {
                delay: 0,
                minLength: 2,
                source: function(request, response) {
                    var callback = function (predictions, status) {
                        if (status != google.maps.places.PlacesServiceStatus.OK) {
                            return;
                        }
                        var data = $.map(predictions, function(item) {
                            return item.description;
                        });
                        currentlyValidTags = data;
                        response(data);
                    }
                    var service = new google.maps.places.AutocompleteService();
                    service.getQueryPredictions({ input: request.term }, callback);
                }
            }
        });
</script>
@endsection
