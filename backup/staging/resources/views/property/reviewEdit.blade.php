@extends('layouts.admin-master')
@section('admin-content')

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif

    {!! Form::model($property,['method' => 'POST','route'=>['propertyreview.update',$propertyReview->id],'files'=>true]) !!}
         
        <div class="form-group">
            {!! Form::label('PropertyName', 'PropertyName:') !!}<span style="color: red;"></span>
            {{$property->property_name}}
        </div>
       <div class="form-group">
            {!! Form::label('Review Title', 'Review Title:') !!}<span style="color: red;"></span>
            {!! Form::text('title',$propertyReview->title,['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('Review Description', 'Review Description:') !!}<span style="color: red;"></span>
            {!! Form::textarea('review',$propertyReview->review,['class'=>'form-control']) !!}
        </div>
       <div class="form-group">
            {!! Form::label('PostedBy', 'PostedBy:') !!}<span style="color: red;"></span>
            {{ucfirst($tenant->firstname).' '. ucfirst($tenant->lastname) }}
        </div>
        
        <div class="form-group">
            {!! Form::label('Rating', 'Rating:') !!}<span style="color: red;"></span>
        </div>
         <div class="form-group">
			 @foreach($propertyRating as $proRate)
				{{$proRate->rating_name}} &nbsp;&nbsp;&nbsp; <img src='{{HTML::getAdminReviewRating($proRate->rate_value)}}'> <br>
			 @endforeach
		 </div>
       
        <div class="form-group">
            {!! Form::label('Posted Date', 'Posted Date:') !!}<span style="color: red;"></span>
            {{date('d M Y',strtotime($propertyReview->created_at))}}
        </div>
        
        
       
        <div class="form-group">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>

     {!! Form::close() !!}

@endsection

@section('page-scripts')

@endsection
