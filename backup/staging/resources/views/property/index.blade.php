@extends('layouts.admin-master')
@section('admin-content')
<div class="users-index">

    @if ($errors->any())
        <div class="alert alert-danger alert-dismissable">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ( $errors->all() as $error )
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (Session::has('message'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {{ Session::get('message') }}
        </div>
    @endif
    
    @if (Session::has('message-success'))
		@include('elements.message-success')
	@endif

    <div class="row">
        {!! Form::model($search, ['method' => 'GET','route'=>['properties.index']]) !!}
        <div class="col-md-4">
            <br/>
             <div class="input-group">
                    {!! Form::text('search',  $search,['class'=>'form-control', 'placeholder'=>'Search']) !!}
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                    </span>

            </div>
        </div>
        {!! Form::close() !!}
        
    </div>
    <div class="row">
        <div class="col-md-4">
            <br/>
           
        </div>
        <div class="col-md-8 text-right">
        {!! $properties->appends(['search' => $search, 'sortby' => $sortby, 'order' => ($order == 'desc')?'asc':'desc'])->render() !!}
        </div>
    </div>
    <table class="table table-bordered table-striped table-hover article-list">
        <thead>
            <tr>
                
                <th class="">#</th>
                <th class="col-md-4">
                   <a>Property Name</a>
                </th>
                <th class="col-md-4">
                    <a>Location</a>
                 </th>
                    
                    <th class="col-md-4">
                        <a >Landlord Name
                        </a>
                    </th>
                    <th class="col-md-4">
                        <a >Featured
                        </a>
                    </th>
                    
                    <th class="col-md-1">
                @if ($sortby == 'status')
                        <a href="{{ url('admin/properties/index?sortby=status&order=' . $order .'&search=' . $search ) }}">Status
                            <i class="fa fa-fw fa-sort-alpha-{!! $order !!}"></i>
                        </a>
                    @else
                        <a href="{{ url('admin/properties/index?sortby=status&order=asc' .'&search=' . $search ) }}">Status</a>
                    @endif</th>
                
                <th class="col-md-3 text-center" colspan="2">Action</th>

            </tr>
        </thead>
        <tbody>
        <?php
$page = $properties->currentPage();
$slNo = (($page - 1) * 10) + 1;
?>
        @foreach ($properties as $property)
            <tr>
                
                <th scope="row">{{$slNo}}</th>
                <td>{{$property->property_name}}</td>
                <td>{{$property->location}}</td>
                <td> {{ucfirst($property->firstname).' '. ucfirst($property->lastname) }}</td>
                @if($property->featured == 1)
                <td> <img style="width:50px;height:50px;" src="{{ asset('public/img/check.png')}}"></td>
                @else
                <td> </td>
                @endif
                <td>
                @if ($property->status == 0)
                    <a href="{{ route('properties.changeStatus',$property->id) }}"><small class="label label-danger"> In Active </small></a>
                @else
                    <a href="{{ route('properties.changeStatus',$property->id) }}"><small class="label label-success"> Active </small></a>
                @endif
                </td>
                
                <td class="text-center">
					<a href="{{ route('properties.edit',$property->id) }}" class="btn btn-link"><i class="fa fa-edit"></i> Edit</a> &nbsp;
                </td>
                <td class="text-center">
                    <a href="{{ route('properties.delete',$property->id) }}" class="btn btn-link" onclick="return confirm('Are you sure you want to delete this property,This property will remove from landlords and tenants profile?');"><i class="fa fa-trash"></i> Delete</a> &nbsp;
                </td>
            </tr>
            <?php
$slNo++;
?>
        @endforeach
        </tbody>
    </table>
    <div class="row">
<div class="col-md-4 dataTables_info"><br/>Showing {{(($page - 1) * 10) + 1}} to {{ $slNo-1 }} of {!! $properties->total() !!} entries</div>
        <div class="col-md-8 text-right">
            {!! str_replace('/?', '?', $properties->render()) !!}
        </div>
    </div>
    </div>
</div>
@endsection


@section('page-scripts')

@endsection
