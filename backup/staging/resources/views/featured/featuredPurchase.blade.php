@extends('layouts.landlord-inner')
@section('inner-content')
<script src="{{ asset('public/admin/plugins/jQuery/jQuery-2.1.4.min.js') }}" type="text/javascript"></script>
<div class="content-wrapper">
  @include('elements.breadcrump-landlord')
  
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="contact-details">
			<div class="text-center refresh">
				<div><img src="{{ asset('public/img/property-preloader.GIF')}}"></div>
				Please wait....Do not refresh..
			</div>
				{!! Form::model('',['method' => 'POST','class'=>'form-inline','id'=>'featuredPay','url'=>'https://www.paypal.com/cgi-bin/webscr']) !!}
				
				<?php $bussEmail =  Config::get('constants.PAYPAL_BUSS_EMAIL');
					  $cancelUrl =  URL::to('landlords/featuredproperties').'?paid=0';
					  $successUrl= URL::to('landlords/featuredproperties').'?paid=1';
					  $notifyUrl = URL::to('/featured_paypal_payment_notify');
				?>
				
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="business" value="{{$bussEmail}}"> 
					<input type="hidden" name="currency_code" value="USD">
					
					<input type="hidden" name="item_number" value="{{ $featured->id }}">
					<input type="hidden" name="item_name" value="Featured property for {{ $featured->duration }}">
					<input type="hidden" name="amount" value="{{ $featured->price }}">
					<input type="hidden" name="first_name" value="{{ $loggedLandlord->firstname }}">
					<input type="hidden" name="last_name" value="{{ $loggedLandlord->lastname}}">
					<input type="hidden" name="custom" value="{{ $featuredPurchase->id }}">
					<input type="hidden" name="cancel_return" value="{{ $cancelUrl }}">
                    <input type="hidden" name="return" value="{{ $successUrl }}"> 
                    <input type="hidden" name="notify_url" value="{{ $notifyUrl}}">
				{!!Form::close() !!}
				</div>
				
			<!--{!!Form::close() !!}-->
			
			<script>
			$(document).ready(function(){
				$("#featuredPay").submit();
			});
			</script>
		</div>
	</div>
	
</div>
@endsection
