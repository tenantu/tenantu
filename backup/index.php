<?php
include 'include/dbConnection.php';
include 'class/tenantuClass.php';

$msg = '';
if(!empty($_POST)){
	$name = $_POST['txtname'];
	$phone = $_POST['txtphone'];
	$email = $_POST['txtemail'];
	$school = $_POST['txtschool'];
	$message = $_POST['txtArea'];
	$radioButton = $_POST['radioGroup'];
	$res = $tenantuObject->signIn($conn, $name, $phone, $email, $school, $message, $radioButton);
	if($res == 'true'){
		$msg = 'Your enquiry has been submitted!';
	}
	else{
		$msg = 'Something went wrong!';
	}
}
?>
<!DOCTYPE html>
<!-- saved from url=(0025)http://localhost/tenantu/ -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>TENANTU coming soon</title>

<!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/bootstrap-theme.css" rel="stylesheet">

<!-- siimple style -->
<link href="css/style.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
</head>

<body>

	<div id="wrapper2">
		<div class="container">
			<div class="row bg">
				<div class="col-sm-3 col-md-3 col-lg-3"></div>
				<div class="col-sm-6 col-md-6 col-lg-6">
					<img src="img/logo.png">
					<h2 class="subtitle">Coming Soon</h2>
					<h3>Whether you're a student or a landlord, see how TenantU can make off-campus life as easy as it should be!</h3>
					<p style="color: springgreen;">
					<?php
						echo "$msg";
					?>
					</p>
					<div class="form-bottom">
						<form role="form" id="signUpForm" name="signUpForm" action="" method="post" class="registration-form">
							<div class="form-group">
								<label class="radio-inline">
									<input name="radioGroup" id="radio1" value="Landlord" type="radio"><span> Landlord</span>
								</label>
								<label class="radio-inline">
									<input name="radioGroup" id="radio2" value="Student" checked="" type="radio"><span> Student</span>
								</label>
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-first-name">Name</label>
								<input type="text" id="txtname" name="txtname" placeholder="Name" class="form-first-name form-control" >
								<span class="text-danger"></span>
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-last-name">Phone</label>
								<input type='text' id="txtphone" name="txtphone" placeholder="(541) 754-3010" class="form-last-name form-control" >
								<span class="text-danger"></span>
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-email">Email</label>
								<input type="text" id="txtemail" name="txtemail" placeholder="Enter a valid email address" class="form-email form-control" >
								<span class="text-danger"></span>
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-email"><span>sgfsg</span>School/University</label>
								<input type="text" id="txtschool" name="txtschool" placeholder="School / University" class="form-email form-control" >
								<span class="text-danger"></span>
							</div>
							<div class="form-group">
								<label class="sr-only" for="form-about-yourself">Message</label>
								<textarea id="txtArea" name="txtArea" placeholder="Message" class="form-about-yourself form-control" ></textarea>
								<span class="text-danger"></span>
							</div>

							<button type="submit" id="btsubmit" name="btsubmit" class="btn">Submit</button>
						</form>
					</div>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3"></div>
			</div>

		</div>
	</div>

	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<div id="liveboard-is-installed"></div>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script>
$(function() {
    $("#signUpForm").validate({

        errorElement: 'span',
        errorClass: 'text-danger',

       rules: {

                txtname: {
                    required: true,
                },
                txtphone: {
                	required: true,
                	phoneUS: true,
                },
                txtemail: {
                    required: true,
                    email: true,

                },
                txtschool: {
                	required: true,
                },
                txtArea: {
                    required: true,
                },

            }, 

            messages: {
                txtname: "Please enter your name",
                txtphone:{
                	required:"Please enter your phone number",
                	phoneUS: "Please enter a valid phone number",
                },
                txtemail:{
                	required: "Please enter your email id",
                	email: "Please enter a valid email id",
                },
                txtschool: "Please enter your school/university",
                txtArea: "Please enter your message",
            },
    });
});
</script>
</body>
</html>
