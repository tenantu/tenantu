<?php
class tenantu { 
    
	function signIn($conn, $name, $phone, $email, $school, $message, $role) {
		
		$insertQuery = $conn->prepare("INSERT INTO `landing_users`(`name`, `phone`, `email`, `school`, `message`, `role`)
						VALUES (:name, :phone, :email, :school, :message, :role)");
		
    	$insertQuery->bindParam(':name', $name);
    	$insertQuery->bindParam(':phone', $phone);
    	$insertQuery->bindParam(':email', $email);
    	$insertQuery->bindParam(':school', $school);
    	$insertQuery->bindParam(':message', $message);
    	$insertQuery->bindParam(':role', $role);

    	if ($insertQuery->execute()) {

			$to      = 'jacob.jeifa@tenantu.com';
			$subject = 'New enquiry from tenantu.com';
			$message = 'Inquiry details are the following'.PHP_EOL.
						"Name			  : $name".PHP_EOL.
						"Phone			  : $phone".PHP_EOL.
						"Email			  : $email".PHP_EOL.
						"School/University: $school".PHP_EOL.
						"Message		  : $message".PHP_EOL;
			$headers = 'From: admin@tenantu.com' . "\r\n" .
			    'Reply-To: webmaster@tenantu.com' . "\r\n" .
			    'X-Mailer: PHP/' . phpversion();

			mail($to, $subject, $message, $headers);
			return true;
		}
		else{
			return false;
		}
    }

}

$tenantuObject = new tenantu(); 
?>
