<?php

return [

'multi' => [
    
    'admin' => [
        'driver' => 'eloquent',
        'model'  => App\Http\Models\Admin::class,
        'table'  => 'admins'
    ],
    'tenant' => [
        'driver' => 'eloquent',
        'model'  => App\Http\Models\Tenant::class,
        'table'  => 'tenants'
    ],
    'landlord' => [
        'driver' => 'eloquent',
        'model'  => App\Http\Models\Landlord::class,
        'table'  => 'landlords'
    ]
 ],

    'password' => [
        'email' => 'emails.password',
        'table' => 'password_resets',
        'expire' => 60,
    ],


];
