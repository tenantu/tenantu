<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */
	
	/*'facebook' => [
		'client_id' => '101266093570352',
		'client_secret' => '729d6b1a1752bbe86ef23e6ab79c71b5',
		'redirect' => 'http://192.168.1.14/tenantu/auth/facebook/callback',
    ],*/
	
	'facebook' => [
		'client_id' => '491795451006116',
		'client_secret' => '23f122007d3244a17504a8ad4307868b',
		'redirect' => 'http://tenantu.com/auth/facebook/callback',
    ],
	
    // 'mailgun' => [
    //     'domain' => 'sandbox332330f101f24ea1bc4455833791f920.mailgun.org',
    //     'secret' => 'key-06f5d1f0dc4f91ec307e49f2480bb56f',
    // ],

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\Http\Models\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

];

?>
