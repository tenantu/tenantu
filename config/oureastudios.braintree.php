<?php

return [

	/*
    |--------------------------------------------------------------------------
    | Enviroment
    |--------------------------------------------------------------------------
    |
    | Please provide the enviroment you would like to use for braintree.
    | This can be either 'sandbox' or 'production'.
    |
    */
	'environment' => 'production',

	/*
    |--------------------------------------------------------------------------
    | Merchant ID
    |--------------------------------------------------------------------------
    |
    | Please provide your Merchant ID.
    |
    */
	'merchantId' => '25rnb7f3h5pkqkfx',

	/*
    |--------------------------------------------------------------------------
    | Public Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Public Key.
    |
    */
	'publicKey' => 'rmxxx3hsfdzg5cdq',

	/*
    |--------------------------------------------------------------------------
    | Private Key
    |--------------------------------------------------------------------------
    |
    | Please provide your Private Key.
    |
    */
	'privateKey' => 'b3eed01ff94d69c4c8e823f3363cc960',

	/*
    |--------------------------------------------------------------------------
    | Client Side Encryption Key
    |--------------------------------------------------------------------------
    |
    | Please provide your CSE Key.
    |
    */
	'clientSideEncryptionKey' => 'MIIBCgKCAQEArBkfV8rcy/P5GT88vuqumN3bvSpGukfDbHdoNxlYHbh9ZVVEf/nt4IfaiYjppxask7pceJWnVNU/x8h5KprxHnuvMXREf4gzCm/OBdUsGnFxAN9bXuLmGw1DLr9hRj/OlwxRFklnGtR7SWAQorbTO00PI4UOJeGuD9l/wdR7wTvyH/gy2LMjPyikYd9BRU0mkZ/sysMo8qPkaE/VMt/Yj/9WgQqwTSwJBL4aC4WiC9N+FmyuqXDhhJ9ujXCTKQOAIA6B0AqXIvTJRKFgxd8DO0Dm8Bs06IaY482ZPJHsQW+jHel1/jRcMCeNl1SSPiTTFmzYlK7Lo5c0GpConyG6JwIDAQAB',
	
];
