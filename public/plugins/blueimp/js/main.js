/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */
$(function () {
    'use strict';

    if( window.location.hostname == 'localhost'){
		var baseUrl = 'http://localhost/tenantu/';
    }else if( window.location.hostname == 'fodof.net') {
		var baseUrl = 'http://fodof.net/tenantu/';
	}
	else if(window.location.hostname == 'tenantu.com'){
	var baseUrl = '';
	}
	 else {
	//	alert('please set base url in blueimp/js/main.js');
    //baseUrl="192.168.32.203/tenantu/";
    var baseUrl = "/tenantu";
	}

	var img_list = [];
    var propId = $('#propId').val();
    // Initialize the jQuery File Upload widget:
    $('#propertyAddForm')
    .fileupload({
        // Uncomment the following to send cross-domain cookies:
        // xhrFields: {withCredentials: true},
        url: baseUrl + '/landlords/property/upload/',
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        autoUpload: true,
        formData: {example: 'test'},
    })
    .bind('fileuploadcompleted', function (e, data) {
			$.each(data.result, function (index, file) {
				if(file.length !== 0 && file[0].name){
                    save_to_database( file[0].name );
				}	
			});
	})
    .bind('fileuploaddestroyed', function (e, data) {
        delete_from_database_blueimp( data.name );
    })

    // Enable iframe cross-domain access via redirect option:
    $('#propertyAddForm').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

        // Load existing files:
        // $('#propertyAddForm').addClass('fileupload-processing');
        // // console.log("Sssssssss--:", $('#propertyAddForm').fileupload('option', 'url') );
        // $.ajax({
        //     // Uncomment the following to send cross-domain cookies:
        //     //xhrFields: {withCredentials: true},
        //     url: $('#propertyAddForm').fileupload('option', 'url'),
        //     dataType: 'json',
        //     context: $('#propertyAddForm')[0]
        // }).always(function () {
        //     $(this).removeClass('fileupload-processing');
        // }).done(function (result) {
        //     alert("ssssss");
        //     console.log("result::", JSON.stringify(result) );
        //     $(this).fileupload('option', 'done')
        //         .call(this, $.Event('done'), {result: result});
        // }); 

    // save uploaded image details to database
    function save_to_database( imgName ){

        $.ajax({
          url: baseUrl + '/property/saveImage',
          type: "post",
          data :{'imgName': imgName, 'propId': propId},
          success: function(data){
              $('#showMsg').html("<span>Added Successfully</span>");
              $('#showMsg span').fadeOut( 4000 );
            }
        });
    }

    // delete blueimp uploaded image
    function delete_from_database_blueimp( imgName ){

        $.ajax({
          url: baseUrl + '/property/deleteImage',
          type: "post",
          data :{'imgName': imgName, 'propId': propId},
          success: function(data){
              $('#showMsg').html("<span>Removed Successfully</span>");
              $('#showMsg span').fadeOut( 4000 );
            }
        });
    }

    // Deleted images loaded from DB
    $(document).on( 'click', '.imgDel', delete_from_database_table );
    function delete_from_database_table(event){

        event.preventDefault();
        var tbId = $(this).attr("data-tbId");
        bootbox.confirm("Do you want to delete this image?", function(result) {
          if(result){
            $.ajax({
              url: baseUrl + '/property/deleteImageDB',
              type: "post",
              data :{'propImgId': tbId, 'propId': propId},
              success: function(data){
                 $('#imgColumn'+tbId).remove();
                 
                 if($('div.imgColumn').length==0){

                    $('#noImgMessage').html('No Images uploaded');
                    $('#noImgMessage').show();
                 }
                  $('#showMsg').html("<span>Removed Successfully</span>");
                  $('#showMsg span').fadeOut( 4000 );
                }
            });
          
          }
        });
    }

    // Set featured Image
    var shwmsg = '';
    $(document).on( 'click', '.featImgSel', set_featured_img );
    function set_featured_img(){
        var tbId = $(this).attr("data-tbId");
        $.ajax({
          url: baseUrl + '/property/setImageFeatured',
          type: "post",
          data :{'propImgId': tbId,'propId': propId},
          success: function(data){
              $('#showMsg').html("<span>Featured Image Selected</span>");
              $('#showMsg span').fadeOut( 4000 );
              $( ".uploaded-image" ).removeClass( "active");
              $('#featuredId'+tbId).addClass( "active" );
            }
        });
    }


    // doc upload using blueimp
    $('#propertyDoc').fileupload({
        // Uncomment the following to send cross-domain cookies:
        // xhrFields: {withCredentials: true},
        url: baseUrl + '/landlords/property/upload',
        acceptFileTypes: /^application\/(pdf|doc|docx|vnd.openxmlformats-officedocument.wordprocessingml.document|msword)$/i,
        autoUpload: true,
        formData: {example: 'test'},
    })
    .bind('fileuploadcompleted', function (e, data) {
            $.each(data.result, function (index, file) {
                if(file.length !== 0 && file[0].name){
                    save_doc_to_database( file[0].name );
                }   
            });
    })
    .bind('fileuploaddestroyed', function (e, data) {
        delete_doc_from_database_blueimp( data.name );
    })

    // save uploaded doc details to database
    function save_doc_to_database( docName ){

        $.ajax({
          url: baseUrl + '/property/saveDoc',
          type: "post",
          data :{'docName': docName, 'propId': propId},
          success: function(data){
              $('#showMsgDoc').html("<span>Added Successfully</span>");
              $('#showMsgDoc span').fadeOut( 4000 );
            }
        });
    }

    // delete blueimp uploaded doc
    function delete_doc_from_database_blueimp( docName ){

        $.ajax({
          url: baseUrl + '/property/deleteDoc',
          type: "post",
          data :{'docName': docName, 'propId': propId},
          success: function(data){
              $('#showMsgDoc').html("<span>Removed Successfully</span>");
              $('#showMsgDoc span').fadeOut( 4000 );
            }
        });
    }

    // Deleted Doc loaded from DB
    $(document).on( 'click', '.docDel', delete_doc_from_database_table );
    function delete_doc_from_database_table(event){

        event.preventDefault();
        var tbId = $(this).attr("data-tbId");
        bootbox.confirm("Do you want to delete this document?", function(result) {
        if(result){
          $.ajax({
            url: baseUrl + '/property/deleteDocDB',
            type: "post",
            data :{'propDocId': tbId},
            success: function(data){
               $('#docRow'+tbId).remove();
                 $('#showMsgDoc').html("<span>Removed Successfully</span>");
                $('#showMsgDoc span').fadeOut( 4000 );
                 if($('tr.docRow').length==0){

                    $('#noDocMessage').html('No Docs uploaded');
                    $('#noDocMessage').show();
               }
            }
          });
        }
      });
    }

    // trigger actual button when clicking on the dummy btn
    $(document).on( 'click', '#submitDummyBtn', submitForm );
    function submitForm(event){
      $('#submitActlBtn').trigger('click');
    }

    $(document).on( 'click', '#cancelDummyBtn', cancelForm );
    function cancelForm(event){
      $('#cancelActlBtn').trigger('click');
    }  

});
