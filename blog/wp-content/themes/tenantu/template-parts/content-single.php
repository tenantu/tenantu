<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<div class="post-container">
	<?php if ( has_post_thumbnail() ) {?>
	<div class="post-image">
		<?php the_post_thumbnail( 'full', array( 'class' => 'img-responsive' ) );?>
	</div>
	<?php } ?>
	<div class="post-details">
		<div class="post-icon">
			<div class="icon">
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/post-icon.png">
			</div>
		</div>
		<div class="post-texts">
			<h3><?php the_title(); ?></h3>
			<div class="post-info">
				<?php echo get_the_date( 'd F Y' ); ?>  I  Comments 
				<span class="comments"><?php echo get_comments_number(); ?></span>  I Posted by: 
				<span class="user"><?php echo get_the_author(); ?></span>
			</div>
			<div class="post-info-details">
				<?php the_content(); ?>
			</div>
		 </div>
	</div>
</div>