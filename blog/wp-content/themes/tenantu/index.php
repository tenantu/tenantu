<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div class="blog-banner">
	<div class="container">
		<div class="blog-banner-texts">
			<h2>Blog</h2>
		</div>
	</div>
</div>
<div class="container blog-container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-4 col-xs-12">

			<?php if ( have_posts() ) : ?>

				<?php if ( is_home() && ! is_front_page() ) : ?>
					<header>
						<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</header>
				<?php endif; ?>

				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				// End the loop.
				endwhile;

				// Previous/next page navigation.
				// the_posts_pagination( array(
				// 	'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				// 	'next_text'          => __( 'Next page', 'twentysixteen' ),
				// 	'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
				// ) );

				?>
				<div class="pagination">
				   	<?php if( get_previous_posts_link() ) ?><div class="pull-left older-posts"><?php previous_posts_link( 'Older Posts' ) ?></div>
				   	<?php if( get_next_posts_link() ) ?><div class="pull-right newer-posts"><?php next_posts_link( 'Newer Posts' ) ?></div>
				</div>
				<?php
			// If no content, include the "No posts found" template.
			else :
				get_template_part( 'template-parts/content', 'none' );

			endif;
			?>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>			
<?php get_footer(); ?>