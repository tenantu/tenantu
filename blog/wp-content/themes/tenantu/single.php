<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="container blog-container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-4 col-xs-12">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>			
<?php get_footer(); ?>