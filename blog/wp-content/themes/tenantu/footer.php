<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

global $options;
?>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?php
					$fargs1 = array(
						'menu'  => 'footer_menu1',
						'container'       => 'div',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'left-menu'
					);
					wp_nav_menu( $fargs1 );

					$fargs2 = array(
						'menu'  => 'footer_menu2',
						'container'       => 'div',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => 'left-menu'
					);
					wp_nav_menu( $fargs2 );
					?>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 col-lg-offset-1 col-md-offset-1 footer-logo"><img src="<?php echo $options['header_option_values']['footer_logo'];?>"></div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
					<div class="social-icons">
						<ul>
							<?php if( $options['header_option_values']['twitter'] ) ?><li><a href="<?php echo $options['header_option_values']['twitter'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png"></a></li>
							<?php if( $options['header_option_values']['facebook'] ) ?><li><a href="<?php echo $options['header_option_values']['facebook'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png"></a></li>
							<?php if( $options['header_option_values']['instagram'] ) ?><li><a href="<?php echo $options['header_option_values']['instagram'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png"></a></li>
							<?php if( $options['header_option_values']['email'] ) ?><li><a href="mailto:<?php echo $options['header_option_values']['email'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/mail.png"></a></li>
						</ul>
					</div>
					<?php if( $options['header_option_values']['copyright'] ) ?><div class="copyright"><?php echo $options['header_option_values']['copyright'];?></div>
				</div>
			</div>
		</div>
	</footer>	
</div>
<?php wp_footer(); ?>
<script>
jQuery(document).ready(function($) {
	$(".nav_trigger").click(function() {
		$("body").toggleClass("show_sidebar");
		$(".nav_trigger .fa").toggleClass("fa-navicon fa-times"); // toggle 2 classes in Jquery: http://goo.gl/3uQAFJ - http://goo.gl/t6BQ9Q
	});
});
</script>
</body>
</html>