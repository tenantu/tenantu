<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
<?php global $options; ?>
<body <?php body_class(); ?>>
<div class="container-fluid subpage-top-cntr">
	<div class="container">
		<!-- nav section starts -->
		<!-- //////// nav section ends //////// -->
		<div id="push_sidebar">
			<?php
			$args = array(
				'theme_location'  => 'primary',
				'container'       => 'div',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'right-menu'
			);

			wp_nav_menu( $args );
			?>
			<div class="social-icons">
				<ul>
					<?php if( $options['header_option_values']['twitter'] ) ?><li><a href="<?php echo $options['header_option_values']['twitter'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png"></a></li>
					<?php if( $options['header_option_values']['facebook'] ) ?><li><a href="<?php echo $options['header_option_values']['facebook'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png"></a></li>
					<?php if( $options['header_option_values']['instagram'] ) ?><li><a href="<?php echo $options['header_option_values']['instagram'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png"></a></li>
					<?php if( $options['header_option_values']['email'] ) ?><li><a href="mailto:<?php echo $options['header_option_values']['email'];?>"><img src="<?php echo get_template_directory_uri(); ?>/img/mail.png"></a></li>
				</ul>
			</div>
			<div class="address">
				<?php if( $options['header_option_values']['address'] ) echo $options['header_option_values']['address']; ?>
				<?php if( $options['header_option_values']['email'] ) ?><div>Email : <a href="mailto:<?php echo $options['header_option_values']['email'];?>"><?php echo $options['header_option_values']['email'];?></a></div>
			</div>
		</div>
		<!-- nav section ends -->
		<!-- //////// ends //////// -->
		<!-- man section -->
		<!-- //////// starts //////// -->
		<div id="wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 scl-icons">
						<div class="social-icons">
							<ul>
								<?php if( $options['header_option_values']['twitter'] ) ?><li><a href="<?php echo $options['header_option_values']['twitter'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png"></a></li>
								<?php if( $options['header_option_values']['facebook'] ) ?><li><a href="<?php echo $options['header_option_values']['facebook'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png"></a></li>
								<?php if( $options['header_option_values']['instagram'] ) ?><li><a href="<?php echo $options['header_option_values']['instagram'];?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png"></a></li>
								<?php if( $options['header_option_values']['email'] ) ?><li><a href="mailto:<?php echo $options['header_option_values']['email'];?>"><img src="<?php echo get_template_directory_uri(); ?>/img/mail.png"></a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><a href="<?php echo $options['header_option_values']['site_url'];?>" class="tenantu-logo"><img src="<?php echo $options['site_logo'];?>"></a></div>
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
						<div class="sub-top-menu">
							<span class="nav_trigger"><i class="fa fa-navicon"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- man section ends-->
		<!-- //////// ends //////// -->
	</div>
</div>