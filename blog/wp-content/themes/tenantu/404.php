<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div class="blog-banner">
	<div class="container">
		<div class="blog-banner-texts">
			<h2><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?></h2>
		</div>
	</div>
</div>
<div class="container blog-container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-4 col-xs-12">

			<section class="error-404 not-found">
				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentysixteen' ); ?></p>

					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<?php get_sidebar(); ?>
		</div>
	</div>
</div>			
<?php get_footer(); ?>